export { default as app } from 'modules/App/config'
export { default as google } from 'modules/Google/config'
export { default as apollo } from 'modules/Apollo/config'
export { default as intercom } from 'modules/Intercom/config'
