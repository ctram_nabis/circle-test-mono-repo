import isEmpty from 'lodash/isEmpty'
import isNumber from 'lodash/isNumber'
import isUndefined from 'lodash/isUndefined'

export const exists = (val: number | string) => isNumber(val) || !isEmpty(val)
export const isDecimal = (val: number | string) => /^[+-]{0,1}?\d*(\.\d{0,2})$/.test(`${val}`)
export const isInteger = (val: number | string) => /^[+-]{0,1}?\d*$/.test(`${val}`)
export const isPositive = (val: number | string) => /^\d*(\.\d{0,2})?$/.test(`${val}`)
export const isNegative = (val: number | string) => /^[-]\d*(\.\d{0,2})?$/.test(`${val}`)
export const minMax = (val: number | string, min: string, max: string) => val >= min && val <= max

export { isEmpty, isNumber, isUndefined }
