import { strFormat } from '../formatters/strFormat'

export const isValidEmail = (email: string) => /\S+@\S+/.test(email)

export const formatEmail = (email: string) => strFormat(email, { trim: true, lowercase: true })
