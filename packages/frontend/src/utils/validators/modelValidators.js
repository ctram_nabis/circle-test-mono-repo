import idx from 'idx'
import { isEmpty, isUndefined } from 'lodash'

// interface IsOrderValidProps {
//   date: Date
//   lineItems: LineItem[]
//   name: String
//   action: typeof ORDER_ACTION
//   daysTillPaymentDue: number
//   site: Site
//   discount: number
// }

// model validators return a list of errors for that model

const validateLineItemsValid = (props, state) => {
  const errors = []
  const { lineItems } = state
  lineItems.forEach((lineItem, ind) => {
    const liError = []
    if (lineItem.skuBatch) {
      if ((lineItem.name && lineItem.name.length === 0) || isEmpty(lineItem.name)) {
        liError.push('name is missing')
      }
      if (Number(lineItem.pricePerUnit) === 0) {
        liError.push('price is 0')
      }
      if (!lineItem.quantity) {
        liError.push('quantity is missing')
      } else if (lineItem.quantity) {
        if (Number(lineItem.quantity) === 0) {
          liError.push('quantity is 0')
        }
        if (Number(lineItem.quantity) > 0) {
          const oversoldError = validateIsOversold(props, state, lineItem)
          if (oversoldError) {
            liError.push(oversoldError)
          }
        }
      }
      const tooManyError = validateTooManyLineItems(props, state, lineItem)
      if (tooManyError) {
        liError.push(tooManyError)
      }
    } else {
      liError.push('Please select a SKU')
    }
    if (liError.length > 0) {
      let message = liError.reduce((accum, e, j) => {
        if (j === 0) {
          return (accum += e)
        } else {
          return (accum += `, ${e}`)
        }
      }, `LineItem ${ind + 1}: `)
      errors.push(message)
    }
  })
  return errors
}

const validateTooManyLineItems = (_props, state, lineItem) => {
  if (lineItem.skuBatch) {
    const { lineItems } = state
    const match = ({ skuBatch }) => (skuBatch && skuBatch.id) === lineItem.skuBatch.id
    const isSample = (_lineItem) =>
      _lineItem.isSample || (_lineItem.pricePerUnit && Number(_lineItem.pricePerUnit)) === 0.01
    const numSkus = lineItems.filter((_lineItem) => match(_lineItem) && !isSample(_lineItem)).length
    const numSamples = lineItems.filter((_lineItem) => match(_lineItem) && isSample(_lineItem)).length

    if (numSkus > 2 || numSamples > 2) {
      return `orders have a maximum of 2 line items and 2 samples per SKU`
    }
  }

  return null
}

const getCombinedQuantityForSku = ({ lineItems = [] }, { id }) =>
  lineItems.reduce((acc, li) => ((li.skuBatch && li.skuBatch.id) === id ? acc + Number(li.quantity) : acc), 0)

const validateIsOversold = (props, state, lineItem) => {
  const { skuBatch } = lineItem
  const { editOrder } = props

  if (skuBatch) {
    const oak = idx(skuBatch, (_) => _.inventoryValues.OAK.available) || 0
    const la = idx(skuBatch, (_) => _.inventoryValues.LA.available) || 0
    const available = oak + la

    const originalOrder = props.originalOrder || {}

    const oldQuantity = getCombinedQuantityForSku(originalOrder, skuBatch)
    let newQuantity = getCombinedQuantityForSku(state, skuBatch)

    // check if order was already allocated
    const wasAllocated =
      editOrder && ['INTERNAL_WAREHOUSE_TRANSFER', 'DELIVERY_TO_RETAILER'].includes(props.order.action)

    if (state.action === 'DELIVERY_TO_RETAILER') {
      if (wasAllocated) {
        // get the delta of the quanity
        newQuantity = newQuantity - oldQuantity
      }

      if (newQuantity > Math.max(0, available)) {
        if (wasAllocated) {
          return `oversold! total change in quantity ( ${newQuantity} ) exceeds available ( ${available} )`
        } else {
          return `oversold! total quantity ( ${newQuantity} ) exceeds available ( ${available} )`
        }
      }
    }
  }

  return null
}

export const isOrderValid = (props, state) => {
  const subtotal = state.lineItems
    ? state.lineItems.reduce((acc, val) => {
        let total = acc
        total += parseFloat(val.quantity) * parseFloat(val.pricePerUnit)
        return total
      }, 0)
    : 0
  let errors = []
  if (isEmpty(state.name)) {
    errors.push('Order name is missing')
  }
  if (!isEmpty(state.action) && state.action.toLowerCase() === 'dropoff' && isUndefined(state.daysTillPaymentDue)) {
    errors.push('Payment terms are missing')
  }
  if (isEmpty(state.site) || (state.site && !state.site.licensedLocation)) {
    errors.push(
      !isEmpty(state.action) && ['DELIVERY_TO_RETAILER', 'DELIVERY_TO_BRAND'].includes(state.action)
        ? 'Destination is missing'
        : 'Pickup Location is missing',
    )
  } else {
    if (isEmpty(state.site.pocName)) {
      errors.push('POC name is missing')
    }
    if (isEmpty(state.site.pocPhoneNumber)) {
      errors.push('POC phone number is missing')
    }
    if (isEmpty(state.site.pocEmail)) {
      errors.push('POC email is missing')
    }
  }
  if (isEmpty(state.lineItems)) {
    errors.push('Line item(s) missing')
  } else {
    errors = errors.concat(validateLineItemsValid(props, state))
  }
  if (state.action === 'DELIVERY_TO_RETAILER' && typeof state.daysTillPaymentDue === 'undefined') {
    errors.push('Payment terms are missing')
  }
  if (subtotal > 0 && parseFloat(state.discount) > subtotal) {
    errors.push(`Discount must be less than the subtotal`)
  }
  if (!state?.date) {
    errors.push('Delivery Date is missing')
  }
  return errors
}
