export default {}

/**
 *  WE SHOULD USE YUP FOR VALIDATION INSTEAD
 *
import { LineItem, Order } from 'generated/graphql'
import { exists, isEmpty, isUndefined } from './validators'



interface Props {}

const validateLineItemsValid = (props: Props, state: Order) => {
  const errors = []
  const { lineItems } = state

  lineItems.forEach((lineItem, ind) => {
    const lineItemErrors = []

    if (lineItem.skuBatch) {
      if ((lineItem.name && lineItem.name.length === 0) || exists(lineItem.name)) {
        lineItemErrors.push('name is missing')
      }

      if (Number(lineItem.pricePerUnit) === 0) lineItemErrors.push('price is 0')

      if (!lineItem.quantity) {
        lineItemErrors.push('quantity is missing')
      } else if (lineItem.quantity) {
        if (Number(lineItem.quantity) === 0) {
          lineItemErrors.push('quantity is 0')
        }
      }

      const tooManyError = validateTooManyLineItems(props, state, lineItem)

      if (tooManyError) {
        lineItemErrors.push(tooManyError)
      }
    } else {
      lineItemErrors.push('Please select a SKU')
    }
    if (lineItemErrors.length > 0) {
      const message = lineItemErrors.reduce(
        (accum, e, j) => (j === 0 ? (accum += e) : (accum += `, ${e}`)),
        `LineItem ${ind + 1}: `,
      )

      errors.push(message)
    }
  })

  return errors
}

const validateTooManyLineItems = <P extends object, S extends Order>(_props: P, state: S, lineItem: LineItem) => {
  if (lineItem.skuBatch) {
    const { lineItems } = state

    const isLineItemSample = ({ isSample, pricePerUnit }: LineItem) =>
      isSample || (pricePerUnit && Number(pricePerUnit)) === 0.01

    const match = ({ skuBatch }: LineItem) => (skuBatch && skuBatch.id) === lineItem.skuBatch.id

    const numSkus = lineItems.filter((_lineItem) => match(_lineItem) && !isLineItemSample(_lineItem)).length

    const numSamples = lineItems.filter((_lineItem) => match(_lineItem) && isLineItemSample(_lineItem)).length

    if (numSkus > 2 || numSamples > 2) {
      return 'orders have a maximum of 2 line items and 2 samples per SKU'
    }
  }
}

export const isOrderValid = <P extends object, S extends Order>(props: P, state: S) => {
  const { lineItems = [], name, action, daysTillPaymentDue, site, discount, date } = state

  const subtotal = lineItems.reduce((acc, { quantity, pricePerUnit }) => acc + quantity * pricePerUnit, 0)

  let errors = []

  if (isEmpty(name)) errors.push('Order name is missing')

  if (!isEmpty(action) && (action.toLowerCase() === 'dropoff' && isUndefined(daysTillPaymentDue))) {
    errors.push('Payment terms are missing')
  }

  if (isEmpty(site) || (site && !site.licensedLocation)) {
    errors.push(
      !isEmpty(action) && ['DELIVERY_TO_RETAILER', 'DELIVERY_TO_BRAND'].includes(action)
        ? 'Destination is missing'
        : 'Pickup Location is missing',
    )
  } else {
    if (isEmpty(site.pocName)) errors.push('POC name is missing')
    if (isEmpty(site.pocPhoneNumber)) errors.push('POC phone number is missing')
    if (isEmpty(site.pocEmail)) errors.push('POC email is missing')
  }

  if (isEmpty(lineItems)) {
    errors.push('Line item(s) missing')
  } else {
    errors = errors.concat(validateLineItemsValid(props, state))
  }

  if (action === 'DELIVERY_TO_RETAILER' && typeof daysTillPaymentDue === 'undefined') {
    errors.push('Payment terms are missing')
  }

  if (subtotal > 0 && discount > subtotal) errors.push('Discount must be less than the subtotal')

  if (isEmpty(date)) errors.push('Delivery Date is missing')

  return errors
}
*/
