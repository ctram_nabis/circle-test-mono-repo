export const sortAlpha = (a: string, b: string) => {
  const _a = a.toLowerCase()
  const _b = b.toLowerCase()
  return _a < _b ? -1 : _a > _b ? 1 : 0
}
