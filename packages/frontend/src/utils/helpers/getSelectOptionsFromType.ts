export const getSelectOptionsFromType = <T>(obj: T) =>
  Object.keys(obj).map((key) => ({
    key,
    value: key,
    text: obj[key],
  }))
