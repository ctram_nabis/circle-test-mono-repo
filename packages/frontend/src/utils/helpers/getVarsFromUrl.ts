import { useRouter } from 'modules/Navigation'

interface QueryParams {
  search?: string
  action?: string
  status?: string
  paymentStatus?: string
  transactionTypes?: string
  warehouseId?: string
  driverId?: string
  vehicleId?: string
  start?: string
  end?: string
  page?: string
}

export const useGetVarsFromURL = () => {
  const router = useRouter()

  const {
    search,
    action,
    status,
    paymentStatus,
    transactionTypes,
    warehouseId,
    driverId,
    vehicleId,
    start,
    end,
    page,
  } = router.query as QueryParams

  return {
    search,
    action,
    status,
    paymentStatus,
    transactionTypes: typeof transactionTypes === 'string' ? transactionTypes.split(',') : transactionTypes,
    warehouseId:
      warehouseId === 'UN_ASSIGNED'
        ? null
        : warehouseId && warehouseId.length
        ? decodeURIComponent(warehouseId)
        : undefined,
    driverId:
      driverId === 'UN_ASSIGNED' ? null : driverId && driverId.length ? decodeURIComponent(driverId) : undefined,
    vehicleId:
      vehicleId === 'UN_ASSIGNED' ? null : vehicleId && vehicleId.length ? decodeURIComponent(vehicleId) : undefined,
    start,
    end,
    pageInfo: {
      page: page || 1,
      numItemsPerPage: 25,
      orderBy: [
        {
          attribute: 'date',
          order: 'DESC',
        },
        {
          attribute: 'createdAt',
          order: 'DESC',
        },
        {
          attribute: 'number',
          order: 'DESC',
        },
      ],
    },
  }
}
