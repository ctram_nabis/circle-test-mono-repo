export const filterMapWithArray = <T extends object, K extends keyof T>(options: T) => (
  allowed = Object.keys(options) as K[],
): Partial<T> =>
  Object.keys(options)
    .filter((key) => allowed.includes(key as K))
    .reduce((accum, key) => {
      accum[key] = options[key]
      return accum
    }, {})
