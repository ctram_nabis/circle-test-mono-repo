import { useMe } from 'modules/User'

export const useGetIsEditable = () => {
  // const status = props.order.status || 'Unscheduled'
  // const roles = props.user.roles || []
  // const { name: roleName } = roles.shift() || {}
  // const organizationId = props.organization.id
  // const orderOrganizationId = props.order.organization.id

  // const isSameOrganization = orderOrganizationId === organizationId
  // const isOwner =
  // Boolean(isSameOrganization && (roleName && ['admin', 'user'].includes(roleName)))

  // return Boolean((['Unscheduled', 'Draft'].includes(titleize(status)) && isOwner) || isAdmin)

  const { user } = useMe()
  return Boolean(user?.isAdmin)
}
