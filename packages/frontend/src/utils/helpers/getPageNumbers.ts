export const getPageNumbers = (page: number, totalPageNumbers: number) => {
  const newPageNumberRange: number[] = []

  const minNumTemp = Math.floor(page / 10) * 10
  const minNum = Math.max(minNumTemp, 1)
  const maxNum = Math.min(minNumTemp + 10, totalPageNumbers)

  for (let i = minNum; i <= maxNum; i++) {
    newPageNumberRange.push(i)
  }
  return newPageNumberRange
}
