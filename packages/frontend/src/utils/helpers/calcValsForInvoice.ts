import { Order } from 'generated/graphql'
import { fixDecimal } from 'utils'

export function calcValsForInvoice(order: Order) {
  const { discount, exciseTax, exciseTaxCollected, gmv, gmvCollected } = order

  const _exciseTaxCollected = exciseTaxCollected || 0

  return {
    collected: fixDecimal(gmvCollected + _exciseTaxCollected),
    due: fixDecimal(gmv - discount + exciseTax - gmvCollected - _exciseTaxCollected),
    gmv: fixDecimal(gmv - discount),
    gmvCollected: fixDecimal(gmvCollected),
    tax: fixDecimal(exciseTax),
    taxCollected: fixDecimal(_exciseTaxCollected),
  }
}
