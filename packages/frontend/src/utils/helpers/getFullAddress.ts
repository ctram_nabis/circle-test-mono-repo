export interface AddressProps {
  address1?: string | null
  address2?: string | null
  city?: string | null
  state?: string | null
  zip?: string | number | null
}

export const getFullAddress = <T extends AddressProps>({ address1, address2, city, state, zip }: T) =>
  `${address1}${address2 && `, ${address2}`}, ${city}, ${state} ${zip}`
