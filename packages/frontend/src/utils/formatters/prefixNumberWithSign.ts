export const prefixNumberWithSign = (n: number): string => `${n >= 0 ? '+' : '-'}${Math.abs(n).toString()}`
