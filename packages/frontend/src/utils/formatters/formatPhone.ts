export const formatPhone = (phone: string) => {
  if (!phone || !phone.length) return ''

  let formatted = '('
  let state = 0
  let count = 0

  for (const p of phone) {
    switch (p) {
      case '(':
      case ')':
      case '-':
      case ' ':
        continue
      default:
        count++
        formatted += p
        if (count === 3 && state === 0) {
          formatted += ') '
          count = 0
          state++
        } else if (count === 3 && state === 1) {
          formatted += '-'
        }
        break
    }
  }
  return formatted
}
