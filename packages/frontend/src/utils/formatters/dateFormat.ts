export const formatDate = (date: Date) => {
  if (!date) return null
  const _d = new Date(date)
  const padNum = (n: number) => {
    const s = String(n)
    if (s.length === 1) return `0${s}`
    return s
  }
  return `${padNum(_d.getUTCMonth() + 1)}/${padNum(_d.getUTCDate())}/${padNum(_d.getFullYear())}`
}
