interface StrFormatOptions {
  trim?: boolean
  lowercase?: boolean
  uppercase?: boolean
}

export function strFormat(str: string, { trim = true, lowercase, uppercase }: StrFormatOptions) {
  let _str = str
  if (trim) _str = _str.trim()
  if (lowercase) _str = _str.toLowerCase()
  if (uppercase) _str = _str.toUpperCase()
  return _str
}
