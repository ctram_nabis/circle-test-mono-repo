export const titleize = (str: string) =>
  str
    .toLowerCase()
    .replace(/-|_/g, ' ')
    .replace(/(?:^|\s|-)\S/g, (m) => {
      return m.toUpperCase()
    })

export default titleize
