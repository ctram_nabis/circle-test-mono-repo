export const fixDecimal = (value: number, decimals = 2) => value.toFixed(decimals)
