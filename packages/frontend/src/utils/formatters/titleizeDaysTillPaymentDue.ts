export const titleizeDaysTillPaymentDue = (num: number) => {
  if (num === 0) return 'COD'
  if (!num) return 'n/a'
  return `Net ${num}`
}

export default titleizeDaysTillPaymentDue
