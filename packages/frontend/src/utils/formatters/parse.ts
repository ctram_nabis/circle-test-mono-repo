export const parseDate = (date: Date) => {
  if (!date) return 'n/a'
  const _date = new Date(date).toISOString()
  const day = _date.split('T')[0].split('-')
  return `${day[1]}/${day[2]}/${day[0]}`
}
