import { exists } from 'utils'

export function formatNumber(value: number, decimals = 2) {
  return exists(value) ? value.toFixed(decimals) : undefined
}
