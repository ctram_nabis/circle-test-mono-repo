const padNumber = (value: number, length: number) => {
  try {
    let str = value.toString()
    while (str.length < length) {
      str = `0${str}`
    }
    return str
  } catch (e) {
    return value
  }
}

export default padNumber
