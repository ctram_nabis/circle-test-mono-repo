interface OptionProps {
  key: string
  text: string
  value: string
}

export const valueToText = (val: string, options: OptionProps[]): string => {
  for (let i = options.length - 1; i >= 0; i--) {
    const current = options[i]
    if (current.value === val) {
      return current.text
    }
  }
  return 'error: invalid value'
}
