export const isTrue = <P extends object, T>(key: keyof P, returnValue: (props: P) => T) => (props: P) =>
  props[key] ? returnValue(props) : undefined

export const isFalse = <P extends object, T>(key: keyof P, returnValue: T) => (props: P) =>
  !props[key] ? returnValue : undefined
