const { REACT_APP_ENV } = process.env

export const isProd = <T>(value: T, defaultValue?: T) => (REACT_APP_ENV === 'production' ? value : defaultValue)

export const isStage = <T>(value: T, defaultValue?: T) => (REACT_APP_ENV === 'staging' ? value : defaultValue)

export const isDev = <T>(value: T, defaultValue?: T) => (REACT_APP_ENV === 'development' ? value : defaultValue)

export const isTest = <T>(value: T, defaultValue?: T) => (REACT_APP_ENV === 'test' ? value : defaultValue)

export const isDemo = <T>(value: T, defaultValue?: T) => (REACT_APP_ENV === 'demo' ? value : defaultValue)

export interface EnvOptionsType<T> {
  development?: T
  production?: T
  staging?: T
  test?: T
  demo?: T
  fallback: T
}

export const isEnv = <T>(options: EnvOptionsType<T>): T => {
  const { development, production, staging, test, demo, fallback } = options
  return isProd(production) || isStage(staging) || isDev(development) || isDemo(demo) || isTest(test) || fallback
}
