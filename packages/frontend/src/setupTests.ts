// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect'

import { GlobalWithFetchMock } from 'jest-fetch-mock'

const customGlobal: GlobalWithFetchMock = global
customGlobal.fetch = require('jest-fetch-mock')
customGlobal.fetchMock = customGlobal.fetch

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
  useParams: () => {},
  useRouteMatch: () => {},
  useLocation: () => ({
    pathname: '',
  }),
  useHistory: () => ({
    push: () => {},
  }),
}))

jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'), // use actual for all non-hook parts
  useParams: () => {},
  useLocation: () => {},
}))

jest.mock('modules/Organization', () => ({
  ...jest.requireActual('modules/Organization'),
  useOrganization: () => ({
    owner: {},
  }),
}))

jest.mock('modules/User', () => ({
  ...jest.requireActual('modules/User'),
  useMe: () => ({
    userRoles: [],
    roles: [],
  }),
}))

export {}
