import * as Sentry from '@sentry/browser'
import { withApolloProvider } from 'modules/Apollo'
import { IS_PRODUCTION } from 'modules/App'
import { PageNotFound } from 'modules/App/containers'
import { withAuthProvider } from 'modules/Auth'
import { AuthSwitch } from 'modules/Auth/containers'
import { LayoutWrapper } from 'modules/Layout'
import { routes, withRouterWrapper } from 'modules/Navigation'
import { withOrganizationProvider } from 'modules/Organization'
import { withUserProvider } from 'modules/User'
import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { compose } from 'recompose'
import { withThemeProvider } from 'system'

if (IS_PRODUCTION) {
  Sentry.init({
    dsn: 'https://53ecb6a6be1a40e99f3d1a3b325cfc9d@sentry.io/1532951',
    // TODO: we should see if we can fix this eventually
    ignoreErrors: ['ResizeObserver loop limit exceeded'],
  })
}

const App = () => (
  <Switch>
    {Object.values(routes).map(({ component: Component, ...routeProps }, i) => (
      <Route key={`route-${i}`} {...routeProps}>
        <LayoutWrapper {...routeProps}>
          <Component />
        </LayoutWrapper>
      </Route>
    ))}
    <Route path="/" exact={true} component={AuthSwitch} />
    <Route path="*" component={PageNotFound} />
  </Switch>
)

export default compose(
  withRouterWrapper,
  withThemeProvider,
  withApolloProvider,
  withAuthProvider,
  withUserProvider,
  withOrganizationProvider,
)(App)
