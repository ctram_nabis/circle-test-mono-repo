/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */

import { ApolloError } from 'apollo-client'
import { GraphQLError } from 'graphql'

export type ChangeEvent = React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>
export interface ChangeData {
  [key: string]: any
  value: any
}

export type ErrorType = string | Error | ApolloError | GraphQLError // | undefined // -- Shouldn't be undefined

export type OnEventTextarea = (event: React.FormEvent<any>, data: ChangeData) => void
export type OnEventInput = (event: ChangeEvent, data: ChangeData) => void
