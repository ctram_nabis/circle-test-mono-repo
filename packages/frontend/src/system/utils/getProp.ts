/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */

/**
 * Get the value of the prop with an accessor function
 * @param accessor (props: (p) => props.somethjing ) => void
 */
export const getProp = <P extends object>(accessor: (props: P) => keyof P) => (props: P) => accessor(props)
