/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */

import { darken, lighten } from 'polished'
import { compose } from 'ramda'
import { Color } from '../theme'
import { themeColor } from './getColor'

/**
 * Get and modify the theme color all in one call
 */
export const lightColor = (color: Color, percent?: number) => compose(lighten(percent || 0.8), themeColor(color))

export const darkColor = (color: Color, percent?: number) => compose(darken(percent || 0.8), themeColor(color))
