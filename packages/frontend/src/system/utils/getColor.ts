/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */
import { Color, ThemedProps } from '../theme/types'
import { themeGet } from './themeGet'

/**
 * Use when you don't want to pass props, but instead have them be auto inserted
 *
 * @example
 * styled.div`
 *   background-color: ${themeColor('primary')}
 * `
 */
export const themeColor = (color: Color) => themeGet((t) => t.colors[color])

/**
 * Use when just wanting to pass prop inside the function
 *
 * @example
 * const MyComponent = withTheme(props => {
 *  const color = withColor('primary', props)
 *  return <div>My color is {color}</div>
 * })
 */
export const getColor = <P extends ThemedProps>(color: Color, props: P) => themeColor(color)(props)
