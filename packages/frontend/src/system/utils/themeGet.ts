/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */
import { Theme } from '../theme'

/**
 * themeGet - allows you to get inner objects from the theme via an accessor script
 * @param accessor pass a function like idx to get the inner object of the theme
 *
 * @example const mySerifFont = themeGet(t => t.fonts.serif)
 */
export const themeGet = <P extends { theme?: T }, T extends Theme>(accessor: (theme: T, props: P) => string) => (
  props: P,
) => accessor(props.theme!, props)
