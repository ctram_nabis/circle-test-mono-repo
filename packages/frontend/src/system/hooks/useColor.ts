/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */

import { Color } from '../theme/types'
import { useTheme } from './useTheme'

/**
 * useColor returns the themed color via hooks!
 *
 * @example
 * const MyComponent = (props: { color: Color }) => {
 *    const _color = useColor('primary') // returns $theme.colors['primary]
 * }
 */
export const useColor = (color: Color) => {
  const theme = useTheme()
  return color && (theme.colors[color] || color)
}
