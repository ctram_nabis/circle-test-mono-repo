/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */
import React from 'react'
import { ThemeContext } from 'styled-components'
import { Theme } from '../theme'

export const useTheme = () => React.useContext(ThemeContext as React.Context<Theme>)
