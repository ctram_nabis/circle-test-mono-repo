/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */
import { Color } from '../theme'
import { useTheme } from './useTheme'

export const useThemeUI = () => {
  const theme = useTheme()

  const components: JSX.Element[] = []

  const getColor = (color: Color) => color && theme.colors[color]

  return { components, theme, getColor }
}
