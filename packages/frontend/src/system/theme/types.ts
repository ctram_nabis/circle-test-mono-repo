/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */
import { theme } from './theme'

export type FontFamily = keyof typeof theme.fonts
export type FontWeight = keyof typeof theme.fontWeights

export type Colors = typeof theme.colors
// TODO: ONCE WE HAVE THE THEMME PROVIDER WORKING TAKE OUT THE | string
export type Color = keyof Colors | string
export type Gradient = keyof typeof theme.gradients

export type ColorMode = keyof typeof theme.modes

export type Theme = typeof theme

export interface ThemedProps {
  theme: Theme
}

export type WithTheme<P> = P & ThemedProps
// TODO: ONCE WE HAVE THE THEMME PROVIDER WORKING TAKE OUT THE | string
export interface ColorProp {
  color?: Color | string
}
