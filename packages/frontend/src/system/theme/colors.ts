/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */
import { lighten } from 'polished'

const palette = {
  accent1: '#2185d0',
  accent2: '#fd7e14',
  accent3: '#17a2b8',
  accent4: '#6f42c1',
  accent5: '#e83e8c',
  accent6: '#20c997',
  accent7: '#ffc107',
  accent8: '#007bff',

  error: '#dc3545',
  warning: '#ffc107',
  success: '#28a745',
  info: '#2185d0',

  shade0: '#FFFFFF', // always white
  shade1: '#F1F2F6',
  shade2: '#CBCFD6',
  shade3: '#A4B0BE',
  shade4: '#747D8C',
  shade5: '#525864',
  shade6: '#2F3542',
  shade7: '#1B1E26',
  shade8: '#090B0E',
  shade9: '#000000', // always black

  brand: '#ffcd00',
  primary: '#2185d0',
  secondary: '#A4B0BE',

  white: '#FFFFFF',
  white80: '#FFFFFF80',
  white60: '#FFFFFF60',
  white40: '#FFFFFF40',
  white20: '#FFFFFF20',
  white10: '#FFFFFF10',
  black: '#000000',
  black80: '#00000080',
  black60: '#00000060',
  black40: '#00000040',
  black20: '#00000020',
  black10: '#00000010',
  transparent: 'transparent',
}

export const colors = {
  ...palette,

  text: palette.shade8,
  muted: palette.shade5,
  background: palette.shade1,
}

export const darkColors = {
  ...palette,
  shade0: palette.shade9,
  shade1: palette.shade8,
  shade2: palette.shade7,
  shade3: palette.shade6,
  shade4: palette.shade5,
  shade5: palette.shade4,
  shade6: palette.shade3,
  shade7: palette.shade2,
  shade8: palette.shade1,
  shade9: palette.shade0,

  text: palette.shade1,
  muted: palette.shade3,
  background: palette.shade8,
}

export const gradients = {
  primary: [colors.accent1, lighten(0.1, colors.accent1)],
}
