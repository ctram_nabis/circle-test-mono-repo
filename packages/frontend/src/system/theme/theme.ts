/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */
import { colors, darkColors, gradients } from './colors'

const heading = {
  color: 'text',
  fontFamily: 'heading',
  lineHeight: 'heading',
  fontWeight: 'heading',
}

export const theme = {
  colors,

  gradients,

  modes: {
    light: colors,
    dark: darkColors,
  },

  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],

  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96],

  fonts: {
    body: 'system-ui, sans-serif',
    heading: 'inherit',
    monospace: 'Menlo, monospace',
  },

  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700,
  },

  lineHeights: {
    body: 1.5,
    heading: 1.125,
  },

  styles: {
    root: {
      fontFamily: 'body',
      lineHeight: 'body',
      fontWeight: 'body',
    },
    h1: {
      ...heading,
      fontSize: 5,
    },
    h2: {
      ...heading,
      fontSize: 4,
    },
    h3: {
      ...heading,
      fontSize: 3,
    },
    h4: {
      ...heading,
      fontSize: 2,
    },
    h5: {
      ...heading,
      fontSize: 1,
    },
    h6: {
      ...heading,
      fontSize: 0,
    },
    p: {
      color: 'text',
      fontFamily: 'body',
      fontWeight: 'body',
      lineHeight: 'body',
    },
    a: {
      color: 'primary',
    },
    pre: {
      fontFamily: 'monospace',
      overflowX: 'auto',
      code: {
        color: 'inherit',
      },
    },
    code: {
      fontFamily: 'monospace',
      fontSize: 'inherit',
    },
    table: {
      width: '100%',
      borderCollapse: 'separate',
      borderSpacing: 0,
    },
    th: {
      textAlign: 'left',
      borderBottomStyle: 'solid',
    },
    td: {
      textAlign: 'left',
      borderBottomStyle: 'solid',
    },
    img: {
      maxWidth: '100%',
    },
  },

  borders: [0, '1px solid', '2px solid', '4px solid', '8px solid', '16px solid', '32px solid'],
  radii: [0, 4, 8, 16, 24, 32, '100%'],
  widths: [16, 24, 32, 64, 128, 256],
  heights: [16, 24, 32, 64, 128, 256],
  sizes: [16, 24, 32, 64, 128, 256],
  maxWidths: [16, 32, 64, 128, 256, 512, 768, 1024, 1536],
  breakpoints: [32, 48, 64],

  shadows: [
    '0px 0px 0px 0px rgba(0, 0, 0, 0.2)',
    '0px 2px 1px -1px rgba(0, 0, 0, 0.2)',
    '0px 3px 1px -2px rgba(0, 0, 0, 0.2)',
    '0px 3px 3px -2px rgba(0, 0, 0, 0.2)',
    '0px 2px 4px -1px rgba(0, 0, 0, 0.2)',
    '0px 3px 5px -1px rgba(0, 0, 0, 0.2)',
    '0px 3px 5px -1px rgba(0, 0, 0, 0.2)',
    '0px 4px 5px -2px rgba(0, 0, 0, 0.2)',
    '0px 5px 5px -3px rgba(0, 0, 0, 0.2)',
    '0px 5px 6px -3px rgba(0, 0, 0, 0.2)',
    '0px 6px 6px -3px rgba(0, 0, 0, 0.2)',
    '0px 6px 7px -4px rgba(0, 0, 0, 0.2)',
    '0px 7px 8px -4px rgba(0, 0, 0, 0.2)',
    '0px 7px 8px -4px rgba(0, 0, 0, 0.2)',
    '0px 7px 9px -4px rgba(0, 0, 0, 0.2)',
    '0px 8px 9px -5px rgba(0, 0, 0, 0.2)',
    '0px 8px 10px -5px rgba(0, 0, 0, 0.2)',
    '0px 8px 11px -5px rgba(0, 0, 0, 0.2)',
    '0px 9px 11px -5px rgba(0, 0, 0, 0.2)',
    '0px 9px 12px -6px rgba(0, 0, 0, 0.2)',
    '0px 10px 13px -6px rgba(0, 0, 0, 0.2)',
    '0px 10px 13px -6px rgba(0, 0, 0, 0.2)',
    '0px 10px 14px -6px rgba(0, 0, 0, 0.2)',
    '0px 11px 14px -7px rgba(0, 0, 0, 0.2)',
    '0px 11px 15px -7px rgba(0, 0, 0, 0.2)',
  ],
}
