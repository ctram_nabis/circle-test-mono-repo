import React from 'react'

import { theme } from '../theme'
import { ThemeProvider } from './ThemeProvider'

export const withThemeProvider = <P extends object>(Component: React.ComponentType<P>) =>
  function WrappedComponent(props: P) {
    return (
      <ThemeProvider theme={theme}>
        <Component {...props} />
      </ThemeProvider>
    )
  }
