import { ThemeProvider as StyledThemeProvider, ThemeProviderComponent } from 'styled-components'
import { Theme } from '../theme/types'

export const ThemeProvider = StyledThemeProvider as ThemeProviderComponent<Theme>
