/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */
import React from 'react'
import styled from 'styled-components'
import { styleText, TextProps } from './styleText'
import { styleView, View, ViewProps } from './styleView'

export interface TouchableProps extends ViewProps {
  onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  onHover?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

const Button = styled.button<TouchableProps>(styleView)
const TextArea = styled.text<TextProps>(styleText)
const TextInput = styled.text<TextProps>(styleText)
const H1 = styled.h1<TextProps>(styleText)
const H2 = styled.h2<TextProps>(styleText)
const H3 = styled.h3<TextProps>(styleText)
const H4 = styled.h4<TextProps>(styleText)
const H5 = styled.h5<TextProps>(styleText)
const H6 = styled.h6<TextProps>(styleText)
const P = styled.p<TextProps>(styleText)
const Small = styled.small<TextProps>(styleText)

export const Primitives = {
  View,
  Button,
  Text,
  TextArea,
  TextInput,
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
  P,
  Small,
}
