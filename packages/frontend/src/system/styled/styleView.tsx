/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */
import css, { CSSObject } from '@styled-system/css'
import React, { ReactNode } from 'react'
import styled from 'styled-components'
import * as S from 'styled-system'
import * as ThemeUI from 'theme-ui'
import { Color, Theme } from '../theme'

export interface ViewProps
  extends S.PositionProps,
    S.ButtonStyleProps,
    S.TextStyleProps,
    S.ColorStyleProps,
    S.BackgroundProps<Color>,
    S.ShadowProps,
    S.BorderProps,
    S.LayoutProps,
    S.GridProps,
    S.FlexboxProps,
    S.SpaceProps,
    ThemeUI.SxProps {
  type?: string
  __css?: ViewProps & CSSObject
  __themeKey?: string
  className?: string
  theme?: Theme
  color?: Color
  // backgroundColor?: Color // -- dont know if this actually works
  borderColor?: Color
  bg?: Color
  css?: CSSObject
  center?: boolean
  middle?: boolean
  row?: boolean
  col?: boolean
  space?: 'between' | 'around' | 'evenly'
  inline?: boolean
  clickable?: boolean
  children?: ReactNode
  themeKey?: string
  cssStyle?: CSSObject
  variant?: string
}

export type OmitHeightWidth<T> = Omit<T, 'width' | 'height'>

export const styleView = S.compose(
  S.space,
  S.color,
  S.layout,
  S.flexbox,
  S.border,
  S.background,
  S.position,
  S.grid,
  S.shadow,
  S.buttonStyle,
  S.colorStyle,
  S.variant,
  (p: ViewProps) => p.sx && css(p.sx),
  (p: ViewProps) => p.css && css(p.css),
  (p: ViewProps) => p.row && { flexDirection: 'row' },
  (p: ViewProps) => p.col && { flexDirection: 'column' },
  (p: ViewProps) => p.center && { alignItems: 'center' },
  (p: ViewProps) => p.middle && { justifyContent: 'center' },
  (p: ViewProps) => p.space && { justifyContent: `space-${p.space}` },
  (p: ViewProps) => p.inline && { display: 'inline' },
  (p: ViewProps) => p.clickable && { cursor: 'pointer' },
)

export const View = styled.div<ViewProps>(
  {
    boxSizing: 'border-box',
    margin: 0,
    minWidth: 0,
  },
  styleView,
)

export const TextView = (props: ViewProps) => <View {...props} />
