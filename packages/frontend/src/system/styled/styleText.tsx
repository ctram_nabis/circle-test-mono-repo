/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */
import css, { CSSObject } from '@styled-system/css'
import * as SS from 'styled-system'
import { SxProps } from 'theme-ui'
import { ThemedProps } from '../theme'

export interface TextProps
  extends SS.SpaceProps,
    SS.VariantArgs,
    SS.FlexboxProps,
    SS.PositionProps,
    SS.TextColorProps,
    SS.TypographyProps,
    SS.TextStyleProps,
    SxProps,
    ThemedProps {
  __css?: CSSObject
  __themeKey?: string
  center?: boolean
  inline?: boolean
  underline?: boolean
  clickable?: boolean
}

export const styleText = SS.compose(
  SS.space,
  SS.typography,
  SS.flexbox,
  SS.position,
  SS.textStyle,
  SS.variant,
  SS.color,
  SS.textStyle,
  (p: TextProps) => p.__css && css(p.__css),
  (p: TextProps) => p.sx && css(p.sx),
  (p: TextProps) => p.center && { alignItems: 'center', textAlign: 'center' },
  (p: TextProps) => p.inline && { display: 'inline' },
  (p: TextProps) => p.underline && { textDecoration: 'underline' },
  (p: TextProps) => p.clickable && { cursor: 'pointer' },
)
