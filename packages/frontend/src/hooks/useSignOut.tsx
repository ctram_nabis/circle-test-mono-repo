/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */

// tslint:disable strict-type-predicates -- chris
import { useCallback } from 'react'
import { useApolloClient } from 'react-apollo'

import { useAuthToken } from 'modules/Auth'
import logger from 'modules/Logger'
import { useMe } from 'modules/User'
// import { useRouter } from 'modules/Navigation'

export function useSignOut() {
  // const router = useRouter()
  const client = useApolloClient()
  const { setToken } = useAuthToken()
  const { clearUser } = useMe()

  return useCallback(() => {
    logger.warn(`⚠️ useSignOut was called!`)

    setToken(null)
    clearUser()
    localStorage.clear()
    client.clearStore()
    document.cookie = `authToken=;expires=${new Date(0)}`

    logger.warn(`➡️ redirecting to sign-in`)
    // router.replace('/sign-in')
  }, [client, setToken, clearUser])
}
