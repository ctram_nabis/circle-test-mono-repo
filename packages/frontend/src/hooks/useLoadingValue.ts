import { useReducer } from 'react'

export interface LoadingValue<T> {
  error?: object
  loading: boolean
  reset: () => void
  setError: (error: object) => void
  setValue: (value: T | null) => void
  value?: T
}

interface ReducerState {
  error?: object
  loading: boolean
  value?: any
}

interface ErrorAction {
  type: 'error'
  error: object
}
interface ResetAction {
  type: 'reset'
  defaultValue?: any
}
interface ValueAction {
  type: 'value'
  value: any
}
type ReducerAction = ErrorAction | ResetAction | ValueAction

const defaultState = (defaultValue?: any) => {
  return {
    loading: defaultValue === undefined || defaultValue === null,
    value: defaultValue,
  }
}

const reducer = (state: ReducerState, action: ReducerAction): ReducerState => {
  switch (action.type) {
    case 'error':
      return {
        ...state,
        error: action.error,
        loading: false,
      }
    case 'reset':
      return defaultState(action.defaultValue)
    case 'value':
      return {
        ...state,
        loading: false,
        value: action.value,
      }
    default:
      return state
  }
}

export const useLoadingValue = <T>(getDefaultValue?: () => T | null) => {
  const defaultValue = getDefaultValue ? getDefaultValue() : undefined
  const [state, dispatch] = useReducer(reducer, defaultState(defaultValue))

  const reset = () => {
    const _defaultValue = getDefaultValue ? getDefaultValue() : undefined
    dispatch({ type: 'reset', defaultValue: _defaultValue })
  }

  const setError = (error: Error) => {
    dispatch({ type: 'error', error })
  }

  const setValue = (value: T | null) => {
    dispatch({ type: 'value', value })
  }

  return {
    error: state.error,
    setError,
    loading: state.loading,
    reset,
    setValue,
    value: state.value,
  }
}
