import { OrderByInput, PageInfoInput } from 'generated/graphql'
import { useSetState } from 'react-use'

export const DEFAULT_PAGE_INFO: PageInfoInput = {
  numItemsPerPage: 50,
  orderBy: [
    {
      attribute: 'createdAt',
      order: 'DESC',
    },
  ],
  page: 1,
}

export const usePageInfo = (defaults = DEFAULT_PAGE_INFO) => {
  const [pageInfo, setPageInfo] = useSetState(defaults)

  const setPage = (page: number) => setPageInfo({ page })
  const setOrderBy = (orderBy: OrderByInput[]) => setPageInfo({ orderBy })
  const setNumItemsPerPage = (numItemsPerPage: number) => setPageInfo({ numItemsPerPage })

  const getPageInfoInput = (): PageInfoInput => {
    const { numItemsPerPage, page } = pageInfo
    const orderBy = pageInfo.orderBy
    return { numItemsPerPage, orderBy, page }
  }

  return {
    pageInfo,
    setPage,
    setOrderBy,
    setPageInfo,
    setNumItemsPerPage,
    getPageInfoInput,
  }
}
