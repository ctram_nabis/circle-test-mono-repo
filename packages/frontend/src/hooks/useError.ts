import logger from 'modules/Logger'
import { Dispatch, SetStateAction, useEffect, useState } from 'react'
import { ErrorType } from 'types'

export function useError(fnName?: string): [ErrorType | undefined, Dispatch<SetStateAction<ErrorType | undefined>>] {
  const [error, setError] = useState<ErrorType>()

  useEffect(() => {
    if (error) {
      logger.error(`Error: ${fnName} -----`, error)
    }
  }, [error, fnName])

  return [error, setError]
}

export function useErrors(
  fnName?: string,
): [ErrorType[] | undefined, Dispatch<SetStateAction<ErrorType[] | undefined>>] {
  const [errors, setErrors] = useState<ErrorType[]>()

  useEffect(() => {
    if (errors) {
      logger.error(`Errors: ${fnName} -----`, errors)
    }
  }, [errors, fnName])

  return [errors, setErrors]
}
