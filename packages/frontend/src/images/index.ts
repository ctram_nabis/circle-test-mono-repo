import add_document from './add_document.png'
import big_menu from './big_menu.png'
import card from './card.png'
import collapse from './collapse.png'
import comment from './comment.png'
import contact from './contact.png'
import cursor_grab from './cursor-grab.png'
import cursor_hand from './cursor-hand.png'
import default_profile from './default_profile.png'
import edit from './edit.png'
import enterprise from './enterprise.png'
import error from './error.gif'
import expand from './expand.png'
import extract from './extract.png'
import eye from './eye.png'
import f01 from './f01.png'
import f02 from './f02.png'
import filter from './filter.png'
import hero from './hero.jpg'
import home from './home.png'
import knowledge from './knowledge.png'
import like_active from './like-active.png'
import like from './like.png'
import m01 from './m01.png'
import m02 from './m02.png'
import menu from './menu.png'
import migrate from './migrate.png'
import nabis_logo_icon from './nabis-logo.png'
import nabis_logo from './nabis_logo.svg'
import nabis_logo_old from './nabis_logo_old.svg'
import organize from './organize.png'
import people from './people.png'
import rank from './rank.png'
import reading from './reading.png'
import search from './search.png'
import settings from './settings.png'
import team from './team.png'
import trends from './trends.png'
import trophy from './trophy.png'
import work from './work.png'

export const Images = {
  add_document,
  big_menu,
  card,
  collapse,
  comment,
  contact,
  cursor_grab,
  cursor_hand,
  default_profile,
  edit,
  enterprise,
  error,
  expand,
  extract,
  eye,
  f01,
  f02,
  filter,
  hero,
  home,
  knowledge,
  like_active,
  like,
  m01,
  m02,
  menu,
  migrate,
  nabis_logo_old,
  nabis_logo,
  nabis_logo_icon,
  organize,
  people,
  rank,
  reading,
  search,
  settings,
  team,
  trends,
  trophy,
  work,
}

export type ImageName = keyof typeof Images
