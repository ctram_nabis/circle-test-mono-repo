/* eslint-disable */
import gql from 'graphql-tag'
import * as React from 'react'
import * as ApolloReactCommon from '@apollo/react-common'
import * as ApolloReactComponents from '@apollo/react-components'
import * as ApolloReactHoc from '@apollo/react-hoc'
import * as ApolloReactHooks from '@apollo/react-hooks'
export type Maybe<T> = T | null
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
  DateTime: any
  JSON: any
  Upload: any
}

export type AllAdminOrdersPayload = {
  results?: Maybe<Array<Order>>
  pageInfo?: Maybe<PageInfo>
}

export type AllDraftsPayload = {
  results?: Maybe<Array<Draft>>
  pageInfo?: Maybe<PageInfo>
}

export type AllLicensedLocationsPayload = {
  results?: Maybe<Array<LicensedLocation>>
  pageInfo?: Maybe<PageInfo>
}

export type AllLicensesPayload = {
  organizationLicenses: Array<License>
}

export type AllOrdersPayload = {
  results?: Maybe<Array<Order>>
  pageInfo?: Maybe<PageInfo>
}

export type Batch = {
  id: Scalars['ID']
  code: Scalars['String']
  testDate?: Maybe<Scalars['DateTime']>
  deletedAt?: Maybe<Scalars['DateTime']>
  status?: Maybe<BatchStatusEnum>
  expirationDate: Scalars['DateTime']
  manufacturingDate: Scalars['DateTime']
  manifestGDriveFileId?: Maybe<Scalars['String']>
  organization: Organization
  license?: Maybe<License>
  skus: Array<Sku>
  skuBatches: Array<SkuBatch>
  createdAt: Scalars['DateTime']
}

export type BatchInfo = {
  results: Array<Batch>
  pageInfo: PageInfo
}

export type BatchStatusEnum = 'QUARANTINE' | 'SAMPLED' | 'PASSED' | 'FAILED'

export type CorrectInventoryItemInput = {
  id: Scalars['ID']
  endCounted: Scalars['Float']
  transactionType: InventoryTransactionType
  notes: Scalars['String']
}

export type CreateBatchInput = {
  code: Scalars['String']
  manifestGDriveFileId?: Maybe<Scalars['String']>
  manufacturingDate?: Maybe<Scalars['DateTime']>
  expirationDate?: Maybe<Scalars['DateTime']>
  organizationId: Scalars['ID']
  licenseId?: Maybe<Scalars['ID']>
  status?: Maybe<BatchStatusEnum>
}

export type CreateDraftInput = {
  organizationId: Scalars['ID']
  body: Scalars['JSON']
}

export type CreateDraftPayload = {
  createdDraft: Draft
}

export type CreateDriverInput = {
  firstName: Scalars['String']
  lastName: Scalars['String']
  driversLicense: Scalars['String']
}

export type CreateDriverPayload = {
  changedDriver: Driver
}

export type CreateLicensedLocationInput = {
  name: Scalars['String']
  address1: Scalars['String']
  address2?: Maybe<Scalars['String']>
  city: Scalars['String']
  state: Scalars['String']
  zip: Scalars['String']
  lat?: Maybe<Scalars['Float']>
  lng?: Maybe<Scalars['Float']>
  siteCategory: SiteCategoryEnum
  licenses: Array<CreateLicenseInput>
}

export type CreateLicenseInput = {
  nickname: Scalars['String']
  category: LicenseCategoryEnum
  type: LicenseTypeEnum
  licenseNumber: Scalars['String']
  legalEntityName: Scalars['String']
  issuanceDate?: Maybe<Scalars['DateTime']>
  expirationDate?: Maybe<Scalars['DateTime']>
  contactName: Scalars['String']
  contactPhone: Scalars['String']
  contactEmail?: Maybe<Scalars['String']>
  address1: Scalars['String']
  address2?: Maybe<Scalars['String']>
  city: Scalars['String']
  state: Scalars['String']
  zip: Scalars['String']
  organizationId?: Maybe<Scalars['ID']>
  siteId?: Maybe<Scalars['ID']>
  licensedLocationId?: Maybe<Scalars['ID']>
}

export type CreateLicensePayload = {
  changedLicense: License
}

export type CreateLineItemInput = {
  code?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  unit?: Maybe<Scalars['String']>
  quantity: Scalars['Float']
  pricePerUnit: Scalars['Float']
  orderId?: Maybe<Scalars['ID']>
  skuBatchId: Scalars['ID']
  organizationId?: Maybe<Scalars['ID']>
  isExciseTaxable: Scalars['Boolean']
  isSample?: Maybe<Scalars['Boolean']>
  oversold?: Maybe<Scalars['Boolean']>
  discount: Scalars['Float']
}

export type CreateLineItemPayload = {
  changedLineItem: LineItem
}

export type CreateOrderInput = {
  name: Scalars['String']
  date: Scalars['DateTime']
  timeWindow?: Maybe<Scalars['String']>
  action: OrderActionEnum
  daysTillPaymentDue?: Maybe<Scalars['Int']>
  orgLicenseNum?: Maybe<Scalars['String']>
  siteLicenseNum?: Maybe<Scalars['String']>
  notes?: Maybe<Scalars['String']>
  status: OrderStatusEnum
  internalNotes?: Maybe<Scalars['String']>
  gmv: Scalars['Float']
  irn?: Maybe<Scalars['String']>
  discount?: Maybe<Scalars['Float']>
  exciseTax?: Maybe<Scalars['Float']>
  gmvCollected?: Maybe<Scalars['Float']>
  exciseTaxCollected?: Maybe<Scalars['Float']>
  paymentStatus: OrderPaymentStatusEnum
  adminNotes?: Maybe<Scalars['String']>
  lineItems: Array<CreateLineItemInput>
  driverId?: Maybe<Scalars['ID']>
  organizationId: Scalars['ID']
  vehicleId?: Maybe<Scalars['ID']>
  siteId: Scalars['ID']
  warehouseId: Scalars['ID']
  creatorId?: Maybe<Scalars['ID']>
  licensedLocationId: Scalars['ID']
  assigneeId?: Maybe<Scalars['ID']>
  surcharge?: Maybe<Scalars['Float']>
  hasUnalterablePaymentTerms?: Maybe<Scalars['Boolean']>
  mustPayPreviousBalance?: Maybe<Scalars['Boolean']>
}

export type CreateOrderPayload = {
  changedOrder: Order
}

export type CreateOrganizationInput = {
  name: Scalars['String']
  alias: Scalars['String']
  address1: Scalars['String']
  address2?: Maybe<Scalars['String']>
  city: Scalars['String']
  state: Scalars['String']
  zip: Scalars['String']
  phone?: Maybe<Scalars['String']>
  doingBusinessAs: Scalars['String']
  licenses: Array<CreateLicenseInput>
}

export type CreateOrganizationInviteInput = {
  email: Scalars['String']
  organizationId: Scalars['ID']
  senderId: Scalars['ID']
}

export type CreateOrganizationInvitePayload = {
  changedOrganizationInvite: OrganizationInvite
}

export type CreateOrganizationPayload = {
  changedOrganization: Organization
}

export type CreateOrganizationUserInput = {
  userId: Scalars['ID']
  organizationId: Scalars['ID']
}

export type CreateOrganizationUserPayload = {
  changedOrganizationUser: OrganizationUser
}

export type CreateRoleInput = {
  name: Scalars['String']
  permissions: Array<Scalars['String']>
  organizationId: Scalars['ID']
}

export type CreateRolePayload = {
  changedRole: Role
}

export type CreateSiteInput = {
  name: Scalars['String']
  pocName?: Maybe<Scalars['String']>
  pocPhoneNumber?: Maybe<Scalars['String']>
  pocEmail?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  lat?: Maybe<Scalars['Float']>
  lng?: Maybe<Scalars['Float']>
  siteCategory: SiteCategoryEnum
  licenses?: Maybe<Array<Maybe<CreateLicenseInput>>>
  organizationId: Scalars['ID']
  licensedLocationId: Scalars['ID']
}

export type CreateSitePayload = {
  changedSite: Site
}

export type CreateSkuInput = {
  name: Scalars['String']
  code: Scalars['String']
  isExciseTaxable?: Maybe<Scalars['Boolean']>
  inventoryType: InventoryTypeEnum
  inventoryClass: InventoryClassEnum
  inventoryCategory: InventoryCategoryEnum
  organizationId: Scalars['ID']
  isSample?: Maybe<Scalars['Boolean']>
  pricePerUnit?: Maybe<Scalars['Float']>
  unit: Scalars['String']
  isArchived?: Maybe<Scalars['Boolean']>
}

export type CreateSkuPayload = {
  changedSku: Sku
}

export type CreateUserInput = {
  email: Scalars['String']
  password: Scalars['String']
  firstName?: Maybe<Scalars['String']>
  lastName?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  phone?: Maybe<Scalars['String']>
  profilePicture?: Maybe<Scalars['String']>
  invitationId?: Maybe<Scalars['String']>
}

export type CreateUserPayload = {
  token: Scalars['String']
  changedUser: User
}

export type CreateUserRoleInput = {
  userId: Scalars['ID']
  roleId: Scalars['ID']
}

export type CreateVehicleInput = {
  name?: Maybe<Scalars['String']>
  make?: Maybe<Scalars['String']>
  model?: Maybe<Scalars['String']>
  licensePlate?: Maybe<Scalars['String']>
  year?: Maybe<Scalars['Int']>
}

export type CreateVehiclePayload = {
  changedVehicle: Vehicle
}

export type DashboardData = {
  totalDelivered: Scalars['Int']
  totalScheduled: Scalars['Int']
  totalUnscheduled: Scalars['Int']
  totalGMVCollected: Scalars['Float']
  totalExciseTaxCollected: Scalars['Float']
}

export type DeleteDraftInput = {
  id: Scalars['ID']
}

export type DeleteDraftPayload = {
  deletedDraft: Draft
}

export type DeleteDriverInput = {
  id: Scalars['ID']
}

export type DeleteDriverPayload = {
  changedDriver: Driver
}

export type DeleteLicensedLocationInput = {
  id: Scalars['ID']
}

export type DeleteLicenseInput = {
  id: Scalars['ID']
}

export type DeleteLicensePayload = {
  changedLicense: License
}

export type DeleteLineItemInput = {
  id: Scalars['ID']
}

export type DeleteLineItemPayload = {
  changedLineItem: LineItem
}

export type DeleteOrderInput = {
  id: Scalars['ID']
}

export type DeleteOrderPayload = {
  changedOrder: Order
}

export type DeleteOrganizationInput = {
  id: Scalars['ID']
}

export type DeleteOrganizationInviteInput = {
  id: Scalars['ID']
}

export type DeleteOrganizationInvitePayload = {
  changedOrganizationInvite: OrganizationInvite
}

export type DeleteOrganizationPayload = {
  changedOrganization: Organization
}

export type DeleteOrganizationUserInput = {
  userId: Scalars['ID']
  organizationId: Scalars['ID']
}

export type DeleteOrganizationUserPayload = {
  changedOrganizationUser: OrganizationUser
}

export type DeleteRoleInput = {
  id: Scalars['ID']
}

export type DeleteRolePayload = {
  changedRole: Role
}

export type DeleteSiteInput = {
  id: Scalars['ID']
}

export type DeleteSitePayload = {
  changedSite: Site
}

export type DeleteSkuInput = {
  id: Scalars['ID']
}

export type DeleteSkuPayload = {
  changedSku: Sku
}

export type DeleteUserInput = {
  id: Scalars['ID']
}

export type DeleteUserPayload = {
  changedUser: User
}

export type DeleteUserRoleInput = {
  userId: Scalars['ID']
  roleId: Scalars['ID']
}

export type DeleteVehicleInput = {
  id: Scalars['ID']
}

export type DeleteVehiclePayload = {
  changedVehicle: Vehicle
}

export type Draft = {
  id: Scalars['ID']
  creator: User
  body?: Maybe<Scalars['JSON']>
  organization: Organization
  createdAt: Scalars['DateTime']
  updatedAt: Scalars['DateTime']
}

export type Driver = {
  id: Scalars['ID']
  firstName: Scalars['String']
  lastName?: Maybe<Scalars['String']>
  driversLicense: Scalars['String']
}

export type GenerateManifestPayload = {
  apSummaryGDriveFileId?: Maybe<Scalars['String']>
  invoicesS3FileLink?: Maybe<Scalars['String']>
}

export type InventoryCategoryEnum =
  | 'BUD'
  | 'SHAKE'
  | 'INDOOR'
  | 'TRIM'
  | 'GREENHOUSE'
  | 'SUN_GROWN'
  | 'COOKIE'
  | 'BROWNIE'
  | 'GUMMY'
  | 'MARSHMALLOW'
  | 'TAFFY'
  | 'MINT'
  | 'HARD_CANDY'
  | 'BUTTER'
  | 'HONEY'
  | 'SODA'
  | 'JUICE'
  | 'TEA'
  | 'WATER'
  | 'CHIPS'
  | 'ICE_CREAM'
  | 'TINCTURES'
  | 'SUBLINGUAL'
  | 'PRE_ROLLS'
  | 'LEAFLETTES'
  | 'INFUSED_PRE_ROLLS'
  | 'BUTANE_HASH_OIL'
  | 'CO2_OIL'
  | 'CONCENTRATE'
  | 'SOLVENTLESS'
  | 'PAX'
  | 'THREAD_510'
  | 'OTHER_BATTERY_TYPES'
  | 'PEN'
  | 'TRANSDERMAL'
  | 'PATCH'
  | 'LIP_BALM'
  | 'CREAM'
  | 'SPRAY'
  | 'INHALER'
  | 'DISSOLVABLE_POWDER'
  | 'MIXED'
  | 'OTHER'

export type InventoryClassEnum =
  | 'FLOWER'
  | 'EDIBLES'
  | 'PRE_ROLL'
  | 'CONCENTRATE'
  | 'CARTRIDGE'
  | 'DISPOSABLE'
  | 'TOPICAL'
  | 'OTHER'

export type InventoryItem = {
  id: Scalars['ID']
  counted: Scalars['Float']
  allocated: Scalars['Float']
  promised: Scalars['Float']
  storageType: InventoryStorageType
  createdAt: Scalars['DateTime']
  updatedAt: Scalars['DateTime']
  skuBatch: SkuBatch
  warehouse: Warehouse
  organization: Organization
  inventoryLedgerItems: Array<InventoryLedgerItem>
}

export type InventoryItemInfo = {
  results: Array<InventoryLedgerItem>
  pageInfo: PageInfo
}

export type InventoryLedgerItem = {
  id: Scalars['ID']
  transactionType: InventoryTransactionType
  startCounted: Scalars['Float']
  endCounted: Scalars['Float']
  notes?: Maybe<Scalars['String']>
  createdAt?: Maybe<Scalars['DateTime']>
  updatedAt?: Maybe<Scalars['DateTime']>
  inventoryItem: InventoryItem
  order?: Maybe<Order>
  lineItem?: Maybe<LineItem>
  creator: User
  organization: Organization
}

export type InventoryRow = {
  skuBatch: SkuBatch
  totalQuantity: Scalars['Float']
  lastUpdatedAt: Scalars['DateTime']
}

export type InventoryStorageType = 'INVENTORY' | 'QUARANTINE'

export type InventoryTransactionType = 'INTAKE' | 'OUTTAKE' | 'CORRECTION' | 'ALLOCATION'

export type InventoryTypeEnum = 'CANNABIS' | 'NON_CANNABIS'

export type InventoryValues = {
  LA: Values
  OAK: Values
}

export type License = {
  id: Scalars['ID']
  nickname?: Maybe<Scalars['String']>
  category: LicenseCategoryEnum
  type: LicenseTypeEnum
  licenseNumber: Scalars['String']
  legalEntityName: Scalars['String']
  issuanceDate?: Maybe<Scalars['DateTime']>
  expirationDate?: Maybe<Scalars['DateTime']>
  contactName: Scalars['String']
  contactPhone: Scalars['String']
  contactEmail?: Maybe<Scalars['String']>
  address1: Scalars['String']
  address2?: Maybe<Scalars['String']>
  city: Scalars['String']
  state: Scalars['String']
  zip: Scalars['String']
  organization: Organization
  site: Site
}

export type LicenseCategoryEnum = 'ADULT_USE' | 'MEDICINAL' | 'COMBINED'

export type LicensedLocation = {
  id: Scalars['ID']
  name?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  lat?: Maybe<Scalars['Float']>
  lng?: Maybe<Scalars['Float']>
  siteCategory?: Maybe<SiteCategoryEnum>
  licenses?: Maybe<Array<Maybe<License>>>
  warehouse?: Maybe<Warehouse>
  verificationStatus?: Maybe<VerificationStatusEnum>
  submittedBy?: Maybe<User>
  isArchived?: Maybe<Scalars['Boolean']>
}

export type LicenseTypeEnum =
  | 'RETAILER'
  | 'RETAILER_NONSTOREFRONT'
  | 'DISTRIBUTOR'
  | 'DISTRIBUTOR_TRANSPORT_ONLY'
  | 'MICROBUSINESS'
  | 'TESTING'
  | 'CANNABIS_EVENTS'
  | 'MANUFACTURER'
  | 'CULTIVATOR'

export type LineItem = {
  id: Scalars['ID']
  code: Scalars['String']
  name: Scalars['String']
  unit: Scalars['String']
  quantity: Scalars['Float']
  pricePerUnit?: Maybe<Scalars['Float']>
  order: Order
  skuBatch?: Maybe<SkuBatch>
  organization?: Maybe<Organization>
  isExciseTaxable: Scalars['Boolean']
  isSample: Scalars['Boolean']
  oversold: Scalars['Boolean']
  discount: Scalars['Float']
}

export type LineItemChange = {
  id: Scalars['ID']
  isExciseTaxable: Scalars['Boolean']
  isSample: Scalars['Boolean']
  lineItemId: Scalars['ID']
  name?: Maybe<Scalars['String']>
  orderChange: OrderChange
  oversold?: Maybe<Scalars['Boolean']>
  pricePerUnit?: Maybe<Scalars['Float']>
  quantity: Scalars['Float']
  skuBatch?: Maybe<SkuBatch>
  skuBatchId: Scalars['ID']
  type: LineItemChangeType
  discount: Scalars['Float']
}

export type LineItemChangeType = 'ADDED' | 'REMOVED'

export type LineItemIntakeOuttakeInput = {
  lineItems: Array<LineItemIntakeOuttakeRow>
  transactionType: Scalars['String']
  warehouseId: Scalars['ID']
  orderId: Scalars['ID']
}

export type LineItemIntakeOuttakeRow = {
  lineItemId: Scalars['ID']
  quantity: Scalars['Float']
  skuBatchId: Scalars['ID']
  organizationId: Scalars['ID']
}

export type LoginUserInput = {
  email: Scalars['String']
  password: Scalars['String']
}

export type LoginUserPayload = {
  token: Scalars['String']
  user: User
}

export type Mutation = {
  uploadBatchFile?: Maybe<Scalars['String']>
  createBatch?: Maybe<Batch>
  bulkUpdateBatchStatus?: Maybe<Scalars['Boolean']>
  deleteBatch?: Maybe<Scalars['Boolean']>
  editBatch?: Maybe<Scalars['Boolean']>
  restoreBatch?: Maybe<Batch>
  createDraft?: Maybe<CreateDraftPayload>
  updateDraft?: Maybe<UpdateDraftPayload>
  deleteDraft?: Maybe<DeleteDraftPayload>
  createDriver?: Maybe<CreateDriverPayload>
  updateDriver?: Maybe<UpdateDriverPayload>
  deleteDriver?: Maybe<DeleteDriverPayload>
  correctInventoryItem?: Maybe<UpdateInventoryItemPayload>
  lineItemIntakeOuttake?: Maybe<Array<Maybe<InventoryItem>>>
  createInventoryItem?: Maybe<InventoryItem>
  createLicensedLocation?: Maybe<Scalars['ID']>
  updateLicensedLocation?: Maybe<Scalars['Boolean']>
  deleteLicensedLocation?: Maybe<Scalars['Boolean']>
  unarchiveLicensedLocation?: Maybe<Scalars['Boolean']>
  createLicense?: Maybe<CreateLicensePayload>
  updateLicense?: Maybe<UpdateLicensePayload>
  deleteLicense?: Maybe<DeleteLicensePayload>
  createLineItem?: Maybe<CreateLineItemPayload>
  updateLineItem?: Maybe<UpdateLineItemPayload>
  deleteLineItem?: Maybe<DeleteLineItemPayload>
  generateManifest?: Maybe<GenerateManifestPayload>
  getOrderSubscriber?: Maybe<OrderSubscriber>
  createOrganizationInvite?: Maybe<CreateOrganizationInvitePayload>
  updateOrganizationInvite?: Maybe<UpdateOrganizationInvitePayload>
  deleteOrganizationInvite?: Maybe<DeleteOrganizationInvitePayload>
  createOrganization?: Maybe<CreateOrganizationPayload>
  updateOrganization?: Maybe<UpdateOrganizationPayload>
  deleteOrganization?: Maybe<DeleteOrganizationPayload>
  createOrganizationUser?: Maybe<CreateOrganizationUserPayload>
  updateOrganizationUser?: Maybe<UpdateOrganizationUserPayload>
  deleteOrganizationUser?: Maybe<DeleteOrganizationUserPayload>
  createOrder?: Maybe<CreateOrderPayload>
  updateOrder?: Maybe<UpdateOrderPayload>
  deleteOrder?: Maybe<DeleteOrderPayload>
  createTokenSendEmail?: Maybe<Scalars['String']>
  validateToken?: Maybe<LoginUserPayload>
  resetPassword?: Maybe<LoginUserPayload>
  createRole?: Maybe<CreateRolePayload>
  updateRole?: Maybe<UpdateRolePayload>
  deleteRole?: Maybe<DeleteRolePayload>
  sendSendGridStatusUpdateEmail?: Maybe<SendSendGridStatusUpdateEmailPayload>
  createEmptySkuBatch?: Maybe<SkuBatch>
  createSku?: Maybe<CreateSkuPayload>
  updateSku?: Maybe<UpdateSkuPayload>
  deleteSku?: Maybe<DeleteSkuPayload>
  restoreSku?: Maybe<Sku>
  createSkuAndSkuBatch?: Maybe<CreateSkuPayload>
  restoreSkuBatch?: Maybe<SkuBatch>
  deleteSkuBatch?: Maybe<SkuBatch>
  createSkuBatch?: Maybe<SkuBatch>
  updateSkuBatch?: Maybe<Scalars['Boolean']>
  updateSkuBatchTitleholder?: Maybe<SkuBatch>
  createSite?: Maybe<CreateSitePayload>
  updateSite?: Maybe<UpdateSitePayload>
  deleteSite?: Maybe<DeleteSitePayload>
  createUserRole?: Maybe<User>
  deleteUserRole?: Maybe<User>
  updateUserPreference?: Maybe<UserPreference>
  changeUserPassword?: Maybe<User>
  loginUser?: Maybe<LoginUserPayload>
  createUser?: Maybe<CreateUserPayload>
  updateUser?: Maybe<UpdateUserPayload>
  deleteUser?: Maybe<DeleteUserPayload>
  createVehicle?: Maybe<CreateVehiclePayload>
  updateVehicle?: Maybe<UpdateVehiclePayload>
  deleteVehicle?: Maybe<DeleteVehiclePayload>
  uploadToOnfleet?: Maybe<UploadToOnfleetPayload>
  createOrderChange?: Maybe<Scalars['Boolean']>
  assignInvoiceToOrder?: Maybe<Scalars['Boolean']>
}

export type MutationUploadBatchFileArgs = {
  inputFile: Scalars['Upload']
  organizationId: Scalars['ID']
}

export type MutationCreateBatchArgs = {
  input: CreateBatchInput
}

export type MutationBulkUpdateBatchStatusArgs = {
  batches: Array<Maybe<UpdateBatchInput>>
  targetStatus?: Maybe<BatchStatusEnum>
}

export type MutationDeleteBatchArgs = {
  id: Scalars['ID']
}

export type MutationEditBatchArgs = {
  input: UpdateBatchInput
}

export type MutationRestoreBatchArgs = {
  batchId: Scalars['ID']
}

export type MutationCreateDraftArgs = {
  input: CreateDraftInput
}

export type MutationUpdateDraftArgs = {
  input: UpdateDraftInput
}

export type MutationDeleteDraftArgs = {
  input: DeleteDraftInput
}

export type MutationCreateDriverArgs = {
  input: CreateDriverInput
}

export type MutationUpdateDriverArgs = {
  input: UpdateDriverInput
}

export type MutationDeleteDriverArgs = {
  input: DeleteDriverInput
}

export type MutationCorrectInventoryItemArgs = {
  input: CorrectInventoryItemInput
}

export type MutationLineItemIntakeOuttakeArgs = {
  input: LineItemIntakeOuttakeInput
}

export type MutationCreateInventoryItemArgs = {
  organizationId: Scalars['ID']
  skuBatchId: Scalars['ID']
  skuId?: Maybe<Scalars['ID']>
  warehouseId: Scalars['ID']
}

export type MutationCreateLicensedLocationArgs = {
  input: CreateLicensedLocationInput
}

export type MutationUpdateLicensedLocationArgs = {
  input: UpdateLicensedLocationInput
}

export type MutationDeleteLicensedLocationArgs = {
  input: DeleteLicensedLocationInput
}

export type MutationUnarchiveLicensedLocationArgs = {
  input: UnarchiveLicensedLocationInput
}

export type MutationCreateLicenseArgs = {
  input: CreateLicenseInput
}

export type MutationUpdateLicenseArgs = {
  input: UpdateLicenseInput
}

export type MutationDeleteLicenseArgs = {
  input: DeleteLicenseInput
}

export type MutationCreateLineItemArgs = {
  input: CreateLineItemInput
}

export type MutationUpdateLineItemArgs = {
  input: UpdateLineItemInput
}

export type MutationDeleteLineItemArgs = {
  input: DeleteLineItemInput
}

export type MutationGenerateManifestArgs = {
  orderId: Scalars['ID']
}

export type MutationGetOrderSubscriberArgs = {
  orderId: Scalars['ID']
  organizationId: Scalars['ID']
}

export type MutationCreateOrganizationInviteArgs = {
  input: CreateOrganizationInviteInput
}

export type MutationUpdateOrganizationInviteArgs = {
  input: UpdateOrganizationInviteInput
}

export type MutationDeleteOrganizationInviteArgs = {
  input: DeleteOrganizationInviteInput
}

export type MutationCreateOrganizationArgs = {
  input: CreateOrganizationInput
}

export type MutationUpdateOrganizationArgs = {
  input: UpdateOrganizationInput
}

export type MutationDeleteOrganizationArgs = {
  input: DeleteOrganizationInput
}

export type MutationCreateOrganizationUserArgs = {
  input: CreateOrganizationUserInput
}

export type MutationUpdateOrganizationUserArgs = {
  input: UpdateOrganizationUserInput
}

export type MutationDeleteOrganizationUserArgs = {
  input: DeleteOrganizationUserInput
}

export type MutationCreateOrderArgs = {
  input: CreateOrderInput
}

export type MutationUpdateOrderArgs = {
  input: UpdateOrderInput
}

export type MutationDeleteOrderArgs = {
  input: DeleteOrderInput
}

export type MutationCreateTokenSendEmailArgs = {
  email?: Maybe<Scalars['String']>
}

export type MutationValidateTokenArgs = {
  email?: Maybe<Scalars['String']>
  token?: Maybe<Scalars['String']>
}

export type MutationResetPasswordArgs = {
  input: ResetPasswordInput
}

export type MutationCreateRoleArgs = {
  input: CreateRoleInput
}

export type MutationUpdateRoleArgs = {
  input: UpdateRoleInput
}

export type MutationDeleteRoleArgs = {
  input: DeleteRoleInput
}

export type MutationSendSendGridStatusUpdateEmailArgs = {
  input: SendSendGridStatusUpdateInput
}

export type MutationCreateEmptySkuBatchArgs = {
  skuId: Scalars['ID']
}

export type MutationCreateSkuArgs = {
  input: CreateSkuInput
}

export type MutationUpdateSkuArgs = {
  input: UpdateSkuInput
}

export type MutationDeleteSkuArgs = {
  input: DeleteSkuInput
}

export type MutationRestoreSkuArgs = {
  skuId: Scalars['ID']
}

export type MutationCreateSkuAndSkuBatchArgs = {
  input: CreateSkuInput
}

export type MutationRestoreSkuBatchArgs = {
  id: Scalars['ID']
}

export type MutationDeleteSkuBatchArgs = {
  id: Scalars['ID']
}

export type MutationCreateSkuBatchArgs = {
  batchId?: Maybe<Scalars['ID']>
  skuId: Scalars['ID']
  titleholderId: Scalars['ID']
}

export type MutationUpdateSkuBatchArgs = {
  skuBatchId: Scalars['ID']
  batchId?: Maybe<Scalars['ID']>
  skuId: Scalars['ID']
}

export type MutationUpdateSkuBatchTitleholderArgs = {
  skuBatchId: Scalars['ID']
  titleholderId: Scalars['ID']
}

export type MutationCreateSiteArgs = {
  input: CreateSiteInput
}

export type MutationUpdateSiteArgs = {
  input: UpdateSiteInput
}

export type MutationDeleteSiteArgs = {
  input: DeleteSiteInput
}

export type MutationCreateUserRoleArgs = {
  input: CreateUserRoleInput
}

export type MutationDeleteUserRoleArgs = {
  input: DeleteUserRoleInput
}

export type MutationUpdateUserPreferenceArgs = {
  userId: Scalars['ID']
  organizationId: Scalars['ID']
  receiveOrderUpdateEmails?: Maybe<Scalars['Boolean']>
}

export type MutationChangeUserPasswordArgs = {
  password: Scalars['String']
  newPassword: Scalars['String']
  confirmPassword: Scalars['String']
}

export type MutationLoginUserArgs = {
  input: LoginUserInput
}

export type MutationCreateUserArgs = {
  input: CreateUserInput
}

export type MutationUpdateUserArgs = {
  input: UpdateUserInput
}

export type MutationDeleteUserArgs = {
  input: DeleteUserInput
}

export type MutationCreateVehicleArgs = {
  input: CreateVehicleInput
}

export type MutationUpdateVehicleArgs = {
  input: UpdateVehicleInput
}

export type MutationDeleteVehicleArgs = {
  input: DeleteVehicleInput
}

export type MutationUploadToOnfleetArgs = {
  input: UploadToOnfleetInput
}

export type MutationCreateOrderChangeArgs = {
  submitterId: Scalars['ID']
  description: Scalars['String']
  orderId: Scalars['ID']
}

export type MutationAssignInvoiceToOrderArgs = {
  invoiceId: Scalars['ID']
  orderId: Scalars['ID']
}

export type Order = {
  id: Scalars['ID']
  assignee?: Maybe<User>
  action: OrderActionEnum
  adminNotes?: Maybe<Scalars['String']>
  createdAt: Scalars['DateTime']
  creator?: Maybe<User>
  collectWithOrder?: Maybe<Order>
  date: Scalars['DateTime']
  daysTillPaymentDue?: Maybe<Scalars['Int']>
  discount: Scalars['Float']
  driver?: Maybe<Driver>
  edited?: Maybe<Scalars['Boolean']>
  exciseTax: Scalars['Float']
  exciseTaxCollected?: Maybe<Scalars['Float']>
  estimatedArrivalTimeBefore?: Maybe<Scalars['DateTime']>
  estimatedArrivalTimeAfter?: Maybe<Scalars['DateTime']>
  gmv: Scalars['Float']
  gmvCollected: Scalars['Float']
  intaken: Scalars['Boolean']
  internalNotes?: Maybe<Scalars['String']>
  inventoryLedgerItems: Array<InventoryLedgerItem>
  irn?: Maybe<Scalars['String']>
  isArchived: Scalars['Boolean']
  licensedLocation: LicensedLocation
  lineItems: Array<LineItem>
  manifestGDriveFileId?: Maybe<Scalars['String']>
  apSummaryGDriveFileId?: Maybe<Scalars['String']>
  invoicesS3FileLink?: Maybe<Scalars['String']>
  name: Scalars['String']
  notes?: Maybe<Scalars['String']>
  number: Scalars['Int']
  orderSubscribers: Array<OrderSubscriber>
  organization: Organization
  orgLicenseNum?: Maybe<Scalars['String']>
  outtaken: Scalars['Boolean']
  paymentStatus: OrderPaymentStatusEnum
  site: Site
  siteLicenseNum?: Maybe<Scalars['String']>
  status: OrderStatusEnum
  timeWindow?: Maybe<Scalars['String']>
  vehicle?: Maybe<Vehicle>
  warehouse?: Maybe<Warehouse>
  warehouseId: Scalars['ID']
  surcharge: Scalars['Float']
  hasUnalterablePaymentTerms?: Maybe<Scalars['Boolean']>
  mustPayPreviousBalance?: Maybe<Scalars['Boolean']>
}

export type OrderActionEnum =
  | 'DELIVERY_TO_RETAILER'
  | 'RETURN_FROM_RETAILER'
  | 'PICKUP_FROM_BRAND'
  | 'DELIVERY_TO_BRAND'
  | 'DROPOFF_TO_NABIS'
  | 'PICKUP_FROM_NABIS'
  | 'PAYMENT_PICKUP_FROM_RETAILER'
  | 'INTERNAL_TRANSFER'
  | 'OTHER'

export type OrderBy = {
  attribute?: Maybe<Scalars['String']>
  order?: Maybe<OrderByEnum>
}

export type OrderByEnum = 'ASC' | 'DESC'

export type OrderByInput = {
  attribute: Scalars['String']
  order: OrderByEnum
}

export type OrderChange = {
  id?: Maybe<Scalars['ID']>
  order: Order
  createdAt: Scalars['DateTime']
  submitter: User
  organization: Organization
  description?: Maybe<Scalars['String']>
  addedLineItems?: Maybe<Array<LineItemChange>>
  removedLineItems?: Maybe<Array<LineItemChange>>
}

export type OrderPaymentStatusEnum = 'UNPAID' | 'COD_PAID' | 'NET_TERMS_PAID' | 'REMITTED' | 'SELF_COLLECTED'

export type OrderStatusEnum =
  | 'DRAFT'
  | 'UNSCHEDULED'
  | 'SCHEDULED'
  | 'TRANSFERRING'
  | 'DELIVERED'
  | 'DELIVERED_WITH_EDITS'
  | 'DELAYED'
  | 'CANCELLED'
  | 'REJECTED'

export type OrderSubscriber = {
  id: Scalars['ID']
  orderId: Scalars['ID']
  subscriberId: Scalars['ID']
  manifestGDriveFileId?: Maybe<Scalars['String']>
  invoicesS3FileLink?: Maybe<Scalars['String']>
  order: Order
}

export type Organization = {
  id: Scalars['ID']
  name: Scalars['String']
  alias: Scalars['String']
  address1: Scalars['String']
  address2?: Maybe<Scalars['String']>
  city: Scalars['String']
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  phone?: Maybe<Scalars['String']>
  doingBusinessAs?: Maybe<Scalars['String']>
  manifestGDriveFolderId?: Maybe<Scalars['String']>
  owner: User
  orders: Array<Order>
  members: Array<User>
  licenses: Array<License>
  sites: Array<Site>
  inventoryItems: Array<InventoryItem>
  inventoryLedgerItems: Array<InventoryLedgerItem>
}

export type OrganizationInvite = {
  id: Scalars['ID']
  email: Scalars['String']
  organization: Organization
  sender: User
  accepted?: Maybe<Scalars['Boolean']>
  respondedAt?: Maybe<Scalars['DateTime']>
}

export type OrganizationUser = {
  id: Scalars['ID']
  user: User
  organization: Organization
}

export type PageInfo = {
  page?: Maybe<Scalars['Int']>
  numItemsPerPage?: Maybe<Scalars['Int']>
  orderBy: Array<OrderBy>
  totalNumItems?: Maybe<Scalars['Int']>
  totalNumPages?: Maybe<Scalars['Int']>
}

export type PageInfoInput = {
  page: Scalars['Int']
  numItemsPerPage: Scalars['Int']
  orderBy?: Maybe<Array<OrderByInput>>
}

export type Query = {
  environment?: Maybe<Scalars['String']>
  viewer?: Maybe<Viewer>
  getBatch?: Maybe<Batch>
  getDraft?: Maybe<Draft>
  getUnavailableDates: Array<Scalars['String']>
  getApiKey?: Maybe<Scalars['String']>
  getUser?: Maybe<User>
  getOrganization?: Maybe<Organization>
  getOrganizationUser?: Maybe<OrganizationUser>
  getRole?: Maybe<Role>
  getUserRole?: Maybe<UserRole>
  getUserPreferences?: Maybe<Array<UserPreference>>
  getOrder?: Maybe<Order>
  getOrders: Array<Order>
  getSite?: Maybe<Site>
  getLicense?: Maybe<License>
  getVehicle?: Maybe<Vehicle>
  getLineItem?: Maybe<LineItem>
  getSku?: Maybe<Sku>
  getOrganizationInvite?: Maybe<OrganizationInvite>
  getInventoryItems: Array<InventoryItem>
  getInventoryItem?: Maybe<InventoryItem>
  getInventoryLedgerItem?: Maybe<InventoryLedgerItem>
  getInventoryQuantity?: Maybe<Scalars['Float']>
  getOrderChangelog: Array<OrderChange>
  getSkuBatch?: Maybe<SkuBatch>
  getActiveOrdersForSkuBatch: Array<Order>
}

export type QueryGetBatchArgs = {
  id: Scalars['ID']
}

export type QueryGetDraftArgs = {
  id: Scalars['ID']
}

export type QueryGetApiKeyArgs = {
  clientUuid: Scalars['String']
}

export type QueryGetUserArgs = {
  id: Scalars['ID']
}

export type QueryGetOrganizationArgs = {
  id: Scalars['ID']
}

export type QueryGetOrganizationUserArgs = {
  id: Scalars['ID']
}

export type QueryGetRoleArgs = {
  id: Scalars['ID']
}

export type QueryGetUserRoleArgs = {
  id: Scalars['ID']
}

export type QueryGetUserPreferencesArgs = {
  organizationId?: Maybe<Scalars['ID']>
  userId: Scalars['ID']
}

export type QueryGetOrderArgs = {
  id: Scalars['ID']
}

export type QueryGetOrdersArgs = {
  ids?: Maybe<Array<Maybe<Scalars['ID']>>>
}

export type QueryGetSiteArgs = {
  id: Scalars['ID']
}

export type QueryGetLicenseArgs = {
  id: Scalars['ID']
}

export type QueryGetVehicleArgs = {
  id: Scalars['ID']
}

export type QueryGetLineItemArgs = {
  id: Scalars['ID']
}

export type QueryGetSkuArgs = {
  id: Scalars['ID']
}

export type QueryGetOrganizationInviteArgs = {
  id: Scalars['ID']
}

export type QueryGetInventoryItemsArgs = {
  skuBatchId?: Maybe<Scalars['ID']>
}

export type QueryGetInventoryItemArgs = {
  id?: Maybe<Scalars['ID']>
  warehouseId?: Maybe<Scalars['ID']>
  skuBatchId?: Maybe<Scalars['ID']>
  organizationId?: Maybe<Scalars['ID']>
  storageType?: Maybe<InventoryStorageType>
}

export type QueryGetInventoryLedgerItemArgs = {
  id: Scalars['ID']
}

export type QueryGetInventoryQuantityArgs = {
  warehouseId: Scalars['ID']
  skuId: Scalars['ID']
  organizationId: Scalars['ID']
  storageType: InventoryStorageType
}

export type QueryGetOrderChangelogArgs = {
  id: Scalars['ID']
}

export type QueryGetSkuBatchArgs = {
  id: Scalars['ID']
}

export type QueryGetActiveOrdersForSkuBatchArgs = {
  skuBatchId: Scalars['ID']
}

export type ResetPasswordInput = {
  email: Scalars['String']
  token: Scalars['String']
  confirmPassword: Scalars['String']
  newPassword: Scalars['String']
}

export type Role = {
  id: Scalars['ID']
  name: Scalars['String']
  description?: Maybe<Scalars['String']>
  permissions: Array<Scalars['String']>
  organization?: Maybe<Organization>
}

export type Sales = {
  gmv: Scalars['Float']
  exciseTax: Scalars['Float']
  day: Scalars['DateTime']
  month: Scalars['DateTime']
  year: Scalars['DateTime']
  organization: Organization
}

export type SendSendGridStatusUpdateEmailPayload = {
  success: Scalars['Boolean']
}

export type SendSendGridStatusUpdateInput = {
  orderId: Scalars['ID']
}

export type Site = {
  id: Scalars['ID']
  name: Scalars['String']
  pocName?: Maybe<Scalars['String']>
  pocPhoneNumber?: Maybe<Scalars['String']>
  pocEmail?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  lat?: Maybe<Scalars['Float']>
  lng?: Maybe<Scalars['Float']>
  siteCategory: SiteCategoryEnum
  organization: Organization
  licenses: Array<License>
  inventoryItems: Array<InventoryItem>
  licensedLocation?: Maybe<LicensedLocation>
  licensedLocationId: Scalars['ID']
  createdAt: Scalars['DateTime']
  updatedAt: Scalars['DateTime']
}

export type SiteCategoryEnum =
  | 'RETAILER'
  | 'MANUFACTURER'
  | 'CULTIVATOR'
  | 'CONSUMER'
  | 'DISTRIBUTOR'
  | 'MICROBUSINESS'
  | 'TESTING'
  | 'OTHER'

export type Sku = {
  id: Scalars['ID']
  code: Scalars['String']
  name: Scalars['String']
  unit: Scalars['String']
  pricePerUnit?: Maybe<Scalars['Float']>
  isArchived?: Maybe<Scalars['Boolean']>
  isExciseTaxable?: Maybe<Scalars['Boolean']>
  organization: Organization
  inventoryType: InventoryTypeEnum
  inventoryClass: InventoryClassEnum
  inventoryCategory: InventoryCategoryEnum
  inventoryItems?: Maybe<Array<Maybe<InventoryItem>>>
  totalCount: Scalars['Float']
  totalCountLA: Scalars['Float']
  totalCountOak: Scalars['Float']
  lastUpdate?: Maybe<Scalars['DateTime']>
  batches?: Maybe<Array<Maybe<Batch>>>
  skuBatches?: Maybe<Array<Maybe<SkuBatch>>>
}

export type SkuBatch = {
  id: Scalars['ID']
  sku: Sku
  batch?: Maybe<Batch>
  createdAt: Scalars['DateTime']
  updatedAt: Scalars['DateTime']
  deletedAt?: Maybe<Scalars['DateTime']>
  inventoryValues: InventoryValues
  inventoryItems?: Maybe<Array<Maybe<InventoryItem>>>
  titleholder: Titleholder
}

export type SkuBatchInfo = {
  results: Array<SkuBatch>
  pageInfo: PageInfo
}

export type SkuInfo = {
  results: Array<Sku>
  pageInfo: PageInfo
}

export type Titleholder = {
  id: Scalars['ID']
  organization?: Maybe<Organization>
  titleholderLicense?: Maybe<License>
  titleholderOrganization?: Maybe<Organization>
  type: TitleholderType
  createdAt: Scalars['DateTime']
  updatedAt: Scalars['DateTime']
}

export type TitleholderType = 'LICENSE' | 'ORGANIZATION'

export type UnarchiveLicensedLocationInput = {
  id: Scalars['ID']
}

export type UpdateBatchInput = {
  id: Scalars['ID']
  code?: Maybe<Scalars['String']>
  manifestGDriveFileId?: Maybe<Scalars['String']>
  manufacturingDate?: Maybe<Scalars['DateTime']>
  expirationDate?: Maybe<Scalars['DateTime']>
  licenseId?: Maybe<Scalars['ID']>
  skuBatchId?: Maybe<Scalars['ID']>
  status?: Maybe<BatchStatusEnum>
}

export type UpdateDraftInput = {
  id: Scalars['ID']
  body: Scalars['JSON']
  organizationId: Scalars['ID']
}

export type UpdateDraftPayload = {
  changedDraft: Draft
}

export type UpdateDriverInput = {
  id: Scalars['ID']
  firstName: Scalars['String']
  lastName: Scalars['String']
  driversLicense: Scalars['String']
}

export type UpdateDriverPayload = {
  changedDriver: Driver
}

export type UpdateInventoryItemPayload = {
  changedInventoryItem: InventoryItem
}

export type UpdateLicensedLocationInput = {
  id: Scalars['ID']
  name?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  lat?: Maybe<Scalars['Float']>
  lng?: Maybe<Scalars['Float']>
  siteCategory?: Maybe<SiteCategoryEnum>
  licenses?: Maybe<Array<Maybe<CreateLicenseInput>>>
}

export type UpdateLicenseInput = {
  id: Scalars['ID']
  nickname?: Maybe<Scalars['String']>
  category?: Maybe<LicenseCategoryEnum>
  type?: Maybe<LicenseTypeEnum>
  licenseNumber?: Maybe<Scalars['String']>
  legalEntityName?: Maybe<Scalars['String']>
  issuanceDate?: Maybe<Scalars['DateTime']>
  expirationDate?: Maybe<Scalars['DateTime']>
  contactName?: Maybe<Scalars['String']>
  contactPhone?: Maybe<Scalars['String']>
  contactEmail?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  organizationId?: Maybe<Scalars['ID']>
  siteId?: Maybe<Scalars['ID']>
  licensedLocationId?: Maybe<Scalars['ID']>
}

export type UpdateLicensePayload = {
  changedLicense: License
}

export type UpdateLineItemInput = {
  id: Scalars['ID']
  code?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  unit?: Maybe<Scalars['String']>
  quantity?: Maybe<Scalars['Float']>
  pricePerUnit?: Maybe<Scalars['Float']>
  orderId?: Maybe<Scalars['ID']>
  skuBatchId?: Maybe<Scalars['ID']>
  organizationId?: Maybe<Scalars['ID']>
  isExciseTaxable?: Maybe<Scalars['Boolean']>
  isSample?: Maybe<Scalars['Boolean']>
  oversold?: Maybe<Scalars['Boolean']>
  discount?: Maybe<Scalars['Float']>
}

export type UpdateLineItemPayload = {
  changedLineItem: LineItem
}

export type UpdateOrderInput = {
  id: Scalars['ID']
  name?: Maybe<Scalars['String']>
  date?: Maybe<Scalars['DateTime']>
  estimatedArrivalTimeBefore?: Maybe<Scalars['DateTime']>
  estimatedArrivalTimeAfter?: Maybe<Scalars['DateTime']>
  timeWindow?: Maybe<Scalars['String']>
  action?: Maybe<OrderActionEnum>
  daysTillPaymentDue?: Maybe<Scalars['Int']>
  orgLicenseNum?: Maybe<Scalars['String']>
  siteLicenseNum?: Maybe<Scalars['String']>
  notes?: Maybe<Scalars['String']>
  status?: Maybe<OrderStatusEnum>
  gmv?: Maybe<Scalars['Float']>
  irn?: Maybe<Scalars['String']>
  internalNotes?: Maybe<Scalars['String']>
  discount?: Maybe<Scalars['Float']>
  exciseTax?: Maybe<Scalars['Float']>
  gmvCollected?: Maybe<Scalars['Float']>
  exciseTaxCollected?: Maybe<Scalars['Float']>
  paymentStatus?: Maybe<OrderPaymentStatusEnum>
  adminNotes?: Maybe<Scalars['String']>
  driverId?: Maybe<Scalars['ID']>
  organizationId?: Maybe<Scalars['ID']>
  vehicleId?: Maybe<Scalars['ID']>
  site?: Maybe<UpdateSiteInput>
  warehouseId?: Maybe<Scalars['ID']>
  lineItems?: Maybe<Array<Maybe<UpsertLineItemInput>>>
  isArchived?: Maybe<Scalars['Boolean']>
  assigneeId?: Maybe<Scalars['ID']>
  surcharge?: Maybe<Scalars['Float']>
  hasUnalterablePaymentTerms?: Maybe<Scalars['Boolean']>
  mustPayPreviousBalance?: Maybe<Scalars['Boolean']>
}

export type UpdateOrderPayload = {
  changedOrder: Order
}

export type UpdateOrganizationInput = {
  id: Scalars['ID']
  alias?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  phone?: Maybe<Scalars['String']>
  doingBusinessAs?: Maybe<Scalars['String']>
  manifestGDriveFolderId?: Maybe<Scalars['String']>
}

export type UpdateOrganizationInviteInput = {
  id: Scalars['ID']
  email?: Maybe<Scalars['String']>
  organizationId?: Maybe<Scalars['ID']>
  senderId?: Maybe<Scalars['ID']>
  accepted?: Maybe<Scalars['Boolean']>
}

export type UpdateOrganizationInvitePayload = {
  changedOrganizationInvite: OrganizationInvite
}

export type UpdateOrganizationPayload = {
  changedOrganization: Organization
}

export type UpdateOrganizationUserInput = {
  id: Scalars['ID']
  userId?: Maybe<Scalars['ID']>
  organizationId?: Maybe<Scalars['ID']>
}

export type UpdateOrganizationUserPayload = {
  changedOrganizationUser: OrganizationUser
}

export type UpdateRoleInput = {
  id: Scalars['ID']
  organizationId?: Maybe<Scalars['ID']>
  name?: Maybe<Scalars['String']>
  permissions?: Maybe<Array<Scalars['String']>>
}

export type UpdateRolePayload = {
  changedRole: Role
}

export type UpdateSiteInput = {
  id: Scalars['ID']
  name?: Maybe<Scalars['String']>
  pocName?: Maybe<Scalars['String']>
  pocPhoneNumber?: Maybe<Scalars['String']>
  pocEmail?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  lat?: Maybe<Scalars['Float']>
  lng?: Maybe<Scalars['Float']>
  siteCategory?: Maybe<SiteCategoryEnum>
  organizationId?: Maybe<Scalars['ID']>
  licensedLocationId?: Maybe<Scalars['ID']>
  licenses?: Maybe<Array<CreateLicenseInput>>
}

export type UpdateSitePayload = {
  changedSite: Site
}

export type UpdateSkuInput = {
  id: Scalars['ID']
  name?: Maybe<Scalars['String']>
  code?: Maybe<Scalars['String']>
  isExciseTaxable?: Maybe<Scalars['Boolean']>
  inventoryType?: Maybe<InventoryTypeEnum>
  inventoryClass?: Maybe<InventoryClassEnum>
  inventoryCategory?: Maybe<InventoryCategoryEnum>
  organizationId?: Maybe<Scalars['ID']>
  isSample?: Maybe<Scalars['Boolean']>
  pricePerUnit?: Maybe<Scalars['Float']>
  unit?: Maybe<Scalars['String']>
  batchId?: Maybe<Scalars['ID']>
  isArchived?: Maybe<Scalars['Boolean']>
}

export type UpdateSkuPayload = {
  changedSku: Sku
}

export type UpdateUserInput = {
  id: Scalars['ID']
  email?: Maybe<Scalars['String']>
  password?: Maybe<Scalars['String']>
  firstName?: Maybe<Scalars['String']>
  lastName?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  phone?: Maybe<Scalars['String']>
  profilePicture?: Maybe<Scalars['String']>
}

export type UpdateUserPayload = {
  changedUser: User
}

export type UpdateVehicleInput = {
  id: Scalars['ID']
  name?: Maybe<Scalars['String']>
  make?: Maybe<Scalars['String']>
  model?: Maybe<Scalars['String']>
  licensePlate?: Maybe<Scalars['String']>
  year?: Maybe<Scalars['Int']>
}

export type UpdateVehiclePayload = {
  changedVehicle: Vehicle
}

export type UploadToOnfleetInput = {
  orders: Array<Scalars['ID']>
}

export type UploadToOnfleetPayload = {
  ok: Scalars['Boolean']
  errors?: Maybe<Scalars['JSON']>
}

export type UpsertLineItemInput = {
  id?: Maybe<Scalars['ID']>
  code?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  unit?: Maybe<Scalars['String']>
  quantity?: Maybe<Scalars['Float']>
  pricePerUnit?: Maybe<Scalars['Float']>
  orderId?: Maybe<Scalars['ID']>
  skuBatchId?: Maybe<Scalars['ID']>
  organization?: Maybe<UpdateOrganizationInput>
  isExciseTaxable?: Maybe<Scalars['Boolean']>
  isSample?: Maybe<Scalars['Boolean']>
  oversold?: Maybe<Scalars['Boolean']>
  discount?: Maybe<Scalars['Float']>
}

export type User = {
  id: Scalars['ID']
  email?: Maybe<Scalars['String']>
  firstName?: Maybe<Scalars['String']>
  lastName?: Maybe<Scalars['String']>
  address1?: Maybe<Scalars['String']>
  address2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  zip?: Maybe<Scalars['String']>
  phone?: Maybe<Scalars['String']>
  profilePicture?: Maybe<Scalars['String']>
  isAdmin?: Maybe<Scalars['Boolean']>
  organizations: Array<Organization>
  organizationInvites: Array<OrganizationInvite>
  assigned: Array<Order>
  isArchived?: Maybe<Scalars['Boolean']>
  userRoles: Array<UserRole>
  roles: Array<Role>
  drafts: Array<Draft>
  userPreferences?: Maybe<Array<UserPreference>>
}

export type UserPreference = {
  id: Scalars['ID']
  organization: Organization
  user: User
  receiveOrderUpdateEmails?: Maybe<Scalars['Boolean']>
}

export type UserRole = {
  id: Scalars['ID']
  organization?: Maybe<Organization>
  role?: Maybe<Role>
  user?: Maybe<User>
}

export type Values = {
  allocated: Scalars['Float']
  available: Scalars['Float']
  counted: Scalars['Float']
  incoming: Scalars['Float']
  outgoing: Scalars['Float']
}

export type Vehicle = {
  id: Scalars['ID']
  name?: Maybe<Scalars['String']>
  make?: Maybe<Scalars['String']>
  model?: Maybe<Scalars['String']>
  licensePlate?: Maybe<Scalars['String']>
  year?: Maybe<Scalars['Int']>
}

export type VerificationStatusEnum = 'VERFIED' | 'UNVERIFIED' | 'REJECTED'

export type Viewer = {
  me?: Maybe<User>
  allUsers: Array<User>
  allOrganizations: Array<Organization>
  allAdminOrganizations: Array<Organization>
  allOrganizationUsers: Array<OrganizationUser>
  allRoles: Array<Role>
  allUserRoles: Array<UserRole>
  allDrafts?: Maybe<AllDraftsPayload>
  allOrders?: Maybe<AllOrdersPayload>
  allAdminOrders?: Maybe<AllAdminOrdersPayload>
  allSkus?: Maybe<SkuInfo>
  allAdminSkus?: Maybe<SkuInfo>
  accessibleSkus?: Maybe<Array<Maybe<Sku>>>
  allSkuBatches?: Maybe<SkuBatchInfo>
  allAdminSkuBatches?: Maybe<SkuBatchInfo>
  accessibleSkuBatches?: Maybe<SkuBatchInfo>
  allBatches?: Maybe<BatchInfo>
  allAdminBatches?: Maybe<BatchInfo>
  getDashboardData?: Maybe<DashboardData>
  allSites: Array<Site>
  allVehicles: Array<Vehicle>
  allLineItems: Array<LineItem>
  allLicenses: AllLicensesPayload
  allSalesTimeSeries: Array<Sales>
  allOrganizationInvites: Array<OrganizationInvite>
  allWarehouses: Array<Warehouse>
  allPublicSites: Array<Site>
  allLicensedLocations: AllLicensedLocationsPayload
  allInventory?: Maybe<Array<InventoryRow>>
  allInventoryLedgerItems: Array<InventoryLedgerItem>
  allInventoryItems: InventoryItemInfo
  allAdminInventoryItems: Array<InventoryRow>
  allDrivers: Array<Driver>
  accessibleBrands: Array<Organization>
  allOutstandingInvoices?: Maybe<AllOrdersPayload>
  allFutureDropoffsToDispensary: Array<Order>
  getAllSiteOutstandingInvoices?: Maybe<Array<Maybe<Order>>>
  getSite?: Maybe<Site>
  getOrder?: Maybe<Order>
  getTitleholders?: Maybe<Array<Maybe<Titleholder>>>
}

export type ViewerAllDraftsArgs = {
  organizationId: Scalars['ID']
  pageInfo?: Maybe<PageInfoInput>
}

export type ViewerAllOrdersArgs = {
  organizationId: Scalars['ID']
  number?: Maybe<Scalars['String']>
  status?: Maybe<OrderStatusEnum>
  paymentStatus?: Maybe<OrderPaymentStatusEnum>
  start?: Maybe<Scalars['DateTime']>
  end?: Maybe<Scalars['DateTime']>
  site?: Maybe<Scalars['String']>
  creator?: Maybe<Scalars['String']>
  pageInfo?: Maybe<PageInfoInput>
}

export type ViewerAllAdminOrdersArgs = {
  organizationId?: Maybe<Scalars['ID']>
  orderBy?: Maybe<Scalars['String']>
  direction?: Maybe<Scalars['String']>
  search?: Maybe<Scalars['String']>
  action?: Maybe<OrderActionEnum>
  status?: Maybe<OrderStatusEnum>
  paymentStatus?: Maybe<OrderPaymentStatusEnum>
  warehouseId?: Maybe<Scalars['ID']>
  driverId?: Maybe<Scalars['ID']>
  vehicleId?: Maybe<Scalars['ID']>
  start?: Maybe<Scalars['DateTime']>
  end?: Maybe<Scalars['DateTime']>
  pageInfo?: Maybe<PageInfoInput>
  transactionTypes?: Maybe<Array<Maybe<Scalars['String']>>>
}

export type ViewerAllSkusArgs = {
  search?: Maybe<Scalars['String']>
  organizationId?: Maybe<Scalars['ID']>
  isArchived?: Maybe<Scalars['Boolean']>
  pageInfo?: Maybe<PageInfoInput>
  isInventoryTab?: Maybe<Scalars['Boolean']>
}

export type ViewerAllAdminSkusArgs = {
  search?: Maybe<Scalars['String']>
  organizationId?: Maybe<Scalars['ID']>
  isArchived?: Maybe<Scalars['Boolean']>
  pageInfo?: Maybe<PageInfoInput>
}

export type ViewerAccessibleSkusArgs = {
  orgId: Scalars['ID']
}

export type ViewerAllSkuBatchesArgs = {
  isArchived?: Maybe<Scalars['Boolean']>
  organizationId?: Maybe<Scalars['ID']>
  pageInfo?: Maybe<PageInfoInput>
  search?: Maybe<Scalars['String']>
  isInventoryTab?: Maybe<Scalars['Boolean']>
}

export type ViewerAllAdminSkuBatchesArgs = {
  isArchived?: Maybe<Scalars['Boolean']>
  pageInfo?: Maybe<PageInfoInput>
  organizationId?: Maybe<Scalars['ID']>
  search?: Maybe<Scalars['String']>
}

export type ViewerAccessibleSkuBatchesArgs = {
  brandId?: Maybe<Scalars['ID']>
  orgId: Scalars['ID']
  pageInfo?: Maybe<PageInfoInput>
  search?: Maybe<Scalars['String']>
}

export type ViewerAllBatchesArgs = {
  isArchived?: Maybe<Scalars['Boolean']>
  organizationId?: Maybe<Scalars['ID']>
  pageInfo?: Maybe<PageInfoInput>
  search?: Maybe<Scalars['String']>
  isInventoryTab?: Maybe<Scalars['Boolean']>
}

export type ViewerAllAdminBatchesArgs = {
  isArchived?: Maybe<Scalars['Boolean']>
}

export type ViewerGetDashboardDataArgs = {
  organizationId: Scalars['ID']
}

export type ViewerAllSitesArgs = {
  organizationId: Scalars['ID']
}

export type ViewerAllLicensesArgs = {
  organizationId: Scalars['ID']
}

export type ViewerAllSalesTimeSeriesArgs = {
  organizationId: Scalars['ID']
}

export type ViewerAllOrganizationInvitesArgs = {
  organizationId?: Maybe<Scalars['ID']>
}

export type ViewerAllLicensedLocationsArgs = {
  name?: Maybe<Scalars['String']>
  showArchived?: Maybe<Scalars['Boolean']>
  pageInfo?: Maybe<PageInfoInput>
}

export type ViewerAllInventoryArgs = {
  organizationId: Scalars['ID']
}

export type ViewerAllInventoryLedgerItemsArgs = {
  inventoryItemId: Scalars['ID']
  orderId?: Maybe<Scalars['ID']>
  organizationID?: Maybe<Scalars['ID']>
}

export type ViewerAllInventoryItemsArgs = {
  skuBatchId: Scalars['ID']
  warehouseId: Scalars['ID']
  pageInfo?: Maybe<PageInfoInput>
}

export type ViewerAllAdminInventoryItemsArgs = {
  organizationId?: Maybe<Scalars['ID']>
  siteId?: Maybe<Scalars['ID']>
}

export type ViewerAccessibleBrandsArgs = {
  orgId: Scalars['ID']
}

export type ViewerAllOutstandingInvoicesArgs = {
  startDate?: Maybe<Scalars['String']>
  endDate?: Maybe<Scalars['String']>
  startDueDate?: Maybe<Scalars['String']>
  endDueDate?: Maybe<Scalars['String']>
  organizationId?: Maybe<Scalars['ID']>
  orderNumber?: Maybe<Scalars['String']>
  pageInfo?: Maybe<PageInfoInput>
  all?: Maybe<Scalars['Boolean']>
  warehouse?: Maybe<Scalars['String']>
  dispensary?: Maybe<Scalars['String']>
  orgName?: Maybe<Scalars['String']>
}

export type ViewerAllFutureDropoffsToDispensaryArgs = {
  id: Scalars['ID']
}

export type ViewerGetAllSiteOutstandingInvoicesArgs = {
  id: Scalars['ID']
}

export type ViewerGetSiteArgs = {
  id: Scalars['ID']
}

export type ViewerGetOrderArgs = {
  number?: Maybe<Scalars['String']>
}

export type ViewerGetTitleholdersArgs = {
  organizationId: Scalars['ID']
}

export type Warehouse = {
  id: Scalars['ID']
  license: License
  site: Site
  isArchived?: Maybe<Scalars['Boolean']>
}

export type AllAdminSkuBatchesForInventoryQueryVariables = {
  isArchived?: Maybe<Scalars['Boolean']>
  pageInfo?: Maybe<PageInfoInput>
  organizationId?: Maybe<Scalars['ID']>
  search?: Maybe<Scalars['String']>
}

export type AllAdminSkuBatchesForInventoryQuery = {
  viewer: Maybe<{
    allAdminSkuBatches: Maybe<{
      results: Array<
        {
          inventoryValues: {
            LA: Pick<Values, 'counted' | 'incoming' | 'outgoing' | 'allocated' | 'available'>
            OAK: Pick<Values, 'counted' | 'incoming' | 'outgoing' | 'allocated' | 'available'>
          }
        } & SkuBatchFragmentFragment
      >
      pageInfo: Pick<PageInfo, 'totalNumItems' | 'totalNumPages'>
    }>
  }>
}

export type SignInMutationVariables = {
  input: LoginUserInput
}

export type SignInMutation = { loginUser: Maybe<Pick<LoginUserPayload, 'token'> & { user: UserFragmentFragment }> }

export type SignUpMutationVariables = {
  input: CreateUserInput
}

export type SignUpMutation = {
  createUser: Maybe<Pick<CreateUserPayload, 'token'> & { changedUser: UserFragmentFragment }>
}

export type BatchFragmentFragment = Pick<
  Batch,
  | 'id'
  | 'code'
  | 'testDate'
  | 'deletedAt'
  | 'status'
  | 'expirationDate'
  | 'manufacturingDate'
  | 'manifestGDriveFileId'
  | 'createdAt'
>

export type CreateBatchMutationVariables = {
  input: CreateBatchInput
}

export type CreateBatchMutation = { createBatch: Maybe<BatchFragmentFragment> }

export type EditBatchMutationVariables = {
  input: UpdateBatchInput
}

export type EditBatchMutation = Pick<Mutation, 'editBatch'>

export type BulkUpdateBatchStatusMutationVariables = {
  batches: Array<Maybe<UpdateBatchInput>>
  targetStatus?: Maybe<BatchStatusEnum>
}

export type BulkUpdateBatchStatusMutation = Pick<Mutation, 'bulkUpdateBatchStatus'>

export type DeleteBatchMutationVariables = {
  id: Scalars['ID']
}

export type DeleteBatchMutation = Pick<Mutation, 'deleteBatch'>

export type UploadBatchFileMutationVariables = {
  inputFile: Scalars['Upload']
  id: Scalars['ID']
}

export type UploadBatchFileMutation = Pick<Mutation, 'uploadBatchFile'>

export type RestoreBatchMutationVariables = {
  batchId: Scalars['ID']
}

export type RestoreBatchMutation = { restoreBatch: Maybe<Pick<Batch, 'id'>> }

export type AllBatchesQueryVariables = {
  organizationId: Scalars['ID']
  pageInfo?: Maybe<PageInfoInput>
}

export type AllBatchesQuery = {
  viewer: Maybe<{
    allBatches: Maybe<{
      results: Array<
        Pick<Batch, 'id' | 'code' | 'manifestGDriveFileId' | 'manufacturingDate' | 'expirationDate' | 'status'> & {
          organization: Pick<Organization, 'id'>
          license: Maybe<LicenseFragmentFragment>
        }
      >
    }>
  }>
}

export type GetBatchQueryVariables = {
  id: Scalars['ID']
}

export type GetBatchQuery = {
  getBatch: Maybe<
    Pick<Batch, 'id' | 'code' | 'manifestGDriveFileId' | 'manufacturingDate' | 'expirationDate' | 'status'> & {
      organization: Pick<Organization, 'id'>
      license: Maybe<LicenseFragmentFragment>
    }
  >
}

export type InventoryBatchesTabQueryVariables = {
  isArchived?: Maybe<Scalars['Boolean']>
  organizationId?: Maybe<Scalars['ID']>
  search?: Maybe<Scalars['String']>
  pageInfo?: Maybe<PageInfoInput>
}

export type InventoryBatchesTabQuery = {
  viewer: Maybe<{
    allBatches: Maybe<{
      results: Array<
        Pick<Batch, 'id' | 'code' | 'manifestGDriveFileId' | 'manufacturingDate' | 'expirationDate' | 'status'> & {
          organization: Pick<Organization, 'id' | 'name'>
          license: Maybe<LicenseFragmentFragment>
          skuBatches: Array<
            Pick<SkuBatch, 'id'> & {
              sku: Pick<Sku, 'id' | 'code' | 'name'>
              inventoryValues: {
                LA: Pick<Values, 'counted' | 'allocated' | 'available'>
                OAK: Pick<Values, 'counted' | 'allocated' | 'available'>
              }
            }
          >
        }
      >
      pageInfo: Pick<PageInfo, 'totalNumItems' | 'totalNumPages'>
    }>
  }>
}

export type AllBatchesForSkuBatchModalQueryVariables = {
  organizationId: Scalars['ID']
}

export type AllBatchesForSkuBatchModalQuery = {
  viewer: Maybe<{
    allBatches: Maybe<{
      results: Array<Pick<Batch, 'id' | 'code' | 'expirationDate' | 'manufacturingDate' | 'manifestGDriveFileId'>>
    }>
  }>
}

export type GetUnavailableDatesQueryVariables = {}

export type GetUnavailableDatesQuery = Pick<Query, 'getUnavailableDates'>

export type DraftFragmentFragment = Pick<Draft, 'id' | 'body' | 'createdAt' | 'updatedAt'>

export type AllDraftsQueryVariables = {
  organizationId: Scalars['ID']
  pageInfo?: Maybe<PageInfoInput>
}

export type AllDraftsQuery = {
  viewer: Maybe<{
    me: Maybe<Pick<User, 'id'> & { drafts: Array<Pick<Draft, 'id' | 'body' | 'updatedAt' | 'createdAt'>> }>
    allDrafts: Maybe<{
      results: Maybe<Array<DraftFragmentFragment>>
      pageInfo: Maybe<Pick<PageInfo, 'totalNumItems' | 'totalNumPages' | 'page'>>
    }>
  }>
}

export type GetDraftQueryVariables = {
  id: Scalars['ID']
}

export type GetDraftQuery = { getDraft: Maybe<DraftFragmentFragment> }

export type CreateDraftMutationVariables = {
  input: CreateDraftInput
}

export type CreateDraftMutation = { createDraft: Maybe<{ createdDraft: Pick<Draft, 'id'> }> }

export type UpdateDraftMutationVariables = {
  input: UpdateDraftInput
}

export type UpdateDraftMutation = { updateDraft: Maybe<{ changedDraft: Pick<Draft, 'id'> }> }

export type DeleteDraftMutationVariables = {
  input: DeleteDraftInput
}

export type DeleteDraftMutation = { deleteDraft: Maybe<{ deletedDraft: Pick<Draft, 'id'> }> }

export type DriverFragmentFragment = Pick<Driver, 'id' | 'firstName' | 'lastName' | 'driversLicense'>

export type CreateDriverMutationVariables = {
  input: CreateDriverInput
}

export type CreateDriverMutation = { createDriver: Maybe<{ changedDriver: DriverFragmentFragment }> }

export type UpdateDriverMutationVariables = {
  input: UpdateDriverInput
}

export type UpdateDriverMutation = { updateDriver: Maybe<{ changedDriver: DriverFragmentFragment }> }

export type DeleteDriverMutationVariables = {
  input: DeleteDriverInput
}

export type DeleteDriverMutation = { deleteDriver: Maybe<{ changedDriver: DriverFragmentFragment }> }

export type AllDriversVehiclesQueryVariables = {}

export type AllDriversVehiclesQuery = {
  viewer: Maybe<{ allVehicles: Array<VehicleFragmentFragment>; allDrivers: Array<DriverFragmentFragment> }>
}

export type InventoryItemFragmentFragment = Pick<
  InventoryItem,
  'id' | 'counted' | 'allocated' | 'promised' | 'storageType' | 'createdAt' | 'updatedAt'
>

export type CorrectInventoryItemMutationVariables = {
  input: CorrectInventoryItemInput
}

export type CorrectInventoryItemMutation = {
  correctInventoryItem: Maybe<{ changedInventoryItem: Pick<InventoryItem, 'id'> }>
}

export type CreateInventoryItemMutationVariables = {
  organizationId: Scalars['ID']
  skuBatchId: Scalars['ID']
  warehouseId: Scalars['ID']
}

export type CreateInventoryItemMutation = { createInventoryItem: Maybe<Pick<InventoryItem, 'id'>> }

export type GetInventoryItemQueryVariables = {
  warehouseId?: Maybe<Scalars['ID']>
  skuBatchId?: Maybe<Scalars['ID']>
  organizationId?: Maybe<Scalars['ID']>
}

export type GetInventoryItemQuery = {
  getInventoryItem: Maybe<
    { warehouse: { site: SiteFragmentFragment } & WarehouseFragmentFragment } & InventoryItemFragmentFragment
  >
}

export type GetInventoryItemsQueryVariables = {
  skuBatchId: Scalars['ID']
}

export type GetInventoryItemsQuery = {
  getInventoryItems: Array<
    {
      skuBatch: Pick<SkuBatch, 'id'>
      warehouse: { site: SiteFragmentFragment } & WarehouseFragmentFragment
    } & InventoryItemFragmentFragment
  >
}

export type AllInventoryItemsQueryVariables = {
  skuBatchId: Scalars['ID']
  warehouseId: Scalars['ID']
  pageInfo?: Maybe<PageInfoInput>
}

export type AllInventoryItemsQuery = {
  viewer: Maybe<{
    allInventoryItems: {
      results: Array<{ order: Maybe<OrderFragmentFragment> } & InventoryLedgerItemFragmentFragment>
      pageInfo: PageInfoFragmentFragment
    }
  }>
}

export type InventoryLedgerItemFragmentFragment = Pick<
  InventoryLedgerItem,
  'id' | 'transactionType' | 'startCounted' | 'endCounted' | 'notes' | 'createdAt' | 'updatedAt'
>

export type LicenseFragmentFragment = Pick<
  License,
  | 'id'
  | 'nickname'
  | 'category'
  | 'type'
  | 'licenseNumber'
  | 'legalEntityName'
  | 'issuanceDate'
  | 'expirationDate'
  | 'contactName'
  | 'contactPhone'
  | 'contactEmail'
  | 'address1'
  | 'address2'
  | 'city'
  | 'state'
  | 'zip'
>

export type CreateLicenseMutationVariables = {
  input: CreateLicenseInput
}

export type CreateLicenseMutation = { createLicense: Maybe<{ changedLicense: LicenseFragmentFragment }> }

export type UpdateLicenseMutationVariables = {
  input: UpdateLicenseInput
}

export type UpdateLicenseMutation = { updateLicense: Maybe<{ changedLicense: LicenseFragmentFragment }> }

export type DeleteLicenseMutationVariables = {
  input: DeleteLicenseInput
}

export type DeleteLicenseMutation = { deleteLicense: Maybe<{ changedLicense: LicenseFragmentFragment }> }

export type AllLicensesQueryVariables = {
  organizationId: Scalars['ID']
}

export type AllLicensesQuery = {
  viewer: Maybe<{ allLicenses: { organizationLicenses: Array<LicenseFragmentFragment> } }>
}

export type AllLicensedLocationsQueryVariables = {
  name?: Maybe<Scalars['String']>
  showArchived?: Maybe<Scalars['Boolean']>
  pageInfo?: Maybe<PageInfoInput>
}

export type AllLicensedLocationsQuery = {
  viewer: Maybe<{
    allLicensedLocations: {
      results: Maybe<
        Array<
          {
            licenses: Maybe<Array<Maybe<LicenseFragmentFragment>>>
            warehouse: Maybe<WarehouseFragmentFragment>
          } & LicensedLocationFragmentFragment
        >
      >
      pageInfo: Maybe<PageInfoFragmentFragment>
    }
  }>
}

export type LicensedLocationFragmentFragment = Pick<
  LicensedLocation,
  'id' | 'name' | 'address1' | 'address2' | 'city' | 'state' | 'zip' | 'siteCategory' | 'lat' | 'lng' | 'isArchived'
>

export type CreateLicensedLocationMutationVariables = {
  input: CreateLicensedLocationInput
}

export type CreateLicensedLocationMutation = Pick<Mutation, 'createLicensedLocation'>

export type UpdateLicensedLocationMutationVariables = {
  input: UpdateLicensedLocationInput
}

export type UpdateLicensedLocationMutation = Pick<Mutation, 'updateLicensedLocation'>

export type DeleteLicensedLocationMutationVariables = {
  input: DeleteLicensedLocationInput
}

export type DeleteLicensedLocationMutation = Pick<Mutation, 'deleteLicensedLocation'>

export type UnarchiveLicensedLocationMutationVariables = {
  input: UnarchiveLicensedLocationInput
}

export type UnarchiveLicensedLocationMutation = Pick<Mutation, 'unarchiveLicensedLocation'>

export type LineItemFragmentFragment = Pick<
  LineItem,
  | 'id'
  | 'code'
  | 'name'
  | 'unit'
  | 'quantity'
  | 'pricePerUnit'
  | 'isExciseTaxable'
  | 'isSample'
  | 'oversold'
  | 'discount'
>

export type LineItemIntakeOuttakeMutationVariables = {
  input: LineItemIntakeOuttakeInput
}

export type LineItemIntakeOuttakeMutation = {
  lineItemIntakeOuttake: Maybe<
    Array<
      Maybe<
        Pick<InventoryItem, 'id'> & {
          skuBatch: SkuBatchFragmentFragment
          inventoryLedgerItems: Array<
            {
              lineItem: Maybe<Pick<LineItem, 'id'>>
              order: Maybe<Pick<Order, 'id'>>
            } & InventoryLedgerItemFragmentFragment
          >
        } & InventoryItemFragmentFragment
      >
    >
  >
}

export type CreateLineItemMutationVariables = {
  input: CreateLineItemInput
}

export type CreateLineItemMutation = { createLineItem: Maybe<{ changedLineItem: Pick<LineItem, 'id'> }> }

export type UpdateLineItemMutationVariables = {
  input: UpdateLineItemInput
}

export type UpdateLineItemMutation = { updateLineItem: Maybe<{ changedLineItem: Pick<LineItem, 'id'> }> }

export type DeleteLineItemMutationVariables = {
  input: DeleteLineItemInput
}

export type DeleteLineItemMutation = { deleteLineItem: Maybe<{ changedLineItem: Pick<LineItem, 'id'> }> }

export type LineItemChangeFragmentFragment = Pick<
  LineItemChange,
  | 'id'
  | 'isExciseTaxable'
  | 'isSample'
  | 'lineItemId'
  | 'name'
  | 'oversold'
  | 'pricePerUnit'
  | 'quantity'
  | 'skuBatchId'
  | 'type'
  | 'discount'
>

export type UploadToOnfleetMutationVariables = {
  input: UploadToOnfleetInput
}

export type UploadToOnfleetMutation = { uploadToOnfleet: Maybe<Pick<UploadToOnfleetPayload, 'ok' | 'errors'>> }

export type CreateOrderChangeMutationVariables = {
  submitterId: Scalars['ID']
  description: Scalars['String']
  orderId: Scalars['ID']
}

export type CreateOrderChangeMutation = Pick<Mutation, 'createOrderChange'>

export type AllAdminOrdersPageQueryVariables = {
  organizationId?: Maybe<Scalars['ID']>
  search?: Maybe<Scalars['String']>
  action?: Maybe<OrderActionEnum>
  status?: Maybe<OrderStatusEnum>
  paymentStatus?: Maybe<OrderPaymentStatusEnum>
  warehouseId?: Maybe<Scalars['ID']>
  driverId?: Maybe<Scalars['ID']>
  vehicleId?: Maybe<Scalars['ID']>
  start?: Maybe<Scalars['DateTime']>
  end?: Maybe<Scalars['DateTime']>
  transactionTypes?: Maybe<Array<Maybe<Scalars['String']>>>
  pageInfo?: Maybe<PageInfoInput>
}

export type AllAdminOrdersPageQuery = {
  viewer: Maybe<{
    allAdminOrders: Maybe<{
      results: Maybe<
        Array<
          Pick<
            Order,
            | 'id'
            | 'action'
            | 'adminNotes'
            | 'internalNotes'
            | 'createdAt'
            | 'date'
            | 'daysTillPaymentDue'
            | 'discount'
            | 'edited'
            | 'estimatedArrivalTimeAfter'
            | 'estimatedArrivalTimeBefore'
            | 'exciseTax'
            | 'exciseTaxCollected'
            | 'gmv'
            | 'gmvCollected'
            | 'intaken'
            | 'manifestGDriveFileId'
            | 'apSummaryGDriveFileId'
            | 'invoicesS3FileLink'
            | 'name'
            | 'notes'
            | 'number'
            | 'outtaken'
            | 'paymentStatus'
            | 'status'
            | 'timeWindow'
          > & {
            creator: Maybe<Pick<User, 'id' | 'email' | 'firstName' | 'lastName'>>
            driver: Maybe<Pick<Driver, 'id' | 'firstName' | 'lastName' | 'driversLicense'>>
            lineItems: Array<
              Pick<
                LineItem,
                | 'id'
                | 'code'
                | 'name'
                | 'unit'
                | 'quantity'
                | 'pricePerUnit'
                | 'isExciseTaxable'
                | 'isSample'
                | 'oversold'
              > & {
                skuBatch: Maybe<
                  Pick<SkuBatch, 'id'> & {
                    batch: Maybe<Pick<Batch, 'id' | 'code' | 'manifestGDriveFileId'>>
                    sku: Pick<Sku, 'id' | 'code'> & { organization: Pick<Organization, 'id' | 'name'> }
                    titleholder: Pick<Titleholder, 'id' | 'type'> & {
                      titleholderLicense: Maybe<LicenseFragmentFragment>
                      titleholderOrganization: Maybe<
                        { licenses: Array<LicenseFragmentFragment> } & OrganizationFragmentFragment
                      >
                    }
                  }
                >
              }
            >
            organization: Pick<Organization, 'id' | 'alias' | 'name'> & {
              owner: Pick<User, 'id' | 'email' | 'firstName' | 'lastName'>
            }
            site: Pick<
              Site,
              | 'id'
              | 'name'
              | 'address1'
              | 'address2'
              | 'city'
              | 'state'
              | 'zip'
              | 'pocName'
              | 'pocPhoneNumber'
              | 'pocEmail'
            >
            vehicle: Maybe<Pick<Vehicle, 'id' | 'name' | 'make' | 'model' | 'licensePlate' | 'year'>>
            warehouse: Maybe<Pick<Warehouse, 'id'> & { site: Pick<Site, 'id' | 'name'> }>
          }
        >
      >
      pageInfo: Maybe<
        Pick<PageInfo, 'page' | 'numItemsPerPage' | 'totalNumItems' | 'totalNumPages'> & {
          orderBy: Array<Pick<OrderBy, 'attribute' | 'order'>>
        }
      >
    }>
    allWarehouses: Array<Pick<Warehouse, 'id'> & { site: Pick<Site, 'id' | 'name' | 'city' | 'state'> }>
    allDrivers: Array<Pick<Driver, 'id' | 'firstName' | 'lastName' | 'driversLicense'>>
    allVehicles: Array<Pick<Vehicle, 'id' | 'name' | 'make' | 'model' | 'licensePlate' | 'year'>>
  }>
}

export type OrderFragmentFragment = Pick<
  Order,
  | 'action'
  | 'adminNotes'
  | 'createdAt'
  | 'date'
  | 'daysTillPaymentDue'
  | 'discount'
  | 'estimatedArrivalTimeAfter'
  | 'estimatedArrivalTimeBefore'
  | 'exciseTax'
  | 'exciseTaxCollected'
  | 'gmv'
  | 'gmvCollected'
  | 'id'
  | 'intaken'
  | 'internalNotes'
  | 'irn'
  | 'isArchived'
  | 'manifestGDriveFileId'
  | 'invoicesS3FileLink'
  | 'name'
  | 'notes'
  | 'number'
  | 'orgLicenseNum'
  | 'outtaken'
  | 'paymentStatus'
  | 'siteLicenseNum'
  | 'status'
  | 'timeWindow'
  | 'warehouseId'
  | 'surcharge'
  | 'hasUnalterablePaymentTerms'
  | 'mustPayPreviousBalance'
>

export type CreateOrderMutationVariables = {
  input: CreateOrderInput
}

export type CreateOrderMutation = { createOrder: Maybe<{ changedOrder: OrderFragmentFragment }> }

export type UpdateOrderMutationVariables = {
  input: UpdateOrderInput
}

export type UpdateOrderMutation = { updateOrder: Maybe<{ changedOrder: OrderFragmentFragment }> }

export type DeleteOrderMutationVariables = {
  input: DeleteOrderInput
}

export type DeleteOrderMutation = { deleteOrder: Maybe<{ changedOrder: OrderFragmentFragment }> }

export type GenerateManifestMutationVariables = {
  orderId: Scalars['ID']
}

export type GenerateManifestMutation = {
  generateManifest: Maybe<Pick<GenerateManifestPayload, 'apSummaryGDriveFileId' | 'invoicesS3FileLink'>>
}

export type GetOrderSubscriberMutationVariables = {
  orderId: Scalars['ID']
  organizationId: Scalars['ID']
}

export type GetOrderSubscriberMutation = { getOrderSubscriber: Maybe<OrderSubscriberFragmentFragment> }

export type AssignInvoiceToOrderMutationVariables = {
  invoiceId: Scalars['ID']
  orderId: Scalars['ID']
}

export type AssignInvoiceToOrderMutation = Pick<Mutation, 'assignInvoiceToOrder'>

export type AllOrdersQueryVariables = {
  organizationId: Scalars['ID']
  number?: Maybe<Scalars['String']>
  status?: Maybe<OrderStatusEnum>
  paymentStatus?: Maybe<OrderPaymentStatusEnum>
  start?: Maybe<Scalars['DateTime']>
  end?: Maybe<Scalars['DateTime']>
  site?: Maybe<Scalars['String']>
  creator?: Maybe<Scalars['String']>
  pageInfo?: Maybe<PageInfoInput>
}

export type AllOrdersQuery = {
  viewer: Maybe<{
    allOrders: Maybe<{
      results: Maybe<
        Array<
          {
            organization: OrganizationFragmentFragment
            site: SiteFragmentFragment
            lineItems: Array<LineItemFragmentFragment>
            creator: Maybe<UserFragmentFragment>
            assignee: Maybe<UserFragmentFragment>
            orderSubscribers: Array<OrderSubscriberFragmentFragment>
          } & OrderFragmentFragment
        >
      >
      pageInfo: Maybe<PageInfoFragmentFragment>
    }>
  }>
}

export type ActiveOrdersForSkuBatchQueryVariables = {
  skuBatchId: Scalars['ID']
}

export type ActiveOrdersForSkuBatchQuery = {
  getActiveOrdersForSkuBatch: Array<
    {
      site: { licensedLocation: Maybe<LicensedLocationFragmentFragment> } & SiteFragmentFragment
      lineItems: Array<{ skuBatch: Maybe<SkuBatchFragmentFragment> } & LineItemFragmentFragment>
      warehouse: Maybe<{ site: SiteFragmentFragment } & WarehouseFragmentFragment>
    } & OrderFragmentFragment
  >
}

export type AllAdminOrdersQueryVariables = {
  organizationId?: Maybe<Scalars['ID']>
  orderBy?: Maybe<Scalars['String']>
  direction?: Maybe<Scalars['String']>
  search?: Maybe<Scalars['String']>
  action?: Maybe<OrderActionEnum>
  status?: Maybe<OrderStatusEnum>
  paymentStatus?: Maybe<OrderPaymentStatusEnum>
  warehouseId?: Maybe<Scalars['ID']>
  driverId?: Maybe<Scalars['ID']>
  vehicleId?: Maybe<Scalars['ID']>
  start?: Maybe<Scalars['DateTime']>
  end?: Maybe<Scalars['DateTime']>
  pageInfo?: Maybe<PageInfoInput>
  transactionTypes?: Maybe<Array<Maybe<Scalars['String']>>>
}

export type AllAdminOrdersQuery = {
  viewer: Maybe<{
    allAdminOrders: Maybe<{
      results: Maybe<
        Array<
          Pick<Order, 'apSummaryGDriveFileId' | 'invoicesS3FileLink'> & {
            creator: Maybe<UserFragmentFragment>
            driver: Maybe<DriverFragmentFragment>
            lineItems: Array<
              {
                skuBatch: Maybe<
                  {
                    batch: Maybe<BatchFragmentFragment>
                    sku: { organization: OrganizationFragmentFragment } & SkuFragmentFragment
                    inventoryItems: Maybe<
                      Array<
                        Maybe<
                          {
                            skuBatch: SkuBatchFragmentFragment
                            warehouse: WarehouseFragmentFragment
                          } & InventoryItemFragmentFragment
                        >
                      >
                    >
                    titleholder: Pick<Titleholder, 'id' | 'type'> & {
                      titleholderLicense: Maybe<LicenseFragmentFragment>
                      titleholderOrganization: Maybe<
                        { licenses: Array<LicenseFragmentFragment> } & OrganizationFragmentFragment
                      >
                    }
                  } & SkuBatchFragmentFragment
                >
              } & LineItemFragmentFragment
            >
            organization: { owner: UserFragmentFragment } & OrganizationFragmentFragment
            site: SiteFragmentFragment
            vehicle: Maybe<VehicleFragmentFragment>
            warehouse: Maybe<{ site: SiteFragmentFragment } & WarehouseFragmentFragment>
          } & OrderFragmentFragment
        >
      >
      pageInfo: Maybe<PageInfoFragmentFragment>
    }>
    allWarehouses: Array<{ site: SiteFragmentFragment } & WarehouseFragmentFragment>
    allDrivers: Array<DriverFragmentFragment>
    allVehicles: Array<VehicleFragmentFragment>
  }>
}

export type AllOrdersSkusQueryVariables = {
  organizationId: Scalars['ID']
}

export type AllOrdersSkusQuery = {
  viewer: Maybe<{
    getDashboardData: Maybe<
      Pick<
        DashboardData,
        'totalDelivered' | 'totalScheduled' | 'totalUnscheduled' | 'totalGMVCollected' | 'totalExciseTaxCollected'
      >
    >
    allSkus: Maybe<{ results: Array<SkuFragmentFragment> }>
  }>
}

export type GetOrderQueryVariables = {
  id: Scalars['ID']
}

export type GetOrderQuery = {
  getOrder: Maybe<
    {
      site: {
        licenses: Array<LicenseFragmentFragment>
        licensedLocation: Maybe<
          { licenses: Maybe<Array<Maybe<LicenseFragmentFragment>>> } & LicensedLocationFragmentFragment
        >
      } & SiteFragmentFragment
      lineItems: Array<
        {
          skuBatch: Maybe<
            {
              inventoryValues: {
                LA: Pick<Values, 'counted' | 'incoming' | 'outgoing' | 'allocated' | 'available'>
                OAK: Pick<Values, 'counted' | 'incoming' | 'outgoing' | 'allocated' | 'available'>
              }
              batch: Maybe<BatchFragmentFragment>
              sku: { organization: OrganizationFragmentFragment } & SkuFragmentFragment
            } & SkuBatchFragmentFragment
          >
          organization: Maybe<OrganizationFragmentFragment>
        } & LineItemFragmentFragment
      >
      warehouse: Maybe<{ site: SiteFragmentFragment } & WarehouseFragmentFragment>
      organization: OrganizationFragmentFragment
      orderSubscribers: Array<OrderSubscriberFragmentFragment>
    } & OrderFragmentFragment
  >
}

export type OrderChangelogQueryVariables = {
  id: Scalars['ID']
}

export type OrderChangelogQuery = {
  getOrderChangelog: Array<
    Pick<OrderChange, 'createdAt' | 'description'> & {
      submitter: UserFragmentFragment
      addedLineItems: Maybe<
        Array<
          {
            skuBatch: Maybe<
              { sku: SkuFragmentFragment; batch: Maybe<BatchFragmentFragment> } & SkuBatchFragmentFragment
            >
          } & LineItemChangeFragmentFragment
        >
      >
      removedLineItems: Maybe<
        Array<
          {
            skuBatch: Maybe<
              { sku: SkuFragmentFragment; batch: Maybe<BatchFragmentFragment> } & SkuBatchFragmentFragment
            >
          } & LineItemChangeFragmentFragment
        >
      >
    }
  >
  getOrder: Maybe<OrderFragmentFragment>
}

export type OrderSubscriberFragmentFragment = Pick<
  OrderSubscriber,
  'id' | 'orderId' | 'subscriberId' | 'manifestGDriveFileId' | 'invoicesS3FileLink'
>

export type AllAdminOrganizationsQueryVariables = {}

export type AllAdminOrganizationsQuery = {
  viewer: Maybe<{ allAdminOrganizations: Array<OrganizationFragmentFragment> }>
}

export type OrganizationFragmentFragment = Pick<
  Organization,
  | 'id'
  | 'name'
  | 'alias'
  | 'address1'
  | 'address2'
  | 'city'
  | 'state'
  | 'zip'
  | 'phone'
  | 'doingBusinessAs'
  | 'manifestGDriveFolderId'
>

export type CreateOrganizationMutationVariables = {
  input: CreateOrganizationInput
}

export type CreateOrganizationMutation = {
  createOrganization: Maybe<{ changedOrganization: OrganizationFragmentFragment }>
}

export type UpdateOrganizationMutationVariables = {
  input: UpdateOrganizationInput
}

export type UpdateOrganizationMutation = {
  updateOrganization: Maybe<{ changedOrganization: OrganizationFragmentFragment }>
}

export type DeleteOrganizationMutationVariables = {
  input: DeleteOrganizationInput
}

export type DeleteOrganizationMutation = {
  deleteOrganization: Maybe<{ changedOrganization: Pick<Organization, 'id'> }>
}

export type OrganizationQueryVariables = {
  id: Scalars['ID']
}

export type OrganizationQuery = {
  getOrganization: Maybe<
    {
      owner: UserFragmentFragment
      members: Array<{ roles: Array<RoleFragmentFragment> } & UserFragmentFragment>
      licenses: Array<LicenseFragmentFragment>
    } & OrganizationFragmentFragment
  >
}

export type GetOrganizationUsersQueryVariables = {
  organizationId: Scalars['ID']
}

export type GetOrganizationUsersQuery = {
  getOrganization: Maybe<
    Pick<Organization, 'id'> & {
      owner: Pick<User, 'id'>
      members: Array<
        Pick<User, 'id' | 'email' | 'phone' | 'firstName' | 'lastName'> & { roles: Array<Pick<Role, 'id'>> }
      >
    }
  >
}

export type OrganizationInviteFragmentFragment = Pick<
  OrganizationInvite,
  'id' | 'email' | 'accepted' | 'respondedAt'
> & { organization: OrganizationFragmentFragment; sender: UserFragmentFragment }

export type CreateOrganizationInviteMutationVariables = {
  input: CreateOrganizationInviteInput
}

export type CreateOrganizationInviteMutation = {
  createOrganizationInvite: Maybe<{
    changedOrganizationInvite: { organization: OrganizationFragmentFragment } & OrganizationInviteFragmentFragment
  }>
}

export type UpdateOrganizationInviteMutationVariables = {
  input: UpdateOrganizationInviteInput
}

export type UpdateOrganizationInviteMutation = {
  updateOrganizationInvite: Maybe<{
    changedOrganizationInvite: {
      organization: OrganizationFragmentFragment
      sender: UserFragmentFragment
    } & OrganizationInviteFragmentFragment
  }>
}

export type AccessibleBrandsQueryVariables = {
  orgId: Scalars['ID']
}

export type AccessibleBrandsQuery = { viewer: Maybe<{ accessibleBrands: Array<OrganizationFragmentFragment> }> }

export type DeleteOrganizationUserMutationVariables = {
  input: DeleteOrganizationUserInput
}

export type DeleteOrganizationUserMutation = {
  deleteOrganizationUser: Maybe<{
    changedOrganizationUser: Pick<OrganizationUser, 'id'> & {
      organization: { owner: UserFragmentFragment } & OrganizationFragmentFragment
    }
  }>
}

export type AllOrganizationsQueryVariables = {}

export type AllOrganizationsQuery = {
  viewer: Maybe<{ allOrganizations: Array<{ owner: UserFragmentFragment } & OrganizationFragmentFragment> }>
}

export type PageInfoFragmentFragment = Pick<PageInfo, 'totalNumItems' | 'totalNumPages'>

export type SendSendGridStatusUpdateEmailMutationVariables = {
  input: SendSendGridStatusUpdateInput
}

export type SendSendGridStatusUpdateEmailMutation = {
  sendSendGridStatusUpdateEmail: Maybe<Pick<SendSendGridStatusUpdateEmailPayload, 'success'>>
}

export type CreateTokenSendEmailMutationVariables = {
  email?: Maybe<Scalars['String']>
}

export type CreateTokenSendEmailMutation = Pick<Mutation, 'createTokenSendEmail'>

export type SiteFragmentFragment = Pick<
  Site,
  | 'id'
  | 'name'
  | 'address1'
  | 'address2'
  | 'city'
  | 'state'
  | 'zip'
  | 'pocName'
  | 'pocPhoneNumber'
  | 'pocEmail'
  | 'siteCategory'
  | 'createdAt'
  | 'licensedLocationId'
>

export type CreateSiteMutationVariables = {
  input: CreateSiteInput
}

export type CreateSiteMutation = { createSite: Maybe<{ changedSite: Pick<Site, 'id'> }> }

export type UpdateSiteMutationVariables = {
  input: UpdateSiteInput
}

export type UpdateSiteMutation = { updateSite: Maybe<{ changedSite: Pick<Site, 'id' | 'name'> }> }

export type AllSitesQueryVariables = {
  organizationId: Scalars['ID']
}

export type AllSitesQuery = {
  viewer: Maybe<{ allSites: Array<{ licenses: Array<LicenseFragmentFragment> } & SiteFragmentFragment> }>
}

export type AllSkusAndSitesQueryVariables = {
  organizationId: Scalars['ID']
}

export type AllSkusAndSitesQuery = {
  viewer: Maybe<{
    allSites: Array<{ licenses: Array<LicenseFragmentFragment> } & SiteFragmentFragment>
    accessibleBrands: Array<Pick<Organization, 'id' | 'name'>>
  }>
}

export type SkuFragmentFragment = Pick<
  Sku,
  | 'id'
  | 'code'
  | 'name'
  | 'unit'
  | 'pricePerUnit'
  | 'isArchived'
  | 'isExciseTaxable'
  | 'totalCount'
  | 'totalCountLA'
  | 'totalCountOak'
  | 'lastUpdate'
  | 'inventoryType'
  | 'inventoryClass'
  | 'inventoryCategory'
>

export type CreateSkuMutationVariables = {
  input: CreateSkuInput
}

export type CreateSkuMutation = { createSku: Maybe<{ changedSku: Pick<Sku, 'id'> }> }

export type UpdateSkuMutationVariables = {
  input: UpdateSkuInput
}

export type UpdateSkuMutation = { updateSku: Maybe<{ changedSku: Pick<Sku, 'id'> }> }

export type DeleteSkuMutationVariables = {
  input: DeleteSkuInput
}

export type DeleteSkuMutation = { deleteSku: Maybe<{ changedSku: Pick<Sku, 'id'> }> }

export type CreateSkuAndBatchMutationVariables = {
  input: CreateSkuInput
}

export type CreateSkuAndBatchMutation = { createSkuAndSkuBatch: Maybe<{ changedSku: Pick<Sku, 'id'> }> }

export type RestoreSkuMutationVariables = {
  skuId: Scalars['ID']
}

export type RestoreSkuMutation = { restoreSku: Maybe<Pick<Sku, 'id'>> }

export type GetSkuQueryVariables = {
  id: Scalars['ID']
}

export type GetSkuQuery = { getSku: Maybe<{ organization: OrganizationFragmentFragment } & SkuFragmentFragment> }

export type AllSkusQueryVariables = {
  organizationId?: Maybe<Scalars['ID']>
  pageInfo?: Maybe<PageInfoInput>
  isArchived?: Maybe<Scalars['Boolean']>
  isInventoryTab?: Maybe<Scalars['Boolean']>
  search?: Maybe<Scalars['String']>
}

export type AllSkusQuery = {
  viewer: Maybe<{
    allSkus: Maybe<{
      results: Array<
        Pick<
          Sku,
          | 'id'
          | 'isArchived'
          | 'code'
          | 'name'
          | 'pricePerUnit'
          | 'unit'
          | 'inventoryType'
          | 'inventoryClass'
          | 'inventoryCategory'
          | 'isExciseTaxable'
        > & {
          organization: Pick<Organization, 'id' | 'name' | 'doingBusinessAs'>
          skuBatches: Maybe<
            Array<Maybe<Pick<SkuBatch, 'id' | 'deletedAt'> & { batch: Maybe<Pick<Batch, 'id' | 'code'>> }>>
          >
        }
      >
      pageInfo: Pick<PageInfo, 'totalNumItems' | 'totalNumPages'>
    }>
  }>
}

export type AllProductsQueryVariables = {
  organizationId: Scalars['ID']
}

export type AllProductsQuery = {
  viewer: Maybe<{
    allSkus: Maybe<{
      results: Array<
        Pick<
          Sku,
          | 'id'
          | 'code'
          | 'name'
          | 'pricePerUnit'
          | 'unit'
          | 'inventoryType'
          | 'inventoryClass'
          | 'inventoryCategory'
          | 'isExciseTaxable'
        > & {
          skuBatches: Maybe<
            Array<Maybe<Pick<SkuBatch, 'id' | 'deletedAt'> & { batch: Maybe<Pick<Batch, 'id' | 'code'>> }>>
          >
        }
      >
    }>
  }>
}

export type SkuBatchFragmentFragment = Pick<SkuBatch, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt'> & {
  inventoryValues: {
    LA: Pick<Values, 'allocated' | 'available' | 'counted' | 'incoming' | 'outgoing'>
    OAK: Pick<Values, 'allocated' | 'available' | 'counted' | 'incoming' | 'outgoing'>
  }
}

export type GetSkuBatchQueryVariables = {
  id: Scalars['ID']
}

export type GetSkuBatchQuery = {
  viewer: Maybe<{ allWarehouses: Array<{ site: SiteFragmentFragment } & WarehouseFragmentFragment> }>
  getSkuBatch: Maybe<
    {
      batch: Maybe<BatchFragmentFragment>
      sku: { organization: OrganizationFragmentFragment } & SkuFragmentFragment
    } & SkuBatchFragmentFragment
  >
}

export type AccessibleSkuBatchesQueryVariables = {
  brandId?: Maybe<Scalars['ID']>
  search?: Maybe<Scalars['String']>
  organizationId: Scalars['ID']
  pageInfo?: Maybe<PageInfoInput>
}

export type AccessibleSkuBatchesQuery = {
  viewer: Maybe<{
    accessibleSkuBatches: Maybe<{
      results: Array<
        {
          sku: { organization: OrganizationFragmentFragment } & SkuFragmentFragment
          batch: Maybe<BatchFragmentFragment>
        } & SkuBatchFragmentFragment
      >
    }>
  }>
}

export type InventorySkuBatchesTabQueryVariables = {
  organizationId?: Maybe<Scalars['ID']>
  isArchived?: Maybe<Scalars['Boolean']>
  search?: Maybe<Scalars['String']>
  pageInfo?: Maybe<PageInfoInput>
}

export type InventorySkuBatchesTabQuery = {
  viewer: Maybe<{
    allSkuBatches: Maybe<{
      results: Array<
        Pick<SkuBatch, 'id' | 'deletedAt'> & {
          inventoryValues: {
            LA: Pick<Values, 'counted' | 'incoming' | 'outgoing' | 'allocated' | 'available'>
            OAK: Pick<Values, 'counted' | 'incoming' | 'outgoing' | 'allocated' | 'available'>
          }
          sku: {
            batches: Maybe<Array<Maybe<Pick<Batch, 'id' | 'code'>>>>
            organization: OrganizationFragmentFragment
          } & SkuFragmentFragment
          batch: Maybe<
            Pick<Batch, 'id' | 'code' | 'manifestGDriveFileId'> & { license: Maybe<LicenseFragmentFragment> }
          >
          inventoryItems: Maybe<
            Array<
              Maybe<
                Pick<InventoryItem, 'id' | 'counted'> & {
                  warehouse: Pick<Warehouse, 'id'> & { site: Pick<Site, 'id' | 'name'> }
                }
              >
            >
          >
          titleholder: Pick<Titleholder, 'id' | 'type'> & {
            titleholderLicense: Maybe<LicenseFragmentFragment>
            titleholderOrganization: Maybe<{ licenses: Array<LicenseFragmentFragment> } & OrganizationFragmentFragment>
          }
        }
      >
      pageInfo: Pick<PageInfo, 'totalNumItems' | 'totalNumPages'>
    }>
  }>
}

export type AllSkuBatchesForCsvQueryVariables = {
  search?: Maybe<Scalars['String']>
  organizationId: Scalars['ID']
  isArchived?: Maybe<Scalars['Boolean']>
  pageInfo?: Maybe<PageInfoInput>
}

export type AllSkuBatchesForCsvQuery = {
  viewer: Maybe<{
    allSkuBatches: Maybe<{
      results: Array<
        {
          inventoryValues: {
            LA: Pick<Values, 'counted' | 'incoming' | 'outgoing' | 'allocated' | 'available'>
            OAK: Pick<Values, 'counted' | 'incoming' | 'outgoing' | 'allocated' | 'available'>
          }
          sku: SkuFragmentFragment
          batch: Maybe<BatchFragmentFragment>
        } & SkuBatchFragmentFragment
      >
    }>
  }>
}

export type CreateSkuBatchMutationVariables = {
  batchId?: Maybe<Scalars['ID']>
  skuId: Scalars['ID']
  titleholderId: Scalars['ID']
}

export type CreateSkuBatchMutation = { createSkuBatch: Maybe<Pick<SkuBatch, 'id'>> }

export type CreateEmptySkuBatchMutationVariables = {
  skuId: Scalars['ID']
}

export type CreateEmptySkuBatchMutation = { createEmptySkuBatch: Maybe<Pick<SkuBatch, 'id'>> }

export type DeleteSkuBatchMutationVariables = {
  id: Scalars['ID']
}

export type DeleteSkuBatchMutation = { deleteSkuBatch: Maybe<Pick<SkuBatch, 'id'>> }

export type RestoreSkuBatchMutationVariables = {
  id: Scalars['ID']
}

export type RestoreSkuBatchMutation = { restoreSkuBatch: Maybe<Pick<SkuBatch, 'id'>> }

export type UpdateSkuBatchMutationVariables = {
  skuBatchId: Scalars['ID']
  skuId: Scalars['ID']
  batchId?: Maybe<Scalars['ID']>
}

export type UpdateSkuBatchMutation = Pick<Mutation, 'updateSkuBatch'>

export type UpdateSkuBatchTitleholderMutationVariables = {
  skuBatchId: Scalars['ID']
  titleholderId: Scalars['ID']
}

export type UpdateSkuBatchTitleholderMutation = { updateSkuBatchTitleholder: Maybe<Pick<SkuBatch, 'id'>> }

export type CreateInvitationMutationVariables = {
  input: CreateOrganizationInviteInput
}

export type CreateInvitationMutation = {
  createOrganizationInvite: Maybe<{
    changedOrganizationInvite: Pick<OrganizationInvite, 'id' | 'email'> & { organization: Pick<Organization, 'id'> }
  }>
}

export type UpdateInvitationMutationVariables = {
  input: UpdateOrganizationInviteInput
}

export type UpdateInvitationMutation = {
  updateOrganizationInvite: Maybe<{
    changedOrganizationInvite: Pick<OrganizationInvite, 'id'> & {
      organization: Pick<Organization, 'id' | 'name' | 'alias'>
      sender: Pick<User, 'id' | 'email'>
    }
  }>
}

export type UserFragmentFragment = Pick<
  User,
  | 'id'
  | 'email'
  | 'firstName'
  | 'lastName'
  | 'address1'
  | 'address2'
  | 'city'
  | 'state'
  | 'zip'
  | 'phone'
  | 'profilePicture'
  | 'isAdmin'
>

export type UpdateUserMutationVariables = {
  input: UpdateUserInput
}

export type UpdateUserMutation = {
  updateUser: Maybe<{
    changedUser: {
      organizationInvites: Array<
        {
          organization: OrganizationFragmentFragment
          sender: UserFragmentFragment
        } & OrganizationInviteFragmentFragment
      >
    } & UserFragmentFragment
  }>
}

export type ValidateTokenMutationVariables = {
  email?: Maybe<Scalars['String']>
  token?: Maybe<Scalars['String']>
}

export type ValidateTokenMutation = { validateToken: Maybe<Pick<LoginUserPayload, 'token'>> }

export type ChangeUserPasswordMutationVariables = {
  password: Scalars['String']
  newPassword: Scalars['String']
  confirmPassword: Scalars['String']
}

export type ChangeUserPasswordMutation = { changeUserPassword: Maybe<Pick<User, 'id'>> }

export type ResetPasswordMutationVariables = {
  input: ResetPasswordInput
}

export type ResetPasswordMutation = { resetPassword: Maybe<Pick<LoginUserPayload, 'token'>> }

export type UpdateUserPreferenceMutationVariables = {
  userId: Scalars['ID']
  organizationId: Scalars['ID']
  receiveOrderUpdateEmails?: Maybe<Scalars['Boolean']>
}

export type UpdateUserPreferenceMutation = {
  updateUserPreference: Maybe<Pick<UserPreference, 'id' | 'receiveOrderUpdateEmails'>>
}

export type MeQueryVariables = {}

export type MeQuery = {
  viewer: Maybe<{
    me: Maybe<
      {
        organizations: Array<{ owner: UserFragmentFragment } & OrganizationFragmentFragment>
        organizationInvites: Array<
          {
            organization: OrganizationFragmentFragment
            sender: UserFragmentFragment
          } & OrganizationInviteFragmentFragment
        >
        assigned: Array<
          { creator: Maybe<UserFragmentFragment>; collectWithOrder: Maybe<Pick<Order, 'id'>> } & OrderFragmentFragment
        >
        userPreferences: Maybe<Array<UserPreferenceFragmentFragment>>
        userRoles: Array<{ role: Maybe<RoleFragmentFragment> } & UserRoleFragmentFragment>
        roles: Array<{ organization: Maybe<OrganizationFragmentFragment> } & RoleFragmentFragment>
      } & UserFragmentFragment
    >
  }>
}

export type UserPreferenceFragmentFragment = Pick<UserPreference, 'id' | 'receiveOrderUpdateEmails'> & {
  organization: OrganizationFragmentFragment
  user: UserFragmentFragment
}

export type CreateUserRoleMutationVariables = {
  input: CreateUserRoleInput
}

export type CreateUserRoleMutation = {
  createUserRole: Maybe<
    Pick<User, 'id' | 'email' | 'phone' | 'firstName' | 'lastName'> & { roles: Array<Pick<Role, 'id'>> }
  >
}

export type AllOutstandingInvoicesQueryVariables = {
  organizationId?: Maybe<Scalars['ID']>
  orderNumber?: Maybe<Scalars['String']>
  startDate?: Maybe<Scalars['String']>
  endDate?: Maybe<Scalars['String']>
  startDueDate?: Maybe<Scalars['String']>
  endDueDate?: Maybe<Scalars['String']>
  pageInfo?: Maybe<PageInfoInput>
  all?: Maybe<Scalars['Boolean']>
  warehouse?: Maybe<Scalars['String']>
  dispensary?: Maybe<Scalars['String']>
  orgName?: Maybe<Scalars['String']>
}

export type AllOutstandingInvoicesQuery = {
  viewer: Maybe<{
    allOutstandingInvoices: Maybe<{
      results: Maybe<
        Array<
          Pick<
            Order,
            | 'id'
            | 'gmv'
            | 'date'
            | 'name'
            | 'number'
            | 'discount'
            | 'exciseTax'
            | 'gmvCollected'
            | 'exciseTaxCollected'
            | 'daysTillPaymentDue'
          > & {
            creator: Maybe<Pick<User, 'id' | 'email' | 'phone' | 'lastName' | 'firstName'>>
            organization: Pick<Organization, 'id' | 'name'>
            site: Pick<Site, 'id' | 'name'>
            collectWithOrder: Maybe<Pick<Order, 'id' | 'number' | 'date'>>
            warehouse: Maybe<
              Pick<Warehouse, 'id'> & {
                site: Pick<Site, 'id'> & { licensedLocation: Maybe<Pick<LicensedLocation, 'id' | 'name'>> }
              }
            >
          }
        >
      >
      pageInfo: Maybe<
        Pick<PageInfo, 'page' | 'numItemsPerPage' | 'totalNumItems' | 'totalNumPages'> & {
          orderBy: Array<Pick<OrderBy, 'attribute' | 'order'>>
        }
      >
    }>
  }>
}

export type InvoiceDataQueryVariables = {
  id: Scalars['ID']
  number?: Maybe<Scalars['String']>
}

export type InvoiceDataQuery = {
  viewer: Maybe<{
    allFutureDropoffsToDispensary: Array<
      Pick<Order, 'id' | 'date' | 'number' | 'manifestGDriveFileId'> & { collectWithOrder: Maybe<Pick<Order, 'id'>> }
    >
    getAllSiteOutstandingInvoices: Maybe<
      Array<
        Maybe<
          Pick<Order, 'id' | 'date' | 'number' | 'manifestGDriveFileId'> & {
            collectWithOrder: Maybe<Pick<Order, 'id'>>
          }
        >
      >
    >
    getSite: Maybe<Pick<Site, 'name'> & { licensedLocation: Maybe<Pick<LicensedLocation, 'name'>> }>
    getOrder: Maybe<Pick<Order, 'id' | 'date' | 'number'>>
  }>
  getOrder: Maybe<
    Pick<Order, 'id' | 'date' | 'number'> & {
      site: { licensedLocation: Maybe<Pick<LicensedLocation, 'name'>> }
      collectWithOrder: Maybe<Pick<Order, 'id'>>
    }
  >
}

export type GetTitleholdersQueryVariables = {
  organizationId: Scalars['ID']
}

export type GetTitleholdersQuery = {
  viewer: Maybe<{
    getTitleholders: Maybe<
      Array<
        Maybe<
          Pick<Titleholder, 'id' | 'type'> & {
            titleholderLicense: Maybe<LicenseFragmentFragment>
            titleholderOrganization: Maybe<{ licenses: Array<LicenseFragmentFragment> } & OrganizationFragmentFragment>
          }
        >
      >
    >
  }>
}

export type RoleFragmentFragment = Pick<Role, 'id' | 'name' | 'description' | 'permissions'>

export type AllRolesQueryVariables = {}

export type AllRolesQuery = { viewer: Maybe<{ allRoles: Array<RoleFragmentFragment> }> }

export type UserRoleFragmentFragment = Pick<UserRole, 'id'>

export type VehicleFragmentFragment = Pick<Vehicle, 'id' | 'name' | 'make' | 'model' | 'licensePlate' | 'year'>

export type CreateVehicleMutationVariables = {
  input: CreateVehicleInput
}

export type CreateVehicleMutation = { createVehicle: Maybe<{ changedVehicle: VehicleFragmentFragment }> }

export type UpdateVehicleMutationVariables = {
  input: UpdateVehicleInput
}

export type UpdateVehicleMutation = { updateVehicle: Maybe<{ changedVehicle: VehicleFragmentFragment }> }

export type DeleteVehicleMutationVariables = {
  input: DeleteVehicleInput
}

export type DeleteVehicleMutation = { deleteVehicle: Maybe<{ changedVehicle: VehicleFragmentFragment }> }

export type WarehouseFragmentFragment = Pick<Warehouse, 'id' | 'isArchived'>

export type AllWarehousesQueryVariables = {}

export type AllWarehousesQuery = {
  viewer: Maybe<{
    allWarehouses: Array<
      { site: { licenses: Array<LicenseFragmentFragment> } & SiteFragmentFragment } & WarehouseFragmentFragment
    >
  }>
}

export const BatchFragmentFragmentDoc = gql`
  fragment batchFragment on Batch {
    id
    code
    testDate
    deletedAt
    status
    expirationDate
    manufacturingDate
    manifestGDriveFileId
    createdAt
  }
`
export const DraftFragmentFragmentDoc = gql`
  fragment draftFragment on Draft {
    id
    body
    createdAt
    updatedAt
  }
`
export const DriverFragmentFragmentDoc = gql`
  fragment driverFragment on Driver {
    id
    firstName
    lastName
    driversLicense
  }
`
export const InventoryItemFragmentFragmentDoc = gql`
  fragment inventoryItemFragment on InventoryItem {
    id
    counted
    allocated
    promised
    storageType
    createdAt
    updatedAt
  }
`
export const InventoryLedgerItemFragmentFragmentDoc = gql`
  fragment inventoryLedgerItemFragment on InventoryLedgerItem {
    id
    transactionType
    startCounted
    endCounted
    notes
    createdAt
    updatedAt
  }
`
export const LicenseFragmentFragmentDoc = gql`
  fragment licenseFragment on License {
    id
    nickname
    category
    type
    licenseNumber
    legalEntityName
    issuanceDate
    expirationDate
    contactName
    contactPhone
    contactEmail
    address1
    address2
    city
    state
    zip
  }
`
export const LicensedLocationFragmentFragmentDoc = gql`
  fragment licensedLocationFragment on LicensedLocation {
    id
    name
    address1
    address2
    city
    state
    zip
    siteCategory
    lat
    lng
    isArchived
  }
`
export const LineItemFragmentFragmentDoc = gql`
  fragment lineItemFragment on LineItem {
    id
    code
    name
    unit
    quantity
    pricePerUnit
    isExciseTaxable
    isSample
    oversold
    discount
  }
`
export const LineItemChangeFragmentFragmentDoc = gql`
  fragment lineItemChangeFragment on LineItemChange {
    id
    isExciseTaxable
    isSample
    lineItemId
    name
    oversold
    pricePerUnit
    quantity
    skuBatchId
    type
    discount
  }
`
export const OrderFragmentFragmentDoc = gql`
  fragment orderFragment on Order {
    action
    adminNotes
    createdAt
    date
    daysTillPaymentDue
    discount
    estimatedArrivalTimeAfter
    estimatedArrivalTimeBefore
    exciseTax
    exciseTaxCollected
    gmv
    gmvCollected
    id
    intaken
    internalNotes
    irn
    isArchived
    manifestGDriveFileId
    invoicesS3FileLink
    name
    notes
    number
    orgLicenseNum
    outtaken
    paymentStatus
    siteLicenseNum
    status
    timeWindow
    warehouseId
    surcharge
    hasUnalterablePaymentTerms
    mustPayPreviousBalance
  }
`
export const OrderSubscriberFragmentFragmentDoc = gql`
  fragment orderSubscriberFragment on OrderSubscriber {
    id
    orderId
    subscriberId
    manifestGDriveFileId
    invoicesS3FileLink
  }
`
export const OrganizationFragmentFragmentDoc = gql`
  fragment organizationFragment on Organization {
    id
    name
    alias
    address1
    address2
    city
    state
    zip
    phone
    doingBusinessAs
    manifestGDriveFolderId
  }
`
export const UserFragmentFragmentDoc = gql`
  fragment userFragment on User {
    id
    email
    firstName
    lastName
    address1
    address2
    city
    state
    zip
    phone
    profilePicture
    isAdmin
  }
`
export const OrganizationInviteFragmentFragmentDoc = gql`
  fragment organizationInviteFragment on OrganizationInvite {
    id
    email
    accepted
    respondedAt
    organization {
      ...organizationFragment
    }
    sender {
      ...userFragment
    }
  }
  ${OrganizationFragmentFragmentDoc}
  ${UserFragmentFragmentDoc}
`
export const PageInfoFragmentFragmentDoc = gql`
  fragment pageInfoFragment on PageInfo {
    totalNumItems
    totalNumPages
  }
`
export const SiteFragmentFragmentDoc = gql`
  fragment siteFragment on Site {
    id
    name
    address1
    address2
    city
    state
    zip
    pocName
    pocPhoneNumber
    pocEmail
    siteCategory
    createdAt
    licensedLocationId
  }
`
export const SkuFragmentFragmentDoc = gql`
  fragment skuFragment on Sku {
    id
    code
    name
    unit
    pricePerUnit
    isArchived
    isExciseTaxable
    totalCount
    totalCountLA
    totalCountOak
    lastUpdate
    inventoryType
    inventoryClass
    inventoryCategory
  }
`
export const SkuBatchFragmentFragmentDoc = gql`
  fragment skuBatchFragment on SkuBatch {
    id
    createdAt
    updatedAt
    deletedAt
    inventoryValues {
      LA {
        allocated
        available
        counted
        incoming
        outgoing
      }
      OAK {
        allocated
        available
        counted
        incoming
        outgoing
      }
    }
  }
`
export const UserPreferenceFragmentFragmentDoc = gql`
  fragment userPreferenceFragment on UserPreference {
    id
    receiveOrderUpdateEmails
    organization {
      ...organizationFragment
    }
    user {
      ...userFragment
    }
  }
  ${OrganizationFragmentFragmentDoc}
  ${UserFragmentFragmentDoc}
`
export const RoleFragmentFragmentDoc = gql`
  fragment roleFragment on Role {
    id
    name
    description
    permissions
  }
`
export const UserRoleFragmentFragmentDoc = gql`
  fragment userRoleFragment on UserRole {
    id
  }
`
export const VehicleFragmentFragmentDoc = gql`
  fragment vehicleFragment on Vehicle {
    id
    name
    make
    model
    licensePlate
    year
  }
`
export const WarehouseFragmentFragmentDoc = gql`
  fragment warehouseFragment on Warehouse {
    id
    isArchived
  }
`
export const AllAdminSkuBatchesForInventoryDocument = gql`
  query AllAdminSkuBatchesForInventory(
    $isArchived: Boolean
    $pageInfo: PageInfoInput
    $organizationId: ID
    $search: String
  ) {
    viewer {
      allAdminSkuBatches(
        isArchived: $isArchived
        pageInfo: $pageInfo
        organizationId: $organizationId
        search: $search
      ) {
        results {
          ...skuBatchFragment
          inventoryValues {
            LA {
              counted
              incoming
              outgoing
              allocated
              available
            }
            OAK {
              counted
              incoming
              outgoing
              allocated
              available
            }
          }
        }
        pageInfo {
          totalNumItems
          totalNumPages
        }
      }
    }
  }
  ${SkuBatchFragmentFragmentDoc}
`
export type AllAdminSkuBatchesForInventoryComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    AllAdminSkuBatchesForInventoryQuery,
    AllAdminSkuBatchesForInventoryQueryVariables
  >,
  'query'
>

export const AllAdminSkuBatchesForInventoryComponent = (props: AllAdminSkuBatchesForInventoryComponentProps) => (
  <ApolloReactComponents.Query<AllAdminSkuBatchesForInventoryQuery, AllAdminSkuBatchesForInventoryQueryVariables>
    query={AllAdminSkuBatchesForInventoryDocument}
    {...props}
  />
)

export type AllAdminSkuBatchesForInventoryProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllAdminSkuBatchesForInventoryQuery,
  AllAdminSkuBatchesForInventoryQueryVariables
> &
  TChildProps
export function withAllAdminSkuBatchesForInventory<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllAdminSkuBatchesForInventoryQuery,
    AllAdminSkuBatchesForInventoryQueryVariables,
    AllAdminSkuBatchesForInventoryProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllAdminSkuBatchesForInventoryQuery,
    AllAdminSkuBatchesForInventoryQueryVariables,
    AllAdminSkuBatchesForInventoryProps<TChildProps>
  >(AllAdminSkuBatchesForInventoryDocument, {
    alias: 'allAdminSkuBatchesForInventory',
    ...operationOptions,
  })
}

/**
 * __useAllAdminSkuBatchesForInventoryQuery__
 *
 * To run a query within a React component, call `useAllAdminSkuBatchesForInventoryQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllAdminSkuBatchesForInventoryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllAdminSkuBatchesForInventoryQuery({
 *   variables: {
 *      isArchived: // value for 'isArchived'
 *      pageInfo: // value for 'pageInfo'
 *      organizationId: // value for 'organizationId'
 *      search: // value for 'search'
 *   },
 * });
 */
export function useAllAdminSkuBatchesForInventoryQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    AllAdminSkuBatchesForInventoryQuery,
    AllAdminSkuBatchesForInventoryQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<AllAdminSkuBatchesForInventoryQuery, AllAdminSkuBatchesForInventoryQueryVariables>(
    AllAdminSkuBatchesForInventoryDocument,
    baseOptions,
  )
}
export function useAllAdminSkuBatchesForInventoryLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    AllAdminSkuBatchesForInventoryQuery,
    AllAdminSkuBatchesForInventoryQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<
    AllAdminSkuBatchesForInventoryQuery,
    AllAdminSkuBatchesForInventoryQueryVariables
  >(AllAdminSkuBatchesForInventoryDocument, baseOptions)
}
export type AllAdminSkuBatchesForInventoryQueryHookResult = ReturnType<typeof useAllAdminSkuBatchesForInventoryQuery>
export type AllAdminSkuBatchesForInventoryLazyQueryHookResult = ReturnType<
  typeof useAllAdminSkuBatchesForInventoryLazyQuery
>
export type AllAdminSkuBatchesForInventoryQueryResult = ApolloReactCommon.QueryResult<
  AllAdminSkuBatchesForInventoryQuery,
  AllAdminSkuBatchesForInventoryQueryVariables
>
export const SignInDocument = gql`
  mutation SignIn($input: LoginUserInput!) {
    loginUser(input: $input) {
      token
      user {
        ...userFragment
      }
    }
  }
  ${UserFragmentFragmentDoc}
`
export type SignInMutationFn = ApolloReactCommon.MutationFunction<SignInMutation, SignInMutationVariables>
export type SignInComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<SignInMutation, SignInMutationVariables>,
  'mutation'
>

export const SignInComponent = (props: SignInComponentProps) => (
  <ApolloReactComponents.Mutation<SignInMutation, SignInMutationVariables> mutation={SignInDocument} {...props} />
)

export type SignInProps<TChildProps = {}> = ApolloReactHoc.MutateProps<SignInMutation, SignInMutationVariables> &
  TChildProps
export function withSignIn<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    SignInMutation,
    SignInMutationVariables,
    SignInProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<TProps, SignInMutation, SignInMutationVariables, SignInProps<TChildProps>>(
    SignInDocument,
    {
      alias: 'signIn',
      ...operationOptions,
    },
  )
}

/**
 * __useSignInMutation__
 *
 * To run a mutation, you first call `useSignInMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignInMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signInMutation, { data, loading, error }] = useSignInMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSignInMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<SignInMutation, SignInMutationVariables>,
) {
  return ApolloReactHooks.useMutation<SignInMutation, SignInMutationVariables>(SignInDocument, baseOptions)
}
export type SignInMutationHookResult = ReturnType<typeof useSignInMutation>
export type SignInMutationResult = ApolloReactCommon.MutationResult<SignInMutation>
export type SignInMutationOptions = ApolloReactCommon.BaseMutationOptions<SignInMutation, SignInMutationVariables>
export const SignUpDocument = gql`
  mutation SignUp($input: CreateUserInput!) {
    createUser(input: $input) {
      token
      changedUser {
        ...userFragment
      }
    }
  }
  ${UserFragmentFragmentDoc}
`
export type SignUpMutationFn = ApolloReactCommon.MutationFunction<SignUpMutation, SignUpMutationVariables>
export type SignUpComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<SignUpMutation, SignUpMutationVariables>,
  'mutation'
>

export const SignUpComponent = (props: SignUpComponentProps) => (
  <ApolloReactComponents.Mutation<SignUpMutation, SignUpMutationVariables> mutation={SignUpDocument} {...props} />
)

export type SignUpProps<TChildProps = {}> = ApolloReactHoc.MutateProps<SignUpMutation, SignUpMutationVariables> &
  TChildProps
export function withSignUp<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    SignUpMutation,
    SignUpMutationVariables,
    SignUpProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<TProps, SignUpMutation, SignUpMutationVariables, SignUpProps<TChildProps>>(
    SignUpDocument,
    {
      alias: 'signUp',
      ...operationOptions,
    },
  )
}

/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSignUpMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<SignUpMutation, SignUpMutationVariables>,
) {
  return ApolloReactHooks.useMutation<SignUpMutation, SignUpMutationVariables>(SignUpDocument, baseOptions)
}
export type SignUpMutationHookResult = ReturnType<typeof useSignUpMutation>
export type SignUpMutationResult = ApolloReactCommon.MutationResult<SignUpMutation>
export type SignUpMutationOptions = ApolloReactCommon.BaseMutationOptions<SignUpMutation, SignUpMutationVariables>
export const CreateBatchDocument = gql`
  mutation CreateBatch($input: CreateBatchInput!) {
    createBatch(input: $input) {
      ...batchFragment
    }
  }
  ${BatchFragmentFragmentDoc}
`
export type CreateBatchMutationFn = ApolloReactCommon.MutationFunction<
  CreateBatchMutation,
  CreateBatchMutationVariables
>
export type CreateBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateBatchMutation, CreateBatchMutationVariables>,
  'mutation'
>

export const CreateBatchComponent = (props: CreateBatchComponentProps) => (
  <ApolloReactComponents.Mutation<CreateBatchMutation, CreateBatchMutationVariables>
    mutation={CreateBatchDocument}
    {...props}
  />
)

export type CreateBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateBatchMutation,
  CreateBatchMutationVariables
> &
  TChildProps
export function withCreateBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateBatchMutation,
    CreateBatchMutationVariables,
    CreateBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateBatchMutation,
    CreateBatchMutationVariables,
    CreateBatchProps<TChildProps>
  >(CreateBatchDocument, {
    alias: 'createBatch',
    ...operationOptions,
  })
}

/**
 * __useCreateBatchMutation__
 *
 * To run a mutation, you first call `useCreateBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createBatchMutation, { data, loading, error }] = useCreateBatchMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateBatchMutation, CreateBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateBatchMutation, CreateBatchMutationVariables>(
    CreateBatchDocument,
    baseOptions,
  )
}
export type CreateBatchMutationHookResult = ReturnType<typeof useCreateBatchMutation>
export type CreateBatchMutationResult = ApolloReactCommon.MutationResult<CreateBatchMutation>
export type CreateBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateBatchMutation,
  CreateBatchMutationVariables
>
export const EditBatchDocument = gql`
  mutation EditBatch($input: UpdateBatchInput!) {
    editBatch(input: $input)
  }
`
export type EditBatchMutationFn = ApolloReactCommon.MutationFunction<EditBatchMutation, EditBatchMutationVariables>
export type EditBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<EditBatchMutation, EditBatchMutationVariables>,
  'mutation'
>

export const EditBatchComponent = (props: EditBatchComponentProps) => (
  <ApolloReactComponents.Mutation<EditBatchMutation, EditBatchMutationVariables>
    mutation={EditBatchDocument}
    {...props}
  />
)

export type EditBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  EditBatchMutation,
  EditBatchMutationVariables
> &
  TChildProps
export function withEditBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    EditBatchMutation,
    EditBatchMutationVariables,
    EditBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    EditBatchMutation,
    EditBatchMutationVariables,
    EditBatchProps<TChildProps>
  >(EditBatchDocument, {
    alias: 'editBatch',
    ...operationOptions,
  })
}

/**
 * __useEditBatchMutation__
 *
 * To run a mutation, you first call `useEditBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editBatchMutation, { data, loading, error }] = useEditBatchMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useEditBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<EditBatchMutation, EditBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<EditBatchMutation, EditBatchMutationVariables>(EditBatchDocument, baseOptions)
}
export type EditBatchMutationHookResult = ReturnType<typeof useEditBatchMutation>
export type EditBatchMutationResult = ApolloReactCommon.MutationResult<EditBatchMutation>
export type EditBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  EditBatchMutation,
  EditBatchMutationVariables
>
export const BulkUpdateBatchStatusDocument = gql`
  mutation BulkUpdateBatchStatus($batches: [UpdateBatchInput]!, $targetStatus: BatchStatusEnum) {
    bulkUpdateBatchStatus(batches: $batches, targetStatus: $targetStatus)
  }
`
export type BulkUpdateBatchStatusMutationFn = ApolloReactCommon.MutationFunction<
  BulkUpdateBatchStatusMutation,
  BulkUpdateBatchStatusMutationVariables
>
export type BulkUpdateBatchStatusComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<BulkUpdateBatchStatusMutation, BulkUpdateBatchStatusMutationVariables>,
  'mutation'
>

export const BulkUpdateBatchStatusComponent = (props: BulkUpdateBatchStatusComponentProps) => (
  <ApolloReactComponents.Mutation<BulkUpdateBatchStatusMutation, BulkUpdateBatchStatusMutationVariables>
    mutation={BulkUpdateBatchStatusDocument}
    {...props}
  />
)

export type BulkUpdateBatchStatusProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  BulkUpdateBatchStatusMutation,
  BulkUpdateBatchStatusMutationVariables
> &
  TChildProps
export function withBulkUpdateBatchStatus<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    BulkUpdateBatchStatusMutation,
    BulkUpdateBatchStatusMutationVariables,
    BulkUpdateBatchStatusProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    BulkUpdateBatchStatusMutation,
    BulkUpdateBatchStatusMutationVariables,
    BulkUpdateBatchStatusProps<TChildProps>
  >(BulkUpdateBatchStatusDocument, {
    alias: 'bulkUpdateBatchStatus',
    ...operationOptions,
  })
}

/**
 * __useBulkUpdateBatchStatusMutation__
 *
 * To run a mutation, you first call `useBulkUpdateBatchStatusMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useBulkUpdateBatchStatusMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [bulkUpdateBatchStatusMutation, { data, loading, error }] = useBulkUpdateBatchStatusMutation({
 *   variables: {
 *      batches: // value for 'batches'
 *      targetStatus: // value for 'targetStatus'
 *   },
 * });
 */
export function useBulkUpdateBatchStatusMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    BulkUpdateBatchStatusMutation,
    BulkUpdateBatchStatusMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<BulkUpdateBatchStatusMutation, BulkUpdateBatchStatusMutationVariables>(
    BulkUpdateBatchStatusDocument,
    baseOptions,
  )
}
export type BulkUpdateBatchStatusMutationHookResult = ReturnType<typeof useBulkUpdateBatchStatusMutation>
export type BulkUpdateBatchStatusMutationResult = ApolloReactCommon.MutationResult<BulkUpdateBatchStatusMutation>
export type BulkUpdateBatchStatusMutationOptions = ApolloReactCommon.BaseMutationOptions<
  BulkUpdateBatchStatusMutation,
  BulkUpdateBatchStatusMutationVariables
>
export const DeleteBatchDocument = gql`
  mutation DeleteBatch($id: ID!) {
    deleteBatch(id: $id)
  }
`
export type DeleteBatchMutationFn = ApolloReactCommon.MutationFunction<
  DeleteBatchMutation,
  DeleteBatchMutationVariables
>
export type DeleteBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteBatchMutation, DeleteBatchMutationVariables>,
  'mutation'
>

export const DeleteBatchComponent = (props: DeleteBatchComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteBatchMutation, DeleteBatchMutationVariables>
    mutation={DeleteBatchDocument}
    {...props}
  />
)

export type DeleteBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteBatchMutation,
  DeleteBatchMutationVariables
> &
  TChildProps
export function withDeleteBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteBatchMutation,
    DeleteBatchMutationVariables,
    DeleteBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteBatchMutation,
    DeleteBatchMutationVariables,
    DeleteBatchProps<TChildProps>
  >(DeleteBatchDocument, {
    alias: 'deleteBatch',
    ...operationOptions,
  })
}

/**
 * __useDeleteBatchMutation__
 *
 * To run a mutation, you first call `useDeleteBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteBatchMutation, { data, loading, error }] = useDeleteBatchMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteBatchMutation, DeleteBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteBatchMutation, DeleteBatchMutationVariables>(
    DeleteBatchDocument,
    baseOptions,
  )
}
export type DeleteBatchMutationHookResult = ReturnType<typeof useDeleteBatchMutation>
export type DeleteBatchMutationResult = ApolloReactCommon.MutationResult<DeleteBatchMutation>
export type DeleteBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteBatchMutation,
  DeleteBatchMutationVariables
>
export const UploadBatchFileDocument = gql`
  mutation UploadBatchFile($inputFile: Upload!, $id: ID!) {
    uploadBatchFile(inputFile: $inputFile, organizationId: $id)
  }
`
export type UploadBatchFileMutationFn = ApolloReactCommon.MutationFunction<
  UploadBatchFileMutation,
  UploadBatchFileMutationVariables
>
export type UploadBatchFileComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UploadBatchFileMutation, UploadBatchFileMutationVariables>,
  'mutation'
>

export const UploadBatchFileComponent = (props: UploadBatchFileComponentProps) => (
  <ApolloReactComponents.Mutation<UploadBatchFileMutation, UploadBatchFileMutationVariables>
    mutation={UploadBatchFileDocument}
    {...props}
  />
)

export type UploadBatchFileProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UploadBatchFileMutation,
  UploadBatchFileMutationVariables
> &
  TChildProps
export function withUploadBatchFile<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UploadBatchFileMutation,
    UploadBatchFileMutationVariables,
    UploadBatchFileProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UploadBatchFileMutation,
    UploadBatchFileMutationVariables,
    UploadBatchFileProps<TChildProps>
  >(UploadBatchFileDocument, {
    alias: 'uploadBatchFile',
    ...operationOptions,
  })
}

/**
 * __useUploadBatchFileMutation__
 *
 * To run a mutation, you first call `useUploadBatchFileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadBatchFileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadBatchFileMutation, { data, loading, error }] = useUploadBatchFileMutation({
 *   variables: {
 *      inputFile: // value for 'inputFile'
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUploadBatchFileMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UploadBatchFileMutation, UploadBatchFileMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UploadBatchFileMutation, UploadBatchFileMutationVariables>(
    UploadBatchFileDocument,
    baseOptions,
  )
}
export type UploadBatchFileMutationHookResult = ReturnType<typeof useUploadBatchFileMutation>
export type UploadBatchFileMutationResult = ApolloReactCommon.MutationResult<UploadBatchFileMutation>
export type UploadBatchFileMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UploadBatchFileMutation,
  UploadBatchFileMutationVariables
>
export const RestoreBatchDocument = gql`
  mutation RestoreBatch($batchId: ID!) {
    restoreBatch(batchId: $batchId) {
      id
    }
  }
`
export type RestoreBatchMutationFn = ApolloReactCommon.MutationFunction<
  RestoreBatchMutation,
  RestoreBatchMutationVariables
>
export type RestoreBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<RestoreBatchMutation, RestoreBatchMutationVariables>,
  'mutation'
>

export const RestoreBatchComponent = (props: RestoreBatchComponentProps) => (
  <ApolloReactComponents.Mutation<RestoreBatchMutation, RestoreBatchMutationVariables>
    mutation={RestoreBatchDocument}
    {...props}
  />
)

export type RestoreBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  RestoreBatchMutation,
  RestoreBatchMutationVariables
> &
  TChildProps
export function withRestoreBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    RestoreBatchMutation,
    RestoreBatchMutationVariables,
    RestoreBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    RestoreBatchMutation,
    RestoreBatchMutationVariables,
    RestoreBatchProps<TChildProps>
  >(RestoreBatchDocument, {
    alias: 'restoreBatch',
    ...operationOptions,
  })
}

/**
 * __useRestoreBatchMutation__
 *
 * To run a mutation, you first call `useRestoreBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRestoreBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [restoreBatchMutation, { data, loading, error }] = useRestoreBatchMutation({
 *   variables: {
 *      batchId: // value for 'batchId'
 *   },
 * });
 */
export function useRestoreBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<RestoreBatchMutation, RestoreBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<RestoreBatchMutation, RestoreBatchMutationVariables>(
    RestoreBatchDocument,
    baseOptions,
  )
}
export type RestoreBatchMutationHookResult = ReturnType<typeof useRestoreBatchMutation>
export type RestoreBatchMutationResult = ApolloReactCommon.MutationResult<RestoreBatchMutation>
export type RestoreBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  RestoreBatchMutation,
  RestoreBatchMutationVariables
>
export const AllBatchesDocument = gql`
  query AllBatches($organizationId: ID!, $pageInfo: PageInfoInput) {
    viewer {
      allBatches(organizationId: $organizationId, pageInfo: $pageInfo) {
        results {
          id
          code
          manifestGDriveFileId
          manufacturingDate
          expirationDate
          status
          organization {
            id
          }
          license {
            ...licenseFragment
          }
        }
      }
    }
  }
  ${LicenseFragmentFragmentDoc}
`
export type AllBatchesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllBatchesQuery, AllBatchesQueryVariables>,
  'query'
> &
  ({ variables: AllBatchesQueryVariables; skip?: boolean } | { skip: boolean })

export const AllBatchesComponent = (props: AllBatchesComponentProps) => (
  <ApolloReactComponents.Query<AllBatchesQuery, AllBatchesQueryVariables> query={AllBatchesDocument} {...props} />
)

export type AllBatchesProps<TChildProps = {}> = ApolloReactHoc.DataProps<AllBatchesQuery, AllBatchesQueryVariables> &
  TChildProps
export function withAllBatches<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllBatchesQuery,
    AllBatchesQueryVariables,
    AllBatchesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, AllBatchesQuery, AllBatchesQueryVariables, AllBatchesProps<TChildProps>>(
    AllBatchesDocument,
    {
      alias: 'allBatches',
      ...operationOptions,
    },
  )
}

/**
 * __useAllBatchesQuery__
 *
 * To run a query within a React component, call `useAllBatchesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllBatchesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllBatchesQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useAllBatchesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllBatchesQuery, AllBatchesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllBatchesQuery, AllBatchesQueryVariables>(AllBatchesDocument, baseOptions)
}
export function useAllBatchesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllBatchesQuery, AllBatchesQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllBatchesQuery, AllBatchesQueryVariables>(AllBatchesDocument, baseOptions)
}
export type AllBatchesQueryHookResult = ReturnType<typeof useAllBatchesQuery>
export type AllBatchesLazyQueryHookResult = ReturnType<typeof useAllBatchesLazyQuery>
export type AllBatchesQueryResult = ApolloReactCommon.QueryResult<AllBatchesQuery, AllBatchesQueryVariables>
export const GetBatchDocument = gql`
  query GetBatch($id: ID!) {
    getBatch(id: $id) {
      id
      code
      manifestGDriveFileId
      manufacturingDate
      expirationDate
      status
      organization {
        id
      }
      license {
        ...licenseFragment
      }
    }
  }
  ${LicenseFragmentFragmentDoc}
`
export type GetBatchComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetBatchQuery, GetBatchQueryVariables>,
  'query'
> &
  ({ variables: GetBatchQueryVariables; skip?: boolean } | { skip: boolean })

export const GetBatchComponent = (props: GetBatchComponentProps) => (
  <ApolloReactComponents.Query<GetBatchQuery, GetBatchQueryVariables> query={GetBatchDocument} {...props} />
)

export type GetBatchProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetBatchQuery, GetBatchQueryVariables> &
  TChildProps
export function withGetBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetBatchQuery,
    GetBatchQueryVariables,
    GetBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, GetBatchQuery, GetBatchQueryVariables, GetBatchProps<TChildProps>>(
    GetBatchDocument,
    {
      alias: 'getBatch',
      ...operationOptions,
    },
  )
}

/**
 * __useGetBatchQuery__
 *
 * To run a query within a React component, call `useGetBatchQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetBatchQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetBatchQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetBatchQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<GetBatchQuery, GetBatchQueryVariables>,
) {
  return ApolloReactHooks.useQuery<GetBatchQuery, GetBatchQueryVariables>(GetBatchDocument, baseOptions)
}
export function useGetBatchLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetBatchQuery, GetBatchQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetBatchQuery, GetBatchQueryVariables>(GetBatchDocument, baseOptions)
}
export type GetBatchQueryHookResult = ReturnType<typeof useGetBatchQuery>
export type GetBatchLazyQueryHookResult = ReturnType<typeof useGetBatchLazyQuery>
export type GetBatchQueryResult = ApolloReactCommon.QueryResult<GetBatchQuery, GetBatchQueryVariables>
export const InventoryBatchesTabDocument = gql`
  query InventoryBatchesTab($isArchived: Boolean, $organizationId: ID, $search: String, $pageInfo: PageInfoInput) {
    viewer {
      allBatches(
        isArchived: $isArchived
        organizationId: $organizationId
        search: $search
        pageInfo: $pageInfo
        isInventoryTab: true
      ) {
        results {
          id
          code
          manifestGDriveFileId
          manufacturingDate
          expirationDate
          status
          organization {
            id
            name
          }
          license {
            ...licenseFragment
          }
          skuBatches {
            id
            sku {
              id
              code
              name
            }
            inventoryValues {
              LA {
                counted
                allocated
                available
              }
              OAK {
                counted
                allocated
                available
              }
            }
          }
        }
        pageInfo {
          totalNumItems
          totalNumPages
        }
      }
    }
  }
  ${LicenseFragmentFragmentDoc}
`
export type InventoryBatchesTabComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<InventoryBatchesTabQuery, InventoryBatchesTabQueryVariables>,
  'query'
>

export const InventoryBatchesTabComponent = (props: InventoryBatchesTabComponentProps) => (
  <ApolloReactComponents.Query<InventoryBatchesTabQuery, InventoryBatchesTabQueryVariables>
    query={InventoryBatchesTabDocument}
    {...props}
  />
)

export type InventoryBatchesTabProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  InventoryBatchesTabQuery,
  InventoryBatchesTabQueryVariables
> &
  TChildProps
export function withInventoryBatchesTab<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    InventoryBatchesTabQuery,
    InventoryBatchesTabQueryVariables,
    InventoryBatchesTabProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    InventoryBatchesTabQuery,
    InventoryBatchesTabQueryVariables,
    InventoryBatchesTabProps<TChildProps>
  >(InventoryBatchesTabDocument, {
    alias: 'inventoryBatchesTab',
    ...operationOptions,
  })
}

/**
 * __useInventoryBatchesTabQuery__
 *
 * To run a query within a React component, call `useInventoryBatchesTabQuery` and pass it any options that fit your needs.
 * When your component renders, `useInventoryBatchesTabQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInventoryBatchesTabQuery({
 *   variables: {
 *      isArchived: // value for 'isArchived'
 *      organizationId: // value for 'organizationId'
 *      search: // value for 'search'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useInventoryBatchesTabQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<InventoryBatchesTabQuery, InventoryBatchesTabQueryVariables>,
) {
  return ApolloReactHooks.useQuery<InventoryBatchesTabQuery, InventoryBatchesTabQueryVariables>(
    InventoryBatchesTabDocument,
    baseOptions,
  )
}
export function useInventoryBatchesTabLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<InventoryBatchesTabQuery, InventoryBatchesTabQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<InventoryBatchesTabQuery, InventoryBatchesTabQueryVariables>(
    InventoryBatchesTabDocument,
    baseOptions,
  )
}
export type InventoryBatchesTabQueryHookResult = ReturnType<typeof useInventoryBatchesTabQuery>
export type InventoryBatchesTabLazyQueryHookResult = ReturnType<typeof useInventoryBatchesTabLazyQuery>
export type InventoryBatchesTabQueryResult = ApolloReactCommon.QueryResult<
  InventoryBatchesTabQuery,
  InventoryBatchesTabQueryVariables
>
export const AllBatchesForSkuBatchModalDocument = gql`
  query AllBatchesForSkuBatchModal($organizationId: ID!) {
    viewer {
      allBatches(
        organizationId: $organizationId
        isArchived: false
        pageInfo: { page: 1, numItemsPerPage: 5000, orderBy: [{ attribute: "code", order: ASC }] }
      ) {
        results {
          id
          code
          expirationDate
          manufacturingDate
          manifestGDriveFileId
        }
      }
    }
  }
`
export type AllBatchesForSkuBatchModalComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<
    AllBatchesForSkuBatchModalQuery,
    AllBatchesForSkuBatchModalQueryVariables
  >,
  'query'
> &
  ({ variables: AllBatchesForSkuBatchModalQueryVariables; skip?: boolean } | { skip: boolean })

export const AllBatchesForSkuBatchModalComponent = (props: AllBatchesForSkuBatchModalComponentProps) => (
  <ApolloReactComponents.Query<AllBatchesForSkuBatchModalQuery, AllBatchesForSkuBatchModalQueryVariables>
    query={AllBatchesForSkuBatchModalDocument}
    {...props}
  />
)

export type AllBatchesForSkuBatchModalProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllBatchesForSkuBatchModalQuery,
  AllBatchesForSkuBatchModalQueryVariables
> &
  TChildProps
export function withAllBatchesForSkuBatchModal<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllBatchesForSkuBatchModalQuery,
    AllBatchesForSkuBatchModalQueryVariables,
    AllBatchesForSkuBatchModalProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllBatchesForSkuBatchModalQuery,
    AllBatchesForSkuBatchModalQueryVariables,
    AllBatchesForSkuBatchModalProps<TChildProps>
  >(AllBatchesForSkuBatchModalDocument, {
    alias: 'allBatchesForSkuBatchModal',
    ...operationOptions,
  })
}

/**
 * __useAllBatchesForSkuBatchModalQuery__
 *
 * To run a query within a React component, call `useAllBatchesForSkuBatchModalQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllBatchesForSkuBatchModalQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllBatchesForSkuBatchModalQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useAllBatchesForSkuBatchModalQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    AllBatchesForSkuBatchModalQuery,
    AllBatchesForSkuBatchModalQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<AllBatchesForSkuBatchModalQuery, AllBatchesForSkuBatchModalQueryVariables>(
    AllBatchesForSkuBatchModalDocument,
    baseOptions,
  )
}
export function useAllBatchesForSkuBatchModalLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    AllBatchesForSkuBatchModalQuery,
    AllBatchesForSkuBatchModalQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<AllBatchesForSkuBatchModalQuery, AllBatchesForSkuBatchModalQueryVariables>(
    AllBatchesForSkuBatchModalDocument,
    baseOptions,
  )
}
export type AllBatchesForSkuBatchModalQueryHookResult = ReturnType<typeof useAllBatchesForSkuBatchModalQuery>
export type AllBatchesForSkuBatchModalLazyQueryHookResult = ReturnType<typeof useAllBatchesForSkuBatchModalLazyQuery>
export type AllBatchesForSkuBatchModalQueryResult = ApolloReactCommon.QueryResult<
  AllBatchesForSkuBatchModalQuery,
  AllBatchesForSkuBatchModalQueryVariables
>
export const GetUnavailableDatesDocument = gql`
  query getUnavailableDates {
    getUnavailableDates
  }
`
export type GetUnavailableDatesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetUnavailableDatesQuery, GetUnavailableDatesQueryVariables>,
  'query'
>

export const GetUnavailableDatesComponent = (props: GetUnavailableDatesComponentProps) => (
  <ApolloReactComponents.Query<GetUnavailableDatesQuery, GetUnavailableDatesQueryVariables>
    query={GetUnavailableDatesDocument}
    {...props}
  />
)

export type GetUnavailableDatesProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  GetUnavailableDatesQuery,
  GetUnavailableDatesQueryVariables
> &
  TChildProps
export function withGetUnavailableDates<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetUnavailableDatesQuery,
    GetUnavailableDatesQueryVariables,
    GetUnavailableDatesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    GetUnavailableDatesQuery,
    GetUnavailableDatesQueryVariables,
    GetUnavailableDatesProps<TChildProps>
  >(GetUnavailableDatesDocument, {
    alias: 'getUnavailableDates',
    ...operationOptions,
  })
}

/**
 * __useGetUnavailableDatesQuery__
 *
 * To run a query within a React component, call `useGetUnavailableDatesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUnavailableDatesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUnavailableDatesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetUnavailableDatesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<GetUnavailableDatesQuery, GetUnavailableDatesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<GetUnavailableDatesQuery, GetUnavailableDatesQueryVariables>(
    GetUnavailableDatesDocument,
    baseOptions,
  )
}
export function useGetUnavailableDatesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetUnavailableDatesQuery, GetUnavailableDatesQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetUnavailableDatesQuery, GetUnavailableDatesQueryVariables>(
    GetUnavailableDatesDocument,
    baseOptions,
  )
}
export type GetUnavailableDatesQueryHookResult = ReturnType<typeof useGetUnavailableDatesQuery>
export type GetUnavailableDatesLazyQueryHookResult = ReturnType<typeof useGetUnavailableDatesLazyQuery>
export type GetUnavailableDatesQueryResult = ApolloReactCommon.QueryResult<
  GetUnavailableDatesQuery,
  GetUnavailableDatesQueryVariables
>
export const AllDraftsDocument = gql`
  query AllDrafts($organizationId: ID!, $pageInfo: PageInfoInput) {
    viewer {
      me {
        id
        drafts {
          id
          body
          updatedAt
          createdAt
        }
      }
      allDrafts(organizationId: $organizationId, pageInfo: $pageInfo) {
        results {
          ...draftFragment
        }
        pageInfo {
          totalNumItems
          totalNumPages
          page
        }
      }
    }
  }
  ${DraftFragmentFragmentDoc}
`
export type AllDraftsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllDraftsQuery, AllDraftsQueryVariables>,
  'query'
> &
  ({ variables: AllDraftsQueryVariables; skip?: boolean } | { skip: boolean })

export const AllDraftsComponent = (props: AllDraftsComponentProps) => (
  <ApolloReactComponents.Query<AllDraftsQuery, AllDraftsQueryVariables> query={AllDraftsDocument} {...props} />
)

export type AllDraftsProps<TChildProps = {}> = ApolloReactHoc.DataProps<AllDraftsQuery, AllDraftsQueryVariables> &
  TChildProps
export function withAllDrafts<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllDraftsQuery,
    AllDraftsQueryVariables,
    AllDraftsProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, AllDraftsQuery, AllDraftsQueryVariables, AllDraftsProps<TChildProps>>(
    AllDraftsDocument,
    {
      alias: 'allDrafts',
      ...operationOptions,
    },
  )
}

/**
 * __useAllDraftsQuery__
 *
 * To run a query within a React component, call `useAllDraftsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllDraftsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllDraftsQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useAllDraftsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllDraftsQuery, AllDraftsQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllDraftsQuery, AllDraftsQueryVariables>(AllDraftsDocument, baseOptions)
}
export function useAllDraftsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllDraftsQuery, AllDraftsQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllDraftsQuery, AllDraftsQueryVariables>(AllDraftsDocument, baseOptions)
}
export type AllDraftsQueryHookResult = ReturnType<typeof useAllDraftsQuery>
export type AllDraftsLazyQueryHookResult = ReturnType<typeof useAllDraftsLazyQuery>
export type AllDraftsQueryResult = ApolloReactCommon.QueryResult<AllDraftsQuery, AllDraftsQueryVariables>
export const GetDraftDocument = gql`
  query GetDraft($id: ID!) {
    getDraft(id: $id) {
      ...draftFragment
    }
  }
  ${DraftFragmentFragmentDoc}
`
export type GetDraftComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetDraftQuery, GetDraftQueryVariables>,
  'query'
> &
  ({ variables: GetDraftQueryVariables; skip?: boolean } | { skip: boolean })

export const GetDraftComponent = (props: GetDraftComponentProps) => (
  <ApolloReactComponents.Query<GetDraftQuery, GetDraftQueryVariables> query={GetDraftDocument} {...props} />
)

export type GetDraftProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetDraftQuery, GetDraftQueryVariables> &
  TChildProps
export function withGetDraft<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetDraftQuery,
    GetDraftQueryVariables,
    GetDraftProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, GetDraftQuery, GetDraftQueryVariables, GetDraftProps<TChildProps>>(
    GetDraftDocument,
    {
      alias: 'getDraft',
      ...operationOptions,
    },
  )
}

/**
 * __useGetDraftQuery__
 *
 * To run a query within a React component, call `useGetDraftQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetDraftQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetDraftQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetDraftQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<GetDraftQuery, GetDraftQueryVariables>,
) {
  return ApolloReactHooks.useQuery<GetDraftQuery, GetDraftQueryVariables>(GetDraftDocument, baseOptions)
}
export function useGetDraftLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetDraftQuery, GetDraftQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetDraftQuery, GetDraftQueryVariables>(GetDraftDocument, baseOptions)
}
export type GetDraftQueryHookResult = ReturnType<typeof useGetDraftQuery>
export type GetDraftLazyQueryHookResult = ReturnType<typeof useGetDraftLazyQuery>
export type GetDraftQueryResult = ApolloReactCommon.QueryResult<GetDraftQuery, GetDraftQueryVariables>
export const CreateDraftDocument = gql`
  mutation CreateDraft($input: CreateDraftInput!) {
    createDraft(input: $input) {
      createdDraft {
        id
      }
    }
  }
`
export type CreateDraftMutationFn = ApolloReactCommon.MutationFunction<
  CreateDraftMutation,
  CreateDraftMutationVariables
>
export type CreateDraftComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateDraftMutation, CreateDraftMutationVariables>,
  'mutation'
>

export const CreateDraftComponent = (props: CreateDraftComponentProps) => (
  <ApolloReactComponents.Mutation<CreateDraftMutation, CreateDraftMutationVariables>
    mutation={CreateDraftDocument}
    {...props}
  />
)

export type CreateDraftProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateDraftMutation,
  CreateDraftMutationVariables
> &
  TChildProps
export function withCreateDraft<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateDraftMutation,
    CreateDraftMutationVariables,
    CreateDraftProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateDraftMutation,
    CreateDraftMutationVariables,
    CreateDraftProps<TChildProps>
  >(CreateDraftDocument, {
    alias: 'createDraft',
    ...operationOptions,
  })
}

/**
 * __useCreateDraftMutation__
 *
 * To run a mutation, you first call `useCreateDraftMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateDraftMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createDraftMutation, { data, loading, error }] = useCreateDraftMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateDraftMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateDraftMutation, CreateDraftMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateDraftMutation, CreateDraftMutationVariables>(
    CreateDraftDocument,
    baseOptions,
  )
}
export type CreateDraftMutationHookResult = ReturnType<typeof useCreateDraftMutation>
export type CreateDraftMutationResult = ApolloReactCommon.MutationResult<CreateDraftMutation>
export type CreateDraftMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateDraftMutation,
  CreateDraftMutationVariables
>
export const UpdateDraftDocument = gql`
  mutation UpdateDraft($input: UpdateDraftInput!) {
    updateDraft(input: $input) {
      changedDraft {
        id
      }
    }
  }
`
export type UpdateDraftMutationFn = ApolloReactCommon.MutationFunction<
  UpdateDraftMutation,
  UpdateDraftMutationVariables
>
export type UpdateDraftComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateDraftMutation, UpdateDraftMutationVariables>,
  'mutation'
>

export const UpdateDraftComponent = (props: UpdateDraftComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateDraftMutation, UpdateDraftMutationVariables>
    mutation={UpdateDraftDocument}
    {...props}
  />
)

export type UpdateDraftProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateDraftMutation,
  UpdateDraftMutationVariables
> &
  TChildProps
export function withUpdateDraft<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateDraftMutation,
    UpdateDraftMutationVariables,
    UpdateDraftProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateDraftMutation,
    UpdateDraftMutationVariables,
    UpdateDraftProps<TChildProps>
  >(UpdateDraftDocument, {
    alias: 'updateDraft',
    ...operationOptions,
  })
}

/**
 * __useUpdateDraftMutation__
 *
 * To run a mutation, you first call `useUpdateDraftMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateDraftMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateDraftMutation, { data, loading, error }] = useUpdateDraftMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateDraftMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateDraftMutation, UpdateDraftMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateDraftMutation, UpdateDraftMutationVariables>(
    UpdateDraftDocument,
    baseOptions,
  )
}
export type UpdateDraftMutationHookResult = ReturnType<typeof useUpdateDraftMutation>
export type UpdateDraftMutationResult = ApolloReactCommon.MutationResult<UpdateDraftMutation>
export type UpdateDraftMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateDraftMutation,
  UpdateDraftMutationVariables
>
export const DeleteDraftDocument = gql`
  mutation DeleteDraft($input: DeleteDraftInput!) {
    deleteDraft(input: $input) {
      deletedDraft {
        id
      }
    }
  }
`
export type DeleteDraftMutationFn = ApolloReactCommon.MutationFunction<
  DeleteDraftMutation,
  DeleteDraftMutationVariables
>
export type DeleteDraftComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteDraftMutation, DeleteDraftMutationVariables>,
  'mutation'
>

export const DeleteDraftComponent = (props: DeleteDraftComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteDraftMutation, DeleteDraftMutationVariables>
    mutation={DeleteDraftDocument}
    {...props}
  />
)

export type DeleteDraftProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteDraftMutation,
  DeleteDraftMutationVariables
> &
  TChildProps
export function withDeleteDraft<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteDraftMutation,
    DeleteDraftMutationVariables,
    DeleteDraftProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteDraftMutation,
    DeleteDraftMutationVariables,
    DeleteDraftProps<TChildProps>
  >(DeleteDraftDocument, {
    alias: 'deleteDraft',
    ...operationOptions,
  })
}

/**
 * __useDeleteDraftMutation__
 *
 * To run a mutation, you first call `useDeleteDraftMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDraftMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDraftMutation, { data, loading, error }] = useDeleteDraftMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteDraftMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteDraftMutation, DeleteDraftMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteDraftMutation, DeleteDraftMutationVariables>(
    DeleteDraftDocument,
    baseOptions,
  )
}
export type DeleteDraftMutationHookResult = ReturnType<typeof useDeleteDraftMutation>
export type DeleteDraftMutationResult = ApolloReactCommon.MutationResult<DeleteDraftMutation>
export type DeleteDraftMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteDraftMutation,
  DeleteDraftMutationVariables
>
export const CreateDriverDocument = gql`
  mutation CreateDriver($input: CreateDriverInput!) {
    createDriver(input: $input) {
      changedDriver {
        ...driverFragment
      }
    }
  }
  ${DriverFragmentFragmentDoc}
`
export type CreateDriverMutationFn = ApolloReactCommon.MutationFunction<
  CreateDriverMutation,
  CreateDriverMutationVariables
>
export type CreateDriverComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateDriverMutation, CreateDriverMutationVariables>,
  'mutation'
>

export const CreateDriverComponent = (props: CreateDriverComponentProps) => (
  <ApolloReactComponents.Mutation<CreateDriverMutation, CreateDriverMutationVariables>
    mutation={CreateDriverDocument}
    {...props}
  />
)

export type CreateDriverProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateDriverMutation,
  CreateDriverMutationVariables
> &
  TChildProps
export function withCreateDriver<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateDriverMutation,
    CreateDriverMutationVariables,
    CreateDriverProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateDriverMutation,
    CreateDriverMutationVariables,
    CreateDriverProps<TChildProps>
  >(CreateDriverDocument, {
    alias: 'createDriver',
    ...operationOptions,
  })
}

/**
 * __useCreateDriverMutation__
 *
 * To run a mutation, you first call `useCreateDriverMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateDriverMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createDriverMutation, { data, loading, error }] = useCreateDriverMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateDriverMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateDriverMutation, CreateDriverMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateDriverMutation, CreateDriverMutationVariables>(
    CreateDriverDocument,
    baseOptions,
  )
}
export type CreateDriverMutationHookResult = ReturnType<typeof useCreateDriverMutation>
export type CreateDriverMutationResult = ApolloReactCommon.MutationResult<CreateDriverMutation>
export type CreateDriverMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateDriverMutation,
  CreateDriverMutationVariables
>
export const UpdateDriverDocument = gql`
  mutation UpdateDriver($input: UpdateDriverInput!) {
    updateDriver(input: $input) {
      changedDriver {
        ...driverFragment
      }
    }
  }
  ${DriverFragmentFragmentDoc}
`
export type UpdateDriverMutationFn = ApolloReactCommon.MutationFunction<
  UpdateDriverMutation,
  UpdateDriverMutationVariables
>
export type UpdateDriverComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateDriverMutation, UpdateDriverMutationVariables>,
  'mutation'
>

export const UpdateDriverComponent = (props: UpdateDriverComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateDriverMutation, UpdateDriverMutationVariables>
    mutation={UpdateDriverDocument}
    {...props}
  />
)

export type UpdateDriverProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateDriverMutation,
  UpdateDriverMutationVariables
> &
  TChildProps
export function withUpdateDriver<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateDriverMutation,
    UpdateDriverMutationVariables,
    UpdateDriverProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateDriverMutation,
    UpdateDriverMutationVariables,
    UpdateDriverProps<TChildProps>
  >(UpdateDriverDocument, {
    alias: 'updateDriver',
    ...operationOptions,
  })
}

/**
 * __useUpdateDriverMutation__
 *
 * To run a mutation, you first call `useUpdateDriverMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateDriverMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateDriverMutation, { data, loading, error }] = useUpdateDriverMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateDriverMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateDriverMutation, UpdateDriverMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateDriverMutation, UpdateDriverMutationVariables>(
    UpdateDriverDocument,
    baseOptions,
  )
}
export type UpdateDriverMutationHookResult = ReturnType<typeof useUpdateDriverMutation>
export type UpdateDriverMutationResult = ApolloReactCommon.MutationResult<UpdateDriverMutation>
export type UpdateDriverMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateDriverMutation,
  UpdateDriverMutationVariables
>
export const DeleteDriverDocument = gql`
  mutation DeleteDriver($input: DeleteDriverInput!) {
    deleteDriver(input: $input) {
      changedDriver {
        ...driverFragment
      }
    }
  }
  ${DriverFragmentFragmentDoc}
`
export type DeleteDriverMutationFn = ApolloReactCommon.MutationFunction<
  DeleteDriverMutation,
  DeleteDriverMutationVariables
>
export type DeleteDriverComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteDriverMutation, DeleteDriverMutationVariables>,
  'mutation'
>

export const DeleteDriverComponent = (props: DeleteDriverComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteDriverMutation, DeleteDriverMutationVariables>
    mutation={DeleteDriverDocument}
    {...props}
  />
)

export type DeleteDriverProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteDriverMutation,
  DeleteDriverMutationVariables
> &
  TChildProps
export function withDeleteDriver<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteDriverMutation,
    DeleteDriverMutationVariables,
    DeleteDriverProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteDriverMutation,
    DeleteDriverMutationVariables,
    DeleteDriverProps<TChildProps>
  >(DeleteDriverDocument, {
    alias: 'deleteDriver',
    ...operationOptions,
  })
}

/**
 * __useDeleteDriverMutation__
 *
 * To run a mutation, you first call `useDeleteDriverMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDriverMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDriverMutation, { data, loading, error }] = useDeleteDriverMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteDriverMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteDriverMutation, DeleteDriverMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteDriverMutation, DeleteDriverMutationVariables>(
    DeleteDriverDocument,
    baseOptions,
  )
}
export type DeleteDriverMutationHookResult = ReturnType<typeof useDeleteDriverMutation>
export type DeleteDriverMutationResult = ApolloReactCommon.MutationResult<DeleteDriverMutation>
export type DeleteDriverMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteDriverMutation,
  DeleteDriverMutationVariables
>
export const AllDriversVehiclesDocument = gql`
  query AllDriversVehicles {
    viewer {
      allVehicles {
        ...vehicleFragment
      }
      allDrivers {
        ...driverFragment
      }
    }
  }
  ${VehicleFragmentFragmentDoc}
  ${DriverFragmentFragmentDoc}
`
export type AllDriversVehiclesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllDriversVehiclesQuery, AllDriversVehiclesQueryVariables>,
  'query'
>

export const AllDriversVehiclesComponent = (props: AllDriversVehiclesComponentProps) => (
  <ApolloReactComponents.Query<AllDriversVehiclesQuery, AllDriversVehiclesQueryVariables>
    query={AllDriversVehiclesDocument}
    {...props}
  />
)

export type AllDriversVehiclesProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllDriversVehiclesQuery,
  AllDriversVehiclesQueryVariables
> &
  TChildProps
export function withAllDriversVehicles<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllDriversVehiclesQuery,
    AllDriversVehiclesQueryVariables,
    AllDriversVehiclesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllDriversVehiclesQuery,
    AllDriversVehiclesQueryVariables,
    AllDriversVehiclesProps<TChildProps>
  >(AllDriversVehiclesDocument, {
    alias: 'allDriversVehicles',
    ...operationOptions,
  })
}

/**
 * __useAllDriversVehiclesQuery__
 *
 * To run a query within a React component, call `useAllDriversVehiclesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllDriversVehiclesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllDriversVehiclesQuery({
 *   variables: {
 *   },
 * });
 */
export function useAllDriversVehiclesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllDriversVehiclesQuery, AllDriversVehiclesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllDriversVehiclesQuery, AllDriversVehiclesQueryVariables>(
    AllDriversVehiclesDocument,
    baseOptions,
  )
}
export function useAllDriversVehiclesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllDriversVehiclesQuery, AllDriversVehiclesQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllDriversVehiclesQuery, AllDriversVehiclesQueryVariables>(
    AllDriversVehiclesDocument,
    baseOptions,
  )
}
export type AllDriversVehiclesQueryHookResult = ReturnType<typeof useAllDriversVehiclesQuery>
export type AllDriversVehiclesLazyQueryHookResult = ReturnType<typeof useAllDriversVehiclesLazyQuery>
export type AllDriversVehiclesQueryResult = ApolloReactCommon.QueryResult<
  AllDriversVehiclesQuery,
  AllDriversVehiclesQueryVariables
>
export const CorrectInventoryItemDocument = gql`
  mutation CorrectInventoryItem($input: CorrectInventoryItemInput!) {
    correctInventoryItem(input: $input) {
      changedInventoryItem {
        id
      }
    }
  }
`
export type CorrectInventoryItemMutationFn = ApolloReactCommon.MutationFunction<
  CorrectInventoryItemMutation,
  CorrectInventoryItemMutationVariables
>
export type CorrectInventoryItemComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CorrectInventoryItemMutation, CorrectInventoryItemMutationVariables>,
  'mutation'
>

export const CorrectInventoryItemComponent = (props: CorrectInventoryItemComponentProps) => (
  <ApolloReactComponents.Mutation<CorrectInventoryItemMutation, CorrectInventoryItemMutationVariables>
    mutation={CorrectInventoryItemDocument}
    {...props}
  />
)

export type CorrectInventoryItemProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CorrectInventoryItemMutation,
  CorrectInventoryItemMutationVariables
> &
  TChildProps
export function withCorrectInventoryItem<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CorrectInventoryItemMutation,
    CorrectInventoryItemMutationVariables,
    CorrectInventoryItemProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CorrectInventoryItemMutation,
    CorrectInventoryItemMutationVariables,
    CorrectInventoryItemProps<TChildProps>
  >(CorrectInventoryItemDocument, {
    alias: 'correctInventoryItem',
    ...operationOptions,
  })
}

/**
 * __useCorrectInventoryItemMutation__
 *
 * To run a mutation, you first call `useCorrectInventoryItemMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCorrectInventoryItemMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [correctInventoryItemMutation, { data, loading, error }] = useCorrectInventoryItemMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCorrectInventoryItemMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    CorrectInventoryItemMutation,
    CorrectInventoryItemMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<CorrectInventoryItemMutation, CorrectInventoryItemMutationVariables>(
    CorrectInventoryItemDocument,
    baseOptions,
  )
}
export type CorrectInventoryItemMutationHookResult = ReturnType<typeof useCorrectInventoryItemMutation>
export type CorrectInventoryItemMutationResult = ApolloReactCommon.MutationResult<CorrectInventoryItemMutation>
export type CorrectInventoryItemMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CorrectInventoryItemMutation,
  CorrectInventoryItemMutationVariables
>
export const CreateInventoryItemDocument = gql`
  mutation CreateInventoryItem($organizationId: ID!, $skuBatchId: ID!, $warehouseId: ID!) {
    createInventoryItem(organizationId: $organizationId, skuBatchId: $skuBatchId, warehouseId: $warehouseId) {
      id
    }
  }
`
export type CreateInventoryItemMutationFn = ApolloReactCommon.MutationFunction<
  CreateInventoryItemMutation,
  CreateInventoryItemMutationVariables
>
export type CreateInventoryItemComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateInventoryItemMutation, CreateInventoryItemMutationVariables>,
  'mutation'
>

export const CreateInventoryItemComponent = (props: CreateInventoryItemComponentProps) => (
  <ApolloReactComponents.Mutation<CreateInventoryItemMutation, CreateInventoryItemMutationVariables>
    mutation={CreateInventoryItemDocument}
    {...props}
  />
)

export type CreateInventoryItemProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateInventoryItemMutation,
  CreateInventoryItemMutationVariables
> &
  TChildProps
export function withCreateInventoryItem<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateInventoryItemMutation,
    CreateInventoryItemMutationVariables,
    CreateInventoryItemProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateInventoryItemMutation,
    CreateInventoryItemMutationVariables,
    CreateInventoryItemProps<TChildProps>
  >(CreateInventoryItemDocument, {
    alias: 'createInventoryItem',
    ...operationOptions,
  })
}

/**
 * __useCreateInventoryItemMutation__
 *
 * To run a mutation, you first call `useCreateInventoryItemMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateInventoryItemMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createInventoryItemMutation, { data, loading, error }] = useCreateInventoryItemMutation({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      skuBatchId: // value for 'skuBatchId'
 *      warehouseId: // value for 'warehouseId'
 *   },
 * });
 */
export function useCreateInventoryItemMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateInventoryItemMutation, CreateInventoryItemMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateInventoryItemMutation, CreateInventoryItemMutationVariables>(
    CreateInventoryItemDocument,
    baseOptions,
  )
}
export type CreateInventoryItemMutationHookResult = ReturnType<typeof useCreateInventoryItemMutation>
export type CreateInventoryItemMutationResult = ApolloReactCommon.MutationResult<CreateInventoryItemMutation>
export type CreateInventoryItemMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateInventoryItemMutation,
  CreateInventoryItemMutationVariables
>
export const GetInventoryItemDocument = gql`
  query GetInventoryItem($warehouseId: ID, $skuBatchId: ID, $organizationId: ID) {
    getInventoryItem(warehouseId: $warehouseId, skuBatchId: $skuBatchId, organizationId: $organizationId) {
      ...inventoryItemFragment
      warehouse {
        ...warehouseFragment
        site {
          ...siteFragment
        }
      }
    }
  }
  ${InventoryItemFragmentFragmentDoc}
  ${WarehouseFragmentFragmentDoc}
  ${SiteFragmentFragmentDoc}
`
export type GetInventoryItemComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetInventoryItemQuery, GetInventoryItemQueryVariables>,
  'query'
>

export const GetInventoryItemComponent = (props: GetInventoryItemComponentProps) => (
  <ApolloReactComponents.Query<GetInventoryItemQuery, GetInventoryItemQueryVariables>
    query={GetInventoryItemDocument}
    {...props}
  />
)

export type GetInventoryItemProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  GetInventoryItemQuery,
  GetInventoryItemQueryVariables
> &
  TChildProps
export function withGetInventoryItem<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetInventoryItemQuery,
    GetInventoryItemQueryVariables,
    GetInventoryItemProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    GetInventoryItemQuery,
    GetInventoryItemQueryVariables,
    GetInventoryItemProps<TChildProps>
  >(GetInventoryItemDocument, {
    alias: 'getInventoryItem',
    ...operationOptions,
  })
}

/**
 * __useGetInventoryItemQuery__
 *
 * To run a query within a React component, call `useGetInventoryItemQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetInventoryItemQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetInventoryItemQuery({
 *   variables: {
 *      warehouseId: // value for 'warehouseId'
 *      skuBatchId: // value for 'skuBatchId'
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useGetInventoryItemQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<GetInventoryItemQuery, GetInventoryItemQueryVariables>,
) {
  return ApolloReactHooks.useQuery<GetInventoryItemQuery, GetInventoryItemQueryVariables>(
    GetInventoryItemDocument,
    baseOptions,
  )
}
export function useGetInventoryItemLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetInventoryItemQuery, GetInventoryItemQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetInventoryItemQuery, GetInventoryItemQueryVariables>(
    GetInventoryItemDocument,
    baseOptions,
  )
}
export type GetInventoryItemQueryHookResult = ReturnType<typeof useGetInventoryItemQuery>
export type GetInventoryItemLazyQueryHookResult = ReturnType<typeof useGetInventoryItemLazyQuery>
export type GetInventoryItemQueryResult = ApolloReactCommon.QueryResult<
  GetInventoryItemQuery,
  GetInventoryItemQueryVariables
>
export const GetInventoryItemsDocument = gql`
  query GetInventoryItems($skuBatchId: ID!) {
    getInventoryItems(skuBatchId: $skuBatchId) {
      ...inventoryItemFragment
      skuBatch {
        id
      }
      warehouse {
        ...warehouseFragment
        site {
          ...siteFragment
        }
      }
    }
  }
  ${InventoryItemFragmentFragmentDoc}
  ${WarehouseFragmentFragmentDoc}
  ${SiteFragmentFragmentDoc}
`
export type GetInventoryItemsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetInventoryItemsQuery, GetInventoryItemsQueryVariables>,
  'query'
> &
  ({ variables: GetInventoryItemsQueryVariables; skip?: boolean } | { skip: boolean })

export const GetInventoryItemsComponent = (props: GetInventoryItemsComponentProps) => (
  <ApolloReactComponents.Query<GetInventoryItemsQuery, GetInventoryItemsQueryVariables>
    query={GetInventoryItemsDocument}
    {...props}
  />
)

export type GetInventoryItemsProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  GetInventoryItemsQuery,
  GetInventoryItemsQueryVariables
> &
  TChildProps
export function withGetInventoryItems<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetInventoryItemsQuery,
    GetInventoryItemsQueryVariables,
    GetInventoryItemsProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    GetInventoryItemsQuery,
    GetInventoryItemsQueryVariables,
    GetInventoryItemsProps<TChildProps>
  >(GetInventoryItemsDocument, {
    alias: 'getInventoryItems',
    ...operationOptions,
  })
}

/**
 * __useGetInventoryItemsQuery__
 *
 * To run a query within a React component, call `useGetInventoryItemsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetInventoryItemsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetInventoryItemsQuery({
 *   variables: {
 *      skuBatchId: // value for 'skuBatchId'
 *   },
 * });
 */
export function useGetInventoryItemsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<GetInventoryItemsQuery, GetInventoryItemsQueryVariables>,
) {
  return ApolloReactHooks.useQuery<GetInventoryItemsQuery, GetInventoryItemsQueryVariables>(
    GetInventoryItemsDocument,
    baseOptions,
  )
}
export function useGetInventoryItemsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetInventoryItemsQuery, GetInventoryItemsQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetInventoryItemsQuery, GetInventoryItemsQueryVariables>(
    GetInventoryItemsDocument,
    baseOptions,
  )
}
export type GetInventoryItemsQueryHookResult = ReturnType<typeof useGetInventoryItemsQuery>
export type GetInventoryItemsLazyQueryHookResult = ReturnType<typeof useGetInventoryItemsLazyQuery>
export type GetInventoryItemsQueryResult = ApolloReactCommon.QueryResult<
  GetInventoryItemsQuery,
  GetInventoryItemsQueryVariables
>
export const AllInventoryItemsDocument = gql`
  query AllInventoryItems($skuBatchId: ID!, $warehouseId: ID!, $pageInfo: PageInfoInput) {
    viewer {
      allInventoryItems(skuBatchId: $skuBatchId, warehouseId: $warehouseId, pageInfo: $pageInfo) {
        results {
          ...inventoryLedgerItemFragment
          order {
            ...orderFragment
          }
        }
        pageInfo {
          ...pageInfoFragment
        }
      }
    }
  }
  ${InventoryLedgerItemFragmentFragmentDoc}
  ${OrderFragmentFragmentDoc}
  ${PageInfoFragmentFragmentDoc}
`
export type AllInventoryItemsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllInventoryItemsQuery, AllInventoryItemsQueryVariables>,
  'query'
> &
  ({ variables: AllInventoryItemsQueryVariables; skip?: boolean } | { skip: boolean })

export const AllInventoryItemsComponent = (props: AllInventoryItemsComponentProps) => (
  <ApolloReactComponents.Query<AllInventoryItemsQuery, AllInventoryItemsQueryVariables>
    query={AllInventoryItemsDocument}
    {...props}
  />
)

export type AllInventoryItemsProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllInventoryItemsQuery,
  AllInventoryItemsQueryVariables
> &
  TChildProps
export function withAllInventoryItems<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllInventoryItemsQuery,
    AllInventoryItemsQueryVariables,
    AllInventoryItemsProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllInventoryItemsQuery,
    AllInventoryItemsQueryVariables,
    AllInventoryItemsProps<TChildProps>
  >(AllInventoryItemsDocument, {
    alias: 'allInventoryItems',
    ...operationOptions,
  })
}

/**
 * __useAllInventoryItemsQuery__
 *
 * To run a query within a React component, call `useAllInventoryItemsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllInventoryItemsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllInventoryItemsQuery({
 *   variables: {
 *      skuBatchId: // value for 'skuBatchId'
 *      warehouseId: // value for 'warehouseId'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useAllInventoryItemsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllInventoryItemsQuery, AllInventoryItemsQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllInventoryItemsQuery, AllInventoryItemsQueryVariables>(
    AllInventoryItemsDocument,
    baseOptions,
  )
}
export function useAllInventoryItemsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllInventoryItemsQuery, AllInventoryItemsQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllInventoryItemsQuery, AllInventoryItemsQueryVariables>(
    AllInventoryItemsDocument,
    baseOptions,
  )
}
export type AllInventoryItemsQueryHookResult = ReturnType<typeof useAllInventoryItemsQuery>
export type AllInventoryItemsLazyQueryHookResult = ReturnType<typeof useAllInventoryItemsLazyQuery>
export type AllInventoryItemsQueryResult = ApolloReactCommon.QueryResult<
  AllInventoryItemsQuery,
  AllInventoryItemsQueryVariables
>
export const CreateLicenseDocument = gql`
  mutation CreateLicense($input: CreateLicenseInput!) {
    createLicense(input: $input) {
      changedLicense {
        ...licenseFragment
      }
    }
  }
  ${LicenseFragmentFragmentDoc}
`
export type CreateLicenseMutationFn = ApolloReactCommon.MutationFunction<
  CreateLicenseMutation,
  CreateLicenseMutationVariables
>
export type CreateLicenseComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateLicenseMutation, CreateLicenseMutationVariables>,
  'mutation'
>

export const CreateLicenseComponent = (props: CreateLicenseComponentProps) => (
  <ApolloReactComponents.Mutation<CreateLicenseMutation, CreateLicenseMutationVariables>
    mutation={CreateLicenseDocument}
    {...props}
  />
)

export type CreateLicenseProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateLicenseMutation,
  CreateLicenseMutationVariables
> &
  TChildProps
export function withCreateLicense<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateLicenseMutation,
    CreateLicenseMutationVariables,
    CreateLicenseProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateLicenseMutation,
    CreateLicenseMutationVariables,
    CreateLicenseProps<TChildProps>
  >(CreateLicenseDocument, {
    alias: 'createLicense',
    ...operationOptions,
  })
}

/**
 * __useCreateLicenseMutation__
 *
 * To run a mutation, you first call `useCreateLicenseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateLicenseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createLicenseMutation, { data, loading, error }] = useCreateLicenseMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateLicenseMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateLicenseMutation, CreateLicenseMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateLicenseMutation, CreateLicenseMutationVariables>(
    CreateLicenseDocument,
    baseOptions,
  )
}
export type CreateLicenseMutationHookResult = ReturnType<typeof useCreateLicenseMutation>
export type CreateLicenseMutationResult = ApolloReactCommon.MutationResult<CreateLicenseMutation>
export type CreateLicenseMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateLicenseMutation,
  CreateLicenseMutationVariables
>
export const UpdateLicenseDocument = gql`
  mutation UpdateLicense($input: UpdateLicenseInput!) {
    updateLicense(input: $input) {
      changedLicense {
        ...licenseFragment
      }
    }
  }
  ${LicenseFragmentFragmentDoc}
`
export type UpdateLicenseMutationFn = ApolloReactCommon.MutationFunction<
  UpdateLicenseMutation,
  UpdateLicenseMutationVariables
>
export type UpdateLicenseComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateLicenseMutation, UpdateLicenseMutationVariables>,
  'mutation'
>

export const UpdateLicenseComponent = (props: UpdateLicenseComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateLicenseMutation, UpdateLicenseMutationVariables>
    mutation={UpdateLicenseDocument}
    {...props}
  />
)

export type UpdateLicenseProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateLicenseMutation,
  UpdateLicenseMutationVariables
> &
  TChildProps
export function withUpdateLicense<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateLicenseMutation,
    UpdateLicenseMutationVariables,
    UpdateLicenseProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateLicenseMutation,
    UpdateLicenseMutationVariables,
    UpdateLicenseProps<TChildProps>
  >(UpdateLicenseDocument, {
    alias: 'updateLicense',
    ...operationOptions,
  })
}

/**
 * __useUpdateLicenseMutation__
 *
 * To run a mutation, you first call `useUpdateLicenseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateLicenseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateLicenseMutation, { data, loading, error }] = useUpdateLicenseMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateLicenseMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateLicenseMutation, UpdateLicenseMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateLicenseMutation, UpdateLicenseMutationVariables>(
    UpdateLicenseDocument,
    baseOptions,
  )
}
export type UpdateLicenseMutationHookResult = ReturnType<typeof useUpdateLicenseMutation>
export type UpdateLicenseMutationResult = ApolloReactCommon.MutationResult<UpdateLicenseMutation>
export type UpdateLicenseMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateLicenseMutation,
  UpdateLicenseMutationVariables
>
export const DeleteLicenseDocument = gql`
  mutation DeleteLicense($input: DeleteLicenseInput!) {
    deleteLicense(input: $input) {
      changedLicense {
        ...licenseFragment
      }
    }
  }
  ${LicenseFragmentFragmentDoc}
`
export type DeleteLicenseMutationFn = ApolloReactCommon.MutationFunction<
  DeleteLicenseMutation,
  DeleteLicenseMutationVariables
>
export type DeleteLicenseComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteLicenseMutation, DeleteLicenseMutationVariables>,
  'mutation'
>

export const DeleteLicenseComponent = (props: DeleteLicenseComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteLicenseMutation, DeleteLicenseMutationVariables>
    mutation={DeleteLicenseDocument}
    {...props}
  />
)

export type DeleteLicenseProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteLicenseMutation,
  DeleteLicenseMutationVariables
> &
  TChildProps
export function withDeleteLicense<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteLicenseMutation,
    DeleteLicenseMutationVariables,
    DeleteLicenseProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteLicenseMutation,
    DeleteLicenseMutationVariables,
    DeleteLicenseProps<TChildProps>
  >(DeleteLicenseDocument, {
    alias: 'deleteLicense',
    ...operationOptions,
  })
}

/**
 * __useDeleteLicenseMutation__
 *
 * To run a mutation, you first call `useDeleteLicenseMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteLicenseMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteLicenseMutation, { data, loading, error }] = useDeleteLicenseMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteLicenseMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteLicenseMutation, DeleteLicenseMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteLicenseMutation, DeleteLicenseMutationVariables>(
    DeleteLicenseDocument,
    baseOptions,
  )
}
export type DeleteLicenseMutationHookResult = ReturnType<typeof useDeleteLicenseMutation>
export type DeleteLicenseMutationResult = ApolloReactCommon.MutationResult<DeleteLicenseMutation>
export type DeleteLicenseMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteLicenseMutation,
  DeleteLicenseMutationVariables
>
export const AllLicensesDocument = gql`
  query AllLicenses($organizationId: ID!) {
    viewer {
      allLicenses(organizationId: $organizationId) {
        organizationLicenses {
          ...licenseFragment
        }
      }
    }
  }
  ${LicenseFragmentFragmentDoc}
`
export type AllLicensesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllLicensesQuery, AllLicensesQueryVariables>,
  'query'
> &
  ({ variables: AllLicensesQueryVariables; skip?: boolean } | { skip: boolean })

export const AllLicensesComponent = (props: AllLicensesComponentProps) => (
  <ApolloReactComponents.Query<AllLicensesQuery, AllLicensesQueryVariables> query={AllLicensesDocument} {...props} />
)

export type AllLicensesProps<TChildProps = {}> = ApolloReactHoc.DataProps<AllLicensesQuery, AllLicensesQueryVariables> &
  TChildProps
export function withAllLicenses<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllLicensesQuery,
    AllLicensesQueryVariables,
    AllLicensesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, AllLicensesQuery, AllLicensesQueryVariables, AllLicensesProps<TChildProps>>(
    AllLicensesDocument,
    {
      alias: 'allLicenses',
      ...operationOptions,
    },
  )
}

/**
 * __useAllLicensesQuery__
 *
 * To run a query within a React component, call `useAllLicensesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllLicensesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllLicensesQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useAllLicensesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllLicensesQuery, AllLicensesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllLicensesQuery, AllLicensesQueryVariables>(AllLicensesDocument, baseOptions)
}
export function useAllLicensesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllLicensesQuery, AllLicensesQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllLicensesQuery, AllLicensesQueryVariables>(AllLicensesDocument, baseOptions)
}
export type AllLicensesQueryHookResult = ReturnType<typeof useAllLicensesQuery>
export type AllLicensesLazyQueryHookResult = ReturnType<typeof useAllLicensesLazyQuery>
export type AllLicensesQueryResult = ApolloReactCommon.QueryResult<AllLicensesQuery, AllLicensesQueryVariables>
export const AllLicensedLocationsDocument = gql`
  query AllLicensedLocations($name: String, $showArchived: Boolean, $pageInfo: PageInfoInput) {
    viewer {
      allLicensedLocations(name: $name, showArchived: $showArchived, pageInfo: $pageInfo) {
        results {
          ...licensedLocationFragment
          licenses {
            ...licenseFragment
          }
          warehouse {
            ...warehouseFragment
          }
        }
        pageInfo {
          ...pageInfoFragment
        }
      }
    }
  }
  ${LicensedLocationFragmentFragmentDoc}
  ${LicenseFragmentFragmentDoc}
  ${WarehouseFragmentFragmentDoc}
  ${PageInfoFragmentFragmentDoc}
`
export type AllLicensedLocationsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllLicensedLocationsQuery, AllLicensedLocationsQueryVariables>,
  'query'
>

export const AllLicensedLocationsComponent = (props: AllLicensedLocationsComponentProps) => (
  <ApolloReactComponents.Query<AllLicensedLocationsQuery, AllLicensedLocationsQueryVariables>
    query={AllLicensedLocationsDocument}
    {...props}
  />
)

export type AllLicensedLocationsProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllLicensedLocationsQuery,
  AllLicensedLocationsQueryVariables
> &
  TChildProps
export function withAllLicensedLocations<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllLicensedLocationsQuery,
    AllLicensedLocationsQueryVariables,
    AllLicensedLocationsProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllLicensedLocationsQuery,
    AllLicensedLocationsQueryVariables,
    AllLicensedLocationsProps<TChildProps>
  >(AllLicensedLocationsDocument, {
    alias: 'allLicensedLocations',
    ...operationOptions,
  })
}

/**
 * __useAllLicensedLocationsQuery__
 *
 * To run a query within a React component, call `useAllLicensedLocationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllLicensedLocationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllLicensedLocationsQuery({
 *   variables: {
 *      name: // value for 'name'
 *      showArchived: // value for 'showArchived'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useAllLicensedLocationsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllLicensedLocationsQuery, AllLicensedLocationsQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllLicensedLocationsQuery, AllLicensedLocationsQueryVariables>(
    AllLicensedLocationsDocument,
    baseOptions,
  )
}
export function useAllLicensedLocationsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllLicensedLocationsQuery, AllLicensedLocationsQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllLicensedLocationsQuery, AllLicensedLocationsQueryVariables>(
    AllLicensedLocationsDocument,
    baseOptions,
  )
}
export type AllLicensedLocationsQueryHookResult = ReturnType<typeof useAllLicensedLocationsQuery>
export type AllLicensedLocationsLazyQueryHookResult = ReturnType<typeof useAllLicensedLocationsLazyQuery>
export type AllLicensedLocationsQueryResult = ApolloReactCommon.QueryResult<
  AllLicensedLocationsQuery,
  AllLicensedLocationsQueryVariables
>
export const CreateLicensedLocationDocument = gql`
  mutation CreateLicensedLocation($input: CreateLicensedLocationInput!) {
    createLicensedLocation(input: $input)
  }
`
export type CreateLicensedLocationMutationFn = ApolloReactCommon.MutationFunction<
  CreateLicensedLocationMutation,
  CreateLicensedLocationMutationVariables
>
export type CreateLicensedLocationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    CreateLicensedLocationMutation,
    CreateLicensedLocationMutationVariables
  >,
  'mutation'
>

export const CreateLicensedLocationComponent = (props: CreateLicensedLocationComponentProps) => (
  <ApolloReactComponents.Mutation<CreateLicensedLocationMutation, CreateLicensedLocationMutationVariables>
    mutation={CreateLicensedLocationDocument}
    {...props}
  />
)

export type CreateLicensedLocationProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateLicensedLocationMutation,
  CreateLicensedLocationMutationVariables
> &
  TChildProps
export function withCreateLicensedLocation<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateLicensedLocationMutation,
    CreateLicensedLocationMutationVariables,
    CreateLicensedLocationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateLicensedLocationMutation,
    CreateLicensedLocationMutationVariables,
    CreateLicensedLocationProps<TChildProps>
  >(CreateLicensedLocationDocument, {
    alias: 'createLicensedLocation',
    ...operationOptions,
  })
}

/**
 * __useCreateLicensedLocationMutation__
 *
 * To run a mutation, you first call `useCreateLicensedLocationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateLicensedLocationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createLicensedLocationMutation, { data, loading, error }] = useCreateLicensedLocationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateLicensedLocationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    CreateLicensedLocationMutation,
    CreateLicensedLocationMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<CreateLicensedLocationMutation, CreateLicensedLocationMutationVariables>(
    CreateLicensedLocationDocument,
    baseOptions,
  )
}
export type CreateLicensedLocationMutationHookResult = ReturnType<typeof useCreateLicensedLocationMutation>
export type CreateLicensedLocationMutationResult = ApolloReactCommon.MutationResult<CreateLicensedLocationMutation>
export type CreateLicensedLocationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateLicensedLocationMutation,
  CreateLicensedLocationMutationVariables
>
export const UpdateLicensedLocationDocument = gql`
  mutation UpdateLicensedLocation($input: UpdateLicensedLocationInput!) {
    updateLicensedLocation(input: $input)
  }
`
export type UpdateLicensedLocationMutationFn = ApolloReactCommon.MutationFunction<
  UpdateLicensedLocationMutation,
  UpdateLicensedLocationMutationVariables
>
export type UpdateLicensedLocationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    UpdateLicensedLocationMutation,
    UpdateLicensedLocationMutationVariables
  >,
  'mutation'
>

export const UpdateLicensedLocationComponent = (props: UpdateLicensedLocationComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateLicensedLocationMutation, UpdateLicensedLocationMutationVariables>
    mutation={UpdateLicensedLocationDocument}
    {...props}
  />
)

export type UpdateLicensedLocationProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateLicensedLocationMutation,
  UpdateLicensedLocationMutationVariables
> &
  TChildProps
export function withUpdateLicensedLocation<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateLicensedLocationMutation,
    UpdateLicensedLocationMutationVariables,
    UpdateLicensedLocationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateLicensedLocationMutation,
    UpdateLicensedLocationMutationVariables,
    UpdateLicensedLocationProps<TChildProps>
  >(UpdateLicensedLocationDocument, {
    alias: 'updateLicensedLocation',
    ...operationOptions,
  })
}

/**
 * __useUpdateLicensedLocationMutation__
 *
 * To run a mutation, you first call `useUpdateLicensedLocationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateLicensedLocationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateLicensedLocationMutation, { data, loading, error }] = useUpdateLicensedLocationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateLicensedLocationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateLicensedLocationMutation,
    UpdateLicensedLocationMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<UpdateLicensedLocationMutation, UpdateLicensedLocationMutationVariables>(
    UpdateLicensedLocationDocument,
    baseOptions,
  )
}
export type UpdateLicensedLocationMutationHookResult = ReturnType<typeof useUpdateLicensedLocationMutation>
export type UpdateLicensedLocationMutationResult = ApolloReactCommon.MutationResult<UpdateLicensedLocationMutation>
export type UpdateLicensedLocationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateLicensedLocationMutation,
  UpdateLicensedLocationMutationVariables
>
export const DeleteLicensedLocationDocument = gql`
  mutation DeleteLicensedLocation($input: DeleteLicensedLocationInput!) {
    deleteLicensedLocation(input: $input)
  }
`
export type DeleteLicensedLocationMutationFn = ApolloReactCommon.MutationFunction<
  DeleteLicensedLocationMutation,
  DeleteLicensedLocationMutationVariables
>
export type DeleteLicensedLocationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    DeleteLicensedLocationMutation,
    DeleteLicensedLocationMutationVariables
  >,
  'mutation'
>

export const DeleteLicensedLocationComponent = (props: DeleteLicensedLocationComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteLicensedLocationMutation, DeleteLicensedLocationMutationVariables>
    mutation={DeleteLicensedLocationDocument}
    {...props}
  />
)

export type DeleteLicensedLocationProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteLicensedLocationMutation,
  DeleteLicensedLocationMutationVariables
> &
  TChildProps
export function withDeleteLicensedLocation<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteLicensedLocationMutation,
    DeleteLicensedLocationMutationVariables,
    DeleteLicensedLocationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteLicensedLocationMutation,
    DeleteLicensedLocationMutationVariables,
    DeleteLicensedLocationProps<TChildProps>
  >(DeleteLicensedLocationDocument, {
    alias: 'deleteLicensedLocation',
    ...operationOptions,
  })
}

/**
 * __useDeleteLicensedLocationMutation__
 *
 * To run a mutation, you first call `useDeleteLicensedLocationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteLicensedLocationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteLicensedLocationMutation, { data, loading, error }] = useDeleteLicensedLocationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteLicensedLocationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    DeleteLicensedLocationMutation,
    DeleteLicensedLocationMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<DeleteLicensedLocationMutation, DeleteLicensedLocationMutationVariables>(
    DeleteLicensedLocationDocument,
    baseOptions,
  )
}
export type DeleteLicensedLocationMutationHookResult = ReturnType<typeof useDeleteLicensedLocationMutation>
export type DeleteLicensedLocationMutationResult = ApolloReactCommon.MutationResult<DeleteLicensedLocationMutation>
export type DeleteLicensedLocationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteLicensedLocationMutation,
  DeleteLicensedLocationMutationVariables
>
export const UnarchiveLicensedLocationDocument = gql`
  mutation UnarchiveLicensedLocation($input: UnarchiveLicensedLocationInput!) {
    unarchiveLicensedLocation(input: $input)
  }
`
export type UnarchiveLicensedLocationMutationFn = ApolloReactCommon.MutationFunction<
  UnarchiveLicensedLocationMutation,
  UnarchiveLicensedLocationMutationVariables
>
export type UnarchiveLicensedLocationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    UnarchiveLicensedLocationMutation,
    UnarchiveLicensedLocationMutationVariables
  >,
  'mutation'
>

export const UnarchiveLicensedLocationComponent = (props: UnarchiveLicensedLocationComponentProps) => (
  <ApolloReactComponents.Mutation<UnarchiveLicensedLocationMutation, UnarchiveLicensedLocationMutationVariables>
    mutation={UnarchiveLicensedLocationDocument}
    {...props}
  />
)

export type UnarchiveLicensedLocationProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UnarchiveLicensedLocationMutation,
  UnarchiveLicensedLocationMutationVariables
> &
  TChildProps
export function withUnarchiveLicensedLocation<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UnarchiveLicensedLocationMutation,
    UnarchiveLicensedLocationMutationVariables,
    UnarchiveLicensedLocationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UnarchiveLicensedLocationMutation,
    UnarchiveLicensedLocationMutationVariables,
    UnarchiveLicensedLocationProps<TChildProps>
  >(UnarchiveLicensedLocationDocument, {
    alias: 'unarchiveLicensedLocation',
    ...operationOptions,
  })
}

/**
 * __useUnarchiveLicensedLocationMutation__
 *
 * To run a mutation, you first call `useUnarchiveLicensedLocationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUnarchiveLicensedLocationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [unarchiveLicensedLocationMutation, { data, loading, error }] = useUnarchiveLicensedLocationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUnarchiveLicensedLocationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UnarchiveLicensedLocationMutation,
    UnarchiveLicensedLocationMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<UnarchiveLicensedLocationMutation, UnarchiveLicensedLocationMutationVariables>(
    UnarchiveLicensedLocationDocument,
    baseOptions,
  )
}
export type UnarchiveLicensedLocationMutationHookResult = ReturnType<typeof useUnarchiveLicensedLocationMutation>
export type UnarchiveLicensedLocationMutationResult = ApolloReactCommon.MutationResult<
  UnarchiveLicensedLocationMutation
>
export type UnarchiveLicensedLocationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UnarchiveLicensedLocationMutation,
  UnarchiveLicensedLocationMutationVariables
>
export const LineItemIntakeOuttakeDocument = gql`
  mutation LineItemIntakeOuttake($input: LineItemIntakeOuttakeInput!) {
    lineItemIntakeOuttake(input: $input) {
      id
      ...inventoryItemFragment
      skuBatch {
        ...skuBatchFragment
      }
      inventoryLedgerItems {
        ...inventoryLedgerItemFragment
        lineItem {
          id
        }
        order {
          id
        }
      }
    }
  }
  ${InventoryItemFragmentFragmentDoc}
  ${SkuBatchFragmentFragmentDoc}
  ${InventoryLedgerItemFragmentFragmentDoc}
`
export type LineItemIntakeOuttakeMutationFn = ApolloReactCommon.MutationFunction<
  LineItemIntakeOuttakeMutation,
  LineItemIntakeOuttakeMutationVariables
>
export type LineItemIntakeOuttakeComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<LineItemIntakeOuttakeMutation, LineItemIntakeOuttakeMutationVariables>,
  'mutation'
>

export const LineItemIntakeOuttakeComponent = (props: LineItemIntakeOuttakeComponentProps) => (
  <ApolloReactComponents.Mutation<LineItemIntakeOuttakeMutation, LineItemIntakeOuttakeMutationVariables>
    mutation={LineItemIntakeOuttakeDocument}
    {...props}
  />
)

export type LineItemIntakeOuttakeProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  LineItemIntakeOuttakeMutation,
  LineItemIntakeOuttakeMutationVariables
> &
  TChildProps
export function withLineItemIntakeOuttake<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    LineItemIntakeOuttakeMutation,
    LineItemIntakeOuttakeMutationVariables,
    LineItemIntakeOuttakeProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    LineItemIntakeOuttakeMutation,
    LineItemIntakeOuttakeMutationVariables,
    LineItemIntakeOuttakeProps<TChildProps>
  >(LineItemIntakeOuttakeDocument, {
    alias: 'lineItemIntakeOuttake',
    ...operationOptions,
  })
}

/**
 * __useLineItemIntakeOuttakeMutation__
 *
 * To run a mutation, you first call `useLineItemIntakeOuttakeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLineItemIntakeOuttakeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [lineItemIntakeOuttakeMutation, { data, loading, error }] = useLineItemIntakeOuttakeMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useLineItemIntakeOuttakeMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    LineItemIntakeOuttakeMutation,
    LineItemIntakeOuttakeMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<LineItemIntakeOuttakeMutation, LineItemIntakeOuttakeMutationVariables>(
    LineItemIntakeOuttakeDocument,
    baseOptions,
  )
}
export type LineItemIntakeOuttakeMutationHookResult = ReturnType<typeof useLineItemIntakeOuttakeMutation>
export type LineItemIntakeOuttakeMutationResult = ApolloReactCommon.MutationResult<LineItemIntakeOuttakeMutation>
export type LineItemIntakeOuttakeMutationOptions = ApolloReactCommon.BaseMutationOptions<
  LineItemIntakeOuttakeMutation,
  LineItemIntakeOuttakeMutationVariables
>
export const CreateLineItemDocument = gql`
  mutation CreateLineItem($input: CreateLineItemInput!) {
    createLineItem(input: $input) {
      changedLineItem {
        id
      }
    }
  }
`
export type CreateLineItemMutationFn = ApolloReactCommon.MutationFunction<
  CreateLineItemMutation,
  CreateLineItemMutationVariables
>
export type CreateLineItemComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateLineItemMutation, CreateLineItemMutationVariables>,
  'mutation'
>

export const CreateLineItemComponent = (props: CreateLineItemComponentProps) => (
  <ApolloReactComponents.Mutation<CreateLineItemMutation, CreateLineItemMutationVariables>
    mutation={CreateLineItemDocument}
    {...props}
  />
)

export type CreateLineItemProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateLineItemMutation,
  CreateLineItemMutationVariables
> &
  TChildProps
export function withCreateLineItem<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateLineItemMutation,
    CreateLineItemMutationVariables,
    CreateLineItemProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateLineItemMutation,
    CreateLineItemMutationVariables,
    CreateLineItemProps<TChildProps>
  >(CreateLineItemDocument, {
    alias: 'createLineItem',
    ...operationOptions,
  })
}

/**
 * __useCreateLineItemMutation__
 *
 * To run a mutation, you first call `useCreateLineItemMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateLineItemMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createLineItemMutation, { data, loading, error }] = useCreateLineItemMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateLineItemMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateLineItemMutation, CreateLineItemMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateLineItemMutation, CreateLineItemMutationVariables>(
    CreateLineItemDocument,
    baseOptions,
  )
}
export type CreateLineItemMutationHookResult = ReturnType<typeof useCreateLineItemMutation>
export type CreateLineItemMutationResult = ApolloReactCommon.MutationResult<CreateLineItemMutation>
export type CreateLineItemMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateLineItemMutation,
  CreateLineItemMutationVariables
>
export const UpdateLineItemDocument = gql`
  mutation UpdateLineItem($input: UpdateLineItemInput!) {
    updateLineItem(input: $input) {
      changedLineItem {
        id
      }
    }
  }
`
export type UpdateLineItemMutationFn = ApolloReactCommon.MutationFunction<
  UpdateLineItemMutation,
  UpdateLineItemMutationVariables
>
export type UpdateLineItemComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateLineItemMutation, UpdateLineItemMutationVariables>,
  'mutation'
>

export const UpdateLineItemComponent = (props: UpdateLineItemComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateLineItemMutation, UpdateLineItemMutationVariables>
    mutation={UpdateLineItemDocument}
    {...props}
  />
)

export type UpdateLineItemProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateLineItemMutation,
  UpdateLineItemMutationVariables
> &
  TChildProps
export function withUpdateLineItem<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateLineItemMutation,
    UpdateLineItemMutationVariables,
    UpdateLineItemProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateLineItemMutation,
    UpdateLineItemMutationVariables,
    UpdateLineItemProps<TChildProps>
  >(UpdateLineItemDocument, {
    alias: 'updateLineItem',
    ...operationOptions,
  })
}

/**
 * __useUpdateLineItemMutation__
 *
 * To run a mutation, you first call `useUpdateLineItemMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateLineItemMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateLineItemMutation, { data, loading, error }] = useUpdateLineItemMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateLineItemMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateLineItemMutation, UpdateLineItemMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateLineItemMutation, UpdateLineItemMutationVariables>(
    UpdateLineItemDocument,
    baseOptions,
  )
}
export type UpdateLineItemMutationHookResult = ReturnType<typeof useUpdateLineItemMutation>
export type UpdateLineItemMutationResult = ApolloReactCommon.MutationResult<UpdateLineItemMutation>
export type UpdateLineItemMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateLineItemMutation,
  UpdateLineItemMutationVariables
>
export const DeleteLineItemDocument = gql`
  mutation DeleteLineItem($input: DeleteLineItemInput!) {
    deleteLineItem(input: $input) {
      changedLineItem {
        id
      }
    }
  }
`
export type DeleteLineItemMutationFn = ApolloReactCommon.MutationFunction<
  DeleteLineItemMutation,
  DeleteLineItemMutationVariables
>
export type DeleteLineItemComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteLineItemMutation, DeleteLineItemMutationVariables>,
  'mutation'
>

export const DeleteLineItemComponent = (props: DeleteLineItemComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteLineItemMutation, DeleteLineItemMutationVariables>
    mutation={DeleteLineItemDocument}
    {...props}
  />
)

export type DeleteLineItemProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteLineItemMutation,
  DeleteLineItemMutationVariables
> &
  TChildProps
export function withDeleteLineItem<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteLineItemMutation,
    DeleteLineItemMutationVariables,
    DeleteLineItemProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteLineItemMutation,
    DeleteLineItemMutationVariables,
    DeleteLineItemProps<TChildProps>
  >(DeleteLineItemDocument, {
    alias: 'deleteLineItem',
    ...operationOptions,
  })
}

/**
 * __useDeleteLineItemMutation__
 *
 * To run a mutation, you first call `useDeleteLineItemMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteLineItemMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteLineItemMutation, { data, loading, error }] = useDeleteLineItemMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteLineItemMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteLineItemMutation, DeleteLineItemMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteLineItemMutation, DeleteLineItemMutationVariables>(
    DeleteLineItemDocument,
    baseOptions,
  )
}
export type DeleteLineItemMutationHookResult = ReturnType<typeof useDeleteLineItemMutation>
export type DeleteLineItemMutationResult = ApolloReactCommon.MutationResult<DeleteLineItemMutation>
export type DeleteLineItemMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteLineItemMutation,
  DeleteLineItemMutationVariables
>
export const UploadToOnfleetDocument = gql`
  mutation UploadToOnfleet($input: UploadToOnfleetInput!) {
    uploadToOnfleet(input: $input) {
      ok
      errors
    }
  }
`
export type UploadToOnfleetMutationFn = ApolloReactCommon.MutationFunction<
  UploadToOnfleetMutation,
  UploadToOnfleetMutationVariables
>
export type UploadToOnfleetComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UploadToOnfleetMutation, UploadToOnfleetMutationVariables>,
  'mutation'
>

export const UploadToOnfleetComponent = (props: UploadToOnfleetComponentProps) => (
  <ApolloReactComponents.Mutation<UploadToOnfleetMutation, UploadToOnfleetMutationVariables>
    mutation={UploadToOnfleetDocument}
    {...props}
  />
)

export type UploadToOnfleetProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UploadToOnfleetMutation,
  UploadToOnfleetMutationVariables
> &
  TChildProps
export function withUploadToOnfleet<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UploadToOnfleetMutation,
    UploadToOnfleetMutationVariables,
    UploadToOnfleetProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UploadToOnfleetMutation,
    UploadToOnfleetMutationVariables,
    UploadToOnfleetProps<TChildProps>
  >(UploadToOnfleetDocument, {
    alias: 'uploadToOnfleet',
    ...operationOptions,
  })
}

/**
 * __useUploadToOnfleetMutation__
 *
 * To run a mutation, you first call `useUploadToOnfleetMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadToOnfleetMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadToOnfleetMutation, { data, loading, error }] = useUploadToOnfleetMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUploadToOnfleetMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UploadToOnfleetMutation, UploadToOnfleetMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UploadToOnfleetMutation, UploadToOnfleetMutationVariables>(
    UploadToOnfleetDocument,
    baseOptions,
  )
}
export type UploadToOnfleetMutationHookResult = ReturnType<typeof useUploadToOnfleetMutation>
export type UploadToOnfleetMutationResult = ApolloReactCommon.MutationResult<UploadToOnfleetMutation>
export type UploadToOnfleetMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UploadToOnfleetMutation,
  UploadToOnfleetMutationVariables
>
export const CreateOrderChangeDocument = gql`
  mutation CreateOrderChange($submitterId: ID!, $description: String!, $orderId: ID!) {
    createOrderChange(submitterId: $submitterId, description: $description, orderId: $orderId)
  }
`
export type CreateOrderChangeMutationFn = ApolloReactCommon.MutationFunction<
  CreateOrderChangeMutation,
  CreateOrderChangeMutationVariables
>
export type CreateOrderChangeComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateOrderChangeMutation, CreateOrderChangeMutationVariables>,
  'mutation'
>

export const CreateOrderChangeComponent = (props: CreateOrderChangeComponentProps) => (
  <ApolloReactComponents.Mutation<CreateOrderChangeMutation, CreateOrderChangeMutationVariables>
    mutation={CreateOrderChangeDocument}
    {...props}
  />
)

export type CreateOrderChangeProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateOrderChangeMutation,
  CreateOrderChangeMutationVariables
> &
  TChildProps
export function withCreateOrderChange<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateOrderChangeMutation,
    CreateOrderChangeMutationVariables,
    CreateOrderChangeProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateOrderChangeMutation,
    CreateOrderChangeMutationVariables,
    CreateOrderChangeProps<TChildProps>
  >(CreateOrderChangeDocument, {
    alias: 'createOrderChange',
    ...operationOptions,
  })
}

/**
 * __useCreateOrderChangeMutation__
 *
 * To run a mutation, you first call `useCreateOrderChangeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateOrderChangeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createOrderChangeMutation, { data, loading, error }] = useCreateOrderChangeMutation({
 *   variables: {
 *      submitterId: // value for 'submitterId'
 *      description: // value for 'description'
 *      orderId: // value for 'orderId'
 *   },
 * });
 */
export function useCreateOrderChangeMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateOrderChangeMutation, CreateOrderChangeMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateOrderChangeMutation, CreateOrderChangeMutationVariables>(
    CreateOrderChangeDocument,
    baseOptions,
  )
}
export type CreateOrderChangeMutationHookResult = ReturnType<typeof useCreateOrderChangeMutation>
export type CreateOrderChangeMutationResult = ApolloReactCommon.MutationResult<CreateOrderChangeMutation>
export type CreateOrderChangeMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateOrderChangeMutation,
  CreateOrderChangeMutationVariables
>
export const AllAdminOrdersPageDocument = gql`
  query AllAdminOrdersPage(
    $organizationId: ID
    $search: String
    $action: OrderActionEnum
    $status: OrderStatusEnum
    $paymentStatus: OrderPaymentStatusEnum
    $warehouseId: ID
    $driverId: ID
    $vehicleId: ID
    $start: DateTime
    $end: DateTime
    $transactionTypes: [String]
    $pageInfo: PageInfoInput
  ) {
    viewer {
      allAdminOrders(
        organizationId: $organizationId
        search: $search
        action: $action
        status: $status
        paymentStatus: $paymentStatus
        warehouseId: $warehouseId
        driverId: $driverId
        vehicleId: $vehicleId
        start: $start
        end: $end
        transactionTypes: $transactionTypes
        pageInfo: $pageInfo
      ) {
        results {
          id
          action
          adminNotes
          internalNotes
          createdAt
          date
          daysTillPaymentDue
          discount
          edited
          estimatedArrivalTimeAfter
          estimatedArrivalTimeBefore
          exciseTax
          exciseTaxCollected
          gmv
          gmvCollected
          intaken
          manifestGDriveFileId
          apSummaryGDriveFileId
          invoicesS3FileLink
          name
          notes
          number
          outtaken
          paymentStatus
          status
          timeWindow
          creator {
            id
            email
            firstName
            lastName
          }
          driver {
            id
            firstName
            lastName
            driversLicense
          }
          lineItems {
            id
            code
            name
            unit
            quantity
            pricePerUnit
            isExciseTaxable
            isSample
            oversold
            skuBatch {
              id
              batch {
                id
                code
                manifestGDriveFileId
              }
              sku {
                id
                code
                organization {
                  id
                  name
                }
              }
              titleholder {
                id
                type
                titleholderLicense {
                  ...licenseFragment
                }
                titleholderOrganization {
                  ...organizationFragment
                  licenses {
                    ...licenseFragment
                  }
                }
              }
            }
          }
          organization {
            id
            alias
            name
            owner {
              id
              email
              firstName
              lastName
            }
          }
          site {
            id
            name
            address1
            address2
            city
            state
            zip
            pocName
            pocPhoneNumber
            pocEmail
          }
          vehicle {
            id
            name
            make
            model
            licensePlate
            year
          }
          warehouse {
            id
            site {
              id
              name
            }
          }
        }
        pageInfo {
          page
          numItemsPerPage
          orderBy {
            attribute
            order
          }
          totalNumItems
          totalNumPages
        }
      }
      allWarehouses {
        id
        site {
          id
          name
          city
          state
        }
      }
      allDrivers {
        id
        firstName
        lastName
        driversLicense
      }
      allVehicles {
        id
        name
        make
        model
        licensePlate
        year
      }
    }
  }
  ${LicenseFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
`
export type AllAdminOrdersPageComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllAdminOrdersPageQuery, AllAdminOrdersPageQueryVariables>,
  'query'
>

export const AllAdminOrdersPageComponent = (props: AllAdminOrdersPageComponentProps) => (
  <ApolloReactComponents.Query<AllAdminOrdersPageQuery, AllAdminOrdersPageQueryVariables>
    query={AllAdminOrdersPageDocument}
    {...props}
  />
)

export type AllAdminOrdersPageProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllAdminOrdersPageQuery,
  AllAdminOrdersPageQueryVariables
> &
  TChildProps
export function withAllAdminOrdersPage<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllAdminOrdersPageQuery,
    AllAdminOrdersPageQueryVariables,
    AllAdminOrdersPageProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllAdminOrdersPageQuery,
    AllAdminOrdersPageQueryVariables,
    AllAdminOrdersPageProps<TChildProps>
  >(AllAdminOrdersPageDocument, {
    alias: 'allAdminOrdersPage',
    ...operationOptions,
  })
}

/**
 * __useAllAdminOrdersPageQuery__
 *
 * To run a query within a React component, call `useAllAdminOrdersPageQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllAdminOrdersPageQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllAdminOrdersPageQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      search: // value for 'search'
 *      action: // value for 'action'
 *      status: // value for 'status'
 *      paymentStatus: // value for 'paymentStatus'
 *      warehouseId: // value for 'warehouseId'
 *      driverId: // value for 'driverId'
 *      vehicleId: // value for 'vehicleId'
 *      start: // value for 'start'
 *      end: // value for 'end'
 *      transactionTypes: // value for 'transactionTypes'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useAllAdminOrdersPageQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllAdminOrdersPageQuery, AllAdminOrdersPageQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllAdminOrdersPageQuery, AllAdminOrdersPageQueryVariables>(
    AllAdminOrdersPageDocument,
    baseOptions,
  )
}
export function useAllAdminOrdersPageLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllAdminOrdersPageQuery, AllAdminOrdersPageQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllAdminOrdersPageQuery, AllAdminOrdersPageQueryVariables>(
    AllAdminOrdersPageDocument,
    baseOptions,
  )
}
export type AllAdminOrdersPageQueryHookResult = ReturnType<typeof useAllAdminOrdersPageQuery>
export type AllAdminOrdersPageLazyQueryHookResult = ReturnType<typeof useAllAdminOrdersPageLazyQuery>
export type AllAdminOrdersPageQueryResult = ApolloReactCommon.QueryResult<
  AllAdminOrdersPageQuery,
  AllAdminOrdersPageQueryVariables
>
export const CreateOrderDocument = gql`
  mutation CreateOrder($input: CreateOrderInput!) {
    createOrder(input: $input) {
      changedOrder {
        ...orderFragment
      }
    }
  }
  ${OrderFragmentFragmentDoc}
`
export type CreateOrderMutationFn = ApolloReactCommon.MutationFunction<
  CreateOrderMutation,
  CreateOrderMutationVariables
>
export type CreateOrderComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateOrderMutation, CreateOrderMutationVariables>,
  'mutation'
>

export const CreateOrderComponent = (props: CreateOrderComponentProps) => (
  <ApolloReactComponents.Mutation<CreateOrderMutation, CreateOrderMutationVariables>
    mutation={CreateOrderDocument}
    {...props}
  />
)

export type CreateOrderProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateOrderMutation,
  CreateOrderMutationVariables
> &
  TChildProps
export function withCreateOrder<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateOrderMutation,
    CreateOrderMutationVariables,
    CreateOrderProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateOrderMutation,
    CreateOrderMutationVariables,
    CreateOrderProps<TChildProps>
  >(CreateOrderDocument, {
    alias: 'createOrder',
    ...operationOptions,
  })
}

/**
 * __useCreateOrderMutation__
 *
 * To run a mutation, you first call `useCreateOrderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateOrderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createOrderMutation, { data, loading, error }] = useCreateOrderMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateOrderMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateOrderMutation, CreateOrderMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateOrderMutation, CreateOrderMutationVariables>(
    CreateOrderDocument,
    baseOptions,
  )
}
export type CreateOrderMutationHookResult = ReturnType<typeof useCreateOrderMutation>
export type CreateOrderMutationResult = ApolloReactCommon.MutationResult<CreateOrderMutation>
export type CreateOrderMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateOrderMutation,
  CreateOrderMutationVariables
>
export const UpdateOrderDocument = gql`
  mutation UpdateOrder($input: UpdateOrderInput!) {
    updateOrder(input: $input) {
      changedOrder {
        ...orderFragment
      }
    }
  }
  ${OrderFragmentFragmentDoc}
`
export type UpdateOrderMutationFn = ApolloReactCommon.MutationFunction<
  UpdateOrderMutation,
  UpdateOrderMutationVariables
>
export type UpdateOrderComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateOrderMutation, UpdateOrderMutationVariables>,
  'mutation'
>

export const UpdateOrderComponent = (props: UpdateOrderComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateOrderMutation, UpdateOrderMutationVariables>
    mutation={UpdateOrderDocument}
    {...props}
  />
)

export type UpdateOrderProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateOrderMutation,
  UpdateOrderMutationVariables
> &
  TChildProps
export function withUpdateOrder<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateOrderMutation,
    UpdateOrderMutationVariables,
    UpdateOrderProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateOrderMutation,
    UpdateOrderMutationVariables,
    UpdateOrderProps<TChildProps>
  >(UpdateOrderDocument, {
    alias: 'updateOrder',
    ...operationOptions,
  })
}

/**
 * __useUpdateOrderMutation__
 *
 * To run a mutation, you first call `useUpdateOrderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateOrderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateOrderMutation, { data, loading, error }] = useUpdateOrderMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateOrderMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateOrderMutation, UpdateOrderMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateOrderMutation, UpdateOrderMutationVariables>(
    UpdateOrderDocument,
    baseOptions,
  )
}
export type UpdateOrderMutationHookResult = ReturnType<typeof useUpdateOrderMutation>
export type UpdateOrderMutationResult = ApolloReactCommon.MutationResult<UpdateOrderMutation>
export type UpdateOrderMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateOrderMutation,
  UpdateOrderMutationVariables
>
export const DeleteOrderDocument = gql`
  mutation DeleteOrder($input: DeleteOrderInput!) {
    deleteOrder(input: $input) {
      changedOrder {
        ...orderFragment
      }
    }
  }
  ${OrderFragmentFragmentDoc}
`
export type DeleteOrderMutationFn = ApolloReactCommon.MutationFunction<
  DeleteOrderMutation,
  DeleteOrderMutationVariables
>
export type DeleteOrderComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteOrderMutation, DeleteOrderMutationVariables>,
  'mutation'
>

export const DeleteOrderComponent = (props: DeleteOrderComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteOrderMutation, DeleteOrderMutationVariables>
    mutation={DeleteOrderDocument}
    {...props}
  />
)

export type DeleteOrderProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteOrderMutation,
  DeleteOrderMutationVariables
> &
  TChildProps
export function withDeleteOrder<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteOrderMutation,
    DeleteOrderMutationVariables,
    DeleteOrderProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteOrderMutation,
    DeleteOrderMutationVariables,
    DeleteOrderProps<TChildProps>
  >(DeleteOrderDocument, {
    alias: 'deleteOrder',
    ...operationOptions,
  })
}

/**
 * __useDeleteOrderMutation__
 *
 * To run a mutation, you first call `useDeleteOrderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteOrderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteOrderMutation, { data, loading, error }] = useDeleteOrderMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteOrderMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteOrderMutation, DeleteOrderMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteOrderMutation, DeleteOrderMutationVariables>(
    DeleteOrderDocument,
    baseOptions,
  )
}
export type DeleteOrderMutationHookResult = ReturnType<typeof useDeleteOrderMutation>
export type DeleteOrderMutationResult = ApolloReactCommon.MutationResult<DeleteOrderMutation>
export type DeleteOrderMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteOrderMutation,
  DeleteOrderMutationVariables
>
export const GenerateManifestDocument = gql`
  mutation GenerateManifest($orderId: ID!) {
    generateManifest(orderId: $orderId) {
      apSummaryGDriveFileId
      invoicesS3FileLink
    }
  }
`
export type GenerateManifestMutationFn = ApolloReactCommon.MutationFunction<
  GenerateManifestMutation,
  GenerateManifestMutationVariables
>
export type GenerateManifestComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<GenerateManifestMutation, GenerateManifestMutationVariables>,
  'mutation'
>

export const GenerateManifestComponent = (props: GenerateManifestComponentProps) => (
  <ApolloReactComponents.Mutation<GenerateManifestMutation, GenerateManifestMutationVariables>
    mutation={GenerateManifestDocument}
    {...props}
  />
)

export type GenerateManifestProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  GenerateManifestMutation,
  GenerateManifestMutationVariables
> &
  TChildProps
export function withGenerateManifest<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GenerateManifestMutation,
    GenerateManifestMutationVariables,
    GenerateManifestProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    GenerateManifestMutation,
    GenerateManifestMutationVariables,
    GenerateManifestProps<TChildProps>
  >(GenerateManifestDocument, {
    alias: 'generateManifest',
    ...operationOptions,
  })
}

/**
 * __useGenerateManifestMutation__
 *
 * To run a mutation, you first call `useGenerateManifestMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useGenerateManifestMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [generateManifestMutation, { data, loading, error }] = useGenerateManifestMutation({
 *   variables: {
 *      orderId: // value for 'orderId'
 *   },
 * });
 */
export function useGenerateManifestMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<GenerateManifestMutation, GenerateManifestMutationVariables>,
) {
  return ApolloReactHooks.useMutation<GenerateManifestMutation, GenerateManifestMutationVariables>(
    GenerateManifestDocument,
    baseOptions,
  )
}
export type GenerateManifestMutationHookResult = ReturnType<typeof useGenerateManifestMutation>
export type GenerateManifestMutationResult = ApolloReactCommon.MutationResult<GenerateManifestMutation>
export type GenerateManifestMutationOptions = ApolloReactCommon.BaseMutationOptions<
  GenerateManifestMutation,
  GenerateManifestMutationVariables
>
export const GetOrderSubscriberDocument = gql`
  mutation GetOrderSubscriber($orderId: ID!, $organizationId: ID!) {
    getOrderSubscriber(orderId: $orderId, organizationId: $organizationId) {
      ...orderSubscriberFragment
    }
  }
  ${OrderSubscriberFragmentFragmentDoc}
`
export type GetOrderSubscriberMutationFn = ApolloReactCommon.MutationFunction<
  GetOrderSubscriberMutation,
  GetOrderSubscriberMutationVariables
>
export type GetOrderSubscriberComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<GetOrderSubscriberMutation, GetOrderSubscriberMutationVariables>,
  'mutation'
>

export const GetOrderSubscriberComponent = (props: GetOrderSubscriberComponentProps) => (
  <ApolloReactComponents.Mutation<GetOrderSubscriberMutation, GetOrderSubscriberMutationVariables>
    mutation={GetOrderSubscriberDocument}
    {...props}
  />
)

export type GetOrderSubscriberProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  GetOrderSubscriberMutation,
  GetOrderSubscriberMutationVariables
> &
  TChildProps
export function withGetOrderSubscriber<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetOrderSubscriberMutation,
    GetOrderSubscriberMutationVariables,
    GetOrderSubscriberProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    GetOrderSubscriberMutation,
    GetOrderSubscriberMutationVariables,
    GetOrderSubscriberProps<TChildProps>
  >(GetOrderSubscriberDocument, {
    alias: 'getOrderSubscriber',
    ...operationOptions,
  })
}

/**
 * __useGetOrderSubscriberMutation__
 *
 * To run a mutation, you first call `useGetOrderSubscriberMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useGetOrderSubscriberMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [getOrderSubscriberMutation, { data, loading, error }] = useGetOrderSubscriberMutation({
 *   variables: {
 *      orderId: // value for 'orderId'
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useGetOrderSubscriberMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<GetOrderSubscriberMutation, GetOrderSubscriberMutationVariables>,
) {
  return ApolloReactHooks.useMutation<GetOrderSubscriberMutation, GetOrderSubscriberMutationVariables>(
    GetOrderSubscriberDocument,
    baseOptions,
  )
}
export type GetOrderSubscriberMutationHookResult = ReturnType<typeof useGetOrderSubscriberMutation>
export type GetOrderSubscriberMutationResult = ApolloReactCommon.MutationResult<GetOrderSubscriberMutation>
export type GetOrderSubscriberMutationOptions = ApolloReactCommon.BaseMutationOptions<
  GetOrderSubscriberMutation,
  GetOrderSubscriberMutationVariables
>
export const AssignInvoiceToOrderDocument = gql`
  mutation AssignInvoiceToOrder($invoiceId: ID!, $orderId: ID!) {
    assignInvoiceToOrder(invoiceId: $invoiceId, orderId: $orderId)
  }
`
export type AssignInvoiceToOrderMutationFn = ApolloReactCommon.MutationFunction<
  AssignInvoiceToOrderMutation,
  AssignInvoiceToOrderMutationVariables
>
export type AssignInvoiceToOrderComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<AssignInvoiceToOrderMutation, AssignInvoiceToOrderMutationVariables>,
  'mutation'
>

export const AssignInvoiceToOrderComponent = (props: AssignInvoiceToOrderComponentProps) => (
  <ApolloReactComponents.Mutation<AssignInvoiceToOrderMutation, AssignInvoiceToOrderMutationVariables>
    mutation={AssignInvoiceToOrderDocument}
    {...props}
  />
)

export type AssignInvoiceToOrderProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  AssignInvoiceToOrderMutation,
  AssignInvoiceToOrderMutationVariables
> &
  TChildProps
export function withAssignInvoiceToOrder<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AssignInvoiceToOrderMutation,
    AssignInvoiceToOrderMutationVariables,
    AssignInvoiceToOrderProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    AssignInvoiceToOrderMutation,
    AssignInvoiceToOrderMutationVariables,
    AssignInvoiceToOrderProps<TChildProps>
  >(AssignInvoiceToOrderDocument, {
    alias: 'assignInvoiceToOrder',
    ...operationOptions,
  })
}

/**
 * __useAssignInvoiceToOrderMutation__
 *
 * To run a mutation, you first call `useAssignInvoiceToOrderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAssignInvoiceToOrderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [assignInvoiceToOrderMutation, { data, loading, error }] = useAssignInvoiceToOrderMutation({
 *   variables: {
 *      invoiceId: // value for 'invoiceId'
 *      orderId: // value for 'orderId'
 *   },
 * });
 */
export function useAssignInvoiceToOrderMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    AssignInvoiceToOrderMutation,
    AssignInvoiceToOrderMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<AssignInvoiceToOrderMutation, AssignInvoiceToOrderMutationVariables>(
    AssignInvoiceToOrderDocument,
    baseOptions,
  )
}
export type AssignInvoiceToOrderMutationHookResult = ReturnType<typeof useAssignInvoiceToOrderMutation>
export type AssignInvoiceToOrderMutationResult = ApolloReactCommon.MutationResult<AssignInvoiceToOrderMutation>
export type AssignInvoiceToOrderMutationOptions = ApolloReactCommon.BaseMutationOptions<
  AssignInvoiceToOrderMutation,
  AssignInvoiceToOrderMutationVariables
>
export const AllOrdersDocument = gql`
  query AllOrders(
    $organizationId: ID!
    $number: String
    $status: OrderStatusEnum
    $paymentStatus: OrderPaymentStatusEnum
    $start: DateTime
    $end: DateTime
    $site: String
    $creator: String
    $pageInfo: PageInfoInput
  ) {
    viewer {
      allOrders(
        organizationId: $organizationId
        number: $number
        status: $status
        paymentStatus: $paymentStatus
        start: $start
        end: $end
        site: $site
        creator: $creator
        pageInfo: $pageInfo
      ) {
        results {
          ...orderFragment
          organization {
            ...organizationFragment
          }
          site {
            ...siteFragment
          }
          lineItems {
            ...lineItemFragment
          }
          creator {
            ...userFragment
          }
          assignee {
            ...userFragment
          }
          orderSubscribers {
            ...orderSubscriberFragment
          }
        }
        pageInfo {
          ...pageInfoFragment
        }
      }
    }
  }
  ${OrderFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
  ${SiteFragmentFragmentDoc}
  ${LineItemFragmentFragmentDoc}
  ${UserFragmentFragmentDoc}
  ${OrderSubscriberFragmentFragmentDoc}
  ${PageInfoFragmentFragmentDoc}
`
export type AllOrdersComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllOrdersQuery, AllOrdersQueryVariables>,
  'query'
> &
  ({ variables: AllOrdersQueryVariables; skip?: boolean } | { skip: boolean })

export const AllOrdersComponent = (props: AllOrdersComponentProps) => (
  <ApolloReactComponents.Query<AllOrdersQuery, AllOrdersQueryVariables> query={AllOrdersDocument} {...props} />
)

export type AllOrdersProps<TChildProps = {}> = ApolloReactHoc.DataProps<AllOrdersQuery, AllOrdersQueryVariables> &
  TChildProps
export function withAllOrders<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllOrdersQuery,
    AllOrdersQueryVariables,
    AllOrdersProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, AllOrdersQuery, AllOrdersQueryVariables, AllOrdersProps<TChildProps>>(
    AllOrdersDocument,
    {
      alias: 'allOrders',
      ...operationOptions,
    },
  )
}

/**
 * __useAllOrdersQuery__
 *
 * To run a query within a React component, call `useAllOrdersQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllOrdersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllOrdersQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      number: // value for 'number'
 *      status: // value for 'status'
 *      paymentStatus: // value for 'paymentStatus'
 *      start: // value for 'start'
 *      end: // value for 'end'
 *      site: // value for 'site'
 *      creator: // value for 'creator'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useAllOrdersQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllOrdersQuery, AllOrdersQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllOrdersQuery, AllOrdersQueryVariables>(AllOrdersDocument, baseOptions)
}
export function useAllOrdersLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllOrdersQuery, AllOrdersQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllOrdersQuery, AllOrdersQueryVariables>(AllOrdersDocument, baseOptions)
}
export type AllOrdersQueryHookResult = ReturnType<typeof useAllOrdersQuery>
export type AllOrdersLazyQueryHookResult = ReturnType<typeof useAllOrdersLazyQuery>
export type AllOrdersQueryResult = ApolloReactCommon.QueryResult<AllOrdersQuery, AllOrdersQueryVariables>
export const ActiveOrdersForSkuBatchDocument = gql`
  query ActiveOrdersForSkuBatch($skuBatchId: ID!) {
    getActiveOrdersForSkuBatch(skuBatchId: $skuBatchId) {
      ...orderFragment
      site {
        ...siteFragment
        licensedLocation {
          ...licensedLocationFragment
        }
      }
      lineItems {
        ...lineItemFragment
        skuBatch {
          ...skuBatchFragment
        }
      }
      warehouse {
        ...warehouseFragment
        site {
          ...siteFragment
        }
      }
    }
  }
  ${OrderFragmentFragmentDoc}
  ${SiteFragmentFragmentDoc}
  ${LicensedLocationFragmentFragmentDoc}
  ${LineItemFragmentFragmentDoc}
  ${SkuBatchFragmentFragmentDoc}
  ${WarehouseFragmentFragmentDoc}
`
export type ActiveOrdersForSkuBatchComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<ActiveOrdersForSkuBatchQuery, ActiveOrdersForSkuBatchQueryVariables>,
  'query'
> &
  ({ variables: ActiveOrdersForSkuBatchQueryVariables; skip?: boolean } | { skip: boolean })

export const ActiveOrdersForSkuBatchComponent = (props: ActiveOrdersForSkuBatchComponentProps) => (
  <ApolloReactComponents.Query<ActiveOrdersForSkuBatchQuery, ActiveOrdersForSkuBatchQueryVariables>
    query={ActiveOrdersForSkuBatchDocument}
    {...props}
  />
)

export type ActiveOrdersForSkuBatchProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  ActiveOrdersForSkuBatchQuery,
  ActiveOrdersForSkuBatchQueryVariables
> &
  TChildProps
export function withActiveOrdersForSkuBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    ActiveOrdersForSkuBatchQuery,
    ActiveOrdersForSkuBatchQueryVariables,
    ActiveOrdersForSkuBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    ActiveOrdersForSkuBatchQuery,
    ActiveOrdersForSkuBatchQueryVariables,
    ActiveOrdersForSkuBatchProps<TChildProps>
  >(ActiveOrdersForSkuBatchDocument, {
    alias: 'activeOrdersForSkuBatch',
    ...operationOptions,
  })
}

/**
 * __useActiveOrdersForSkuBatchQuery__
 *
 * To run a query within a React component, call `useActiveOrdersForSkuBatchQuery` and pass it any options that fit your needs.
 * When your component renders, `useActiveOrdersForSkuBatchQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useActiveOrdersForSkuBatchQuery({
 *   variables: {
 *      skuBatchId: // value for 'skuBatchId'
 *   },
 * });
 */
export function useActiveOrdersForSkuBatchQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<ActiveOrdersForSkuBatchQuery, ActiveOrdersForSkuBatchQueryVariables>,
) {
  return ApolloReactHooks.useQuery<ActiveOrdersForSkuBatchQuery, ActiveOrdersForSkuBatchQueryVariables>(
    ActiveOrdersForSkuBatchDocument,
    baseOptions,
  )
}
export function useActiveOrdersForSkuBatchLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    ActiveOrdersForSkuBatchQuery,
    ActiveOrdersForSkuBatchQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<ActiveOrdersForSkuBatchQuery, ActiveOrdersForSkuBatchQueryVariables>(
    ActiveOrdersForSkuBatchDocument,
    baseOptions,
  )
}
export type ActiveOrdersForSkuBatchQueryHookResult = ReturnType<typeof useActiveOrdersForSkuBatchQuery>
export type ActiveOrdersForSkuBatchLazyQueryHookResult = ReturnType<typeof useActiveOrdersForSkuBatchLazyQuery>
export type ActiveOrdersForSkuBatchQueryResult = ApolloReactCommon.QueryResult<
  ActiveOrdersForSkuBatchQuery,
  ActiveOrdersForSkuBatchQueryVariables
>
export const AllAdminOrdersDocument = gql`
  query AllAdminOrders(
    $organizationId: ID
    $orderBy: String
    $direction: String
    $search: String
    $action: OrderActionEnum
    $status: OrderStatusEnum
    $paymentStatus: OrderPaymentStatusEnum
    $warehouseId: ID
    $driverId: ID
    $vehicleId: ID
    $start: DateTime
    $end: DateTime
    $pageInfo: PageInfoInput
    $transactionTypes: [String]
  ) {
    viewer {
      allAdminOrders(
        organizationId: $organizationId
        orderBy: $orderBy
        direction: $direction
        search: $search
        action: $action
        status: $status
        paymentStatus: $paymentStatus
        warehouseId: $warehouseId
        driverId: $driverId
        vehicleId: $vehicleId
        start: $start
        end: $end
        pageInfo: $pageInfo
        transactionTypes: $transactionTypes
      ) {
        results {
          ...orderFragment
          apSummaryGDriveFileId
          invoicesS3FileLink
          creator {
            ...userFragment
          }
          driver {
            ...driverFragment
          }
          lineItems {
            ...lineItemFragment
            skuBatch {
              ...skuBatchFragment
              batch {
                ...batchFragment
              }
              sku {
                ...skuFragment
                organization {
                  ...organizationFragment
                }
              }
              inventoryItems {
                ...inventoryItemFragment
                skuBatch {
                  ...skuBatchFragment
                }
                warehouse {
                  ...warehouseFragment
                }
              }
              titleholder {
                id
                type
                titleholderLicense {
                  ...licenseFragment
                }
                titleholderOrganization {
                  ...organizationFragment
                  licenses {
                    ...licenseFragment
                  }
                }
              }
            }
          }
          organization {
            ...organizationFragment
            owner {
              ...userFragment
            }
          }
          site {
            ...siteFragment
          }
          vehicle {
            ...vehicleFragment
          }
          warehouse {
            ...warehouseFragment
            site {
              ...siteFragment
            }
          }
        }
        pageInfo {
          ...pageInfoFragment
        }
      }
      allWarehouses {
        ...warehouseFragment
        site {
          ...siteFragment
        }
      }
      allDrivers {
        ...driverFragment
      }
      allVehicles {
        ...vehicleFragment
      }
    }
  }
  ${OrderFragmentFragmentDoc}
  ${UserFragmentFragmentDoc}
  ${DriverFragmentFragmentDoc}
  ${LineItemFragmentFragmentDoc}
  ${SkuBatchFragmentFragmentDoc}
  ${BatchFragmentFragmentDoc}
  ${SkuFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
  ${InventoryItemFragmentFragmentDoc}
  ${WarehouseFragmentFragmentDoc}
  ${LicenseFragmentFragmentDoc}
  ${SiteFragmentFragmentDoc}
  ${VehicleFragmentFragmentDoc}
  ${PageInfoFragmentFragmentDoc}
`
export type AllAdminOrdersComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllAdminOrdersQuery, AllAdminOrdersQueryVariables>,
  'query'
>

export const AllAdminOrdersComponent = (props: AllAdminOrdersComponentProps) => (
  <ApolloReactComponents.Query<AllAdminOrdersQuery, AllAdminOrdersQueryVariables>
    query={AllAdminOrdersDocument}
    {...props}
  />
)

export type AllAdminOrdersProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllAdminOrdersQuery,
  AllAdminOrdersQueryVariables
> &
  TChildProps
export function withAllAdminOrders<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllAdminOrdersQuery,
    AllAdminOrdersQueryVariables,
    AllAdminOrdersProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllAdminOrdersQuery,
    AllAdminOrdersQueryVariables,
    AllAdminOrdersProps<TChildProps>
  >(AllAdminOrdersDocument, {
    alias: 'allAdminOrders',
    ...operationOptions,
  })
}

/**
 * __useAllAdminOrdersQuery__
 *
 * To run a query within a React component, call `useAllAdminOrdersQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllAdminOrdersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllAdminOrdersQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      orderBy: // value for 'orderBy'
 *      direction: // value for 'direction'
 *      search: // value for 'search'
 *      action: // value for 'action'
 *      status: // value for 'status'
 *      paymentStatus: // value for 'paymentStatus'
 *      warehouseId: // value for 'warehouseId'
 *      driverId: // value for 'driverId'
 *      vehicleId: // value for 'vehicleId'
 *      start: // value for 'start'
 *      end: // value for 'end'
 *      pageInfo: // value for 'pageInfo'
 *      transactionTypes: // value for 'transactionTypes'
 *   },
 * });
 */
export function useAllAdminOrdersQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllAdminOrdersQuery, AllAdminOrdersQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllAdminOrdersQuery, AllAdminOrdersQueryVariables>(
    AllAdminOrdersDocument,
    baseOptions,
  )
}
export function useAllAdminOrdersLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllAdminOrdersQuery, AllAdminOrdersQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllAdminOrdersQuery, AllAdminOrdersQueryVariables>(
    AllAdminOrdersDocument,
    baseOptions,
  )
}
export type AllAdminOrdersQueryHookResult = ReturnType<typeof useAllAdminOrdersQuery>
export type AllAdminOrdersLazyQueryHookResult = ReturnType<typeof useAllAdminOrdersLazyQuery>
export type AllAdminOrdersQueryResult = ApolloReactCommon.QueryResult<AllAdminOrdersQuery, AllAdminOrdersQueryVariables>
export const AllOrdersSkusDocument = gql`
  query AllOrdersSkus($organizationId: ID!) {
    viewer {
      getDashboardData(organizationId: $organizationId) {
        totalDelivered
        totalScheduled
        totalUnscheduled
        totalGMVCollected
        totalExciseTaxCollected
      }
      allSkus(organizationId: $organizationId) {
        results {
          ...skuFragment
        }
      }
    }
  }
  ${SkuFragmentFragmentDoc}
`
export type AllOrdersSkusComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllOrdersSkusQuery, AllOrdersSkusQueryVariables>,
  'query'
> &
  ({ variables: AllOrdersSkusQueryVariables; skip?: boolean } | { skip: boolean })

export const AllOrdersSkusComponent = (props: AllOrdersSkusComponentProps) => (
  <ApolloReactComponents.Query<AllOrdersSkusQuery, AllOrdersSkusQueryVariables>
    query={AllOrdersSkusDocument}
    {...props}
  />
)

export type AllOrdersSkusProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllOrdersSkusQuery,
  AllOrdersSkusQueryVariables
> &
  TChildProps
export function withAllOrdersSkus<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllOrdersSkusQuery,
    AllOrdersSkusQueryVariables,
    AllOrdersSkusProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllOrdersSkusQuery,
    AllOrdersSkusQueryVariables,
    AllOrdersSkusProps<TChildProps>
  >(AllOrdersSkusDocument, {
    alias: 'allOrdersSkus',
    ...operationOptions,
  })
}

/**
 * __useAllOrdersSkusQuery__
 *
 * To run a query within a React component, call `useAllOrdersSkusQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllOrdersSkusQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllOrdersSkusQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useAllOrdersSkusQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllOrdersSkusQuery, AllOrdersSkusQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllOrdersSkusQuery, AllOrdersSkusQueryVariables>(AllOrdersSkusDocument, baseOptions)
}
export function useAllOrdersSkusLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllOrdersSkusQuery, AllOrdersSkusQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllOrdersSkusQuery, AllOrdersSkusQueryVariables>(
    AllOrdersSkusDocument,
    baseOptions,
  )
}
export type AllOrdersSkusQueryHookResult = ReturnType<typeof useAllOrdersSkusQuery>
export type AllOrdersSkusLazyQueryHookResult = ReturnType<typeof useAllOrdersSkusLazyQuery>
export type AllOrdersSkusQueryResult = ApolloReactCommon.QueryResult<AllOrdersSkusQuery, AllOrdersSkusQueryVariables>
export const GetOrderDocument = gql`
  query GetOrder($id: ID!) {
    getOrder(id: $id) {
      ...orderFragment
      site {
        ...siteFragment
        licenses {
          ...licenseFragment
        }
        licensedLocation {
          ...licensedLocationFragment
          licenses {
            ...licenseFragment
          }
        }
      }
      lineItems {
        ...lineItemFragment
        skuBatch {
          ...skuBatchFragment
          inventoryValues {
            LA {
              counted
              incoming
              outgoing
              allocated
              available
            }
            OAK {
              counted
              incoming
              outgoing
              allocated
              available
            }
          }
          batch {
            ...batchFragment
          }
          sku {
            ...skuFragment
            organization {
              ...organizationFragment
            }
          }
        }
        organization {
          ...organizationFragment
        }
      }
      warehouse {
        ...warehouseFragment
        site {
          ...siteFragment
        }
      }
      organization {
        ...organizationFragment
      }
      orderSubscribers {
        ...orderSubscriberFragment
      }
    }
  }
  ${OrderFragmentFragmentDoc}
  ${SiteFragmentFragmentDoc}
  ${LicenseFragmentFragmentDoc}
  ${LicensedLocationFragmentFragmentDoc}
  ${LineItemFragmentFragmentDoc}
  ${SkuBatchFragmentFragmentDoc}
  ${BatchFragmentFragmentDoc}
  ${SkuFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
  ${WarehouseFragmentFragmentDoc}
  ${OrderSubscriberFragmentFragmentDoc}
`
export type GetOrderComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetOrderQuery, GetOrderQueryVariables>,
  'query'
> &
  ({ variables: GetOrderQueryVariables; skip?: boolean } | { skip: boolean })

export const GetOrderComponent = (props: GetOrderComponentProps) => (
  <ApolloReactComponents.Query<GetOrderQuery, GetOrderQueryVariables> query={GetOrderDocument} {...props} />
)

export type GetOrderProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetOrderQuery, GetOrderQueryVariables> &
  TChildProps
export function withGetOrder<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetOrderQuery,
    GetOrderQueryVariables,
    GetOrderProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, GetOrderQuery, GetOrderQueryVariables, GetOrderProps<TChildProps>>(
    GetOrderDocument,
    {
      alias: 'getOrder',
      ...operationOptions,
    },
  )
}

/**
 * __useGetOrderQuery__
 *
 * To run a query within a React component, call `useGetOrderQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOrderQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOrderQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetOrderQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<GetOrderQuery, GetOrderQueryVariables>,
) {
  return ApolloReactHooks.useQuery<GetOrderQuery, GetOrderQueryVariables>(GetOrderDocument, baseOptions)
}
export function useGetOrderLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetOrderQuery, GetOrderQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetOrderQuery, GetOrderQueryVariables>(GetOrderDocument, baseOptions)
}
export type GetOrderQueryHookResult = ReturnType<typeof useGetOrderQuery>
export type GetOrderLazyQueryHookResult = ReturnType<typeof useGetOrderLazyQuery>
export type GetOrderQueryResult = ApolloReactCommon.QueryResult<GetOrderQuery, GetOrderQueryVariables>
export const OrderChangelogDocument = gql`
  query OrderChangelog($id: ID!) {
    getOrderChangelog(id: $id) {
      createdAt
      description
      submitter {
        ...userFragment
      }
      addedLineItems {
        ...lineItemChangeFragment
        skuBatch {
          ...skuBatchFragment
          sku {
            ...skuFragment
          }
          batch {
            ...batchFragment
          }
        }
      }
      removedLineItems {
        ...lineItemChangeFragment
        skuBatch {
          ...skuBatchFragment
          sku {
            ...skuFragment
          }
          batch {
            ...batchFragment
          }
        }
      }
    }
    getOrder(id: $id) {
      ...orderFragment
    }
  }
  ${UserFragmentFragmentDoc}
  ${LineItemChangeFragmentFragmentDoc}
  ${SkuBatchFragmentFragmentDoc}
  ${SkuFragmentFragmentDoc}
  ${BatchFragmentFragmentDoc}
  ${OrderFragmentFragmentDoc}
`
export type OrderChangelogComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<OrderChangelogQuery, OrderChangelogQueryVariables>,
  'query'
> &
  ({ variables: OrderChangelogQueryVariables; skip?: boolean } | { skip: boolean })

export const OrderChangelogComponent = (props: OrderChangelogComponentProps) => (
  <ApolloReactComponents.Query<OrderChangelogQuery, OrderChangelogQueryVariables>
    query={OrderChangelogDocument}
    {...props}
  />
)

export type OrderChangelogProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  OrderChangelogQuery,
  OrderChangelogQueryVariables
> &
  TChildProps
export function withOrderChangelog<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    OrderChangelogQuery,
    OrderChangelogQueryVariables,
    OrderChangelogProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    OrderChangelogQuery,
    OrderChangelogQueryVariables,
    OrderChangelogProps<TChildProps>
  >(OrderChangelogDocument, {
    alias: 'orderChangelog',
    ...operationOptions,
  })
}

/**
 * __useOrderChangelogQuery__
 *
 * To run a query within a React component, call `useOrderChangelogQuery` and pass it any options that fit your needs.
 * When your component renders, `useOrderChangelogQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useOrderChangelogQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useOrderChangelogQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<OrderChangelogQuery, OrderChangelogQueryVariables>,
) {
  return ApolloReactHooks.useQuery<OrderChangelogQuery, OrderChangelogQueryVariables>(
    OrderChangelogDocument,
    baseOptions,
  )
}
export function useOrderChangelogLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<OrderChangelogQuery, OrderChangelogQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<OrderChangelogQuery, OrderChangelogQueryVariables>(
    OrderChangelogDocument,
    baseOptions,
  )
}
export type OrderChangelogQueryHookResult = ReturnType<typeof useOrderChangelogQuery>
export type OrderChangelogLazyQueryHookResult = ReturnType<typeof useOrderChangelogLazyQuery>
export type OrderChangelogQueryResult = ApolloReactCommon.QueryResult<OrderChangelogQuery, OrderChangelogQueryVariables>
export const AllAdminOrganizationsDocument = gql`
  query AllAdminOrganizations {
    viewer {
      allAdminOrganizations {
        ...organizationFragment
      }
    }
  }
  ${OrganizationFragmentFragmentDoc}
`
export type AllAdminOrganizationsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllAdminOrganizationsQuery, AllAdminOrganizationsQueryVariables>,
  'query'
>

export const AllAdminOrganizationsComponent = (props: AllAdminOrganizationsComponentProps) => (
  <ApolloReactComponents.Query<AllAdminOrganizationsQuery, AllAdminOrganizationsQueryVariables>
    query={AllAdminOrganizationsDocument}
    {...props}
  />
)

export type AllAdminOrganizationsProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllAdminOrganizationsQuery,
  AllAdminOrganizationsQueryVariables
> &
  TChildProps
export function withAllAdminOrganizations<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllAdminOrganizationsQuery,
    AllAdminOrganizationsQueryVariables,
    AllAdminOrganizationsProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllAdminOrganizationsQuery,
    AllAdminOrganizationsQueryVariables,
    AllAdminOrganizationsProps<TChildProps>
  >(AllAdminOrganizationsDocument, {
    alias: 'allAdminOrganizations',
    ...operationOptions,
  })
}

/**
 * __useAllAdminOrganizationsQuery__
 *
 * To run a query within a React component, call `useAllAdminOrganizationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllAdminOrganizationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllAdminOrganizationsQuery({
 *   variables: {
 *   },
 * });
 */
export function useAllAdminOrganizationsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllAdminOrganizationsQuery, AllAdminOrganizationsQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllAdminOrganizationsQuery, AllAdminOrganizationsQueryVariables>(
    AllAdminOrganizationsDocument,
    baseOptions,
  )
}
export function useAllAdminOrganizationsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllAdminOrganizationsQuery, AllAdminOrganizationsQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllAdminOrganizationsQuery, AllAdminOrganizationsQueryVariables>(
    AllAdminOrganizationsDocument,
    baseOptions,
  )
}
export type AllAdminOrganizationsQueryHookResult = ReturnType<typeof useAllAdminOrganizationsQuery>
export type AllAdminOrganizationsLazyQueryHookResult = ReturnType<typeof useAllAdminOrganizationsLazyQuery>
export type AllAdminOrganizationsQueryResult = ApolloReactCommon.QueryResult<
  AllAdminOrganizationsQuery,
  AllAdminOrganizationsQueryVariables
>
export const CreateOrganizationDocument = gql`
  mutation CreateOrganization($input: CreateOrganizationInput!) {
    createOrganization(input: $input) {
      changedOrganization {
        ...organizationFragment
      }
    }
  }
  ${OrganizationFragmentFragmentDoc}
`
export type CreateOrganizationMutationFn = ApolloReactCommon.MutationFunction<
  CreateOrganizationMutation,
  CreateOrganizationMutationVariables
>
export type CreateOrganizationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateOrganizationMutation, CreateOrganizationMutationVariables>,
  'mutation'
>

export const CreateOrganizationComponent = (props: CreateOrganizationComponentProps) => (
  <ApolloReactComponents.Mutation<CreateOrganizationMutation, CreateOrganizationMutationVariables>
    mutation={CreateOrganizationDocument}
    {...props}
  />
)

export type CreateOrganizationProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateOrganizationMutation,
  CreateOrganizationMutationVariables
> &
  TChildProps
export function withCreateOrganization<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateOrganizationMutation,
    CreateOrganizationMutationVariables,
    CreateOrganizationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateOrganizationMutation,
    CreateOrganizationMutationVariables,
    CreateOrganizationProps<TChildProps>
  >(CreateOrganizationDocument, {
    alias: 'createOrganization',
    ...operationOptions,
  })
}

/**
 * __useCreateOrganizationMutation__
 *
 * To run a mutation, you first call `useCreateOrganizationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateOrganizationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createOrganizationMutation, { data, loading, error }] = useCreateOrganizationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateOrganizationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateOrganizationMutation, CreateOrganizationMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateOrganizationMutation, CreateOrganizationMutationVariables>(
    CreateOrganizationDocument,
    baseOptions,
  )
}
export type CreateOrganizationMutationHookResult = ReturnType<typeof useCreateOrganizationMutation>
export type CreateOrganizationMutationResult = ApolloReactCommon.MutationResult<CreateOrganizationMutation>
export type CreateOrganizationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateOrganizationMutation,
  CreateOrganizationMutationVariables
>
export const UpdateOrganizationDocument = gql`
  mutation UpdateOrganization($input: UpdateOrganizationInput!) {
    updateOrganization(input: $input) {
      changedOrganization {
        ...organizationFragment
      }
    }
  }
  ${OrganizationFragmentFragmentDoc}
`
export type UpdateOrganizationMutationFn = ApolloReactCommon.MutationFunction<
  UpdateOrganizationMutation,
  UpdateOrganizationMutationVariables
>
export type UpdateOrganizationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateOrganizationMutation, UpdateOrganizationMutationVariables>,
  'mutation'
>

export const UpdateOrganizationComponent = (props: UpdateOrganizationComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateOrganizationMutation, UpdateOrganizationMutationVariables>
    mutation={UpdateOrganizationDocument}
    {...props}
  />
)

export type UpdateOrganizationProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateOrganizationMutation,
  UpdateOrganizationMutationVariables
> &
  TChildProps
export function withUpdateOrganization<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateOrganizationMutation,
    UpdateOrganizationMutationVariables,
    UpdateOrganizationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateOrganizationMutation,
    UpdateOrganizationMutationVariables,
    UpdateOrganizationProps<TChildProps>
  >(UpdateOrganizationDocument, {
    alias: 'updateOrganization',
    ...operationOptions,
  })
}

/**
 * __useUpdateOrganizationMutation__
 *
 * To run a mutation, you first call `useUpdateOrganizationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateOrganizationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateOrganizationMutation, { data, loading, error }] = useUpdateOrganizationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateOrganizationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateOrganizationMutation, UpdateOrganizationMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateOrganizationMutation, UpdateOrganizationMutationVariables>(
    UpdateOrganizationDocument,
    baseOptions,
  )
}
export type UpdateOrganizationMutationHookResult = ReturnType<typeof useUpdateOrganizationMutation>
export type UpdateOrganizationMutationResult = ApolloReactCommon.MutationResult<UpdateOrganizationMutation>
export type UpdateOrganizationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateOrganizationMutation,
  UpdateOrganizationMutationVariables
>
export const DeleteOrganizationDocument = gql`
  mutation DeleteOrganization($input: DeleteOrganizationInput!) {
    deleteOrganization(input: $input) {
      changedOrganization {
        id
      }
    }
  }
`
export type DeleteOrganizationMutationFn = ApolloReactCommon.MutationFunction<
  DeleteOrganizationMutation,
  DeleteOrganizationMutationVariables
>
export type DeleteOrganizationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteOrganizationMutation, DeleteOrganizationMutationVariables>,
  'mutation'
>

export const DeleteOrganizationComponent = (props: DeleteOrganizationComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteOrganizationMutation, DeleteOrganizationMutationVariables>
    mutation={DeleteOrganizationDocument}
    {...props}
  />
)

export type DeleteOrganizationProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteOrganizationMutation,
  DeleteOrganizationMutationVariables
> &
  TChildProps
export function withDeleteOrganization<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteOrganizationMutation,
    DeleteOrganizationMutationVariables,
    DeleteOrganizationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteOrganizationMutation,
    DeleteOrganizationMutationVariables,
    DeleteOrganizationProps<TChildProps>
  >(DeleteOrganizationDocument, {
    alias: 'deleteOrganization',
    ...operationOptions,
  })
}

/**
 * __useDeleteOrganizationMutation__
 *
 * To run a mutation, you first call `useDeleteOrganizationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteOrganizationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteOrganizationMutation, { data, loading, error }] = useDeleteOrganizationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteOrganizationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteOrganizationMutation, DeleteOrganizationMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteOrganizationMutation, DeleteOrganizationMutationVariables>(
    DeleteOrganizationDocument,
    baseOptions,
  )
}
export type DeleteOrganizationMutationHookResult = ReturnType<typeof useDeleteOrganizationMutation>
export type DeleteOrganizationMutationResult = ApolloReactCommon.MutationResult<DeleteOrganizationMutation>
export type DeleteOrganizationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteOrganizationMutation,
  DeleteOrganizationMutationVariables
>
export const OrganizationDocument = gql`
  query Organization($id: ID!) {
    getOrganization(id: $id) {
      ...organizationFragment
      owner {
        ...userFragment
      }
      members {
        ...userFragment
        roles {
          ...roleFragment
        }
      }
      licenses {
        ...licenseFragment
      }
    }
  }
  ${OrganizationFragmentFragmentDoc}
  ${UserFragmentFragmentDoc}
  ${RoleFragmentFragmentDoc}
  ${LicenseFragmentFragmentDoc}
`
export type OrganizationComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<OrganizationQuery, OrganizationQueryVariables>,
  'query'
> &
  ({ variables: OrganizationQueryVariables; skip?: boolean } | { skip: boolean })

export const OrganizationComponent = (props: OrganizationComponentProps) => (
  <ApolloReactComponents.Query<OrganizationQuery, OrganizationQueryVariables> query={OrganizationDocument} {...props} />
)

export type OrganizationProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  OrganizationQuery,
  OrganizationQueryVariables
> &
  TChildProps
export function withOrganization<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    OrganizationQuery,
    OrganizationQueryVariables,
    OrganizationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    OrganizationQuery,
    OrganizationQueryVariables,
    OrganizationProps<TChildProps>
  >(OrganizationDocument, {
    alias: 'organization',
    ...operationOptions,
  })
}

/**
 * __useOrganizationQuery__
 *
 * To run a query within a React component, call `useOrganizationQuery` and pass it any options that fit your needs.
 * When your component renders, `useOrganizationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useOrganizationQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useOrganizationQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<OrganizationQuery, OrganizationQueryVariables>,
) {
  return ApolloReactHooks.useQuery<OrganizationQuery, OrganizationQueryVariables>(OrganizationDocument, baseOptions)
}
export function useOrganizationLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<OrganizationQuery, OrganizationQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<OrganizationQuery, OrganizationQueryVariables>(OrganizationDocument, baseOptions)
}
export type OrganizationQueryHookResult = ReturnType<typeof useOrganizationQuery>
export type OrganizationLazyQueryHookResult = ReturnType<typeof useOrganizationLazyQuery>
export type OrganizationQueryResult = ApolloReactCommon.QueryResult<OrganizationQuery, OrganizationQueryVariables>
export const GetOrganizationUsersDocument = gql`
  query GetOrganizationUsers($organizationId: ID!) {
    getOrganization(id: $organizationId) {
      id
      owner {
        id
      }
      members {
        id
        email
        phone
        firstName
        lastName
        roles {
          id
        }
      }
    }
  }
`
export type GetOrganizationUsersComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetOrganizationUsersQuery, GetOrganizationUsersQueryVariables>,
  'query'
> &
  ({ variables: GetOrganizationUsersQueryVariables; skip?: boolean } | { skip: boolean })

export const GetOrganizationUsersComponent = (props: GetOrganizationUsersComponentProps) => (
  <ApolloReactComponents.Query<GetOrganizationUsersQuery, GetOrganizationUsersQueryVariables>
    query={GetOrganizationUsersDocument}
    {...props}
  />
)

export type GetOrganizationUsersProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  GetOrganizationUsersQuery,
  GetOrganizationUsersQueryVariables
> &
  TChildProps
export function withGetOrganizationUsers<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetOrganizationUsersQuery,
    GetOrganizationUsersQueryVariables,
    GetOrganizationUsersProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    GetOrganizationUsersQuery,
    GetOrganizationUsersQueryVariables,
    GetOrganizationUsersProps<TChildProps>
  >(GetOrganizationUsersDocument, {
    alias: 'getOrganizationUsers',
    ...operationOptions,
  })
}

/**
 * __useGetOrganizationUsersQuery__
 *
 * To run a query within a React component, call `useGetOrganizationUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOrganizationUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOrganizationUsersQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useGetOrganizationUsersQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<GetOrganizationUsersQuery, GetOrganizationUsersQueryVariables>,
) {
  return ApolloReactHooks.useQuery<GetOrganizationUsersQuery, GetOrganizationUsersQueryVariables>(
    GetOrganizationUsersDocument,
    baseOptions,
  )
}
export function useGetOrganizationUsersLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetOrganizationUsersQuery, GetOrganizationUsersQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetOrganizationUsersQuery, GetOrganizationUsersQueryVariables>(
    GetOrganizationUsersDocument,
    baseOptions,
  )
}
export type GetOrganizationUsersQueryHookResult = ReturnType<typeof useGetOrganizationUsersQuery>
export type GetOrganizationUsersLazyQueryHookResult = ReturnType<typeof useGetOrganizationUsersLazyQuery>
export type GetOrganizationUsersQueryResult = ApolloReactCommon.QueryResult<
  GetOrganizationUsersQuery,
  GetOrganizationUsersQueryVariables
>
export const CreateOrganizationInviteDocument = gql`
  mutation CreateOrganizationInvite($input: CreateOrganizationInviteInput!) {
    createOrganizationInvite(input: $input) {
      changedOrganizationInvite {
        ...organizationInviteFragment
        organization {
          ...organizationFragment
        }
      }
    }
  }
  ${OrganizationInviteFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
`
export type CreateOrganizationInviteMutationFn = ApolloReactCommon.MutationFunction<
  CreateOrganizationInviteMutation,
  CreateOrganizationInviteMutationVariables
>
export type CreateOrganizationInviteComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    CreateOrganizationInviteMutation,
    CreateOrganizationInviteMutationVariables
  >,
  'mutation'
>

export const CreateOrganizationInviteComponent = (props: CreateOrganizationInviteComponentProps) => (
  <ApolloReactComponents.Mutation<CreateOrganizationInviteMutation, CreateOrganizationInviteMutationVariables>
    mutation={CreateOrganizationInviteDocument}
    {...props}
  />
)

export type CreateOrganizationInviteProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateOrganizationInviteMutation,
  CreateOrganizationInviteMutationVariables
> &
  TChildProps
export function withCreateOrganizationInvite<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateOrganizationInviteMutation,
    CreateOrganizationInviteMutationVariables,
    CreateOrganizationInviteProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateOrganizationInviteMutation,
    CreateOrganizationInviteMutationVariables,
    CreateOrganizationInviteProps<TChildProps>
  >(CreateOrganizationInviteDocument, {
    alias: 'createOrganizationInvite',
    ...operationOptions,
  })
}

/**
 * __useCreateOrganizationInviteMutation__
 *
 * To run a mutation, you first call `useCreateOrganizationInviteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateOrganizationInviteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createOrganizationInviteMutation, { data, loading, error }] = useCreateOrganizationInviteMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateOrganizationInviteMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    CreateOrganizationInviteMutation,
    CreateOrganizationInviteMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<CreateOrganizationInviteMutation, CreateOrganizationInviteMutationVariables>(
    CreateOrganizationInviteDocument,
    baseOptions,
  )
}
export type CreateOrganizationInviteMutationHookResult = ReturnType<typeof useCreateOrganizationInviteMutation>
export type CreateOrganizationInviteMutationResult = ApolloReactCommon.MutationResult<CreateOrganizationInviteMutation>
export type CreateOrganizationInviteMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateOrganizationInviteMutation,
  CreateOrganizationInviteMutationVariables
>
export const UpdateOrganizationInviteDocument = gql`
  mutation UpdateOrganizationInvite($input: UpdateOrganizationInviteInput!) {
    updateOrganizationInvite(input: $input) {
      changedOrganizationInvite {
        ...organizationInviteFragment
        organization {
          ...organizationFragment
        }
        sender {
          ...userFragment
        }
      }
    }
  }
  ${OrganizationInviteFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
  ${UserFragmentFragmentDoc}
`
export type UpdateOrganizationInviteMutationFn = ApolloReactCommon.MutationFunction<
  UpdateOrganizationInviteMutation,
  UpdateOrganizationInviteMutationVariables
>
export type UpdateOrganizationInviteComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    UpdateOrganizationInviteMutation,
    UpdateOrganizationInviteMutationVariables
  >,
  'mutation'
>

export const UpdateOrganizationInviteComponent = (props: UpdateOrganizationInviteComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateOrganizationInviteMutation, UpdateOrganizationInviteMutationVariables>
    mutation={UpdateOrganizationInviteDocument}
    {...props}
  />
)

export type UpdateOrganizationInviteProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateOrganizationInviteMutation,
  UpdateOrganizationInviteMutationVariables
> &
  TChildProps
export function withUpdateOrganizationInvite<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateOrganizationInviteMutation,
    UpdateOrganizationInviteMutationVariables,
    UpdateOrganizationInviteProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateOrganizationInviteMutation,
    UpdateOrganizationInviteMutationVariables,
    UpdateOrganizationInviteProps<TChildProps>
  >(UpdateOrganizationInviteDocument, {
    alias: 'updateOrganizationInvite',
    ...operationOptions,
  })
}

/**
 * __useUpdateOrganizationInviteMutation__
 *
 * To run a mutation, you first call `useUpdateOrganizationInviteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateOrganizationInviteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateOrganizationInviteMutation, { data, loading, error }] = useUpdateOrganizationInviteMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateOrganizationInviteMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateOrganizationInviteMutation,
    UpdateOrganizationInviteMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<UpdateOrganizationInviteMutation, UpdateOrganizationInviteMutationVariables>(
    UpdateOrganizationInviteDocument,
    baseOptions,
  )
}
export type UpdateOrganizationInviteMutationHookResult = ReturnType<typeof useUpdateOrganizationInviteMutation>
export type UpdateOrganizationInviteMutationResult = ApolloReactCommon.MutationResult<UpdateOrganizationInviteMutation>
export type UpdateOrganizationInviteMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateOrganizationInviteMutation,
  UpdateOrganizationInviteMutationVariables
>
export const AccessibleBrandsDocument = gql`
  query AccessibleBrands($orgId: ID!) {
    viewer {
      accessibleBrands(orgId: $orgId) {
        ...organizationFragment
      }
    }
  }
  ${OrganizationFragmentFragmentDoc}
`
export type AccessibleBrandsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AccessibleBrandsQuery, AccessibleBrandsQueryVariables>,
  'query'
> &
  ({ variables: AccessibleBrandsQueryVariables; skip?: boolean } | { skip: boolean })

export const AccessibleBrandsComponent = (props: AccessibleBrandsComponentProps) => (
  <ApolloReactComponents.Query<AccessibleBrandsQuery, AccessibleBrandsQueryVariables>
    query={AccessibleBrandsDocument}
    {...props}
  />
)

export type AccessibleBrandsProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AccessibleBrandsQuery,
  AccessibleBrandsQueryVariables
> &
  TChildProps
export function withAccessibleBrands<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AccessibleBrandsQuery,
    AccessibleBrandsQueryVariables,
    AccessibleBrandsProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AccessibleBrandsQuery,
    AccessibleBrandsQueryVariables,
    AccessibleBrandsProps<TChildProps>
  >(AccessibleBrandsDocument, {
    alias: 'accessibleBrands',
    ...operationOptions,
  })
}

/**
 * __useAccessibleBrandsQuery__
 *
 * To run a query within a React component, call `useAccessibleBrandsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAccessibleBrandsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAccessibleBrandsQuery({
 *   variables: {
 *      orgId: // value for 'orgId'
 *   },
 * });
 */
export function useAccessibleBrandsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AccessibleBrandsQuery, AccessibleBrandsQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AccessibleBrandsQuery, AccessibleBrandsQueryVariables>(
    AccessibleBrandsDocument,
    baseOptions,
  )
}
export function useAccessibleBrandsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AccessibleBrandsQuery, AccessibleBrandsQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AccessibleBrandsQuery, AccessibleBrandsQueryVariables>(
    AccessibleBrandsDocument,
    baseOptions,
  )
}
export type AccessibleBrandsQueryHookResult = ReturnType<typeof useAccessibleBrandsQuery>
export type AccessibleBrandsLazyQueryHookResult = ReturnType<typeof useAccessibleBrandsLazyQuery>
export type AccessibleBrandsQueryResult = ApolloReactCommon.QueryResult<
  AccessibleBrandsQuery,
  AccessibleBrandsQueryVariables
>
export const DeleteOrganizationUserDocument = gql`
  mutation DeleteOrganizationUser($input: DeleteOrganizationUserInput!) {
    deleteOrganizationUser(input: $input) {
      changedOrganizationUser {
        id
        organization {
          ...organizationFragment
          owner {
            ...userFragment
          }
        }
      }
    }
  }
  ${OrganizationFragmentFragmentDoc}
  ${UserFragmentFragmentDoc}
`
export type DeleteOrganizationUserMutationFn = ApolloReactCommon.MutationFunction<
  DeleteOrganizationUserMutation,
  DeleteOrganizationUserMutationVariables
>
export type DeleteOrganizationUserComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    DeleteOrganizationUserMutation,
    DeleteOrganizationUserMutationVariables
  >,
  'mutation'
>

export const DeleteOrganizationUserComponent = (props: DeleteOrganizationUserComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteOrganizationUserMutation, DeleteOrganizationUserMutationVariables>
    mutation={DeleteOrganizationUserDocument}
    {...props}
  />
)

export type DeleteOrganizationUserProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteOrganizationUserMutation,
  DeleteOrganizationUserMutationVariables
> &
  TChildProps
export function withDeleteOrganizationUser<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteOrganizationUserMutation,
    DeleteOrganizationUserMutationVariables,
    DeleteOrganizationUserProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteOrganizationUserMutation,
    DeleteOrganizationUserMutationVariables,
    DeleteOrganizationUserProps<TChildProps>
  >(DeleteOrganizationUserDocument, {
    alias: 'deleteOrganizationUser',
    ...operationOptions,
  })
}

/**
 * __useDeleteOrganizationUserMutation__
 *
 * To run a mutation, you first call `useDeleteOrganizationUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteOrganizationUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteOrganizationUserMutation, { data, loading, error }] = useDeleteOrganizationUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteOrganizationUserMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    DeleteOrganizationUserMutation,
    DeleteOrganizationUserMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<DeleteOrganizationUserMutation, DeleteOrganizationUserMutationVariables>(
    DeleteOrganizationUserDocument,
    baseOptions,
  )
}
export type DeleteOrganizationUserMutationHookResult = ReturnType<typeof useDeleteOrganizationUserMutation>
export type DeleteOrganizationUserMutationResult = ApolloReactCommon.MutationResult<DeleteOrganizationUserMutation>
export type DeleteOrganizationUserMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteOrganizationUserMutation,
  DeleteOrganizationUserMutationVariables
>
export const AllOrganizationsDocument = gql`
  query AllOrganizations {
    viewer {
      allOrganizations {
        ...organizationFragment
        owner {
          ...userFragment
        }
      }
    }
  }
  ${OrganizationFragmentFragmentDoc}
  ${UserFragmentFragmentDoc}
`
export type AllOrganizationsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllOrganizationsQuery, AllOrganizationsQueryVariables>,
  'query'
>

export const AllOrganizationsComponent = (props: AllOrganizationsComponentProps) => (
  <ApolloReactComponents.Query<AllOrganizationsQuery, AllOrganizationsQueryVariables>
    query={AllOrganizationsDocument}
    {...props}
  />
)

export type AllOrganizationsProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllOrganizationsQuery,
  AllOrganizationsQueryVariables
> &
  TChildProps
export function withAllOrganizations<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllOrganizationsQuery,
    AllOrganizationsQueryVariables,
    AllOrganizationsProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllOrganizationsQuery,
    AllOrganizationsQueryVariables,
    AllOrganizationsProps<TChildProps>
  >(AllOrganizationsDocument, {
    alias: 'allOrganizations',
    ...operationOptions,
  })
}

/**
 * __useAllOrganizationsQuery__
 *
 * To run a query within a React component, call `useAllOrganizationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllOrganizationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllOrganizationsQuery({
 *   variables: {
 *   },
 * });
 */
export function useAllOrganizationsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllOrganizationsQuery, AllOrganizationsQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllOrganizationsQuery, AllOrganizationsQueryVariables>(
    AllOrganizationsDocument,
    baseOptions,
  )
}
export function useAllOrganizationsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllOrganizationsQuery, AllOrganizationsQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllOrganizationsQuery, AllOrganizationsQueryVariables>(
    AllOrganizationsDocument,
    baseOptions,
  )
}
export type AllOrganizationsQueryHookResult = ReturnType<typeof useAllOrganizationsQuery>
export type AllOrganizationsLazyQueryHookResult = ReturnType<typeof useAllOrganizationsLazyQuery>
export type AllOrganizationsQueryResult = ApolloReactCommon.QueryResult<
  AllOrganizationsQuery,
  AllOrganizationsQueryVariables
>
export const SendSendGridStatusUpdateEmailDocument = gql`
  mutation SendSendGridStatusUpdateEmail($input: SendSendGridStatusUpdateInput!) {
    sendSendGridStatusUpdateEmail(input: $input) {
      success
    }
  }
`
export type SendSendGridStatusUpdateEmailMutationFn = ApolloReactCommon.MutationFunction<
  SendSendGridStatusUpdateEmailMutation,
  SendSendGridStatusUpdateEmailMutationVariables
>
export type SendSendGridStatusUpdateEmailComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    SendSendGridStatusUpdateEmailMutation,
    SendSendGridStatusUpdateEmailMutationVariables
  >,
  'mutation'
>

export const SendSendGridStatusUpdateEmailComponent = (props: SendSendGridStatusUpdateEmailComponentProps) => (
  <ApolloReactComponents.Mutation<SendSendGridStatusUpdateEmailMutation, SendSendGridStatusUpdateEmailMutationVariables>
    mutation={SendSendGridStatusUpdateEmailDocument}
    {...props}
  />
)

export type SendSendGridStatusUpdateEmailProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  SendSendGridStatusUpdateEmailMutation,
  SendSendGridStatusUpdateEmailMutationVariables
> &
  TChildProps
export function withSendSendGridStatusUpdateEmail<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    SendSendGridStatusUpdateEmailMutation,
    SendSendGridStatusUpdateEmailMutationVariables,
    SendSendGridStatusUpdateEmailProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    SendSendGridStatusUpdateEmailMutation,
    SendSendGridStatusUpdateEmailMutationVariables,
    SendSendGridStatusUpdateEmailProps<TChildProps>
  >(SendSendGridStatusUpdateEmailDocument, {
    alias: 'sendSendGridStatusUpdateEmail',
    ...operationOptions,
  })
}

/**
 * __useSendSendGridStatusUpdateEmailMutation__
 *
 * To run a mutation, you first call `useSendSendGridStatusUpdateEmailMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSendSendGridStatusUpdateEmailMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [sendSendGridStatusUpdateEmailMutation, { data, loading, error }] = useSendSendGridStatusUpdateEmailMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSendSendGridStatusUpdateEmailMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    SendSendGridStatusUpdateEmailMutation,
    SendSendGridStatusUpdateEmailMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<
    SendSendGridStatusUpdateEmailMutation,
    SendSendGridStatusUpdateEmailMutationVariables
  >(SendSendGridStatusUpdateEmailDocument, baseOptions)
}
export type SendSendGridStatusUpdateEmailMutationHookResult = ReturnType<
  typeof useSendSendGridStatusUpdateEmailMutation
>
export type SendSendGridStatusUpdateEmailMutationResult = ApolloReactCommon.MutationResult<
  SendSendGridStatusUpdateEmailMutation
>
export type SendSendGridStatusUpdateEmailMutationOptions = ApolloReactCommon.BaseMutationOptions<
  SendSendGridStatusUpdateEmailMutation,
  SendSendGridStatusUpdateEmailMutationVariables
>
export const CreateTokenSendEmailDocument = gql`
  mutation CreateTokenSendEmail($email: String) {
    createTokenSendEmail(email: $email)
  }
`
export type CreateTokenSendEmailMutationFn = ApolloReactCommon.MutationFunction<
  CreateTokenSendEmailMutation,
  CreateTokenSendEmailMutationVariables
>
export type CreateTokenSendEmailComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateTokenSendEmailMutation, CreateTokenSendEmailMutationVariables>,
  'mutation'
>

export const CreateTokenSendEmailComponent = (props: CreateTokenSendEmailComponentProps) => (
  <ApolloReactComponents.Mutation<CreateTokenSendEmailMutation, CreateTokenSendEmailMutationVariables>
    mutation={CreateTokenSendEmailDocument}
    {...props}
  />
)

export type CreateTokenSendEmailProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateTokenSendEmailMutation,
  CreateTokenSendEmailMutationVariables
> &
  TChildProps
export function withCreateTokenSendEmail<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateTokenSendEmailMutation,
    CreateTokenSendEmailMutationVariables,
    CreateTokenSendEmailProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateTokenSendEmailMutation,
    CreateTokenSendEmailMutationVariables,
    CreateTokenSendEmailProps<TChildProps>
  >(CreateTokenSendEmailDocument, {
    alias: 'createTokenSendEmail',
    ...operationOptions,
  })
}

/**
 * __useCreateTokenSendEmailMutation__
 *
 * To run a mutation, you first call `useCreateTokenSendEmailMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateTokenSendEmailMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createTokenSendEmailMutation, { data, loading, error }] = useCreateTokenSendEmailMutation({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useCreateTokenSendEmailMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    CreateTokenSendEmailMutation,
    CreateTokenSendEmailMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<CreateTokenSendEmailMutation, CreateTokenSendEmailMutationVariables>(
    CreateTokenSendEmailDocument,
    baseOptions,
  )
}
export type CreateTokenSendEmailMutationHookResult = ReturnType<typeof useCreateTokenSendEmailMutation>
export type CreateTokenSendEmailMutationResult = ApolloReactCommon.MutationResult<CreateTokenSendEmailMutation>
export type CreateTokenSendEmailMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateTokenSendEmailMutation,
  CreateTokenSendEmailMutationVariables
>
export const CreateSiteDocument = gql`
  mutation CreateSite($input: CreateSiteInput!) {
    createSite(input: $input) {
      changedSite {
        id
      }
    }
  }
`
export type CreateSiteMutationFn = ApolloReactCommon.MutationFunction<CreateSiteMutation, CreateSiteMutationVariables>
export type CreateSiteComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateSiteMutation, CreateSiteMutationVariables>,
  'mutation'
>

export const CreateSiteComponent = (props: CreateSiteComponentProps) => (
  <ApolloReactComponents.Mutation<CreateSiteMutation, CreateSiteMutationVariables>
    mutation={CreateSiteDocument}
    {...props}
  />
)

export type CreateSiteProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateSiteMutation,
  CreateSiteMutationVariables
> &
  TChildProps
export function withCreateSite<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateSiteMutation,
    CreateSiteMutationVariables,
    CreateSiteProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateSiteMutation,
    CreateSiteMutationVariables,
    CreateSiteProps<TChildProps>
  >(CreateSiteDocument, {
    alias: 'createSite',
    ...operationOptions,
  })
}

/**
 * __useCreateSiteMutation__
 *
 * To run a mutation, you first call `useCreateSiteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSiteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSiteMutation, { data, loading, error }] = useCreateSiteMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateSiteMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateSiteMutation, CreateSiteMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateSiteMutation, CreateSiteMutationVariables>(CreateSiteDocument, baseOptions)
}
export type CreateSiteMutationHookResult = ReturnType<typeof useCreateSiteMutation>
export type CreateSiteMutationResult = ApolloReactCommon.MutationResult<CreateSiteMutation>
export type CreateSiteMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateSiteMutation,
  CreateSiteMutationVariables
>
export const UpdateSiteDocument = gql`
  mutation UpdateSite($input: UpdateSiteInput!) {
    updateSite(input: $input) {
      changedSite {
        id
        name
      }
    }
  }
`
export type UpdateSiteMutationFn = ApolloReactCommon.MutationFunction<UpdateSiteMutation, UpdateSiteMutationVariables>
export type UpdateSiteComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateSiteMutation, UpdateSiteMutationVariables>,
  'mutation'
>

export const UpdateSiteComponent = (props: UpdateSiteComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateSiteMutation, UpdateSiteMutationVariables>
    mutation={UpdateSiteDocument}
    {...props}
  />
)

export type UpdateSiteProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateSiteMutation,
  UpdateSiteMutationVariables
> &
  TChildProps
export function withUpdateSite<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateSiteMutation,
    UpdateSiteMutationVariables,
    UpdateSiteProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateSiteMutation,
    UpdateSiteMutationVariables,
    UpdateSiteProps<TChildProps>
  >(UpdateSiteDocument, {
    alias: 'updateSite',
    ...operationOptions,
  })
}

/**
 * __useUpdateSiteMutation__
 *
 * To run a mutation, you first call `useUpdateSiteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSiteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateSiteMutation, { data, loading, error }] = useUpdateSiteMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateSiteMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateSiteMutation, UpdateSiteMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateSiteMutation, UpdateSiteMutationVariables>(UpdateSiteDocument, baseOptions)
}
export type UpdateSiteMutationHookResult = ReturnType<typeof useUpdateSiteMutation>
export type UpdateSiteMutationResult = ApolloReactCommon.MutationResult<UpdateSiteMutation>
export type UpdateSiteMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateSiteMutation,
  UpdateSiteMutationVariables
>
export const AllSitesDocument = gql`
  query AllSites($organizationId: ID!) {
    viewer {
      allSites(organizationId: $organizationId) {
        ...siteFragment
        licenses {
          ...licenseFragment
        }
      }
    }
  }
  ${SiteFragmentFragmentDoc}
  ${LicenseFragmentFragmentDoc}
`
export type AllSitesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllSitesQuery, AllSitesQueryVariables>,
  'query'
> &
  ({ variables: AllSitesQueryVariables; skip?: boolean } | { skip: boolean })

export const AllSitesComponent = (props: AllSitesComponentProps) => (
  <ApolloReactComponents.Query<AllSitesQuery, AllSitesQueryVariables> query={AllSitesDocument} {...props} />
)

export type AllSitesProps<TChildProps = {}> = ApolloReactHoc.DataProps<AllSitesQuery, AllSitesQueryVariables> &
  TChildProps
export function withAllSites<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllSitesQuery,
    AllSitesQueryVariables,
    AllSitesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, AllSitesQuery, AllSitesQueryVariables, AllSitesProps<TChildProps>>(
    AllSitesDocument,
    {
      alias: 'allSites',
      ...operationOptions,
    },
  )
}

/**
 * __useAllSitesQuery__
 *
 * To run a query within a React component, call `useAllSitesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllSitesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllSitesQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useAllSitesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllSitesQuery, AllSitesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllSitesQuery, AllSitesQueryVariables>(AllSitesDocument, baseOptions)
}
export function useAllSitesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllSitesQuery, AllSitesQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllSitesQuery, AllSitesQueryVariables>(AllSitesDocument, baseOptions)
}
export type AllSitesQueryHookResult = ReturnType<typeof useAllSitesQuery>
export type AllSitesLazyQueryHookResult = ReturnType<typeof useAllSitesLazyQuery>
export type AllSitesQueryResult = ApolloReactCommon.QueryResult<AllSitesQuery, AllSitesQueryVariables>
export const AllSkusAndSitesDocument = gql`
  query AllSkusAndSites($organizationId: ID!) {
    viewer {
      allSites(organizationId: $organizationId) {
        ...siteFragment
        licenses {
          ...licenseFragment
        }
      }
      accessibleBrands(orgId: $organizationId) {
        id
        name
      }
    }
  }
  ${SiteFragmentFragmentDoc}
  ${LicenseFragmentFragmentDoc}
`
export type AllSkusAndSitesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllSkusAndSitesQuery, AllSkusAndSitesQueryVariables>,
  'query'
> &
  ({ variables: AllSkusAndSitesQueryVariables; skip?: boolean } | { skip: boolean })

export const AllSkusAndSitesComponent = (props: AllSkusAndSitesComponentProps) => (
  <ApolloReactComponents.Query<AllSkusAndSitesQuery, AllSkusAndSitesQueryVariables>
    query={AllSkusAndSitesDocument}
    {...props}
  />
)

export type AllSkusAndSitesProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllSkusAndSitesQuery,
  AllSkusAndSitesQueryVariables
> &
  TChildProps
export function withAllSkusAndSites<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllSkusAndSitesQuery,
    AllSkusAndSitesQueryVariables,
    AllSkusAndSitesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllSkusAndSitesQuery,
    AllSkusAndSitesQueryVariables,
    AllSkusAndSitesProps<TChildProps>
  >(AllSkusAndSitesDocument, {
    alias: 'allSkusAndSites',
    ...operationOptions,
  })
}

/**
 * __useAllSkusAndSitesQuery__
 *
 * To run a query within a React component, call `useAllSkusAndSitesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllSkusAndSitesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllSkusAndSitesQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useAllSkusAndSitesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllSkusAndSitesQuery, AllSkusAndSitesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllSkusAndSitesQuery, AllSkusAndSitesQueryVariables>(
    AllSkusAndSitesDocument,
    baseOptions,
  )
}
export function useAllSkusAndSitesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllSkusAndSitesQuery, AllSkusAndSitesQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllSkusAndSitesQuery, AllSkusAndSitesQueryVariables>(
    AllSkusAndSitesDocument,
    baseOptions,
  )
}
export type AllSkusAndSitesQueryHookResult = ReturnType<typeof useAllSkusAndSitesQuery>
export type AllSkusAndSitesLazyQueryHookResult = ReturnType<typeof useAllSkusAndSitesLazyQuery>
export type AllSkusAndSitesQueryResult = ApolloReactCommon.QueryResult<
  AllSkusAndSitesQuery,
  AllSkusAndSitesQueryVariables
>
export const CreateSkuDocument = gql`
  mutation CreateSku($input: CreateSkuInput!) {
    createSku(input: $input) {
      changedSku {
        id
      }
    }
  }
`
export type CreateSkuMutationFn = ApolloReactCommon.MutationFunction<CreateSkuMutation, CreateSkuMutationVariables>
export type CreateSkuComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateSkuMutation, CreateSkuMutationVariables>,
  'mutation'
>

export const CreateSkuComponent = (props: CreateSkuComponentProps) => (
  <ApolloReactComponents.Mutation<CreateSkuMutation, CreateSkuMutationVariables>
    mutation={CreateSkuDocument}
    {...props}
  />
)

export type CreateSkuProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateSkuMutation,
  CreateSkuMutationVariables
> &
  TChildProps
export function withCreateSku<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateSkuMutation,
    CreateSkuMutationVariables,
    CreateSkuProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateSkuMutation,
    CreateSkuMutationVariables,
    CreateSkuProps<TChildProps>
  >(CreateSkuDocument, {
    alias: 'createSku',
    ...operationOptions,
  })
}

/**
 * __useCreateSkuMutation__
 *
 * To run a mutation, you first call `useCreateSkuMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSkuMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSkuMutation, { data, loading, error }] = useCreateSkuMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateSkuMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateSkuMutation, CreateSkuMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateSkuMutation, CreateSkuMutationVariables>(CreateSkuDocument, baseOptions)
}
export type CreateSkuMutationHookResult = ReturnType<typeof useCreateSkuMutation>
export type CreateSkuMutationResult = ApolloReactCommon.MutationResult<CreateSkuMutation>
export type CreateSkuMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateSkuMutation,
  CreateSkuMutationVariables
>
export const UpdateSkuDocument = gql`
  mutation UpdateSku($input: UpdateSkuInput!) {
    updateSku(input: $input) {
      changedSku {
        id
      }
    }
  }
`
export type UpdateSkuMutationFn = ApolloReactCommon.MutationFunction<UpdateSkuMutation, UpdateSkuMutationVariables>
export type UpdateSkuComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateSkuMutation, UpdateSkuMutationVariables>,
  'mutation'
>

export const UpdateSkuComponent = (props: UpdateSkuComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateSkuMutation, UpdateSkuMutationVariables>
    mutation={UpdateSkuDocument}
    {...props}
  />
)

export type UpdateSkuProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateSkuMutation,
  UpdateSkuMutationVariables
> &
  TChildProps
export function withUpdateSku<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateSkuMutation,
    UpdateSkuMutationVariables,
    UpdateSkuProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateSkuMutation,
    UpdateSkuMutationVariables,
    UpdateSkuProps<TChildProps>
  >(UpdateSkuDocument, {
    alias: 'updateSku',
    ...operationOptions,
  })
}

/**
 * __useUpdateSkuMutation__
 *
 * To run a mutation, you first call `useUpdateSkuMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSkuMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateSkuMutation, { data, loading, error }] = useUpdateSkuMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateSkuMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateSkuMutation, UpdateSkuMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateSkuMutation, UpdateSkuMutationVariables>(UpdateSkuDocument, baseOptions)
}
export type UpdateSkuMutationHookResult = ReturnType<typeof useUpdateSkuMutation>
export type UpdateSkuMutationResult = ApolloReactCommon.MutationResult<UpdateSkuMutation>
export type UpdateSkuMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateSkuMutation,
  UpdateSkuMutationVariables
>
export const DeleteSkuDocument = gql`
  mutation DeleteSku($input: DeleteSkuInput!) {
    deleteSku(input: $input) {
      changedSku {
        id
      }
    }
  }
`
export type DeleteSkuMutationFn = ApolloReactCommon.MutationFunction<DeleteSkuMutation, DeleteSkuMutationVariables>
export type DeleteSkuComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteSkuMutation, DeleteSkuMutationVariables>,
  'mutation'
>

export const DeleteSkuComponent = (props: DeleteSkuComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteSkuMutation, DeleteSkuMutationVariables>
    mutation={DeleteSkuDocument}
    {...props}
  />
)

export type DeleteSkuProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteSkuMutation,
  DeleteSkuMutationVariables
> &
  TChildProps
export function withDeleteSku<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteSkuMutation,
    DeleteSkuMutationVariables,
    DeleteSkuProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteSkuMutation,
    DeleteSkuMutationVariables,
    DeleteSkuProps<TChildProps>
  >(DeleteSkuDocument, {
    alias: 'deleteSku',
    ...operationOptions,
  })
}

/**
 * __useDeleteSkuMutation__
 *
 * To run a mutation, you first call `useDeleteSkuMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteSkuMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteSkuMutation, { data, loading, error }] = useDeleteSkuMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteSkuMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteSkuMutation, DeleteSkuMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteSkuMutation, DeleteSkuMutationVariables>(DeleteSkuDocument, baseOptions)
}
export type DeleteSkuMutationHookResult = ReturnType<typeof useDeleteSkuMutation>
export type DeleteSkuMutationResult = ApolloReactCommon.MutationResult<DeleteSkuMutation>
export type DeleteSkuMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteSkuMutation,
  DeleteSkuMutationVariables
>
export const CreateSkuAndBatchDocument = gql`
  mutation CreateSkuAndBatch($input: CreateSkuInput!) {
    createSkuAndSkuBatch(input: $input) {
      changedSku {
        id
      }
    }
  }
`
export type CreateSkuAndBatchMutationFn = ApolloReactCommon.MutationFunction<
  CreateSkuAndBatchMutation,
  CreateSkuAndBatchMutationVariables
>
export type CreateSkuAndBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateSkuAndBatchMutation, CreateSkuAndBatchMutationVariables>,
  'mutation'
>

export const CreateSkuAndBatchComponent = (props: CreateSkuAndBatchComponentProps) => (
  <ApolloReactComponents.Mutation<CreateSkuAndBatchMutation, CreateSkuAndBatchMutationVariables>
    mutation={CreateSkuAndBatchDocument}
    {...props}
  />
)

export type CreateSkuAndBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateSkuAndBatchMutation,
  CreateSkuAndBatchMutationVariables
> &
  TChildProps
export function withCreateSkuAndBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateSkuAndBatchMutation,
    CreateSkuAndBatchMutationVariables,
    CreateSkuAndBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateSkuAndBatchMutation,
    CreateSkuAndBatchMutationVariables,
    CreateSkuAndBatchProps<TChildProps>
  >(CreateSkuAndBatchDocument, {
    alias: 'createSkuAndBatch',
    ...operationOptions,
  })
}

/**
 * __useCreateSkuAndBatchMutation__
 *
 * To run a mutation, you first call `useCreateSkuAndBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSkuAndBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSkuAndBatchMutation, { data, loading, error }] = useCreateSkuAndBatchMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateSkuAndBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateSkuAndBatchMutation, CreateSkuAndBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateSkuAndBatchMutation, CreateSkuAndBatchMutationVariables>(
    CreateSkuAndBatchDocument,
    baseOptions,
  )
}
export type CreateSkuAndBatchMutationHookResult = ReturnType<typeof useCreateSkuAndBatchMutation>
export type CreateSkuAndBatchMutationResult = ApolloReactCommon.MutationResult<CreateSkuAndBatchMutation>
export type CreateSkuAndBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateSkuAndBatchMutation,
  CreateSkuAndBatchMutationVariables
>
export const RestoreSkuDocument = gql`
  mutation RestoreSku($skuId: ID!) {
    restoreSku(skuId: $skuId) {
      id
    }
  }
`
export type RestoreSkuMutationFn = ApolloReactCommon.MutationFunction<RestoreSkuMutation, RestoreSkuMutationVariables>
export type RestoreSkuComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<RestoreSkuMutation, RestoreSkuMutationVariables>,
  'mutation'
>

export const RestoreSkuComponent = (props: RestoreSkuComponentProps) => (
  <ApolloReactComponents.Mutation<RestoreSkuMutation, RestoreSkuMutationVariables>
    mutation={RestoreSkuDocument}
    {...props}
  />
)

export type RestoreSkuProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  RestoreSkuMutation,
  RestoreSkuMutationVariables
> &
  TChildProps
export function withRestoreSku<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    RestoreSkuMutation,
    RestoreSkuMutationVariables,
    RestoreSkuProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    RestoreSkuMutation,
    RestoreSkuMutationVariables,
    RestoreSkuProps<TChildProps>
  >(RestoreSkuDocument, {
    alias: 'restoreSku',
    ...operationOptions,
  })
}

/**
 * __useRestoreSkuMutation__
 *
 * To run a mutation, you first call `useRestoreSkuMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRestoreSkuMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [restoreSkuMutation, { data, loading, error }] = useRestoreSkuMutation({
 *   variables: {
 *      skuId: // value for 'skuId'
 *   },
 * });
 */
export function useRestoreSkuMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<RestoreSkuMutation, RestoreSkuMutationVariables>,
) {
  return ApolloReactHooks.useMutation<RestoreSkuMutation, RestoreSkuMutationVariables>(RestoreSkuDocument, baseOptions)
}
export type RestoreSkuMutationHookResult = ReturnType<typeof useRestoreSkuMutation>
export type RestoreSkuMutationResult = ApolloReactCommon.MutationResult<RestoreSkuMutation>
export type RestoreSkuMutationOptions = ApolloReactCommon.BaseMutationOptions<
  RestoreSkuMutation,
  RestoreSkuMutationVariables
>
export const GetSkuDocument = gql`
  query GetSku($id: ID!) {
    getSku(id: $id) {
      ...skuFragment
      organization {
        ...organizationFragment
      }
    }
  }
  ${SkuFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
`
export type GetSkuComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetSkuQuery, GetSkuQueryVariables>,
  'query'
> &
  ({ variables: GetSkuQueryVariables; skip?: boolean } | { skip: boolean })

export const GetSkuComponent = (props: GetSkuComponentProps) => (
  <ApolloReactComponents.Query<GetSkuQuery, GetSkuQueryVariables> query={GetSkuDocument} {...props} />
)

export type GetSkuProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetSkuQuery, GetSkuQueryVariables> & TChildProps
export function withGetSku<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetSkuQuery,
    GetSkuQueryVariables,
    GetSkuProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, GetSkuQuery, GetSkuQueryVariables, GetSkuProps<TChildProps>>(GetSkuDocument, {
    alias: 'getSku',
    ...operationOptions,
  })
}

/**
 * __useGetSkuQuery__
 *
 * To run a query within a React component, call `useGetSkuQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSkuQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSkuQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetSkuQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetSkuQuery, GetSkuQueryVariables>) {
  return ApolloReactHooks.useQuery<GetSkuQuery, GetSkuQueryVariables>(GetSkuDocument, baseOptions)
}
export function useGetSkuLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetSkuQuery, GetSkuQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetSkuQuery, GetSkuQueryVariables>(GetSkuDocument, baseOptions)
}
export type GetSkuQueryHookResult = ReturnType<typeof useGetSkuQuery>
export type GetSkuLazyQueryHookResult = ReturnType<typeof useGetSkuLazyQuery>
export type GetSkuQueryResult = ApolloReactCommon.QueryResult<GetSkuQuery, GetSkuQueryVariables>
export const AllSkusDocument = gql`
  query AllSkus(
    $organizationId: ID
    $pageInfo: PageInfoInput
    $isArchived: Boolean
    $isInventoryTab: Boolean
    $search: String
  ) {
    viewer {
      allSkus(
        organizationId: $organizationId
        pageInfo: $pageInfo
        isArchived: $isArchived
        isInventoryTab: $isInventoryTab
        search: $search
      ) {
        results {
          id
          isArchived
          code
          name
          pricePerUnit
          unit
          inventoryType
          inventoryClass
          inventoryCategory
          isExciseTaxable
          organization {
            id
            name
            doingBusinessAs
          }
          skuBatches {
            id
            deletedAt
            batch {
              id
              code
            }
          }
        }
        pageInfo {
          totalNumItems
          totalNumPages
        }
      }
    }
  }
`
export type AllSkusComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllSkusQuery, AllSkusQueryVariables>,
  'query'
>

export const AllSkusComponent = (props: AllSkusComponentProps) => (
  <ApolloReactComponents.Query<AllSkusQuery, AllSkusQueryVariables> query={AllSkusDocument} {...props} />
)

export type AllSkusProps<TChildProps = {}> = ApolloReactHoc.DataProps<AllSkusQuery, AllSkusQueryVariables> & TChildProps
export function withAllSkus<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllSkusQuery,
    AllSkusQueryVariables,
    AllSkusProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, AllSkusQuery, AllSkusQueryVariables, AllSkusProps<TChildProps>>(
    AllSkusDocument,
    {
      alias: 'allSkus',
      ...operationOptions,
    },
  )
}

/**
 * __useAllSkusQuery__
 *
 * To run a query within a React component, call `useAllSkusQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllSkusQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllSkusQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      pageInfo: // value for 'pageInfo'
 *      isArchived: // value for 'isArchived'
 *      isInventoryTab: // value for 'isInventoryTab'
 *      search: // value for 'search'
 *   },
 * });
 */
export function useAllSkusQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<AllSkusQuery, AllSkusQueryVariables>) {
  return ApolloReactHooks.useQuery<AllSkusQuery, AllSkusQueryVariables>(AllSkusDocument, baseOptions)
}
export function useAllSkusLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllSkusQuery, AllSkusQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllSkusQuery, AllSkusQueryVariables>(AllSkusDocument, baseOptions)
}
export type AllSkusQueryHookResult = ReturnType<typeof useAllSkusQuery>
export type AllSkusLazyQueryHookResult = ReturnType<typeof useAllSkusLazyQuery>
export type AllSkusQueryResult = ApolloReactCommon.QueryResult<AllSkusQuery, AllSkusQueryVariables>
export const AllProductsDocument = gql`
  query AllProducts($organizationId: ID!) {
    viewer {
      allSkus(organizationId: $organizationId) {
        results {
          id
          code
          name
          pricePerUnit
          unit
          inventoryType
          inventoryClass
          inventoryCategory
          isExciseTaxable
          skuBatches {
            id
            deletedAt
            batch {
              id
              code
            }
          }
        }
      }
    }
  }
`
export type AllProductsComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllProductsQuery, AllProductsQueryVariables>,
  'query'
> &
  ({ variables: AllProductsQueryVariables; skip?: boolean } | { skip: boolean })

export const AllProductsComponent = (props: AllProductsComponentProps) => (
  <ApolloReactComponents.Query<AllProductsQuery, AllProductsQueryVariables> query={AllProductsDocument} {...props} />
)

export type AllProductsProps<TChildProps = {}> = ApolloReactHoc.DataProps<AllProductsQuery, AllProductsQueryVariables> &
  TChildProps
export function withAllProducts<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllProductsQuery,
    AllProductsQueryVariables,
    AllProductsProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, AllProductsQuery, AllProductsQueryVariables, AllProductsProps<TChildProps>>(
    AllProductsDocument,
    {
      alias: 'allProducts',
      ...operationOptions,
    },
  )
}

/**
 * __useAllProductsQuery__
 *
 * To run a query within a React component, call `useAllProductsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllProductsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllProductsQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useAllProductsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllProductsQuery, AllProductsQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllProductsQuery, AllProductsQueryVariables>(AllProductsDocument, baseOptions)
}
export function useAllProductsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllProductsQuery, AllProductsQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllProductsQuery, AllProductsQueryVariables>(AllProductsDocument, baseOptions)
}
export type AllProductsQueryHookResult = ReturnType<typeof useAllProductsQuery>
export type AllProductsLazyQueryHookResult = ReturnType<typeof useAllProductsLazyQuery>
export type AllProductsQueryResult = ApolloReactCommon.QueryResult<AllProductsQuery, AllProductsQueryVariables>
export const GetSkuBatchDocument = gql`
  query GetSkuBatch($id: ID!) {
    viewer {
      allWarehouses {
        ...warehouseFragment
        site {
          ...siteFragment
        }
      }
    }
    getSkuBatch(id: $id) {
      ...skuBatchFragment
      batch {
        ...batchFragment
      }
      sku {
        ...skuFragment
        organization {
          ...organizationFragment
        }
      }
    }
  }
  ${WarehouseFragmentFragmentDoc}
  ${SiteFragmentFragmentDoc}
  ${SkuBatchFragmentFragmentDoc}
  ${BatchFragmentFragmentDoc}
  ${SkuFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
`
export type GetSkuBatchComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetSkuBatchQuery, GetSkuBatchQueryVariables>,
  'query'
> &
  ({ variables: GetSkuBatchQueryVariables; skip?: boolean } | { skip: boolean })

export const GetSkuBatchComponent = (props: GetSkuBatchComponentProps) => (
  <ApolloReactComponents.Query<GetSkuBatchQuery, GetSkuBatchQueryVariables> query={GetSkuBatchDocument} {...props} />
)

export type GetSkuBatchProps<TChildProps = {}> = ApolloReactHoc.DataProps<GetSkuBatchQuery, GetSkuBatchQueryVariables> &
  TChildProps
export function withGetSkuBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetSkuBatchQuery,
    GetSkuBatchQueryVariables,
    GetSkuBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, GetSkuBatchQuery, GetSkuBatchQueryVariables, GetSkuBatchProps<TChildProps>>(
    GetSkuBatchDocument,
    {
      alias: 'getSkuBatch',
      ...operationOptions,
    },
  )
}

/**
 * __useGetSkuBatchQuery__
 *
 * To run a query within a React component, call `useGetSkuBatchQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSkuBatchQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSkuBatchQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetSkuBatchQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<GetSkuBatchQuery, GetSkuBatchQueryVariables>,
) {
  return ApolloReactHooks.useQuery<GetSkuBatchQuery, GetSkuBatchQueryVariables>(GetSkuBatchDocument, baseOptions)
}
export function useGetSkuBatchLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetSkuBatchQuery, GetSkuBatchQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetSkuBatchQuery, GetSkuBatchQueryVariables>(GetSkuBatchDocument, baseOptions)
}
export type GetSkuBatchQueryHookResult = ReturnType<typeof useGetSkuBatchQuery>
export type GetSkuBatchLazyQueryHookResult = ReturnType<typeof useGetSkuBatchLazyQuery>
export type GetSkuBatchQueryResult = ApolloReactCommon.QueryResult<GetSkuBatchQuery, GetSkuBatchQueryVariables>
export const AccessibleSkuBatchesDocument = gql`
  query AccessibleSkuBatches($brandId: ID, $search: String, $organizationId: ID!, $pageInfo: PageInfoInput) {
    viewer {
      accessibleSkuBatches(search: $search, brandId: $brandId, pageInfo: $pageInfo, orgId: $organizationId) {
        results {
          ...skuBatchFragment
          sku {
            ...skuFragment
            organization {
              ...organizationFragment
            }
          }
          batch {
            ...batchFragment
          }
        }
      }
    }
  }
  ${SkuBatchFragmentFragmentDoc}
  ${SkuFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
  ${BatchFragmentFragmentDoc}
`
export type AccessibleSkuBatchesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AccessibleSkuBatchesQuery, AccessibleSkuBatchesQueryVariables>,
  'query'
> &
  ({ variables: AccessibleSkuBatchesQueryVariables; skip?: boolean } | { skip: boolean })

export const AccessibleSkuBatchesComponent = (props: AccessibleSkuBatchesComponentProps) => (
  <ApolloReactComponents.Query<AccessibleSkuBatchesQuery, AccessibleSkuBatchesQueryVariables>
    query={AccessibleSkuBatchesDocument}
    {...props}
  />
)

export type AccessibleSkuBatchesProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AccessibleSkuBatchesQuery,
  AccessibleSkuBatchesQueryVariables
> &
  TChildProps
export function withAccessibleSkuBatches<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AccessibleSkuBatchesQuery,
    AccessibleSkuBatchesQueryVariables,
    AccessibleSkuBatchesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AccessibleSkuBatchesQuery,
    AccessibleSkuBatchesQueryVariables,
    AccessibleSkuBatchesProps<TChildProps>
  >(AccessibleSkuBatchesDocument, {
    alias: 'accessibleSkuBatches',
    ...operationOptions,
  })
}

/**
 * __useAccessibleSkuBatchesQuery__
 *
 * To run a query within a React component, call `useAccessibleSkuBatchesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAccessibleSkuBatchesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAccessibleSkuBatchesQuery({
 *   variables: {
 *      brandId: // value for 'brandId'
 *      search: // value for 'search'
 *      organizationId: // value for 'organizationId'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useAccessibleSkuBatchesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AccessibleSkuBatchesQuery, AccessibleSkuBatchesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AccessibleSkuBatchesQuery, AccessibleSkuBatchesQueryVariables>(
    AccessibleSkuBatchesDocument,
    baseOptions,
  )
}
export function useAccessibleSkuBatchesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AccessibleSkuBatchesQuery, AccessibleSkuBatchesQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AccessibleSkuBatchesQuery, AccessibleSkuBatchesQueryVariables>(
    AccessibleSkuBatchesDocument,
    baseOptions,
  )
}
export type AccessibleSkuBatchesQueryHookResult = ReturnType<typeof useAccessibleSkuBatchesQuery>
export type AccessibleSkuBatchesLazyQueryHookResult = ReturnType<typeof useAccessibleSkuBatchesLazyQuery>
export type AccessibleSkuBatchesQueryResult = ApolloReactCommon.QueryResult<
  AccessibleSkuBatchesQuery,
  AccessibleSkuBatchesQueryVariables
>
export const InventorySkuBatchesTabDocument = gql`
  query InventorySkuBatchesTab($organizationId: ID, $isArchived: Boolean, $search: String, $pageInfo: PageInfoInput) {
    viewer {
      allSkuBatches(
        organizationId: $organizationId
        isArchived: $isArchived
        search: $search
        pageInfo: $pageInfo
        isInventoryTab: true
      ) {
        results {
          id
          deletedAt
          inventoryValues {
            LA {
              counted
              incoming
              outgoing
              allocated
              available
            }
            OAK {
              counted
              incoming
              outgoing
              allocated
              available
            }
          }
          sku {
            ...skuFragment
            batches {
              id
              code
            }
            organization {
              ...organizationFragment
            }
          }
          batch {
            id
            code
            manifestGDriveFileId
            license {
              ...licenseFragment
            }
          }
          inventoryItems {
            id
            counted
            warehouse {
              id
              site {
                id
                name
              }
            }
          }
          titleholder {
            id
            type
            titleholderLicense {
              ...licenseFragment
            }
            titleholderOrganization {
              ...organizationFragment
              licenses {
                ...licenseFragment
              }
            }
          }
        }
        pageInfo {
          totalNumItems
          totalNumPages
        }
      }
    }
  }
  ${SkuFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
  ${LicenseFragmentFragmentDoc}
`
export type InventorySkuBatchesTabComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<InventorySkuBatchesTabQuery, InventorySkuBatchesTabQueryVariables>,
  'query'
>

export const InventorySkuBatchesTabComponent = (props: InventorySkuBatchesTabComponentProps) => (
  <ApolloReactComponents.Query<InventorySkuBatchesTabQuery, InventorySkuBatchesTabQueryVariables>
    query={InventorySkuBatchesTabDocument}
    {...props}
  />
)

export type InventorySkuBatchesTabProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  InventorySkuBatchesTabQuery,
  InventorySkuBatchesTabQueryVariables
> &
  TChildProps
export function withInventorySkuBatchesTab<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    InventorySkuBatchesTabQuery,
    InventorySkuBatchesTabQueryVariables,
    InventorySkuBatchesTabProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    InventorySkuBatchesTabQuery,
    InventorySkuBatchesTabQueryVariables,
    InventorySkuBatchesTabProps<TChildProps>
  >(InventorySkuBatchesTabDocument, {
    alias: 'inventorySkuBatchesTab',
    ...operationOptions,
  })
}

/**
 * __useInventorySkuBatchesTabQuery__
 *
 * To run a query within a React component, call `useInventorySkuBatchesTabQuery` and pass it any options that fit your needs.
 * When your component renders, `useInventorySkuBatchesTabQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInventorySkuBatchesTabQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      isArchived: // value for 'isArchived'
 *      search: // value for 'search'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useInventorySkuBatchesTabQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<InventorySkuBatchesTabQuery, InventorySkuBatchesTabQueryVariables>,
) {
  return ApolloReactHooks.useQuery<InventorySkuBatchesTabQuery, InventorySkuBatchesTabQueryVariables>(
    InventorySkuBatchesTabDocument,
    baseOptions,
  )
}
export function useInventorySkuBatchesTabLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    InventorySkuBatchesTabQuery,
    InventorySkuBatchesTabQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<InventorySkuBatchesTabQuery, InventorySkuBatchesTabQueryVariables>(
    InventorySkuBatchesTabDocument,
    baseOptions,
  )
}
export type InventorySkuBatchesTabQueryHookResult = ReturnType<typeof useInventorySkuBatchesTabQuery>
export type InventorySkuBatchesTabLazyQueryHookResult = ReturnType<typeof useInventorySkuBatchesTabLazyQuery>
export type InventorySkuBatchesTabQueryResult = ApolloReactCommon.QueryResult<
  InventorySkuBatchesTabQuery,
  InventorySkuBatchesTabQueryVariables
>
export const AllSkuBatchesForCsvDocument = gql`
  query AllSkuBatchesForCSV($search: String, $organizationId: ID!, $isArchived: Boolean, $pageInfo: PageInfoInput) {
    viewer {
      allSkuBatches(search: $search, organizationId: $organizationId, isArchived: $isArchived, pageInfo: $pageInfo) {
        results {
          ...skuBatchFragment
          inventoryValues {
            LA {
              counted
              incoming
              outgoing
              allocated
              available
            }
            OAK {
              counted
              incoming
              outgoing
              allocated
              available
            }
          }
          sku {
            ...skuFragment
          }
          batch {
            ...batchFragment
          }
        }
      }
    }
  }
  ${SkuBatchFragmentFragmentDoc}
  ${SkuFragmentFragmentDoc}
  ${BatchFragmentFragmentDoc}
`
export type AllSkuBatchesForCsvComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllSkuBatchesForCsvQuery, AllSkuBatchesForCsvQueryVariables>,
  'query'
> &
  ({ variables: AllSkuBatchesForCsvQueryVariables; skip?: boolean } | { skip: boolean })

export const AllSkuBatchesForCsvComponent = (props: AllSkuBatchesForCsvComponentProps) => (
  <ApolloReactComponents.Query<AllSkuBatchesForCsvQuery, AllSkuBatchesForCsvQueryVariables>
    query={AllSkuBatchesForCsvDocument}
    {...props}
  />
)

export type AllSkuBatchesForCsvProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllSkuBatchesForCsvQuery,
  AllSkuBatchesForCsvQueryVariables
> &
  TChildProps
export function withAllSkuBatchesForCsv<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllSkuBatchesForCsvQuery,
    AllSkuBatchesForCsvQueryVariables,
    AllSkuBatchesForCsvProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllSkuBatchesForCsvQuery,
    AllSkuBatchesForCsvQueryVariables,
    AllSkuBatchesForCsvProps<TChildProps>
  >(AllSkuBatchesForCsvDocument, {
    alias: 'allSkuBatchesForCsv',
    ...operationOptions,
  })
}

/**
 * __useAllSkuBatchesForCsvQuery__
 *
 * To run a query within a React component, call `useAllSkuBatchesForCsvQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllSkuBatchesForCsvQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllSkuBatchesForCsvQuery({
 *   variables: {
 *      search: // value for 'search'
 *      organizationId: // value for 'organizationId'
 *      isArchived: // value for 'isArchived'
 *      pageInfo: // value for 'pageInfo'
 *   },
 * });
 */
export function useAllSkuBatchesForCsvQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllSkuBatchesForCsvQuery, AllSkuBatchesForCsvQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllSkuBatchesForCsvQuery, AllSkuBatchesForCsvQueryVariables>(
    AllSkuBatchesForCsvDocument,
    baseOptions,
  )
}
export function useAllSkuBatchesForCsvLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllSkuBatchesForCsvQuery, AllSkuBatchesForCsvQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllSkuBatchesForCsvQuery, AllSkuBatchesForCsvQueryVariables>(
    AllSkuBatchesForCsvDocument,
    baseOptions,
  )
}
export type AllSkuBatchesForCsvQueryHookResult = ReturnType<typeof useAllSkuBatchesForCsvQuery>
export type AllSkuBatchesForCsvLazyQueryHookResult = ReturnType<typeof useAllSkuBatchesForCsvLazyQuery>
export type AllSkuBatchesForCsvQueryResult = ApolloReactCommon.QueryResult<
  AllSkuBatchesForCsvQuery,
  AllSkuBatchesForCsvQueryVariables
>
export const CreateSkuBatchDocument = gql`
  mutation CreateSkuBatch($batchId: ID, $skuId: ID!, $titleholderId: ID!) {
    createSkuBatch(batchId: $batchId, skuId: $skuId, titleholderId: $titleholderId) {
      id
    }
  }
`
export type CreateSkuBatchMutationFn = ApolloReactCommon.MutationFunction<
  CreateSkuBatchMutation,
  CreateSkuBatchMutationVariables
>
export type CreateSkuBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateSkuBatchMutation, CreateSkuBatchMutationVariables>,
  'mutation'
>

export const CreateSkuBatchComponent = (props: CreateSkuBatchComponentProps) => (
  <ApolloReactComponents.Mutation<CreateSkuBatchMutation, CreateSkuBatchMutationVariables>
    mutation={CreateSkuBatchDocument}
    {...props}
  />
)

export type CreateSkuBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateSkuBatchMutation,
  CreateSkuBatchMutationVariables
> &
  TChildProps
export function withCreateSkuBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateSkuBatchMutation,
    CreateSkuBatchMutationVariables,
    CreateSkuBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateSkuBatchMutation,
    CreateSkuBatchMutationVariables,
    CreateSkuBatchProps<TChildProps>
  >(CreateSkuBatchDocument, {
    alias: 'createSkuBatch',
    ...operationOptions,
  })
}

/**
 * __useCreateSkuBatchMutation__
 *
 * To run a mutation, you first call `useCreateSkuBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSkuBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSkuBatchMutation, { data, loading, error }] = useCreateSkuBatchMutation({
 *   variables: {
 *      batchId: // value for 'batchId'
 *      skuId: // value for 'skuId'
 *      titleholderId: // value for 'titleholderId'
 *   },
 * });
 */
export function useCreateSkuBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateSkuBatchMutation, CreateSkuBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateSkuBatchMutation, CreateSkuBatchMutationVariables>(
    CreateSkuBatchDocument,
    baseOptions,
  )
}
export type CreateSkuBatchMutationHookResult = ReturnType<typeof useCreateSkuBatchMutation>
export type CreateSkuBatchMutationResult = ApolloReactCommon.MutationResult<CreateSkuBatchMutation>
export type CreateSkuBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateSkuBatchMutation,
  CreateSkuBatchMutationVariables
>
export const CreateEmptySkuBatchDocument = gql`
  mutation CreateEmptySkuBatch($skuId: ID!) {
    createEmptySkuBatch(skuId: $skuId) {
      id
    }
  }
`
export type CreateEmptySkuBatchMutationFn = ApolloReactCommon.MutationFunction<
  CreateEmptySkuBatchMutation,
  CreateEmptySkuBatchMutationVariables
>
export type CreateEmptySkuBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateEmptySkuBatchMutation, CreateEmptySkuBatchMutationVariables>,
  'mutation'
>

export const CreateEmptySkuBatchComponent = (props: CreateEmptySkuBatchComponentProps) => (
  <ApolloReactComponents.Mutation<CreateEmptySkuBatchMutation, CreateEmptySkuBatchMutationVariables>
    mutation={CreateEmptySkuBatchDocument}
    {...props}
  />
)

export type CreateEmptySkuBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateEmptySkuBatchMutation,
  CreateEmptySkuBatchMutationVariables
> &
  TChildProps
export function withCreateEmptySkuBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateEmptySkuBatchMutation,
    CreateEmptySkuBatchMutationVariables,
    CreateEmptySkuBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateEmptySkuBatchMutation,
    CreateEmptySkuBatchMutationVariables,
    CreateEmptySkuBatchProps<TChildProps>
  >(CreateEmptySkuBatchDocument, {
    alias: 'createEmptySkuBatch',
    ...operationOptions,
  })
}

/**
 * __useCreateEmptySkuBatchMutation__
 *
 * To run a mutation, you first call `useCreateEmptySkuBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateEmptySkuBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createEmptySkuBatchMutation, { data, loading, error }] = useCreateEmptySkuBatchMutation({
 *   variables: {
 *      skuId: // value for 'skuId'
 *   },
 * });
 */
export function useCreateEmptySkuBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateEmptySkuBatchMutation, CreateEmptySkuBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateEmptySkuBatchMutation, CreateEmptySkuBatchMutationVariables>(
    CreateEmptySkuBatchDocument,
    baseOptions,
  )
}
export type CreateEmptySkuBatchMutationHookResult = ReturnType<typeof useCreateEmptySkuBatchMutation>
export type CreateEmptySkuBatchMutationResult = ApolloReactCommon.MutationResult<CreateEmptySkuBatchMutation>
export type CreateEmptySkuBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateEmptySkuBatchMutation,
  CreateEmptySkuBatchMutationVariables
>
export const DeleteSkuBatchDocument = gql`
  mutation DeleteSkuBatch($id: ID!) {
    deleteSkuBatch(id: $id) {
      id
    }
  }
`
export type DeleteSkuBatchMutationFn = ApolloReactCommon.MutationFunction<
  DeleteSkuBatchMutation,
  DeleteSkuBatchMutationVariables
>
export type DeleteSkuBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteSkuBatchMutation, DeleteSkuBatchMutationVariables>,
  'mutation'
>

export const DeleteSkuBatchComponent = (props: DeleteSkuBatchComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteSkuBatchMutation, DeleteSkuBatchMutationVariables>
    mutation={DeleteSkuBatchDocument}
    {...props}
  />
)

export type DeleteSkuBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteSkuBatchMutation,
  DeleteSkuBatchMutationVariables
> &
  TChildProps
export function withDeleteSkuBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteSkuBatchMutation,
    DeleteSkuBatchMutationVariables,
    DeleteSkuBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteSkuBatchMutation,
    DeleteSkuBatchMutationVariables,
    DeleteSkuBatchProps<TChildProps>
  >(DeleteSkuBatchDocument, {
    alias: 'deleteSkuBatch',
    ...operationOptions,
  })
}

/**
 * __useDeleteSkuBatchMutation__
 *
 * To run a mutation, you first call `useDeleteSkuBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteSkuBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteSkuBatchMutation, { data, loading, error }] = useDeleteSkuBatchMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteSkuBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteSkuBatchMutation, DeleteSkuBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteSkuBatchMutation, DeleteSkuBatchMutationVariables>(
    DeleteSkuBatchDocument,
    baseOptions,
  )
}
export type DeleteSkuBatchMutationHookResult = ReturnType<typeof useDeleteSkuBatchMutation>
export type DeleteSkuBatchMutationResult = ApolloReactCommon.MutationResult<DeleteSkuBatchMutation>
export type DeleteSkuBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteSkuBatchMutation,
  DeleteSkuBatchMutationVariables
>
export const RestoreSkuBatchDocument = gql`
  mutation RestoreSkuBatch($id: ID!) {
    restoreSkuBatch(id: $id) {
      id
    }
  }
`
export type RestoreSkuBatchMutationFn = ApolloReactCommon.MutationFunction<
  RestoreSkuBatchMutation,
  RestoreSkuBatchMutationVariables
>
export type RestoreSkuBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<RestoreSkuBatchMutation, RestoreSkuBatchMutationVariables>,
  'mutation'
>

export const RestoreSkuBatchComponent = (props: RestoreSkuBatchComponentProps) => (
  <ApolloReactComponents.Mutation<RestoreSkuBatchMutation, RestoreSkuBatchMutationVariables>
    mutation={RestoreSkuBatchDocument}
    {...props}
  />
)

export type RestoreSkuBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  RestoreSkuBatchMutation,
  RestoreSkuBatchMutationVariables
> &
  TChildProps
export function withRestoreSkuBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    RestoreSkuBatchMutation,
    RestoreSkuBatchMutationVariables,
    RestoreSkuBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    RestoreSkuBatchMutation,
    RestoreSkuBatchMutationVariables,
    RestoreSkuBatchProps<TChildProps>
  >(RestoreSkuBatchDocument, {
    alias: 'restoreSkuBatch',
    ...operationOptions,
  })
}

/**
 * __useRestoreSkuBatchMutation__
 *
 * To run a mutation, you first call `useRestoreSkuBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRestoreSkuBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [restoreSkuBatchMutation, { data, loading, error }] = useRestoreSkuBatchMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRestoreSkuBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<RestoreSkuBatchMutation, RestoreSkuBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<RestoreSkuBatchMutation, RestoreSkuBatchMutationVariables>(
    RestoreSkuBatchDocument,
    baseOptions,
  )
}
export type RestoreSkuBatchMutationHookResult = ReturnType<typeof useRestoreSkuBatchMutation>
export type RestoreSkuBatchMutationResult = ApolloReactCommon.MutationResult<RestoreSkuBatchMutation>
export type RestoreSkuBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  RestoreSkuBatchMutation,
  RestoreSkuBatchMutationVariables
>
export const UpdateSkuBatchDocument = gql`
  mutation UpdateSkuBatch($skuBatchId: ID!, $skuId: ID!, $batchId: ID) {
    updateSkuBatch(skuBatchId: $skuBatchId, skuId: $skuId, batchId: $batchId)
  }
`
export type UpdateSkuBatchMutationFn = ApolloReactCommon.MutationFunction<
  UpdateSkuBatchMutation,
  UpdateSkuBatchMutationVariables
>
export type UpdateSkuBatchComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateSkuBatchMutation, UpdateSkuBatchMutationVariables>,
  'mutation'
>

export const UpdateSkuBatchComponent = (props: UpdateSkuBatchComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateSkuBatchMutation, UpdateSkuBatchMutationVariables>
    mutation={UpdateSkuBatchDocument}
    {...props}
  />
)

export type UpdateSkuBatchProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateSkuBatchMutation,
  UpdateSkuBatchMutationVariables
> &
  TChildProps
export function withUpdateSkuBatch<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateSkuBatchMutation,
    UpdateSkuBatchMutationVariables,
    UpdateSkuBatchProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateSkuBatchMutation,
    UpdateSkuBatchMutationVariables,
    UpdateSkuBatchProps<TChildProps>
  >(UpdateSkuBatchDocument, {
    alias: 'updateSkuBatch',
    ...operationOptions,
  })
}

/**
 * __useUpdateSkuBatchMutation__
 *
 * To run a mutation, you first call `useUpdateSkuBatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSkuBatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateSkuBatchMutation, { data, loading, error }] = useUpdateSkuBatchMutation({
 *   variables: {
 *      skuBatchId: // value for 'skuBatchId'
 *      skuId: // value for 'skuId'
 *      batchId: // value for 'batchId'
 *   },
 * });
 */
export function useUpdateSkuBatchMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateSkuBatchMutation, UpdateSkuBatchMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateSkuBatchMutation, UpdateSkuBatchMutationVariables>(
    UpdateSkuBatchDocument,
    baseOptions,
  )
}
export type UpdateSkuBatchMutationHookResult = ReturnType<typeof useUpdateSkuBatchMutation>
export type UpdateSkuBatchMutationResult = ApolloReactCommon.MutationResult<UpdateSkuBatchMutation>
export type UpdateSkuBatchMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateSkuBatchMutation,
  UpdateSkuBatchMutationVariables
>
export const UpdateSkuBatchTitleholderDocument = gql`
  mutation UpdateSkuBatchTitleholder($skuBatchId: ID!, $titleholderId: ID!) {
    updateSkuBatchTitleholder(skuBatchId: $skuBatchId, titleholderId: $titleholderId) {
      id
    }
  }
`
export type UpdateSkuBatchTitleholderMutationFn = ApolloReactCommon.MutationFunction<
  UpdateSkuBatchTitleholderMutation,
  UpdateSkuBatchTitleholderMutationVariables
>
export type UpdateSkuBatchTitleholderComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<
    UpdateSkuBatchTitleholderMutation,
    UpdateSkuBatchTitleholderMutationVariables
  >,
  'mutation'
>

export const UpdateSkuBatchTitleholderComponent = (props: UpdateSkuBatchTitleholderComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateSkuBatchTitleholderMutation, UpdateSkuBatchTitleholderMutationVariables>
    mutation={UpdateSkuBatchTitleholderDocument}
    {...props}
  />
)

export type UpdateSkuBatchTitleholderProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateSkuBatchTitleholderMutation,
  UpdateSkuBatchTitleholderMutationVariables
> &
  TChildProps
export function withUpdateSkuBatchTitleholder<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateSkuBatchTitleholderMutation,
    UpdateSkuBatchTitleholderMutationVariables,
    UpdateSkuBatchTitleholderProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateSkuBatchTitleholderMutation,
    UpdateSkuBatchTitleholderMutationVariables,
    UpdateSkuBatchTitleholderProps<TChildProps>
  >(UpdateSkuBatchTitleholderDocument, {
    alias: 'updateSkuBatchTitleholder',
    ...operationOptions,
  })
}

/**
 * __useUpdateSkuBatchTitleholderMutation__
 *
 * To run a mutation, you first call `useUpdateSkuBatchTitleholderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSkuBatchTitleholderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateSkuBatchTitleholderMutation, { data, loading, error }] = useUpdateSkuBatchTitleholderMutation({
 *   variables: {
 *      skuBatchId: // value for 'skuBatchId'
 *      titleholderId: // value for 'titleholderId'
 *   },
 * });
 */
export function useUpdateSkuBatchTitleholderMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateSkuBatchTitleholderMutation,
    UpdateSkuBatchTitleholderMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<UpdateSkuBatchTitleholderMutation, UpdateSkuBatchTitleholderMutationVariables>(
    UpdateSkuBatchTitleholderDocument,
    baseOptions,
  )
}
export type UpdateSkuBatchTitleholderMutationHookResult = ReturnType<typeof useUpdateSkuBatchTitleholderMutation>
export type UpdateSkuBatchTitleholderMutationResult = ApolloReactCommon.MutationResult<
  UpdateSkuBatchTitleholderMutation
>
export type UpdateSkuBatchTitleholderMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateSkuBatchTitleholderMutation,
  UpdateSkuBatchTitleholderMutationVariables
>
export const CreateInvitationDocument = gql`
  mutation CreateInvitation($input: CreateOrganizationInviteInput!) {
    createOrganizationInvite(input: $input) {
      changedOrganizationInvite {
        id
        email
        organization {
          id
        }
      }
    }
  }
`
export type CreateInvitationMutationFn = ApolloReactCommon.MutationFunction<
  CreateInvitationMutation,
  CreateInvitationMutationVariables
>
export type CreateInvitationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateInvitationMutation, CreateInvitationMutationVariables>,
  'mutation'
>

export const CreateInvitationComponent = (props: CreateInvitationComponentProps) => (
  <ApolloReactComponents.Mutation<CreateInvitationMutation, CreateInvitationMutationVariables>
    mutation={CreateInvitationDocument}
    {...props}
  />
)

export type CreateInvitationProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateInvitationMutation,
  CreateInvitationMutationVariables
> &
  TChildProps
export function withCreateInvitation<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateInvitationMutation,
    CreateInvitationMutationVariables,
    CreateInvitationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateInvitationMutation,
    CreateInvitationMutationVariables,
    CreateInvitationProps<TChildProps>
  >(CreateInvitationDocument, {
    alias: 'createInvitation',
    ...operationOptions,
  })
}

/**
 * __useCreateInvitationMutation__
 *
 * To run a mutation, you first call `useCreateInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createInvitationMutation, { data, loading, error }] = useCreateInvitationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateInvitationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateInvitationMutation, CreateInvitationMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateInvitationMutation, CreateInvitationMutationVariables>(
    CreateInvitationDocument,
    baseOptions,
  )
}
export type CreateInvitationMutationHookResult = ReturnType<typeof useCreateInvitationMutation>
export type CreateInvitationMutationResult = ApolloReactCommon.MutationResult<CreateInvitationMutation>
export type CreateInvitationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateInvitationMutation,
  CreateInvitationMutationVariables
>
export const UpdateInvitationDocument = gql`
  mutation UpdateInvitation($input: UpdateOrganizationInviteInput!) {
    updateOrganizationInvite(input: $input) {
      changedOrganizationInvite {
        id
        organization {
          id
          name
          alias
        }
        sender {
          id
          email
        }
      }
    }
  }
`
export type UpdateInvitationMutationFn = ApolloReactCommon.MutationFunction<
  UpdateInvitationMutation,
  UpdateInvitationMutationVariables
>
export type UpdateInvitationComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateInvitationMutation, UpdateInvitationMutationVariables>,
  'mutation'
>

export const UpdateInvitationComponent = (props: UpdateInvitationComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateInvitationMutation, UpdateInvitationMutationVariables>
    mutation={UpdateInvitationDocument}
    {...props}
  />
)

export type UpdateInvitationProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateInvitationMutation,
  UpdateInvitationMutationVariables
> &
  TChildProps
export function withUpdateInvitation<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateInvitationMutation,
    UpdateInvitationMutationVariables,
    UpdateInvitationProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateInvitationMutation,
    UpdateInvitationMutationVariables,
    UpdateInvitationProps<TChildProps>
  >(UpdateInvitationDocument, {
    alias: 'updateInvitation',
    ...operationOptions,
  })
}

/**
 * __useUpdateInvitationMutation__
 *
 * To run a mutation, you first call `useUpdateInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateInvitationMutation, { data, loading, error }] = useUpdateInvitationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateInvitationMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateInvitationMutation, UpdateInvitationMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateInvitationMutation, UpdateInvitationMutationVariables>(
    UpdateInvitationDocument,
    baseOptions,
  )
}
export type UpdateInvitationMutationHookResult = ReturnType<typeof useUpdateInvitationMutation>
export type UpdateInvitationMutationResult = ApolloReactCommon.MutationResult<UpdateInvitationMutation>
export type UpdateInvitationMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateInvitationMutation,
  UpdateInvitationMutationVariables
>
export const UpdateUserDocument = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      changedUser {
        ...userFragment
        organizationInvites {
          ...organizationInviteFragment
          organization {
            ...organizationFragment
          }
          sender {
            ...userFragment
          }
        }
      }
    }
  }
  ${UserFragmentFragmentDoc}
  ${OrganizationInviteFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
`
export type UpdateUserMutationFn = ApolloReactCommon.MutationFunction<UpdateUserMutation, UpdateUserMutationVariables>
export type UpdateUserComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateUserMutation, UpdateUserMutationVariables>,
  'mutation'
>

export const UpdateUserComponent = (props: UpdateUserComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateUserMutation, UpdateUserMutationVariables>
    mutation={UpdateUserDocument}
    {...props}
  />
)

export type UpdateUserProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateUserMutation,
  UpdateUserMutationVariables
> &
  TChildProps
export function withUpdateUser<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateUserMutation,
    UpdateUserMutationVariables,
    UpdateUserProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateUserMutation,
    UpdateUserMutationVariables,
    UpdateUserProps<TChildProps>
  >(UpdateUserDocument, {
    alias: 'updateUser',
    ...operationOptions,
  })
}

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUserMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(UpdateUserDocument, baseOptions)
}
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>
export type UpdateUserMutationResult = ApolloReactCommon.MutationResult<UpdateUserMutation>
export type UpdateUserMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateUserMutation,
  UpdateUserMutationVariables
>
export const ValidateTokenDocument = gql`
  mutation ValidateToken($email: String, $token: String) {
    validateToken(email: $email, token: $token) {
      token
    }
  }
`
export type ValidateTokenMutationFn = ApolloReactCommon.MutationFunction<
  ValidateTokenMutation,
  ValidateTokenMutationVariables
>
export type ValidateTokenComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<ValidateTokenMutation, ValidateTokenMutationVariables>,
  'mutation'
>

export const ValidateTokenComponent = (props: ValidateTokenComponentProps) => (
  <ApolloReactComponents.Mutation<ValidateTokenMutation, ValidateTokenMutationVariables>
    mutation={ValidateTokenDocument}
    {...props}
  />
)

export type ValidateTokenProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  ValidateTokenMutation,
  ValidateTokenMutationVariables
> &
  TChildProps
export function withValidateToken<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    ValidateTokenMutation,
    ValidateTokenMutationVariables,
    ValidateTokenProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    ValidateTokenMutation,
    ValidateTokenMutationVariables,
    ValidateTokenProps<TChildProps>
  >(ValidateTokenDocument, {
    alias: 'validateToken',
    ...operationOptions,
  })
}

/**
 * __useValidateTokenMutation__
 *
 * To run a mutation, you first call `useValidateTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useValidateTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [validateTokenMutation, { data, loading, error }] = useValidateTokenMutation({
 *   variables: {
 *      email: // value for 'email'
 *      token: // value for 'token'
 *   },
 * });
 */
export function useValidateTokenMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<ValidateTokenMutation, ValidateTokenMutationVariables>,
) {
  return ApolloReactHooks.useMutation<ValidateTokenMutation, ValidateTokenMutationVariables>(
    ValidateTokenDocument,
    baseOptions,
  )
}
export type ValidateTokenMutationHookResult = ReturnType<typeof useValidateTokenMutation>
export type ValidateTokenMutationResult = ApolloReactCommon.MutationResult<ValidateTokenMutation>
export type ValidateTokenMutationOptions = ApolloReactCommon.BaseMutationOptions<
  ValidateTokenMutation,
  ValidateTokenMutationVariables
>
export const ChangeUserPasswordDocument = gql`
  mutation ChangeUserPassword($password: String!, $newPassword: String!, $confirmPassword: String!) {
    changeUserPassword(password: $password, newPassword: $newPassword, confirmPassword: $confirmPassword) {
      id
    }
  }
`
export type ChangeUserPasswordMutationFn = ApolloReactCommon.MutationFunction<
  ChangeUserPasswordMutation,
  ChangeUserPasswordMutationVariables
>
export type ChangeUserPasswordComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<ChangeUserPasswordMutation, ChangeUserPasswordMutationVariables>,
  'mutation'
>

export const ChangeUserPasswordComponent = (props: ChangeUserPasswordComponentProps) => (
  <ApolloReactComponents.Mutation<ChangeUserPasswordMutation, ChangeUserPasswordMutationVariables>
    mutation={ChangeUserPasswordDocument}
    {...props}
  />
)

export type ChangeUserPasswordProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  ChangeUserPasswordMutation,
  ChangeUserPasswordMutationVariables
> &
  TChildProps
export function withChangeUserPassword<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    ChangeUserPasswordMutation,
    ChangeUserPasswordMutationVariables,
    ChangeUserPasswordProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    ChangeUserPasswordMutation,
    ChangeUserPasswordMutationVariables,
    ChangeUserPasswordProps<TChildProps>
  >(ChangeUserPasswordDocument, {
    alias: 'changeUserPassword',
    ...operationOptions,
  })
}

/**
 * __useChangeUserPasswordMutation__
 *
 * To run a mutation, you first call `useChangeUserPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeUserPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeUserPasswordMutation, { data, loading, error }] = useChangeUserPasswordMutation({
 *   variables: {
 *      password: // value for 'password'
 *      newPassword: // value for 'newPassword'
 *      confirmPassword: // value for 'confirmPassword'
 *   },
 * });
 */
export function useChangeUserPasswordMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<ChangeUserPasswordMutation, ChangeUserPasswordMutationVariables>,
) {
  return ApolloReactHooks.useMutation<ChangeUserPasswordMutation, ChangeUserPasswordMutationVariables>(
    ChangeUserPasswordDocument,
    baseOptions,
  )
}
export type ChangeUserPasswordMutationHookResult = ReturnType<typeof useChangeUserPasswordMutation>
export type ChangeUserPasswordMutationResult = ApolloReactCommon.MutationResult<ChangeUserPasswordMutation>
export type ChangeUserPasswordMutationOptions = ApolloReactCommon.BaseMutationOptions<
  ChangeUserPasswordMutation,
  ChangeUserPasswordMutationVariables
>
export const ResetPasswordDocument = gql`
  mutation ResetPassword($input: ResetPasswordInput!) {
    resetPassword(input: $input) {
      token
    }
  }
`
export type ResetPasswordMutationFn = ApolloReactCommon.MutationFunction<
  ResetPasswordMutation,
  ResetPasswordMutationVariables
>
export type ResetPasswordComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<ResetPasswordMutation, ResetPasswordMutationVariables>,
  'mutation'
>

export const ResetPasswordComponent = (props: ResetPasswordComponentProps) => (
  <ApolloReactComponents.Mutation<ResetPasswordMutation, ResetPasswordMutationVariables>
    mutation={ResetPasswordDocument}
    {...props}
  />
)

export type ResetPasswordProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  ResetPasswordMutation,
  ResetPasswordMutationVariables
> &
  TChildProps
export function withResetPassword<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    ResetPasswordMutation,
    ResetPasswordMutationVariables,
    ResetPasswordProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    ResetPasswordMutation,
    ResetPasswordMutationVariables,
    ResetPasswordProps<TChildProps>
  >(ResetPasswordDocument, {
    alias: 'resetPassword',
    ...operationOptions,
  })
}

/**
 * __useResetPasswordMutation__
 *
 * To run a mutation, you first call `useResetPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useResetPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [resetPasswordMutation, { data, loading, error }] = useResetPasswordMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useResetPasswordMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<ResetPasswordMutation, ResetPasswordMutationVariables>,
) {
  return ApolloReactHooks.useMutation<ResetPasswordMutation, ResetPasswordMutationVariables>(
    ResetPasswordDocument,
    baseOptions,
  )
}
export type ResetPasswordMutationHookResult = ReturnType<typeof useResetPasswordMutation>
export type ResetPasswordMutationResult = ApolloReactCommon.MutationResult<ResetPasswordMutation>
export type ResetPasswordMutationOptions = ApolloReactCommon.BaseMutationOptions<
  ResetPasswordMutation,
  ResetPasswordMutationVariables
>
export const UpdateUserPreferenceDocument = gql`
  mutation UpdateUserPreference($userId: ID!, $organizationId: ID!, $receiveOrderUpdateEmails: Boolean) {
    updateUserPreference(
      userId: $userId
      organizationId: $organizationId
      receiveOrderUpdateEmails: $receiveOrderUpdateEmails
    ) {
      id
      receiveOrderUpdateEmails
    }
  }
`
export type UpdateUserPreferenceMutationFn = ApolloReactCommon.MutationFunction<
  UpdateUserPreferenceMutation,
  UpdateUserPreferenceMutationVariables
>
export type UpdateUserPreferenceComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateUserPreferenceMutation, UpdateUserPreferenceMutationVariables>,
  'mutation'
>

export const UpdateUserPreferenceComponent = (props: UpdateUserPreferenceComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateUserPreferenceMutation, UpdateUserPreferenceMutationVariables>
    mutation={UpdateUserPreferenceDocument}
    {...props}
  />
)

export type UpdateUserPreferenceProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateUserPreferenceMutation,
  UpdateUserPreferenceMutationVariables
> &
  TChildProps
export function withUpdateUserPreference<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateUserPreferenceMutation,
    UpdateUserPreferenceMutationVariables,
    UpdateUserPreferenceProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateUserPreferenceMutation,
    UpdateUserPreferenceMutationVariables,
    UpdateUserPreferenceProps<TChildProps>
  >(UpdateUserPreferenceDocument, {
    alias: 'updateUserPreference',
    ...operationOptions,
  })
}

/**
 * __useUpdateUserPreferenceMutation__
 *
 * To run a mutation, you first call `useUpdateUserPreferenceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserPreferenceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserPreferenceMutation, { data, loading, error }] = useUpdateUserPreferenceMutation({
 *   variables: {
 *      userId: // value for 'userId'
 *      organizationId: // value for 'organizationId'
 *      receiveOrderUpdateEmails: // value for 'receiveOrderUpdateEmails'
 *   },
 * });
 */
export function useUpdateUserPreferenceMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    UpdateUserPreferenceMutation,
    UpdateUserPreferenceMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<UpdateUserPreferenceMutation, UpdateUserPreferenceMutationVariables>(
    UpdateUserPreferenceDocument,
    baseOptions,
  )
}
export type UpdateUserPreferenceMutationHookResult = ReturnType<typeof useUpdateUserPreferenceMutation>
export type UpdateUserPreferenceMutationResult = ApolloReactCommon.MutationResult<UpdateUserPreferenceMutation>
export type UpdateUserPreferenceMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateUserPreferenceMutation,
  UpdateUserPreferenceMutationVariables
>
export const MeDocument = gql`
  query Me {
    viewer {
      me {
        ...userFragment
        organizations {
          ...organizationFragment
          owner {
            ...userFragment
          }
        }
        organizationInvites {
          ...organizationInviteFragment
          organization {
            ...organizationFragment
          }
          sender {
            ...userFragment
          }
        }
        assigned {
          ...orderFragment
          creator {
            ...userFragment
          }
          collectWithOrder {
            id
          }
        }
        userPreferences {
          ...userPreferenceFragment
        }
        userRoles {
          ...userRoleFragment
          role {
            ...roleFragment
          }
        }
        roles {
          ...roleFragment
          organization {
            ...organizationFragment
          }
        }
      }
    }
  }
  ${UserFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
  ${OrganizationInviteFragmentFragmentDoc}
  ${OrderFragmentFragmentDoc}
  ${UserPreferenceFragmentFragmentDoc}
  ${UserRoleFragmentFragmentDoc}
  ${RoleFragmentFragmentDoc}
`
export type MeComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<MeQuery, MeQueryVariables>, 'query'>

export const MeComponent = (props: MeComponentProps) => (
  <ApolloReactComponents.Query<MeQuery, MeQueryVariables> query={MeDocument} {...props} />
)

export type MeProps<TChildProps = {}> = ApolloReactHoc.DataProps<MeQuery, MeQueryVariables> & TChildProps
export function withMe<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<TProps, MeQuery, MeQueryVariables, MeProps<TChildProps>>,
) {
  return ApolloReactHoc.withQuery<TProps, MeQuery, MeQueryVariables, MeProps<TChildProps>>(MeDocument, {
    alias: 'me',
    ...operationOptions,
  })
}

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<MeQuery, MeQueryVariables>) {
  return ApolloReactHooks.useQuery<MeQuery, MeQueryVariables>(MeDocument, baseOptions)
}
export function useMeLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
  return ApolloReactHooks.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, baseOptions)
}
export type MeQueryHookResult = ReturnType<typeof useMeQuery>
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>
export type MeQueryResult = ApolloReactCommon.QueryResult<MeQuery, MeQueryVariables>
export const CreateUserRoleDocument = gql`
  mutation CreateUserRole($input: CreateUserRoleInput!) {
    createUserRole(input: $input) {
      id
      email
      phone
      firstName
      lastName
      roles {
        id
      }
    }
  }
`
export type CreateUserRoleMutationFn = ApolloReactCommon.MutationFunction<
  CreateUserRoleMutation,
  CreateUserRoleMutationVariables
>
export type CreateUserRoleComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateUserRoleMutation, CreateUserRoleMutationVariables>,
  'mutation'
>

export const CreateUserRoleComponent = (props: CreateUserRoleComponentProps) => (
  <ApolloReactComponents.Mutation<CreateUserRoleMutation, CreateUserRoleMutationVariables>
    mutation={CreateUserRoleDocument}
    {...props}
  />
)

export type CreateUserRoleProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateUserRoleMutation,
  CreateUserRoleMutationVariables
> &
  TChildProps
export function withCreateUserRole<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateUserRoleMutation,
    CreateUserRoleMutationVariables,
    CreateUserRoleProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateUserRoleMutation,
    CreateUserRoleMutationVariables,
    CreateUserRoleProps<TChildProps>
  >(CreateUserRoleDocument, {
    alias: 'createUserRole',
    ...operationOptions,
  })
}

/**
 * __useCreateUserRoleMutation__
 *
 * To run a mutation, you first call `useCreateUserRoleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateUserRoleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createUserRoleMutation, { data, loading, error }] = useCreateUserRoleMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateUserRoleMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateUserRoleMutation, CreateUserRoleMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateUserRoleMutation, CreateUserRoleMutationVariables>(
    CreateUserRoleDocument,
    baseOptions,
  )
}
export type CreateUserRoleMutationHookResult = ReturnType<typeof useCreateUserRoleMutation>
export type CreateUserRoleMutationResult = ApolloReactCommon.MutationResult<CreateUserRoleMutation>
export type CreateUserRoleMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateUserRoleMutation,
  CreateUserRoleMutationVariables
>
export const AllOutstandingInvoicesDocument = gql`
  query AllOutstandingInvoices(
    $organizationId: ID
    $orderNumber: String
    $startDate: String
    $endDate: String
    $startDueDate: String
    $endDueDate: String
    $pageInfo: PageInfoInput
    $all: Boolean
    $warehouse: String
    $dispensary: String
    $orgName: String
  ) {
    viewer {
      allOutstandingInvoices(
        organizationId: $organizationId
        orderNumber: $orderNumber
        startDate: $startDate
        endDate: $endDate
        startDueDate: $startDueDate
        endDueDate: $endDueDate
        pageInfo: $pageInfo
        all: $all
        warehouse: $warehouse
        dispensary: $dispensary
        orgName: $orgName
      ) {
        results {
          id
          gmv
          date
          name
          number
          discount
          exciseTax
          gmvCollected
          exciseTaxCollected
          daysTillPaymentDue
          creator {
            id
            email
            phone
            lastName
            firstName
          }
          organization {
            id
            name
          }
          site {
            id
            name
          }
          collectWithOrder {
            id
            number
            date
          }
          warehouse {
            id
            site {
              id
              licensedLocation {
                id
                name
              }
            }
          }
        }
        pageInfo {
          page
          numItemsPerPage
          orderBy {
            attribute
            order
          }
          totalNumItems
          totalNumPages
        }
      }
    }
  }
`
export type AllOutstandingInvoicesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllOutstandingInvoicesQuery, AllOutstandingInvoicesQueryVariables>,
  'query'
>

export const AllOutstandingInvoicesComponent = (props: AllOutstandingInvoicesComponentProps) => (
  <ApolloReactComponents.Query<AllOutstandingInvoicesQuery, AllOutstandingInvoicesQueryVariables>
    query={AllOutstandingInvoicesDocument}
    {...props}
  />
)

export type AllOutstandingInvoicesProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllOutstandingInvoicesQuery,
  AllOutstandingInvoicesQueryVariables
> &
  TChildProps
export function withAllOutstandingInvoices<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllOutstandingInvoicesQuery,
    AllOutstandingInvoicesQueryVariables,
    AllOutstandingInvoicesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllOutstandingInvoicesQuery,
    AllOutstandingInvoicesQueryVariables,
    AllOutstandingInvoicesProps<TChildProps>
  >(AllOutstandingInvoicesDocument, {
    alias: 'allOutstandingInvoices',
    ...operationOptions,
  })
}

/**
 * __useAllOutstandingInvoicesQuery__
 *
 * To run a query within a React component, call `useAllOutstandingInvoicesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllOutstandingInvoicesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllOutstandingInvoicesQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      orderNumber: // value for 'orderNumber'
 *      startDate: // value for 'startDate'
 *      endDate: // value for 'endDate'
 *      startDueDate: // value for 'startDueDate'
 *      endDueDate: // value for 'endDueDate'
 *      pageInfo: // value for 'pageInfo'
 *      all: // value for 'all'
 *      warehouse: // value for 'warehouse'
 *      dispensary: // value for 'dispensary'
 *      orgName: // value for 'orgName'
 *   },
 * });
 */
export function useAllOutstandingInvoicesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllOutstandingInvoicesQuery, AllOutstandingInvoicesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllOutstandingInvoicesQuery, AllOutstandingInvoicesQueryVariables>(
    AllOutstandingInvoicesDocument,
    baseOptions,
  )
}
export function useAllOutstandingInvoicesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    AllOutstandingInvoicesQuery,
    AllOutstandingInvoicesQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<AllOutstandingInvoicesQuery, AllOutstandingInvoicesQueryVariables>(
    AllOutstandingInvoicesDocument,
    baseOptions,
  )
}
export type AllOutstandingInvoicesQueryHookResult = ReturnType<typeof useAllOutstandingInvoicesQuery>
export type AllOutstandingInvoicesLazyQueryHookResult = ReturnType<typeof useAllOutstandingInvoicesLazyQuery>
export type AllOutstandingInvoicesQueryResult = ApolloReactCommon.QueryResult<
  AllOutstandingInvoicesQuery,
  AllOutstandingInvoicesQueryVariables
>
export const InvoiceDataDocument = gql`
  query InvoiceData($id: ID!, $number: String) {
    viewer {
      allFutureDropoffsToDispensary(id: $id) {
        id
        date
        number
        manifestGDriveFileId
        collectWithOrder {
          id
        }
      }
      getAllSiteOutstandingInvoices(id: $id) {
        id
        date
        number
        manifestGDriveFileId
        collectWithOrder {
          id
        }
      }
      getSite(id: $id) {
        name
        licensedLocation {
          name
        }
      }
      getOrder(number: $number) {
        id
        date
        number
      }
    }
    getOrder(id: $id) {
      id
      date
      number
      site {
        licensedLocation {
          name
        }
      }
      collectWithOrder {
        id
      }
    }
  }
`
export type InvoiceDataComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<InvoiceDataQuery, InvoiceDataQueryVariables>,
  'query'
> &
  ({ variables: InvoiceDataQueryVariables; skip?: boolean } | { skip: boolean })

export const InvoiceDataComponent = (props: InvoiceDataComponentProps) => (
  <ApolloReactComponents.Query<InvoiceDataQuery, InvoiceDataQueryVariables> query={InvoiceDataDocument} {...props} />
)

export type InvoiceDataProps<TChildProps = {}> = ApolloReactHoc.DataProps<InvoiceDataQuery, InvoiceDataQueryVariables> &
  TChildProps
export function withInvoiceData<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    InvoiceDataQuery,
    InvoiceDataQueryVariables,
    InvoiceDataProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, InvoiceDataQuery, InvoiceDataQueryVariables, InvoiceDataProps<TChildProps>>(
    InvoiceDataDocument,
    {
      alias: 'invoiceData',
      ...operationOptions,
    },
  )
}

/**
 * __useInvoiceDataQuery__
 *
 * To run a query within a React component, call `useInvoiceDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useInvoiceDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInvoiceDataQuery({
 *   variables: {
 *      id: // value for 'id'
 *      number: // value for 'number'
 *   },
 * });
 */
export function useInvoiceDataQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<InvoiceDataQuery, InvoiceDataQueryVariables>,
) {
  return ApolloReactHooks.useQuery<InvoiceDataQuery, InvoiceDataQueryVariables>(InvoiceDataDocument, baseOptions)
}
export function useInvoiceDataLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<InvoiceDataQuery, InvoiceDataQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<InvoiceDataQuery, InvoiceDataQueryVariables>(InvoiceDataDocument, baseOptions)
}
export type InvoiceDataQueryHookResult = ReturnType<typeof useInvoiceDataQuery>
export type InvoiceDataLazyQueryHookResult = ReturnType<typeof useInvoiceDataLazyQuery>
export type InvoiceDataQueryResult = ApolloReactCommon.QueryResult<InvoiceDataQuery, InvoiceDataQueryVariables>
export const GetTitleholdersDocument = gql`
  query GetTitleholders($organizationId: ID!) {
    viewer {
      getTitleholders(organizationId: $organizationId) {
        id
        type
        titleholderLicense {
          ...licenseFragment
        }
        titleholderOrganization {
          ...organizationFragment
          licenses {
            ...licenseFragment
          }
        }
      }
    }
  }
  ${LicenseFragmentFragmentDoc}
  ${OrganizationFragmentFragmentDoc}
`
export type GetTitleholdersComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<GetTitleholdersQuery, GetTitleholdersQueryVariables>,
  'query'
> &
  ({ variables: GetTitleholdersQueryVariables; skip?: boolean } | { skip: boolean })

export const GetTitleholdersComponent = (props: GetTitleholdersComponentProps) => (
  <ApolloReactComponents.Query<GetTitleholdersQuery, GetTitleholdersQueryVariables>
    query={GetTitleholdersDocument}
    {...props}
  />
)

export type GetTitleholdersProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  GetTitleholdersQuery,
  GetTitleholdersQueryVariables
> &
  TChildProps
export function withGetTitleholders<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    GetTitleholdersQuery,
    GetTitleholdersQueryVariables,
    GetTitleholdersProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    GetTitleholdersQuery,
    GetTitleholdersQueryVariables,
    GetTitleholdersProps<TChildProps>
  >(GetTitleholdersDocument, {
    alias: 'getTitleholders',
    ...operationOptions,
  })
}

/**
 * __useGetTitleholdersQuery__
 *
 * To run a query within a React component, call `useGetTitleholdersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTitleholdersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTitleholdersQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useGetTitleholdersQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<GetTitleholdersQuery, GetTitleholdersQueryVariables>,
) {
  return ApolloReactHooks.useQuery<GetTitleholdersQuery, GetTitleholdersQueryVariables>(
    GetTitleholdersDocument,
    baseOptions,
  )
}
export function useGetTitleholdersLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetTitleholdersQuery, GetTitleholdersQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<GetTitleholdersQuery, GetTitleholdersQueryVariables>(
    GetTitleholdersDocument,
    baseOptions,
  )
}
export type GetTitleholdersQueryHookResult = ReturnType<typeof useGetTitleholdersQuery>
export type GetTitleholdersLazyQueryHookResult = ReturnType<typeof useGetTitleholdersLazyQuery>
export type GetTitleholdersQueryResult = ApolloReactCommon.QueryResult<
  GetTitleholdersQuery,
  GetTitleholdersQueryVariables
>
export const AllRolesDocument = gql`
  query AllRoles {
    viewer {
      allRoles {
        ...roleFragment
      }
    }
  }
  ${RoleFragmentFragmentDoc}
`
export type AllRolesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllRolesQuery, AllRolesQueryVariables>,
  'query'
>

export const AllRolesComponent = (props: AllRolesComponentProps) => (
  <ApolloReactComponents.Query<AllRolesQuery, AllRolesQueryVariables> query={AllRolesDocument} {...props} />
)

export type AllRolesProps<TChildProps = {}> = ApolloReactHoc.DataProps<AllRolesQuery, AllRolesQueryVariables> &
  TChildProps
export function withAllRoles<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllRolesQuery,
    AllRolesQueryVariables,
    AllRolesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<TProps, AllRolesQuery, AllRolesQueryVariables, AllRolesProps<TChildProps>>(
    AllRolesDocument,
    {
      alias: 'allRoles',
      ...operationOptions,
    },
  )
}

/**
 * __useAllRolesQuery__
 *
 * To run a query within a React component, call `useAllRolesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllRolesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllRolesQuery({
 *   variables: {
 *   },
 * });
 */
export function useAllRolesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllRolesQuery, AllRolesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllRolesQuery, AllRolesQueryVariables>(AllRolesDocument, baseOptions)
}
export function useAllRolesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllRolesQuery, AllRolesQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllRolesQuery, AllRolesQueryVariables>(AllRolesDocument, baseOptions)
}
export type AllRolesQueryHookResult = ReturnType<typeof useAllRolesQuery>
export type AllRolesLazyQueryHookResult = ReturnType<typeof useAllRolesLazyQuery>
export type AllRolesQueryResult = ApolloReactCommon.QueryResult<AllRolesQuery, AllRolesQueryVariables>
export const CreateVehicleDocument = gql`
  mutation CreateVehicle($input: CreateVehicleInput!) {
    createVehicle(input: $input) {
      changedVehicle {
        ...vehicleFragment
      }
    }
  }
  ${VehicleFragmentFragmentDoc}
`
export type CreateVehicleMutationFn = ApolloReactCommon.MutationFunction<
  CreateVehicleMutation,
  CreateVehicleMutationVariables
>
export type CreateVehicleComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<CreateVehicleMutation, CreateVehicleMutationVariables>,
  'mutation'
>

export const CreateVehicleComponent = (props: CreateVehicleComponentProps) => (
  <ApolloReactComponents.Mutation<CreateVehicleMutation, CreateVehicleMutationVariables>
    mutation={CreateVehicleDocument}
    {...props}
  />
)

export type CreateVehicleProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  CreateVehicleMutation,
  CreateVehicleMutationVariables
> &
  TChildProps
export function withCreateVehicle<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    CreateVehicleMutation,
    CreateVehicleMutationVariables,
    CreateVehicleProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    CreateVehicleMutation,
    CreateVehicleMutationVariables,
    CreateVehicleProps<TChildProps>
  >(CreateVehicleDocument, {
    alias: 'createVehicle',
    ...operationOptions,
  })
}

/**
 * __useCreateVehicleMutation__
 *
 * To run a mutation, you first call `useCreateVehicleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateVehicleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createVehicleMutation, { data, loading, error }] = useCreateVehicleMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateVehicleMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<CreateVehicleMutation, CreateVehicleMutationVariables>,
) {
  return ApolloReactHooks.useMutation<CreateVehicleMutation, CreateVehicleMutationVariables>(
    CreateVehicleDocument,
    baseOptions,
  )
}
export type CreateVehicleMutationHookResult = ReturnType<typeof useCreateVehicleMutation>
export type CreateVehicleMutationResult = ApolloReactCommon.MutationResult<CreateVehicleMutation>
export type CreateVehicleMutationOptions = ApolloReactCommon.BaseMutationOptions<
  CreateVehicleMutation,
  CreateVehicleMutationVariables
>
export const UpdateVehicleDocument = gql`
  mutation UpdateVehicle($input: UpdateVehicleInput!) {
    updateVehicle(input: $input) {
      changedVehicle {
        ...vehicleFragment
      }
    }
  }
  ${VehicleFragmentFragmentDoc}
`
export type UpdateVehicleMutationFn = ApolloReactCommon.MutationFunction<
  UpdateVehicleMutation,
  UpdateVehicleMutationVariables
>
export type UpdateVehicleComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<UpdateVehicleMutation, UpdateVehicleMutationVariables>,
  'mutation'
>

export const UpdateVehicleComponent = (props: UpdateVehicleComponentProps) => (
  <ApolloReactComponents.Mutation<UpdateVehicleMutation, UpdateVehicleMutationVariables>
    mutation={UpdateVehicleDocument}
    {...props}
  />
)

export type UpdateVehicleProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  UpdateVehicleMutation,
  UpdateVehicleMutationVariables
> &
  TChildProps
export function withUpdateVehicle<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    UpdateVehicleMutation,
    UpdateVehicleMutationVariables,
    UpdateVehicleProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    UpdateVehicleMutation,
    UpdateVehicleMutationVariables,
    UpdateVehicleProps<TChildProps>
  >(UpdateVehicleDocument, {
    alias: 'updateVehicle',
    ...operationOptions,
  })
}

/**
 * __useUpdateVehicleMutation__
 *
 * To run a mutation, you first call `useUpdateVehicleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateVehicleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateVehicleMutation, { data, loading, error }] = useUpdateVehicleMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateVehicleMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateVehicleMutation, UpdateVehicleMutationVariables>,
) {
  return ApolloReactHooks.useMutation<UpdateVehicleMutation, UpdateVehicleMutationVariables>(
    UpdateVehicleDocument,
    baseOptions,
  )
}
export type UpdateVehicleMutationHookResult = ReturnType<typeof useUpdateVehicleMutation>
export type UpdateVehicleMutationResult = ApolloReactCommon.MutationResult<UpdateVehicleMutation>
export type UpdateVehicleMutationOptions = ApolloReactCommon.BaseMutationOptions<
  UpdateVehicleMutation,
  UpdateVehicleMutationVariables
>
export const DeleteVehicleDocument = gql`
  mutation DeleteVehicle($input: DeleteVehicleInput!) {
    deleteVehicle(input: $input) {
      changedVehicle {
        ...vehicleFragment
      }
    }
  }
  ${VehicleFragmentFragmentDoc}
`
export type DeleteVehicleMutationFn = ApolloReactCommon.MutationFunction<
  DeleteVehicleMutation,
  DeleteVehicleMutationVariables
>
export type DeleteVehicleComponentProps = Omit<
  ApolloReactComponents.MutationComponentOptions<DeleteVehicleMutation, DeleteVehicleMutationVariables>,
  'mutation'
>

export const DeleteVehicleComponent = (props: DeleteVehicleComponentProps) => (
  <ApolloReactComponents.Mutation<DeleteVehicleMutation, DeleteVehicleMutationVariables>
    mutation={DeleteVehicleDocument}
    {...props}
  />
)

export type DeleteVehicleProps<TChildProps = {}> = ApolloReactHoc.MutateProps<
  DeleteVehicleMutation,
  DeleteVehicleMutationVariables
> &
  TChildProps
export function withDeleteVehicle<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    DeleteVehicleMutation,
    DeleteVehicleMutationVariables,
    DeleteVehicleProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withMutation<
    TProps,
    DeleteVehicleMutation,
    DeleteVehicleMutationVariables,
    DeleteVehicleProps<TChildProps>
  >(DeleteVehicleDocument, {
    alias: 'deleteVehicle',
    ...operationOptions,
  })
}

/**
 * __useDeleteVehicleMutation__
 *
 * To run a mutation, you first call `useDeleteVehicleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteVehicleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteVehicleMutation, { data, loading, error }] = useDeleteVehicleMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteVehicleMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteVehicleMutation, DeleteVehicleMutationVariables>,
) {
  return ApolloReactHooks.useMutation<DeleteVehicleMutation, DeleteVehicleMutationVariables>(
    DeleteVehicleDocument,
    baseOptions,
  )
}
export type DeleteVehicleMutationHookResult = ReturnType<typeof useDeleteVehicleMutation>
export type DeleteVehicleMutationResult = ApolloReactCommon.MutationResult<DeleteVehicleMutation>
export type DeleteVehicleMutationOptions = ApolloReactCommon.BaseMutationOptions<
  DeleteVehicleMutation,
  DeleteVehicleMutationVariables
>
export const AllWarehousesDocument = gql`
  query AllWarehouses {
    viewer {
      allWarehouses {
        ...warehouseFragment
        site {
          ...siteFragment
          licenses {
            ...licenseFragment
          }
        }
      }
    }
  }
  ${WarehouseFragmentFragmentDoc}
  ${SiteFragmentFragmentDoc}
  ${LicenseFragmentFragmentDoc}
`
export type AllWarehousesComponentProps = Omit<
  ApolloReactComponents.QueryComponentOptions<AllWarehousesQuery, AllWarehousesQueryVariables>,
  'query'
>

export const AllWarehousesComponent = (props: AllWarehousesComponentProps) => (
  <ApolloReactComponents.Query<AllWarehousesQuery, AllWarehousesQueryVariables>
    query={AllWarehousesDocument}
    {...props}
  />
)

export type AllWarehousesProps<TChildProps = {}> = ApolloReactHoc.DataProps<
  AllWarehousesQuery,
  AllWarehousesQueryVariables
> &
  TChildProps
export function withAllWarehouses<TProps, TChildProps = {}>(
  operationOptions?: ApolloReactHoc.OperationOption<
    TProps,
    AllWarehousesQuery,
    AllWarehousesQueryVariables,
    AllWarehousesProps<TChildProps>
  >,
) {
  return ApolloReactHoc.withQuery<
    TProps,
    AllWarehousesQuery,
    AllWarehousesQueryVariables,
    AllWarehousesProps<TChildProps>
  >(AllWarehousesDocument, {
    alias: 'allWarehouses',
    ...operationOptions,
  })
}

/**
 * __useAllWarehousesQuery__
 *
 * To run a query within a React component, call `useAllWarehousesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllWarehousesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllWarehousesQuery({
 *   variables: {
 *   },
 * });
 */
export function useAllWarehousesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<AllWarehousesQuery, AllWarehousesQueryVariables>,
) {
  return ApolloReactHooks.useQuery<AllWarehousesQuery, AllWarehousesQueryVariables>(AllWarehousesDocument, baseOptions)
}
export function useAllWarehousesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AllWarehousesQuery, AllWarehousesQueryVariables>,
) {
  return ApolloReactHooks.useLazyQuery<AllWarehousesQuery, AllWarehousesQueryVariables>(
    AllWarehousesDocument,
    baseOptions,
  )
}
export type AllWarehousesQueryHookResult = ReturnType<typeof useAllWarehousesQuery>
export type AllWarehousesLazyQueryHookResult = ReturnType<typeof useAllWarehousesLazyQuery>
export type AllWarehousesQueryResult = ApolloReactCommon.QueryResult<AllWarehousesQuery, AllWarehousesQueryVariables>

export interface IntrospectionResultData {
  __schema: {
    types: {
      kind: string
      name: string
      possibleTypes: {
        name: string
      }[]
    }[]
  }
}
const result: IntrospectionResultData = {
  __schema: {
    types: [],
  },
}
export default result
