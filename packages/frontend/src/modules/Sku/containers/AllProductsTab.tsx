import React, { useEffect, useState } from 'react'

import { Pagination, S } from 'components'
import { Scalars, Sku, useAllSkusQuery } from 'generated/graphql'
import { usePageInfo } from 'hooks'
import { useOrganization } from 'modules/Organization'

import { FeedbackType } from 'modules/Inventory'
import { useMe } from 'modules/User'
import { Header, Segment, Table } from 'semantic-ui-react'
import { ProductRow } from '../components/ProductRow'
import { ProductsHeaders } from '../components/ProductsHeaders'

interface Props {
  adminOverrideOrganizationId?: Scalars['ID']
  onFeedback: (f: FeedbackType) => void
}

export function AllProductsTab({ onFeedback, adminOverrideOrganizationId }: Props) {
  const [isArchived, setIsArchived] = useState<boolean>(false)
  const [search, setSearch] = useState<string>('')
  const { isAdmin } = useMe()
  const { pageInfo, setPage } = usePageInfo()
  const { organization } = useOrganization()
  const organizationId = isAdmin ? adminOverrideOrganizationId : organization.id

  const { data, loading, error, ...allSkusQuery } = useAllSkusQuery({
    variables: {
      organizationId,
      pageInfo,
      isArchived,
      isInventoryTab: true,
      search: search.trim(),
    },
  })

  useEffect(() => {
    allSkusQuery.refetch()
  }, [isArchived, allSkusQuery, pageInfo])

  const products = data?.viewer?.allSkus?.results ?? []
  const totalNumPages = data?.viewer?.allSkus?.pageInfo?.totalNumPages ?? 1

  const COL_SPAN_LENGTH = '9'

  return (
    <>
      <Pagination
        onPageChange={(_e, d) => setPage(d.activePage)}
        activePage={pageInfo.page}
        totalNumPages={totalNumPages}
        siblingRange={3}
      />
      <Table attached celled selectable structured data-test="all-products-tab-table">
        <ProductsHeaders
          adminOverrideOrganizationId={adminOverrideOrganizationId}
          search={search}
          onChangeSearch={setSearch}
          isArchived={isArchived}
          onChangeIsArchived={setIsArchived}
        />
        <Table.Body>
          {loading ? (
            <Table.Row>
              <Table.Cell colSpan={COL_SPAN_LENGTH}>
                <Segment placeholder className="flex-style">
                  <Header>
                    <S.Loader active inline="centered" size="large">
                      Loading...
                    </S.Loader>
                  </Header>
                </Segment>
              </Table.Cell>
            </Table.Row>
          ) : error ? (
            <Table.Row>
              <Table.Cell colSpan={COL_SPAN_LENGTH}>
                <Segment placeholder className="flex-style">
                  <Header>
                    <S.Message negative header="Oops... something went wrong" content={error.message} />
                  </Header>
                </Segment>
              </Table.Cell>
            </Table.Row>
          ) : products && products.length > 0 ? (
            products.map((product) => (
              <ProductRow
                adminOverrideOrganizationId={adminOverrideOrganizationId}
                key={product.id}
                sku={product as Sku}
                onFeedback={onFeedback}
              />
            ))
          ) : (
            <Table.Cell colSpan={COL_SPAN_LENGTH}>
              <Segment placeholder className="flex-style">
                <Header>No data available</Header>
              </Segment>
            </Table.Cell>
          )}
        </Table.Body>
      </Table>
      {products && products.length > 0 && (
        <Pagination
          onPageChange={(_e, d) => setPage(d.activePage)}
          activePage={pageInfo.page}
          totalNumPages={totalNumPages}
          siblingRange={3}
        />
      )}
    </>
  )
}
