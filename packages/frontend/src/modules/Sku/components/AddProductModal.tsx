import { S } from 'components'
import React from 'react'
import { AddProduct } from './AddProduct'

interface Props {
  onOpen: () => void
  onClose: () => void
  onSuccess?: () => void
  isOpen: boolean
}

export function AddProductModal({ onOpen, onClose, isOpen, onSuccess }: Props) {
  return (
    <S.Modal
      open={isOpen}
      closeIcon
      closeOnDimmerClick={false}
      onOpen={onOpen}
      onClose={onClose}
      header="Add Product"
      content={<AddProduct onSuccess={onSuccess} />}
    />
  )
}
