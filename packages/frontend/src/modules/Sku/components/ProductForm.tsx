import React from 'react'

import { Box, Button, ErrorBox, Loading, S } from 'components'
import {
  CreateSkuInput,
  InventoryCategoryEnum,
  InventoryClassEnum,
  InventoryTypeEnum,
  Scalars,
  useCreateSkuMutation,
  useUpdateSkuMutation,
} from 'generated/graphql'
import {
  createFormCheckbox,
  createFormDropdown,
  createFormField,
  createValidationSchema,
  useForm,
  validators,
} from 'modules/Forms'
import {
  INVENTORY_CATEGORY_ARRAY,
  INVENTORY_CATEGORY_SELECT_OPTIONS,
  INVENTORY_CLASS_ARRAY,
  INVENTORY_CLASS_SELECT_OPTIONS,
  INVENTORY_TYPE_SELECT_OPTIONS,
  INVENTORY_TYPES_ARRAY,
} from 'modules/Inventory'
import { useAdminOverrideOrganization } from 'modules/Inventory/provider/useAdminOverrideOrganization'
import { titleize } from 'utils/formatters'

const EMPTY_FORM_VALS: Required<Omit<InitialValues, 'pricePerUnit'>> & { pricePerUnit: null } = {
  name: '',
  code: '',
  isExciseTaxable: true,
  inventoryType: 'CANNABIS',
  inventoryClass: 'FLOWER',
  inventoryCategory: 'BUD',
  pricePerUnit: null,
  unit: '',
}

const INLINE_REQUIRED_MSG = ' * Required'

const validationSchema = createValidationSchema<Omit<CreateSkuInput, 'organizationId'>>({
  name: validators.string.required(INLINE_REQUIRED_MSG),
  code: validators.string.required(INLINE_REQUIRED_MSG),
  isExciseTaxable: validators.boolean.required(INLINE_REQUIRED_MSG),
  inventoryType: validators.oneOf(INVENTORY_TYPES_ARRAY).required(INLINE_REQUIRED_MSG),
  inventoryClass: validators.oneOf(INVENTORY_CLASS_ARRAY).required(INLINE_REQUIRED_MSG),
  inventoryCategory: validators.oneOf(INVENTORY_CATEGORY_ARRAY).required(INLINE_REQUIRED_MSG),
  pricePerUnit: validators.number.required(INLINE_REQUIRED_MSG),
  unit: validators.string.required(INLINE_REQUIRED_MSG),
})

interface InitialValues {
  name?: string
  code?: string
  isExciseTaxable?: boolean
  inventoryType?: InventoryTypeEnum
  inventoryClass?: InventoryClassEnum
  inventoryCategory?: InventoryCategoryEnum
  pricePerUnit?: number
  unit?: string
}

interface Props {
  initialValues?: InitialValues
  isAddForm: boolean
  setParentProductId?: (id: Scalars['ID']) => void
  productId?: string
  onSuccess?: () => void
}

export function ProductForm({ initialValues, isAddForm, productId, setParentProductId, onSuccess }: Props) {
  const [updateMutation, { loading: updateLoading, error: updateError }] = useUpdateSkuMutation()
  const [createMutation, { loading: createLoading, error: createError }] = useCreateSkuMutation()

  const [loading, error] = [isAddForm ? createLoading : updateLoading, isAddForm ? createError : updateError]

  const { adminOverrideOrganizationId, organization } = useAdminOverrideOrganization()

  const organizationId =
    adminOverrideOrganizationId !== 'All Organizations' ? adminOverrideOrganizationId : organization.id

  const { errors, handleBlur, handleChange, handleSubmit, values, isSubmitting, ...form } = useForm<
    typeof validationSchema
  >({
    validationSchema,
    initialValues: { ...EMPTY_FORM_VALS, ...initialValues },
    // TODO: form needs to be typed better
    onSubmit: async (inputs: Required<InitialValues>) => {
      const refetchQueries = ['InventorySkuBatchesTab', 'AllSkus']
      if (isAddForm) {
        const payload = {
          ...inputs,
          organizationId,
        }
        const createData = await createMutation({ variables: { input: payload }, refetchQueries })

        if (createData.errors) {
          alert(`Error:\n${createData.errors.map((err) => `${err.message}\n`)}`)
          return
        }

        if (setParentProductId) {
          const newId = createData?.data?.createSku?.changedSku?.id
          if (newId) {
            setParentProductId(newId)
          }
        }
        if (onSuccess) {
          onSuccess()
        }
      } else {
        if (!productId) {
          throw new Error('Failed to submit update, no skuId was provided')
        }
        const payload = {
          ...inputs,
          id: productId,
        }
        await updateMutation({ variables: { input: payload }, refetchQueries })

        if (setParentProductId) {
          setParentProductId(productId)
        }
        if (onSuccess) {
          onSuccess()
        }
      }
    },
  })

  const fields: JSX.Element[][] = [
    [
      createFormField({
        name: 'name',
        type: 'text',
        label: 'Name',
        placeholder: 'Exciting Thing',
        value: values.name,
        values,
        error: Boolean(errors.name),
        onChange: handleChange,
        onBlur: handleBlur,
        required: true,
      }),
      createFormField({
        name: 'code',
        type: 'text',
        label: 'SKU #',
        placeholder: 'THING-1',
        value: values.code,
        values,
        error: Boolean(errors.code),
        onChange: handleChange,
        onBlur: handleBlur,
        required: true,
      }),
      createFormField({
        name: 'pricePerUnit',
        type: 'number',
        label: 'Price Per Unit',
        value: values.pricePerUnit,
        values,
        error: Boolean(errors.pricePerUnit),
        onChange: handleChange,
        onBlur: handleBlur,
        required: true,
      }),
      createFormField({
        name: 'unit',
        type: 'text',
        label: 'Unit',
        value: values.unit,
        values,
        error: Boolean(errors.unit),
        onChange: handleChange,
        onBlur: handleBlur,
        required: true,
      }),
    ],
    [
      createFormDropdown({
        name: 'inventoryType',
        type: 'text',
        label: 'Product Type',
        placeholder: 'Is this a cannabis product?',
        value: values.inventoryType,
        values,
        text: titleize(String(values.inventoryType)),
        error: Boolean(errors.inventoryType),
        onChange: (_e, { value }) => {
          form.setFieldValue('inventoryType', value)
          if (values.isExciseTaxable && value === 'NON_CANNABIS') {
            form.setFieldValue('isExciseTaxable', false)
          } else {
            form.setFieldValue('isExciseTaxable', true)
          }
        },
        onBlur: handleBlur,
        options: INVENTORY_TYPE_SELECT_OPTIONS,
        selection: true,
        required: true,
      }),
      createFormDropdown({
        name: 'inventoryClass',
        type: 'text',
        label: 'Product Class',
        placeholder: 'Class',
        value: values.inventoryClass,
        values,
        text: titleize(String(values.inventoryClass)),
        error: Boolean(errors.inventoryClass),
        onChange: (_e, { value }) => form.setFieldValue('inventoryClass', value),
        onBlur: handleBlur,
        options: INVENTORY_CLASS_SELECT_OPTIONS,
        selection: true,
        required: true,
      }),
      createFormDropdown({
        name: 'inventoryCategory',
        type: 'text',
        label: 'Product Category',
        placeholder: 'Category',
        value: values.inventoryCategory,
        values,
        text: titleize(String(values.inventoryCategory)),
        error: Boolean(errors.inventoryCategory),
        onChange: (_e, { value }) => form.setFieldValue('inventoryCategory', value),
        onBlur: handleBlur,
        options: INVENTORY_CATEGORY_SELECT_OPTIONS,
        selection: true,
        required: true,
      }),
    ],
    [
      createFormCheckbox({
        name: 'isExciseTaxable',
        label: 'Apply Excise Tax',
        checked: values.isExciseTaxable || false,
        disabled: true,
        error: Boolean(errors.isExciseTaxable),
        onChange: handleChange,
        onBlur: handleBlur,
        required: true,
      }),
    ],
  ]

  return (
    <Box>
      <Loading loading={loading} />
      <ErrorBox error={error} />
      <S.Grid padded>
        <S.Grid.Row>
          <S.Grid.Column>
            <S.Form>
              {fields.map((field, i) => (
                <S.FormGroup key={`formGroup-${i}`} widths="equal">
                  {field}
                </S.FormGroup>
              ))}
              <Button
                color="green"
                name="check"
                title={isAddForm ? 'Create' : 'Update'}
                onClick={() => handleSubmit()}
              />
            </S.Form>
          </S.Grid.Column>
        </S.Grid.Row>
      </S.Grid>
    </Box>
  )
}
