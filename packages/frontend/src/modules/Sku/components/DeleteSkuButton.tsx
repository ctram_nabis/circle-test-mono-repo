import { Box, S } from 'components'
import { Sku, useDeleteSkuMutation, useRestoreSkuMutation } from 'generated/graphql'
import React from 'react'

interface Props {
  disabled?: boolean
  isArchived: boolean
  sku: Sku
}

export const ArchiveSkuButton = ({ disabled, sku, isArchived }: Props) => {
  const [deleteSku, { loading }] = useDeleteSkuMutation()
  const [restoreSku, { loading: restoreLoading }] = useRestoreSkuMutation()

  const handleUndoClick = () => {
    restoreSku({
      variables: {
        skuId: sku.id,
      },
      refetchQueries: ['AllSkus'],
    })
  }

  const handleArchiveClick = () => {
    deleteSku({
      variables: {
        input: {
          id: sku.id,
        },
      },
      refetchQueries: ['AllSkus'],
    })
  }

  const trigger = isArchived ? (
    <S.Button
      loading={restoreLoading}
      icon={'undo'}
      disabled={restoreLoading}
      onClick={() => handleUndoClick()}
      color="grey"
      basic
    />
  ) : (
    <S.Button
      loading={loading}
      icon={disabled || loading ? 'lock' : 'archive'}
      disabled={disabled || loading}
      onClick={() => handleArchiveClick()}
      color="grey"
      basic
    />
  )

  const content = () => (
    <Box p={20}>
      <S.Header>Are you sure you want to archive Product: {sku.code}?</S.Header>
    </Box>
  )

  return <S.Popup content={content} trigger={trigger} />
}
