import { INVENTORY_CATEGORY_ARRAY, INVENTORY_CLASS_ARRAY, INVENTORY_TYPES_ARRAY } from 'modules/Inventory'
import * as Yup from 'yup'

const REQUIRED_MESSAGE = '* Required'

export const productValidationSchema = {
  name: Yup.string()
    .trim()
    .required(REQUIRED_MESSAGE),
  code: Yup.string()
    .trim()
    .required(REQUIRED_MESSAGE),
  isExciseTaxable: Yup.boolean().required(REQUIRED_MESSAGE),
  inventoryType: Yup.mixed()
    .oneOf(INVENTORY_TYPES_ARRAY)
    .required(REQUIRED_MESSAGE)
    .isType(INVENTORY_TYPES_ARRAY),
  inventoryClass: Yup.mixed()
    .oneOf(INVENTORY_CLASS_ARRAY)
    .required(REQUIRED_MESSAGE)
    .isType(INVENTORY_CLASS_ARRAY),
  inventoryCategory: Yup.mixed()
    .oneOf(INVENTORY_CATEGORY_ARRAY)
    .required(REQUIRED_MESSAGE)
    .isType(INVENTORY_CATEGORY_ARRAY),
  pricePerUnit: Yup.number()
    .required(REQUIRED_MESSAGE)
    .positive(),
  unit: Yup.string()
    .trim()
    .required(REQUIRED_MESSAGE),
}

export type ProductFormInputType = Yup.InferType<typeof productValidationSchema>
