import { S } from 'components'
import { Scalars } from 'generated/graphql'
import React from 'react'
import { EditProduct } from './EditProduct'

interface Props {
  onOpen: () => void
  onClose: () => void
  onSuccess?: () => void
  isOpen: boolean
  productId: Scalars['ID']
}

export function EditProductModal({ onOpen, onClose, isOpen, onSuccess, productId }: Props) {
  return (
    <S.Modal
      open={isOpen}
      closeIcon
      closeOnDimmerClick={false}
      onOpen={onOpen}
      onClose={onClose}
      header="Edit Product"
      content={<EditProduct onSuccess={onSuccess} productId={productId} />}
    />
  )
}
