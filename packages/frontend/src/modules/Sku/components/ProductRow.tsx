import { S } from 'components'
import React from 'react'

import { Scalars, Sku } from 'generated/graphql'

import { FeedbackType } from 'modules/Inventory/containers/InventoryPage'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import { strFormat } from 'utils'
import { ArchiveSkuButton } from './DeleteSkuButton'
import { EditProductModalButton } from './EditProductModalButton'

interface Props {
  adminOverrideOrganizationId?: Scalars['ID']
  sku: Sku
  onFeedback: (f: FeedbackType) => void
}

export function ProductRow({ sku: product, adminOverrideOrganizationId }: Props) {
  const { canCreate } = useOrganization()
  const { user } = useMe()
  const { code, name, isExciseTaxable, unit, inventoryType, inventoryClass, inventoryCategory, pricePerUnit } = product

  const productDescription =
    user.isAdmin && !adminOverrideOrganizationId ? `${name}, ${product?.organization?.name ?? 'ERROR'}` : name

  const showActions = canCreate.inventory || user.isAdmin
  const disableButtons = !showActions

  return (
    <S.Table.Row>
      <S.Table.Cell>{code}</S.Table.Cell>
      <S.Table.Cell>{productDescription}</S.Table.Cell>
      <S.Table.Cell>{pricePerUnit && `$${pricePerUnit.toFixed(2)}`}</S.Table.Cell>
      <S.Table.Cell>{unit}</S.Table.Cell>
      <S.Table.Cell>{strFormat(inventoryType as string, {})}</S.Table.Cell>
      <S.Table.Cell>{strFormat(inventoryClass as string, {})}</S.Table.Cell>
      <S.Table.Cell>{strFormat(inventoryCategory as string, {})}</S.Table.Cell>
      <S.Table.Cell>{isExciseTaxable ? 'Yes' : 'No'}</S.Table.Cell>
      {showActions && (
        <S.Table.Cell>
          {user.isAdmin && <EditProductModalButton sku={product} />}
          {canCreate.inventory && (
            <S.Popup
              content={`This will archive SKU: ${code}, so all SKU-Batch pairs related to ${code} will be archived.`}
              trigger={
                <ArchiveSkuButton isArchived={Boolean(product.isArchived)} disabled={disableButtons} sku={product} />
              }
            />
          )}
        </S.Table.Cell>
      )}
    </S.Table.Row>
  )
}
