import { ErrorBox, Loading, S } from 'components'
import { Text } from 'components/core/src/Text'
import React, { Fragment } from 'react'

interface Props {
  sku?: any
  loading?: any
  error?: any
}

export function ProductDetails({ sku, loading, error }: Props) {
  return (
    <Fragment>
      <Loading loading={loading} />
      <ErrorBox error={error} />
      {!loading && !error && sku && (
        <S.Grid padded columns={3}>
          <S.Grid.Row>
            <S.Grid.Column>
              <Text fontWeight="bold">Code: {sku.code}</Text>
            </S.Grid.Column>
            <S.Grid.Column>Name: {sku.name}</S.Grid.Column>
          </S.Grid.Row>
          <S.Grid.Row>
            <S.Grid.Column>Price: {sku.pricePerUnit}</S.Grid.Column>
            <S.Grid.Column>Unit: {sku.unit}</S.Grid.Column>
            <S.Grid.Column>
              Excise Taxable?
              <S.Icon name={sku.isExciseTaxable ? 'check' : 'x'} />
            </S.Grid.Column>
          </S.Grid.Row>
          <S.Grid.Row>
            <S.Grid.Column>Type: {sku.inventoryType}</S.Grid.Column>
            <S.Grid.Column>Class: {sku.inventoryClass}</S.Grid.Column>
            <S.Grid.Column>Category: {sku.inventoryCategory}</S.Grid.Column>
          </S.Grid.Row>
        </S.Grid>
      )}
    </Fragment>
  )
}
