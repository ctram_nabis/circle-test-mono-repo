import { S } from 'components'
import React, { Fragment, useState } from 'react'
import { AddProductModal } from './AddProductModal'

export function AddProductModalButton() {
  const [showModal, setShowModal] = useState<boolean>(false)

  const onOpen = () => setShowModal(true)
  const onClose = () => setShowModal(false)

  return (
    <Fragment>
      {showModal && <AddProductModal onSuccess={onClose} isOpen={showModal} onOpen={onOpen} onClose={onClose} />}
      <S.Button icon="plus" color="green" onClick={onOpen} label="Product" />
    </Fragment>
  )
}
