import { MockedProvider } from '@apollo/react-testing'
import { mount } from 'enzyme'
import React from 'react'
import { AllProductsTab } from '../containers/AllProductsTab'

describe('Products container', () => {
  describe('AllProductsTab', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <AllProductsTab />
        </MockedProvider>,
      )
    })
  })
})
