import { S } from 'components'
import { Sku } from 'generated/graphql'
import React, { Fragment, useState } from 'react'
import { EditProductModal } from './EditProductModal'

interface Props {
  sku: Sku
}

export function EditProductModalButton({ sku }: Props) {
  const [showModal, setShowModal] = useState<boolean>(false)

  const onOpen = () => setShowModal(true)
  const onClose = () => setShowModal(false)

  return (
    <Fragment>
      {showModal && (
        <EditProductModal onSuccess={onClose} isOpen={showModal} onOpen={onOpen} onClose={onClose} productId={sku.id} />
      )}
      <S.Button icon="pencil" color="yellow" basic onClick={onOpen} title="Edit Product" />
    </Fragment>
  )
}
