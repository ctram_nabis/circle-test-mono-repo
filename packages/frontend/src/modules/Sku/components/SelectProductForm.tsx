import React, { Fragment, useState } from 'react'

import { Button, Dropdown, ErrorBox, Label, S } from 'components'
import { Scalars, useAllProductsQuery } from 'generated/graphql'
import { useAdminOverrideOrganization } from 'modules/Inventory/provider/useAdminOverrideOrganization'

interface Props {
  setParentProductId: (productId: Scalars['ID']) => void
  setIsSelecting: (value: boolean) => void
}

export function SelectProductForm({ setParentProductId, setIsSelecting }: Props) {
  const { adminOverrideOrganizationId, organization } = useAdminOverrideOrganization()
  const [inputError, setInputError] = useState<string | undefined>(undefined)
  const [productId, setProductId] = useState<Scalars['ID'] | undefined>(undefined)

  const organizationId =
    adminOverrideOrganizationId !== 'All Organizations' ? adminOverrideOrganizationId : organization.id

  const { data, error, loading } = useAllProductsQuery({ variables: { organizationId } })

  const products = data?.viewer?.allSkus?.results
  const productOptions =
    products &&
    products
      .sort((a, b) => (a.name === b.name ? 0 : a.name && b.name && a.name > b.name ? 1 : -1))
      .map((product) => ({
        key: product.id,
        text: `${product.name} (${product.code})`,
        value: product.id,
      }))
  return (
    <Fragment>
      <ErrorBox error={inputError || error} />
      <S.Grid padded>
        <S.GridRow columns={2}>
          <S.GridColumn>
            <Label fontSize={18} fontWeight="bold">
              Select an Existing Product
            </Label>
          </S.GridColumn>
          <S.GridColumn textAlign="right">
            <Button name="plus" title="Create a new product" color="green" onClick={() => setIsSelecting(false)} />
          </S.GridColumn>
        </S.GridRow>
        <S.GridRow columns={1}>
          <Dropdown
            fluid
            selection
            placeholder="Choose one..."
            loading={loading}
            disabled={loading}
            value={productId}
            onChange={(_e: any, _data: any) => {
              // TODO: fix event types
              const id = _data.value as string
              setProductId(id)
            }}
            options={productOptions}
          />
        </S.GridRow>
        <S.GridRow columns={1}>
          <Button
            title="Next"
            disabled={!productId}
            onClick={() => {
              if (productId) {
                setParentProductId(productId)
              }
              setInputError('Please select a product...')
            }}
          />
        </S.GridRow>
      </S.Grid>
    </Fragment>
  )
}
