import React from 'react'

import { S } from 'components'
import { Box } from 'components/core'
import { Scalars } from 'generated/graphql'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import { AddProductModalButton } from './AddProductModalButton'

interface Props {
  adminOverrideOrganizationId?: Scalars['ID']
  isArchived: boolean
  search?: string
  onChangeSearch: (s: string) => void
  onChangeIsArchived: (b: boolean) => void
}

export function ProductsHeaders({ onChangeSearch, onChangeIsArchived, isArchived, search }: Props) {
  const { user } = useMe()
  const { canCreate } = useOrganization()

  const updateSearch = (_e: React.MouseEvent<HTMLElement, MouseEvent>, { value }: S.SearchProps) => {
    if (value) {
      onChangeSearch(value)
    } else {
      onChangeSearch('')
    }
  }

  const showActions = canCreate.inventory || user.isAdmin

  return (
    <S.Table.Header>
      <S.Table.Row>
        <S.Table.HeaderCell colSpan="1">
          <S.Search
            value={search || ''}
            size="small"
            placeholder="Search..."
            open={false}
            onSearchChange={updateSearch}
          />
        </S.Table.HeaderCell>
        <S.Table.HeaderCell colspan={showActions ? '6' : '4'}>
          {canCreate.inventory && <AddProductModalButton />}
        </S.Table.HeaderCell>
        <S.Table.HeaderCell colSpan="3">
          <S.Checkbox
            toggle
            label="Archived"
            onChange={() => onChangeIsArchived(!isArchived)}
            defaultChecked={isArchived}
          />
        </S.Table.HeaderCell>
      </S.Table.Row>
      <S.Table.Row>
        <S.Table.HeaderCell>
          <Box>Code</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={175}>Name</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={60}>Price</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={75}>Unit</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={110}>Type</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={100}>Class</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={100}>Category</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={55}>Taxable</Box>
        </S.Table.HeaderCell>
        {showActions && (
          <S.Table.HeaderCell>
            <Box width={90}>
              <S.Icon name="settings" />
            </Box>
          </S.Table.HeaderCell>
        )}
      </S.Table.Row>
    </S.Table.Header>
  )
}
