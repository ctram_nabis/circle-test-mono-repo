import { ErrorBox, Loading } from 'components'
import { Maybe, Organization, Scalars, SkuFragmentFragment, useGetSkuQuery } from 'generated/graphql'
import { ProductForm } from 'modules/Sku/components/ProductForm'
import React, { Fragment } from 'react'

type ProductQueryType = { organization: Maybe<Pick<Organization, 'id'>> } & SkuFragmentFragment

interface Props {
  productId: Scalars['ID']
  setParentProductId?: (id: Scalars['ID']) => void
  onSuccess?: () => void
}

export function EditProduct({ productId, setParentProductId, onSuccess }: Props) {
  const { data, loading, error } = useGetSkuQuery({ variables: { id: productId } })
  const product = data?.getSku

  const getInitialFormValues = ({
    name,
    code,
    isExciseTaxable,
    inventoryType,
    inventoryClass,
    inventoryCategory,
    pricePerUnit,
    unit,
  }: ProductQueryType) => ({
    name: name || undefined,
    code: code || undefined,
    isExciseTaxable: typeof isExciseTaxable === 'boolean' ? isExciseTaxable : undefined,
    inventoryType: inventoryType || undefined,
    inventoryClass: inventoryClass || undefined,
    inventoryCategory: inventoryCategory || undefined,
    pricePerUnit: pricePerUnit || undefined,
    unit: unit || undefined,
  })

  return (
    <Fragment>
      <Loading loading={loading} />
      <ErrorBox error={error} />
      {!loading && product && (
        <ProductForm
          onSuccess={onSuccess}
          isAddForm={false}
          initialValues={getInitialFormValues(product)}
          productId={product.id}
          setParentProductId={setParentProductId}
        />
      )}
    </Fragment>
  )
}
