import { Scalars } from 'generated/graphql'
import React from 'react'
import { ProductForm } from './ProductForm'

interface Props {
  setParentProductId?: (id: Scalars['ID']) => void
  onSuccess?: () => void
}

export function AddProduct({ setParentProductId, onSuccess }: Props) {
  return <ProductForm onSuccess={onSuccess} isAddForm={true} setParentProductId={setParentProductId} />
}
