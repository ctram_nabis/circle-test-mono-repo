import { ErrorBox, Loading, Message, PageHeader, S } from 'components'
import { Maybe, useInvoiceDataQuery } from 'generated/graphql'
import { useRouter } from 'modules/Navigation'
import React from 'react'
import { FutureOrders, OrderFromQueryType } from '../components/FutureOrders'

export const FutureOrdersPage = () => {
  const router = useRouter()
  const { id, number: queryNumber = '' } = router.query

  const { data, loading, error } = useInvoiceDataQuery({
    variables: {
      id,
      number: queryNumber,
    },
  })

  const order = data?.getOrder

  const licensedLocationName = order?.site?.licensedLocation?.name
  const invoiceNumber = order?.number

  let futureOrders: Array<Maybe<OrderFromQueryType>> = data?.viewer?.allFutureDropoffsToDispensary || []

  if (!futureOrders.length) {
    futureOrders = data?.viewer?.getAllSiteOutstandingInvoices || []
  }

  return (
    <S.Container>
      <Loading loading={loading} label="Loading Related Orders..." />
      <ErrorBox error={error} />
      {error && (
        <Message
          warning
          header="ask support for help here!"
          content={`tell them: the AdminFutureDropoffsPage broke for invoice #${invoiceNumber} because its licensedLocationId is null. can you set its licensedLocationId?`}
        />
      )}

      {!loading && !error && order && (
        <>
          <PageHeader>
            All orders to {licensedLocationName}, the dispensary of Invoice #{invoiceNumber}
          </PageHeader>
          <>Showing {futureOrders.length} options from the database</>
          <FutureOrders data={futureOrders} order={order} />
        </>
      )}
    </S.Container>
  )
}
