import { Box } from 'components/core/src/Box'
import { DatePicker } from 'components/DatePicker'
// import { ErrorBox } from 'components/ErrorBox'
import { PageHeader } from 'components/PageHeader'
import { Pagination } from 'components/Pagination'
import { Ribbon } from 'components/Ribbon'
import { S } from 'components/SemanticComponents'
import { AllOutstandingInvoicesDocument, Order, useAllOutstandingInvoicesQuery } from 'generated/graphql'
import { usePageInfo } from 'hooks'
import { client } from 'modules/Apollo'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import moment from 'moment'
import React from 'react'
import { useSetState } from 'react-use'
import styled from 'styled-components'
import { fixDecimal, formatEmail, formatPhone, isEmpty, parseDate } from 'utils'
import { AgingReport, CSVButton, NoOrders, Search } from '../components'

const SITE_ERROR = 'ERROR: missing site name, contact support'
const ORG_ERROR = 'ERROR: missing organization name, contact support'

const StyledDatePicker = styled(DatePicker)`
  background-color: transparent;
  border: none;
`

const Wrapper = styled(Box)`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  width: 100%;
  margin-bottom: 12px;
  overflow-x: auto;
`

export interface Invoice {
  collected: string
  creator: string
  creatorEmail: string
  date: string
  dispensary: string
  due: string
  dueDate: string
  futureDate: string
  futureNumber: number
  gmv: string
  gmvCollected: string
  id: string
  name: string
  number: number
  orgName: string
  overdue: number
  tax: string
  taxCollected: string
  warehouse: string
}

const dateFmtMoment = 'MM-DD-YYYY'
const dateFmtDatePicker = 'LL-dd-yyyy'

export const AgingReportsPage = () => {
  const router = useRouter()

  const [state, setState] = useSetState<{
    invoices: Invoice[]
    totalNumPages: number
    orderNumber: string
    debouncedOrderNumber: string
    startDate: string | null
    endDate: string | null
    startDueDate: string | null
    endDueDate: string | null
    warehouse: string
    debouncedWarehouse: string
    dispensary: string
    debouncedDispensary: string
    orgName: string
    debouncedOrgName: string
  }>({
    invoices: [],
    totalNumPages: 0,
    orderNumber: '',
    debouncedOrderNumber: '',
    startDate: null,
    endDate: null,
    startDueDate: null,
    endDueDate: null,
    warehouse: '',
    debouncedWarehouse: '',
    dispensary: '',
    debouncedDispensary: '',
    orgName: '',
    debouncedOrgName: '',
  })

  const { organization, canCreate } = useOrganization()
  const { pageInfo, setPage } = usePageInfo()
  const activePage = Number(router.query.page) || pageInfo.page || 1

  const isAgingReports = router.pathname.includes('aging-reports')

  const { loading, error } = useAllOutstandingInvoicesQuery({
    variables: {
      organizationId: isAgingReports ? organization.id : undefined,
      pageInfo: { ...pageInfo, page: Number(router.query.page) || 1 },
      orderNumber: state.debouncedOrderNumber,
      startDate: state.startDate,
      endDate: state.endDate,
      startDueDate: state.startDueDate,
      endDueDate: state.endDueDate,
      warehouse: state.debouncedWarehouse,
      dispensary: state.debouncedDispensary,
      orgName: state.debouncedOrgName,
    },
    onCompleted: (data) => {
      const _invoices = data?.viewer?.allOutstandingInvoices?.results || []

      const outstandingInvoices = convertOrdersToInvoices(_invoices as Order[])

      setState({
        invoices: outstandingInvoices,
        totalNumPages: data?.viewer?.allOutstandingInvoices?.pageInfo?.totalNumPages || 0,
      })
    },
  })

  const handlePageChange = (_e: Event, data: any) => {
    const actPage: number = data.activePage

    setPage(actPage)

    router.replace(`${router.pathname}?page=${actPage}`)
  }

  const renderDatePicker = (key: string, placeholderText: string = '') => (
    <>
      <StyledDatePicker
        id={key}
        name={key}
        selected={state[key] ? moment(state[key], dateFmtMoment).toDate() : null}
        onChange={(date: any) => {
          // TODO: Need to debounce network requests when user is typing in date
          const mDate = date && moment(date, dateFmtMoment)

          if (mDate && mDate.isValid()) {
            setState({
              [key]: moment(date, dateFmtMoment).format(dateFmtMoment),
            })
          } else {
            setState({
              [key]: null,
            })
          }
        }}
        dateFormat={dateFmtDatePicker}
        placeholderText={placeholderText}
      />
      {state[key] && (
        <S.Button size="small" onClick={() => setState({ [key]: null })}>
          clear
        </S.Button>
      )}
    </>
  )

  const getAllInvoices = async () => {
    const response = await client.query({
      query: AllOutstandingInvoicesDocument,
      variables: {
        organizationId: isAgingReports ? organization.id : undefined,
        orderNumber: state.debouncedOrderNumber,
        startDate: state.startDate,
        endDate: state.endDate,
        startDueDate: state.startDueDate,
        endDueDate: state.endDueDate,
        warehouse: state.debouncedWarehouse,
        dispensary: state.debouncedDispensary,
        orgName: state.debouncedOrgName,
        all: true,
      },
    })

    return response.data?.viewer?.allOutstandingInvoices?.results
  }

  const convertOrdersToInvoices = (_invoices: Order[]) =>
    _invoices.map((invoice) => {
      const {
        id,
        name,
        number: _orderNumber,
        collectWithOrder,
        creator,
        date: utcDate,
        daysTillPaymentDue,
        discount,
        exciseTax,
        exciseTaxCollected,
        gmv,
        gmvCollected,
        site,
        organization: invoiceOrg,
      } = invoice

      const momentDate = moment(utcDate).tz('America/Los_Angeles')

      const dueDate = momentDate
        .clone()
        .add(daysTillPaymentDue || 0, 'days')
        .toDate()
      const futureDate = (collectWithOrder && collectWithOrder.date) as Date

      const { firstName, lastName, phone, email } = creator || {}

      const exciseTaxCollectedZero = exciseTaxCollected || 0
      const calculatedAmountDue = gmv - discount + exciseTax - gmvCollected - exciseTaxCollectedZero
      const collectedAmount = gmvCollected + exciseTaxCollectedZero
      const taxCollectedAmount = exciseTaxCollectedZero

      const due = fixDecimal(calculatedAmountDue)
      const collected = fixDecimal(collectedAmount)
      const taxCollected = fixDecimal(taxCollectedAmount)

      return {
        overdue: moment().diff(dueDate, 'days'),
        id,
        name,
        due,
        gmv: fixDecimal(gmv - discount),
        tax: fixDecimal(exciseTax),
        number: _orderNumber,
        date: parseDate(momentDate.toDate()),
        dueDate: parseDate(dueDate),
        creator:
          (creator && phone && email && `${firstName} ${lastName}\n${formatPhone(phone)}\n${formatEmail(email)}`) || '',
        creatorEmail: (email && formatEmail(email)) || '',
        futureDate: parseDate(futureDate),
        collected,
        gmvCollected: fixDecimal(gmvCollected),
        taxCollected,
        dispensary: site.name || SITE_ERROR,
        orgName: invoiceOrg.name || ORG_ERROR,
        futureNumber: (collectWithOrder && collectWithOrder.number) || 0,
        warehouse: invoice?.warehouse?.site?.licensedLocation?.name || '',
      }
    })

  return (
    <Wrapper>
      <S.Container fluid>
        <S.Grid columns={1} padded>
          <S.GridRow>
            <S.GridColumn>
              <Box display="flex" justifyContent="space-between">
                <PageHeader>{!isAgingReports ? 'Outstanding Invoices' : 'Aging Reports'}</PageHeader>
                {!loading && !error && canCreate.csvExport && (
                  <S.Button.Group>
                    <CSVButton invoices={state.invoices} />
                    <CSVButton all getAllInvoices={getAllInvoices} convertOrdersToInvoices={convertOrdersToInvoices} />
                  </S.Button.Group>
                )}
              </Box>
            </S.GridColumn>
          </S.GridRow>

          {/* TODO: Reenable ErrorBox */}
          {/* <ErrorBox error={error} /> */}

          <S.GridRow>
            <S.GridColumn>
              <Pagination
                onPageChange={(e, data) => handlePageChange(e as Event, data as object)}
                activePage={activePage}
                totalNumPages={state.totalNumPages}
                siblingRange={3}
              />

              <S.Table>
                <S.Table.Header>
                  <S.Table.Row>
                    <S.Table.HeaderCell>
                      <Box width={100}>
                        <Ribbon color="green">Order</Ribbon>
                      </Box>
                      <Search
                        value={state.orderNumber}
                        setState={(val) => setState({ orderNumber: val })}
                        setDebouncedState={(val) => setState({ debouncedOrderNumber: val })}
                        placeholder="#"
                      />
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={90}>Warehouse</Box>
                      {/* <Search
                      value={state.warehouse}
                      setState={(val) => setState({ warehouse: val })}
                      setDebouncedState={(val) => setState({ debouncedWarehouse: val })}
                      placeholder="Warehouse"
                    /> */}
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={125}>{!isAgingReports ? 'Brand' : 'Name'}</Box>
                      <Search
                        value={state.orgName}
                        setState={(val) => setState({ orgName: val })}
                        setDebouncedState={(val) => setState({ debouncedOrgName: val })}
                        placeholder={!isAgingReports ? 'Brand' : 'Name'}
                      />
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={125}>Dispensary</Box>
                      <Search
                        value={state.dispensary}
                        setState={(val) => setState({ dispensary: val })}
                        setDebouncedState={(val) => setState({ debouncedDispensary: val })}
                        placeholder="Dispensary"
                      />
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={100}>Delivery Date</Box>
                      <Box width={100}>
                        {renderDatePicker('startDate', 'start date')}
                        {renderDatePicker('endDate', 'end date')}
                      </Box>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={100}>Due Date</Box>
                      <Box width={100}>
                        {renderDatePicker('startDueDate', 'start date')}
                        {renderDatePicker('endDueDate', 'end date')}
                      </Box>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={80}>Days Overdue</Box>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={80}>Collected</Box>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={80}>Due</Box>
                    </S.Table.HeaderCell>
                    {/* TODO: DEPRECATED TEMPORARILY - FUTURE DROPOFFS */}
                    {/* <S.Table.HeaderCell>{!isAgingReports ? 'Piggybacking On' : 'Sales Person'}</S.Table.HeaderCell> */}
                    {isAgingReports && (
                      <S.Table.HeaderCell>
                        <Box width={150}>Sales Person</Box>
                      </S.Table.HeaderCell>
                    )}
                  </S.Table.Row>
                </S.Table.Header>

                <S.Table.Body>
                  {loading ? (
                    <S.Loader
                      active
                      size="large"
                      content={`Loading ${isAgingReports ? 'Aging Reports...' : 'Outstanding Invoices...'}`}
                      style={{ marginTop: 180 }}
                    />
                  ) : (
                    !error &&
                    (isEmpty(state.invoices) ? (
                      <NoOrders />
                    ) : (
                      state.invoices.map((filteredInvoice) => (
                        <AgingReport
                          key={filteredInvoice.id}
                          invoice={filteredInvoice}
                          isAgingReports={isAgingReports}
                        />
                      ))
                    ))
                  )}
                </S.Table.Body>
              </S.Table>
            </S.GridColumn>
          </S.GridRow>

          <Pagination
            onPageChange={(e, data) => handlePageChange(e as Event, data as object)}
            activePage={activePage}
            totalNumPages={state.totalNumPages}
            siblingRange={3}
          />
        </S.Grid>
      </S.Container>
    </Wrapper>
  )
}
