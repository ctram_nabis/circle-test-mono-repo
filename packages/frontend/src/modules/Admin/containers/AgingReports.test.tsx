import { MockedProvider } from '@apollo/react-testing'
import { shallow } from 'enzyme'
import React from 'react'
import { AgingReport } from '../components/AgingReport'
import { AgingReportsPage } from './AgingReportsPage'

describe('`AgingReports` container', () => {
  it('`AgingReport` renders without crashing', () => {
    shallow(<AgingReport invoice={{}} />)
  })
  it('`AgingReportsPage` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <AgingReportsPage />
      </MockedProvider>,
    )
  })
})
