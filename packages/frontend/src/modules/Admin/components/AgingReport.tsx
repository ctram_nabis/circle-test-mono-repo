// TODO: Replace class-based styling with styled
import { S } from 'components'
import { Text } from 'components/core/src/Text'
import { useOrganization } from 'modules/Organization'
import React from 'react'
import { dollarFormat } from 'utils'
import { Invoice } from '../containers/AgingReportsPage'

interface Props {
  invoice: Invoice
  isAgingReports: boolean
}

export const AgingReport = (props: Props) => {
  const { invoice, isAgingReports } = props
  const {
    collected,
    creator,
    date,
    dispensary,
    due,
    dueDate,
    // TODO: DEPRECATED TEMPORARILY - FUTURE DROPOFFS
    // futureDate,
    // futureNumber,
    // id,
    name,
    number: invoiceNumber,
    orgName,
    overdue,
    warehouse,
  } = invoice
  const { organization } = useOrganization()

  // TODO: DEPRECATED TEMPORARILY - FUTURE DROPOFFS
  // const organization = useOrganization()
  // const isAssignedToFutureDropoff = Boolean(futureNumber && futureDate)

  // const linkText = isAssignedToFutureDropoff
  //   ? `Assigned to #${futureNumber} on ${futureDate}`
  //   : `See all related dropoffs to ${dispensary}`

  const creatorInfoArray =
    creator && creator.split('\n').map((lineVal, i) => <Text key={`${lineVal}-${i}`}>{lineVal}</Text>)
  const decodedOrderId = atob(invoice.id)

  return (
    <S.Table.Row className={overdue > 0 ? 'negative' : overdue <= 0 && overdue > -8 ? 'warning' : ''}>
      <S.Table.Cell>
        <a href={`/${organization.alias}/app/orders/${btoa(decodedOrderId)}`}>#{invoiceNumber}</a>
      </S.Table.Cell>
      <S.Table.Cell>{warehouse}</S.Table.Cell>
      <S.Table.Cell>{!isAgingReports ? orgName : name}</S.Table.Cell>
      <S.Table.Cell>{dispensary}</S.Table.Cell>
      <S.Table.Cell>{date}</S.Table.Cell>
      <S.Table.Cell>{dueDate}</S.Table.Cell>
      <S.Table.Cell>{overdue}</S.Table.Cell>
      <S.Table.Cell>{dollarFormat(collected)}</S.Table.Cell>
      <S.Table.Cell>{dollarFormat(due)}</S.Table.Cell>
      {isAgingReports && <S.Table.Cell>{creatorInfoArray}</S.Table.Cell>}
      {/* TODO: DEPRECATED TEMPORARILY - FUTURE DROPOFFS */}
      {/* <S.Table.Cell>
        {!isAgingReports ? (
          <Link
            to={`/${organization.alias}/app/admin-outstanding-invoices/${id}/future-dropoffs`}
            target="_blank"
            rel="noopener noreferrer"
          >
            {linkText}
          </Link>
        ) : (
          creatorInfoArray
        )}
      </S.Table.Cell> */}
    </S.Table.Row>
  )
}
