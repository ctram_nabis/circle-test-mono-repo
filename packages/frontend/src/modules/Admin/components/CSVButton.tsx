import { S } from 'components'
import { ExportToCsv } from 'export-to-csv'
import { Order } from 'generated/graphql'
import { useRouter } from 'modules/Navigation'
import React, { useState } from 'react'
import { isEmpty } from 'utils'
import { Invoice } from '../containers/AgingReportsPage'

interface Props {
  invoices?: Invoice[]
  all?: boolean
  getAllInvoices?: () => Promise<any[]>
  convertOrdersToInvoices?: (invoices: Order[]) => Invoice[]
}

export const CSVButton = ({ invoices = [], all = false, getAllInvoices, convertOrdersToInvoices }: Props) => {
  const router = useRouter()
  const [loading, setLoading] = useState<boolean>(false)

  const exportCSV = async () => {
    try {
      setLoading(true)

      let invoicesToExport = invoices

      if (all && getAllInvoices && convertOrdersToInvoices) {
        /**
         * Export ALL outstanding orders
         */

        const results = await getAllInvoices()
        invoicesToExport = convertOrdersToInvoices(results)
      }

      const toJoin = Object.values(router.query)
      toJoin.push(router.location.hash)

      let filename = toJoin.join('_').trim()
      filename += 'aging_reports'

      if (isEmpty(invoicesToExport)) return

      const csvExporter = new ExportToCsv({
        filename,
        useBom: true,
        useKeysAsHeaders: true,
      })

      csvExporter.generateCsv(invoicesToExport)
      setLoading(false)
      return
    } catch (error) {
      alert(`Error: ${error.message}`)
      setLoading(false)
    }
  }

  return (
    <S.Popup
      trigger={
        <S.Button basic={all ? false : true} color="green" onClick={exportCSV} disabled={loading} loading={loading}>
          {loading ? 'Loading...' : all ? 'Export All Invoices (CSV)' : 'Export Current Page (CSV)'}
        </S.Button>
      }
      content="You might have to disable your ad-blocker if you have not already!"
      position="bottom center"
    />
  )
}
