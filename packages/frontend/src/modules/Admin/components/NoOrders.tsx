import { Button, S } from 'components'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import React from 'react'

export const NoOrders = () => {
  const router = useRouter()
  const { organization, canCreate } = useOrganization()

  return (
    <S.Table.Row>
      <S.Table.Cell colSpan="10">
        <S.Segment placeholder className="flex-style">
          <S.Header icon>
            <S.Icon name="database" />
            Looks like you don't have any due orders! Would you like to place an order?
          </S.Header>
          {canCreate.order && (
            <Button onClick={() => router.push(`/${organization.alias}/app/orders/new`)} title="Create an Order" />
          )}
        </S.Segment>
      </S.Table.Cell>
    </S.Table.Row>
  )
}
