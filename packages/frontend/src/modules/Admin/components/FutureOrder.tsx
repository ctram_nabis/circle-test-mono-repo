import { Button, S } from 'components'
import { useAssignInvoiceToOrderMutation } from 'generated/graphql'
import React from 'react'
import { parseDate } from 'utils'
import { AssignVars, OrderFromQueryType } from './FutureOrders'

interface Props {
  assignButtonText: string
  assignVars: AssignVars
  isAssigned: boolean
  order: OrderFromQueryType
  xor: boolean
}

export const FutureOrder = (props: Props) => {
  const { assignButtonText, assignVars, isAssigned, order, xor } = props
  const { date, manifestGDriveFileId, number: orderNumber } = order

  const [assignInvoiceToOrderMutation, { loading }] = useAssignInvoiceToOrderMutation({
    refetchQueries: ['InvoiceData'],
  })

  const handleAssignOrder = () => {
    assignInvoiceToOrderMutation({ variables: assignVars })
  }

  return (
    <S.Table.Row>
      <S.Table.Cell>{parseDate(date)}</S.Table.Cell>
      <S.Table.Cell>#{orderNumber}</S.Table.Cell>
      <S.Table.Cell>
        <a href={`https://drive.google.com/file/d/${manifestGDriveFileId}`} target="_blank" rel="noopener noreferrer">
          View in new tab
        </a>
        {xor ? (
          <Button disabled>{assignButtonText}</Button>
        ) : (
          <Button
            color={isAssigned ? 'grey' : 'green'}
            onClick={handleAssignOrder}
            disabled={loading}
            loading={loading}
          >
            {assignButtonText}
          </Button>
        )}
      </S.Table.Cell>
    </S.Table.Row>
  )
}
