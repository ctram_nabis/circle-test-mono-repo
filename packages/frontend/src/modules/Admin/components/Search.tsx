import { S } from 'components'
import React from 'react'
import { useDebounce } from 'react-use'
import { ChangeEvent } from 'types'

interface Props {
  value: string
  setState: (str: string) => void
  setDebouncedState: (str: string) => void
  placeholder?: string
}

export const Search = (props: Props) => {
  const { value, setState, setDebouncedState, placeholder } = props

  useDebounce(
    () => {
      setDebouncedState(value)
    },
    500,
    [value],
  )

  return (
    <S.Input
      icon="search"
      onInput={(e: ChangeEvent) => setState(e.target.value)}
      placeholder={placeholder}
      value={value}
      fluid
    />
  )
}
