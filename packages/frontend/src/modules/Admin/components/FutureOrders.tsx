import { S } from 'components'
import { Maybe, Order } from 'generated/graphql'
import React from 'react'
import { parseDate } from 'utils'
import { FutureOrder } from './FutureOrder'

export type OrderFromQueryType = Pick<Order, 'number' | 'date' | 'id' | 'manifestGDriveFileId'> & {
  collectWithOrder: Maybe<Pick<Order, 'id'>>
}

interface Props {
  data: Array<Maybe<OrderFromQueryType>>
  order: OrderFromQueryType
}

export interface AssignVars {
  invoiceId: string
  orderId: string
}

export const FutureOrders = (props: Props) => {
  const isInvoice = (invoice = props.order) => {
    return new Date(invoice?.date) > new Date()
  }

  const getAssignVars = (futureOrder: OrderFromQueryType) => {
    const invoice = props.order
    return isInvoice()
      ? {
          invoiceId: futureOrder.id,
          orderId: invoice.id,
        }
      : {
          invoiceId: invoice.id,
          orderId: futureOrder.id,
        }
  }

  const xor = (order: OrderFromQueryType) => {
    const isNotInvoiceYet = new Date(order?.date) < new Date()
    const _isInvoice = isInvoice()
    // return isInvoice() ^ isNotInvoiceYet
    // http://www.howtocreate.co.uk/xor.html
    return (_isInvoice || isNotInvoiceYet) && !(_isInvoice && isNotInvoiceYet)
  }

  const getAssignButtonText = (futureOrder: OrderFromQueryType) => {
    const invoice = props.order
    let text = isAssignedInvoice(futureOrder)
      ? isInvoice()
        ? `#${invoice.number} currently assigned to #${futureOrder.number} to collect on ${getDateString(futureOrder)}`
        : `#${futureOrder.number} currently assigned to #${invoice.number} to collect on ${getDateString(futureOrder)}`
      : isInvoice()
      ? `Assign #${invoice.number} to #${futureOrder.number} to collect on ${getDateString(futureOrder)}`
      : `Assign #${futureOrder.number} to #${invoice.number} to collect on ${getDateString(futureOrder)}`

    if (xor(futureOrder)) {
      text = isInvoice()
        ? `Unable to assign #${invoice.number} to #${futureOrder.number} for ${getDateString(futureOrder)}`
        : `Unable to assign #${futureOrder.number} to #${invoice.number} for ${getDateString(futureOrder)}`
    }

    return text
  }

  const getDateString = (futureOrder: OrderFromQueryType) => {
    const invoice = props.order
    return isInvoice() ? parseDate(invoice.date) : parseDate(futureOrder.date)
  }

  const isAssignedInvoice = (futureOrder: OrderFromQueryType) => {
    const invoice = props.order
    return invoice.collectWithOrder?.id === futureOrder.id || futureOrder.collectWithOrder?.id === invoice.id
  }

  return (
    <S.Table>
      <S.Table.Header>
        <S.Table.Row>
          <S.Table.HeaderCell>Date</S.Table.HeaderCell>
          <S.Table.HeaderCell>Order</S.Table.HeaderCell>
          <S.Table.HeaderCell>Actions</S.Table.HeaderCell>
        </S.Table.Row>
      </S.Table.Header>
      <S.Table.Body>
        {(props.data || []).map((order) => (
          <FutureOrder
            key={order!.id}
            order={order!}
            assignButtonText={getAssignButtonText(order!)}
            xor={xor(order!)}
            isAssigned={isAssignedInvoice(order!)}
            assignVars={getAssignVars(order!)}
          />
        ))}
      </S.Table.Body>
    </S.Table>
  )
}
