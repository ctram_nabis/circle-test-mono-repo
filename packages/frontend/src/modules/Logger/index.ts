import logger from 'loglevel'

logger.setLevel('info')

export { logger }

export default logger
