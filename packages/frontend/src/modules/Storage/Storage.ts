import deepmerge from 'deepmerge'

const storage = localStorage

export class Storage {
  static saveString = (name: string, value: string) => storage.setItem(name, value)

  static save = (name: string, value: any) => storage.setItem(name, JSON.stringify(value))

  static loadString = (name: string) => storage.getItem(name)

  static load = <T>(name: string): Promise<T | null> => {
    const savedItem = Storage.loadString(name)
    return savedItem && JSON.parse(savedItem)
  }

  static merge = <T>(name: string, value: T) => {
    const savedItem = Storage.load<T>(name)
    const mergedItem = deepmerge(savedItem || {}, value)
    Storage.save(name, mergedItem)
  }

  static remove = (name: string) => storage.removeItem(name)

  static clear = () => storage.clear()
}
