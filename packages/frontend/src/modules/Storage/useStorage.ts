import { Dispatch, SetStateAction, useCallback, useState } from 'react'

export const useStorage = (key: string): [string | null, Dispatch<SetStateAction<string | null>>] => {
  const [value, setValue] = useState(localStorage.getItem(key))

  const setStorageValue = useCallback(
    (newValue: string | null) => {
      if (!newValue || newValue === 'undefined') {
        localStorage.removeItem(key)
        setValue(null)
      } else {
        localStorage.setItem(key, newValue)
        setValue(newValue)
      }
    },
    [setValue, key],
  )

  return [value, setStorageValue]
}
