import queryString from 'query-string'
import { oc } from 'ts-optchain'

export default (location) => {
  const qs = queryString.parse(oc(location).search(''))

  const warehouseId = typeof qs.warehouseId === 'string' ? qs.warehouseId : ''
  const driverId = typeof qs.driverId === 'string' ? qs.driverId : ''
  const vehicleId = typeof qs.vehicleId === 'string' ? qs.vehicleId : ''
  const page = typeof qs.page === 'string' ? qs.page : ''

  return {
    search: qs.search,
    action: qs.action,
    status: qs.status,
    paymentStatus: qs.paymentStatus,
    transactionTypes: typeof qs.transactionTypes === 'string' ? qs.transactionTypes.split(',') : qs.transactionTypes,
    warehouseId:
      qs.warehouseId === 'UN_ASSIGNED'
        ? null
        : qs.warehouseId && qs.warehouseId.length
        ? decodeURIComponent(warehouseId)
        : undefined,
    driverId:
      qs.driverId === 'UN_ASSIGNED'
        ? null
        : qs.driverId && qs.driverId.length
        ? decodeURIComponent(driverId)
        : undefined,
    vehicleId:
      qs.vehicleId === 'UN_ASSIGNED'
        ? null
        : qs.vehicleId && qs.vehicleId.length
        ? decodeURIComponent(vehicleId)
        : undefined,
    start: qs.start,
    end: qs.end,
    pageInfo: {
      page: parseInt(page, 10) || 1,
      numItemsPerPage: 25,
      orderBy: [
        {
          attribute: 'date',
          order: 'DESC',
        },
        {
          attribute: 'createdAt',
          order: 'DESC',
        },
        {
          attribute: 'number',
          order: 'DESC',
        },
      ],
    },
  }
}
