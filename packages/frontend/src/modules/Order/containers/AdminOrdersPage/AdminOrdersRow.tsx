import { ORDER_ACTIONS } from '../../constants/OrderActionEnum'

import moment from 'moment-timezone'
// import multiDownload from 'multi-download'
import React, { useState } from 'react'
import styled from 'styled-components'

import { logger } from '@sentry/utils'
import dollarFormat from 'utils/formatters/dollarFormat'
import { parseDate } from 'utils/formatters/parse'
import titleize from 'utils/formatters/titleize'
import titleizeDaysTillPaymentDue from 'utils/formatters/titleizeDaysTillPaymentDue'
import { calculateMoney } from '../../helpers/calculateMoney'

import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { DatePicker } from 'components/DatePicker'
import { Dropdown } from 'components/Dropdown'
import { Input } from 'components/Input'
import { P } from 'components/P'
import { Popup } from 'components/Popup'
import { S } from 'components/SemanticComponents'
import { IntakeOuttakeModalButtons } from 'modules/Inventory'
import { CellOptionLabel } from 'modules/Order/components/OrdersTableRow'
import 'react-datepicker/dist/react-datepicker.css'
import { ActionCookie } from '../../components/ActionCookie'
import AdminOrderNotes from '../../components/AdminOrderNotes'
import { CopyOrder } from '../../components/CopyOrder'

import {
  Order,
  useCreateOrderChangeMutation,
  useGenerateManifestMutation,
  useSendSendGridStatusUpdateEmailMutation,
  useUpdateOrderMutation,
} from 'generated/graphql'

import { Text } from 'components'
import { useRouter } from 'modules/Navigation'
import { useSetState } from 'react-use'

const CellOption = styled.div`
  margin-bottom: 5px;
`
const ActionLink = styled.a`
  cursor: pointer;
  text-decoration: underline;
`
const SubLabelWrapper = styled.span`
  font-weight: 100;
`

const AfterBeforeContainer = styled.div`
  width: 100%;
`
const WarningBold = styled.strong`
  color: #ffbb00;
`

const StyledDatePicker = styled(DatePicker)`
  background-color: transparent;
  border: none;
`
interface AdminHoversInterface {
  bTag: HTMLElement
  child: AdminOrderNotes
}

interface resumeDataPromptInterface {
  _e: any
  data: string | any
}

interface updateOrderWithNotesInterface {
  id: string
  internalNotes?: string
  adminNotes?: string
}

const statuses: Array<{ value: string; text: string; key: string }> = [
  { key: 'UNSCHEDULED', value: 'UNSCHEDULED', text: 'Unscheduled' },
  { key: 'SCHEDULED', value: 'SCHEDULED', text: 'Scheduled' },
  { key: 'TRANSFERRING', value: 'TRANSFERRING', text: 'Transferring' },
  { key: 'DELIVERED', value: 'DELIVERED', text: 'Delivered' },
  {
    key: 'DELIVERED_WITH_EDITS',
    value: 'DELIVERED_WITH_EDITS',
    text: 'Delivered with Edits',
  },
  { key: 'DELAYED', value: 'DELAYED', text: 'Delayed' },
  { key: 'REJECTED', value: 'REJECTED', text: 'Rejected' },
  { key: 'CANCELLED', value: 'CANCELLED', text: 'Cancelled' },
]

const paymentStatuses: Array<{ key: string; value: string; text: string }> = [
  { key: 'UNPAID', value: 'UNPAID', text: 'Unpaid' },
  { key: 'COD_PAID', value: 'COD_PAID', text: 'COD, Paid' },
  { key: 'NET_TERMS_PAID', value: 'NET_TERMS_PAID', text: 'Net Terms, Paid' },
  { key: 'REMITTED', value: 'REMITTED', text: 'Remitted' },
  { key: 'SELF_COLLECTED', value: 'SELF_COLLECTED', text: 'Self Collected' },
]

interface OptionsArrayInterface {
  key: string
  value: string
  text: string
  disabled?: boolean
}

interface Props {
  key: string
  onClickCheckbox: (isSelected: boolean) => void
  order: Order
  allWarehouses: OptionsArrayInterface[]
  allDrivers: OptionsArrayInterface[]
  allVehicles: OptionsArrayInterface[]
  userId: string
  isSelected: boolean
}

function AdminOrdersRow(props: Props) {
  const { order, allWarehouses, userId, allDrivers, allVehicles, onClickCheckbox, isSelected } = props

  const router = useRouter()

  let resumePromptData: resumeDataPromptInterface | null = null

  const [paymentStatusMessage, setPaymentStatusMessage] = useState(<p />)
  const [orderStatusMessage, setOrderStatusMessage] = useState(<p />)
  const [estimatedArrivalTimeAfter, setEstimatedArrivalTimeAfter] = useState(order.estimatedArrivalTimeAfter)
  const [estimatedArrivalTimeBefore, setEstimatedArrivalTimeBefore] = useState(order.estimatedArrivalTimeBefore)
  const [gmvCollected, setGmvCollected] = useState(order.gmvCollected?.toString())
  const [exciseTaxCollected, setExciseTaxCollected] = useState(
    order.exciseTaxCollected ? order.exciseTaxCollected.toString() : '',
  )

  const [warehouseId, setWarehouseId] = useState(order?.warehouse?.id)
  const [driverId, setDriverId] = useState(order?.driver?.id)
  const [vehicleId, setVehicleId] = useState(order?.vehicle?.id)

  const [isSendingWarehouse, setIsSendingWarehouse] = useState(false)
  const [isSendingDriver, setIsSendingDriver] = useState(false)
  const [isSendingVehicle, setIsSendingVehicle] = useState(false)

  const [isSendingOrderStatus, setIsSendingOrderStatus] = useState(false)
  const [isSendingPaymentInfo, setIsSendingPaymentInfo] = useState(false)
  const [isSendingEstArrivalTime, setIsSendingEstArrivalTime] = useState(false)
  const [isSendingPaymentStatus, setIsSendingPaymentStatus] = useState(false)
  const [isGeneratingManifest, setIsGeneratingManifest] = useState(false)

  const [{ apSummaryGDriveFileId, invoicesS3FileLink }, setFileLinks] = useSetState({
    apSummaryGDriveFileId: order.apSummaryGDriveFileId,
    invoicesS3FileLink: order.invoicesS3FileLink,
  })

  let adminHovers: AdminHoversInterface | null = null

  const refetchQueries = ['AllAdminOrdersPage']

  const [generateManifestMutation] = useGenerateManifestMutation({
    refetchQueries,
  })

  const [updateOrderMutation] = useUpdateOrderMutation()

  const [sendSendGridStatusUpdateEmailMutation] = useSendSendGridStatusUpdateEmailMutation()

  const [createOrderChangeMutation] = useCreateOrderChangeMutation()

  const checkStatusAdminNotes = (_e: React.SyntheticEvent<HTMLElement, Event>, data: any) => {
    const value = data.value
    const temp = statuses.find((s) => s.value === value)

    const newText = temp ? temp.text : ''
    const promptStatuses = ['Delivered with Edits', 'Delayed', 'Rejected', 'Cancelled']

    if (promptStatuses.includes(newText)) {
      const triggerRippleHover = () => {
        const event = new MouseEvent('mouseover', {
          view: window,
          bubbles: true,
          cancelable: true,
        })
        if (adminHovers) {
          adminHovers.bTag.dispatchEvent(event)
          adminHovers.child.PARENT_setPrompt(true)
        }
      }
      triggerRippleHover()
      resumePromptData = { _e, data }
    } else {
      if (adminHovers) {
        adminHovers.child.PARENT_setPrompt(false)
        handleStatusChange(_e, data)
      }
    }
  }

  const resumeStatusChange = ({ adminNotes }: { adminNotes: string }) => {
    if (resumePromptData === null || !resumePromptData._e || !resumePromptData.data) return

    const { _e, data } = resumePromptData

    const triggerMouseOut = () => {
      const event = new MouseEvent('mouseout', {
        view: window,
        bubbles: true,
        cancelable: true,
      })
      if (adminHovers) {
        adminHovers.bTag.dispatchEvent(event)
      }
    }
    triggerMouseOut()
    handleStatusChange(_e, data, adminNotes)
  }

  const handleStatusChange = async (_e: React.SyntheticEvent<HTMLElement, Event>, data: any, adminNotes?: string) => {
    resumePromptData = null
    const value = data.value
    const msg = (
      <P>
        Send email? <button onClick={() => handleEmailYesClick('order')}>yes</button> /{' '}
        <button onClick={() => handleEmailNoClick('order')}>no</button>
      </P>
    )

    setIsSendingOrderStatus(true)

    try {
      if (adminNotes) {
        await updateOrderMutation({
          variables: {
            input: {
              id: order.id,
              status: value,
              adminNotes,
            },
          },
        })
      } else {
        await updateOrderMutation({
          variables: {
            input: {
              id: order.id,
              status: value,
            },
          },
        })
      }

      setIsSendingOrderStatus(false)
      setStatusMessages('order', msg)
    } catch (error) {
      logger.error('Error with updateOrder on AdminOrderPage:handleStatusChange', error)
    }
  }

  const handleEmailYesClick = async (field: string) => {
    setStatusMessages(field, <P>Queueing...</P>)

    try {
      sendSendGridStatusUpdateEmailMutation({
        variables: {
          input: {
            orderId: order.id,
          },
        },
      })

      // TODO: save the field of the message type and update the UI after completion
      const newMsg = <P>Email scheduled to send. Bye!</P>
      setStatusMessages(field, newMsg)
      setTimeout(() => {
        setStatusMessages(field, <p />)
      }, 1300)
    } catch (error) {
      logger.error('Error with sendSendGridStatusUpdateEmail on AdminOrderPage:handleEmailYesClick', error)
    }
  }

  const handleEmailNoClick = async (field: string) => {
    setStatusMessages(field, <P>Will not send then. Bye!</P>)

    setTimeout(() => {
      setStatusMessages(field, <p />)
    }, 1300)
  }

  const setStatusMessages: (type: string, msg: JSX.Element) => void = (type, msg) => {
    if (type === 'order') {
      setOrderStatusMessage(msg)
    } else if (type === 'payment') {
      setPaymentStatusMessage(msg)
    }
  }

  //  onTextBoxInit({ element }: {element: ElementRef }){}

  const updateOrderWithNotes = ({
    id,
    internalNotes,
    adminNotes,
  }: {
    id: string
    internalNotes?: string
    adminNotes?: string
  }) => {
    let value = ''
    let key = ''

    if (internalNotes) {
      value = internalNotes
      key = 'internalNotes'
    } else if (adminNotes) {
      value = adminNotes
      key = 'adminNotes'
    }

    return updateOrderMutation({
      variables: {
        input: {
          id,
          [key]: value,
        },
      },
    })
  }

  /*
      We don't want to nag about missing intakes / outtakes
      for actions that don't require them. These are simple
      checks to determine if the label should be included.
    */

  const requiresIntakeOnly: (action: string) => boolean = (action) => {
    return (
      action === ORDER_ACTIONS.PICKUP_FROM_BRAND ||
      action === ORDER_ACTIONS.RETURN_FROM_RETAILER ||
      action === ORDER_ACTIONS.DROPOFF_TO_NABIS
    )
  }

  const requiresOuttakeOnly: (action: string) => boolean = (action) => {
    return (
      action === ORDER_ACTIONS.DELIVERY_TO_BRAND ||
      action === ORDER_ACTIONS.DELIVERY_TO_RETAILER ||
      action === ORDER_ACTIONS.PICKUP_FROM_NABIS
    )
  }

  const requiresIntakeAndOuttake: (action: string) => boolean = (action) => {
    return action === ORDER_ACTIONS.INTERNAL_TRANSFER || action === ORDER_ACTIONS.OTHER
  }

  const handleEstimatedArrivalTimeBeforeChange = (date: Date | null) => {
    const [deliveryDate, inputDate] = [new Date(order.date), new Date(date || 0)]

    const [year, month, day, hours, minutes] = [
      deliveryDate.getFullYear(),
      deliveryDate.getUTCMonth(),
      deliveryDate.getUTCDate(),
      inputDate.getHours(),
      inputDate.getMinutes(),
    ]
    const newBefore = new Date(year, month, day, hours, minutes)

    // if after is null we will set it to 2 hours before... before
    const noAfter = !estimatedArrivalTimeAfter
    if (noAfter) {
      const defaultAfter = new Date(year, month, day, hours - 2, minutes)
      setEstimatedArrivalTimeAfter(defaultAfter.toUTCString())
    } else {
      const after = new Date(estimatedArrivalTimeAfter)
      if (after >= newBefore) {
        return
      }
    }

    setEstimatedArrivalTimeBefore(newBefore.toUTCString())
  }

  const handleEstimatedArrivalTimeAfterChange = (date: Date | null) => {
    const [deliveryDate, inputDate] = [new Date(order.date), new Date(date || 0)]

    const [year, month, day, hours, minutes] = [
      deliveryDate.getFullYear(),
      deliveryDate.getUTCMonth(),
      deliveryDate.getUTCDate(),
      inputDate.getHours(),
      inputDate.getMinutes(),
    ]
    const newAfter = new Date(year, month, day, hours, minutes)

    // if before is null we will set it to 2 hours after... after
    const noBefore = !estimatedArrivalTimeBefore
    if (noBefore) {
      const defaultBefore = new Date(year, month, day, hours + 2, minutes)
      setEstimatedArrivalTimeBefore(defaultBefore.toUTCString())
    } else {
      const before = new Date(estimatedArrivalTimeBefore)
      if (before <= newAfter) {
        return
      }
    }

    setEstimatedArrivalTimeAfter(newAfter.toUTCString())
  }

  const saveEstimatedArrivalTime = async () => {
    setIsSendingEstArrivalTime(true)

    await updateOrderMutation({
      variables: {
        input: {
          id: order.id,
          estimatedArrivalTimeAfter,
          estimatedArrivalTimeBefore,
        },
      },
    })

    setIsSendingEstArrivalTime(false)
    // TODO: move the following lines to an oncompleted callback in the definition of the useMutation
    setIsSendingEstArrivalTime(false)
  }

  const handleWarehouseChange = async (data: S.DropdownProps) => {
    if (typeof data.value !== 'string') {
      logger.error(
        `Error with updateOrder in handleWarehouseChange for warehouseId: Dropdown is type ${typeof data.value} expecting string`,
      )
      return
    }

    setIsSendingWarehouse(true)

    await updateOrderMutation({
      variables: {
        input: {
          id: order.id,
          warehouseId: data.value,
        },
      },
    })

    setWarehouseId(data.value)
    setIsSendingWarehouse(false)

    const newWarehouse = allWarehouses.find((w) => w.key === data.value)
    createOrderChangeMutation({
      variables: {
        submitterId: userId,
        description: `Assign warehouse ${newWarehouse?.text || 'New Warehouse'}`,
        orderId: order.id,
      },
    })
  }

  const handleDriverChange = async (data: S.DropdownProps) => {
    if (typeof data.value !== 'string') {
      logger.error(
        `Error with updateOrder in handleDriverChange for driverId: Dropdown is type ${typeof data.value} expecting string`,
      )
      return
    }

    setIsSendingDriver(true)

    await updateOrderMutation({
      variables: {
        input: {
          id: order.id,
          driverId: data.value,
        },
      },
    })

    setDriverId(data.value)
    setIsSendingDriver(false)

    const newDriver = allDrivers.find((d) => d.key === data.value)

    createOrderChangeMutation({
      variables: {
        submitterId: userId,
        description: `Assign driver ${newDriver?.text || 'New Driver'}`,
        orderId: order.id,
      },
    })
  }

  const handleVehicleChange = async (data: S.DropdownProps) => {
    if (typeof data.value !== 'string') {
      logger.error(
        `Error with updateOrder in handleVehicleChange for vehicleId: Dropdown is type ${typeof data.value} expecting string`,
      )
      return
    }

    setIsSendingVehicle(true)

    await updateOrderMutation({
      variables: {
        input: {
          id: order.id,
          vehicleId: data.value,
        },
      },
    })

    setVehicleId(data.value)
    setIsSendingVehicle(false)
    const newVehicle = allVehicles.find((v) => v.key === data.value)

    createOrderChangeMutation({
      variables: {
        submitterId: userId,
        description: `Assign vehicle ${newVehicle?.text || 'New Vehicle'}`,
        orderId: order.id,
      },
    })
  }

  const savePayments = async () => {
    setIsSendingPaymentInfo(true)

    const gmvNum = parseFloat(parseFloat(gmvCollected).toFixed(2))
    const exciseNum = parseFloat(parseFloat(exciseTaxCollected).toFixed(2))

    try {
      await updateOrderMutation({
        variables: {
          input: {
            id: order.id,
            gmvCollected: gmvNum || 0,
            exciseTaxCollected: exciseNum || 0,
          },
        },
      })
      setIsSendingPaymentInfo(false)
    } catch (error) {
      logger.error('Error with updateOrder on AdminOrderPage: savePayments', error)
    }
  }

  const payInFull = (subtotal: number, discount: number, exciseTax: number) => {
    const discountNum = parseFloat(discount.toFixed(2))
    const exciseTaxString = exciseTax.toFixed(2)
    const gmvCollectedString = (subtotal - discountNum).toFixed(2)

    setGmvCollected(gmvCollectedString)
    setExciseTaxCollected(exciseTaxString)
  }

  const handlePaymentInfoChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const key = event.target.name
    const value = event.target.value

    if (key === 'gmvCollected') {
      setGmvCollected(value || '')
    } else if (key === 'exciseTaxCollected') {
      setExciseTaxCollected(value || '')
    }
  }

  const handlePaymentStatusChange = async (data: S.DropdownProps) => {
    const value = data.value
    if (
      value !== 'UNPAID' &&
      value !== 'COD_PAID' &&
      value !== 'NET_TERMS_PAID' &&
      value !== 'REMITTED' &&
      value !== 'SELF_COLLECTED'
    ) {
      logger.error(
        `Error with updateOrder in handlePaymentStatusChange for paymentStatus: Dropdown is type ${typeof value} expecting OrderPaymentStatusEnum`,
      )
      return
    }

    const newText = (paymentStatuses.find((s) => s.value === value) || {}).text
    const msg = (
      <P>
        Send email? <button onClick={() => handleEmailYesClick('payment')}>yes</button> /{' '}
        <button onClick={() => handleEmailNoClick('payment')}>no</button>
      </P>
    )

    setIsSendingPaymentStatus(true)

    try {
      await updateOrderMutation({
        variables: {
          input: {
            id: order.id,
            paymentStatus: value,
          },
        },
      })

      setIsSendingPaymentStatus(false)
      setStatusMessages('payment', msg)

      createOrderChangeMutation({
        variables: {
          submitterId: userId,
          description: `Set payment status to ${newText}`,
          orderId: order.id,
        },
      })
    } catch (error) {
      logger.error('Error with updateOrder on AdminOrderPage:handlePaymentStatusChange', error)
    }
  }

  const getManifestPopup = () => {
    const getBatchLinks = () => {
      return order.lineItems.reduce(
        (acc: any, { skuBatch }) => {
          const { batch, sku } = skuBatch || {}
          if (sku && batch && batch.manifestGDriveFileId) {
            acc.batchLinks.push({
              fileId: batch.manifestGDriveFileId,
              text: `${sku.code} : ${
                sku.organization.name.length > 16 ? sku.organization.name.slice(0, 13) + '...' : sku.organization.name
              }`,
            })
            const link = `https://drive.google.com/uc?export=download&id=${batch.manifestGDriveFileId}`
            acc.gdriveLinks.push(link)
          } else if (sku && batch && !batch.manifestGDriveFileId) {
            acc.batchLinks.push({
              fileId: 'none',
              text: `${sku.code} : ${
                sku.organization.name.length > 16 ? sku.organization.name.slice(0, 13) + '...' : sku.organization.name
              }`,
            })
          }
          return acc
        },
        { batchLinks: [], gdriveLinks: [] },
      )
    }

    const {
      batchLinks,
      // gdriveLinks
    } = getBatchLinks()

    // const handleDownload = () => {
    //   multiDownload(gdriveLinks)
    // }

    if (batchLinks.length < 1) {
      return getManifestLink({ col: true, ml: 0 })
    } else {
      return (
        <Popup basic on={'click'} position="top right" trigger={<S.Icon link size="big" name="folder" />}>
          <Box width="600px" justifyContent="space-around" flexWrap="wrap" row>
            {getManifestLink()}

            {batchLinks.map(({ fileId, text }, i) => {
              if (fileId === 'none') {
                return (
                  <Text my="5px" mx="8px" center key={`batchLink-${i}`}>
                    <S.Icon size="large" name="warning" color="yellow" />
                    <br />
                    <WarningBold>No COA</WarningBold>
                    <br />
                    <P>{text}</P>
                  </Text>
                )
              }
              return (
                <Text my="5px" mx="8px" center key={fileId}>
                  <ActionLink target="_blank" href={`https://drive.google.com/file/d/${fileId}`}>
                    <S.Icon size="large" name="file pdf outline" />
                  </ActionLink>
                  <P>{text}</P>
                </Text>
              )
            })}
          </Box>

          {/* TODO: Commented out because it doesn't work ALL THE WAY. Will usually miss a few COAs */}
          {/* {gdriveLinks.length > 0 && (
            <Box col center>
              <ActionLink onClick={() => handleDownload()}>
                <S.Icon size="large" name="download" />
              </ActionLink>
              <ActionLink onClick={() => handleDownload()}>Download All COAs</ActionLink>
            </Box>
          )} */}
        </Popup>
      )
    }
  }

  const getManifestLink = (opts?: any) => {
    const { col = false, ml } = opts || {}

    return (
      <Box row justifyContent="flex-end" width="100%" col={col} mb={2}>
        {isGeneratingManifest ? (
          <Box col center ml={ml !== undefined ? ml : 3} mb={col ? 2 : 0}>
            <S.Loader active inline />
          </Box>
        ) : (
          <>
            {invoicesS3FileLink ? (
              <Box col center ml={ml !== undefined ? ml : 3} mb={col ? 2 : 0}>
                <ActionLink target="_blank" href={invoicesS3FileLink}>
                  <S.Icon size="large" name="file pdf outline" />
                </ActionLink>
                <ActionLink onClick={() => generateManifest()}>Update</ActionLink>
              </Box>
            ) : (
              <Box col center ml={ml !== undefined ? ml : 3} mb={col ? 2 : 0}>
                <ActionLink onClick={() => generateManifest()}>
                  <S.Icon size="large" name="sync" />
                </ActionLink>
                <ActionLink onClick={() => generateManifest()}>Generate</ActionLink>
              </Box>
            )}

            {apSummaryGDriveFileId && (
              <Box col center ml={ml !== undefined ? ml : 3} mb={col ? 2 : 0}>
                <ActionLink target="_blank" href={`https://drive.google.com/file/d/${apSummaryGDriveFileId}`}>
                  <S.Icon size="large" name="dollar sign" />
                </ActionLink>
                <ActionLink target="_blank" href={`https://drive.google.com/file/d/${apSummaryGDriveFileId}`}>
                  AP
                </ActionLink>
              </Box>
            )}
          </>
        )}
        {order.manifestGDriveFileId && (
          <Box col center ml={ml !== undefined ? ml : 3}>
            <ActionLink target="_blank" href={`https://drive.google.com/file/d/${order.manifestGDriveFileId}`}>
              <S.Icon size="large" name="file pdf outline" />
            </ActionLink>
            <ActionLink target="_blank" href={`https://drive.google.com/file/d/${order.manifestGDriveFileId}`}>
              Legacy
            </ActionLink>
          </Box>
        )}
      </Box>
    )
  }

  const generateManifest = async () => {
    setIsGeneratingManifest(true)

    const { data, errors } = await generateManifestMutation({
      variables: {
        orderId: order.id,
      },
    })

    if (errors) {
      setIsGeneratingManifest(false)
      return alert(`Error:\n${errors.map((err) => ` - ${err.message}\n`)}`)
    }

    const _apSummaryGDriveFileId = data?.generateManifest?.apSummaryGDriveFileId
    const _invoicesS3FileLink = data?.generateManifest?.invoicesS3FileLink

    setFileLinks({
      apSummaryGDriveFileId: _apSummaryGDriveFileId,
      invoicesS3FileLink: _invoicesS3FileLink,
    })
    setIsGeneratingManifest(false)
  }

  const alias = router.query.alias

  const goToOrderChangeLog = () => {
    router.push(`/${alias}/app/orders/${order.id}/changelog`)
  }

  const openRelatedDeliveries = () => {
    window.open(
      `/${alias}/app/admin-outstanding-invoices/${order.site.id}/future-dropoffs?number=${order.number}`,
      '_blank',
    )
  }

  const goToOrder = () => {
    return router.push(`/${alias}/app/orders/${order.id}`)
  }

  const { lineItems, discount, site, date: deliveryDate, surcharge } = order
  const { subtotal, totalExciseTax } = calculateMoney({
    lineItems,
    discount,
    siteUsState: site.state,
    deliveryDate,
    surcharge,
  })

  return (
    <S.Table.Row data-test="admin-orders-row">
      <S.Table.Cell>
        <S.Checkbox
          checked={!!isSelected}
          onChange={(_e, d) => {
            onClickCheckbox(!isSelected)
          }}
        />
        <b style={{ position: 'relative', top: -3, left: 10 }}>{order.number}</b>
        <S.Divider />
        <div>
          <CellOptionLabel>Created At:</CellOptionLabel>
          <CellOptionLabel>{`${moment(order.createdAt).format('MM/DD/YYYY h:mmA')}`}</CellOptionLabel>
        </div>
        <br />
      </S.Table.Cell>
      <S.Table.Cell>{parseDate(order.date)}</S.Table.Cell>
      <S.Table.Cell>
        <h3>{order.name}</h3>

        <S.Divider />
        <P mb={0}>
          {order.organization.name}{' '}
          {order.organization.owner && order.organization.owner.email ? `(${order.organization.owner.email})` : ''}
        </P>
        {order.creator?.id && (
          <CellOptionLabel style={{ lineHeight: 1.25 }}>
            <S.Divider />
            Submitted by: {order?.creator?.firstName} {order?.creator?.lastName} ({order?.creator?.email})
          </CellOptionLabel>
        )}
        <AdminOrderNotes
          order={order}
          resumeStatusChange={resumeStatusChange}
          ref={(element) => {
            adminHovers = element ? { bTag: element.adminHover, child: element } : null
          }}
          updateOrder={updateOrderWithNotes}
        />
      </S.Table.Cell>
      <S.Table.Cell>
        {order.site &&
          `
                ${order.site.name} (${order.site.address1} ${order.site.address2 || ''},
                ${order.site.city}, ${order.site.state} ${order.site.zip})
              `}
      </S.Table.Cell>
      <S.Table.Cell width="1" textAlign="center">
        <div>
          <ActionCookie action={order.action} />
          <br />
          {titleize(order.action)}
        </div>
        <CellOptionLabel>({titleize(order.timeWindow || '')})</CellOptionLabel>
      </S.Table.Cell>
      <S.Table.Cell>
        {order.intaken && <S.Label color="pink">Intaken</S.Label>}
        {order.outtaken && <S.Label color="teal">Outtaken</S.Label>}
        {order.edited && <S.Label color="red">Edited</S.Label>}
        {!order.intaken && (requiresIntakeAndOuttake(order.action) || requiresIntakeOnly(order.action)) && (
          <S.Label color="yellow">Not Intaken</S.Label>
        )}
        {!order.outtaken && (requiresIntakeAndOuttake(order.action) || requiresOuttakeOnly(order.action)) && (
          <S.Label color="yellow">Not Outtaken</S.Label>
        )}
        {(order.intaken || order.outtaken || !order.outtaken || !order.intaken || order.edited) && <S.Divider />}
        <Dropdown
          search
          text={isSendingOrderStatus ? '... sending ...' : titleize(order.status)}
          value={titleize(order.status)}
          // required
          placeholder="Select status..."
          onChange={checkStatusAdminNotes}
          selection
          options={statuses}
        />
        {orderStatusMessage}
        <S.Divider />
        <div>
          <CellOptionLabel>Estimated Arrival Time:</CellOptionLabel>
          <AfterBeforeContainer>
            <div>
              <StyledDatePicker
                name="estimatedArrivalTimeAfter"
                selected={estimatedArrivalTimeAfter ? moment(estimatedArrivalTimeAfter).toDate() : null}
                onChange={(e, _data) => handleEstimatedArrivalTimeAfterChange(e)}
                showTimeSelect
                showTimeSelectOnly
                timeIntervals={30}
                dateFormat="HH:mm"
                placeholderText="After"
                timeCaption="After"
              />
            </div>
            <div>
              <StyledDatePicker
                name="estimatedArrivalTimeBefore"
                selected={estimatedArrivalTimeBefore ? moment(estimatedArrivalTimeBefore).toDate() : null}
                onChange={(e, _data) => handleEstimatedArrivalTimeBeforeChange(e)}
                showTimeSelect
                showTimeSelectOnly
                timeIntervals={30}
                dateFormat="HH:mm"
                placeholderText="Before"
                timeCaption="Before"
              />
            </div>
            <Button
              style={{ margin: '6px 0' }}
              positive
              onClick={saveEstimatedArrivalTime}
              size="mini"
              loading={isSendingEstArrivalTime}
            >
              Save
            </Button>
          </AfterBeforeContainer>
        </div>
      </S.Table.Cell>
      <S.Table.Cell>
        <CellOption>
          <div>
            <CellOptionLabel htmlFor="source">Source Warehouse:</CellOptionLabel>
          </div>
          <div>
            <Dropdown
              text={isSendingWarehouse ? '... sending ...' : allWarehouses.find((w) => w.key === warehouseId)?.text}
              value={warehouseId}
              placeholder="Select warehouse..."
              selection
              onChange={(_e, data) => handleWarehouseChange(data)}
              options={allWarehouses}
            />
          </div>
        </CellOption>
        <CellOption>
          <div>
            <CellOptionLabel htmlFor="driver">Driver:</CellOptionLabel>
          </div>
          <Dropdown
            search
            text={isSendingDriver ? '... sending ...' : allDrivers.find((d) => d.key === driverId)?.text}
            value={driverId}
            placeholder="Select driver..."
            selection
            onChange={(_e, data) => handleDriverChange(data)}
            options={allDrivers.sort((a, b) => (a.text <= b.text ? -1 : 1))}
          />
        </CellOption>
        <CellOption>
          <div>
            <CellOptionLabel htmlFor="vehicle">Vehicle:</CellOptionLabel>
          </div>
          <Dropdown
            search
            text={isSendingVehicle ? '... sending ...' : allVehicles.find((v) => v.key === vehicleId)?.text || ''}
            value={vehicleId}
            placeholder="Select vehicle..."
            selection
            onChange={(_e, data) => handleVehicleChange(data)}
            options={allVehicles.sort((a, b) => (a.text <= b.text ? -1 : 1))}
          />
        </CellOption>
      </S.Table.Cell>
      <S.Table.Cell>
        <CellOption>
          <CellOptionLabel>Payment Terms: {titleizeDaysTillPaymentDue(order.daysTillPaymentDue || 0)}</CellOptionLabel>
        </CellOption>
        <S.Divider />
        <CellOption>
          <CellOptionLabel>
            <a href="#/" onClick={() => payInFull(subtotal, order.discount, totalExciseTax)}>
              Paid in Full?
            </a>
          </CellOptionLabel>
          <div>
            <CellOptionLabel htmlFor="gmvCollected">
              GMV Collected:
              <br />
              <SubLabelWrapper>(Subtotal - Discount = {dollarFormat(subtotal - order.discount)})</SubLabelWrapper>
            </CellOptionLabel>
          </div>
          <Input
            autoComplete="gmvCollected"
            name="gmvCollected"
            type="number"
            value={gmvCollected}
            min="0.00"
            step="0.01"
            icon
            iconPosition="left"
            placeholder="100"
            onChange={(e) => handlePaymentInfoChange(e)}
            size="mini"
            disabled={isSendingPaymentInfo}
          >
            <S.Icon name="dollar" />
            <input />
          </Input>
        </CellOption>
        <CellOption>
          <div>
            <CellOptionLabel htmlFor="exciseTaxCollected">
              Excise Tax Collected&nbsp;
              <SubLabelWrapper>({dollarFormat(totalExciseTax.toFixed(2))}):</SubLabelWrapper>
            </CellOptionLabel>
          </div>
          <Input
            autoComplete="exciseTaxCollected"
            name="exciseTaxCollected"
            type="number"
            value={exciseTaxCollected}
            min="0.00"
            step="0.01"
            icon
            iconPosition="left"
            placeholder="100"
            onChange={(e) => handlePaymentInfoChange(e)}
            size="mini"
            disabled={isSendingPaymentInfo}
          >
            <S.Icon name="dollar" />
            <input />
          </Input>
        </CellOption>
        <Button style={{ margin: '6px 0' }} positive onClick={savePayments} size="mini" loading={isSendingPaymentInfo}>
          Save
        </Button>
        <S.Divider />
        <CellOption>
          <div>
            <CellOptionLabel htmlFor="paymentStatus">Payment Status:</CellOptionLabel>
          </div>
          <Dropdown
            search
            text={isSendingPaymentStatus ? '... sending ...' : titleize(order.paymentStatus)}
            value={titleize(order.paymentStatus)}
            placeholder="Select payment status..."
            onChange={(_e, data) => handlePaymentStatusChange(data)}
            selection
            options={paymentStatuses}
            disabled={isSendingPaymentStatus || isSendingPaymentInfo}
          />
          {paymentStatusMessage}
        </CellOption>
      </S.Table.Cell>
      <S.Table.Cell textAlign="center">{getManifestPopup()}</S.Table.Cell>
      <S.Table.Cell data-test="admin-orders-row-actions">
        <Button color="yellow" size="tiny" icon="edit" onClick={() => goToOrder()} label="Edit" />
        <CopyOrder order={order} />
        <Button
          color="purple"
          size="tiny"
          icon="clipboard list"
          label="Changelog"
          onClick={() => goToOrderChangeLog()}
        />
        <Button color="pink" size="tiny" icon="truck" label="Related AR" onClick={() => openRelatedDeliveries()} />
        <IntakeOuttakeModalButtons order={order} />
      </S.Table.Cell>
    </S.Table.Row>
  )
}

export default AdminOrdersRow
