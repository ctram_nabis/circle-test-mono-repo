/*
 * AdminOrdersPage
 */
import {
  AllAdminOrdersPageDocument,
  CreateOrderChangeDocument,
  GenerateManifestDocument,
  SendSendGridStatusUpdateEmailDocument,
  UpdateOrderDocument,
  UploadToOnfleetDocument,
} from 'generated/graphql'
import { graphql, withApollo } from 'react-apollo'
import 'react-datepicker/dist/react-datepicker.css'
import { withRouter } from 'react-router-dom'
import { compose } from 'recompose'
import { AdminOrdersPage } from './AdminOrdersPage'
import getVarsFromURL from './getVarsFromURL'

export default compose(
  withRouter,
  graphql(AllAdminOrdersPageDocument, {
    options: (props: any) => {
      const { location } = props.history

      return {
        skip: !props.user?.isAdmin && !props.organization?.id,
        fetchPolicy: 'network-only',
        variables: (() => getVarsFromURL(location))(),
      }
    },
  }),
  graphql(GenerateManifestDocument, {
    props: ({ mutate }) => ({
      generateManifest: (orderId) => {
        return mutate({
          variables: {
            orderId,
          },
          refetchQueries: ['AllAdminOrders'],
        })
      },
    }),
  } as any),
  graphql(UpdateOrderDocument, {
    props: ({ mutate }) => ({
      editOrder: (input) => {
        return mutate({
          variables: {
            input,
          },
          refetchQueries: ['AllAdminOrders'],
        })
      },
    }),
  } as any),
  graphql(SendSendGridStatusUpdateEmailDocument, {
    props: ({ mutate }) => ({
      sendSendGridStatusUpdateEmail: (input) => {
        return mutate({
          variables: {
            input,
          },
        })
      },
    }),
  } as any),
  graphql(UploadToOnfleetDocument, {
    props: ({ mutate }) => ({
      uploadToOnfleet: (input) => {
        return mutate({
          variables: {
            input,
          },
        })
      },
    }),
  } as any),
  graphql(CreateOrderChangeDocument, {
    props: ({ mutate }) => ({
      createOrderChange: (variables) => {
        return mutate({ variables })
      },
    }),
  } as any),
  withApollo,
)(AdminOrdersPage)
