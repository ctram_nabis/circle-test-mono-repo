/*
 * AdminOrdersPage
 */
import moment from 'moment-timezone'
import React, { useState } from 'react'
import styled from 'styled-components'

import cloneDeep from 'lodash/cloneDeep'
import debounce from 'lodash/debounce'
import queryString from 'query-string'
import 'react-datepicker/dist/react-datepicker.css'
import getVarsFromURL from './getVarsFromURL'

import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { CSVExportButton } from 'components/CSVExportButton'
import { Dropdown } from 'components/Dropdown'
import { PageHeader } from 'components/PageHeader'
import { Pagination } from 'components/Pagination'
import { S } from 'components/SemanticComponents'
import { SearchByDate } from '../../components/SearchByDate'
import AdminOrdersRow from './AdminOrdersRow'

import { AllAdminOrdersDocument } from 'generated/graphql'

import { usePageInfo } from 'hooks'
import logger from 'modules/Logger'
import { useRouter } from 'modules/Navigation'
import { useMe } from 'modules/User'
import { useEffect } from 'react'
import { useSetState } from 'react-use'

export const ActionLink = styled.a`
  cursor: pointer;
  text-decoration: underline;
`

export const CellOptionLabel = styled.label`
  border: none;
  font-family: Lato, 'Helvetica Neue', Arial, Helvetica, sans-serif;
  font-weight: 700;
  color: rgba(0, 0, 0, 0.87);
  display: block;
  font-size: 0.67em;
`

const Wrapper = styled(Box)`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  width: 100%;
  margin-bottom: 12px;
`

const actions = [
  // Visible to brands
  {
    key: 'DELIVERY_TO_RETAILER',
    value: 'DELIVERY_TO_RETAILER',
    text: 'Delivery To Retailer',
  },
  {
    key: 'PICKUP_FROM_BRAND',
    value: 'PICKUP_FROM_BRAND',
    text: 'Pick Up From Brand',
  },
  {
    key: 'RETURN_FROM_RETAILER',
    value: 'RETURN_FROM_RETAILER',
    text: 'Return From Retailer',
  },
  {
    key: 'PICKUP_FROM_NABIS',
    value: 'PICKUP_FROM_NABIS',
    text: 'Brand Picks Up From Nabis',
  },
  {
    key: 'DROPOFF_TO_NABIS',
    text: 'Brand Drops Off To Nabis',
    value: 'DROPOFF_TO_NABIS',
  },
  {
    key: 'DELIVERY_TO_BRAND',
    value: 'DELIVERY_TO_BRAND',
    text: 'Delivery To Brand',
  },
  {
    key: 'INTERNAL_TRANSFER',
    value: 'INTERNAL_TRANSFER',
    text: 'Internal Warehouse Transfer',
  },
  // For internal use only
  {
    key: 'PAYMENT_PICKUP_FROM_RETAILER',
    value: 'PAYMENT_PICKUP_FROM_RETAILER',
    text: 'Payment Pickup From Retailer',
  },
  { key: 'OTHER', value: 'OTHER', text: 'Other' },
]

const statuses = [
  // { key: 'DRAFT', value: 'DRAFT', text: 'Draft' },
  { key: 'UNSCHEDULED', value: 'UNSCHEDULED', text: 'Unscheduled' },
  { key: 'SCHEDULED', value: 'SCHEDULED', text: 'Scheduled' },
  { key: 'TRANSFERRING', value: 'TRANSFERRING', text: 'Transferring' },
  { key: 'DELIVERED', value: 'DELIVERED', text: 'Delivered' },
  {
    key: 'DELIVERED_WITH_EDITS',
    value: 'DELIVERED_WITH_EDITS',
    text: 'Delivered with Edits',
  },
  { key: 'DELAYED', value: 'DELAYED', text: 'Delayed' },
  { key: 'REJECTED', value: 'REJECTED', text: 'Rejected' },
  { key: 'CANCELLED', value: 'CANCELLED', text: 'Cancelled' },
]

const paymentStatuses = [
  { key: 'UNPAID', value: 'UNPAID', text: 'Unpaid' },
  { key: 'COD_PAID', value: 'COD_PAID', text: 'COD, Paid' },
  { key: 'NET_TERMS_PAID', value: 'NET_TERMS_PAID', text: 'Net Terms, Paid' },
  { key: 'REMITTED', value: 'REMITTED', text: 'Remitted' },
  { key: 'SELF_COLLECTED', value: 'SELF_COLLECTED', text: 'Self Collected' },
]

const intakeOuttakeEditedStatuses = [
  { key: 'INTAKE', value: 'INTAKE', text: 'Intaken' },
  { key: 'OUTTAKE', value: 'OUTTAKE', text: 'Outtaken' },
  { key: 'EDITED', value: 'EDITED', text: 'Edited' },
  { key: 'NOT_INTAKE', value: 'NOT_INTAKE', text: 'Not Intaken' },
  { key: 'NOT_OUTTAKE', value: 'NOT_OUTTAKE', text: 'Not Outtaken' },
  { key: 'NOT_EDITED', value: 'NOT_EDITED', text: 'Not Edited' },
]

// interface Props extends AllAdminOrdersPageQueryResult {
// }
//
export function AdminOrdersPage(props: any) {
  const { data, client, uploadToOnfleet } = props

  const router = useRouter()
  const { user } = useMe()

  const qs: any = queryString.parse(router.location.search || '')

  const warehouseId = (qs.warehouseId && qs.warehouseId[0]) || ''
  const driverId = (qs.driverId && qs.driverId[0]) || ''
  const vehicleId = (qs.vehicleId && qs.vehicleId[0]) || ''
  const transactionTypes = (qs.transactionTypes && qs.transactionTypes[0]) || ''

  const [isUploadingToOnfleet, setIsUploadingToOnfleet] = useState(false)
  const [shouldShowUploadedMsg, setShouldShowUploadedMsg] = useState(false)
  const [selectedOrders, setSelectedOrders] = useSetState({})

  const [filters, setFilters] = useState({
    search: qs.search || '',
    start: qs.start || null,
    end: qs.end || null,
    action: qs.action || null,
    status: qs.status || null,
    paymentStatus: qs.paymentStatus || null,
    warehouseId: qs.warehouseId && qs.warehouseId.length ? decodeURIComponent(warehouseId) : undefined,
    driverId: qs.driverId && qs.driverId.length ? decodeURIComponent(driverId) : undefined,
    vehicleId: qs.vehicleId && qs.vehicleId.length ? decodeURIComponent(vehicleId) : undefined,
    transactionTypes: qs.transactionTypes && qs.transactionTypes.length ? decodeURIComponent(transactionTypes) : [],
    // Pagination Info
    totalNumPages: 0,
  })

  const { pageInfo, setPage } = usePageInfo({
    numItemsPerPage: 25,
    orderBy: [
      {
        attribute: 'createdAt',
        order: 'DESC',
      },
    ],
    page: qs.page || 1,
  })

  const refetchOrders = () => {
    const f = filters

    const cleanQueryString = (() => {
      const _qs: any = {}
      if (f.search) {
        _qs.search = f.search
      }
      if (f.start) {
        _qs.start = f.start
      }
      if (f.end) {
        _qs.end = f.end
      }
      if (f.action) {
        _qs.action = f.action
      }
      if (f.status) {
        _qs.status = f.status
      }
      if (f.paymentStatus) {
        _qs.paymentStatus = f.paymentStatus
      }
      if (f.warehouseId) {
        _qs.warehouseId = f.warehouseId
      }
      if (f.driverId) {
        _qs.driverId = f.driverId
      }
      if (f.vehicleId) {
        _qs.vehicleId = f.vehicleId
      }
      if (pageInfo.page) {
        _qs.page = pageInfo.page
      }
      if (f.transactionTypes) {
        _qs.transactionTypes = f.transactionTypes
      }
      return _qs
    })()

    const newQueryString = queryString.stringify(cleanQueryString, {
      arrayFormat: 'comma',
    })

    router.push(`/${router.query.alias}/app/admin-orders${newQueryString ? '?' : ''}${newQueryString}`)
    if (data) {
      data.refetch()
    }
  }

  const debounceRefetchOrders = React.useCallback(debounce(refetchOrders, 1500), [filters, pageInfo])

  useEffect(() => {
    debounceRefetchOrders()
  }, [debounceRefetchOrders])

  const fetchAdminOrders = async () => {
    const res = await client.query({
      query: AllAdminOrdersDocument,
      variables: getVarsFromURL(router.location),
    })

    const raw = res?.data?.viewer?.allAdminOrders?.results || []

    return raw.map((order) => {
      // const titleholdersSet = new Set()

      // order.lineItems.forEach((lineItem) => {
      //   const titleholderName = lineItem.skuBatch.titleholder.titleholderLicense
      //     ? lineItem.skuBatch.titleholder.titleholderLicense.legalEntityName
      //     : lineItem.skuBatch.titleholder.titleholderOrganization.name

      //   titleholdersSet.add(titleholderName)
      // })

      // const titleholdersStrings = Array.from(titleholdersSet).join(' - ')

      const Manifests = order.invoicesS3FileLink
        ? order.invoicesS3FileLink
        : order.manifestGDriveFileId
        ? `https://drive.google.com/file/d/${order.manifestGDriveFileId}`
        : ''

      return {
        'Delivery Date': (order.date && order.date.split('T')[0]) || '',
        'Missed Order': '',
        'PICKUP or DROP-OFF?': order.action || '',
        'Order Number': order.number || '',
        Brand: order.organization.name || '',
        Retailer: order.site.name || '',
        City: order.site.city || '',
        'Contact Name': order.site.pocName || '',
        'Phone Number': order.site.pocPhoneNumber || '',
        Email: order.site.pocEmail || '',
        'Outstanding Invoices to Pickup? (Brand Order #)': '',
        Confirmation: '',
        'Phone Call': '',
        'Email Sent?': '',
        'Printed COAs?': '',
        'OnFleet Task?': '',
        'Order Packed?': '',
        'Inventory Initials': '',
        Notes: order.notes || '',
        _: '',
        Manifests,
        'Excise Tax': order.exciseTax || '0',
        // Titleholders: titleholdersStrings,
      }
    })
  }

  const renderPaginationComponent = () => {
    const totalNumPages = data.viewer?.allAdminOrders?.pageInfo?.totalNumPages

    if (!totalNumPages) return null

    return (
      <S.Table.Row>
        <S.Table.HeaderCell>
          <CSVExportButton data={[]} fetchFn={fetchAdminOrders} />
        </S.Table.HeaderCell>
        <S.Table.HeaderCell colSpan={13}>
          <Pagination
            onPageChange={(e, data) => handlePageChange(e as Event, data as object)}
            activePage={pageInfo.page}
            totalNumPages={totalNumPages}
            siblingRange={3}
          />
        </S.Table.HeaderCell>
      </S.Table.Row>
    )
  }

  const handlePageChange = (e: Event, data: any) => {
    setPage(data.activePage)
  }

  const handleFilterChange = (filter, value) => {
    const newFilters = cloneDeep(filters)
    newFilters[filter] = value
    setPage(1)
    setFilters(newFilters)
  }

  const toggleOrderRowCheckbox = (_orderId: string, isSelected: boolean) => {
    if (!isSelected) {
      delete selectedOrders[_orderId]
    } else {
      selectedOrders[_orderId] = true
    }

    setSelectedOrders(selectedOrders)
  }

  const handleClearSelected = () => {
    for (const id in selectedOrders) {
      if (selectedOrders[id]) {
        delete selectedOrders[id]
      }
    }

    setSelectedOrders(selectedOrders)
  }

  const handleOnfleetClick = () => {
    setIsUploadingToOnfleet(true)

    uploadToOnfleet({ orders: Object.keys(selectedOrders) })
      .then((res) => {
        logger.info('=== ONFLEET uploading tasks', { res })
        if (res.data?.uploadToOnfleet?.ok) {
          setShouldShowUploadedMsg(true)

          setIsUploadingToOnfleet(false)

          setTimeout(() => {
            setShouldShowUploadedMsg(false)
          }, 1300)
        } else {
          logger.info('=== ONFLEET errors', { errors: res.data?.uploadToOnfleet?.errors })
          // TODO: Need to handle errors better than this. Quick and dirty Alert for Ops
          setIsUploadingToOnfleet(false)

          const formattedRes = res.data?.uploadToOnfleet?.errors.map((resObj) => {
            const orderNumberAndRecipientName = resObj.task?.recipients[0]?.name
            const errorMessage = resObj.error?.message
            logger.info({ orderNumberAndRecipientName, errorMessage })

            return { orderNumberAndRecipientName, errorMessage }
          })

          alert(
            `Error: Uploading onFleet tasks was unsuccessful for these orders:\n${formattedRes.map(
              (resObj) => ` - ${resObj.orderNumberAndRecipientName}: ${resObj.errorMessage}\n`,
            )}\n** Orders NOT mentioned above ARE successfully uploaded. 👌 Just retry the ones in this list! **`,
          )
        }
      })
      .catch((error) => {
        logger.info('=== ONFLEET caught error', { error })

        setIsUploadingToOnfleet(false)

        alert(`${error.message}`)
      })
  }

  const ordersToShow = data?.viewer?.allAdminOrders?.results || []
  const dateFmtMoment = 'MM-DD-YYYY'
  const allWarehouses = data?.viewer?.allWarehouses || []
  const allDrivers = data?.viewer?.allDrivers || []
  const allVehicles = data?.viewer?.allVehicles || []

  const allWarehousesSourceOptions = allWarehouses.map((w) => ({
    key: w.id,
    value: w.id,
    text: w.site.name,
    disabled: w.site.name === 'Superior Herbal Health',
  }))
  const allWarehousesParsed = allWarehouses.map((w) => ({
    key: w.id,
    value: w.id,
    text: w.site.name,
  }))
  const allDriversParsed = allDrivers.map((w) => ({
    key: w.id,
    value: w.id,
    text: `${w.firstName} ${w.lastName}`,
  }))
  const allVehiclesParsed = allVehicles.map((w) => ({
    key: w.id,
    value: w.id,
    text: w.name,
  }))

  const numSelected = Object.keys(selectedOrders).length

  return (
    <Wrapper data-test="admin-orders-page">
      <S.Container fluid>
        <S.Grid columns={1} padded>
          <S.GridRow>
            <S.GridColumn>
              <S.GridColumn>
                <PageHeader> Admin - All Orders </PageHeader>
              </S.GridColumn>
            </S.GridColumn>
          </S.GridRow>
          <S.Grid columns={4}>
            <S.GridColumn>
              <S.Search
                placeholder="Order number"
                open={false}
                onSearchChange={(e: any) => handleFilterChange('search', e.target.value)}
                value={filters.search}
              />
            </S.GridColumn>
            <S.GridColumn>
              <Button
                disabled={isUploadingToOnfleet || shouldShowUploadedMsg || numSelected === 0}
                onClick={handleOnfleetClick}
              >
                {isUploadingToOnfleet
                  ? '... uploading ...'
                  : shouldShowUploadedMsg
                  ? 'Successfully uploaded!'
                  : `Upload ${numSelected} task${numSelected === 1 ? '' : 's'} to Onfleet`}
              </Button>
            </S.GridColumn>
            <S.GridColumn>
              <Button onClick={handleClearSelected} color="grey" disabled={Object.keys(selectedOrders).length === 0}>
                Deselect Orders
              </Button>
            </S.GridColumn>
          </S.Grid>
          <S.GridRow>
            <S.GridColumn>
              <S.Table sortable celled selectable unstackable verticalAlign="middle" color="green">
                <S.Table.Header>
                  {renderPaginationComponent()}
                  <S.Table.Row>
                    <S.Table.HeaderCell>
                      <S.Label ribbon color="green">
                        Order #
                      </S.Label>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      Date
                      <Box width={75}>
                        <SearchByDate
                          name="start"
                          select={filters.start && moment(filters.start).toDate()}
                          filter={(_, date: any) => handleFilterChange('start', moment(date).format(dateFmtMoment))}
                          isClearable={false}
                        />
                      </Box>
                      <a href="#/" onClick={() => handleFilterChange('start', null)}>
                        clear
                      </a>
                      <Box width={75}>
                        <SearchByDate
                          name="end"
                          select={filters.end && moment(filters.end).toDate()}
                          filter={(_, date: any) => handleFilterChange('end', moment(date).format(dateFmtMoment))}
                          isClearable={false}
                        />
                      </Box>
                      <a href="#/" onClick={() => handleFilterChange('end', null)}>
                        clear
                      </a>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={225}>Order Details</Box>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={115}>Location</Box>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={115}>
                        <Dropdown
                          search
                          fluid
                          placeholder="Action"
                          selection
                          options={actions}
                          value={filters.action}
                          onChange={(_e, d) => handleFilterChange('action', d.value)}
                        />
                        <div>
                          <a href="#/" onClick={() => handleFilterChange('action', null)}>
                            clear
                          </a>
                        </div>
                      </Box>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Dropdown
                        search
                        placeholder="Delivery Status"
                        selection
                        options={statuses}
                        value={filters.status}
                        onChange={(_e, d) => handleFilterChange('status', d.value)}
                      />
                      <div>
                        <a href="#/" onClick={() => handleFilterChange('status', null)}>
                          clear
                        </a>
                      </div>
                      <Dropdown
                        search
                        placeholder="Intake/Outtake/Edited"
                        multiple
                        fluid
                        selection
                        options={intakeOuttakeEditedStatuses}
                        value={
                          // the initial state of transactionTypes is a string
                          // this will process them if necessary. otherwise it'll
                          // just return the array of transactionType filters
                          typeof filters.transactionTypes === 'string'
                            ? filters.transactionTypes.split(',')
                            : filters.transactionTypes
                        }
                        onChange={(_e, d) => handleFilterChange('transactionTypes', d.value)}
                      />
                      <a href="#/" onClick={() => handleFilterChange('transactionTypes', [])}>
                        clear
                      </a>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <div>Delivery Options</div>
                      <br />
                      <Dropdown
                        placeholder="Filter by Warehouse"
                        fluid
                        selection
                        options={[
                          {
                            key: 0,
                            value: 'UN_ASSIGNED',
                            text: 'Un-Assigned',
                          },
                        ].concat(allWarehousesSourceOptions)}
                        value={filters.warehouseId}
                        onChange={(_e, d) => handleFilterChange('warehouseId', d.value)}
                      />
                      <a href="#/" onClick={() => handleFilterChange('warehouseId', null)}>
                        clear
                      </a>

                      <Dropdown
                        search
                        placeholder="Filter by Driver"
                        fluid
                        selection
                        options={[
                          {
                            key: 0,
                            value: 'UN_ASSIGNED',
                            text: 'Un-Assigned',
                          },
                        ].concat(allDriversParsed)}
                        value={filters.driverId}
                        onChange={(_e, d) => handleFilterChange('driverId', d.value)}
                      />
                      <a href="#/" onClick={() => handleFilterChange('driverId', null)}>
                        clear
                      </a>
                      <Dropdown
                        search
                        placeholder="Filter by Vehicle"
                        fluid
                        selection
                        options={[
                          {
                            key: 0,
                            value: 'UN_ASSIGNED',
                            text: 'Un-Assigned',
                          },
                        ].concat(allVehiclesParsed)}
                        value={filters.vehicleId}
                        onChange={(_e, d) => handleFilterChange('vehicleId', d.value)}
                      />
                      <a href="#/" onClick={() => handleFilterChange('vehicleId', null)}>
                        clear
                      </a>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Dropdown
                        search
                        placeholder="Payment Status"
                        fluid
                        selection
                        options={paymentStatuses}
                        value={filters.paymentStatus}
                        onChange={(_e, d) => handleFilterChange('paymentStatus', d.value)}
                      />
                      <a href="#/" onClick={() => handleFilterChange('paymentStatus', null)}>
                        clear
                      </a>
                    </S.Table.HeaderCell>
                    <S.Table.HeaderCell>Manifest</S.Table.HeaderCell>
                    <S.Table.HeaderCell>
                      <Box width={155}>Actions</Box>
                    </S.Table.HeaderCell>
                  </S.Table.Row>
                </S.Table.Header>
                <S.Table.Body>
                  {data?.loading ? (
                    <S.Loader active size="large" content="Loading Orders" style={{ marginTop: 200 }} />
                  ) : (
                    ordersToShow.map((order) => {
                      return (
                        <AdminOrdersRow
                          key={order.id}
                          order={order}
                          allWarehouses={allWarehousesParsed}
                          userId={user.id}
                          allDrivers={allDriversParsed}
                          allVehicles={allVehiclesParsed}
                          onClickCheckbox={(_isSelected) => toggleOrderRowCheckbox(order.id, _isSelected)}
                          isSelected={selectedOrders[order.id]}
                        />
                      )
                    })
                  )}
                </S.Table.Body>
                <S.Table.Footer>{renderPaginationComponent()}</S.Table.Footer>
              </S.Table>
            </S.GridColumn>
          </S.GridRow>
        </S.Grid>
      </S.Container>
    </Wrapper>
  )
}
