import {
  CreateLineItemDocument,
  CreateOrderDocument,
  CreateSiteDocument,
  DeleteDraftDocument,
  DeleteLineItemDocument,
  GenerateManifestDocument,
  GetOrderDocument,
  UpdateLineItemDocument,
  UpdateOrderDocument,
  UpdateSiteDocument,
} from 'generated/graphql'
import { graphql } from 'react-apollo'
import { compose } from 'recompose'

export default (module) =>
  compose(
    graphql(CreateOrderDocument, {
      props: ({ mutate }) =>
        ({
          createOrder: (input) => {
            if (mutate) {
              mutate({
                variables: {
                  input,
                },
              })
            }
          },
        } as any),
    }),
    graphql(DeleteDraftDocument, {
      props: ({ mutate }) =>
        ({
          deleteDraft: (input) => {
            if (mutate) {
              mutate({ variables: { input } })
            }
          },
        } as any),
    }),
    graphql(CreateSiteDocument, {
      props: ({ mutate }) => ({
        createSite: (input) => mutate({ variables: { input } }),
      }),
    } as any),
    graphql(UpdateOrderDocument, {
      props: ({ mutate }) => ({
        editOrder: (input) => mutate({ variables: { input } }),
      }),
    } as any),
    graphql(UpdateSiteDocument, {
      props: ({ mutate }) => ({
        editSite: (input) => mutate({ variables: { input } }),
      }),
    } as any),
    graphql(CreateLineItemDocument, {
      props: ({ mutate }) => ({
        createLineItem: (input) => mutate({ variables: { input } }),
      }),
    } as any),
    graphql(UpdateLineItemDocument, {
      props: ({ mutate }) => ({
        editLineItem: (input) => mutate({ variables: { input } }),
      }),
    } as any),
    graphql(DeleteLineItemDocument, {
      props: ({ mutate }) => ({
        deleteLineItem: (input) => mutate({ variables: { input } }),
      }),
    } as any),
    graphql(GenerateManifestDocument, {
      props: ({ mutate }) =>
        ({
          generateManifest: (input) => {
            if (mutate) {
              mutate({ variables: { input } })
            }
          },
        } as any),
      options: () => ({
        refetchQueries: [GetOrderDocument],
      }),
    }),
  )(module)
