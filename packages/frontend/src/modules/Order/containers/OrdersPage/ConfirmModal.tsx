import { Button, P, S } from 'components'
import React from 'react'

interface Props {
  onClose?: () => void
  onOpen?: () => void
  onSuccess?: () => void
  open?: boolean
  trigger?: JSX.Element
}

export const ConfirmModal = (props: Props) => {
  const { onClose, onSuccess, onOpen, open = true, trigger } = props

  return (
    <S.Modal size="tiny" closeIcon open={open} onOpen={onOpen} onClose={onClose} trigger={trigger}>
      <S.Modal.Content>
        <P>
          Please, confirm that you are ready to submit your order. Once you create the order, you will not be able to
          make any changes unless you contact us! If you are not ready to submit this order, save it as a draft using
          Save Draft on the bottom left.
        </P>
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button onClick={onClose} title="Continue Editing" />
        <Button title="Confirm" color="green" name="check" onClick={onSuccess} />
      </S.Modal.Actions>
    </S.Modal>
  )
}
