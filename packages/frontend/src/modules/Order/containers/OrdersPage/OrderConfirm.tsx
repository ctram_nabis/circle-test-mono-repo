import { ErrorBox } from 'components'
import { CreateOrderDocument, Organization } from 'generated/graphql'
import idx from 'idx'
import { get, omit, pick } from 'lodash'
import { client } from 'modules/Apollo'
import logger from 'modules/Logger'
import { withOrganizationHook } from 'modules/Order/helpers/withOrganizationHook'
import { withMeHook } from 'modules/User/helpers/withMeHook'
import React from 'react'
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from 'react-google-maps'
import { withRouter } from 'react-router'
import { compose } from 'recompose'
import { Button, Card, Container, Divider, Form, Grid, Header, Icon, Label, Loader } from 'semantic-ui-react'
import styled from 'styled-components'
import { dollarFormat, formatDate, isEmpty, titleize, titleizeDaysTillPaymentDue } from 'utils'
import SendOrderStatusUpdateEmailModal from '../SendOrderStatusUpdateEmailModal'
import { NEW_ORDER_STEPS } from './AddEditOrder'
import OrderConfirmApollo from './OrderConfirmApollo'
import { MustPayLabel } from 'modules/Order/components'

const SubmitContainer = styled.div`
  flex: 0 0 50px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 20px 0px;
`

const OrderMapComponent = withScriptjs(
  withGoogleMap((props: any) => (
    <GoogleMap defaultZoom={10} defaultCenter={props.coords} center={props.coords}>
      {props.isMarkerShown && <Marker position={props.coords} />}
    </GoogleMap>
  )),
)

interface Props {
  params: any
  userId: string
  organization: Organization

  [key: string]: any
}

interface State {
  fullAddress: string
  coords: Coordinates
  isLoading: boolean

  // TODO: This allows anything in the State. This should be typed out
  [key: string]: any
}

class OrderConfirm extends React.Component<Props, State> {
  constructor(props) {
    super(props)

    const site = props?.order?.site
    const fullAddress = site
      ? `${site.address1}${site.address2 ? `, ${site.address2}` : ''}, ${site.city}, ${site.state} ${site.zip}`
      : ''

    this.state = {
      coords: { lat: 37.735501, lng: -122.20032700000002 },
      fullAddress,
      ...props.order,
      errors: [],
    }
  }

  componentWillMount() {
    const Geocoder = get(window, 'google.maps.Geocoder')
    if (Geocoder) {
      const gmaps = new Geocoder()
      gmaps.geocode({ address: this.state.fullAddress }, (results, status) => {
        let coords = this.state.coords
        if (status === window.google.maps.GeocoderStatus.OK) {
          if (status !== window.google.maps.GeocoderStatus.ZERO_RESULTS) {
            coords = results[0].geometry.location.toJSON()
          } else {
            const error = 'No results found'

            this.setState({
              errors: [error],
            })

            logger.error(error)
          }
        } else {
          const error = `Geocode was not successful for the following reason: ${status}`

          this.setState({
            errors: [error],
          })

          logger.error(error)
        }

        return this.setState({
          coords,
        })
      })
    }
  }

  getSubmitButton(newOrder, isAdmin) {
    if (newOrder) {
      return (
        <Form.Field
          disabled={this.state.isLoading}
          loading={this.state.isLoading}
          control={Button}
          positive
          size="big"
          fluid
          onClick={(e) => this.create(e)}
        >
          <Icon name="checkmark" />
          Create Order
        </Form.Field>
      )
    }
    if (isAdmin && !newOrder) {
      return (
        <SendOrderStatusUpdateEmailModal
          params={this.props.params}
          userId={this.props.userId}
          email={this.state}
          handleEdit={(e) => this.edit(e)}
          trigger={
            <Form.Field control={Button} positive size="big" fluid>
              <Icon name="checkmark" />
              Update Order
            </Form.Field>
          }
        />
      )
    }
    return (
      <Form.Field
        disabled={this.state.isLoading}
        loading={this.state.isLoading}
        control={Button}
        positive
        size="big"
        fluid
        onClick={(e) => this.edit(e)}
      >
        <Icon name="checkmark" />
        Update Order
      </Form.Field>
    )
  }

  create = async (event) => {
    event.target.setAttribute('disabled', true)
    event.preventDefault()

    this.setState({ isLoading: true })

    const {
      action,
      adminNotes, // state fallback in case DOM ref's value cannot be read
      internalNotes, // state fallback in case DOM ref's value cannot be read
      date,
      daysTillPaymentDue,
      discount,
      draftId,
      exciseTax,
      gmv,
      irn,
      lineItems,
      name, // state fallback in case DOM ref's value cannot be read
      notes, // state fallback in case DOM ref's value cannot be read
      orgLicenseNum,
      siteCategory,
      siteLicenseNum,
      timeWindow,
      warehouseId,
      surcharge,
      hasUnalterablePaymentTerms,
      mustPayPreviousBalance,
    } = this.state

    const { organization, me, history } = this.props

    if (!organization.id) {
      return
    }

    const newSite: any = pick(this.state.site, [
      'name',
      'pocName',
      'pocEmail',
      'pocPhoneNumber',
      'address1',
      'address2',
      'city',
      'state',
      'zip',
      'lat',
      'lng',
      'siteCategory',
      'licensedLocationId',
    ])

    newSite.organizationId = organization.id

    const createSite = await this.props.createSite(newSite)

    // List of fields that have DOM refs because pulling values from state is not always consistent (likely due to debouncer for input handler):
    // `adminNotes`
    // `internalNotes`
    // `name`
    // `notes`

    const data = {
      action,
      adminNotes: idx(this.props, (_) => _.fieldRefs.adminNotes.value) || adminNotes,
      internalNotes: idx(this.props, (_) => _.fieldRefs.internalNotes.value) || internalNotes,
      creatorId: me?.user?.id,
      date,
      daysTillPaymentDue,
      discount: parseFloat(discount) || 0.0,
      exciseTax: parseFloat(exciseTax) || 0.0,
      exciseTaxCollected: 0,
      gmv: parseFloat(gmv) || 0.0,
      gmvCollected: 0,
      irn,
      licensedLocationId: this.state.site.licensedLocationId,
      lineItems: lineItems.map((li) => {
        if (li.organization) {
          li.organizationId = li.organization.id
        }
        if (li.skuBatch) {
          li.skuBatchId = li.skuBatch.id
        }
        li.quantity = parseFloat(li.quantity)
        li.pricePerUnit = parseFloat(li.pricePerUnit)
        li.discount = parseFloat(li.discount)
        const newLi = pick(li, [
          'code',
          'isExciseTaxable',
          'isSample',
          'name',
          'orderId',
          'organizationId',
          'oversold',
          'pricePerUnit',
          'quantity',
          'skuBatchId',
          'unit',
          'discount',
        ])
        return newLi
      }),
      name: idx(this.props, (_) => _.fieldRefs.name.firstElementChild.value) || name,
      notes: idx(this.props, (_) => _.fieldRefs.notes.value) || notes,
      orgLicenseNum,
      organizationId: organization.id,
      siteCategory,
      siteId: createSite.data.createSite.changedSite.id,
      siteLicenseNum,
      paymentStatus: 'UNPAID',
      status: 'UNSCHEDULED',
      timeWindow,
      // TODO: change key location
      warehouseId,
      surcharge: parseFloat(surcharge) || 0,
      hasUnalterablePaymentTerms,
      mustPayPreviousBalance,
    }

    try {
      const res = await client.mutate({
        mutation: CreateOrderDocument,
        variables: {
          input: {
            ...data,
          },
        },
      })

      if (res) {
        if (draftId) {
          await this.props.deleteDraft({ id: draftId })
        }

        if (me?.isAdmin) {
          history.push(`/${organization?.alias}/app/admin-orders`)
        } else {
          history.push(`/${organization?.alias}/app/orders`)
        }
      }
    } catch (error) {
      logger.error(error)
      this.setState({ GQLErrors: error })
    } finally {
      this.setState({ isLoading: false })
    }
  }

  edit = async (event) => {
    event.preventDefault()

    this.setState({ isLoading: true })

    const {
      action,
      adminNotes, // state fallback in case DOM ref's value cannot be read
      internalNotes, // state fallback in case DOM ref's value cannot be read
      date,
      daysTillPaymentDue,
      discount,
      draftId,
      exciseTax,
      exciseTaxCollected,
      gmv,
      gmvCollected,
      id,
      irn,
      lineItems,
      name, // state fallback in case DOM ref's value cannot be read
      notes, // state fallback in case DOM ref's value cannot be read
      orgLicenseNum,
      site,
      siteCategory,
      siteLicenseNum,
      status,
      timeWindow,
      warehouseId,
      surcharge,
      hasUnalterablePaymentTerms,
      mustPayPreviousBalance,
    } = this.state

    // List of fields that have DOM refs because pulling values from state is not always consistent (likely due to debouncer for input handler):
    // `adminNotes`
    // `internalNotes`
    // `name`
    // `notes`

    const data = {
      action,
      adminNotes, // idx(this.props, (_) => _.fieldRefs.adminNotes.value) || adminNotes,
      internalNotes, // : idx(this.props, (_) => _.fieldRefs.internalNotes.value) || internalNotes,
      date,
      daysTillPaymentDue,
      discount: parseFloat(discount) || 0.0,
      exciseTax: parseFloat(exciseTax) || 0.0,
      exciseTaxCollected: parseFloat(exciseTaxCollected) || 0.0,
      gmv: parseFloat(gmv) || 0.0,
      gmvCollected: parseFloat(gmvCollected) || 0.0,
      id,
      irn,
      lineItems: lineItems.map((li) => {
        if (li.skuBatch) {
          li.skuBatchId = li.skuBatch.id
        }
        li.quantity = parseFloat(li.quantity)
        li.pricePerUnit = parseFloat(li.pricePerUnit)
        li.discount = parseFloat(li.discount)
        const newLi = pick(li, [
          'id',
          'code',
          'isExciseTaxable',
          'isSample',
          'name',
          'orderId',
          'organization',
          'oversold',
          'pricePerUnit',
          'quantity',
          'skuBatchId',
          'unit',
          'discount',
        ])
        return omit(newLi, ['organization.__typename'])
      }),
      site: pick(site, [
        'pocName',
        'pocPhoneNumber',
        'pocEmail',
        'id',
        'name',
        'address1',
        'address2',
        'city',
        'state',
        'zip',
        'siteCategory',
        'licensedLocationId',
      ]),
      name: idx(this.props, (_) => _.fieldRefs.name.firstElementChild.value) || name,
      notes: idx(this.props, (_) => _.fieldRefs.notes.value) || notes,
      orgLicenseNum,
      siteCategory,
      siteLicenseNum,
      status,
      timeWindow,
      warehouseId,
      surcharge: parseFloat(surcharge) || 0,
      hasUnalterablePaymentTerms,
      mustPayPreviousBalance,
    }

    try {
      const res = await this.props.editOrder(data)
      const { history, organization, me } = this.props

      if (res) {
        if (draftId) {
          await this.props.deleteDraft({ id: draftId })
        }

        if (me?.isAdmin) {
          return history.push(`/${organization?.alias}/app/admin-orders`)
        }

        return history.push(`/${organization?.alias}/app/orders`)
      }

      if (me?.isAdmin) {
        history.push(`/${organization?.alias}/app/admin-orders`)
      } else {
        history.push(`/${organization?.alias}/app/orders`)
      }
    } catch (error) {
      logger.error(error)
      return this.setState({ GQLErrors: error })
    } finally {
      this.setState({ isLoading: false })
    }
  }

  generateManifestAsync(order) {
    return this.props.generateManifest(order.id)
  }

  render() {
    const {
      action,
      adminNotes,
      internalNotes,
      date,
      daysTillPaymentDue,
      discount,
      exciseTax,
      gmv,
      gmvCollected,
      GQLErrors,
      exciseTaxCollected,
      name,
      notes,
      site,
      timeWindow,
      surcharge,
      hasUnalterablePaymentTerms,
      mustPayPreviousBalance,
    } = this.state

    const { newOrder, data } = this.props

    const { isAdmin } = this.props?.me

    if (data && data.loading) {
      return <Loader active inline="centered" />
    }

    const fullAddress = site
      ? `${site.address1}${site.address2 ? `, ${site.address2}` : ''}, ${site.city}, ${site.state} ${site.zip}`
      : ''

    // console.log(
    //   'this.props.fieldRefs',
    //   this.props.fieldRefs,
    //   'this.props.fieldRefs.name.firstElementChild.value',
    //   idx(this.props, (_) => _.fieldRefs.name.firstElementChild.value),
    //   'this.props.fieldRefs.notes.value',
    //   idx(this.props, (_) => _.fieldRefs.notes.value),
    //   'this.props.fieldRefs.adminNotes.value',
    //   idx(this.props, (_) => _.fieldRefs.adminNotes.value),
    // )

    const netOffTotal = Number(surcharge || 0) - Number(discount || 0)
    const totalInvoiceAmount = Number(gmv || 0) + netOffTotal + Number(exciseTax || 0)

    const isPositiveNetCharge = netOffTotal > 0

    return (
      <Form>
        <Header>Order Details</Header>
        {!isEmpty(this.state.errors) && <ErrorBox errors={this.state.errors} />}
        <Card fluid>
          <Label color="green">Admin Only</Label>
          <Card.Content>
            <Container>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={3}>
                    <Header floated="left" size="small">
                      Admin Notes
                    </Header>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <Grid.Column>{adminNotes}</Grid.Column>
                  </Grid.Column>
                </Grid.Row>
                {isAdmin && (
                  <Grid.Row>
                    <Grid.Column width={3}>
                      <Header floated="left" size="small">
                        <div>Internal Notes</div>
                        <small>(Nabis Only)</small>
                      </Header>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <Grid.Column>{internalNotes}</Grid.Column>
                    </Grid.Column>
                  </Grid.Row>
                )}
              </Grid>
            </Container>
          </Card.Content>
        </Card>

        <Card fluid>
          <Card.Content>
            <Container>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={3}>
                    <Header floated="left" size="small">
                      Name
                    </Header>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <Grid.Column>{name}</Grid.Column>
                  </Grid.Column>

                  <Grid.Column width={5}>
                    <Header floated="left" size="small">
                      Action
                    </Header>
                  </Grid.Column>
                  <Grid.Column width={3}>
                    <Grid.Column>{titleize(action)}</Grid.Column>
                  </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                  <Grid.Column width={3}>
                    <Header floated="left" size="small">
                      Delivery Date
                    </Header>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <Grid.Column>{formatDate(date)}</Grid.Column>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    {newOrder ? (
                      <Header floated="left" size="small">
                        Gross Merchandise Value (GMV)
                      </Header>
                    ) : (
                      <Header floated="left" size="small">
                        GMV Collected
                        <Header as="h6">GMV (Invoice): {dollarFormat(gmv)}</Header>
                      </Header>
                    )}
                  </Grid.Column>
                  <Grid.Column width={3}>
                    {newOrder ? (
                      <Grid.Column>{dollarFormat(gmv)}</Grid.Column>
                    ) : (
                      <Grid.Column>{dollarFormat(gmvCollected)}</Grid.Column>
                    )}
                  </Grid.Column>
                </Grid.Row>

                {action === 'DELIVERY_TO_RETAILER' ? (
                  <Grid.Row>
                    <Grid.Column width={3}>
                      <Header floated="left" size="small">
                        Order Time Window
                      </Header>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <Grid.Column>{titleize(timeWindow)}</Grid.Column>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <Header floated="left" size="small">
                        {isPositiveNetCharge ? 'Surcharge' : 'Discount'} to Subtotal
                      </Header>
                    </Grid.Column>
                    <Grid.Column width={3}>
                      <Grid.Column>
                        {isPositiveNetCharge ? dollarFormat(netOffTotal) : `(${dollarFormat(Math.abs(netOffTotal))})`}
                      </Grid.Column>
                    </Grid.Column>
                  </Grid.Row>
                ) : (
                  <Grid.Row>
                    <Grid.Column width={3}>
                      <Header floated="left" size="small">
                        Order Time Window
                      </Header>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <Grid.Column>{timeWindow}</Grid.Column>
                    </Grid.Column>
                  </Grid.Row>
                )}

                {action === 'DELIVERY_TO_RETAILER' ? (
                  <Grid.Row>
                    <Grid.Column width={3}>
                      <Header floated="left" size="small">
                        Address
                      </Header>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <Grid.Column>{fullAddress}</Grid.Column>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      {newOrder ? (
                        <Header floated="left" size="small">
                          Excise Tax
                        </Header>
                      ) : (
                        <Header floated="left" size="small">
                          Excise Tax Collected
                          <Header as="h6">Excise Tax (Invoice): {dollarFormat(exciseTax)}</Header>
                        </Header>
                      )}
                    </Grid.Column>
                    <Grid.Column width={3}>
                      {newOrder ? (
                        <Grid.Column>{dollarFormat(exciseTax)}</Grid.Column>
                      ) : (
                        <Grid.Column>{dollarFormat(exciseTaxCollected)}</Grid.Column>
                      )}
                    </Grid.Column>
                  </Grid.Row>
                ) : (
                  <Grid.Row>
                    <Grid.Column width={3}>
                      <Header floated="left" size="small">
                        Address
                      </Header>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <Grid.Column>{fullAddress}</Grid.Column>
                    </Grid.Column>
                  </Grid.Row>
                )}

                {action === 'DELIVERY_TO_RETAILER' ? (
                  <Grid.Row>
                    <Grid.Column width={3}>
                      <Header floated="left" size="small">
                        Notes
                      </Header>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <Grid.Column>{notes}</Grid.Column>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      {newOrder ? (
                        <Header floated="left" size="small">
                          Full Payment
                        </Header>
                      ) : (
                        <Header floated="left" size="small">
                          Payment Collected
                          <Header as="h6">Total (Invoice): {dollarFormat(totalInvoiceAmount)}</Header>
                        </Header>
                      )}
                    </Grid.Column>
                    <Grid.Column width={3}>
                      {newOrder ? (
                        <Grid.Column>{dollarFormat(totalInvoiceAmount)}</Grid.Column>
                      ) : (
                        <Grid.Column>{dollarFormat(gmvCollected + exciseTaxCollected)}</Grid.Column>
                      )}
                    </Grid.Column>
                  </Grid.Row>
                ) : (
                  <Grid.Row>
                    <Grid.Column width={3}>
                      <Header floated="left" size="small">
                        Notes
                      </Header>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <Grid.Column>{notes}</Grid.Column>
                    </Grid.Column>
                  </Grid.Row>
                )}

                {action === 'DELIVERY_TO_RETAILER' && (
                  <>
                    <Grid.Row>
                      <Grid.Column width={3} />
                      <Grid.Column width={5} />
                      <Grid.Column width={5}>
                        <Header floated="left" size="small">
                          Payment Terms
                        </Header>
                      </Grid.Column>
                      <Grid.Column width={3}>
                        <Grid.Column>{titleizeDaysTillPaymentDue(daysTillPaymentDue)}</Grid.Column>
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column width={3} />
                      <Grid.Column width={5} />
                      <Grid.Column width={5}>
                        <Header floated="left" size="small">
                          Unalterable?
                        </Header>
                      </Grid.Column>
                      <Grid.Column width={3}>
                        <Grid.Column>{hasUnalterablePaymentTerms ? 'Yes' : 'No'}</Grid.Column>
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column width={3} />
                      <Grid.Column width={5} />
                      <Grid.Column width={5}>
                        <Header floated="left" size="small">
                          <MustPayLabel linebreak={false} />
                        </Header>
                      </Grid.Column>
                      <Grid.Column width={3}>
                        <Grid.Column>{mustPayPreviousBalance ? 'Yes' : 'No'}</Grid.Column>
                      </Grid.Column>
                    </Grid.Row>
                  </>
                )}
              </Grid>

              <Divider />

              <OrderMapComponent
                isMarkerShown
                coords={this.state.coords}
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXpq530P70c0iiCSJ2L6i_cfplT8iTXZY&v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: '100%' }} />}
                containerElement={<div style={{ height: '400px' }} />}
                mapElement={<div style={{ height: '100%' }} />}
              />
            </Container>
          </Card.Content>
        </Card>

        {GQLErrors && (
          <Header color="red" size="small">
            {GQLErrors.toString()}
          </Header>
        )}

        <SubmitContainer>
          <Form.Field
            control={Button}
            size="big"
            fluid
            onClick={() => this.props.onUpdateOrder(this.state, NEW_ORDER_STEPS.FORM)}
          >
            Back
          </Form.Field>
          {this.getSubmitButton(newOrder, isAdmin)}
        </SubmitContainer>
      </Form>
    )
  }
}

export default compose(OrderConfirmApollo, withRouter, withMeHook, withOrganizationHook)(OrderConfirm)
