import { GetOrderDocument, GetSkuBatchDocument, withGenerateManifest } from 'generated/graphql'
import { graphql } from 'react-apollo'
import { withRouter } from 'react-router'
import { compose, withProps } from 'recompose'
import { oc } from 'ts-optchain'
import AddEditOrder from './AddEditOrder'

export default compose(
  withRouter,
  graphql(GetOrderDocument, {
    options: (props) => {
      return {
        fetchPolicy: 'network-only',
        variables: {
          id: oc(props as any).match.params.orderId(),
        },
      }
    },
    skip: (props) => !oc(props as any).match.params.orderId(),
  }),
  withGenerateManifest({
    name: 'generateManifest',
    options: () => ({ refetchQueries: [GetOrderDocument] }),
    props: ({ ownProps }: { ownProps: any }) =>
      ({
        orderId: ownProps?.orderId,
      } as any),
  }),
  withProps({
    editOrder: true,
    newOrder: false,
    getSkuBatch: GetSkuBatchDocument,
  }),
)(AddEditOrder)
