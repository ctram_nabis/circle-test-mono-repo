import { AllLicensedLocationsDocument, AllWarehousesDocument } from 'generated/graphql'
import idx from 'idx'
import { graphql } from 'react-apollo'
import { compose } from 'recompose'

export default (module) =>
  compose(
    graphql(AllWarehousesDocument, {
      options: (_props: any) => {
        return {
          fetchPolicy: 'network-only',
        }
      },
      props: ({ data }) => ({
        allWarehouses: idx(data as any, (_) => _.viewer.allWarehouses),
      }),
    }),
    graphql(AllLicensedLocationsDocument, {
      props: ({ data }) => ({
        allLicensedLocations: idx(data as any, (_) => _.viewer.allLicensedLocations),
      }),
      options: (props: any) => ({
        variables: {
          pageInfo: {
            page: 1,
            numItemsPerPage: 5000,
            orderBy: [
              {
                attribute: 'name',
                order: 'ASC',
              },
              {
                attribute: 'createdAt',
                order: 'DESC',
              },
            ],
          },
        },
      }),
    }),
  )(module)
