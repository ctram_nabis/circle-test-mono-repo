import { GetDraftDocument, GetOrderDocument, GetSkuBatchDocument, withGenerateManifest } from 'generated/graphql'
import idx from 'idx'
import { graphql } from 'react-apollo'
import { withRouter } from 'react-router'
import { compose, withProps } from 'recompose'
import AddEditOrder from './AddEditOrder'

export default compose(
  withRouter,
  graphql(GetDraftDocument, {
    options: (props: any) => ({
      variables: {
        id: idx(props, (_) => _.params.draftId) || idx(props, (_) => _.location.state.draftId),
      },
      fetchPolicy: 'network-only',
    }),
    skip: (props) => !(idx(props, (_) => _.params.draftId) || idx(props, (_) => _.location.state.draftId)),
  }),
  graphql(GetOrderDocument, {
    options: (props: any) => {
      return {
        variables: {
          id: idx(props, (_) => _.params.id) || idx(props, (_) => _.location.state.orderId),
        },
        fetchPolicy: 'network-only',
      }
    },
    skip: (props) => !(idx(props, (_) => _.params.id) || idx(props, (_) => _.location.state.orderId)),
  }),
  withGenerateManifest({
    name: 'generateManifest',
    options: () => ({ refetchQueries: [GetOrderDocument] }),
    props: ({ ownProps }) =>
      ({
        orderId: idx(ownProps as any, (_) => _.orderId),
      } as any),
  }),
  withProps({
    newOrder: true,
    editOrder: false,
    getSkuBatch: GetSkuBatchDocument,
  }),
)(AddEditOrder)
