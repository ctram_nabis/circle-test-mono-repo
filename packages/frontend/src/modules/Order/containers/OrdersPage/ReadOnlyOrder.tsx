import { PageHeader } from 'components'
import { Text } from 'components/core/src/Text'
import { AllWarehousesDocument } from 'generated/graphql'
import idx from 'idx'
import { calculateMoney } from 'modules/Order/helpers'
import moment from 'moment-timezone'
import React from 'react'
import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import {
  Card,
  Checkbox,
  Divider,
  Form,
  Grid,
  GridColumn,
  GridRow,
  Header,
  Label,
  Popup,
  Segment,
} from 'semantic-ui-react'
import styled from 'styled-components'
import { colors } from 'system/theme'
import { dollarFormat, titleize } from 'utils'
import { getTaxRate } from '../../helpers/calculateMoney'
import { MustPayLabel } from 'modules/Order/components'

const Input = (props) => {
  const DivStyled = styled.div`
    line-height: 1.21428571em;
    padding: 0.67857143em 1em;
    background: #fff;
    border: 1px solid rgba(34, 36, 38, 0.1);
    border-radius: 0.28571429rem;
    min-height: 38px;
    color: rgba(0, 0, 0, 0.87);
  `

  return <DivStyled>{props.value}</DivStyled>
}

const StatusLabelContainer = styled.span`
  float: right;
`

const SubHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const SubLabelWrapper = styled.span`
  font-weight: 100;
`

const Help = styled.span`
  text-decoration: underline;
`

interface Props {
  order: any

  [key: string]: any
}

class ReadOnlyOrder extends React.Component<Props, any> {
  getStatusLabelColor() {
    const { order } = this.props
    const status = order ? order.status : 'UNSCHEDULED'

    switch (status) {
      case 'UNSCHEDULED':
        return 'grey'
      case 'IN_PROGRESS':
        return 'yellow'
      case 'COMPLETE':
        return 'green'
      default:
        return 'grey'
    }
  }

  renderWarehouseDropdown = ({ allWarehouses = [], warehouseId }) => {
    const warehouseText = (
      allWarehouses
        .map((w: any) => ({
          key: w.id,
          text: w.site ? `${w.site.name} | ${w.site.city}, ${w.site.state}` : 'error: site missing',
          value: w.id,
        }))
        .find((wh) => wh.value === warehouseId) || {}
    ).text

    return (
      <Segment>
        <Label htmlFor="warehouse" attached="top">
          Source Warehouse
        </Label>
        <Input value={warehouseText} />
      </Segment>
    )
  }

  generateSkuBatchSelectValue = ({ sku, batch }) => {
    const skuName = sku && sku.name
    const skuCode = sku && sku.code
    const batchCode = batch ? batch.code : 'n/a'

    const text = `${skuCode}: ${batchCode}, ${skuName} `

    return text
  }

  renderLineItems = () => {
    const { lineItems } = this.props.order || {}

    if (!lineItems) {
      return null
    }

    return (
      <Segment>
        <div style={{ display: 'inline-block' }}>
          <label htmlFor="line-items">Line Items</label>
        </div>
        <Segment.Group>
          {lineItems.map((lineItem, i) => {
            const { quantity, pricePerUnit, isExciseTaxable, unit, isSample, discount } = lineItem
            return (
              <Segment key={`line-item-${i}`} tertiary style={{ background: `${colors.shade1}` }}>
                <Header as="h5">Line Item {i + 1}:</Header>
                <Form.Group>
                  <Form.Field width={8}>
                    <label htmlFor="lineItem">SKU</label>
                    <Input
                      value={
                        lineItem.name && lineItem.skuBatch ? this.generateSkuBatchSelectValue(lineItem.skuBatch) : ''
                      }
                    />
                  </Form.Field>
                  <Form.Field width={4}>
                    <label htmlFor="quantity">Quantity</label>
                    <Input value={quantity} />
                  </Form.Field>
                  <Form.Field width={4}>
                    <label htmlFor="pricePerUnit">
                      Price Per Unit{' '}
                      <SubLabelWrapper>
                        (Sample?{' '}
                        <Checkbox
                          name="isSample"
                          type="checkbox"
                          checked={isSample || false}
                          style={{ minHeight: '12px' }}
                        />
                        )
                      </SubLabelWrapper>
                    </label>
                    <Input value={`${dollarFormat(parseFloat(pricePerUnit).toFixed(2))} / ${unit || 'unit'}`} />
                    {isSample && (
                      <Text bold color="red">
                        * NOT FOR RESALE *
                      </Text>
                    )}
                  </Form.Field>
                </Form.Group>
                <Form.Group>
                  <Form.Field width={4}>
                    <label htmlFor="subtotal">Subtotal</label>
                    <Input value={`${dollarFormat((quantity * pricePerUnit).toFixed(2))}`} />
                  </Form.Field>
                  <Form.Field width={4}>
                    <label htmlFor="discount">Discount Total</label>
                    <Input value={`${dollarFormat(discount.toFixed(2))}`} />
                  </Form.Field>
                  <Form.Field width={4}>
                    <label htmlFor="lineItemTotal">Line Item Total</label>
                    <Input value={`${dollarFormat((quantity * pricePerUnit - (discount || 0)).toFixed(2))}`} />
                  </Form.Field>
                  <Form.Field width={4}>
                    <label htmlFor="isExciseTaxable">Excise Tax?</label>
                    <Checkbox name="isExciseTaxable" type="checkbox" checked={isExciseTaxable || false} />
                  </Form.Field>
                </Form.Group>
              </Segment>
            )
          })}
        </Segment.Group>
      </Segment>
    )
  }

  renderAdminSection = ({ subtotal, totalExciseTax }) => {
    const { gmvCollected, exciseTaxCollected, adminNotes, status, discount } = this.props.order || {}
    const parsedDiscount = parseFloat(discount) || 0.0

    return (
      <Card fluid>
        <Label color="green">Admin Only</Label>
        <Card.Content>
          <Form.Group widths="equal">
            <Form.Field>
              <label htmlFor="status">Order Status</label>
              <Input value={titleize(status)} />
            </Form.Field>
            <Form.Field>
              <label htmlFor="gmvCollected">
                GMV Collected{' '}
                <SubLabelWrapper>
                  (Subtotal - Discount = {dollarFormat((parseFloat(subtotal) - parsedDiscount).toFixed(2))})
                </SubLabelWrapper>
              </label>
              <Input value={`${dollarFormat(parseFloat(gmvCollected).toFixed(2))}`} />
            </Form.Field>
            <Form.Field>
              <label htmlFor="exciseTaxCollected">
                Excise Tax Collected{' '}
                <SubLabelWrapper>
                  (Invoiced Excise Tax = {dollarFormat(parseFloat(totalExciseTax).toFixed(2))})
                </SubLabelWrapper>
              </label>
              <Input value={`${dollarFormat(parseFloat(exciseTaxCollected).toFixed(2))}`} />
            </Form.Field>
          </Form.Group>

          <Form.Group widths="equal">
            <Form.Field>
              <label htmlFor="form-textarea-control-admin-notes">Admin Notes</label>
              <Input id="form-textarea-control-admin-notes" value={adminNotes} />
            </Form.Field>
          </Form.Group>
        </Card.Content>
      </Card>
    )
  }

  render() {
    const { order, allWarehouses = [] } = this.props

    const {
      action,
      date,
      daysTillPaymentDue,
      discount,
      irn,
      name,
      notes,
      site,
      siteLicenseNum,
      timeWindow = 'ALL DAY',
      lineItems,
      warehouse,
      surcharge,
      hasUnalterablePaymentTerms,
      mustPayPreviousBalance,
    } = order

    const { subtotal, totalExciseTax, totalBalance } = calculateMoney({
      discount,
      lineItems,
      siteUsState: site.state,
      deliveryDate: date,
      surcharge: parseFloat(surcharge) || 0,
    })

    const IrnHelp = (
      <Popup
        trigger={<Help>What is this?</Help>}
        content="Optional. Provide a purchase order or sales order number to help dispensaries reconcile a Nabis shipment with your SO or PO. We put this number on the manifest if you provide it."
      />
    )

    const taxRate = (getTaxRate(site.state, date) * 100).toFixed(2)
    const taxRateLabel = `Excise tax (${taxRate}% of taxable subtotal)`

    return (
      <Grid>
        <GridRow>
          <GridColumn>
            <PageHeader>
              <span>View Order</span>
              <StatusLabelContainer>
                <Label color={this.getStatusLabelColor()}>
                  Order Status
                  <Label.Detail>{titleize(order.status)}</Label.Detail>
                </Label>
              </StatusLabelContainer>
            </PageHeader>
          </GridColumn>
        </GridRow>

        <Grid.Row>
          <Grid.Column>
            <SubHeader>
              <Label>Order #{order.number}</Label>
            </SubHeader>
          </Grid.Column>
        </Grid.Row>

        <GridRow>
          <GridColumn>
            <Form id="order-form">
              {this.renderAdminSection({ subtotal, totalExciseTax })}
              <Card fluid>
                <Card.Content>
                  <Form.Group widths="equal">
                    <Form.Field>
                      <label htmlFor="name">Order Nickname or DBA (Doing Business As)</label>
                      <Input value={name} />
                    </Form.Field>
                    <Form.Field>
                      <label htmlFor="irn">PO/SO Number ({IrnHelp})</label>
                      <Input value={irn} />
                    </Form.Field>
                  </Form.Group>
                  <Divider />
                  <Form.Group widths="equal">
                    <Form.Field>
                      <label htmlFor="action">Action</label>
                      <Input value={titleize(action)} />
                    </Form.Field>
                    <Form.Field>
                      <label htmlFor="date">Delivery Date</label>
                      <Input
                        value={moment(date)
                          .utc()
                          .format('LL')}
                      />
                    </Form.Field>
                    <Form.Field>
                      <label htmlFor="timeWindow">Time Window</label>
                      <Input value={timeWindow} />
                    </Form.Field>
                    {action === 'DELIVERY_TO_RETAILER' && (
                      <Form.Field>
                        <label htmlFor="daysTillPaymentDue">
                          Payment Terms*&nbsp;(Unalterable?&nbsp;
                          <Checkbox
                            name="hasUnalterablePaymentTerms"
                            type="checkbox"
                            checked={hasUnalterablePaymentTerms || false}
                            style={{ minHeight: '12px' }}
                          />
                          )
                        </label>
                        <Input
                          value={
                            (
                              [
                                { key: 0, value: 0, text: 'COD' },
                                { key: 7, value: 7, text: 'Net 7' },
                                { key: 14, value: 14, text: 'Net 14' },
                                { key: 15, value: 15, text: 'Net 15' },
                                { key: 30, value: 30, text: 'Net 30' },
                                { key: 45, value: 45, text: 'Net 45' },
                                { key: 60, value: 60, text: 'Net 60' },
                                { key: 90, value: 90, text: 'Net 90' },
                                {
                                  key: 'n/a',
                                  text: 'None (pickup or transfer)',
                                  value: null,
                                },
                              ].find((el) => el.value === daysTillPaymentDue) || {}
                            ).text
                          }
                        />
                      </Form.Field>
                    )}
                  </Form.Group>
                  <Form.Group>
                    <Form.Field width={8}>
                      <label htmlFor="site">
                        {action === 'PICKUP_FROM_BRAND' ? 'Pickup Location ' : 'Licensed Entity '}
                      </label>
                      <Input
                        value={
                          (site && site.name && (
                            <div>
                              <b>{site.name}</b> ({site.address1}
                              {site.address2 ? `, ${site.address2}` : ''}, {site.city}, {site.state}, {site.zip})
                            </div>
                          )) ||
                          ''
                        }
                      />
                    </Form.Field>
                    <Form.Field width={4}>
                      <label htmlFor="siteLicenseNum">Licensed Entity's License Number</label>
                      <Input value={siteLicenseNum} />
                    </Form.Field>
                    {action === 'DELIVERY_TO_RETAILER' && (
                      <Form.Field width={4}>
                        <label>
                          <MustPayLabel />
                          &nbsp;
                          <Checkbox
                            name="mustPayPreviousBalance"
                            type="checkbox"
                            checked={mustPayPreviousBalance || false}
                            style={{ minHeight: '12px' }}
                          />
                        </label>
                      </Form.Field>
                    )}
                  </Form.Group>
                  <Form.Group widths="equal">
                    <Form.Field>
                      <label htmlFor="pocName">Contact Person*</label>
                      <Input value={idx(this.props, (_) => _.order.site.pocName)} />
                    </Form.Field>
                    <Form.Field>
                      <label htmlFor="pocPhoneNumber">Phone Number*</label>
                      <Input value={idx(this.props, (_) => _.order.site.pocPhoneNumber)} />
                    </Form.Field>
                    <Form.Field>
                      <label htmlFor="email">Email*</label>
                      <Input value={idx(this.props, (_) => _.order.site.pocEmail)} />
                    </Form.Field>
                  </Form.Group>
                  <Form.Group widths="equal">
                    <Form.Field>
                      <label htmlFor="form-textarea-control-delivery-notes">Delivery Notes</label>
                      <Input id="form-textarea-control-delivery-notes" value={notes} />
                    </Form.Field>
                  </Form.Group>
                  <Form.Field>
                    <div>
                      {this.renderWarehouseDropdown({
                        allWarehouses,
                        warehouseId: warehouse?.id,
                      })}
                    </div>
                    {this.renderLineItems()}
                  </Form.Field>
                </Card.Content>

                <Card.Content>
                  <Form.Group widths="equal">
                    <Form.Field>
                      <label htmlFor="subtotal">Subtotal</label>
                      <Input value={`${dollarFormat(subtotal.toFixed(2))}`} />
                    </Form.Field>
                    <Form.Field>
                      <label htmlFor="discount">Discount off subtotal ($)</label>
                      <Input value={`${dollarFormat(discount.toFixed(2))}`} />
                    </Form.Field>
                    <Form.Field>
                      <label htmlFor="exciseTax">{taxRateLabel}</label>
                      <Input value={`${dollarFormat(totalExciseTax.toFixed(2))}`} />
                    </Form.Field>
                    <Form.Field>
                      <label htmlFor="totalBalance">Total Balance Due</label>
                      <Input value={`${dollarFormat(totalBalance.toFixed(2))}`} />
                    </Form.Field>
                  </Form.Group>
                  <Form.Group widths="equal">
                    <Form.Field />
                    <Form.Field>
                      <label htmlFor="surcharge">Surcharge on subtotal ($)</label>
                      <Input value={`${dollarFormat(surcharge)}`} />
                    </Form.Field>
                    <Form.Field />
                    <Form.Field />
                  </Form.Group>
                </Card.Content>
              </Card>
            </Form>
          </GridColumn>
        </GridRow>
      </Grid>
    )
  }
}

export default compose<Props, any>(
  graphql(AllWarehousesDocument, {
    options: () => {
      return {
        fetchPolicy: 'network-only',
      }
    },
    props: ({ data }) =>
      ({
        allWarehouses: idx(data as any, (_) => _.viewer.allWarehouses),
      } as any),
  }),
)(ReadOnlyOrder)
