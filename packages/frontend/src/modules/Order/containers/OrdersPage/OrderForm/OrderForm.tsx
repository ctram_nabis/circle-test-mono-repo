import { ErrorBox } from 'components'
import { Button } from 'components/Button'
import {
  AccessibleSkuBatchesDocument,
  AllSitesDocument,
  CreateDraftDocument,
  GetDraftDocument,
  GetOrderDocument,
  License,
  Organization,
  OrganizationDocument,
  Site,
  UpdateDraftDocument,
  UpdateOrderDocument,
} from 'generated/graphql'
import idx from 'idx'
import { cloneDeep, isEmpty, isEqual, maxBy } from 'lodash'
import { client } from 'modules/Apollo'
import logger from 'modules/Logger'
import { MustPayLabel } from 'modules/Order/components'
import { OrderFormDatePicker } from 'modules/Order/components/OrderFormDatePicker'
import { ORDER_ACTIONS } from 'modules/Order/constants'
import { withOrganizationHook } from 'modules/Order/helpers/withOrganizationHook'
import { AddSiteModal, EditSiteModal } from 'modules/Site'
import { withMeHook } from 'modules/User/helpers/withMeHook'
import moment from 'moment'
import React from 'react'
import { withRouter } from 'react-router'
import { compose } from 'recompose'
import reportValidity from 'report-validity'
import {
  Card,
  Checkbox,
  Dimmer,
  Divider,
  Dropdown,
  DropdownItemProps,
  Form,
  Icon,
  Input,
  Loader,
  Popup,
  Ref,
  Segment,
  TextArea,
} from 'semantic-ui-react'
import styled from 'styled-components'
import { colors } from 'system/theme'
import { isOrderValid } from 'utils'
import { ORDER_ACTION_ADMIN_ADDITIONAL_OPTIONS } from '../../..'
import { WarehouseDropdown } from '../../../components'
import { ORDER_ACTION_OPTIONS } from '../../../constants/OrderActionOptions'
import { calculateMoney, getTaxRate } from '../../../helpers/calculateMoney'
import { getDefaultOrder, NEW_ORDER_STEPS, pickOrderFromState } from '../AddEditOrder'
import { ConfirmModal } from '../ConfirmModal'
import OrderFormApollo from '../OrderFormApollo'
import { AdminSection } from './AdminSection'
import { LineItem } from './LineItem'
import { OrderFormTooltip } from './OrderFormTooltip'

const AddLineItemContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

const AddLineItem = styled.div`
  cursor: pointer;
  text-align: right;
  font-size: 14px;
  padding-right: 10px;
  color: ${colors.primary};
  &:hover {
    text-decoration: underline;
  }
`

const LoadingSegment = styled(Segment)`
  height: 50px !important;
  width: 100%;
`
const SubmitContainer = styled.div`
  flex: 0 0 50px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 20px 0px;
`

const Help = styled.span`
  text-decoration: underline;
`

const createEmptyLineItem = () => ({
  code: '',
  isExciseTaxable: true,
  isSample: false,
  name: '',
  pricePerUnit: 0,
  quantity: 1,
  unit: '',
  discount: 0,
})

const isCanndescent = (organization): boolean => organization?.alias === 'canndescent'
const isHenrys = (organization): boolean => organization?.alias === 'hhc'

class OrderForm extends React.Component<any, any> {
  inputChangeTimerMap = {}
  constructor(props) {
    super(props)
    const newState = this.getStateFromProps(props)

    this.state = {
      ...newState,
    }
  }

  componentWillReceiveProps(props) {
    const newState = this.getStateFromProps(props)
    const { history } = props

    this.setState(
      {
        ...newState,
        draftId: history?.location?.state?.draftId,
        orderId: history?.location?.state?.orderId,
        editDraft: history?.location?.state?.editDraft,
        editOrder: history?.location?.state?.editOrder,
        isLoadingSkus: false,
        errors: [],
      },
      () => this.forceUpdate(),
    )
  }

  getStateFromProps = ({ order, newOrder, editOrder, editDraft, me, ...props }: any) => {
    const { date, site } = this.state || {}
    const { email: userEmail, id: userId, isAdmin } = me || {}

    const stateLineItems = cloneDeep(idx(this.state, (_) => _.lineItems)) || [createEmptyLineItem()]
    const propLineItems = cloneDeep(idx(props, (_) => _.order.lineItems)) || []

    const newLineItems = [...stateLineItems, ...propLineItems]
    // if all the items are non-taxable, the initial toggleAllExcise value will be true
    const noExciseTaxableLineItems = newLineItems.filter((li) => li.isExciseTaxable).length === 0

    // make sure site doesn't get overwritten by incoming apollo props
    const newSite = isEmpty(site) ? cloneDeep(order?.site) || {} : site

    const newState = {
      isAdmin,
      // extract from order object
      lineItems: newLineItems,
      editOrder,
      editDraft,
      userEmail,
      userId,
      ...props,
      ...order,
      date: date || idx(order, (_) => _.date),
      site: newSite,
      siteLicenseNum:
        idx(order, (_) => _.siteLicenseNum) || idx(order, (_) => _.site.licensedLocation.licenses[0].licenseNumber),
      skuBatches: this.state?.skuBatches || [],
      toggleAllExciseOn: noExciseTaxableLineItems,
      // TODO: order.warehouseId is incorrect
      // time permitting, need to figure out WHY!
      warehouseId: idx(order, (_) => _.warehouse.id) || idx(order, (_) => _.warehouseId),
    }

    return newState
  }

  addLineItem = () => {
    const { lineItems } = this.state
    lineItems.push(createEmptyLineItem())
    this.setState({ lineItems })
  }

  removeLineItem = (index) => {
    const confirmation = window.confirm('Are you sure you want to remove this line item?')

    if (confirmation) {
      const { lineItems } = this.state
      lineItems.splice(index, 1)
      this.setState({ lineItems })
    }
  }
  // TODO: Paid in Full is broken (CalculateMoney issue). Delete below if no one complains about the
  // TODO: missing feature. today date 12/24/19 - Miranda Howitt See: AdminSection.tsx
  // payInFullOnClick = () => {
  //   const { subtotal, discount, exciseTax } = this.state
  //   const parsedDiscount = parseFloat(discount) || 0.0
  //   this.setState({
  //     exciseTaxCollected: parseFloat(exciseTax).toFixed(2),
  //     gmvCollected: (parseFloat(subtotal) - parsedDiscount).toFixed(2),
  //   })
  // }

  debounce = (func, key, time = 1000) => {
    if (this.inputChangeTimerMap[key]) {
      clearTimeout(this.inputChangeTimerMap[key])
    }

    if (typeof func === 'function') {
      this.inputChangeTimerMap[key] = setTimeout(() => func(), time)
    }
  }

  handleInputChange = (key, value) => {
    if (key === 'gmv') {
      const gmv = parseFloat(value || 0)
      const exciseTax = gmv * 1.6 * 0.15
      this.setState({ gmv: gmv.toFixed(2), exciseTax: exciseTax.toFixed(2) })
    } else {
      this.setState({ [key]: value })
    }
  }

  // TODO: THIS WILL BE DELETED ONCE REFACTOR of page is complete
  onInputChange = (key) => (_e, { value }) =>
    this.debounce(
      async () => {
        if (key === 'gmv') {
          const gmv = parseFloat(value || 0)
          const exciseTax = gmv * 1.6 * 0.15
          this.setState({ gmv: gmv.toFixed(2), exciseTax: exciseTax.toFixed(2) })
        } else {
          this.setState({ [key]: value })
        }
      },
      key,
      800,
    )

  // YUCK! TODO: Only created this because onInputChange is hard to use with Checkbox component
  onCheckboxChange = (key, value) => {
    this.setState({ [key]: value })
  }

  getMinDate = () => {
    const timeInCalifornia = moment().tz('America/Los_Angeles')
    const whatDayCalifornia = timeInCalifornia.day()

    const noonDayInCalifornia = moment(timeInCalifornia)
      .hours(12)
      .minutes(0)
      .seconds(0)

    const beforeNoonInCalifornia = timeInCalifornia < noonDayInCalifornia

    const daysTillMinDate = {
      0: 3, // Sun -> W
      1: beforeNoonInCalifornia ? 2 : 3, // M -> W, Th
      2: beforeNoonInCalifornia ? 2 : 3, // T -> Th, F
      3: beforeNoonInCalifornia ? 2 : 5, // W -> F, M
      4: beforeNoonInCalifornia ? 4 : 5, // Th -> M, T
      5: beforeNoonInCalifornia ? 4 : 5, // F -> T, W
      6: 4, // Sat -> W
    }[whatDayCalifornia]

    const { isAdmin } = this.props?.me
    const { organization } = this.state
    const isThursdayValue = whatDayCalifornia === 6 ? 10 : 2
    const daysExtended = isCanndescent(organization) ? isThursdayValue : daysTillMinDate

    const minimumDate = moment(timeInCalifornia)
      .add(daysExtended, 'days')
      .hours(0)
      .minutes(0)
      .seconds(0)

    return isAdmin ? moment(0) : minimumDate
  }

  handleOrderDateChange = (datePickerInput: Date | null) => {
    if (datePickerInput) {
      const strippedDate = datePickerInput
      strippedDate.setHours(0, 0, 0, 0)
      this.setState({ date: strippedDate.toString() })
    }
  }

  onActionChange = (_e, action) => {
    if (action.value === 'INTERNAL_TRANSFER') {
      this.setState({ site: {}, siteLicenseNum: null })
    }
    this.setState({ action: action.value })
  }

  handleSiteChange = async (data: any) => {
    const publicSites = idx(this.props, (_) => _.allLicensedLocations.results) || []
    const selectedPublicSite = publicSites.find((ps) => ps.id === data) || {}

    const { name } = selectedPublicSite
    const sameName = (s) => s.name === name

    const sites = idx(this.state, (_) => _.allSites) || []

    if (!sites.filter) {
      return
    }

    const selectedSite = maxBy(
      sites.filter((ss) => sameName(ss) && ss.zip === selectedPublicSite.zip),
      (s: Site) => s.createdAt,
    )

    const licensedLocationId = selectedPublicSite.id

    // if creating, ensure that id remains undefined to create a new site.
    // if editing, ensure that id is same id so that edit is successful.
    const id = idx(this.state, (_) => _.site.id) || (selectedSite && selectedSite.id)

    if (selectedPublicSite.warehouse) {
      this.setState({ warehouseId: selectedPublicSite.warehouse.id })
    }

    const newSite = {
      // default POC info is all empty strings
      pocEmail: '',
      pocName: '',
      pocPhoneNumber: '',

      ...selectedSite, // overrides poc info
      ...selectedPublicSite, // it looks like this overrides address and name of selected site

      id,
      licensedLocation: selectedPublicSite, // ew
      licensedLocationId,
    }

    this.setState({
      site: newSite,
      siteLicenseNum: idx(newSite, (_) => _.licenses[0].licenseNumber),
    })
  }

  handleLicenseChange = (_e, { value }) => {
    this.setState({ siteLicenseNum: value })
  }

  handlePocChange = (key, val) => {
    const { site } = this.state
    site[key] = val
    this.setState({ site })
  }

  handleSiteLocationChange = (_e, data) => {
    const value = data.value
    const site = {
      ...this.state.site,
      ...value,
    }

    this.setState({ site })
  }

  handleOrgLicenseNumChange = (_e, data) => {
    const value = data.value
    this.setState({
      orgLicenseNum: value,
    })
  }

  handleLineItemChange = (i) => (_e, { name, value }) => {
    const { lineItems } = this.state || {}
    lineItems[i][name] = value
    this.setState({ lineItems })
  }

  async GetOrganization(id) {
    const response = await client.query({
      query: OrganizationDocument,
      variables: {
        id,
      },
    })

    return response?.data?.getOrganization
  }

  async AllSites() {
    const { organization } = this.props
    const response = await client.query({
      query: AllSitesDocument,
      variables: {
        organizationId: organization?.id,
      },
    })

    return response?.data?.viewer?.allSites || []
  }

  handleLineItemBrandChange = async (organizationId, i) => {
    this.setState({ isLoadingSkus: true })

    const { lineItems } = this.state
    const lineItem = lineItems[i]
    const { organization: submittingOrg } = this.props

    lineItem.organization = await this.GetOrganization(organizationId)
    lineItem.organizationId = organizationId
    lineItem.skuBatch = undefined

    const response = await client.query({
      query: AccessibleSkuBatchesDocument,
      variables: {
        brandId: organizationId,
        organizationId: submittingOrg?.id,
        pageInfo: {
          page: 1,
          numItemsPerPage: 200,
          orderBy: [
            {
              attribute: 'createdAt',
              order: 'DESC',
            },
          ],
        },
      },
    })

    this.setState({
      lineItems,
      skuBatches: response?.data?.viewer?.accessibleSkuBatches?.results || [],
      isLoadingSkus: false,
    })
  }

  handleExciseTaxChange = (index = 0) => (_e, { checked }) => {
    const { lineItems = [] } = this.state
    const lineItem = lineItems[index]

    if (!lineItem) {
      throw new Error('Missing lineitem in handleExciseTax')
    }

    if (lineItem.isSample && checked === true) {
      alert('No excise taxes for samples!')
      return
    }

    lineItems[index] = { ...lineItem, isExciseTaxable: Boolean(checked) }

    this.setState({ lineItems })
  }

  handleIsSampleChange = (index) => (_e, { checked }) => {
    const { lineItems } = this.state
    const lineItem = lineItems[index]
    const isSample = checked

    const { quantity, pricePerUnit = 0 } = lineItem

    lineItems[index] = {
      ...lineItem,
      isSample,
      discount: isSample ? Number(quantity) * Number(pricePerUnit) : 0,
      isExciseTaxable: !Boolean(isSample),
    }

    this.setState({ lineItems })
  }

  async componentDidMount() {
    this.setState({ isLoading: true })

    if (this.state?.isLoading) {
      return
    }

    const allSites = await this.AllSites()
    this.setState({ allSites })

    const skuBatches = await this.SkuBatches()
    this.setState({ skuBatches })

    if (this.state?.editDraft) {
      const draft = await this.GetDraft()
      this.setState({ draft })
    }

    if (this.state?.editOrder) {
      const order = await this.GetOrder()
      this.setState({ order })
    }

    this.setState({ isLoading: false })
  }

  SkuBatches = async () => {
    const response = await client.query({
      query: AccessibleSkuBatchesDocument,
      variables: {
        organizationId: this.props?.organization?.id,
        pageInfo: { page: 1, numItemsPerPage: this.state.isAdmin ? 20 : 400 },
      },
    })

    const allSkuBatches = response?.data?.viewer?.accessibleSkuBatches?.results || []
    return allSkuBatches
  }

  GetOrder = async () => {
    const { history } = this.props || {}
    const orderId = history?.location?.state?.orderId

    if (orderId) {
      const response = await client.query({
        query: GetOrderDocument,
        variables: {
          id: orderId,
        },
      })

      return response?.data?.getOrder
    }

    return {}
  }

  GetDraft = async () => {
    const { history } = this.props || {}
    const draftId = history?.location?.state?.draftId

    if (draftId) {
      const response = await client.query({
        query: GetDraftDocument,
        variables: {
          id: draftId,
        },
      })

      return response?.data?.getDraft
    }

    return {}
  }

  handleSkuChange = (i, _event, skuBatchId) => {
    const { lineItems, skuBatches } = this.state || {}

    const selectedSkuBatch = skuBatches.find((skuBatch) => skuBatch.id === skuBatchId) || {}

    const { sku = {}, batch = {} } = selectedSkuBatch
    const { name, code, pricePerUnit, isExciseTaxable, organization } = sku

    const lineItem = lineItems[i]

    const newLineItem = {
      ...lineItem,
      batch,
      batchId: batch?.id,
      code,
      isExciseTaxable,
      name,
      organization,
      organizationId: organization?.id,
      pricePerUnit,
      sku,
      skuBatch: selectedSkuBatch,
      skuBatchId: selectedSkuBatch.id,
    }

    lineItems[i] = newLineItem

    this.setState({ lineItems })
  }

  handleWarehouseChange = (warehouseId: string) => {
    this.setState({ warehouseId })
  }

  toggleAllLineItemExcise = (isTaxable) => {
    const { lineItems, toggleAllExciseOn } = this.state

    lineItems.forEach((li) => {
      li.isExciseTaxable = li.isSample ? false : Boolean(isTaxable)
    })

    this.setState({ lineItems, toggleAllExciseOn: !toggleAllExciseOn })
  }

  toggleAlert = async (confirm) => {
    this.setState({
      confirm,
      open: !this.state.open,
    })

    if (confirm) {
      await this.saveButtonOnClick()
      this.submitForm()
    }
  }

  submitForm = async () => {
    const {
      lineItems,
      discount,
      site: { state: siteUsState },
      date: deliveryDate,
      surcharge,
    } = this.state
    const { subtotal, totalExciseTax } = calculateMoney({
      lineItems,
      discount,
      siteUsState,
      deliveryDate,
      surcharge: parseFloat(surcharge) || 0,
    })

    const orderData: any = {
      ...this.state,
      exciseTax: totalExciseTax,
      gmv: subtotal,
    }

    delete orderData.toggleAllExciseOn

    await this.props.onUpdateOrder(orderData, NEW_ORDER_STEPS.CONFIRM)
  }

  nextButtonOnClick = async (event) => {
    event.preventDefault()
    const form = document.getElementById(event.target.getAttribute('form'))
    if (!reportValidity(form)) {
      return
    }

    if (!this.state.isAdmin && !this.state.open && !this.state.confirm) {
      this.toggleAlert(false)
      return
    }

    await this.saveButtonOnClick()
    this.submitForm()
  }

  saveButtonOnClick = async () => {
    const {
      organization: { id: organizationId },
      draftId,
      orderId,
      editDraft,
      lineItems,
      discount,
      site: { state: siteUsState },
      date: deliveryDate,
      surcharge,
    } = this.state
    const { subtotal, totalExciseTax } = calculateMoney({
      lineItems,
      discount,
      siteUsState,
      deliveryDate,
      surcharge: parseFloat(surcharge) || 0,
    })

    const body = pickOrderFromState({
      state: this.state,
      subtotal,
      totalExciseTax,
    })

    this.setState({ isSaving: true })

    if (!editDraft) {
      const response = await client.mutate({
        mutation: CreateDraftDocument,
        variables: {
          input: {
            organizationId,
            body,
          },
        },
      })

      const newDraftId = idx(response, (_) => _.data.createDraft.createdDraft.id)

      if (!newDraftId) {
        logger.error('There was an error creating a draft')
        this.setState({ errors: ['There was an error creating the draft'] })
      } else {
        await client.mutate({
          mutation: UpdateDraftDocument,
          variables: {
            input: {
              body,
              organizationId,
              id: newDraftId,
            },
          },
        })

        this.setState({ draftId: newDraftId, editDraft: true, isSaving: false })
        this.setState({ showDraftSavedPrompt: true })
        setTimeout(() => {
          this.setState({ showDraftSavedPrompt: false })
        }, 2500)
      }
    } else {
      if (draftId) {
        await client.mutate({
          mutation: UpdateDraftDocument,
          variables: {
            input: {
              organizationId,
              id: draftId,
              body,
            },
          },
        })
      }

      if (orderId) {
        await client.mutate({
          mutation: UpdateOrderDocument,
          variables: {
            input: {
              id: orderId,
              ...body,
            },
          },
        })
      }

      this.setState({ isSaving: false })
      this.setState({ showDraftSavedPrompt: true })
      setTimeout(() => {
        this.setState({ showDraftSavedPrompt: false })
      }, 2500)
    }
  }

  generateSkuBatchSelectValue = ({ inventoryValues, sku, batch }) => {
    const availableLA = idx(inventoryValues, (_) => _.LA.available) || 0
    const availableOAK = idx(inventoryValues, (_) => _.OAK.available) || 0

    const available = availableLA + availableOAK
    const skuName = sku ? sku.name : ''
    const skuCode = sku ? sku.code : ''
    const batchCode = batch ? batch.code : 'n/a'

    const text =
      `${skuCode}: ${batchCode}, ${skuName} ` +
      ` . . . . . . . . . . . . . . . . ` +
      `LA: ${availableLA} + Oak: ${availableOAK} ` +
      `= Available: ${available}`

    return text
  }

  skuBatchOptions = (_lineItems) => {
    const { skuBatches } = this.state || {}

    if (!skuBatches?.map) {
      return
    }

    const options = skuBatches
      .map((z, _k) => ({
        key: z.id,
        text: this.generateSkuBatchSelectValue(z),
        value: z.id,
      }))
      .filter((option) => option.value !== undefined)

    return options
  }

  onSkuBatchSearch = async (_e, { searchQuery }, { organization }: { organization: Organization }) =>
    this.debounce(async () => {
      if (!organization) return
      const { id } = organization
      this.setState({ isLoadingSkus: true })

      const response = await client.query({
        query: AccessibleSkuBatchesDocument,
        variables: {
          brandId: id,
          search: searchQuery,
          organizationId: this.state?.organization?.id,
          pageInfo: {
            page: 1,
            numItemsPerPage: 200,
            orderBy: [
              {
                attribute: 'createdAt',
                order: 'DESC',
              },
            ],
          },
        },
      })

      this.setState({
        skuBatches: idx(response, (_) => _.data.viewer.accessibleSkuBatches.results),
        isLoadingSkus: false,
      })
    }, 750)

  renderLineItems = (errors) => {
    const { isLoading, isLoadingSkus, lineItems, toggleAllExciseOn = false } = this.state || {}

    if (!lineItems?.map) {
      return null
    }

    return (
      <Segment>
        <div style={{ display: 'inline-block' }}>
          <label htmlFor="line-items">Line Items*</label>
          <Button
            form="line-items"
            onClick={() => this.toggleAllLineItemExcise(toggleAllExciseOn)}
            style={{ marginLeft: 20, padding: 9, fontSize: 12 }}
          >
            {toggleAllExciseOn ? 'add excise to all' : 'remove all excise'}
          </Button>
        </div>
        <Segment.Group>
          {lineItems?.map &&
            lineItems.map((lineItem, i) => (
              <LineItem
                key={`lineitem-${i}`}
                lineItem={lineItem}
                lineItemIndex={i}
                errors={errors}
                removeLineItem={this.removeLineItem}
                onSkuBatchSearch={this.onSkuBatchSearch}
                generateSkuBatchSelectValue={this.generateSkuBatchSelectValue}
                lineItems={lineItems}
                skuBatchOptions={this.skuBatchOptions}
                handleSkuChange={this.handleSkuChange}
                isLoadingSkus={isLoadingSkus}
                isLoading={isLoading}
                handleLineItemBrandChange={this.handleLineItemBrandChange}
                handleLineItemChange={this.handleLineItemChange}
                handleIsSampleChange={this.handleIsSampleChange}
                handleExciseTaxChange={this.handleExciseTaxChange}
              />
            ))}
        </Segment.Group>
      </Segment>
    )
  }

  getLicenseOptions = () => {
    if (!this.state.site.licenses) {
      return
    }
    const { site } = this.state
    let lics = []

    if (site.licenses.length) {
      lics = site.licenses
    } else {
      if (site.licensedLocation) {
        lics = site.licensedLocation.licenses
      }
    }

    if (!lics.map) {
      return
    }

    return lics.map((lic: License) => ({
      key: lic.licenseNumber,
      value: lic.licenseNumber,
      text: `${lic.nickname} (${lic.licenseNumber})`,
    }))
  }

  renderLoading() {
    return (
      <LoadingSegment>
        <Dimmer active inverted>
          <Loader inverted />
        </Dimmer>
      </LoadingSegment>
    )
  }

  renderPage() {
    const { allLicensedLocations = {}, allWarehouses = [], newOrder } = this.props || {}
    const { me, isNabisDeliveries } = this.props
    const { isAdmin = false } = me || {}

    const { showDraftSavedPrompt = false } = this.state

    const {
      action,
      date,
      daysTillPaymentDue,
      discount,
      irn,
      isSaving,
      name,
      notes,
      site,
      organization,
      siteLicenseNum,
      timeWindow = 'ALL DAY',
      warehouseId,
      lineItems,
      surcharge,
      hasUnalterablePaymentTerms,
      mustPayPreviousBalance,
    } = this.state

    const errors = isOrderValid(this.props, this.state)

    const { subtotal, totalExciseTax, totalBalance } = calculateMoney({
      discount,
      lineItems,
      siteUsState: site.state,
      deliveryDate: date,
      surcharge: parseFloat(surcharge) || 0,
    })

    const IrnHelp = (
      <Popup
        trigger={<Help>What is this?</Help>}
        content="Optional. Provide a purchase order or sales order number to help dispensaries reconcile a Nabis shipment with your SO or PO. We put this number on the manifest if you provide it."
      />
    )

    const getLicensedLocOptions = () => {
      const licLocs = idx(allLicensedLocations, (_) => _.results) || []

      if (action === 'INTERNAL_TRANSFER') {
        if (!allWarehouses?.map) {
          return
        }

        const warehouseNames = allWarehouses.map((w) => idx(w, (_) => _.site.name))
        const filtered = licLocs.filter((ll) => warehouseNames.includes(ll.name))
        return filtered.map((_w) => ({
          key: _w.id,
          text: `${_w.name}, ${_w.address1}${_w.address2 ? `, ${_w.address2}` : ''}, ${_w.city}, ${_w.state}, ${
            _w.zip
          }`,
          value: _w.id,
        }))
      } else {
        if (!licLocs?.map) {
          return
        }

        return licLocs.map((s) => ({
          key: s.id,
          value: s.id,
          text: `${s.name}, ${s.address1}${s.address2 ? `, ${s.address2}` : ''}, ${s.city}, ${s.state}, ${s.zip}`,
        }))
      }
    }

    const actions: DropdownItemProps[] = [...ORDER_ACTION_OPTIONS].filter((option) => {
      if (isNabisDeliveries && option.key === ORDER_ACTIONS.DELIVERY_TO_RETAILER && this.props.newOrder) {
        return false
      }

      return true
    })

    // canndescent and henrys have access to payment_pickup option by request
    if (isAdmin || isCanndescent(organization) || isHenrys(organization)) {
      actions.push(...ORDER_ACTION_ADMIN_ADDITIONAL_OPTIONS)
    }

    const { isLoading } = this.state

    if (isLoading) {
      return (
        <Card fluid>
          <Card.Content>
            <Segment size="massive" basic>
              <Loader active />
            </Segment>
          </Card.Content>
        </Card>
      )
    }

    const taxRate = (getTaxRate(site.state, date) * 100).toFixed(2)
    const taxRateLabel = `Excise tax (${taxRate}% of taxable subtotal)`

    return (
      <Form id="order-form" autoComplete="off">
        {/* FOLLOWING LINE FOR DISABLING AUTOCOMPLETE/AUTOFILL */}
        <input type="hidden" value="disabled" />
        {!isEmpty(this.state.errors) && <ErrorBox errors={this.state.errors} />}
        {(isAdmin || (!isAdmin && !this.props.newOrder)) && (
          <AdminSection
            subtotal={subtotal}
            totalExciseTax={totalExciseTax}
            gmvCollected={this.state.gmvCollected}
            exciseTaxCollected={this.state.exciseTaxCollected}
            adminNotes={this.state.adminNotes}
            internalNotes={this.state.internalNotes}
            // payInFullOnClick={this.payInFullOnClick}
            status={this.state.status}
            discount={this.state.discount}
            newOrder={this.props.newOrder}
            handleInputChange={this.handleInputChange}
          />
        )}
        <Card fluid>
          <Card.Content>
            <Form.Group widths="equal">
              <Form.Field>
                <label htmlFor="name">Order Nickname or DBA (Doing Business As)</label>
                <Ref innerRef={(el) => this.props.setRef(el, 'name')}>
                  <Input
                    autoComplete="disabled"
                    name="name"
                    type="text"
                    defaultValue={name}
                    required
                    placeholder="Order #1"
                    onChange={this.onInputChange('name')}
                  />
                </Ref>
              </Form.Field>
              <Form.Field>
                <label htmlFor="irn">PO/SO Number ({IrnHelp})</label>
                <Input
                  autoComplete="disabled"
                  name="irn"
                  type="text"
                  onChange={this.onInputChange('irn')}
                  defaultValue={irn}
                />
              </Form.Field>
            </Form.Group>
            <Divider />
            <Form.Group widths="equal">
              <Form.Field>
                <label htmlFor="action">Action</label>
                <Dropdown
                  autoComplete="disabled"
                  name="action"
                  type="text"
                  value={action}
                  required
                  placeholder="Select action..."
                  onChange={this.onActionChange}
                  search
                  selection
                  options={actions}
                />
              </Form.Field>
              <Form.Field>
                <OrderFormDatePicker
                  date={date}
                  onOrderDateChange={this.handleOrderDateChange}
                  isCanndescent={isCanndescent(organization)}
                />
              </Form.Field>
              <Form.Field>
                <label htmlFor="timeWindow">Time Window</label>
                <Input
                  autoComplete="disabled"
                  name="timeWindow"
                  type="text"
                  defaultValue={timeWindow}
                  required
                  placeholder="10AM - 5PM"
                  onChange={this.onInputChange('timeWindow')}
                />
              </Form.Field>
              {action === 'DELIVERY_TO_RETAILER' && (
                <Form.Field>
                  <label htmlFor="daysTillPaymentDue">
                    Payment Terms *&nbsp;(
                    <Popup
                      trigger={<Help>Unalterable?</Help>}
                      content="Check this box if Payment Terms CANNOT be changed upon Delivery. If this is selected and a Retailer cannot pay COD, the order will be Delayed and your team will need to coordinate a new delivery date when the account can provide payment."
                    />
                    &nbsp;
                    <Checkbox
                      name="hasUnalterablePaymentTerms"
                      type="checkbox"
                      disabled={isLoading}
                      checked={hasUnalterablePaymentTerms || false}
                      onClick={() =>
                        this.onCheckboxChange('hasUnalterablePaymentTerms', !Boolean(hasUnalterablePaymentTerms))
                      }
                      style={{ minHeight: '12px' }}
                    />
                    )
                  </label>

                  <Dropdown
                    autoComplete="disabled"
                    name="daysTillPaymentDue"
                    type="text"
                    value={daysTillPaymentDue}
                    onChange={this.onInputChange('daysTillPaymentDue')}
                    placeholder={daysTillPaymentDue ? 'None (pickup or transfer)' : ''}
                    selection
                    options={[
                      { value: 0, text: 'COD' },
                      { value: 7, text: 'Net 7' },
                      { value: 14, text: 'Net 14' },
                      { value: 15, text: 'Net 15' },
                      { value: 30, text: 'Net 30' },
                      { value: 45, text: 'Net 45' },
                      { value: 60, text: 'Net 60' },
                      { value: 90, text: 'Net 90' },
                      {
                        text: 'None (pickup or transfer)',
                        value: undefined,
                      },
                    ]}
                  />
                </Form.Field>
              )}
            </Form.Group>
            <Form.Group>
              <Form.Field width={8}>
                <label htmlFor="site">
                  {action === 'PICKUP_FROM_BRAND' ? 'Pickup Location ' : 'Licensed Entity '}
                  <a href="#/" onClick={() => this.setState({ isAddSiteModalOpen: true })}>
                    add new licensed entity
                  </a>
                  {this.state?.isAddSiteModalOpen ? (
                    <AddSiteModal
                      open={this.state?.isAddSiteModalOpen}
                      onSuccess={() => this.setState({ isAddSiteModalOpen: false })}
                      onClose={() => this.setState({ isAddSiteModalOpen: false })}
                    />
                  ) : null}
                </label>
                {isLoading && <Loader active inline="centered" />}
                {!isLoading && (
                  <Dropdown
                    autoComplete="disabled"
                    name="site"
                    type="text"
                    text={
                      site && site.name
                        ? `${site.name} (${site.address1} ${site.address2 ? `, ${site.address2}` : ''}, ${site.city}, ${
                            site.state
                          }, ${site.zip})`
                        : 'Select an address'
                    }
                    value={site.id}
                    required
                    search
                    selection
                    onChange={(_e, d) => this.handleSiteChange(d?.value)}
                    loading={!allLicensedLocations}
                    options={getLicensedLocOptions()}
                  />
                )}
                <label htmlFor="address-info">
                  <a
                    href="#/"
                    onClick={() => {
                      this.setState({ isEditSiteModalOpen: true })
                    }}
                  >
                    {[ORDER_ACTIONS.PICKUP_FROM_BRAND, ORDER_ACTIONS.RETURN_FROM_RETAILER].includes(action)
                      ? 'View Pickup Location Info'
                      : 'View Licensed Entity Info'}
                  </a>
                  {this.state?.isEditSiteModalOpen && (
                    <EditSiteModal
                      site={site?.licensedLocation}
                      onSuccess={() => this.setState({ isEditSiteModalOpen: false })}
                      onClose={() => this.setState({ isEditSiteModalOpen: false })}
                    />
                  )}
                </label>
              </Form.Field>
              <Form.Field width={4}>
                <label htmlFor="siteLicenseNum">Licensed Entity's License Number</label>
                <Dropdown
                  autoComplete="disabled"
                  name="siteLicenseNum"
                  type="text"
                  text={siteLicenseNum || 'Select a license...'}
                  value={siteLicenseNum}
                  required
                  search
                  onChange={this.handleLicenseChange}
                  disabled={site && !site.id && !site.licenses}
                  options={this.getLicenseOptions()}
                />
              </Form.Field>
              {action === 'DELIVERY_TO_RETAILER' && (
                <Form.Field width={4}>
                  <label>
                    <Popup
                      trigger={
                        <Help>
                          <MustPayLabel />
                        </Help>
                      }
                      content="Check this box if the current order should NOT be delivered unless the previous outstanding invoice is paid. If this is selected and a Retailer cannot pay the outstanding invoice, the current order will be Delayed and your team will need to coordinate a new delivery date when the account can provide payment."
                    />
                    &nbsp;
                    <Checkbox
                      name="mustPayPreviousBalance"
                      type="checkbox"
                      disabled={isLoading}
                      checked={mustPayPreviousBalance || false}
                      onClick={() => this.onCheckboxChange('mustPayPreviousBalance', !Boolean(mustPayPreviousBalance))}
                      style={{ minHeight: '12px' }}
                    />
                  </label>
                </Form.Field>
              )}
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field>
                <label htmlFor="pocName">Contact Person*</label>
                <Input
                  name="pocName"
                  type="text"
                  required
                  onChange={(e) => this.handlePocChange('pocName', e.target.value)}
                  value={idx(this.state, (_) => _.site.pocName) || ''}
                  placeholder="Mrs. Order Recipient"
                  autoComplete="disabled"
                />
              </Form.Field>
              <Form.Field>
                <label htmlFor="pocPhoneNumber">Phone Number*</label>
                <Input
                  name="pocPhoneNumber"
                  type="text"
                  required
                  onChange={(e) => this.handlePocChange('pocPhoneNumber', e.target.value)}
                  value={idx(this.state, (_) => _.site.pocPhoneNumber) || ''}
                  placeholder="123-456-7890"
                  autoComplete="disabled"
                />
              </Form.Field>
              <Form.Field>
                <label htmlFor="email">Email*</label>
                <Input
                  name="email"
                  type="email"
                  required
                  onChange={(e) => this.handlePocChange('pocEmail', e.target.value)}
                  value={idx(this.state, (_) => _.site.pocEmail) || ''}
                  placeholder="name@email.com"
                  autoComplete="disabled"
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field>
                <label htmlFor="form-textarea-control-delivery-notes">Delivery Notes</label>
                <Ref innerRef={(el) => this.props.setRef(el, 'notes')}>
                  <TextArea
                    id="form-textarea-control-delivery-notes"
                    autoComplete="disabled"
                    name="notes"
                    defaultValue={notes}
                    onChange={this.onInputChange('notes')}
                  />
                </Ref>
              </Form.Field>
            </Form.Group>
            <Form.Field>
              <div>
                <WarehouseDropdown
                  action={action}
                  allWarehouses={allWarehouses}
                  handleWarehouseChange={(_warehouseId) => this.handleWarehouseChange(_warehouseId)}
                  isAdmin={isAdmin}
                  warehouseId={warehouseId}
                  isLoading={isLoading}
                />
              </div>
              {this.renderLineItems(errors)}
            </Form.Field>
            <AddLineItemContainer>
              <AddLineItem onClick={() => this.addLineItem()}>+ Add Item</AddLineItem>
            </AddLineItemContainer>
          </Card.Content>

          <Card.Content>
            <Form.Group widths="equal">
              <Form.Field>
                <label htmlFor="subtotal">Subtotal</label>
                <Input
                  autoComplete="disabled"
                  name="subtotal"
                  type="number"
                  value={subtotal.toFixed(2)}
                  required
                  min="0.00"
                  step="0.01"
                  placeholder="e.g. 10"
                  icon
                  iconPosition="left"
                  disabled
                  style={{ opacity: 1 }}
                >
                  <Icon name="dollar" />
                  <input />
                </Input>
              </Form.Field>
              <Form.Field>
                <label htmlFor="discount">Discount off subtotal ($)</label>
                <Input
                  autoComplete="disabled"
                  name="discount"
                  type="number"
                  defaultValue={discount}
                  required
                  onChange={this.onInputChange('discount')}
                  min="0.00"
                  max={subtotal.toFixed(2)}
                  step="0.01"
                  placeholder="e.g. 10"
                  icon
                  iconPosition="left"
                  style={{ opacity: 1 }}
                >
                  <Icon name="dollar" />
                  <input />
                </Input>
              </Form.Field>
              <Form.Field>
                <label htmlFor="exciseTax">{taxRateLabel}</label>
                <Input
                  autoComplete="disabled"
                  name="exciseTax"
                  type="number"
                  value={totalExciseTax.toFixed(2)}
                  required
                  min="0.00"
                  step="0.01"
                  placeholder="e.g. 10"
                  icon
                  iconPosition="left"
                  disabled
                  style={{ opacity: 1 }}
                >
                  <Icon name="dollar" />
                  <input />
                </Input>
              </Form.Field>
              <Form.Field>
                <label htmlFor="totalBalance">Total Balance Due</label>
                <Input
                  autoComplete="disabled"
                  name="totalBalance"
                  type="number"
                  value={totalBalance.toFixed(2)}
                  required
                  min="0.00"
                  step="0.01"
                  placeholder="e.g. 10"
                  icon
                  iconPosition="left"
                  disabled
                  style={{ opacity: 1 }}
                >
                  <Icon name="dollar" />
                  <input />
                </Input>
              </Form.Field>
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Field />
              <Form.Field>
                <label htmlFor="surcharge">Surcharge on subtotal ($)</label>
                <Input
                  autoComplete="disabled"
                  name="surcharge"
                  type="number"
                  defaultValue={surcharge}
                  required
                  onChange={this.onInputChange('surcharge')}
                  min="0.00"
                  step="0.01"
                  placeholder="e.g. 10"
                  icon
                  iconPosition="left"
                  style={{ opacity: 1 }}
                >
                  <Icon name="dollar" />
                  <input />
                </Input>
              </Form.Field>
              <Form.Field />
              <Form.Field />
            </Form.Group>
            <OrderFormTooltip errors={errors} />
          </Card.Content>
        </Card>
        <SubmitContainer>
          {newOrder && (
            <ConfirmModal
              onClose={() => this.toggleAlert(false)}
              onSuccess={() => this.toggleAlert(true)}
              open={this.state.open}
              trigger={
                <Button
                  title={showDraftSavedPrompt ? 'Saved!' : 'Save Draft'}
                  name="save outline"
                  form="order-form"
                  disabled={isSaving}
                  loading={showDraftSavedPrompt ? isSaving : showDraftSavedPrompt}
                  size="big"
                  onClick={() => this.saveButtonOnClick()}
                  fluid
                />
              }
            />
          )}
          <Button
            form="order-form"
            positive
            size="big"
            disabled={errors.length > 0 || isSaving}
            onClick={(e) => this.nextButtonOnClick(e)}
            fluid
          >
            Next
          </Button>
        </SubmitContainer>
      </Form>
    )
  }

  render() {
    const orderNotYetLoaded = isEqual(this.state.order, getDefaultOrder(this.props.isNabisDeliveries))
    if ((this.state.orderId && orderNotYetLoaded) || (this.state.draftId && orderNotYetLoaded)) {
      return this.renderLoading()
    }
    return this.renderPage()
  }
}

export default compose(OrderFormApollo, withRouter, withOrganizationHook, withMeHook)(OrderForm)
