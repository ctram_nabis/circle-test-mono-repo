import { Text } from 'components/core/src/Text'
// import { TextButton } from 'components/TextButton'

import { ORDER_STATUS_OPTIONS } from 'modules/Order/constants/OrderStatusOptions'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import { Card, Dropdown, Form, Icon, Input, Label, TextArea } from 'semantic-ui-react'
import { dollarFormat } from 'utils'

export const AdminSection = ({
  subtotal,
  totalExciseTax,
  gmvCollected,
  exciseTaxCollected,
  adminNotes,
  internalNotes,
  // payInFullOnClick,
  status,
  discount,
  newOrder,
  handleInputChange,
}) => {
  const { isAdmin } = useMe()
  const [_adminNotes, setAdminNotes] = useState(adminNotes)
  const [_internalNotes, setInternalNotes] = useState(internalNotes)
  const [_status, setStatus] = useState(status)
  const [_exciseTaxCollected, setExciseTax] = useState(exciseTaxCollected)
  const [_gmvCollected, setGMVCollected] = useState(gmvCollected)

  const _discount = parseFloat(discount) || 0.0

  return (
    <Card fluid>
      <Label color="green">Admin Only</Label>
      <Card.Content>
        {/* HIDDEN WHEN CREATING NEW ORDER / SHOWN WHEN EDITING EXISTING ORDER;
            creating new order has gmvCollected, exciseTaxCollected values set to 0 + status set to `UNSCHEDULED`
            anyways (in OrderConfirm.react.js)
          */}
        {!newOrder && (
          <>
            {/* TODO: Paid in Full is broken (CalculateMoney issue). Delete below if no one complains about the
            TODO: missing feature. today date 12/24/19 - Miranda Howitt */}
            {/* <Form.Group
              style={{
                alignItems: 'center',
                display: 'flex',
                justifyContent: 'space-between',
              }}
            >
              <Form.Field>
                <TextButton title="Paid in Full?" color="green" onClick={payInFullOnClick} type="button" />
              </Form.Field>
            </Form.Group> */}
            <Form.Group widths="equal">
              <Form.Field>
                <label htmlFor="status">Order Status</label>
                <Dropdown
                  autoComplete="disabled"
                  name="status"
                  type="text"
                  defaultValue={_status}
                  required
                  placeholder="Select status..."
                  onChange={(_e, d) => setStatus(d.value)}
                  onBlur={() => handleInputChange('status', _status)}
                  search
                  selection
                  options={ORDER_STATUS_OPTIONS}
                  disabled={!isAdmin}
                />
              </Form.Field>
              <Form.Field>
                <label htmlFor="gmvCollected">
                  GMV Collected&nbsp;
                  <Text fontWeight={100} display="inline-block">
                    (Subtotal - Discount = {dollarFormat((parseFloat(subtotal) - _discount).toFixed(2))})
                  </Text>
                </label>
                <Input
                  autoComplete="disabled"
                  name="gmvCollected"
                  type="number"
                  defaultValue={_gmvCollected}
                  min="0.00"
                  step="0.01"
                  icon
                  iconPosition="left"
                  placeholder="100"
                  onChange={(_e, d) => setGMVCollected(d.value)}
                  onBlur={() => handleInputChange('gmvCollected', _gmvCollected)}
                  disabled={!isAdmin}
                >
                  <Icon name="dollar" />
                  <input />
                </Input>
              </Form.Field>
              <Form.Field>
                <label htmlFor="exciseTaxCollected">
                  Excise Tax Collected&nbsp;
                  <Text fontWeight={100} display="inline-block">
                    (Invoiced Excise Tax = {dollarFormat(totalExciseTax.toFixed(2))})
                  </Text>
                </label>
                <Input
                  autoComplete="disabled"
                  name="exciseTaxCollected"
                  type="number"
                  defaultValue={_exciseTaxCollected}
                  min="0.00"
                  step="0.01"
                  icon
                  iconPosition="left"
                  placeholder="24"
                  onChange={(_e, d) => setExciseTax(d.value)}
                  onBlur={() => handleInputChange('exciseTaxCollected', _exciseTaxCollected)}
                  disabled={!isAdmin}
                >
                  <Icon name="dollar" />
                  <input />
                </Input>
              </Form.Field>
            </Form.Group>
          </>
        )}

        <Form.Group widths="equal">
          <Form.Field>
            <label htmlFor="form-textarea-control-admin-notes">Admin Notes</label>
            <TextArea
              id="form-textarea-control-admin-notes"
              autoComplete="disabled"
              name="adminNotes"
              defaultValue={_adminNotes}
              placeholder="Additional info..."
              onChange={(_e, d) => setAdminNotes(d.value)}
              onBlur={() => handleInputChange('adminNotes', _adminNotes)}
              disabled={!isAdmin}
            />
          </Form.Field>
          {isAdmin && (
            <Form.Field>
              <label htmlFor="form-textarea-control-internal-notes">Internal Notes (Nabis Only)</label>
              <TextArea
                id="form-textarea-control-internal-notes"
                autoComplete="disabled"
                name="internalNotes"
                defaultValue={_internalNotes}
                onChange={(_e, d) => setInternalNotes(d.value)}
                onBlur={() => handleInputChange('internalNotes', _internalNotes)}
                // disabled={!isAdmin}x
              />
            </Form.Field>
          )}
        </Form.Group>
      </Card.Content>
    </Card>
  )
}
