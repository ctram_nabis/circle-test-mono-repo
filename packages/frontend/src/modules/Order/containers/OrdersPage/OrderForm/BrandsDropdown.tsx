import { LineItem, useAccessibleBrandsQuery } from 'generated/graphql'
import { useOrganization } from 'modules/Organization'
import React from 'react'
import { Dropdown, DropdownItemProps, Form } from 'semantic-ui-react'
interface Props {
  lineItem: LineItem
  handleLineItemBrandChange: (brand: any, lineItemIndex: number) => void
  lineItemIndex: number
}
export const BrandsDropdown = ({ lineItem, handleLineItemBrandChange, lineItemIndex }: Props) => {
  const { organization } = useOrganization()
  const { data, error, loading } = useAccessibleBrandsQuery({
    variables: {
      orgId: organization.id,
    },
  })
  const accessibleBrands = data?.viewer?.accessibleBrands

  const options = (accessibleBrands || []).map<DropdownItemProps>((brand) => ({
    value: brand.id,
    text: brand.name,
  }))

  return !loading && !error && accessibleBrands && organization.alias === 'nabione-inc-deliveries' ? (
    <Form.Group widths="equal">
      <Form.Field>
        <label htmlFor="lineItem.organization">Brand</label>
        <Dropdown
          key="brand-selection-order-form"
          autoComplete="disabled"
          name="lineItem.organization"
          options={options}
          type="text"
          required
          placeholder="Please, select a brand..."
          selection
          search
          onChange={(_e, d) => handleLineItemBrandChange(d.value, lineItemIndex)}
          value={lineItem?.organization?.id}
          text={lineItem?.organization?.name}
        />
      </Form.Field>
    </Form.Group>
  ) : null
}
