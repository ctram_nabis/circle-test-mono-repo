import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import React from 'react'
import { Checkbox, Dropdown, Form, Header, Icon, Input, Label, Segment } from 'semantic-ui-react'
import styled from 'styled-components'
import { colors } from 'system/theme'
import { BrandsDropdown } from './BrandsDropdown'
import { LineItemError } from './LineItemError'
// import { LineItemEntities } from './LineItemEntities'
const Exit = styled.div`
  color: ${colors.text};
  font-size: 18px;
  line-height: 18px;
  opacity: 0.4;
  font-family: 'Avenir-Book', san-serif;
  cursor: pointer;
  position: absolute;
  right: 5px;
  top: 0px;
  padding: 10px;
  transition: all 0.2s ease-in;
  user-select: none;
  z-index: 10000;
  &:hover {
    opacity: 1;
  }
`
export const LineItem = ({
  errors,
  removeLineItem,
  lineItem,
  lineItemIndex,
  onSkuBatchSearch,
  generateSkuBatchSelectValue,
  lineItems,
  skuBatchOptions,
  handleSkuChange,
  isLoadingSkus,
  isLoading,
  handleLineItemBrandChange,
  handleLineItemChange,
  handleIsSampleChange,
  handleExciseTaxChange,
}) => {
  const { unit, isSample, isExciseTaxable, pricePerUnit, quantity, discount } = lineItem
  const lineItemErrors = errors.filter((message) => message.includes(`LineItem ${lineItemIndex + 1}:`))

  return (
    <Segment tertiary style={{ background: `${colors.shade1}` }}>
      {lineItemIndex === 0 && lineItems.length < 2 ? null : (
        <Exit onClick={() => removeLineItem(lineItemIndex)}>x</Exit>
      )}
      <Box flexDirection="row" space="between">
        <Header as="h5">Line Item {lineItemIndex + 1}:</Header>
        {/* <LineItemEntities lineItem={lineItem} /> */}
      </Box>

      <BrandsDropdown
        lineItemIndex={lineItemIndex}
        handleLineItemBrandChange={handleLineItemBrandChange}
        lineItem={lineItem}
      />
      <Form.Group>
        <Form.Field width={8}>
          <label htmlFor="lineItem">SKU</label>
          <Dropdown
            autoComplete="disabled"
            name="lineItem"
            type="text"
            text={
              lineItem.name && lineItem.skuBatch
                ? generateSkuBatchSelectValue(lineItem.skuBatch)
                : 'Please, search for a SKU...'
            }
            value={lineItem}
            required
            search
            disabled={isLoadingSkus}
            loading={isLoadingSkus}
            onSearchChange={(e, d) => onSkuBatchSearch(e, d, lineItem)}
            // onClick={(e) => this.onSkuBatchSearch(e, { searchQuery: '' }, lineItem)}
            onChange={(_e, d) => handleSkuChange(lineItemIndex, _e, d?.value)}
            options={skuBatchOptions(lineItems)}
            selection
          />
        </Form.Field>
        <Form.Field width={4}>
          <label htmlFor="quantity">
            Quantity
            {!!lineItemErrors.length && (
              <LineItemError lineItemErrors={lineItemErrors} errors={errors} lineItemIndex={lineItemIndex} />
            )}
          </label>
          <Input
            autoComplete="disabled"
            name="quantity"
            type="number"
            min="1"
            step="1"
            value={quantity}
            disabled={isLoading}
            loading={isLoading}
            required
            placeholder="e.g. 100"
            onChange={handleLineItemChange(lineItemIndex)}
          />
        </Form.Field>
        <Form.Field width={4}>
          <label htmlFor="pricePerUnit">
            Price Per Unit &nbsp;
            <Text fontWeight={100} display="inline-block">
              (Sample?&nbsp;
              <Checkbox
                name="isSample"
                type="checkbox"
                disabled={isLoading}
                checked={isSample || false}
                onClick={handleIsSampleChange(lineItemIndex)}
                style={{ minHeight: '12px' }}
              />
              )
            </Text>
          </label>
          <Input
            autoComplete="disabled"
            name="pricePerUnit"
            type="number"
            value={pricePerUnit || 0}
            required
            disabled={isLoading}
            loading={isLoading}
            min="0.01"
            step="0.01"
            placeholder="e.g. 11.99"
            onChange={handleLineItemChange(lineItemIndex)}
            icon
            iconPosition="left"
            labelPosition="right"
          >
            <Icon name="dollar" />
            <input />
            <Label>/ {unit || 'unit'}</Label>
          </Input>
          {isSample && (
            <Text bold color="red">
              * NOT FOR RESALE *
            </Text>
          )}
        </Form.Field>
      </Form.Group>
      <Form.Group>
        <Form.Field width={4}>
          <label htmlFor="subtotal">Subtotal</label>
          <Input
            autoComplete="disabled"
            name="subtotal"
            type="number"
            disabled
            loading={isLoading}
            value={(quantity * pricePerUnit).toFixed(2)}
            required
            min="0.00"
            step="0.01"
            placeholder="e.g. 11.99"
            icon
            iconPosition="left"
            style={{ opacity: 1 }}
          >
            <Icon name="dollar" />
            <input />
          </Input>
        </Form.Field>
        <Form.Field width={4}>
          <label htmlFor="discount">Discount Total</label>
          <Input
            autoComplete="disabled"
            name="discount"
            type="number"
            value={discount}
            required
            disabled={isLoading}
            loading={isLoading}
            step="0.01"
            placeholder="e.g. 11.99"
            onChange={handleLineItemChange(lineItemIndex)}
            icon
            iconPosition="left"
            min={0}
            max={(quantity * pricePerUnit).toFixed(2)}
          >
            <Icon name="dollar" />
            <input />
          </Input>
        </Form.Field>
        <Form.Field width={4}>
          <label htmlFor="lineItemTotal">Line Item Total</label>
          <Input
            autoComplete="disabled"
            name="lineItemTotal"
            type="number"
            disabled
            loading={isLoading}
            value={(quantity * pricePerUnit - (discount || 0)).toFixed(2)}
            required
            min="0.00"
            step="0.01"
            placeholder="e.g. 11.99"
            icon
            iconPosition="left"
            style={{ opacity: 1 }}
          >
            <Icon name="dollar" />
            <input />
          </Input>
        </Form.Field>
        <Form.Field width={4}>
          <label htmlFor="isExciseTaxable">Excise Tax?</label>
          <Checkbox
            name="isExciseTaxable"
            type="checkbox"
            checked={isExciseTaxable || false}
            onClick={handleExciseTaxChange(lineItemIndex)}
          />
        </Form.Field>
      </Form.Group>
    </Segment>
  )
}
