import React from 'react'
import { Icon, Popup } from 'semantic-ui-react'
import styled from 'styled-components'

const ErrorMessage = styled.em`
  color: red;
  font-size: 0.85em;
`

export const OrderFormTooltip = ({ errors }) => {
  const color = errors.length > 0 ? 'red' : 'green'
  const icon = errors.length > 0 ? 'exclamation circle' : 'check circle'
  const text = errors.length > 0 ? 'Order Incomplete' : 'Ready to submit'
  return (
    <Popup
      flowing
      trigger={
        <div>
          <Icon
            color={color}
            name={icon}
            style={{
              display: 'inlineBlock',
              verticalAlign: 'middle',
              marginBottom: '6px',
            }}
          />
          <em
            style={{
              color: `${errors.length > 0 ? 'red' : 'green'}`,
              fontSize: '.85em',
              display: 'inlineBlock',
              verticalAlign: 'middle',
            }}
          >
            {text}
          </em>
        </div>
      }
    >
      {errors.length > 0 && errors.map ? (
        <Popup.Content>
          <strong>Order has the following errors:</strong>
          {errors.map((e, i) => {
            return (
              <div key={`error_div_${i}`}>
                <ErrorMessage
                  style={{
                    color: 'red',
                    fontSize: '.85em',
                  }}
                >
                  {e}
                </ErrorMessage>
              </div>
            )
          })}
        </Popup.Content>
      ) : (
        <Popup.Content>
          <p>
            Click <u>next</u> and then click <u>submit order</u> to activate this order
          </p>
        </Popup.Content>
      )}
    </Popup>
  )
}
