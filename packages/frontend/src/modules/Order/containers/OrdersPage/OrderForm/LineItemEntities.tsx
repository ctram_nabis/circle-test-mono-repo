import { Box } from 'components/core/src/Box'
import { LineItem } from 'generated/graphql'
import { TITLEHOLDER_LABEL } from 'modules/SkuBatch/constants'
import React from 'react'

interface Props {
  lineItem: LineItem
}

export const LineItemEntities = ({ lineItem }: Props) => {
  const manufacturerText =
    lineItem?.skuBatch?.batch?.license &&
    `${lineItem?.skuBatch?.batch?.license?.nickname} (${lineItem?.skuBatch?.batch?.license?.licenseNumber})`

  if (!manufacturerText) return null

  return (
    <Box flexDirection="row">
      <label style={{ marginRight: 20 }}>
        {TITLEHOLDER_LABEL}: {manufacturerText}
      </label>
      <label style={{ marginRight: 20 }}>Manufacturer: {manufacturerText}</label>
    </Box>
  )
}
