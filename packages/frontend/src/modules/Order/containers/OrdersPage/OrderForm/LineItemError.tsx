import { Box } from 'components/core/src/Box'
import React from 'react'
import { Icon, Popup } from 'semantic-ui-react'
import styled from 'styled-components'
const OversoldPopupContent = styled(Popup.Content)`
  color: red !important;
`
const InlineIcon = styled(Icon)`
  display: 'inlineBlock';
  vertical-align: 'middle';
  margin-bottom: '6px';
`
const ErrorList = styled.ul`
  margin: 0;
  padding: 3px 0 0 0;
  list-style-type: none;
`

const ErrorMessage = styled.em`
  color: red;
  font-size: 0.85em;
`

export const LineItemError = ({ lineItemErrors, errors, lineItemIndex }) => {
  const [formattedErrors] = lineItemErrors.map((error) => error.split(': ')[1].split(', '))

  return (
    formattedErrors && (
      <Popup
        flowing
        trigger={
          <Box display="inline-block">
            <InlineIcon color="red" name="exclamation circle" />
          </Box>
        }
      >
        <OversoldPopupContent>
          {formattedErrors.map(
            (errorMessage, i) =>
              errorMessage.length > 0 && (
                <ErrorList key={`error_${i}`}>
                  <li>
                    <ErrorMessage>{errorMessage}</ErrorMessage>
                  </li>
                </ErrorList>
              ),
          )}
        </OversoldPopupContent>
      </Popup>
    )
  )
}
