import { ApolloQueryResult } from 'apollo-boost'
import { Container, PageHeader, S } from 'components'
import { GetDraftDocument, GetOrderDocument } from 'generated/graphql'
import idx from 'idx'
import cloneDeep from 'lodash/cloneDeep'
import omit from 'lodash/omit'
import pick from 'lodash/pick'
import { client } from 'modules/Apollo'
import { ORDER_ACTIONS } from 'modules/Order/constants'
import { withOrganizationHook } from 'modules/Order/helpers/withOrganizationHook'
import { withMeHook } from 'modules/User/helpers/withMeHook'
import React from 'react'
import { withRouter } from 'react-router'
import { compose } from 'recompose'
import { Grid, GridColumn, GridRow, Label, Loader, Step, StepGroup } from 'semantic-ui-react'
import styled from 'styled-components'
import { titleize } from 'utils'
import uuid from 'uuid/v4'
import OrderConfirm from './OrderConfirm'
import OrderForm from './OrderForm/OrderForm'
import ReadOnlyOrder from './ReadOnlyOrder'
import { calculateMoney } from '../../helpers/calculateMoney'

const StatusLabelContainer = styled.span`
  float: right;
`

const StepContainer = styled.div`
  margin-bottom: 20px;
`

const SubHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const NEW_ORDER_STEPS = {
  CONFIRM: 1,
  FORM: 0,
}

export const getDefaultOrder = (isNabisDeliveries: boolean) => ({
  action: isNabisDeliveries ? ORDER_ACTIONS.INTERNAL_TRANSFER : ORDER_ACTIONS.DELIVERY_TO_RETAILER,
  adminNotes: '',
  internalNotes: '',
  confirm: false,
  discount: 0,
  errors: [],
  exciseTaxCollected: 0,
  gmvCollected: 0,
  isSaving: false,
  open: false,
  site: {},
  status: 'UNSCHEDULED',
  subtotal: 0,
  timeWindow: 'ALL DAY',
  toggleAllExciseOn: false,
  totalExciseTax: 0,
  surcharge: 0,
  hasUnalterablePaymentTerms: false,
  mustPayPreviousBalance: false,
})

// interface Props {
//   // ...props
//   newOrder: Boolean
//   editOrder: Boolean
// }

export const pickOrderFromState = ({ state, subtotal, totalExciseTax }) =>
  pick(
    {
      ...cloneDeep(state),
      exciseTax: totalExciseTax,
      gmv: subtotal,
    },
    [
      'action',
      'adminNotes',
      'date',
      'daysTillPaymentDue',
      'discount',
      'exciseTax',
      'exciseTaxCollected',
      'gmv',
      'gmvCollected',
      'internalNotes',
      'irn',
      'lineItems',
      'licensedLocation',
      'licensedLocationId',
      'name',
      'notes',
      'orderSubscribers',
      'organization',
      'organizationId',
      'orgLicenseNum',
      'site',
      'siteCategory',
      'siteId',
      'siteLicenseNum',
      'status',
      'timeWindow',
      'warehouse',
      'warehouseId',
      'surcharge',
      'hasUnalterablePaymentTerms',
      'mustPayPreviousBalance',
    ], // TODO: replace with interface
  )

const fieldRefs = {
  adminNotes: null,
  internalNotes: null,
  name: null,
  notes: null,
}

const setRef = (el, key) => {
  fieldRefs[key] = el
}

class AddEditOrder extends React.Component<any, any> {
  constructor(props) {
    super(props)

    const { history } = props
    const orderId = history.location?.state?.orderId || ''
    const draftId = history.location?.state?.draftId || ''
    const editDraft = history.location?.state?.editDraft
    const copyOrder = history.location?.state?.copyOrder

    const organization = props?.organization

    this.state = {
      copyOrder,
      draftId,
      editDraft,
      order: getDefaultOrder(props.isNabisDeliveries),
      orderId,
      step: NEW_ORDER_STEPS.FORM,
      upsertId: uuid(),
      organization,
    }
  }

  async componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      const { copyOrder, order, editDraft, draftId } = this.state
      const currentUrl = window.location.href
      const orderId = currentUrl.split('orders/')[1].replace(/#|\//g, '')

      let existingOrderResponse: ApolloQueryResult<any> | undefined
      let existingOrder

      let existingDraftResponse: ApolloQueryResult<any> | undefined
      let existingDraft

      // TODO: Review: This could probably be replaced with createOrder boolean in state
      if (orderId !== 'new') {
        existingOrderResponse = await client.query({
          query: GetOrderDocument,
          variables: {
            id: orderId,
          },
        })

        existingOrder = existingOrderResponse?.data?.getOrder
      } else if (copyOrder) {
        existingOrder = this.props.data?.getOrder
      }

      if (editDraft) {
        existingDraftResponse = await client.query({
          query: GetDraftDocument,
          variables: {
            id: draftId,
          },
        })

        existingDraft = existingDraftResponse?.data?.getDraft?.body
        // copying a draft
      } else if (!editDraft && idx(this.props, (_) => _.data.getDraft.body)) {
        existingDraft = cloneDeep(this.props.data.getDraft.body)
      }

      if (existingDraft === order || existingOrder === order) {
        return
      }

      if (existingDraft && existingDraft !== order) {
        this.updateOrderFromDraft(existingDraft)
      } else if (existingOrder && existingOrder !== order) {
        if (copyOrder) {
          delete existingOrder.id
          delete existingOrder.number
          delete existingOrder.adminNotes
          existingOrder.status = 'UNSCHEDULED'
        }

        this.setState({
          order: existingOrder,
          originalOrder: cloneDeep(existingOrder),
        })
      }
    }
  }

  getStatusLabelColor() {
    const { order } = this.state
    const status = order ? order.status : 'UNSCHEDULED'

    switch (status) {
      case 'UNSCHEDULED':
        return 'grey'
      case 'IN_PROGRESS':
        return 'yellow'
      case 'COMPLETE':
        return 'green'
      default:
        return 'grey'
    }
  }

  getNewOrderStep = () => {
    const { step } = this.state
    const OrderComponent: any = step === NEW_ORDER_STEPS.CONFIRM ? OrderConfirm : OrderForm

    return (
      <OrderComponent
        {...this.props}
        {...this.state}
        onUpdateOrder={this.onUpdateOrder}
        fieldRefs={fieldRefs}
        setRef={setRef}
      />
    )
  }

  changeNewOrderStep = (step) => this.setState({ step })

  onUpdateOrder = (newData, step) => {
    const { order: oldData } = this.state
    const order = { ...oldData, ...newData }
    this.setState({ order, step })
  }

  // if provided a draftId in props.location.state will fetch and load draft.body
  updateOrderFromDraft = async (order: any = {}) => {
    const { discount, lineItems, orderSubscribers, organization, site, date: deliveryDate, surcharge } = order
    const { subtotal, totalExciseTax } = calculateMoney({
      lineItems,
      discount,
      siteUsState: site.state,
      deliveryDate,
      surcharge: parseFloat(surcharge) || 0,
    })

    const orderProps = pickOrderFromState({
      state: cloneDeep(order),
      subtotal,
      totalExciseTax,
    })

    if (lineItems) {
      const lineItemsWithSkuBatch = lineItems.filter((lineItem) => Boolean(lineItem.skuBatch))
      let refreshedSkuBatchResponses

      if (lineItemsWithSkuBatch.map) {
        refreshedSkuBatchResponses = await Promise.all(
          lineItemsWithSkuBatch.map(({ skuBatch: { id } }) =>
            client.query({
              query: this.props.getSkuBatch,
              variables: { id },
            }),
          ),
        )
      }

      let refreshedSkuBatches
      if (refreshedSkuBatchResponses) {
        refreshedSkuBatches = refreshedSkuBatchResponses.map((response: any) => response?.data?.getSkuBatch)

        orderProps.lineItems = lineItems.map(({ id, __typename, skuBatch, ...lineItem }) => ({
          ...lineItem,
          skuBatch: refreshedSkuBatches.find((refreshedSkuBatch) => skuBatch?.id === refreshedSkuBatch?.id),
          skuBatchId: skuBatch?.id,
        }))
      }
    }

    if (orderSubscribers) {
      orderProps.orderSubscribers = orderSubscribers.map((os) => omit(os, ['__typename']))
    }

    if (organization) {
      orderProps.organization = omit(organization, ['__typename'])
    }

    if (site) {
      orderProps.site = omit(site, ['__typename'])
      if (site.licensedLocation) {
        orderProps.site.licensedLocationId = site.licensedLocation.id
      }

      if (site.licenses) {
        orderProps.site.licenses = site.licenses.map((li) => omit(li, ['__typename']))
      }
    }

    this.setState({ order: orderProps })
  }

  updateManifestOnClick = async () => {
    const { orderId } = this.state
    this.setState({ isGeneratingManifest: true })
    await this.props.generateManifest(orderId)
    this.setState({ isGeneratingManifest: false })
  }

  render() {
    const { data, newOrder, me } = this.props
    const { step, order } = this.state
    const { FORM, CONFIRM } = NEW_ORDER_STEPS

    const { loading = false } = data || {}

    if (loading) {
      return (
        <Container>
          <S.Container>
            <Loader active inline="centered" />
          </S.Container>
        </Container>
      )
    }

    const isExistingOrder = !!order.id
    const isEditable = me?.isAdmin

    return (
      <Container>
        <S.Container>
          {isExistingOrder && !isEditable ? (
            <ReadOnlyOrder order={order} />
          ) : (
            <Grid columns={1} padded>
              <GridRow>
                <GridColumn>
                  <PageHeader>
                    <span>{newOrder ? 'New' : 'Edit'} Order</span>
                    {isExistingOrder && (
                      <StatusLabelContainer>
                        <Label color={this.getStatusLabelColor()}>
                          Order Status
                          <Label.Detail>{titleize(order.status)}</Label.Detail>
                        </Label>
                      </StatusLabelContainer>
                    )}
                  </PageHeader>
                </GridColumn>
              </GridRow>
              {isExistingOrder && (
                <Grid.Row>
                  <Grid.Column>
                    <SubHeader>
                      <Label>Order #{order.number}</Label>
                    </SubHeader>
                  </Grid.Column>
                </Grid.Row>
              )}
              <GridRow>
                <GridColumn>
                  <StepContainer style={{ textAlign: 'left' }}>
                    <StepGroup ordered fluid>
                      <Step
                        active={step === FORM}
                        completed={step === CONFIRM}
                        onClick={() => this.changeNewOrderStep(FORM)}
                      >
                        <Step.Content>
                          <Step.Title>Order Form</Step.Title>
                          <Step.Description>Enter your order details</Step.Description>
                        </Step.Content>
                      </Step>

                      <Step active={step === CONFIRM}>
                        <Step.Content>
                          <Step.Title>Confirm</Step.Title>
                          <Step.Description>Review order & ship</Step.Description>
                        </Step.Content>
                      </Step>
                    </StepGroup>
                  </StepContainer>
                  {this.getNewOrderStep()}
                </GridColumn>
              </GridRow>
            </Grid>
          )}
        </S.Container>
      </Container>
    )
  }
}

export default compose(withRouter, withMeHook, withOrganizationHook)(AddEditOrder)
