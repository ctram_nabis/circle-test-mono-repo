import gql from 'graphql-tag'
import React from 'react'
import { graphql } from 'react-apollo'
import { Button, Header, Icon, Modal } from 'semantic-ui-react'

class SendOrderStatusUpdateModal extends React.Component<any, any> {
  constructor(props) {
    super(props)

    this.state = {
      isOpen: false,
      email: props.email,
      loading: false,
    }
  }

  toggleSendOrderStatusUpdateModal(isOpen = false) {
    this.setState({
      isOpen,
    })
  }

  goToAdmin(e) {
    return this.props.handleEdit(e).then((res) => {
      if (res && res.data) {
        // return browserHistory.push(`/${this.props.params.alias}/app/admin`)
        return this.props.navigation.navigate(`/${this.props.params.alias}/app/admin`)
      }
    })
  }

  sendEmail(e) {
    const { id } = this.props.email
    return this.props.sendSendGridStatusUpdateEmail({ orderId: id }).then((res) => {
      if (res && res.data) {
        return this.goToAdmin(e)
      }
    })
  }

  render() {
    const { isOpen } = this.state

    return (
      <Modal
        open={isOpen}
        onOpen={() => this.toggleSendOrderStatusUpdateModal(true)}
        onClose={() => this.toggleSendOrderStatusUpdateModal(false)}
        trigger={this.props.trigger}
        // closeIcon
        closeOnDimmerClick={false}
      >
        <Header icon="mail" content="Send Order Status Update Email" />
        <Modal.Content>
          <p>Would you like to send an email update to the customer?</p>
        </Modal.Content>
        <Modal.Actions>
          <Button
            loading={this.state.loading}
            disabled={this.state.loading}
            onClick={() => this.toggleSendOrderStatusUpdateModal(false)}
          >
            <Icon name="remove" /> Cancel
          </Button>
          <Button
            color="yellow"
            loading={this.state.loading}
            disabled={this.state.loading}
            onClick={(e) => {
              this.setState({ loading: true })
              this.goToAdmin(e)
            }}
          >
            Save & Return
          </Button>
          <Button
            color="green"
            loading={this.state.loading}
            disabled={this.state.loading}
            onClick={(e) => {
              this.setState({ loading: true })
              this.sendEmail(e)
            }}
          >
            <Icon name="checkmark" /> Send Email
          </Button>
        </Modal.Actions>
      </Modal>
    )
  }
}

const SendSendGridStatusUpdateEmail = gql`
  mutation SendEmail($input: SendSendGridStatusUpdateInput!) {
    sendSendGridStatusUpdateEmail(input: $input) {
      success
    }
  }
`

const SendOrderStatusUpdateModalWithData = graphql<any, any>(SendSendGridStatusUpdateEmail, {
  props: ({ mutate }) =>
    ({
      sendSendGridStatusUpdateEmail: (input) => {
        if (mutate) {
          return mutate({ variables: { input } })
        }

        return Promise.resolve()
      },
    } as any),
})(SendOrderStatusUpdateModal)

export default SendOrderStatusUpdateModalWithData
