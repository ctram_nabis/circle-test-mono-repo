import { Button } from 'components/Button'
import { S } from 'components/SemanticComponents'
import { Text } from 'components/core/src/Text'
import { OrderChange } from 'generated/graphql'
import includes from 'lodash/includes'
import moment from 'moment-timezone'
import React from 'react'
import styled from 'styled-components'
import { AddedShipmentContents } from './AddedShipmentContents'
import { RemovedShipmentContents } from './RemovedShipmentContents'
import { UpdatedShipmentContents } from './UpdatedShipmentContents'
// TODO: REPLACE WITH CHRIS COMPONENT ONCE MERGED
const TextButton = styled(Button)`
  &.ui.button.basic.black {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    margin-left: 5px;
    text-decoration: underline;
  }
`

const TriggerText = styled(Text)`
  text-decoration: underline;
  cursor: pointer;
  white-space: nowrap;
  overflow: hidden;
  width: 200px;
  text-overflow: ellipsis;
  display: inline-block;
`

const PopupText = styled(Text)`
  white-space: wrap;
  display: inline-block;
  overflow-wrap: break-word;
`
interface Props {
  change?: Partial<OrderChange> | null
  expandedIndexes?: number[]
  i: number
  toggleDetails: (i: number) => void
}

export const OrderChangelogTableRow = ({ change, expandedIndexes, toggleDetails, i }: Props) => {
  const pst = 'America/Los_Angeles'
  const submitter = change?.submitter
  const isThereSubmitter = !submitter
  const addedLineItems = change?.addedLineItems || []
  const removedLineItems = change?.removedLineItems || []
  const createdAt = change?.createdAt
  const submitterToDisplay = isThereSubmitter
    ? 'N/A'
    : `${submitter?.firstName} ${submitter?.lastName} (${submitter?.email?.replace(/^[^@]+/, '') || ''})`

  const areThereItemChanges = Boolean(addedLineItems.length || removedLineItems.length)

  const MoreDetails = Boolean(addedLineItems.length && removedLineItems.length) ? (
    <UpdatedShipmentContents addedLineItems={addedLineItems} removedLineItems={removedLineItems} />
  ) : Boolean(addedLineItems.length && !removedLineItems.length) ? (
    <AddedShipmentContents addedLineItems={addedLineItems} />
  ) : (
    <RemovedShipmentContents removedLineItems={removedLineItems} />
  )

  const expanded = includes(expandedIndexes, i)

  const getAdminNotesStrings = (change?.description || '').split('%••%')
  const [oldNotes, newNotes] = [...getAdminNotesStrings.slice(2)]
  const changeIsAdminNotes = change?.description?.includes('%••%AdminNotes%••%')

  return (
    <S.Table.Row>
      <S.Table.Cell>
        {moment(createdAt)
          .tz(pst)
          .format('lll')}
      </S.Table.Cell>
      <S.Table.Cell>{submitterToDisplay}</S.Table.Cell>
      <S.Table.Cell>
        {changeIsAdminNotes && (
          <S.Popup
            flowing
            hoverable
            on={['click']}
            trigger={<TriggerText>{'New Admin Notes: ' + newNotes}</TriggerText>}
          >
            <S.Grid divided columns="equal">
              <S.Grid.Column>
                <Text color="success" fontWeight="bold">
                  New Admin Notes
                </Text>
                <PopupText>{newNotes}</PopupText>
              </S.Grid.Column>
              <S.Grid.Column>
                <Text color="error" fontWeight="bold">
                  Old Admin Notes
                </Text>
                <PopupText>{oldNotes}</PopupText>
              </S.Grid.Column>
            </S.Grid>
          </S.Popup>
        )}
        {!changeIsAdminNotes && change?.description}
        {!changeIsAdminNotes && areThereItemChanges && (
          <TextButton
            basic
            color="black"
            onClick={() => toggleDetails(i)}
            title={`(${expanded ? 'hide' : 'show'} details)`}
          />
        )}
        {!changeIsAdminNotes && expanded && areThereItemChanges && MoreDetails}
      </S.Table.Cell>
    </S.Table.Row>
  )
}
