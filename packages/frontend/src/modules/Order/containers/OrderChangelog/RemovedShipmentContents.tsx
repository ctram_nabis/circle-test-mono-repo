import { Box, H4, S } from 'components'
import { LineItemChange } from 'generated/graphql'
import React from 'react'
import { OrderChangelogRow } from './OrderChangelogRow'
import { ShipmentContentsHeader } from './ShipmentContentsHeader'

interface Props {
  removedLineItems?: LineItemChange[]
}

export const RemovedShipmentContents = ({ removedLineItems }: Props) => (
  <Box>
    <br />
    <Box>
      <H4>Removed Order Contents</H4>
      <S.Table>
        <S.Table.Body>
          <ShipmentContentsHeader />
          {removedLineItems &&
            removedLineItems.map((lineItem) => (
              <OrderChangelogRow key={`removedOrderItem-${lineItem.id}`} lineItem={lineItem} />
            ))}
        </S.Table.Body>
      </S.Table>
    </Box>
  </Box>
)
