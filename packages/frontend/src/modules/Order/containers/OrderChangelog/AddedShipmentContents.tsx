import { Box, S } from 'components'
import { LineItemChange } from 'generated/graphql'
import React from 'react'
import { OrderChangelogRow } from './OrderChangelogRow'
import { ShipmentContentsHeader } from './ShipmentContentsHeader'

interface Props {
  addedLineItems: LineItemChange[]
}

export const AddedShipmentContents = ({ addedLineItems }: Props) => (
  <Box>
    <br />
    <Box>
      <h4>Added Order Contents</h4>
      <S.Table>
        <S.Table.Body>
          <ShipmentContentsHeader />

          {addedLineItems.map((lineItem) => (
            <OrderChangelogRow key={`addedOrderItem-${lineItem.id}`} lineItem={lineItem} isAdded />
          ))}
        </S.Table.Body>
      </S.Table>
    </Box>
  </Box>
)
