import { Container } from 'components/Container'
import { Box } from 'components/core/src/Box'
import { ErrorBox } from 'components/ErrorBox'
import { Grid } from 'components/Grid'
import { PageHeader } from 'components/PageHeader'
import { S } from 'components/SemanticComponents'
import { Small } from 'components/Small'
import { useOrderChangelogQuery } from 'generated/graphql'
import { GraphQLError } from 'graphql'
import includes from 'lodash/includes'
import { useRouter } from 'modules/Navigation'
import React, { useState } from 'react'
import styled from 'styled-components'
import { OrderChangelogTableRow } from './OrderChangelogTableRow'
// TODO: REPLACE WITH CHRIS COMPONENT ONCE MERGED
const Wrapper = styled(Box)`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  width: 100%;
  margin-bottom: 12px;
`

export const OrderChangelog = () => {
  const [expandedIndexes, setExpandedIndexes] = useState<number[]>([])

  const router = useRouter()
  const { data, loading, error } = useOrderChangelogQuery({
    variables: {
      id: router.query.id,
    },
    fetchPolicy: 'cache-and-network',
  })
  const changes = data?.getOrderChangelog
  const orderNumber = data?.getOrder?.number
  const header = orderNumber && `Order #${orderNumber} Changelog`

  const toggleDetails = (index: number) => {
    const collapsing = includes(expandedIndexes, index)

    const notAtIndex = (_index: number) => index !== _index
    const newIndexes = collapsing ? expandedIndexes.filter(notAtIndex) : expandedIndexes.concat(index)
    setExpandedIndexes(newIndexes)
  }
  return (
    <Container>
      <Wrapper>
        <S.Loader active={loading} content="Loading changelog..." />
        <ErrorBox errors={error?.graphQLErrors as GraphQLError[]} />
        {!error && !loading && (
          <S.Container>
            <Grid>
              <S.GridRow>
                <S.GridColumn>
                  <PageHeader>{header}</PageHeader>
                  <Small>
                    If you think this changelog is missing any order update activity, check out our Help Center post
                    about it{' '}
                    <a
                      href="https://nabis.zendesk.com/hc/en-us/articles/360022169391"
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      here
                    </a>
                    .
                  </Small>
                  <S.Table>
                    <S.Table.Header>
                      <S.Table.Row>
                        <S.Table.HeaderCell>Timestamp (PST)</S.Table.HeaderCell>
                        <S.Table.HeaderCell>Change submitted by</S.Table.HeaderCell>
                        <S.Table.HeaderCell>Change</S.Table.HeaderCell>
                      </S.Table.Row>
                    </S.Table.Header>
                    <S.Table.Body>
                      {changes &&
                        changes.map((change, i) => (
                          <OrderChangelogTableRow
                            expandedIndexes={expandedIndexes}
                            key={`orderChangelogTableRow-${i}`}
                            i={i}
                            toggleDetails={toggleDetails}
                            change={change}
                          />
                        ))}
                    </S.Table.Body>
                  </S.Table>
                </S.GridColumn>
              </S.GridRow>
            </Grid>
          </S.Container>
        )}
      </Wrapper>
    </Container>
  )
}
