import styled from 'styled-components'

interface Props {
  color?: string
}
// TODO: REPLACE WITH CHRIS COMPONENT ONCE MERGED
export const AddedRemovedComponent = styled.span`
  color: ${(props: Props) => props.color || 'inherit'};
`
