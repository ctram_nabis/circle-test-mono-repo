import { Box, Grid, H4, S } from 'components'
import { LineItemChange } from 'generated/graphql'
import React from 'react'
import { OrderChangelogRow } from './OrderChangelogRow'
import { ShipmentContentsHeader } from './ShipmentContentsHeader'

interface Props {
  addedLineItems: LineItemChange[]
  removedLineItems: LineItemChange[]
}

export const UpdatedShipmentContents = ({ addedLineItems, removedLineItems }: Props) => (
  <Box>
    <Box>
      <br />
      <Grid>
        <S.GridRow>
          <S.GridColumn>
            <H4>Before update</H4>
            <S.Table>
              <S.Table.Body>
                <ShipmentContentsHeader />
                {removedLineItems.map((removedLineItem, i) => {
                  const otherEnd = addedLineItems[i]
                  return <OrderChangelogRow key={removedLineItem.id} lineItem={removedLineItem} otherEnd={otherEnd} />
                })}
              </S.Table.Body>
            </S.Table>
          </S.GridColumn>
        </S.GridRow>
        <S.GridRow>
          <S.GridColumn>
            <H4>After update</H4>
            <S.Table>
              <S.Table.Body>
                <ShipmentContentsHeader />
                {addedLineItems.map((addedLineItem, i) => {
                  const otherEnd = removedLineItems[i]
                  return (
                    <OrderChangelogRow key={addedLineItem.id} lineItem={addedLineItem} otherEnd={otherEnd} isAdded />
                  )
                })}
              </S.Table.Body>
            </S.Table>
          </S.GridColumn>
        </S.GridRow>
      </Grid>
    </Box>
  </Box>
)
