import { S } from 'components'
import { LineItemChange, SkuBatch } from 'generated/graphql'
import React from 'react'
import { dollarFormat } from 'utils'
import { AddedRemovedComponent } from './components'
const dollar = (num: string | number) => dollarFormat(Number(num).toFixed(2))

const getSkuNameAndBatchCode = (skuBatch: SkuBatch | undefined | null) => ({
  skuName: skuBatch?.sku?.name,
  batchCode: skuBatch ? skuBatch?.batch?.code : 'n/a',
})

const addColor = 'rgba(60, 190, 145)'
const removeColor = 'rgba(235, 120, 100)'

interface Props {
  isAdded?: boolean
  lineItem?: LineItemChange
  otherEnd?: LineItemChange
}

export const OrderChangelogRow = ({ isAdded, lineItem, otherEnd }: Props) => {
  const isExciseTaxable = lineItem?.isExciseTaxable
  const isSample = lineItem?.isSample
  const name = lineItem?.name || ''
  const pricePerUnit = lineItem?.pricePerUnit || 0
  const quantity = lineItem?.quantity || 0
  const skuBatch = lineItem?.skuBatch
  const discount = lineItem?.discount || 0
  const { skuName, batchCode } = getSkuNameAndBatchCode(skuBatch)

  const colorToRender = isAdded ? addColor : removeColor

  // Legacy code
  const NameColor = otherEnd && otherEnd.name !== name ? 'inherit' : colorToRender
  const QuantityColor = otherEnd && otherEnd.quantity === quantity ? 'inherit' : colorToRender
  const PpuColor = otherEnd && otherEnd.pricePerUnit === pricePerUnit ? 'inherit' : colorToRender
  const ExciseColor = otherEnd && otherEnd.isExciseTaxable === isExciseTaxable ? 'inherit' : colorToRender
  const SampleColor = otherEnd && otherEnd.isSample === isSample ? 'inherit' : colorToRender
  const BatchCodeColor = otherEnd && otherEnd?.skuBatch?.batch?.code === batchCode ? 'inherit' : colorToRender
  const SkuNameColor = otherEnd && otherEnd?.skuBatch?.sku?.name === skuName ? 'inherit' : colorToRender
  const DiscountColor = otherEnd && otherEnd?.discount === discount ? 'inherit' : colorToRender

  return (
    <S.Table.Row>
      <S.Table.Cell>
        {skuName ? (
          <AddedRemovedComponent color={SkuNameColor}>{skuName}</AddedRemovedComponent>
        ) : (
          // fallback to name in case sku is falsey, this is true for records prior to this change
          <AddedRemovedComponent color={NameColor}>{name}</AddedRemovedComponent>
        )}
      </S.Table.Cell>
      <S.Table.Cell>
        <AddedRemovedComponent color={BatchCodeColor}>{batchCode || '---'}</AddedRemovedComponent>
      </S.Table.Cell>
      <S.Table.Cell>
        <AddedRemovedComponent color={QuantityColor}>{quantity}</AddedRemovedComponent>
      </S.Table.Cell>
      <S.Table.Cell>
        <AddedRemovedComponent color={PpuColor}>{dollar(pricePerUnit)}</AddedRemovedComponent>
      </S.Table.Cell>
      <S.Table.Cell>
        <AddedRemovedComponent color={ExciseColor}>{isExciseTaxable ? 'yes' : 'no'}</AddedRemovedComponent>
      </S.Table.Cell>
      <S.Table.Cell>
        <AddedRemovedComponent color={SampleColor}>{isSample ? 'yes' : 'no'}</AddedRemovedComponent>
      </S.Table.Cell>
      <S.Table.Cell>
        <AddedRemovedComponent color={DiscountColor}>{dollar(discount)}</AddedRemovedComponent>
      </S.Table.Cell>
    </S.Table.Row>
  )
}
