import { P, S } from 'components'
import React from 'react'

export const ShipmentContentsHeader = () => (
  <S.Table.Row>
    <S.Table.Cell width={4}>
      <P fontSize="14px" fontWeight="bold">
        SKU
      </P>
    </S.Table.Cell>
    <S.Table.Cell width={3}>
      <P fontSize="14px" fontWeight="bold">
        Batch
      </P>
    </S.Table.Cell>
    <S.Table.Cell>
      <P fontSize="14px" fontWeight="bold">
        Quantity
      </P>
    </S.Table.Cell>
    <S.Table.Cell>
      <P fontSize="14px" fontWeight="bold">
        Price per unit
      </P>
    </S.Table.Cell>
    <S.Table.Cell>
      <P fontSize="14px" fontWeight="bold">
        Excise?
      </P>
    </S.Table.Cell>
    <S.Table.Cell>
      <P fontSize="14px" fontWeight="bold">
        Sample?
      </P>
    </S.Table.Cell>
    <S.Table.Cell>
      <P fontSize="14px" fontWeight="bold">
        Discount
      </P>
    </S.Table.Cell>
  </S.Table.Row>
)
