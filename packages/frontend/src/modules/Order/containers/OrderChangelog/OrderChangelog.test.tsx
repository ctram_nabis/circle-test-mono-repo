import { MockedProvider } from '@apollo/react-testing'
import { shallow } from 'enzyme'
import React from 'react'
import { AddedShipmentContents } from './AddedShipmentContents'
import { AddedRemovedComponent } from './components/AddedRemovedComponent'
import { OrderChangelog } from './OrderChangelog'
import { OrderChangelogRow } from './OrderChangelogRow'
import { RemovedShipmentContents } from './RemovedShipmentContents'
import { ShipmentContentsHeader } from './ShipmentContentsHeader'
import { UpdatedShipmentContents } from './UpdatedShipmentContents'

describe('`OrderChangelog` container', () => {
  it('`AddedRemovedComponent` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <AddedRemovedComponent />
      </MockedProvider>,
    )
  })
  it('`AddedShipmentContents` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <AddedShipmentContents />
      </MockedProvider>,
    )
  })
  it('`OrderChangelog` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <OrderChangelog />
      </MockedProvider>,
    )
  })
  it('`OrderChangelogRow` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <OrderChangelogRow />
      </MockedProvider>,
    )
  })
  it('`RemovedShipmentContents` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <RemovedShipmentContents />
      </MockedProvider>,
    )
  })
  it('`ShipmentContentsHeader` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <ShipmentContentsHeader />
      </MockedProvider>,
    )
  })
  it('`UpdatedShipmentContents` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <UpdatedShipmentContents />
      </MockedProvider>,
    )
  })
})
