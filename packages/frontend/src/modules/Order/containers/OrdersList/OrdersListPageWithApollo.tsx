import { DeleteOrderDocument, GenerateManifestDocument, GetOrderSubscriberDocument } from 'generated/graphql'
import { graphql } from 'react-apollo'
import { withRouter } from 'react-router-dom'
import { compose } from 'recompose'
import OrdersListPage from './OrdersListPage'

const OrdersListPageWithApollo: any = compose(
  graphql(GenerateManifestDocument, {
    props: ({ mutate }) => ({
      generateManifest: (orderId) =>
        mutate({
          variables: {
            orderId,
          },
          // refetchQueries: ['AllOrders', 'allOrders'],
        }),
    }),
  } as any),
  graphql(GetOrderSubscriberDocument, {
    props: ({ mutate }) => ({
      getOrderSubscriber: (orderId, organizationId) =>
        mutate({
          variables: {
            orderId,
            organizationId,
          },
        }),
    }),
  } as any),
  graphql(DeleteOrderDocument, {
    props: ({ mutate }) => ({
      deleteOrder: (input) =>
        mutate({
          variables: {
            input,
          },
          // refetchQueries: ['AllOrdersDocument'], //commenting out since it wasn't refetching
        }),
    }),
  } as any),
)(OrdersListPage)

export default withRouter(OrdersListPageWithApollo)
