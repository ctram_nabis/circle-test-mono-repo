import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { Dropdown } from 'components/Dropdown'
import { ErrorBox } from 'components/ErrorBox'
import { PageHeader } from 'components/PageHeader'
import { Pagination } from 'components/Pagination'
import { S } from 'components/SemanticComponents'
import config from 'config'
import { AccessibleBrandsDocument, AllOrdersDocument, OrganizationDocument } from 'generated/graphql'
import fileDownload from 'js-file-download'
import { cloneDeep, debounce } from 'lodash'
import isEmpty from 'lodash/isEmpty'
import { client } from 'modules/Apollo'
import { logger } from 'modules/Logger'
import { OrdersHeaders } from 'modules/Order/components'
import OrderTableRow from 'modules/Order/components/OrdersTableRow'
import { withOrganizationHook } from 'modules/Order/helpers/withOrganizationHook'
import { withMeHook } from 'modules/User/helpers/withMeHook'
import queryString from 'query-string'
import React from 'react'
import { compose } from 'recompose'
import { GridColumn, Segment, Table } from 'semantic-ui-react'
import styled from 'styled-components'
import { oc } from 'ts-optchain'

const NoOrdersContainer = styled(Box)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
`

const NoOrdersText = styled(Box)`
  margin: 20px 0;
`

const OrderGridColumn = styled(GridColumn)`
  display: flex !important;
  justify-content: space-between !important;
  align-items: center !important;
`

const Wrapper = styled(Box)`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  width: 100%;
  margin-bottom: 12px;
`
const AppContainer = styled(Box)`
  display: flex;
  height: 100%;
  justify-content: center;
  padding: 15px 10px;
  width: 100%;
  overflow: auto;
`

const COL_SPAN_LENGTH = 13

class OrdersListPage extends React.Component<any, any> {
  debounceRefetchOrders: () => any

  constructor(props) {
    super(props)

    const qs: any = queryString.parse(oc(props).location.search(''))

    this.state = {
      itemsToShow: [],
      errorMsg: null,
      generatingManifest: {
        isGenerating: false,
        orderId: null,
      },
      filters: {
        number: qs.number || '',
        status: qs.status || null,
        paymentStatus: qs.paymentStatus || null,
        start: qs.start || null,
        end: qs.start || null,
        site: qs.site || '',
        creator: qs.creator || '',
        // Pagination Info
        page: parseInt(qs.page as string) || 1,
        numItemsPerPage: 25,
        orderBy: [
          {
            attribute: 'createdAt',
            order: 'DESC',
          },
        ],
        totalNumItems: 0,
        totalNumPages: 0,
      },
      loaded: null,
      loadingOrders: false,
      refetchingOrders: false,
    }

    this.debounceRefetchOrders = debounce(this.refetchOrders, 600)
  }

  async componentDidMount() {
    this.setState({ ...this.state, loadingOrders: true })
    const organization = await this.Organization()
    this.setState({ organization })
    const allOrders = await this.AllOrders()
    this.setState({ ...this.state, allOrders, loadingOrders: false })
    const accessibleBrands = await this.AccessibleBrands()
    this.setState({ accessibleBrands })
  }

  async AccessibleBrands() {
    const { organization } = this.state
    const response = await client.query({
      query: AccessibleBrandsDocument,
      variables: {
        orgId: organization?.id,
      },
    })

    return oc(response).data.viewer.accessibleBrands({})
  }

  async Organization() {
    const { organization } = this.props || {}
    const response = await client.query({
      query: OrganizationDocument,
      variables: {
        id: organization?.id,
      },
    })

    return oc(response).data.getOrganization({})
  }

  async AllOrders() {
    const qs = queryString.parse(this.props?.location?.search)
    const organizationId = this.state?.organization?.id

    const allOrdersVariables = {
      organizationId,
      number: qs.number,
      status: qs.status,
      paymentStatus: qs.paymentStatus,
      start: qs.start,
      end: qs.end,
      site: qs.site,
      creator: qs.creator,
      pageInfo: {
        page: parseInt(qs.page as string) || 1,
        numItemsPerPage: 25,
        orderBy: [
          {
            attribute: 'date',
            order: 'DESC',
          },
          {
            attribute: 'createdAt',
            order: 'DESC',
          },
        ],
      },
    }

    try {
      this.setState({ errorMsg: null })

      if (allOrdersVariables.organizationId) {
        const response = await client.query({
          query: AllOrdersDocument,
          fetchPolicy: 'network-only',
          variables: {
            ...allOrdersVariables,
          },
        })

        this.setState({ loadingOrders: false })

        // handle if there are no results found
        if (isEmpty(response?.data?.viewer?.allOrders?.results)) {
          this.setState({ errorMsg: 'No orders found' })
          return []
        }

        return oc(response).data.viewer.allOrders({})
      } else {
        this.setState({ loadingOrders: false })
        return new Promise((resolve) => resolve([]))
      }
    } catch (e) {
      logger.error('Failed to query for orders')

      this.setState({
        errorMsg: e?.message,
      })

      return []
    }
  }

  componentDidUpdate() {
    // TODO: Review: This code was in componentWillReceiveProps,
    // however this needs to be called multiple times for functionality.
    // Doing this creats a memory leak.
    if (!oc(this).props.data.loading(false)) {
      const allOrders = oc(this).state.allOrders.results([])
      const pageInfo = oc(this).state.allOrders.pageInfo({})
      const users = oc(this).state.organization.members([])

      const canCreate = this.props?.canCreate?.order || false

      const newState = {
        users,
        canCreate,
        itemsToShow: allOrders || [],
        filters: {
          ...this.state.filters,
          ...pageInfo,
        },
      }
      // TODO: Review: This is a simple check to prevent a setState loop
      for (const key of Object.keys(newState)) {
        if (JSON.stringify(newState[key]) !== JSON.stringify(this.state[key])) {
          this.setState({ ...newState })
          break
        }
      }
    }
  }

  // TODO: Make a more elegant solution for refetching after delete
  deleteOrder = async (id) => {
    try {
      const response = await this.props.deleteOrder({ id })
      if (!response.errors) {
        const allOrders = await this.AllOrders()
        this.setState({ allOrders })
      }
    } catch (err) {
      logger.error(err)
    }
  }

  generateManifest = (order) => {
    this.setState({
      generatingManifest: {
        isGenerating: true,
        orderId: order.id,
      },
    })

    return this.props.generateManifest(order.id).then((res) => {
      this.setState({
        generatingManifest: {
          isGenerating: false,
          orderId: null,
        },
      })

      return res
    })
  }

  goToNewOrderPage = () => {
    const { history, organization } = this.props
    return history.push(`/${organization.alias}/app/orders/new`)
  }

  reportsBtnClick = async () => {
    const brands = this.state?.accessibleBrands || []
    const selectedId = brands.length === 1 ? brands[0].id : this.state.loaded

    const brandReportCsv = config.apollo.GRAPHQL_URL.replace('/graphql/admin', `/report-csv/${selectedId}`)

    const selectedName = brands.length === 1 ? brands[0].name : this.state.selectedBrand

    const headers = new Headers()
    const auth = `Basic ${Buffer.from(`${config.app.apiKey}:`).toString('base64')}`
    headers.set('Authorization', auth)
    const res = await fetch(brandReportCsv, { headers })
    const data = await res.text()
    return fileDownload(data, `${selectedName}.csv`)
  }

  selectBrandReport = ({ value: selectedBrandId }) => {
    const accessibleBrands = this.state?.accessibleBrands || {}
    const { name } = accessibleBrands?.find((b) => b.id === selectedBrandId)

    this.setState({ rows: [], loaded: null, selectedBrand: null })
    this.getReport(selectedBrandId)
      .then((res) => res.json())
      .then(({ rows }) => this.setState({ rows, loaded: selectedBrandId, selectedBrand: name }))
  }

  getReport = (id) => {
    const brandReportData = config.apollo.GRAPHQL_URL.replace('/graphql/admin', `/report/${id}`)

    const headers = new Headers()
    const auth = `Basic ${Buffer.from(`${config.app.apiKey}:`).toString('base64')}`
    headers.set('Authorization', auth)
    return fetch(brandReportData, { headers })
  }

  areThereParams = () => {
    const { location } = this.props
    const qs = queryString.parse(location?.search)

    return Object.keys(qs).length === 0
  }

  handlePageChange = (e: Event, data: any) => {
    this.handleFilterChange('page', data.activePage)
  }

  handleFilterChange = (filter, value) => {
    const newFilters = cloneDeep(this.state.filters)
    newFilters[filter] = value
    if (filter !== 'page') {
      newFilters.page = 1
    }
    this.setState(
      {
        filters: newFilters,
      },
      async () => this.debounceRefetchOrders(),
    )
  }

  refetchOrders = async () => {
    const f = this.state.filters
    const {
      match: {
        params: { alias },
      },
    } = this.props

    const cleanQueryString = (() => {
      const qs: any = {}
      f.number && (qs.number = f.number)
      f.status && (qs.status = f.status)
      f.paymentStatus && (qs.paymentStatus = f.paymentStatus)
      f.start && (qs.start = f.start)
      f.end && (qs.end = f.end)
      f.site && (qs.site = f.site)
      f.creator && (qs.creator = f.creator)
      f.page && (qs.page = f.page)
      return qs
    })()

    const newQueryString = queryString.stringify(cleanQueryString)

    const { history } = this.props
    history.push(`/${alias}/app/orders${newQueryString ? '?' : ''}${newQueryString}`)
    this.setState({ ...this.state, refetchingOrders: true })
    const allOrders = await this.AllOrders()
    this.setState({ ...this.state, allOrders, refetchingOrders: false })
  }

  renderOversoldBadge = (order) => {
    const oversold = order.lineItems.reduce((acc, li) => {
      if (li.oversold) {
        acc.push(<li>{`(${li.code}) ${li.name}, qty: ${li.quantity}`}</li>)
        return acc
      }
      return acc
    }, [])
    if (oversold.length > 0) {
      return (
        <S.Popup wide trigger={<S.Icon name="warning sign" color="red" />}>
          <S.Popup.Header>Oversold items!</S.Popup.Header>
          <S.Popup.Content>{oversold}</S.Popup.Content>
        </S.Popup>
      )
    } else return ''
  }

  renderBrandDropdown = () => {
    const brands = oc(this).state.accessibleBrands([])
    const { canCreate } = this.props

    if (!brands.map) {
      return
    }

    const options = brands.map((b) => ({
      key: b.id,
      value: b.id,
      text: b.name,
    }))

    return (
      canCreate.csvExport && (
        <>
          <Dropdown
            mr={1}
            placeholder="Select a brand"
            search
            selection
            value={options.length === 1 ? options[0].value : this.state.loaded}
            options={options}
            onChange={(_e, d) => this.selectBrandReport(d as any)}
          />

          <Button
            onClick={() => this.reportsBtnClick()}
            disabled={options.length === 1 ? !options[0].value : !this.state.loaded}
          >
            Download CSV
          </Button>
        </>
      )
    )
  }

  renderHeader = () => {
    const { filters } = this.state
    return <OrdersHeaders filters={filters} filter={this.handleFilterChange} />
  }

  renderTableRow = (o) => {
    const { me = {} }: any = this.props
    const { isAdmin } = me.user

    const { users = [], organization = {} } = this.state
    const { isOrgAdmin } = this.props

    const someoneElseSoldThisOrder = Boolean(o.organization.id !== organization.id)

    return (
      <OrderTableRow
        key={`order-row-${o.id}`}
        user={me?.user}
        organization={organization}
        alias={organization.alias}
        isAdmin={isAdmin}
        deleteOrder={this.deleteOrder}
        notMine={isOrgAdmin ? false : someoneElseSoldThisOrder}
        order={o}
        users={users}
        generateManifest={this.generateManifest}
        generatingManifest={this.state.generatingManifest}
        getOrderSubscriber={this.props.getOrderSubscriber}
      />
    )
  }

  render() {
    const { page = 0, totalNumPages = 0 } = this.state.filters
    const { allOrders = [], loadingOrders, refetchingOrders } = this.state

    const definitelyNoOrders = isEmpty(allOrders?.results) && this.areThereParams()
    const isError = Boolean(this.state?.errorMsg)
    const orders = oc(this).state.itemsToShow([])

    const renderComponent = (
      <S.Container fluid data-test="orders-list-page">
        <S.Grid columns={1} padded>
          <S.Loader active={loadingOrders} inline="centered" size="large" content="Loading Orders..." />
          {!loadingOrders && (
            <>
              <S.GridRow>
                <S.GridColumn>
                  <OrderGridColumn>
                    <PageHeader>Orders</PageHeader>
                    <Box>
                      {this.state.canCreate && this.renderBrandDropdown()}
                      {this.state.canCreate && !definitelyNoOrders && (
                        <Button
                          icon
                          positive
                          centered
                          title="New Order"
                          labelPosition="left"
                          onClick={() => this.goToNewOrderPage()}
                          name="plus"
                          style={{ display: 'inline-block' }}
                        />
                      )}
                    </Box>
                  </OrderGridColumn>
                </S.GridColumn>
              </S.GridRow>
              {definitelyNoOrders ? (
                <NoOrdersContainer>
                  <NoOrdersText>Looks like you don't have any orders yet. Please add a new order.</NoOrdersText>
                  {this.state.canCreate && (
                    <Button
                      icon
                      name="plus"
                      labelPosition="left"
                      positive
                      centered
                      onClick={() => this.goToNewOrderPage()}
                      title="New Order"
                    />
                  )}
                </NoOrdersContainer>
              ) : (
                <>
                  <S.GridRow>
                    <S.GridColumn>
                      <S.Table celled selectable unstackable verticalAlign="middle" sortable color="green">
                        {this.renderHeader()}
                        <S.Table.Body>
                          {refetchingOrders ? (
                            <S.Table.Row>
                              <S.Table.Cell width={COL_SPAN_LENGTH}>
                                <S.Dimmer active={refetchingOrders} inverted>
                                  <S.Loader active={refetchingOrders} />
                                </S.Dimmer>
                              </S.Table.Cell>
                            </S.Table.Row>
                          ) : isError ? (
                            <Table.Row>
                              <Table.Cell colSpan={COL_SPAN_LENGTH}>
                                <Segment placeholder className="flex-style">
                                  <S.Header>
                                    <ErrorBox errors={[this.state?.errorMsg]} />
                                  </S.Header>
                                </Segment>
                              </Table.Cell>
                            </Table.Row>
                          ) : (
                            orders.map((o, i) => this.renderTableRow(o))
                          )}
                        </S.Table.Body>
                        <S.Table.Footer />
                      </S.Table>
                      <Pagination
                        onPageChange={(e, data) => this.handlePageChange(e as Event, data as object)}
                        activePage={page}
                        totalNumPages={totalNumPages}
                        siblingRange={3}
                      />
                    </S.GridColumn>
                  </S.GridRow>
                </>
              )}
            </>
          )}
        </S.Grid>
      </S.Container>
    )

    return (
      <AppContainer overflow="auto">
        <Wrapper>{renderComponent}</Wrapper>
      </AppContainer>
    )
  }
}

export default compose(withOrganizationHook, withMeHook)(OrdersListPage)
