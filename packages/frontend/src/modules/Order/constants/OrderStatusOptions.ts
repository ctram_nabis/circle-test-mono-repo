export const ORDER_STATUS_OPTIONS = [
  { key: 'UNSCHEDULED', value: 'UNSCHEDULED', text: 'Unscheduled' },
  { key: 'SCHEDULED', value: 'SCHEDULED', text: 'Scheduled' },
  { key: 'TRANSFERRING', value: 'TRANSFERRING', text: 'Transferring' },
  { key: 'DELIVERED', value: 'DELIVERED', text: 'Delivered' },
  {
    key: 'DELIVERED_WITH_EDITS',
    text: 'Delivered with Edits',
    value: 'DELIVERED_WITH_EDITS',
  },
  { key: 'DELAYED', value: 'DELAYED', text: 'Delayed' },
  { key: 'REJECTED', value: 'REJECTED', text: 'Rejected' },
  { key: 'CANCELLED', value: 'CANCELLED', text: 'Cancelled' },
]
