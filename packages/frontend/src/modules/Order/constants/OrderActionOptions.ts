import { ORDER_ACTIONS } from 'modules/Order/constants/OrderActionEnum'

const {
  DELIVERY_TO_BRAND,
  DELIVERY_TO_RETAILER,
  DROPOFF_TO_NABIS,
  PICKUP_FROM_BRAND,
  PICKUP_FROM_NABIS,
  RETURN_FROM_RETAILER,
  INTERNAL_TRANSFER,
  // PAYMENT_PICKUP_FROM_RETAILER,
} = ORDER_ACTIONS

export const ORDER_ACTION_OPTIONS = [
  {
    key: DELIVERY_TO_RETAILER,
    text: 'Delivery To Retailer',
    value: DELIVERY_TO_RETAILER,
  },
  {
    key: PICKUP_FROM_BRAND,
    text: 'Pick Up From Brand',
    value: PICKUP_FROM_BRAND,
  },
  {
    key: RETURN_FROM_RETAILER,
    text: 'Return From Retailer',
    value: RETURN_FROM_RETAILER,
  },
  {
    key: PICKUP_FROM_NABIS,
    text: 'Brand Picks Up From Nabis',
    value: PICKUP_FROM_NABIS,
  },
  {
    key: DROPOFF_TO_NABIS,
    text: 'Brand Drops Off To Nabis',
    value: DROPOFF_TO_NABIS,
  },
  {
    key: DELIVERY_TO_BRAND,
    text: 'Delivery To Brand',
    value: DELIVERY_TO_BRAND,
  },
  {
    key: INTERNAL_TRANSFER,
    text: 'Internal Warehouse Transfer',
    value: INTERNAL_TRANSFER,
  },
  //   { key: OTHER, value: OTHER, text: 'Other' },
]

export const ORDER_ACTION_ADMIN_ADDITIONAL_OPTIONS = [
  {
    key: 'PAYMENT_PICKUP_FROM_RETAILER',
    text: 'Payment Pickup From Retailer',
    value: 'PAYMENT_PICKUP_FROM_RETAILER',
  },
]
