import { OrderPaymentStatusEnum } from 'generated/graphql'

export const PAYMENT_STATUS: Record<OrderPaymentStatusEnum, OrderPaymentStatusEnum> = {
  UNPAID: 'UNPAID',
  COD_PAID: 'COD_PAID',
  NET_TERMS_PAID: 'NET_TERMS_PAID',
  REMITTED: 'REMITTED',
  SELF_COLLECTED: 'SELF_COLLECTED',
}

export const PAYMENT_STATUS_SELECT_OPTIONS = [
  { key: PAYMENT_STATUS.UNPAID, value: PAYMENT_STATUS.UNPAID, text: 'Unpaid' },
  { key: PAYMENT_STATUS.COD_PAID, value: PAYMENT_STATUS.COD_PAID, text: 'COD, Paid' },
  { key: PAYMENT_STATUS.NET_TERMS_PAID, value: PAYMENT_STATUS.NET_TERMS_PAID, text: 'Net Terms, Paid' },
  { key: PAYMENT_STATUS.REMITTED, value: PAYMENT_STATUS.REMITTED, text: 'Remitted' },
  { key: PAYMENT_STATUS.SELF_COLLECTED, value: PAYMENT_STATUS.SELF_COLLECTED, text: 'Self Collected' },
]
