import { OrderStatusEnum } from 'generated/graphql'

export const ORDER_STATUS: Record<OrderStatusEnum, OrderStatusEnum> = {
  UNSCHEDULED: 'UNSCHEDULED',
  SCHEDULED: 'SCHEDULED',
  TRANSFERRING: 'TRANSFERRING',
  DELIVERED: 'DELIVERED',
  DELIVERED_WITH_EDITS: 'DELIVERED_WITH_EDITS',
  DELAYED: 'DELAYED',
  REJECTED: 'REJECTED',
  CANCELLED: 'CANCELLED',
  DRAFT: 'DRAFT',
}

export const ORDER_STATUS_DESC: Record<OrderStatusEnum, string> = {
  DRAFT: 'Draft',
  UNSCHEDULED: 'Unscheduled',
  SCHEDULED: 'Scheduled',
  TRANSFERRING: 'Transferring',
  DELIVERED: 'Delivered',
  DELIVERED_WITH_EDITS: 'Delivered with Edits',
  DELAYED: 'Delayed',
  REJECTED: 'Rejected',
  CANCELLED: 'Cancelled',
}
