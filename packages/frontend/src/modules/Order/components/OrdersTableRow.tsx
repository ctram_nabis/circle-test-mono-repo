/* not done */
import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { Label } from 'components/Label'
import { S } from 'components/SemanticComponents'
import idx from 'idx'
import { NABIS_ORGANIZATION_TO_ID } from 'modules/Warehouse/constants'
import moment from 'moment-timezone'
import React from 'react'
import styled from 'styled-components'
import { dollarFormat, parseDate, titleize, titleizeDaysTillPaymentDue } from 'utils'
import { ActionCookie } from './ActionCookie'
import { AssignOrder } from './AssignOrder'
import { OrderNotes } from './OrderNotes'
import { OrdersActions } from './OrdersActions'
// import { calculateMoney } from '../helpers'

export const CellOptionLabel = styled(Label)`
  border: none;
  font-family: Lato, 'Helvetica Neue', Arial, Helvetica, sans-serif;
  font-weight: 700;
  color: rgba(0, 0, 0, 0.87);
  display: block;
  font-size: 0.67em;
`

const GMVandDiscountText = styled(Box)`
  display: block;
`
const DiscountText = styled(Box)`
  text-align: left;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-size: 0.8em;
`
const StyledText = styled(Text)`
  display: inline-block;
  width: 70px;
  overflow-wrap: break-word;
`
const GMVText = styled(StyledText)`
  text-align: left;
`

const ActionLink = styled.a`
  cursor: pointer;
  text-decoration: underline;
`

export class OrderTableRow extends React.Component<any, any> {
  constructor(props) {
    super(props)

    const { organization = {}, order = {} } = props

    const correctSubscriber = Array.isArray(order.orderSubscribers) && order.orderSubscribers[0]

    let manifestGDriveFileId = idx(correctSubscriber, (_) => _.manifestGDriveFileId)
    let invoicesS3FileLink = idx(correctSubscriber, (_) => _.invoicesS3FileLink)

    const isNabisOrg = organization?.id === NABIS_ORGANIZATION_TO_ID.DELIVERIES

    if (isNabisOrg) {
      if (order.manifestGDriveFileId) manifestGDriveFileId = order.manifestGDriveFileId
      if (order.invoicesS3FileLink) invoicesS3FileLink = order.invoicesS3FileLink
    }

    this.state = {
      manifestGDriveFileId,
      invoicesS3FileLink,
    }
  }

  renderSellerLabel = (name) => {
    return (
      <Box>
        <S.Divider />
        <em style={{ fontSize: '.8em' }}>Submitted by {name}</em>
      </Box>
    )
  }

  renderOversoldBadge = (order) => {
    const oversold = order.lineItems.reduce((acc, li) => {
      if (li.oversold) {
        acc.push(<li>{`(${li.code}) ${li.name}, qty: ${li.quantity}`}</li>)
        return acc
      }
      return acc
    }, [])
    if (oversold.length > 0) {
      return (
        <S.Popup wide trigger={<S.Icon name="warning sign" color="red" />}>
          <S.Popup.Header>Oversold items!</S.Popup.Header>
          <S.Popup.Content>{oversold}</S.Popup.Content>
        </S.Popup>
      )
    } else {
      return ''
    }
  }

  renderGMVandDiscount = (order) => {
    const isDiscounted = order.discount > 0

    // const { subtotal } = calculateMoney({
    //   lineItems: order.lineItems,
    //   siteUsState: order.site.state,
    //   deliveryDate: order.date,
    // })

    // TODO: will need to revisit the GMV value below

    return (
      <GMVandDiscountText>
        <GMVText>{dollarFormat(order.gmv)}</GMVText>
        {isDiscounted && (
          <Box>
            <S.Divider fitted />
            <DiscountText>{`-${dollarFormat(order.discount)}`}</DiscountText>
          </Box>
        )}
      </GMVandDiscountText>
    )
  }

  onUpdateManifestClick = async (o) => {
    const { organization = {} } = this.props
    const isNabisOrg = organization?.id === NABIS_ORGANIZATION_TO_ID.DELIVERIES

    let invoicesS3FileLink
    let manifestGDriveFileId

    const response = await this.props.generateManifest(o)

    if (isNabisOrg) {
      invoicesS3FileLink = idx(response, (_) => _.data.generateManifest.invoicesS3FileLink)

      this.setState({
        invoicesS3FileLink,
      })

      return
    }

    // get order subscriber link
    const res = await this.props.getOrderSubscriber(o.id, this.props.organization.id)

    invoicesS3FileLink = idx(res, (_) => _.data.getOrderSubscriber.invoicesS3FileLink)
    manifestGDriveFileId = idx(res, (_) => _.data.getOrderSubscriber.manifestGDriveFileId)

    this.setState({
      invoicesS3FileLink,
      manifestGDriveFileId,
    })
  }

  getManifestLink = (o) => {
    const { generatingManifest } = this.props
    const { manifestGDriveFileId, invoicesS3FileLink } = this.state

    return (
      <>
        {generatingManifest.isGenerating && o.id === generatingManifest.orderId ? (
          <Box col center mb={2}>
            <S.Loader active inline />
          </Box>
        ) : invoicesS3FileLink ? (
          <Box col center mb={2}>
            <ActionLink target="_blank" href={invoicesS3FileLink}>
              <S.Icon size="large" name="file pdf outline" />
            </ActionLink>
            <ActionLink target="_blank" href={invoicesS3FileLink}>
              Manifest
            </ActionLink>
          </Box>
        ) : (
          <Box col center mb={2}>
            <ActionLink onClick={() => this.onUpdateManifestClick(o)}>
              <S.Icon size="large" name="sync" />
            </ActionLink>
            <ActionLink onClick={() => this.onUpdateManifestClick(o)}>Generate</ActionLink>
          </Box>
        )}

        {manifestGDriveFileId && (
          <Box col center>
            <ActionLink target="_blank" href={`https://drive.google.com/file/d/${manifestGDriveFileId}`}>
              <S.Icon size="large" name="file pdf outline" />
            </ActionLink>
            <ActionLink target="_blank" href={`https://drive.google.com/file/d/${manifestGDriveFileId}`}>
              Legacy
            </ActionLink>
          </Box>
        )}
      </>
    )
  }

  render() {
    const {
      alias,
      order,
      // user,
      notMine,
      // isAdmin,
      // organization,
      users,
      updateOrder,
      deleteOrder,
    } = this.props

    const { notes, adminNotes } = order || {}

    return (
      <S.Table.Row data-test="orders-table-row">
        <S.Table.Cell>
          <Text fontWeight="bold">
            {order.number}
            {this.renderOversoldBadge(order)}
          </Text>
          <S.Divider />
          <Box>
            <CellOptionLabel>Created At:</CellOptionLabel>
            <CellOptionLabel>{`${moment(order.createdAt).format('MM/DD/YYYY h:mmA')}`}</CellOptionLabel>
          </Box>
          {notMine && this.renderSellerLabel(order.organization.name)}
        </S.Table.Cell>
        <S.Table.Cell>
          {order.name}
          <OrderNotes notes={notes} adminNotes={adminNotes} />
        </S.Table.Cell>
        <S.Table.Cell>{parseDate(order.date)}</S.Table.Cell>
        <S.Table.Cell>
          <ActionCookie action={order.action} />
          <StyledText>{titleize(order.action)}</StyledText>
        </S.Table.Cell>
        <S.Table.Cell>{this.renderGMVandDiscount(order)}</S.Table.Cell>
        <S.Table.Cell>{titleizeDaysTillPaymentDue(order.daysTillPaymentDue)}</S.Table.Cell>
        <S.Table.Cell>{order.site && order.site.name}</S.Table.Cell>
        <S.Table.Cell>{titleize(order.status)}</S.Table.Cell>
        <S.Table.Cell>{titleize(order.paymentStatus)}</S.Table.Cell>
        <S.Table.Cell textAlign="center">
          <Box>
            {idx(order, (_) => _.creator.firstName)} {idx(order, (_) => _.creator.lastName)}
          </Box>
          <em>{idx(order, (_) => _.creator.id) ? `(${idx(order, (_) => _.creator.email)})` : ''}</em>
        </S.Table.Cell>
        <S.Table.Cell>
          <AssignOrder notMine={notMine} alias={alias} order={order} users={users} update={updateOrder} />
        </S.Table.Cell>
        <S.Table.Cell textAlign="center">{this.getManifestLink(order)}</S.Table.Cell>
        <S.Table.Cell>
          <OrdersActions order={order} deleteOrder={deleteOrder} />
        </S.Table.Cell>
      </S.Table.Row>
    )
  }
}

export default React.memo(OrderTableRow, (prevProps, nextProps) => {
  return JSON.stringify(prevProps) === JSON.stringify(nextProps)
})
