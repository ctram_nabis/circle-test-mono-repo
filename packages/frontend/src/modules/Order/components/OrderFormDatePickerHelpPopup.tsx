import { Box } from 'components'
import { S } from 'components/SemanticComponents'
import React from 'react'

export const OrderFormDatePickerHelpPopup = () => {
  return (
    <S.Popup
      flowing
      content={
        <Box>
          <p>A date may be unavailable for the following reasons:</p>
          <ul>
            <li>It is within the 2-3 day lead time</li>
            <li>It is a weekend or holiday</li>
            <li>We have reached the maximum number of orders for that day</li>
          </ul>
          <a
            href="https://nabis.zendesk.com/hc/en-us/articles/360022232291-When-is-the-soonest-date-that-I-can-set-for-an-order-"
            target="_blank"
            rel="noopener noreferrer"
          >
            More Info
          </a>
        </Box>
      }
      trigger={<S.Icon name="question circle outline" color="green" />}
    />
  )
}
