/* not done */
import React from 'react'
import { Icon, Menu, Table } from 'semantic-ui-react'

export class OrdersPagination extends React.Component<any, any> {
  getPageNumbers = (currentPage, totalPageNumbers) => {
    const newPageNumberRange: number[] = []

    const minNumTemp = Math.floor(currentPage / 10) * 10
    const minNum = Math.max(minNumTemp, 1)
    const maxNum = Math.min(minNumTemp + 10, totalPageNumbers)

    for (let i = minNum; i <= maxNum; i++) {
      newPageNumberRange.push(i)
    }

    return newPageNumberRange
  }

  render() {
    const { filters, filter } = this.props
    return (
      <Table.Row>
        <Table.HeaderCell colSpan={13}>
          <Menu pagination>
            {filters.page > 1 && (
              <Menu.Item icon onClick={() => filter('page', filters.page - 1)}>
                <Icon name="chevron left" />
              </Menu.Item>
            )}
            {this.getPageNumbers(filters.page, filters.totalNumPages).map((page) => (
              <Menu.Item key={page} as="a" active={page === filters.page} onClick={() => filter('page', page)}>
                {page}
              </Menu.Item>
            ))}
            {filters.page < filters.totalNumPages && (
              <Menu.Item icon onClick={() => filter('page', filters.page + 1)}>
                <Icon name="chevron right" />
              </Menu.Item>
            )}
          </Menu>
        </Table.HeaderCell>
      </Table.Row>
    )
  }
}
