import React from 'react'
import { Button, Form, Popup, TextArea, Header, Icon } from 'semantic-ui-react'

export default class AdminOrderNotes extends React.Component<any, any> {
  resumePrompt: any
  warningColor: any
  adminHover: any

  constructor(props) {
    super(props)
    const { notes = '', adminNotes = '', internalNotes = '' } = props.order
    this.state = {
      notes,
      adminNotes,
      internalNotes,
      isSaving: false,
    }
    this.resumePrompt = false
    this.warningColor = 'green'
  }
  PARENT_setPrompt = (status) => {
    this.resumePrompt = status
    this.warningColor = 'green'
    this.forceUpdate()
  }
  saveNotes = async (key) => {
    if (!['adminNotes', 'internalNotes'].includes(key)) {
      return
    }
    this.setState({
      isSaving: true,
    })
    const { id } = this.props.order
    if (this.resumePrompt === true && this.state.adminNotes !== this.props.order.adminNotes) {
      this.resumePrompt = false
      this.warningColor = 'green'
      this.props.resumeStatusChange({ [key]: this.state[key] })
    } else if (this.resumePrompt === false) {
      this.warningColor = 'green'
      await this.props.updateOrder({
        id,
        [key]: this.state[key],
      })
    } else {
      this.warningColor = 'red'
    }
    this.setState({
      isSaving: false,
    })
  }
  render() {
    const { notes, adminNotes, internalNotes, isSaving } = this.state
    return (
      <div>
        {notes && (
          <Popup
            flowing
            hoverable
            hideOnScroll
            content={notes}
            trigger={
              <div>
                <b style={{ textDecoration: 'underline', cursor: 'pointer' }}>Notes</b>
              </div>
            }
          />
        )}
        <Popup
          flowing
          hoverable
          position="bottom center"
          content={
            <Form>
              {this.props.order.adminNotes === this.state.adminNotes && this.resumePrompt && (
                <Header dividing color={this.warningColor} as="h4">
                  <Icon name="warning sign"></Icon>
                  Update the admin notes
                </Header>
              )}
              <TextArea
                placeholder="Add A Note"
                value={adminNotes}
                onChange={(e: any) => this.setState({ adminNotes: e.target.value })}
              />
              <Button
                style={{ marginTop: '5px' }}
                floated="right"
                size="mini"
                positive
                content="Save"
                onClick={() => this.saveNotes('adminNotes')}
                loading={isSaving}
                disabled={isSaving}
              />
            </Form>
          }
          trigger={
            <b
              ref={(element) => (this.adminHover = element)}
              style={{ textDecoration: 'underline', cursor: 'pointer' }}
            >
              Admin Notes
            </b>
          }
        />
        <Popup
          flowing
          hoverable
          position="bottom center"
          content={
            <Form>
              <TextArea
                placeholder="Nabis Only"
                value={internalNotes}
                onChange={(e: any) => this.setState({ internalNotes: e.target.value })}
              />
              <Button
                style={{ marginTop: '5px' }}
                floated="right"
                size="mini"
                positive
                content="Save"
                onClick={() => this.saveNotes('internalNotes')}
                loading={isSaving}
                disabled={isSaving}
              />
            </Form>
          }
          trigger={
            <b style={{ textDecoration: 'underline', cursor: 'pointer', display: 'block' }}>
              Internal Notes (Nabis Only)
            </b>
          }
        />
      </div>
    )
  }
}
