import { Loading } from 'components'
import { useGetUnavailableDatesQuery } from 'generated/graphql'
import { getMinimumBookableDate, isValidBookingDate, toDisplayDate } from 'modules/Date'
import { isDateBlacklisted } from 'modules/Date/utils/isDateBlacklisted'
import { useMe } from 'modules/User'
import moment from 'moment'
import React, { Fragment } from 'react'
import DatePicker from 'react-datepicker'
import { OrderFormDatePickerHelpPopup } from './OrderFormDatePickerHelpPopup'

interface Props {
  date: string
  onOrderDateChange: (datePickerInput: Date | null) => void
  isCanndescent: boolean
}

export function OrderFormDatePicker({ date, onOrderDateChange, isCanndescent = false }: Props) {
  const { data, loading } = useGetUnavailableDatesQuery()

  const { isAdmin: _isAdmin } = useMe()
  const isAdmin = Boolean(_isAdmin)

  const omitList = data?.getUnavailableDates ?? []
  const blacklistFilter = (_d: Date) => isDateBlacklisted(_d, omitList, isAdmin || isCanndescent)
  const filterDates = (d: Date) => !blacklistFilter(d) && isValidBookingDate(d, isAdmin)

  const earliestDeliveryDate = getMinimumBookableDate(isAdmin, blacklistFilter, isCanndescent)
  const deliveryDate = moment(date).utc()
  const defaultStart = earliestDeliveryDate.format('MM/DD/YYYY')

  return (
    <Fragment>
      {loading ? (
        <Loading />
      ) : (
        <Fragment>
          <label htmlFor="date">
            Delivery Date <OrderFormDatePickerHelpPopup />
          </label>
          <DatePicker
            autoComplete="off"
            name="date"
            selected={
              date && deliveryDate.isSameOrAfter(earliestDeliveryDate, 'day') ? toDisplayDate(new Date(date)) : null
            }
            onChange={(d) => onOrderDateChange(d)}
            minDate={earliestDeliveryDate.toDate()}
            filterDate={(d) => filterDates(d)}
            dateFormat="MM/dd/yyyy"
            required
            placeholderText={defaultStart}
          />
        </Fragment>
      )}
    </Fragment>
  )
}
