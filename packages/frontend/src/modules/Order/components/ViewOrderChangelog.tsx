import { S } from 'components/SemanticComponents'
import { Order } from 'generated/graphql'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import React from 'react'

interface Props {
  order: Order
}

export function ViewOrderChangeLog({ order }: Props) {
  const router = useRouter()
  const { organization } = useOrganization()

  return (
    <S.Button
      color="purple"
      size="tiny"
      icon="clipboard list"
      label="Changelog"
      onClick={() => {
        router.push(`/${organization.alias}/app/orders/${order.id}/changelog`)
      }}
    />
  )
}
