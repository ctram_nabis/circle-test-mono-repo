import { S } from 'components'
import { Order } from 'generated/graphql'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React from 'react'

interface Props {
  order: Order
}

export function ViewOrEditOrderButton({ order }: Props) {
  const { organization } = useOrganization()
  const { user } = useMe()
  const router = useRouter()

  const edit = user.isAdmin

  return (
    <S.Button
      color={edit ? 'yellow' : 'blue'}
      size="tiny"
      icon={edit ? 'edit' : 'magnify'}
      onClick={() => {
        router.push(`/${organization.alias}/app/orders/${order.id}`)
      }}
      label={edit ? 'Edit' : 'View'}
    />
  )
}
