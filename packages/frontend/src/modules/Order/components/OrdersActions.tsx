import { Box } from 'components/core/src/Box'
import { S } from 'components/SemanticComponents'
import { Order, Scalars } from 'generated/graphql'
import { DeleteConfirmationModal } from 'modules/Modal'
import { useMe } from 'modules/User'
import React from 'react'
import { CopyOrder } from './CopyOrder'
import { ViewOrderChangeLog } from './ViewOrderChangelog'
import { ViewOrEditOrderButton } from './ViewOrEditOrderButton'

interface Props {
  order: Order
  deleteOrder: (id: Scalars['ID']) => Promise<any>
}

export function OrdersActions(props: Props) {
  const { user } = useMe()
  const { deleteOrder, order } = props

  return (
    <Box display="flex" flexDirection="column" width={150} data-test="orders-actions">
      <ViewOrEditOrderButton order={order} />
      <CopyOrder order={order} />
      <ViewOrderChangeLog order={order} />
      {user.isAdmin && (
        <DeleteConfirmationModal
          open={false}
          delete={() => deleteOrder(order.id)}
          trigger={<S.Button color="red" size="tiny" icon="delete" label="Delete" />}
        />
      )}
    </Box>
  )
}
