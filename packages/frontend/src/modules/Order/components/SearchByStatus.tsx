import { Dropdown } from 'components/Dropdown'
import React from 'react'
import styled from 'styled-components'

const StyledDropdown = styled(Dropdown)`
  &.ui.fluid.selection.dropdown {
    width: 150px;
  }
`
export class SearchByStatus extends React.Component<any, any> {
  statuses = [
    // { key: 'DRAFT', value: 'DRAFT', text: 'Draft' },
    { key: 'ALL', value: 'ALL', text: 'All Statuses' },
    { key: 'UNSCHEDULED', value: 'UNSCHEDULED', text: 'Unscheduled' },
    { key: 'SCHEDULED', value: 'SCHEDULED', text: 'Scheduled' },
    { key: 'TRANSFERRING', value: 'TRANSFERRING', text: 'Transferring' },
    { key: 'DELIVERED', value: 'DELIVERED', text: 'Delivered' },
    {
      key: 'DELIVERED_WITH_EDITS',
      value: 'DELIVERED_WITH_EDITS',
      text: 'Delivered with Edits',
    },
    { key: 'DELAYED', value: 'DELAYED', text: 'Delayed' },
    { key: 'REJECTED', value: 'REJECTED', text: 'Rejected' },
    { key: 'CANCELLED', value: 'CANCELLED', text: 'Cancelled' },
  ]

  render() {
    const { status, filter } = this.props
    return (
      <StyledDropdown
        placeholder="Status"
        fluid
        selection
        options={this.statuses}
        value={status || 'ALL'}
        onChange={(_, d) => {
          if (d.value === 'ALL') filter('status', null)
          else filter('status', d.value)
        }}
      />
    )
  }
}
