/* not done */
import React from 'react'

import { Box, Ribbon, S, Text } from 'components'
import { OrdersPagination } from './OrdersPagination'
import { SearchByDate } from './SearchByDate'
import { SearchByPaymentStatus } from './SearchByPaymentStatus'
import { SearchByStatus } from './SearchByStatus'
import { SearchByString } from './SearchByString'

export class OrdersHeaders extends React.Component<any, any> {
  render() {
    const { filters, filter } = this.props
    return (
      <S.Table.Header>
        <OrdersPagination filters={filters} filter={filter} />
        <S.Table.Row>
          <S.Table.HeaderCell>
            <Box width={200}>
              <Ribbon color="green">Order #</Ribbon>
              <SearchByString name="number" string={filters.number} filter={filter} />
            </Box>
          </S.Table.HeaderCell>
          <S.Table.HeaderCell>
            <Box width={100}>Name</Box>
          </S.Table.HeaderCell>
          <S.Table.HeaderCell>
            Delivery Date
            <Text fontWeight={400}>
              <Box>
                <SearchByDate name="start" select={filters.start} filter={filter} />
              </Box>
              <Box>
                <SearchByDate name="end" select={filters.end} filter={filter} />
              </Box>
            </Text>
          </S.Table.HeaderCell>
          <S.Table.HeaderCell>Action</S.Table.HeaderCell>
          <S.Table.HeaderCell>GMV</S.Table.HeaderCell>
          <S.Table.HeaderCell>
            Payment
            <br />
            Terms
          </S.Table.HeaderCell>
          <S.Table.HeaderCell>
            <SearchByString name="site" string={filters.site} filter={filter} />
          </S.Table.HeaderCell>
          <S.Table.HeaderCell>
            <SearchByStatus filter={filter} status={filters.status} />
          </S.Table.HeaderCell>
          <S.Table.HeaderCell>
            <SearchByPaymentStatus filter={filter} status={filters.paymentStatus} />
          </S.Table.HeaderCell>
          <S.Table.HeaderCell>
            <SearchByString name="creator" string={filters.creator} filter={filter} />
          </S.Table.HeaderCell>
          <S.Table.HeaderCell>Sold By</S.Table.HeaderCell>
          <S.Table.HeaderCell>Manifest</S.Table.HeaderCell>
          <S.Table.HeaderCell>Actions</S.Table.HeaderCell>
        </S.Table.Row>
      </S.Table.Header>
    )
  }
}
