import { S } from 'components/SemanticComponents'
import { Order } from 'generated/graphql'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import React from 'react'

interface Props {
  order: Order
}

export function CopyOrder({ order }: Props) {
  const router = useRouter()
  const { organization } = useOrganization()

  return (
    <S.Button
      color="orange"
      size="tiny"
      icon="copy"
      onClick={() => {
        router.push({
          pathname: `/${organization.alias}/app/orders/new`,
          state: {
            orderId: order.id,
            copyOrder: true,
          },
        })
      }}
      label="Copy"
    />
  )
}
