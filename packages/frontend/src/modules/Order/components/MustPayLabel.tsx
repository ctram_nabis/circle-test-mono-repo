import React from 'react'

export const MustPayLabel = ({ linebreak = true }: { linebreak?: boolean }) => {
  return (
    <>
      Must Pay Previous Balance
      {linebreak ? <br /> : '\u00A0'}
      Prior to Delivery?
    </>
  )
}
