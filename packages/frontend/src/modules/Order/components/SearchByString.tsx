import React from 'react'
import { Search } from 'semantic-ui-react'

export class SearchByString extends React.Component<any, any> {
  render() {
    const { name, filter, string } = this.props
    return (
      <Search
        placeholder={`Order ${name}`}
        open={false}
        onSearchChange={(_e, d) => filter(name, d.value)}
        value={string}
        size="mini"
        style={{ paddingTop: '7px' }}
      />
    )
  }
}
