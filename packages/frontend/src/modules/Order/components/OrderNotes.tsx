import React from 'react'
import { Divider, Popup } from 'semantic-ui-react'
import styled from 'styled-components'

const TriggerStyled = styled.small`
  display: block;
  text-decoration: underline;
  cursor: pointer;
  font-weight: bold;
`

export const OrderNotes = ({ notes, adminNotes }) => {
  return (
    <div>
      {(notes || adminNotes) && <Divider />}
      {notes && <Popup flowing hoverable content={notes} trigger={<TriggerStyled>Notes</TriggerStyled>} />}
      {adminNotes && (
        <Popup
          position="bottom center"
          flowing
          hoverable
          content={adminNotes}
          trigger={<TriggerStyled>Admin Notes</TriggerStyled>}
        />
      )}
    </div>
  )
}
