import { OrderActionEnum, Warehouse } from 'generated/graphql'
import React from 'react'
import { Dropdown, Label, Segment } from 'semantic-ui-react'

interface Props {
  action?: OrderActionEnum
  allWarehouses: Warehouse[]
  handleWarehouseChange: (warehouseId: string) => void
  isAdmin: boolean
  warehouseId?: string
  isLoading: boolean
}

export const WarehouseDropdown = ({
  action,
  allWarehouses = [],
  handleWarehouseChange,
  isAdmin,
  warehouseId,
  isLoading,
}: Props) => {
  if (!allWarehouses?.map) {
    return null
  }

  const sourceWarehouseOptions = allWarehouses
    .map((w) => ({
      key: w.id,
      text: w.site ? `${w.site.name} | ${w.site.city}, ${w.site.state}` : 'error: site missing',
      value: w.id,
    }))
    .filter((option) => !!option.value)

  return (
    <Segment>
      <Label htmlFor="warehouse" attached="top">
        Source Warehouse
      </Label>
      <Dropdown
        name="warehouse"
        options={sourceWarehouseOptions}
        placeholder={`Select warehouse to ${
          action === 'PICKUP_FROM_BRAND' ? 'replenish' : 'allocate from'
        } (Default: Nabis, OAK)...`}
        selection
        loading={isLoading}
        disabled={!isAdmin || isLoading}
        value={warehouseId}
        onChange={(_e, data) => handleWarehouseChange(data.value as string)}
      />
    </Segment>
  )
}
