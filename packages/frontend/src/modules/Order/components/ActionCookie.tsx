import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'
import { ORDER_ACTIONS, OrderActionType } from '../constants'

export const ActionCookie = (props: { action: OrderActionType }) => {
  let cookieColorCss
  switch (props.action) {
    case ORDER_ACTIONS.DELIVERY_TO_RETAILER:
      cookieColorCss = '#44cb85'
      break
    case ORDER_ACTIONS.PICKUP_FROM_BRAND:
      cookieColorCss = '#949494'
      break
    case ORDER_ACTIONS.RETURN_FROM_RETAILER:
      cookieColorCss = '#f47196'
      break
    case ORDER_ACTIONS.PICKUP_FROM_NABIS:
      cookieColorCss = '#ffcd00'
      break
    case ORDER_ACTIONS.DROPOFF_TO_NABIS:
      cookieColorCss = '#0376e3'
      break
    case ORDER_ACTIONS.DELIVERY_TO_BRAND:
      cookieColorCss = '#6842f0'
      break
    case ORDER_ACTIONS.INTERNAL_TRANSFER:
      cookieColorCss = '#ebebea'
      break
    case ORDER_ACTIONS.PAYMENT_PICKUP_FROM_RETAILER:
      cookieColorCss = '#fe7731'
      break
    case ORDER_ACTIONS.OTHER:
      cookieColorCss = '#282828'
      break
    default:
      cookieColorCss = 'rgba(0, 0, 0, 0)'
  }

  const ColorCookie = styled.div`
    display: inline-block;
    background-color: ${cookieColorCss};
    width: 50%;
    height: 8px;
    border-radius: 1000px;
    margin-bottom: 8px;
  `

  return <ColorCookie />
}

ActionCookie.propTypes = {
  action: PropTypes.string.isRequired,
}
