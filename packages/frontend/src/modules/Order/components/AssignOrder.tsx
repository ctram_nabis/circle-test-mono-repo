import { UpdateOrderDocument } from 'generated/graphql'
import { client } from 'modules/Apollo'
import React from 'react'
import styled from 'styled-components'
import { oc } from 'ts-optchain'
import Dropdown from './Dropdown'

const StyledDropdown = styled(Dropdown)`
  &.ui.fluid.selection.dropdown {
    width: 250px;
  }
`
export class AssignOrder extends React.Component<any, any> {
  state = {
    loading: false,
    assignee: oc(this).props.order.assignee.id(''),
  }

  assignOrder = (order, user) => {
    const { assignee } = this.props.order
    if (assignee?.id === user) return
    this.setState({ loading: true })
    this.updateOrder(order.id, user).then(() => {
      setTimeout(() => {
        this.setState({ ...this.state, loading: false, assignee: user })
      }, 1000)
    })
  }

  updateOrder = (id: string, assigneeId: string) => {
    const response = client.mutate({
      mutation: UpdateOrderDocument,
      variables: {
        input: {
          id,
          assigneeId,
        },
      },
    })

    return response
  }

  users = () => {
    const { alias } = this.props
    if (!this.props.users.reduce) {
      return
    }

    return this.props.users.reduce(
      (a, u) => {
        if (
          u.email.toLowerCase().indexOf('getnabis.com') !== -1 &&
          alias.indexOf('nabis') === -1 &&
          alias.indexOf('nabione') === -1
        ) {
          return a
        }

        a.push({
          key: u.id,
          value: u.id,
          text: `
        ${u.firstName} ${u.lastName}
        ${u.email}
        `,
        })

        return a
      },
      [
        {
          key: 'Select',
          value: null,
          text: 'Select',
        },
      ],
    )
  }

  render() {
    const { order, notMine } = this.props
    const users = this.users()

    return (
      <StyledDropdown
        loading={this.state.loading}
        disabled={notMine}
        placeholder="Select"
        fluid
        selection
        options={users || []}
        value={this.state.assignee}
        onChange={(_, d) => {
          this.assignOrder(order, d.value)
        }}
      />
    )
  }
}
