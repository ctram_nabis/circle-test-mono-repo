import { Dropdown } from 'components/Dropdown'
import React from 'react'
import styled from 'styled-components'

const StyledDropdown = styled(Dropdown)`
  &.ui.fluid.selection.dropdown {
    width: 150px;
  }
`
export class SearchByPaymentStatus extends React.Component<any, any> {
  paymentStatuses = [
    { key: 'ALL', value: 'ALL', text: 'All Payment Statuses' },
    { key: 'UNPAID', value: 'UNPAID', text: 'Unpaid' },
    { key: 'COD_PAID', value: 'COD_PAID', text: 'COD, Paid' },
    { key: 'NET_TERMS_PAID', value: 'NET_TERMS_PAID', text: 'Net Terms, Paid' },
    { key: 'REMITTED', value: 'REMITTED', text: 'Remitted' },
    { key: 'SELF_COLLECTED', value: 'SELF_COLLECTED', text: 'Self Collected' },
  ]

  render() {
    const { status, filter } = this.props
    return (
      <StyledDropdown
        placeholder="Payment Status"
        fluid
        selection
        options={this.paymentStatuses}
        value={status || 'ALL'}
        onChange={(_, d) => {
          if (d.value === 'ALL') filter('paymentStatus', null)
          else filter('paymentStatus', d.value)
        }}
      />
    )
  }
}
