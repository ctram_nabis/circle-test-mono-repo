import { DatePicker } from 'components/DatePicker'
import moment from 'moment-timezone'
import React from 'react'
import styled from 'styled-components'

const StyledDatePicker = styled(DatePicker)`
  background-color: transparent;
  border: none;
`
export class SearchByDate extends React.Component<any, any> {
  dateFmt = 'LL-dd-yyyy'
  dateFmtMoment = 'MM-DD-YYYY'

  render() {
    const { name, filter } = this.props
    const { select } = this.props

    const isClearable = 'isClearable' in this.props ? this.props.isClearable : true

    return (
      <StyledDatePicker
        placeholderText={`${name} date`}
        autoComplete={name}
        name={name}
        dateFormat={this.dateFmt}
        selected={select ? moment(select, this.dateFmtMoment).toDate() : null}
        onChange={(date) => filter(name, date ? moment(date, this.dateFmtMoment) : null)}
        isClearable={isClearable}
        clearButtonTitle="clear"
      />
    )
  }
}
