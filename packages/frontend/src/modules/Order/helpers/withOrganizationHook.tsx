import { FullScreenLoader } from 'components/FullScreenLoader'
import { useOrganization } from 'modules/Organization'
import React from 'react'

export function withOrganizationHook(Component) {
  return function WrappedComponent(props) {
    const hook = useOrganization()
    return hook.organization ? <Component {...props} {...hook} /> : <FullScreenLoader />
  }
}
