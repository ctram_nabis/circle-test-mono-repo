import { OrderActionEnum } from 'generated/graphql'
import { Color } from 'system'

const ActionColorMap: Record<OrderActionEnum, Color> = {
  DELIVERY_TO_RETAILER: 'success',
  PICKUP_FROM_BRAND: 'shade3',
  RETURN_FROM_RETAILER: 'accent5',
  PICKUP_FROM_NABIS: 'brand',
  DROPOFF_TO_NABIS: 'accent8',
  DELIVERY_TO_BRAND: 'accent4',
  INTERNAL_TRANSFER: 'shade1',
  PAYMENT_PICKUP_FROM_RETAILER: 'accent3',
  OTHER: 'shade7',
}

export const getOrderActionColor = (action: OrderActionEnum) => ActionColorMap[action]
