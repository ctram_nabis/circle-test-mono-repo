import { FormikErrors } from 'formik'
import { Scalars } from 'generated/graphql'
import { ChangeData, ChangeEvent, ErrorType } from 'types'

/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */
export interface SharedFormikProps<Values extends object, T extends Scalars['DateTime'] | string> {
  errorMsg?: ErrorType
  errors?: FormikErrors<Values>
  label?: string
  name: keyof Values
  value: T
  values: Values
  onChange: (event: ChangeEvent, data: ChangeData) => void
}
