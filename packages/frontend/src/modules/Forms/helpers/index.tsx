import { View } from 'components/core/src/View'
import { Field } from 'components/Field'
import { FormField } from 'components/FormField'
import { Label } from 'components/Label'
import React from 'react'
import ReactDatePicker, { ReactDatePickerProps } from 'react-datepicker'
import { Checkbox, CheckboxProps, Dropdown, DropdownProps } from 'semantic-ui-react'
import * as Yup from 'yup'

export const createFormField = (props) => <FormField key={props.name} {...props} />
// TODO: Fix types for ReactDatePicker
// TODO: this will get fixed/replaced with chris component, but making the styling look good until it's replaced
export const createFormDatePicker = (
  props: ReactDatePickerProps & { label?: string; labelStyles?: object; values: any; errors: any },
) => {
  return (
    <Field key={props.name}>
      <View my={2} flex={1} display="flex" flexDirection="column">
        {props.label && <Label {...props.labelStyles}>{props.label}</Label>}
        <ReactDatePicker {...props} />
      </View>
    </Field>
  )
}
// TODO: this will get fixed/replaced with chris component, but making the styling look good until it's replaced
export const createFormDropdown = (props: DropdownProps & { label?: string; labelStyles?: object }) => (
  <Field key={props.name}>
    <View my={2} flex={1} display="flex" flexDirection="column">
      {props.label && <Label {...props.labelStyles}>{props.label}</Label>}
      <Dropdown {...props} />
    </View>
  </Field>
)

// TODO: this will get fixed/replaced with chris component, but making the styling look good until it's replaced
export const createFormCheckbox = ({ label = '', ...props }: CheckboxProps & { labelStyles?: object }) => (
  <Field key={props.name}>
    <View my={2} flex={1} display="flex" flexDirection="column">
      {label && <Label {...props.labelStyles}>{label}</Label>}
      <Checkbox {...props} />
    </View>
  </Field>
)
export const createValidationSchema = <T extends object>(obj: Yup.ObjectSchemaDefinition<T>) => Yup.object().shape(obj)
// export const createFormField = <T extends object>(props: FormFieldProps<T>) => <FormField {...props} />
// export const createFormDatePicker = <T extends object>(props: FormDatePickerProps<T>) => <FormDatePicker {...props} />
// export const createFormDropdown = <T extends object>(props: FormDropdownProps<T>) => <FormDropdown {...props} />
// export const createValidationSchema = <T extends object>(obj: Yup.ObjectSchemaDefinition<T>) => Yup.object().shape(obj)
export type InferType<T> = Yup.InferType<T>
