import * as Yup from 'yup'

const NANP_REGEXP = /^(\+\d{1,2}\s?)?1?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/

export const validators = {
  validate: Yup,
  boolean: Yup.boolean(),
  string: Yup.string().trim(),
  number: Yup.number(),
  oneOf: (
    arrayOfValues: any[],
    message?:
      | string
      | ((
          params: {
            values: any
          } & Partial<Yup.TestMessageParams>,
        ) => any)
      | undefined,
  ) => Yup.mixed().oneOf(arrayOfValues, message),
  mixed: Yup.mixed(),
  date: Yup.date().default(new Date()),
  array: Yup.array,
  arrayOfObjects: <T extends object>() => Yup.array().of<T>(Yup.object()),
  name: Yup.string().trim(),
  email: Yup.string()
    .trim()
    .lowercase()
    .email()
    .required('Email is required'),
  password: Yup.string().required('Password is required'),
  passwordSignUpConfirmation: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match'),
  newPassword: Yup.string()
    .required('Password is required')
    .max(30, 'Too long')
    .min(8, 'Too short'),
  passwordConfirmation: Yup.string().oneOf([Yup.ref('newPassword'), null], 'Passwords must match'),
  phoneNumber: Yup.string().test({
    name: 'phone',
    exclusive: true,
    message: 'Must be a valid phone number',
    test(value: string) {
      if (!value) return true
      return NANP_REGEXP.test(value)
    },
  }),
}
