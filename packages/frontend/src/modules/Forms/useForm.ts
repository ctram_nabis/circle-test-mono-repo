import { FormikConfig, useFormik } from 'formik'
import { InferType } from 'yup'

export const useForm = <V extends object>(config: FormikConfig<InferType<V>>) => useFormik(config)
