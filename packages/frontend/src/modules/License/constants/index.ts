import { LicenseCategoryEnum, LicenseTypeEnum } from 'generated/graphql'
import { getSelectOptionsFromType } from 'utils/helpers'

export const LICENSE_TYPES: Record<LicenseTypeEnum, LicenseTypeEnum> = {
  RETAILER: 'RETAILER',
  RETAILER_NONSTOREFRONT: 'RETAILER_NONSTOREFRONT',
  DISTRIBUTOR: 'DISTRIBUTOR',
  DISTRIBUTOR_TRANSPORT_ONLY: 'DISTRIBUTOR_TRANSPORT_ONLY',
  MANUFACTURER: 'MANUFACTURER',
  CULTIVATOR: 'CULTIVATOR',
  TESTING: 'TESTING',
  MICROBUSINESS: 'MICROBUSINESS',
  CANNABIS_EVENTS: 'CANNABIS_EVENTS',
}

export const LICENSE_TYPES_DESC: Record<LicenseTypeEnum, string> = {
  RETAILER: 'Retailer',
  RETAILER_NONSTOREFRONT: 'Retailer (Non-Storefront)',
  DISTRIBUTOR: 'Distributor',
  DISTRIBUTOR_TRANSPORT_ONLY: 'Distributor (Transport-Only)',
  MANUFACTURER: 'Manufacturer',
  CULTIVATOR: 'Cultivator',
  TESTING: 'Testing',
  MICROBUSINESS: 'Microbusiness',
  CANNABIS_EVENTS: 'Event',
}

export const LICENSE_TYPE_OPTIONS = getSelectOptionsFromType(LICENSE_TYPES_DESC)

/* -------- LICENSE CATEGORY --------- */

export const LICENSE_CATEGORY: Record<LicenseCategoryEnum, LicenseCategoryEnum> = {
  ADULT_USE: 'ADULT_USE',
  MEDICINAL: 'MEDICINAL',
  COMBINED: 'COMBINED',
}

export const LICENSE_CATEGORY_TYPES_DESC: Record<LicenseCategoryEnum, string> = {
  ADULT_USE: 'Adult-Use (Recreational)',
  MEDICINAL: 'Medicinal',
  COMBINED: 'Combined',
}

export const LICENSE_CATEGORY_TYPE_OPTIONS = getSelectOptionsFromType(LICENSE_CATEGORY_TYPES_DESC)
