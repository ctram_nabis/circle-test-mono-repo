/**
 * This modal needs to work for multiple flows, all critical for creating/updating Licenses for:
 *   - organizations (new org or editing an existing org),
 *   - licensed locations (Address Book or Order Form)
 *
 * ANY CHANGES TO THIS MODAL NEEDS TO BE SURE THAT THEY DON'T BREAK THE ABOVE FLOWS
 * +++++++++++++++++++++++++ BE CAREFUL!!!! +++++++++++++++++++++++++
 *
 * TODO: Future enhancement
 * Split modal for different screens, split for Adding and Editing. Still need to think about this and
 * compare to existing modal component
 */
// import { ErrorBox } from 'components'
import { P, S } from 'components'
import { useFormik } from 'formik'
import {
  CreateLicenseInput,
  License,
  LicensedLocation,
  Organization,
  UpdateLicenseInput,
  useCreateLicenseMutation,
  useUpdateLicenseMutation,
} from 'generated/graphql'
import cloneDeep from 'lodash/cloneDeep'
import { createFormDatePicker, createFormDropdown, createFormField } from 'modules/Forms'
import { AddressPOC } from 'modules/Organization'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useToggle } from 'react-use'
import { ErrorType } from 'types'
import { formatPhone } from 'utils'
import * as Yup from 'yup'
import { LICENSE_CATEGORY_TYPE_OPTIONS, LICENSE_TYPE_OPTIONS } from '../constants'

const INLINE_REQUIRED_MSG = ' * Required'

export const CreateOrUpdateLicenseValidationSchema = Yup.object().shape({
  nickname: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  category: Yup.mixed().oneOf(['ADULT_USE', 'MEDICINAL', 'COMBINED']),
  type: Yup.mixed()
    .oneOf([
      'RETAILER',
      'RETAILER_NONSTOREFRONT',
      'DISTRIBUTOR',
      'DISTRIBUTOR_TRANSPORT_ONLY',
      'MANUFACTURER',
      'CULTIVATOR',
      'TESTING',
      'MICROBUSINESS',
      'CANNABIS_EVENTS',
    ])
    .required(INLINE_REQUIRED_MSG),
  licenseNumber: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  legalEntityName: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  // TODO: issuance date should be less than expiration date
  issuanceDate: Yup.date()
    .required(INLINE_REQUIRED_MSG)
    .max(Yup.ref('expirationDate'), 'Issuance date must be before the expiration date'),
  expirationDate: Yup.date().required(INLINE_REQUIRED_MSG),
  contactName: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  contactPhone: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  contactEmail: Yup.string()
    .email()
    .trim(),
  address1: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  address2: Yup.string().trim(),
  city: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  state: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  zip: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
})

interface Props {
  action: 'ADD' | 'EDIT'
  address: AddressPOC
  license?: License
  licensedLocation?: Partial<LicensedLocation>
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: (license: Partial<License>) => any
  open?: boolean
  organization?: Partial<Organization>
  returnLicense?: (license: Partial<License>) => void
  trigger?: JSX.Element
}

export const AddEditLicensesModal = (props: Props) => {
  const {
    action,
    address,
    license,
    licensedLocation,
    onClose,
    onFailure,
    onOpen,
    onSuccess,
    open = true,
    organization,
    returnLicense,
    trigger,
  } = props

  const [isOpen, toggleModal] = useToggle(open)
  const [requestErrors, setRequestErrors] = useState()

  const refetchQueries = [
    'GetUser',
    'GetOrganization',
    'AllLicenses',
    'AllOrganizations',
    'AllSites',
    'AllSkusAndSites',
  ]

  const initialInput = { refetchQueries }

  // TODO: Need to handle errors
  const [createLicenseMutation, { loading: loadingCreate }] = useCreateLicenseMutation(initialInput)
  const [updateLicenseMutation, { loading: loadingUpdate }] = useUpdateLicenseMutation(initialInput)

  const handleResponse = (_license?: License | null, errors?: Error[]) => {
    if (errors) {
      setRequestErrors(errors)
      return onFailure && onFailure(errors)
    }

    toggleModal(false)
    return _license ? onSuccess && onSuccess(_license) : onFailure && onFailure(['Oops! No license was returned'])
  }

  // type InputValues = CreateLicenseInput | Omit<UpdateLicenseInput, 'id'>

  const { id: organizationId, owner = {} } = organization || {}

  const {
    firstName = '',
    lastName = '',
    contactName = '',
    contactPhone = '',
    contactEmail = '',
    address1 = address.address1 || '',
    address2 = address.address2 || '',
    city = address.city || '',
    state = address.state || 'CA',
    zip = address.zip || '',
    phone = '',
    nickname = '',
    category = 'ADULT_USE',
    type = 'RETAILER',
    licenseNumber = '',
    legalEntityName = '',
    issuanceDate = new Date(),
    expirationDate = new Date(),
  } = {
    ...owner,
    ...license,
  }

  const _contactPhone = contactPhone || (phone && formatPhone(phone))
  const _contactName = contactName || `${firstName} ${lastName}`

  const { id: licenseId } = license || {}

  // TODO: REPLACE WITH useForm and createValidationSchema helper
  const form = useFormik({
    validationSchema: CreateOrUpdateLicenseValidationSchema,
    initialValues: {
      address1,
      address2: address2 || '',
      city,
      state,
      zip,
      contactName: _contactName,
      contactPhone: _contactPhone,
      contactEmail,
      nickname,
      category,
      type,
      licenseNumber,
      legalEntityName,
      issuanceDate: issuanceDate ? moment(issuanceDate).toDate() : null,
      expirationDate: expirationDate ? moment(expirationDate).toDate() : null,
      organizationId,
      // siteId: site?.id, // TODO: need to import site?
    },
    onSubmit: async (inputs) => {
      // TODO: This is when we don't want to Create or Update a license just yet
      // Rather we want to return the in-memory state to another part of the app, e.g. creating a new org
      const _license: Omit<UpdateLicenseInput, 'id'> = cloneDeep(inputs)

      if (organization && organization.id) {
        _license.organizationId = organization.id
      }

      if (licensedLocation && licensedLocation.id) {
        _license.licensedLocationId = licensedLocation.id
      }

      if (returnLicense) {
        return returnLicense(_license as Partial<License>)
      }

      try {
        if (licenseId) {
          const { data, errors } = await updateLicenseMutation({
            variables: {
              input: {
                ..._license,
                id: licenseId,
              } as UpdateLicenseInput,
            },
          })

          const updatedLicense = data?.updateLicense?.changedLicense as License
          handleResponse(updatedLicense, errors)
          return
        } else {
          const { data, errors } = await createLicenseMutation({
            variables: {
              input: _license as CreateLicenseInput,
            },
          })
          const createdLicense = data?.createLicense?.changedLicense as License
          handleResponse(createdLicense, errors)
        }
      } catch (caughtError) {
        setRequestErrors(caughtError.graphQLErrors)
      }
    },
  })

  useEffect(() => {
    return isOpen ? onOpen && onOpen() : onClose && onClose()
  }, [isOpen, onOpen, onClose])

  const formFields = {
    legalEntityName: createFormField({
      type: 'text',
      size: 'large',
      label: 'Legal Entity Name*',
      placeholder: 'e.g. Nabione, Inc.',
      name: 'legalEntityName',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: true,
    }),
    nickname: createFormField({
      type: 'text',
      size: 'large',
      label: 'License Nickname*',
      placeholder: 'e.g. Nabione, Inc.',
      name: 'nickname',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: true,
    }),
    category: createFormDropdown({
      label: 'License Category*',
      name: 'category',
      placeholder: 'Select category...',
      search: true,
      selection: true,
      options: LICENSE_CATEGORY_TYPE_OPTIONS,
      values: form.values,
      errors: form.errors,
      onChange: (_e, d) => form.setFieldValue('category', d.value),
      onBlur: () => form.setFieldTouched('category'),
      value: form.values.category,
      required: true,
    }),
    type: createFormDropdown({
      label: 'License Type*',
      name: 'type',
      type: 'text',
      placeholder: 'Select type...',
      search: true,
      selection: true,
      options: LICENSE_TYPE_OPTIONS,
      values: form.values,
      errors: form.errors,
      onChange: (_e, d) => form.setFieldValue('type', d.value),
      onBlur: () => form.setFieldTouched('type'),
      value: form.values.type,
      required: true,
    }),
    licenseNumber: createFormField({
      type: 'text',
      size: 'large',
      label: 'License Number*',
      placeholder: 'e.g. A-1010101010',
      name: 'licenseNumber',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: true,
    }),
    // TODO: FIX ME WHEN WE REWRAP FORM
    issuanceDate: createFormDatePicker({
      label: 'Issuance Date*',
      name: 'issuanceDate',
      dateFormat: 'MM/dd/yyyy',
      placeholderText: '01/01/2019',
      values: form.values,
      errors: form.errors,
      onChange: (date) => {
        if (date) {
          form.setFieldValue('issuanceDate', date)
          const expDate = form.getFieldMeta('expirationDate')
          const expirationIsDefault = moment(expDate.value).isSame(moment(), 'day')
          if (expirationIsDefault && !expDate.touched) {
            form.setFieldValue(
              'expirationDate',
              moment(date)
                .add(1, 'year')
                .toDate(),
            )
          }
        }
      },
      onBlur: form.handleBlur,
      selected: form.values.issuanceDate,
      required: true,
    }),
    expirationDate: createFormDatePicker({
      label: 'Expiration Date*',
      name: 'expirationDate',
      values: form.values,
      errors: form.errors,
      selected: form.values.expirationDate,
      onChange: (date) => form.setFieldValue('expirationDate', date),
      onBlur: form.handleBlur,
      dateFormat: 'MM/dd/yyyy',
      placeholderText: '07/01/2018',
      required: true,
    }),
    contactName: createFormField({
      type: 'text',
      size: 'large',
      label: 'Contact Name*',
      placeholder: 'e.g. John Smith',
      name: 'contactName',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: true,
    }),
    contactPhone: createFormField({
      type: 'text',
      size: 'large',
      label: 'Contact Phone*',
      placeholder: '123-456-7890',
      name: 'contactPhone',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: true,
    }),
    contactEmail: createFormField({
      type: 'email',
      size: 'large',
      label: 'Contact Email',
      placeholder: 'jsmith@nabis.com',
      name: 'contactEmail',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: false,
    }),
    address1: createFormField({
      type: 'text',
      size: 'large',
      label: 'Address 1*',
      placeholder: 'e.g. 100 Cannabis Way',
      name: 'address1',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: true,
    }),
    address2: createFormField({
      type: 'text',
      size: 'large',
      label: 'Address 2',
      placeholder: 'e.g. Suite 1',
      name: 'address2',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
    }),
    city: createFormField({
      type: 'text',
      size: 'large',
      label: 'City*',
      placeholder: 'e.g. Oakland',
      name: 'city',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: true,
    }),
    state: createFormField({
      type: 'text',
      size: 'large',
      label: 'State*',
      placeholder: 'e.g. CA',
      name: 'state',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: true,
    }),
    zip: createFormField({
      type: 'text',
      size: 'large',
      label: 'Zip*',
      placeholder: 'e.g. 94621',
      name: 'zip',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      required: true,
    }),
  }

  const isEditMode = licenseId || action === 'EDIT'

  return (
    <S.Modal
      closeIcon
      open={isOpen}
      onOpen={() => toggleModal(true)}
      onClose={() => toggleModal(false)}
      trigger={trigger}
    >
      <S.Header icon={isEditMode ? 'edit' : 'plus'} content={isEditMode ? 'Edit License' : 'Create New License'} />

      <S.Modal.Content>
        <P>Enter in your license details.</P>
        <S.Form id="add-edit-license" onSubmit={form.handleSubmit}>
          {/* TODO: Reenable ErrorBox, replace demo implementation below */}
          {/* <ErrorBox errors={requestErrors} /> */}
          {requestErrors && requestErrors.map((e) => <>{e.message}</>)}

          <S.Form.Group widths="4">
            {formFields.legalEntityName}
            {formFields.nickname}
            {formFields.category}
            {formFields.type}
          </S.Form.Group>
          <S.Form.Group widths="three">
            {formFields.licenseNumber}
            {formFields.issuanceDate}
            {formFields.expirationDate}
          </S.Form.Group>

          <S.Divider />

          <S.Form.Group widths="equal">
            {formFields.contactName}
            {formFields.contactPhone}
            {formFields.contactEmail}
          </S.Form.Group>
          <S.Form.Group widths="4">
            {formFields.address1}
            {formFields.address2}
            {formFields.city}
            {formFields.state}
            {formFields.zip}
          </S.Form.Group>
        </S.Form>
      </S.Modal.Content>
      <S.Modal.Actions>
        <S.Button type="button" onClick={() => toggleModal(false)}>
          Cancel
        </S.Button>

        <S.Button
          type="submit"
          // `handleSubmit` is in the button onClick because this button needs to be outsisde of the form element
          // onClick={() => form.handleSubmit()}
          color={isEditMode ? 'yellow' : 'green'}
          loading={(!isEditMode ? loadingCreate : loadingUpdate) || form.isSubmitting}
          disabled={(!isEditMode ? loadingCreate : loadingUpdate) || form.isSubmitting}
          form="add-edit-license"
        >
          {isEditMode ? 'Update' : 'Create'}
        </S.Button>
      </S.Modal.Actions>
    </S.Modal>
  )
}
