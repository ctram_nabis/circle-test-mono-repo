import { MockedProvider } from '@apollo/react-testing'
import { mount } from 'enzyme'
import React from 'react'
import { AddEditLicensesModal } from './AddEditLicensesModal'

describe('AddEditLicensesModal component', () => {
  it('should render without crashing', () => {
    mount(
      <MockedProvider>
        <AddEditLicensesModal />
      </MockedProvider>,
    )
  })
})
