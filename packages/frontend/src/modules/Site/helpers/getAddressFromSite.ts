import { Site } from 'generated/graphql'

export const getAddressFromSite = <T extends Partial<Site>>({ address1, address2, city, state, zip }: T) =>
  `${address1}${address2 && `, ${address2}`}, ${city}, ${state} ${zip}`
