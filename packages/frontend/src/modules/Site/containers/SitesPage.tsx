import { Button } from 'components/Button'
import { Container } from 'components/Container'
import { Box } from 'components/core/src/Box'
import { ErrorBox } from 'components/ErrorBox'
import { PageHeader } from 'components/PageHeader'
import { Pagination } from 'components/Pagination'
import { Ribbon } from 'components/Ribbon'
import { S } from 'components/SemanticComponents'
import { LicensedLocation, useAllLicensedLocationsQuery } from 'generated/graphql'
import { usePageInfo } from 'hooks'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import { useDebounce } from 'react-use'
import styled from 'styled-components'
import { oc } from 'ts-optchain'
import { ErrorType } from 'types'
import { AddSiteModal } from './AddSiteModal'
import { SiteGridColumn } from './components'
import { EditSiteModal } from './EditSiteModal'
import { SiteRow } from './SiteRow'

// TODO: switch to the components once they are merged
const Wrapper = styled(Box)`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  width: 100%;
  max-width: 1600px;
  margin-bottom: 12px;
`

enum ModalEnum {
  EDIT = 'EDIT',
  ADD = 'ADD',
}

export const SitesPage = () => {
  const router = useRouter()
  const { isAdmin } = useMe()
  const { pageInfo, setPage } = usePageInfo()
  const [modalOpen, setModalOpen] = useState<ModalEnum | undefined>(undefined)
  const [openSite, setOpenSite] = useState<Partial<LicensedLocation> | undefined>(undefined)
  const searchQuery = (router.query && router.query.name) || ''
  const [searchTerm, setSearchTerm] = useState<string>(searchQuery)

  const { data, loading, error } = useAllLicensedLocationsQuery({
    variables: {
      name: searchQuery,
      showArchived: isAdmin,
      pageInfo: { ...pageInfo, page: Number(router.query.page) || 1 },
    },
  })

  const onModalOpen = (modal: ModalEnum, _site?: Partial<LicensedLocation>) => {
    setOpenSite(_site)
    setModalOpen(modal)
  }
  const onModalClose = () => {
    setOpenSite(undefined)
    setModalOpen(undefined)
  }

  const totalNumPages = oc(data).viewer.allLicensedLocations.pageInfo.totalNumPages(0)
  const { canCreate } = useOrganization()
  const onChange = (value: string) => {
    setSearchTerm(value)
  }

  const handlePageChange = (_e: any, d: any) => {
    setPage(d.activePage)
    if (!searchTerm) {
      router.push(`${router.pathname}${`?page=${d.activePage}`}`)
    } else {
      router.push(`${router.pathname}?name=${encodeURI(searchTerm)}${`&page=${d.activePage}`}`)
    }
  }

  useDebounce(
    () => {
      const currentPage = router.query.page
      if (!searchTerm) {
        router.push(`${router.pathname}${currentPage ? `?page=${currentPage}` : ''}`)
      } else {
        router.push(`${router.pathname}?name=${encodeURI(searchTerm)}${currentPage ? `&page=${currentPage}` : ''}`)
      }
    },
    500,
    [searchTerm],
  )

  const licensedLocationRows = data?.viewer?.allLicensedLocations.results
  const definitelyNoSites = licensedLocationRows?.length === 0

  return (
    <Container>
      <Wrapper>
        <S.Container>
          <S.Grid padded>
            <S.GridRow>
              <S.GridColumn>
                <SiteGridColumn>
                  <PageHeader>Address Book</PageHeader>
                </SiteGridColumn>
              </S.GridColumn>
            </S.GridRow>
            <S.GridRow>
              <S.GridColumn>
                <SiteGridColumn>
                  <Box>
                    <S.Search
                      placeholder="Search by name..."
                      open={false}
                      defaultValue={searchTerm}
                      autoFocus
                      onSearchChange={(_e, d) => onChange(d.value as string)}
                    />
                  </Box>
                  {canCreate.licenseLocation && (
                    <Button positive name="plus" title="New Address" onClick={() => onModalOpen(ModalEnum.ADD)} />
                  )}
                </SiteGridColumn>
              </S.GridColumn>
            </S.GridRow>
            <S.GridRow>
              <SiteGridColumn>
                <Pagination
                  onPageChange={handlePageChange}
                  activePage={pageInfo.page}
                  totalNumPages={totalNumPages}
                  siblingRange={3}
                />
              </SiteGridColumn>
            </S.GridRow>
            <S.Loader active={loading} inline="centered" size="large" content="Loading Addresses..." />
            <ErrorBox errors={error?.graphQLErrors as ErrorType[]} />
            {!loading && !error && (
              <S.GridRow>
                <S.GridColumn>
                  <S.Table celled unstackable selectable verticalAlign="middle" sortable color="green">
                    <S.Table.Header>
                      <S.Table.Row>
                        <S.Table.HeaderCell>
                          <Ribbon color="green">Address Name</Ribbon>
                        </S.Table.HeaderCell>
                        <S.Table.HeaderCell>Address</S.Table.HeaderCell>
                        <S.Table.HeaderCell>City</S.Table.HeaderCell>
                        <S.Table.HeaderCell>State</S.Table.HeaderCell>
                        <S.Table.HeaderCell>Zip</S.Table.HeaderCell>
                        {canCreate.licenseLocation && <S.Table.HeaderCell>Actions</S.Table.HeaderCell>}
                      </S.Table.Row>
                    </S.Table.Header>
                    <S.Table.Body>
                      {definitelyNoSites ? (
                        <S.Table.Row>
                          <S.Table.Cell>
                            We couldn't find any addresses. Please try a different search or add one if it's missing
                          </S.Table.Cell>
                        </S.Table.Row>
                      ) : (
                        licensedLocationRows?.map((site) => (
                          <SiteRow
                            key={site.id}
                            site={site}
                            canCreate={canCreate.licenseLocation}
                            onModalOpen={() => onModalOpen(ModalEnum.EDIT, site)}
                          />
                        ))
                      )}
                    </S.Table.Body>
                  </S.Table>
                </S.GridColumn>
              </S.GridRow>
            )}
            <S.GridRow centered>
              <S.GridColumn>
                {!loading && (
                  <Pagination
                    onPageChange={handlePageChange}
                    activePage={pageInfo.page}
                    totalNumPages={totalNumPages}
                    siblingRange={3}
                  />
                )}
              </S.GridColumn>
            </S.GridRow>
          </S.Grid>
        </S.Container>
      </Wrapper>
      {modalOpen === 'EDIT' && <EditSiteModal site={openSite} onClose={onModalClose} onSuccess={onModalClose} />}
      {modalOpen === 'ADD' && <AddSiteModal onClose={onModalClose} onSuccess={onModalClose} />}
    </Container>
  )
}
