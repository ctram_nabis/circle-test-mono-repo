import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { ErrorBox } from 'components/ErrorBox'
import { ErrorLabel } from 'components/ErrorLabel'
import { Field } from 'components/Field'
import { Form } from 'components/Form'
import { Grid } from 'components/Grid'
import { Label } from 'components/Label'
import { P } from 'components/P'
import { S } from 'components/SemanticComponents'
import {
  CreateLicensedLocationInput,
  CreateLicenseInput,
  License,
  SiteCategoryEnum,
  UpdateLicenseInput,
  useCreateLicensedLocationMutation,
} from 'generated/graphql'
import { createFormDropdown, createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import { getCoordsFromAddress } from 'modules/Google'
import { AddEditLicensesModal } from 'modules/License'
import { logger } from 'modules/Logger'
import React, { useState } from 'react'
import styled from 'styled-components'
import { ErrorType } from 'types'
import * as Yup from 'yup'
import { DeleteLicenseButton } from './DeleteLicenseButton'

const TextButton = styled(Button)`
  &.ui.button.basic.green {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
`

const INLINE_REQUIRED_MSG = ' * Required'

const siteCategoryEnum: SiteCategoryEnum[] = [
  'RETAILER',
  'MANUFACTURER',
  'CULTIVATOR',
  'CONSUMER',
  'DISTRIBUTOR',
  'MICROBUSINESS',
  'TESTING',
  'OTHER',
]

const validationSchema = createValidationSchema<CreateLicensedLocationInput>({
  name: validators.string.required(INLINE_REQUIRED_MSG),
  siteCategory: validators.string.oneOf(siteCategoryEnum).required(INLINE_REQUIRED_MSG) as Yup.Schema<SiteCategoryEnum>,
  address1: validators.string.required(INLINE_REQUIRED_MSG),
  address2: validators.string,
  city: validators.string.required(INLINE_REQUIRED_MSG),
  state: validators.string.required(INLINE_REQUIRED_MSG),
  zip: validators.string.required(INLINE_REQUIRED_MSG),
  licenses: validators.validate
    .array()
    .of(Yup.object())
    .min(1, 'Please add at least once license that belongs to your organization')
    .required(INLINE_REQUIRED_MSG) as any,
})

interface Props {
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: (site: string) => void
  open?: boolean
  trigger?: JSX.Element
}

enum LicenseModalEnum {
  EDIT = 'EDIT',
  ADD = 'ADD',
}

export const AddSiteModal = (props: Props) => {
  const { onClose, onFailure, onSuccess, open = true, trigger } = props
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()
  const [modalOpen, setModalOpen] = useState<LicenseModalEnum | undefined>(undefined)
  const [openLicense, setOpenLicense] = useState<Partial<CreateLicenseInput> | undefined>(undefined)
  const [openLicenseIndex, setOpenLicenseIndex] = useState<number | undefined>(undefined)

  const [createLicenseLocationMutation] = useCreateLicensedLocationMutation({
    refetchQueries: ['AllSites', 'AllSkusAndSites', 'AllLicensedLocations'],
  })

  const onModalOpen = (modal: LicenseModalEnum, _license?: Partial<CreateLicenseInput>, _index?: number) => {
    if (_index !== undefined) setOpenLicenseIndex(_index)
    if (_license) setOpenLicense(_license)
    setModalOpen(modal)
  }
  const onModalClose = () => {
    setOpenLicenseIndex(undefined)
    setOpenLicense(undefined)
    setModalOpen(undefined)
  }

  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      name: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      zip: '',
      siteCategory: 'RETAILER',
      licenses: [],
    },
    onSubmit: async (inputs: CreateLicensedLocationInput) => {
      try {
        const addressInputs = {
          address1: inputs.address1,
          address2: inputs.address2,
          city: inputs.city,
          state: inputs.state,
          zip: inputs.zip,
        }

        const coords = await getCoordsFromAddress(addressInputs)

        const response = await createLicenseLocationMutation({
          variables: {
            input: {
              ...inputs,
              ...coords,
            },
          },
        })

        if (response?.errors) {
          setRequestErrors(response.errors)
          return onFailure && onFailure(response.errors)
        }

        if (response?.data?.createLicensedLocation) {
          return onSuccess && onSuccess(response.data.createLicensedLocation)
        }
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors?.message)
        }
        return onFailure && onFailure(err)
      }
    },
  })

  const addLicense = (addedlicense: Partial<CreateLicenseInput>) => {
    const { licenses } = form.values
    const newLicenses = [...licenses, addedlicense]
    form.setFieldValue('licenses', newLicenses)
  }

  const editLicense = (partialEditedLicense: Partial<UpdateLicenseInput>) => {
    const index = openLicenseIndex
    if (index !== undefined) {
      const { licenses } = form.values
      const newLicenses: Array<Partial<CreateLicenseInput> | any> = [...licenses]
      newLicenses[index] = {
        ...newLicenses[index],
        ...partialEditedLicense,
      }
      form.setFieldValue('licenses', newLicenses)
    } else {
      setCatchError('License needs to be created first')
    }
  }

  const deleteLicense = (index: number) => {
    const { licenses } = form.values
    const newLicenses = [...licenses]
    newLicenses.splice(index, 1)
    form.setFieldValue('licenses', newLicenses)
  }

  type InputValues = typeof form.values

  const handleDropdownChange = (key: keyof InputValues, value: string) => form.setFieldValue(key, value)

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        required: true,
        label: 'Address Name*',
        name: 'name',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. Nabis Oakland Warehouse',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormDropdown({
        required: true,
        label: 'Address Category*',
        name: 'siteCategory',
        type: 'text',
        placeholder: 'Select category...',
        onChange: (_e, d) => handleDropdownChange('siteCategory', d.value as string),
        search: true,
        selection: true,
        options: [
          { key: 'RETAILER', value: 'RETAILER', text: 'Retailer' },
          {
            key: 'MANUFACTURER',
            value: 'MANUFACTURER',
            text: 'Manufacturer',
          },
          {
            key: 'DISTRIBUTOR',
            value: 'DISTRIBUTOR',
            text: 'Distributor',
          },
          {
            key: 'CULTIVATOR',
            value: 'CULTIVATOR',
            text: 'Cultivator',
          },
          { key: 'TESTING', value: 'TESTING', text: 'Testing' },
          {
            key: 'MICROBUSINESS',
            value: 'MICROBUSINESS',
            text: 'Microbusiness',
          },
          { key: 'CONSUMER', value: 'CONSUMER', text: 'Consumer' },
          { key: 'OTHER', value: 'OTHER', text: 'Other' },
        ],
        value: form.values.siteCategory,
        errors: form.errors,
      }),
    ],
    [
      <Box width="100%" key="divider">
        <S.Divider fitted />
      </Box>,
    ],
    [
      createFormField({
        required: true,
        label: 'Address 1*',
        name: 'address1',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. 300 Pendleton Way',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        label: 'Address 2',
        name: 'address2',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. Suite 348',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'City*',
        name: 'city',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. Oakland',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'State*',
        name: 'state',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. CA',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Zip*',
        name: 'zip',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. 94621',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
  ]

  return (
    <S.Modal closeIcon open={open} onClose={onClose} trigger={trigger}>
      <S.Header icon="plus" content="Create New Address" />
      <S.Modal.Content>
        <P>Enter in your address details.</P>
        <ErrorBox errors={requestErrors} error={catchError} />
        <Form id="add-site-form" onSubmit={form.handleSubmit}>
          {fieldsInGroups.map((fields, i) => (
            <S.FormGroup key={`formGroup-${i}`} widths="equal">
              {fields}
            </S.FormGroup>
          ))}
          <S.Divider />
          <S.Card fluid>
            <S.Card.Content>
              <Field>
                <ErrorLabel error={form.errors.licenses} />
                <Label>Licenses</Label>
                <S.Container>
                  {form?.values?.licenses &&
                    form?.values?.licenses.map((license: Partial<CreateLicenseInput> | any, index: number) => {
                      return (
                        <Grid key={`licenses-${index}`}>
                          <S.GridRow verticalAlign="bottom">
                            <S.GridColumn width={6}>
                              <Box>
                                {index + 1}. {license?.nickname}
                              </Box>
                            </S.GridColumn>
                            <S.GridColumn width={4}>
                              <Box>({license?.licenseNumber})</Box>
                            </S.GridColumn>
                            <S.GridColumn width={6}>
                              <TextButton
                                type="button"
                                color="green"
                                basic
                                title="Edit"
                                onClick={() => onModalOpen(LicenseModalEnum.EDIT, license, index)}
                              />
                              {(form?.values?.licenses || []).length > 1 && (
                                <DeleteLicenseButton
                                  isAddAction
                                  deleteLicense={() => deleteLicense(index)}
                                  licenseId={license.id}
                                />
                              )}
                            </S.GridColumn>
                          </S.GridRow>
                        </Grid>
                      )
                    })}
                  <S.Divider />
                  <Button
                    type="button"
                    color="green"
                    name="plus"
                    title="Add New License"
                    onClick={() => onModalOpen(LicenseModalEnum.ADD)}
                  />
                </S.Container>
              </Field>
            </S.Card.Content>
          </S.Card>
        </Form>
        {/* The ADD EDIT LICENSE MODALS  */}
        {modalOpen === 'EDIT' && (
          <AddEditLicensesModal
            address={{
              address1: form.values.address1,
              address2: form.values.address2,
              city: form.values.city,
              state: form.values.state,
              zip: form.values.zip,
            }}
            license={openLicense as License}
            action="EDIT"
            onClose={onModalClose}
            returnLicense={(_license: Partial<License>) => {
              editLicense(_license as CreateLicenseInput)
              onModalClose()
            }}
          />
        )}
        {modalOpen === 'ADD' && (
          <AddEditLicensesModal
            action="ADD"
            address={{
              address1: form.values.address1,
              address2: form.values.address2,
              city: form.values.city,
              state: form.values.state,
              zip: form.values.zip,
            }}
            onClose={onModalClose}
            returnLicense={(_license: Partial<License>) => {
              addLicense(_license as CreateLicenseInput)
              onModalClose()
            }}
          />
        )}
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button onClick={onClose} name="remove" title="Cancel" />
        <Button
          color="green"
          type="submit"
          form="add-site-form"
          name="checkmark"
          title="Create"
          disabled={form.isSubmitting}
          loading={form.isSubmitting}
        />
      </S.Modal.Actions>
    </S.Modal>
  )
}
