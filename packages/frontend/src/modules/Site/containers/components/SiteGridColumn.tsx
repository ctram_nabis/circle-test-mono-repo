import { GridColumn } from 'semantic-ui-react'
import styled from 'styled-components'

export const SiteGridColumn = styled(GridColumn)`
  display: flex !important;
  justify-content: space-between !important;
  align-items: center !important;
`
