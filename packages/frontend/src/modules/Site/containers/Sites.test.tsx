/**
 * @jest-environment jsdom
 */
import { MockedProvider } from '@apollo/react-testing'
import { shallow } from 'enzyme'
import 'jest-styled-components'
import React from 'react'
import { AddSiteModal } from './AddSiteModal'
import { DeleteLicenseButton } from './DeleteLicenseButton'
import { EditSiteModal } from './EditSiteModal'
import { NoSites } from './NoSites'
import { SiteRow } from './SiteRow'
import { SitesPage } from './SitesPage'

describe('`Sites` container', () => {
  it('`AddEditModalLicensesSection` renders without crashing', () => {
    const deleteLicense = jest.fn()
    shallow(
      <MockedProvider>
        <DeleteLicenseButton deleteLicense={deleteLicense} licenseId="5" />
      </MockedProvider>,
    )
  })
  it('`AddSiteModal` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <AddSiteModal />
      </MockedProvider>,
    )
  })
  it('`EditSiteModal` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <EditSiteModal />
      </MockedProvider>,
    )
  })
  it('`NoSites` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <NoSites canCreate={true} />
      </MockedProvider>,
    )
  })
  it('`SiteRow` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <SiteRow site={{ id: '' }} />
      </MockedProvider>,
    )
  })
  it('`SitesPage` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <SitesPage />
      </MockedProvider>,
    )
  })
})
