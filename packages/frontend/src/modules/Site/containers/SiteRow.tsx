import { S, Text } from 'components'
import { Button } from 'components/Button'
import {
  LicensedLocation,
  useDeleteLicensedLocationMutation,
  useUnarchiveLicensedLocationMutation,
} from 'generated/graphql'
import { DeleteConfirmationModal } from 'modules/Modal'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import { oc } from 'ts-optchain'

interface Props {
  site: Partial<LicensedLocation>
  canCreate?: boolean
  onModalOpen: () => void
}

export const SiteRow = ({ site, canCreate, onModalOpen }: Props) => {
  const { isAdmin } = useMe()
  const [isDeleteOpen, setDeleteOpen] = useState<true | undefined>(undefined)
  const [unarchiveLicensedLocation] = useUnarchiveLicensedLocationMutation({
    refetchQueries: ['AllSites', 'AllSkusAndSites', 'AllLicensedLocations'],
  })
  const [deleteLicensedLocationMutation] = useDeleteLicensedLocationMutation({
    refetchQueries: ['AllSites', 'AllSkusAndSites', 'AllLicensedLocations'],
  })
  const handleArchiveLocation = () => {
    deleteLicensedLocationMutation({
      variables: {
        input: {
          id: site?.id || '',
        },
      },
    })
  }

  const unArchiveLocation = async () => {
    const undoLocationArchiveConfirmText = 'Are you sure you want to restore this location?'

    if (site.isArchived) {
      if (!window.confirm(undoLocationArchiveConfirmText)) {
        return
      }

      await unarchiveLicensedLocation({
        variables: {
          input: {
            id: site?.id || '',
          },
        },
      })
    }
  }
  const { name, address1, address2, city, state, zip } = site
  return (
    <S.Table.Row onClick={() => /* this.goToOrder(o.id) */ {}}>
      <S.Table.Cell>
        <Text fontSize="bold">{name}</Text>
      </S.Table.Cell>
      <S.Table.Cell>{`${address1} ${oc(address2)('')}`}</S.Table.Cell>
      <S.Table.Cell>{city}</S.Table.Cell>
      <S.Table.Cell>{state}</S.Table.Cell>
      <S.Table.Cell>{zip}</S.Table.Cell>
      {canCreate && (
        <S.Table.Cell>
          {!site?.isArchived && <S.Button color="yellow" size="tiny" icon="edit" label="Edit" onClick={onModalOpen} />}
          {isAdmin && (
            <>
              {site.isArchived ? (
                <Button name="undo" title="Unarchive" onClick={unArchiveLocation} />
              ) : (
                <S.Button color="red" size="tiny" icon="delete" label="Archive" onClick={() => setDeleteOpen(true)} />
              )}
            </>
          )}
        </S.Table.Cell>
      )}
      {isDeleteOpen && (
        <DeleteConfirmationModal
          onSuccess={() => setDeleteOpen(undefined)}
          onClose={() => setDeleteOpen(undefined)}
          delete={handleArchiveLocation}
          isArchiveText
        />
      )}
    </S.Table.Row>
  )
}
