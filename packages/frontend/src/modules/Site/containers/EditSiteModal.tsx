import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { ErrorBox } from 'components/ErrorBox'
import { ErrorLabel } from 'components/ErrorLabel'
import { Field } from 'components/Field'
import { Form } from 'components/Form'
import { Grid } from 'components/Grid'
import { Label } from 'components/Label'
import { P } from 'components/P'
import { S } from 'components/SemanticComponents'
import {
  License,
  LicensedLocation,
  Maybe,
  SiteCategoryEnum,
  UpdateLicensedLocationInput,
  useUpdateLicensedLocationMutation,
} from 'generated/graphql'
import { GraphQLError } from 'graphql'
import cloneDeep from 'lodash/cloneDeep'
import { createFormDropdown, createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import { getCoordsFromAddress } from 'modules/Google'
import { AddEditLicensesModal } from 'modules/License'
import { logger } from 'modules/Logger'
import { useOrganization } from 'modules/Organization'
import React, { useState } from 'react'
import styled from 'styled-components'
import { ErrorType } from 'types'
import * as Yup from 'yup'
import { DeleteLicenseButton } from './DeleteLicenseButton'

const TextButton = styled(Button)`
  &.ui.button.basic.green {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
`

const INLINE_REQUIRED_MSG = ' * Required'

export const SiteCategoryOptions: SiteCategoryEnum[] = [
  'RETAILER',
  'MANUFACTURER',
  'CULTIVATOR',
  'CONSUMER',
  'DISTRIBUTOR',
  'MICROBUSINESS',
  'TESTING',
  'OTHER',
]

const EditSiteValidation = createValidationSchema<
  Omit<UpdateLicensedLocationInput, 'licenses'> & { licenses: Maybe<Array<Partial<License>>> }
>({
  id: validators.string.required(),
  name: validators.string.required(INLINE_REQUIRED_MSG),
  siteCategory: validators.validate.mixed().oneOf(SiteCategoryOptions),
  address1: validators.string.required(INLINE_REQUIRED_MSG),
  address2: validators.string,
  city: validators.string.required(INLINE_REQUIRED_MSG),
  state: validators.string.required(INLINE_REQUIRED_MSG),
  lat: validators.number,
  lng: validators.number,
  zip: validators.string.required(INLINE_REQUIRED_MSG),
  licenses: validators.validate
    .array()
    .of(Yup.object())
    .min(1, 'Please add at least once license that belongs to your organization')
    .required(INLINE_REQUIRED_MSG) as any,
})

interface Props {
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: () => void
  open?: boolean
  site?: Partial<LicensedLocation>
  trigger?: JSX.Element
}

enum LicenseModalEnum {
  EDIT = 'EDIT',
  ADD = 'ADD',
}

export const EditSiteModal = (props: Props) => {
  const { onClose, onFailure, onSuccess, open = true, site, trigger } = props
  const { organization } = useOrganization()
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()
  const [modalOpen, setModalOpen] = useState<LicenseModalEnum | undefined>(undefined)
  const [openLicense, setOpenLicense] = useState<Partial<License> | undefined>(undefined)

  const [updateLicenseLocationMutation, { error }] = useUpdateLicensedLocationMutation({
    refetchQueries: ['AllSites', 'AllSkusAndSites', 'AllLicensedLocations'],
  })

  const onModalOpen = (modal: LicenseModalEnum, _license?: Partial<License>) => {
    setOpenLicense(_license)
    setModalOpen(modal)
  }
  const onModalClose = () => {
    setOpenLicense(undefined)
    setModalOpen(undefined)
  }

  const form = useForm<typeof EditSiteValidation>({
    validationSchema: EditSiteValidation,
    initialValues: {
      id: site?.id || '',
      name: site?.name || '',
      address1: site?.address1 || '',
      address2: site?.address2 || '',
      city: site?.city || '',
      state: site?.state || '',
      lat: site?.lat,
      lng: site?.lng,
      zip: site?.zip || '',
      licenses: site?.licenses as Maybe<Array<Partial<License>>>,
      siteCategory: site?.siteCategory || 'RETAILER',
    },
    onSubmit: async (inputs) => {
      try {
        const inputsToSend = { ...inputs }
        delete inputsToSend.licenses
        const addressInputs = {
          address1: inputs.address1,
          address2: inputs.address2,
          city: inputs.city,
          state: inputs.state,
          zip: inputs.zip,
        }

        const coords = await getCoordsFromAddress(addressInputs)
        const inputsWithCoords = { ...inputsToSend, ...coords }
        const response = await updateLicenseLocationMutation({
          variables: {
            input: inputsWithCoords as Omit<UpdateLicensedLocationInput, 'licenses'>,
          },
        })

        if (response?.errors) {
          setRequestErrors(response.errors)
          if (onFailure) {
            onFailure(response.errors)
          }
          return
        } else {
          return onSuccess && onSuccess()
        }
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors?.message)
        }
        if (onFailure) {
          onFailure(err?.graphQLErrors)
        }
      }
    },
  })

  const deleteLicense = (index: number) => {
    const currentLicenses = form?.values?.licenses || []
    const newLicenses: Array<Partial<License> | any> = currentLicenses.slice(0)
    newLicenses.splice(index, 1)
    form.setFieldValue('licenses', newLicenses)
  }

  type InputValues = typeof form.values

  const handleDropdownChange = (key: keyof InputValues, value: SiteCategoryEnum) =>
    form.setFieldValue(key as string, value)

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        required: true,
        label: 'Address Name*',
        name: 'name',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. Nabis Oakland Warehouse',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormDropdown({
        required: true,
        label: 'Address Category*',
        name: 'siteCategory',
        type: 'text',
        placeholder: 'Select category...',
        onChange: (_e, d) => handleDropdownChange('siteCategory', d.value as SiteCategoryEnum),
        search: true,
        selection: true,
        options: [
          { key: 'RETAILER', value: 'RETAILER', text: 'Retailer' },
          {
            key: 'MANUFACTURER',
            value: 'MANUFACTURER',
            text: 'Manufacturer',
          },
          {
            key: 'DISTRIBUTOR',
            value: 'DISTRIBUTOR',
            text: 'Distributor',
          },
          {
            key: 'CULTIVATOR',
            value: 'CULTIVATOR',
            text: 'Cultivator',
          },
          { key: 'TESTING', value: 'TESTING', text: 'Testing' },
          {
            key: 'MICROBUSINESS',
            value: 'MICROBUSINESS',
            text: 'Microbusiness',
          },
          { key: 'CONSUMER', value: 'CONSUMER', text: 'Consumer' },
          { key: 'OTHER', value: 'OTHER', text: 'Other' },
        ],
        defaultValue: form?.values?.siteCategory || 'RETAILER',
        values: form.values,
        errors: form.errors,
      }),
    ],
    [
      <Box width="100%" key="divider">
        <S.Divider fitted />
      </Box>,
    ],
    [
      createFormField({
        required: true,
        label: 'Address 1*',
        name: 'address1',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. 300 Pendleton Way',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        label: 'Address 2',
        name: 'address2',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. Suite 348',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'City*',
        name: 'city',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. Oakland',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'State*',
        name: 'state',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. CA',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Zip*',
        name: 'zip',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'e.g. 94621',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
  ]
  const licenses = form?.values?.licenses
  const requestError = error?.graphQLErrors as GraphQLError[]
  return (
    <S.Modal closeIcon open={open} onClose={onClose} trigger={trigger}>
      <S.Header icon="edit" content="Edit Address" />
      <S.Modal.Content>
        <P>Enter in your address details.</P>
        <ErrorBox errors={requestError || requestErrors} error={catchError} />
        <Form id="edit-site-form" onSubmit={form.handleSubmit}>
          {fieldsInGroups.map((fields, i) => (
            <S.FormGroup key={`formGroup-${i}`} widths="equal">
              {fields}
            </S.FormGroup>
          ))}
          <S.Divider />
          <S.Card fluid>
            <S.Card.Content>
              <Field>
                <Label>Licenses</Label>
                <ErrorLabel error={form.errors.licenses} />
                <S.Container>
                  {licenses &&
                    licenses.map((license: Partial<License> | any, index: number) => {
                      return (
                        <Grid key={`licenses-${index}`}>
                          <S.GridRow verticalAlign="bottom">
                            <S.GridColumn width={6}>
                              <Box>
                                {index + 1}. {license?.nickname}
                              </Box>
                            </S.GridColumn>
                            <S.GridColumn width={4}>
                              <Box>({license?.licenseNumber})</Box>
                            </S.GridColumn>
                            <S.GridColumn width={6}>
                              <TextButton
                                basic
                                color="green"
                                type="button"
                                title="Edit"
                                onClick={() => onModalOpen(LicenseModalEnum.EDIT, license)}
                              />
                              {(licenses || []).length > 1 && (
                                <DeleteLicenseButton
                                  deleteLicense={() => deleteLicense(index)}
                                  licenseId={license?.id}
                                />
                              )}
                            </S.GridColumn>
                          </S.GridRow>
                        </Grid>
                      )
                    })}
                  <S.Divider />
                  <Button
                    color="green"
                    name="plus"
                    type="button"
                    form="add-license"
                    title="Add New License"
                    onClick={() => onModalOpen(LicenseModalEnum.ADD)}
                  />
                </S.Container>
              </Field>
            </S.Card.Content>
          </S.Card>
        </Form>
        {/* The ADD EDIT LICENSE MODALS  */}
        {modalOpen === 'EDIT' && (
          <AddEditLicensesModal
            address={{
              address1: form.values.address1,
              address2: form.values.address2,
              city: form.values.city,
              state: form.values.state,
              zip: form.values.zip,
            }}
            organization={organization}
            license={openLicense as License}
            action="EDIT"
            onClose={onModalClose}
            onSuccess={(license) => {
              // Update recently edited license to form's `licenses` state
              const clonedLicenses = cloneDeep(form.values.licenses || [])
              // TODO: need to change below to ID but typescript won't take id since it's not in CREATELICENCEINPUT
              const licenseIndex = clonedLicenses.findIndex((lic) => lic?.id === license.id)

              if (licenseIndex > -1) {
                clonedLicenses[licenseIndex] = license
              } else {
                // TODO: Need to handle this better
                alert('Error: Unable to find edited license')
              }

              // Set form state
              form.setFieldValue('licenses', clonedLicenses)
            }}
            licensedLocation={site as Partial<LicensedLocation>}
          />
        )}
        {modalOpen === 'ADD' && (
          <AddEditLicensesModal
            action="ADD"
            address={{
              address1: form.values.address1,
              address2: form.values.address2,
              city: form.values.city,
              state: form.values.state,
              zip: form.values.zip,
            }}
            organization={organization}
            onClose={onModalClose}
            onSuccess={(license) => {
              // Add recently created license to form's `licenses` state
              const clonedLicenses = cloneDeep<Array<Partial<License>>>(form?.values?.licenses || [])
              clonedLicenses.push(license)

              // Set form state
              form.setFieldValue('licenses', clonedLicenses)
            }}
            licensedLocation={site as Partial<LicensedLocation>} // TODO: do we use Site or LicensedLocation????
          />
        )}
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button onClick={onClose} name="remove" title="Cancel" />
        <Button
          name="check"
          color="yellow"
          title="Update"
          type="submit"
          form="edit-site-form"
          disabled={form.isSubmitting}
          loading={form.isSubmitting}
        />
      </S.Modal.Actions>
    </S.Modal>
  )
}
