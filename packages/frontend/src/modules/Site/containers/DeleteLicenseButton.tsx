import { Button } from 'components/Button'
import { useDeleteLicenseMutation } from 'generated/graphql'
import { DeleteConfirmationModal } from 'modules/Modal'
import React, { useState } from 'react'
import styled from 'styled-components'

const TextButton = styled(Button)`
  &.ui.button.basic.red {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
`
interface Props {
  licenseId: string
  deleteLicense: () => void
  isAddAction?: boolean
}

export const DeleteLicenseButton = ({ licenseId, deleteLicense, isAddAction = false }: Props) => {
  const [isDeleteOpen, setDeleteOpen] = useState<true | undefined>(undefined)
  const [deleteLicenseMutation] = useDeleteLicenseMutation({
    refetchQueries: ['AllSites', 'AllSkusAndSites', 'AllLicenses'],
  })
  const handleDeletedLicense = () => {
    setDeleteOpen(undefined)
    return deleteLicense()
  }

  if (isAddAction) {
    return <TextButton color="red" type="button" basic title="Delete" onClick={deleteLicense} />
  }
  return (
    <>
      <TextButton color="red" basic title="Delete" type="button" onClick={() => setDeleteOpen(true)} />
      {isDeleteOpen && (
        <DeleteConfirmationModal
          onClose={() => setDeleteOpen(undefined)}
          onSuccess={handleDeletedLicense}
          delete={() => deleteLicenseMutation({ variables: { input: { id: licenseId } } })}
        />
      )}
    </>
  )
}
