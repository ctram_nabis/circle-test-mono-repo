import { S } from 'components'
import { useDeleteVehicleMutation, Vehicle } from 'generated/graphql'
import { DeleteConfirmationModal } from 'modules/Modal'
import React, { useState } from 'react'
import { ModalEnum } from './Vehicles'
interface Props {
  vehicle: Partial<Vehicle>
  onModalOpen: (modal: ModalEnum, driver: Partial<Vehicle>) => void
}

export const VehiclesRow = ({ onModalOpen, vehicle }: Props) => {
  const { name, licensePlate, make, model, year } = vehicle
  const [isDeleteOpen, setDeleteOpen] = useState<true | undefined>(undefined)
  const [deleteVehicleMutation, { loading }] = useDeleteVehicleMutation({
    refetchQueries: ['AllDriversVehicles'],
  })
  const handleDeleteVehicle = async () => {
    if (!vehicle || !vehicle.id) return
    await deleteVehicleMutation({
      variables: {
        input: {
          id: vehicle.id,
        },
      },
    })
  }
  return (
    <>
      <S.Table.Row>
        <S.Table.Cell>{name}</S.Table.Cell>
        <S.Table.Cell>{licensePlate}</S.Table.Cell>
        <S.Table.Cell>{make}</S.Table.Cell>
        <S.Table.Cell>{model}</S.Table.Cell>
        <S.Table.Cell>{year}</S.Table.Cell>
        <S.Table.Cell>
          <S.Button
            color="yellow"
            size="tiny"
            icon="edit"
            label="Edit"
            disabled={loading}
            onClick={() => onModalOpen(ModalEnum.EDIT, vehicle)}
          />
          <S.Button
            color="red"
            size="tiny"
            icon="delete"
            label="Delete"
            disabled={loading}
            onClick={() => setDeleteOpen(true)}
          />
        </S.Table.Cell>
      </S.Table.Row>
      {isDeleteOpen && (
        <DeleteConfirmationModal
          delete={handleDeleteVehicle}
          onSuccess={() => setDeleteOpen(undefined)}
          onClose={() => setDeleteOpen(undefined)}
        />
      )}
    </>
  )
}
