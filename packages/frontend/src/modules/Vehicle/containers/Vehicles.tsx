import { Button } from 'components/Button'
import { CenterView } from 'components/CenterView'
import { PageHeader } from 'components/PageHeader'
import { Ribbon } from 'components/Ribbon'
import { S } from 'components/SemanticComponents'
import { Vehicle } from 'generated/graphql'
import React, { Fragment, useState } from 'react'
import { AddVehicleModal, EditVehicleModal } from './AddEditVehicleModal'
import { VehiclesRow } from './VehiclesRow'

interface Props {
  vehicles: Array<Partial<Vehicle>>
}

export enum ModalEnum {
  EDIT = 'EDIT',
  ADD = 'ADD',
}

export const Vehicles = (props: Props) => {
  const { vehicles } = props
  const [modalOpen, setModalOpen] = useState<ModalEnum | undefined>(undefined)
  const [openVehicle, setOpenVehicle] = useState<Partial<Vehicle> | undefined>(undefined)

  const onModalOpen = (modal: ModalEnum, _vehicle?: Partial<Vehicle>) => {
    setOpenVehicle(_vehicle)
    setModalOpen(modal)
  }
  const onModalClose = () => {
    setOpenVehicle(undefined)
    setModalOpen(undefined)
  }
  return (
    <Fragment>
      <S.GridRow>
        {/* Vehicles */}
        <S.GridColumn>
          <CenterView>
            <PageHeader>Vehicles</PageHeader>
            <Button positive name="plus" title="New Vehicle" onClick={() => onModalOpen(ModalEnum.ADD)} />
          </CenterView>
        </S.GridColumn>
      </S.GridRow>
      <S.GridRow>
        <S.GridColumn>
          <S.Table celled padded selectable unstackable verticalAlign="middle" sortable color="green">
            <S.Table.Header>
              <S.Table.Row>
                <S.Table.HeaderCell>
                  <Ribbon color="green">Name</Ribbon>
                </S.Table.HeaderCell>
                <S.Table.HeaderCell>Plate</S.Table.HeaderCell>
                <S.Table.HeaderCell>Make</S.Table.HeaderCell>
                <S.Table.HeaderCell>Model</S.Table.HeaderCell>
                <S.Table.HeaderCell>Year</S.Table.HeaderCell>
                <S.Table.HeaderCell>Actions</S.Table.HeaderCell>
              </S.Table.Row>
            </S.Table.Header>
            <S.Table.Body>
              {vehicles.map((vehicle, idx) => (
                <VehiclesRow key={idx + 1} vehicle={vehicle} onModalOpen={onModalOpen} />
              ))}
            </S.Table.Body>
          </S.Table>
        </S.GridColumn>
      </S.GridRow>
      {modalOpen === 'EDIT' && (
        <EditVehicleModal vehicle={openVehicle} onClose={onModalClose} onSuccess={onModalClose} />
      )}
      {modalOpen === 'ADD' && <AddVehicleModal onClose={onModalClose} onSuccess={onModalClose} />}
    </Fragment>
  )
}
