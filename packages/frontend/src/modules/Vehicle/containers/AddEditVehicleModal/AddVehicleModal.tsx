import { Button, ErrorBox, Form, P, S } from 'components'
import { CreateVehicleInput, useCreateVehicleMutation } from 'generated/graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import { logger } from 'modules/Logger'
import React, { useState } from 'react'
import { ErrorType } from 'types'

const INLINE_REQUIRED_MSG = ' * Required'
const validationSchema = createValidationSchema<CreateVehicleInput>({
  name: validators.string.required(INLINE_REQUIRED_MSG),
  licensePlate: validators.string.required(INLINE_REQUIRED_MSG),
  make: validators.string.required(INLINE_REQUIRED_MSG),
  model: validators.string.required(INLINE_REQUIRED_MSG),
  year: validators.number.required(INLINE_REQUIRED_MSG),
})
interface Props {
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: () => void
  open?: boolean
}

export function AddVehicleModal(props: Props) {
  const { open = true, onSuccess, onFailure, onOpen, onClose } = props

  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()
  const [isOpen, setIsOpen] = useState(open)

  const [createVehicleMutation] = useCreateVehicleMutation({
    refetchQueries: ['AllDriversVehicles'],
  })

  const handleClose = () => {
    setIsOpen(false)
    if (onClose) onClose()
  }

  const handleOpen = () => {
    setIsOpen(true)
    if (onOpen) onOpen()
  }
  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      name: '',
      licensePlate: '',
      make: '',
      model: '',
      year: null,
    },
    onSubmit: async (inputs) => {
      try {
        const { year } = inputs
        const newVehicle = { ...inputs, year: Number(year) }
        const response = await createVehicleMutation({
          variables: {
            input: newVehicle,
          },
        })
        if (response.errors) {
          setRequestErrors(response.errors)
          return onFailure && onFailure(response.errors)
        } else {
          return onSuccess && onSuccess()
        }
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors?.message)
        }
        return onFailure && onFailure(err.message)
      }
    },
  })

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        required: true,
        label: 'Name',
        name: 'name',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'S0',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Plate',
        name: 'licensePlate',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'XVANITYX',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'Make',
        name: 'make',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Mercedes-Benz',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Model',
        name: 'model',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Sprinter Cargo Van',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Year',
        name: 'year',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: '2018',
        type: 'number',
        values: form.values,
        errors: form.errors,
      }),
    ],
  ]

  return (
    <S.Modal closeIcon open={isOpen} onOpen={handleOpen} onClose={handleClose}>
      <S.Header icon="plus" content="Create New Vehicle" />
      <S.Modal.Content>
        <P>Enter in the vehicle information</P>
        <Form onSubmit={form.handleSubmit} id="add-vehicle-form">
          <ErrorBox errors={requestErrors} error={catchError} />
          {fieldsInGroups.map((fields, i) => (
            // TODO: maybe create a helper function?
            <S.FormGroup key={`formGroup-${i}`} widths="equal">
              {fields}
            </S.FormGroup>
          ))}
        </Form>
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button name="x" type="button" onClick={handleClose} title="Cancel" />
        <Button
          type="submit"
          name="check"
          color="green"
          title="Create"
          disabled={form.isSubmitting}
          loading={form.isSubmitting}
          form="add-vehicle-form"
        />
      </S.Modal.Actions>
    </S.Modal>
  )
}
