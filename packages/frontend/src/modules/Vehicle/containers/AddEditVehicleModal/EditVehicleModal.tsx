import { Button, ErrorBox, Form, P, S } from 'components'
import { UpdateVehicleInput, useUpdateVehicleMutation, Vehicle } from 'generated/graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import { logger } from 'modules/Logger'
import React, { useState } from 'react'
import { ErrorType } from 'types'

const INLINE_REQUIRED_MSG = ' * Required'
const validationSchema = createValidationSchema<UpdateVehicleInput>({
  id: validators.string.required(INLINE_REQUIRED_MSG),
  name: validators.string.required(INLINE_REQUIRED_MSG),
  licensePlate: validators.string.required(INLINE_REQUIRED_MSG),
  make: validators.string.required(INLINE_REQUIRED_MSG),
  model: validators.string.required(INLINE_REQUIRED_MSG),
  year: validators.number.required(INLINE_REQUIRED_MSG),
})

interface Props {
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: () => void
  open?: boolean
  vehicle?: Partial<Vehicle>
}

export function EditVehicleModal(props: Props) {
  const { vehicle, open = true, onSuccess, onFailure, onOpen, onClose } = props
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()
  const [isOpen, setIsOpen] = useState(open)

  const [updateVehicleMutation] = useUpdateVehicleMutation({
    refetchQueries: ['AllDriversVehicles'],
  })

  const handleClose = () => {
    setIsOpen(false)
    if (onClose) onClose()
  }

  const handleOpen = () => {
    setIsOpen(true)
    if (onOpen) onOpen()
  }

  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      id: vehicle?.id || '',
      name: vehicle?.name || '',
      licensePlate: vehicle?.licensePlate || '',
      make: vehicle?.make || '',
      model: vehicle?.model || '',
      year: vehicle?.year || null,
    },
    onSubmit: async (inputs) => {
      try {
        const { year } = inputs
        const newVehicle = { ...inputs, year: Number(year) }
        const response = await updateVehicleMutation({
          variables: {
            input: newVehicle,
          },
        })
        if (response.errors) {
          setRequestErrors(response.errors)
          return onFailure && onFailure(response.errors)
        } else {
          return onSuccess && onSuccess()
        }
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors?.message)
        }
        return onFailure && onFailure(err.message)
      }
    },
  })
  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        required: true,
        label: 'Name',
        name: 'name',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'S0',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Plate',
        name: 'licensePlate',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'XVANITYX',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'Make',
        name: 'make',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Mercedes-Benz',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Model',
        name: 'model',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Sprinter Cargo Van',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Year',
        name: 'year',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: '2019',
        type: 'number',
        values: form.values,
        errors: form.errors,
      }),
    ],
  ]

  return (
    <S.Modal closeIcon open={isOpen} onOpen={handleOpen} onClose={handleClose}>
      <S.Header icon="edit" content="Edit Vehicle" />
      <S.Modal.Content>
        <P>Enter in the vehicle information</P>
        <Form id="edit-vehicle-form" onSubmit={form.handleSubmit}>
          <ErrorBox errors={requestErrors} error={catchError} />
          {fieldsInGroups.map((field, i) => (
            <S.FormGroup key={`formGroup-${i}`} widths="equal">
              {field}
            </S.FormGroup>
          ))}
        </Form>
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button name="remove" type="button" onClick={handleClose} title="Cancel" />
        <Button
          color="yellow"
          form="edit-vehicle-form"
          name="checkmark"
          title="Update"
          type="submit"
          disabled={form.isSubmitting}
          loading={form.isSubmitting}
        />
      </S.Modal.Actions>
    </S.Modal>
  )
}
