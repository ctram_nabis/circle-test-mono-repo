import { isEnv } from 'utils/isHelpers'

export default {
  GRAPHQL_URL: isEnv({
    production: 'https://api.getnabis.com/graphql/admin',
    staging: 'https://api.staging.getnabis.com/graphql/admin',
    fallback: 'http://localhost:8080/graphql/admin',
    test: 'http://localhost:8081/graphql/admin',
  }),
}
