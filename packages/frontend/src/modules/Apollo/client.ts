import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { createUploadLink } from 'apollo-upload-client'
import config from 'config'
import { AUTH_TOKEN, IS_DEV } from 'modules/App'
import logger from 'modules/Logger'
import { InitialState, initialState } from './initialState'

const httpUri = config.apollo.GRAPHQL_URL

const uploadLink = createUploadLink({ uri: httpUri })

const authLink = setContext((_, { headers, ...context }) => {
  const token = localStorage.getItem(AUTH_TOKEN)

  const _headers = {
    headers: {
      ...(token && token !== 'undefined' && { AUTHORIZATION: `Bearer ${token}` }),
    },
    ...context,
  }

  return _headers
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
  logger.error('error afterware, graphQLErrors: ', graphQLErrors)

  if (networkError) {
    console.log('error afterware, networkError: ', networkError)
    console.error('NETWORK ERROR')
    return
  }
})

const link = ApolloLink.from([authLink, errorLink, uploadLink])

const cache = new InMemoryCache().restore(initialState)

export const client: ApolloClient<InitialState> = new ApolloClient({
  link,
  cache,
  connectToDevTools: IS_DEV,
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'network-only',
      errorPolicy: 'ignore',
    },
    query: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
    mutate: {
      errorPolicy: 'all',
    },
  },
})
