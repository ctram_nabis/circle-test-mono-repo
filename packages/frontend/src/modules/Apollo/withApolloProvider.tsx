import React from 'react'

import { ApolloProvider } from '@apollo/react-hooks'

import { client } from './client'

export const withApolloProvider = <T extends object>(Component: React.ComponentType<T>) => (props: T) => (
  <ApolloProvider client={client}>
    <Component {...props} />
  </ApolloProvider>
)
