import { InventoryValues } from 'generated/graphql'

export function hasNonZeroInventoryValues(inventoryValues: InventoryValues) {
  return Object.keys(inventoryValues).reduce((_isLocked, warehouseKey) => {
    const warehouseValues = inventoryValues[warehouseKey]
    return (
      _isLocked ||
      Object.keys(warehouseValues).reduce((existsNonZeroValue, inventoryValueKey) => {
        const inventoryValue = warehouseValues[inventoryValueKey]
        return existsNonZeroValue || inventoryValue !== 0
      }, false)
    )
  }, false)
}
