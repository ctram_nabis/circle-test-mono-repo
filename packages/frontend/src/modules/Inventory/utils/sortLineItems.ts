import { LineItem } from 'generated/graphql'

export function sortLineItems(lineItems: LineItem[]): LineItem[] {
  return lineItems.sort((a, b) => {
    const aId = a?.skuBatch?.id
    const bId = b?.skuBatch?.id
    return aId === bId ? (a.id > b.id ? 1 : -1) : aId && bId && aId > bId ? 1 : -1
  })
}
