import React, { useMemo, useState } from 'react'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'

import { PageHeader, S } from 'components'
import { Box } from 'components/core/src/Box'
import { Scalars } from 'generated/graphql'
import { useAdminOverrideOrganization } from 'modules/Inventory/provider/useAdminOverrideOrganization'
import { withAdminOverrideOrganizationProvider } from 'modules/Inventory/provider/withAdminOverrideOrganizationProvider'
import { AllProductsTab } from 'modules/Sku/containers/AllProductsTab'
import { AllSkuBatchesTab } from 'modules/SkuBatch/containers'
import { useMe } from 'modules/User'
import { AllBatchesTab } from '../../Batch/containers/AllBatchesTab'

export interface InventoryUrlParamsType {
  organizationId?: string
  search?: string
}

export interface FeedbackType {
  content: string
  error: boolean
  header: string
  success: boolean
}

const Wrapper = styled(Box)`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  width: 100%;
  margin-bottom: 12px;
`
const AppContainer = styled(Box)`
  display: flex;
  height: 100%;
  justify-content: center;
  padding: 15px 10px;
  width: 100%;
  overflow: auto;
`

function InventoryPageRoot() {
  const { user, isAdmin } = useMe()
  const currentParams = useParams()

  const { adminOverrideOrganizationId, setAdminOverrideOrganizationId } = useAdminOverrideOrganization()

  const [showFeedback, setShowFeedback] = useState<boolean>(false)
  const [feedback, setFeedback] = useState<FeedbackType | null>(null)
  const [, setUrlParams] = useState<InventoryUrlParamsType>(currentParams)

  useMemo(() => {
    setUrlParams(currentParams)
  }, [currentParams, setUrlParams])

  const onFeedback = (data: FeedbackType) => {
    setShowFeedback(true)
    setFeedback(data)
  }

  const { content, error, header, success } = feedback || {}

  const adminOrganizationOptions = user.organizations.map((org) => ({
    key: org.id,
    text: `${org.name}${org.doingBusinessAs ? `, dba ${org.doingBusinessAs}` : ''}`,
    value: org.id,
  }))

  const getTextForAdminOrganizationDropdown = () => {
    if (isAdmin && adminOverrideOrganizationId === 'All Organizations') return 'All Organizations'

    const selectedOption = adminOrganizationOptions.find((option) => option.key === adminOverrideOrganizationId)
    if (!selectedOption) {
      return 'Error'
    } else {
      return selectedOption.text
    }
  }
  const adminOrganizationIdValue =
    adminOverrideOrganizationId === 'All Organizations' ? undefined : adminOverrideOrganizationId

  return (
    <AppContainer overflow="auto">
      <Wrapper>
        <S.Container fluid>
          <S.Grid columns={1} padded>
            <S.GridRow>
              <S.GridColumn>
                <Box display="flex" alignItems="baseline">
                  <PageHeader mr={20}>Inventory</PageHeader>
                  {isAdmin && (
                    <S.Dropdown
                      search
                      options={[
                        { key: '0', text: 'All Organizations', value: 'All Organizations' },
                        ...adminOrganizationOptions,
                      ]}
                      value={adminOverrideOrganizationId}
                      text={getTextForAdminOrganizationDropdown()}
                      onChange={(_e, { value }) => {
                        setAdminOverrideOrganizationId(value as Scalars['ID'])
                      }}
                    />
                  )}
                  {showFeedback && (
                    <Box margin="5px" data-test="inventory-page-feedback">
                      <S.Message
                        success={success}
                        error={error}
                        header={header}
                        content={content}
                        onDismiss={() => setShowFeedback(false)}
                      />
                    </Box>
                  )}
                </Box>
                <S.Tab
                  panes={[
                    {
                      menuItem: 'SKUs',
                      render: () => (
                        <AllSkuBatchesTab
                          onFeedback={onFeedback}
                          adminOverrideOrganizationId={adminOrganizationIdValue}
                        />
                      ),
                    },
                    {
                      menuItem: 'Batches',
                      render: () => (
                        <AllBatchesTab onFeedback={onFeedback} adminOverrideOrganizationId={adminOrganizationIdValue} />
                      ),
                    },
                    {
                      menuItem: 'Products',
                      render: () => (
                        <AllProductsTab
                          onFeedback={onFeedback}
                          adminOverrideOrganizationId={adminOrganizationIdValue}
                        />
                      ),
                    },
                  ]}
                  className="inv-tabs"
                />
              </S.GridColumn>
            </S.GridRow>
          </S.Grid>
        </S.Container>
      </Wrapper>
    </AppContainer>
  )
}

export let InventoryPageRootWithOverrideWrapper = withAdminOverrideOrganizationProvider(InventoryPageRoot)
