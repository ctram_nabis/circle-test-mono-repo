import { shallow } from 'enzyme'
import { AddBatchModalButton, EditBatchModalButton } from 'modules/Batch'
import { AddSkuBatchModalButton } from 'modules/SkuBatch/components/AddSkuBatchModal'
import React from 'react'
import { InventoryPageRoot } from './InventoryPage'

describe('InventoryPage container', () => {
  describe('AddBatchModalButton', () => {
    it('should render without crashing', () => {
      shallow(<AddBatchModalButton />)
    })

    /**
     * TODO: simulate form input change, corresponding state change
     */

    /**
     * TODO: simulate button click handler, make sure jest mock fn is called
     */
  })
  describe('EditBatchModalButton', () => {
    it('should render without crashing', () => {
      shallow(<EditBatchModalButton />)
    })

    /**
     * TODO: simulate form input change, corresponding state change
     */

    /**
     * TODO: simulate button click handler, make sure jest mock fn is called
     */
  })

  describe('AddSkuBatchModalButton', () => {
    it('should render without crashing', () => {
      shallow(<AddSkuBatchModalButton />)
    })

    /**
     * TODO: simulate form input change, corresponding state change
     */

    /**
     * TODO: simulate button click handler, make sure jest mock fn is called
     */
  })

  describe('InventoryRoot', () => {
    it('should render without crashing', () => {
      shallow(<InventoryPageRoot />)
    })
  })
})
