import { Box, S } from 'components'
import { InventoryItem, Scalars, SkuBatch } from 'generated/graphql'
import React from 'react'

interface Props {
  inventoryItemId?: Scalars['ID']
  inventoryItems: InventoryItem[]
  onChange: (inventoryItemId: Scalars['ID']) => void
  skuBatch: SkuBatch
}

export const InventoryItemDropdown = ({ inventoryItemId, inventoryItems, onChange, skuBatch }: Props) => {
  const { sku } = skuBatch || {}
  const { code: skuBatchCode, name: skuBatchName } = sku

  const getInventoryItemOptions = (_inventoryItems: InventoryItem[] = []) =>
    _inventoryItems
      .map((_inventoryItem) => ({
        key: _inventoryItem.id,
        text: `${_inventoryItem.warehouse?.site?.name ?? 'error'}: ${_inventoryItem.counted}`,
        value: _inventoryItem.id,
      }))
      .sort((a, b) => (a.text > b.text ? 1 : -1))

  return (
    <S.Segment>
      {/* SELECT FROM EXISTING INVENTORY OPTIONS */}

      {inventoryItems && inventoryItems.length > 0 ? (
        <S.Dropdown
          autoComplete="inventoryItemId"
          name="inventoryItemId"
          onChange={(_e: React.SyntheticEvent, d: S.DropdownProps) => onChange(d.value as string)}
          options={getInventoryItemOptions(inventoryItems)}
          placeholder="Select an existing inventory item..."
          required
          selection
          type="text"
          value={inventoryItemId}
        />
      ) : (
        <Box>
          <S.Header size="small">
            <S.Icon name="bell" />
            No Inventory Items for {skuBatchName} ({skuBatchCode || '-----'})
          </S.Header>
        </Box>
      )}
    </S.Segment>
  )
}
