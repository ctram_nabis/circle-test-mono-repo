import { Button } from 'components'
import { InventoryItem, SkuBatch, useCreateInventoryItemMutation } from 'generated/graphql'
import idx from 'idx'
import { logger } from 'modules/Logger'
import { getWarehouseKeyFromWarehouseId, getWarehouseOptions } from 'modules/Warehouse'
import React from 'react'
import { unwrap } from 'utils'

interface Props {
  inventoryItems: InventoryItem[]
  skuBatch?: SkuBatch | null
}

export const AddInventoryItemButton = (props: Props) => {
  const { inventoryItems, skuBatch } = props
  const [createInventoryItemMutation, { loading }] = useCreateInventoryItemMutation({
    refetchQueries: ['InventorySkuBatchesTab', 'GetInventoryItems'],
  })
  const handleAddInventoryItem = async (warehouseIds: string[]) => {
    const organizationId = skuBatch?.sku?.organization?.id
    const skuBatchId = skuBatch?.id
    try {
      if (!organizationId) {
        throw new Error(`skuBatch.sku.organization.id is missing from payload`)
      }
      if (!skuBatchId) {
        throw new Error(`skuBatch.id is missing from payload`)
      }
      await Promise.all(
        warehouseIds.map((warehouseId) =>
          createInventoryItemMutation({
            variables: {
              organizationId,
              skuBatchId,
              warehouseId: btoa(`Warehouse:${warehouseId}`),
            },
          }),
        ),
      )
    } catch (error) {
      logger.error(error)
    }
  }
  const decodedWarehouseIdsWithExistingInventory = inventoryItems.map((_inventoryItem: Partial<InventoryItem>) =>
    idx(_inventoryItem, (_) => unwrap(_.warehouse.id)),
  )
  const warehouseIdsWithoutInventoryItems = getWarehouseOptions()
    .filter(({ warehouseId }) => !decodedWarehouseIdsWithExistingInventory.includes(warehouseId))
    .map((warehouse) => warehouse.warehouseId)
  const warehouseNamesWithoutInventoryItems: string = warehouseIdsWithoutInventoryItems
    .map((warehouseId) => getWarehouseKeyFromWarehouseId(warehouseId, false))
    .join(', ')

  return warehouseNamesWithoutInventoryItems ? (
    <Button
      color="green"
      disabled={loading}
      loading={loading}
      onClick={() => handleAddInventoryItem(warehouseIdsWithoutInventoryItems)}
      icon="plus"
      title={`Initialize inventory for ${warehouseNamesWithoutInventoryItems}`}
    />
  ) : null
}
