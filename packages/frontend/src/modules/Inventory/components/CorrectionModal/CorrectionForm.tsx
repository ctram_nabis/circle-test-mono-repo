import { Box, ErrorBox, FormButton, InputProps, S } from 'components'
import { CorrectInventoryItemInput, InventoryItem, useCorrectInventoryItemMutation } from 'generated/graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import { logger } from 'modules/Logger'
import React, { useState } from 'react'
import { ErrorType } from 'types'
import { CorrectionTooltip } from './CorrectionTooltip'

const INLINE_REQUIRED_MSG = ' * Required'

const validationSchema = createValidationSchema<Omit<CorrectInventoryItemInput, 'transactionType' | 'id'>>({
  endCounted: validators.number.required(INLINE_REQUIRED_MSG),
  notes: validators.string.required(INLINE_REQUIRED_MSG),
})
interface Props {
  closeModal?: () => void
  inventoryItem: InventoryItem
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: (inventoryItemChanged: string) => void
}

export const CorrectionForm = ({ closeModal, inventoryItem, onSuccess, onFailure }: Props) => {
  const [correctInventoryItemMutation] = useCorrectInventoryItemMutation()
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()

  const { errors, handleBlur, setFieldValue, handleSubmit, isSubmitting, values } = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      endCounted: inventoryItem.counted,
      notes: '',
    },
    onSubmit: async (inputs) => {
      try {
        const response = await correctInventoryItemMutation({
          variables: {
            input: {
              ...inputs,
              id: inventoryItem.id,
              transactionType: 'CORRECTION',
            },
          },
          refetchQueries: ['InventorySkuBatchesTab', 'GetSkuBatch', 'GetInventoryItems'],
        })
        if (response.errors) {
          setRequestErrors(response.errors)
          if (onFailure) onFailure(response.errors)
          return
        } else {
          const _inventoryItemChanged = response?.data?.correctInventoryItem?.changedInventoryItem?.id || ''
          if (onSuccess) {
            onSuccess(_inventoryItemChanged)
          }
          if (closeModal) {
            closeModal()
          }
        }
      } catch (error) {
        logger.error(error)
        setCatchError(error.message)
        if (onFailure) {
          onFailure(error.message)
        }
      }
    },
  })

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        required: true,
        label: 'Count*',
        name: 'endCounted',
        onChange: (_e: React.SyntheticEvent, d: InputProps) => setFieldValue('endCounted', Number(d.value)),
        onBlur: handleBlur,
        placeholder: 'e.g. 0',
        type: 'number',
        values,
        errors,
        disabled: isSubmitting,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'Notes*',
        name: 'notes',
        onChange: (_e: React.SyntheticEvent, d: InputProps) => setFieldValue('notes', d.value),
        onBlur: handleBlur,
        placeholder: 'Additional notes and reason for correction...',
        type: 'text',
        values,
        errors,
        textarea: true,
        disabled: isSubmitting,
      }),
    ],
  ]

  return (
    <S.Form id="intake-outtake-form" onSubmit={handleSubmit}>
      <ErrorBox errors={requestErrors} error={catchError} />
      <Box>{fieldsInGroups}</Box>
      <CorrectionTooltip inventoryItem={inventoryItem} ledgerInventoryItemValues={values} />
      <S.Modal.Actions>
        <FormButton onClick={closeModal} icon="x" title="Cancel" />
        <FormButton color="green" disabled={isSubmitting} loading={isSubmitting} icon="check" title="Submit" />
      </S.Modal.Actions>
    </S.Form>
  )
}
