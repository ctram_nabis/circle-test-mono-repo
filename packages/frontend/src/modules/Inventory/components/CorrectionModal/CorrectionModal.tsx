import { ErrorBox, Loading, P } from 'components'
import { Box } from 'components/core'
import { InventoryItem, Scalars, SkuBatch, useGetInventoryItemsQuery } from 'generated/graphql'
import React, { useState } from 'react'
import { Header, Label, Modal, Segment } from 'semantic-ui-react'
import { ErrorType } from 'types'
import { AddInventoryItemButton } from './AddInventoryItemButton'
import { CorrectionForm } from './CorrectionForm'
import { InventoryItemDropdown } from './InventoryItemDropdown'

interface Props {
  skuBatch: SkuBatch
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: () => void
  open?: boolean
  trigger?: JSX.Element
}

export const CorrectionModal = (props: Props) => {
  const {
    skuBatch,
    trigger,
    open = true,
    onClose = () => null,
    onFailure = () => null,
    onOpen = () => null,
    onSuccess = () => null,
  } = props

  const { data, loading, error } = useGetInventoryItemsQuery({
    variables: {
      skuBatchId: (skuBatch && skuBatch.id) || '',
    },
  })

  const inventoryItems = (data && data.getInventoryItems) || []
  const [isOpen, setIsOpen] = useState(open)
  const [inventoryItemId, setInventoryItemId] = useState<Scalars['ID'] | undefined>(undefined)
  const [showForm, setShowForm] = useState<boolean>(false)

  const openModal = () => {
    if (onOpen) {
      onOpen()
    } else {
      setIsOpen(true)
    }
  }

  const closeModal = () => {
    if (onClose) {
      onClose()
    } else {
      setIsOpen(false)
    }
  }

  const handleInventoryItemDropdownChange = (selectedInventoryItemId: Scalars['ID']) => {
    setInventoryItemId(selectedInventoryItemId)
    setShowForm(true)
  }

  const inventoryItem = inventoryItems.find((item) => item.id === inventoryItemId)

  // TODO: should inventoryItems be casted as InventoryItem[]
  return (
    <Modal closeIcon open={isOpen} onOpen={openModal} onClose={closeModal} trigger={trigger} size="large">
      <Header icon="edit" content="Correct Item (Quantity Only)" />
      <Modal.Content scrollable>
        <Loading loading={loading} />
        <ErrorBox error={error} />
        {!loading && !error && (
          <Box>
            <P>Enter in the inventory item details.</P>
            <Segment tertiary bg="shade1">
              <Segment widths="equal">
                <Label htmlFor="inventoryItemId">Existing Inventory Item</Label>
                <InventoryItemDropdown
                  inventoryItemId={inventoryItemId}
                  inventoryItems={inventoryItems as InventoryItem[]}
                  onChange={handleInventoryItemDropdownChange}
                  skuBatch={skuBatch}
                />
                <AddInventoryItemButton inventoryItems={inventoryItems as InventoryItem[]} skuBatch={skuBatch} />
              </Segment>
              {showForm && inventoryItem && (
                <CorrectionForm
                  closeModal={closeModal}
                  inventoryItem={inventoryItem as InventoryItem}
                  onFailure={onFailure}
                  onSuccess={onSuccess}
                />
              )}
            </Segment>
          </Box>
        )}
      </Modal.Content>
    </Modal>
  )
}
