import * as Yup from 'yup'

export const INLINE_REQUIRED_MSG = ' * Required'

export const correctionValidationSchema = Yup.object().shape({
  sku: Yup.string()
    .trim()
    .required(INLINE_REQUIRED_MSG),
  batch: Yup.string()
    .trim()
    .required(INLINE_REQUIRED_MSG),
  organization: Yup.string()
    .trim()
    .required(INLINE_REQUIRED_MSG),
  endCounted: Yup.number()
    .integer()
    .positive()
    .required('Please enter a count for this correction'),
  notes: Yup.string().required('Please enter notes for this correction'),
})

export type CorrectionFormInputValues = Yup.InferType<typeof correctionValidationSchema>
