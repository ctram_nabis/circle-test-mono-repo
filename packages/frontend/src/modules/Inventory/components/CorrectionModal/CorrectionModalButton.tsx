import { S } from 'components/SemanticComponents'
import { SkuBatch } from 'generated/graphql'
import React, { Fragment, useState } from 'react'
import { CorrectionModal } from './CorrectionModal'

interface Props {
  skuBatch: SkuBatch
}

// TODO: replace this with generic button
export function CorrectionModalButton({ skuBatch }: Props) {
  const [showModal, setShowModal] = useState<boolean>(false)

  return (
    <Fragment>
      {showModal && (
        <CorrectionModal skuBatch={skuBatch} onClose={() => setShowModal(false)} onOpen={() => setShowModal(false)} />
      )}
      <S.Button onClick={() => setShowModal(true)} basic icon="wrench" title="Correct Count" color="orange" />
    </Fragment>
  )
}
