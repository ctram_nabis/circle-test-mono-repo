import { ErrorBox, P } from 'components'
import { Box } from 'components/core'
import { InventoryItem } from 'generated/graphql'
import React from 'react'
import { Icon, Popup } from 'semantic-ui-react'

interface FormValues {
  endCounted?: number
  notes?: string
}
interface Props {
  inventoryItem: InventoryItem
  ledgerInventoryItemValues: FormValues
}

export const CorrectionTooltip = (props: Props) => {
  const { inventoryItem, ledgerInventoryItemValues } = props
  // If initial warehouse dropdown is not selected, don't display the tooltip at all
  if (!inventoryItem.warehouse) {
    return null
  }

  const errors: string[] = []

  if (!ledgerInventoryItemValues.endCounted) {
    errors.push('Missing count')
  }

  if (!ledgerInventoryItemValues.notes) {
    errors.push('Missing notes')
  }

  const color = errors.length > 0 ? 'red' : 'green'
  const icon = errors.length > 0 ? 'warning sign' : 'check circle'
  const text = errors.length > 0 ? 'Incomplete' : 'Ready to submit'
  const fontSize = '0.85em'

  return (
    <Popup
      flowing
      trigger={
        <Box center>
          <Icon color={color} name={icon} />
          <em
            style={{
              color,
              fontSize,
              display: 'inlineBlock',
              verticalAlign: 'middle',
            }}
          >
            {text}
          </em>
        </Box>
      }
    >
      {errors.length > 0 ? (
        <Popup.Content>
          <strong>Correction has the following errors:</strong>
          <ErrorBox errors={errors} />
        </Popup.Content>
      ) : (
        <Popup.Content>
          <P>
            Click <u>Submit</u> to correct item
          </P>
        </Popup.Content>
      )}
    </Popup>
  )
}
