import { MockedProvider } from '@apollo/react-testing'
import { mount, shallow } from 'enzyme'
import React from 'react'
import { Box } from 'theme-ui'
import { AddInventoryItemButton } from './AddInventoryItemButton'
import { CorrectionForm } from './CorrectionForm'
import { CorrectionModal } from './CorrectionModal'
import { CorrectionTooltip } from './CorrectionTooltip'

describe('CorrectionModal container', () => {
  describe('AddInventoryItemButton', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <AddInventoryItemButton inventoryItems={[{ id: '1' }]} skuBatch={{ id: '1' }} />
        </MockedProvider>,
      )
    })

    /**
     * TODO: simulate button click handler, make sure jest mock fn is called
     */
  })

  describe('CorrectionForm', () => {
    it('should render without crashing', () => {
      const closeModal = jest.fn()
      mount(
        <MockedProvider>
          <CorrectionForm inventoryItem={{ id: '' }} closeModal={closeModal} />
        </MockedProvider>,
      )
    })
  })

  describe('CorrectionModal', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <CorrectionModal inventoryRow={{}} trigger={<Box />} />
        </MockedProvider>,
      )
    })

    /**
     * TODO: simulate form input change, corresponding state change
     */

    /**
     * TODO: simulate button click handler, make sure jest mock fn is called
     */
  })

  describe('CorrectionTooltip', () => {
    it('should render without crashing', () => {
      shallow(<CorrectionTooltip inventoryItem={{ id: '' }} ledgerInventoryItemValues={{ id: '2' }} />)
    })
  })
})
