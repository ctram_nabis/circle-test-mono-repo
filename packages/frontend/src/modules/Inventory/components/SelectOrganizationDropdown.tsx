import { ErrorBox } from 'components'
import { Organization, useAllAdminOrganizationsQuery } from 'generated/graphql'
import React from 'react'
import { Dropdown, Table } from 'semantic-ui-react'
import { oc } from 'ts-optchain'

interface Props {
  organization?: Organization
  onOrganizationChange: (o: Organization) => void
}

// This component is for nabis admins, so we will be setting the organization in the parent
export function SelectOrganizationDropdown({ organization, onOrganizationChange }: Props) {
  const { data, error } = useAllAdminOrganizationsQuery()
  // const organizations = idx(data, (_) => _.viewer.allAdminOrganizations) as Organization[]
  const organizations = oc(data).viewer.allAdminOrganizations([]) as Organization[]

  return (
    <Table.HeaderCell>
      <ErrorBox error={error} />
      {!error && (
        <Dropdown
          search
          name="activeOrg"
          className="icon"
          icon="dropdown"
          loading={!organizations}
          noneOption={[true, false, {}]}
          options={organizations.map((org) => ({ value: org.id, text: org.name }))}
          defaultValue={organization && organization.id}
          placeholder="Select an organization"
          onChange={(_e, _data) => {
            const newOrg = organizations.find(({ id }) => id === _data.value)
            if (newOrg) {
              onOrganizationChange(newOrg)
            }
          }}
          data-test="updateorg"
        />
      )}
    </Table.HeaderCell>
  )
}
