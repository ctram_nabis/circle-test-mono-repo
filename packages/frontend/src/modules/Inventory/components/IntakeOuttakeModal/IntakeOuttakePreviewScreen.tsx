import React, { useEffect, useReducer } from 'react'

import { Grid, Table } from 'semantic-ui-react'
import styled from 'styled-components'

import { Order } from 'generated/graphql'
import { getInventoryValueRulesForOrderActionAndIsIntake, IntakeOrOuttakeType, sortLineItems } from 'modules/Inventory'
import { getColor, lightColor } from 'system'
import is from 'typescript-styled-is'
import { titleize } from 'utils'
import { IntakeOuttakeLineItemRow } from './IntakeOuttakeLineItemRow'
import { OrderSummary } from './OrderSummary'

interface Props {
  order: Order
  intakeOrOuttake: IntakeOrOuttakeType
  onNegativeCount?: () => void
  onNoNegativeCounts?: () => void
}

const IntakeOrOuttakeHeader = styled(Table.HeaderCell)`
  color: ${(props) => getColor('accent2', props)};
  background-color: ${(props) => lightColor('accent2')(props)};
  ${is('intake')`
    color: ${(props) => getColor('accent1', props)};
    background-color: ${(props) => lightColor('accent1')(props)};
  `}
`

export function IntakeOuttakePreviewScreen({ order, intakeOrOuttake, onNegativeCount, onNoNegativeCounts }: Props) {
  const { action, lineItems = [] } = order
  const isIntake = intakeOrOuttake === 'INTAKE'
  const sortedLineItems = sortLineItems(lineItems)

  // track possible negative counts
  const [countWillBeNegativeMap, dispatch] = useReducer(
    // dispatch will update map at id
    (state, _action) => {
      state[_action.id] = _action.value
      return state
    },
    // initialize a map for each lineItemId with value false
    lineItems.reduce((acc, { id }) => {
      acc[id] = false
      return acc
    }, {}),
  )

  // if a lineItem will result in a negative count, disable the parent formButton
  useEffect(() => {
    const willBeANegative = Object.keys(countWillBeNegativeMap).reduce((accum, key) => {
      return accum || countWillBeNegativeMap[key]
    }, false)

    if (willBeANegative) {
      if (onNegativeCount) {
        onNegativeCount()
      }
    }
    if (!willBeANegative) {
      if (onNoNegativeCounts) {
        onNoNegativeCounts()
      }
    }
  }, [countWillBeNegativeMap, onNegativeCount, onNoNegativeCounts])

  const inventoryKeysAndMultipliers = getInventoryValueRulesForOrderActionAndIsIntake({ action, intakeOrOuttake })
  const inventoryValueHeaders = inventoryKeysAndMultipliers.map(([inventoryKey]) => {
    return (
      <IntakeOrOuttakeHeader key={`header-${inventoryKey}`} intake={isIntake}>{`\u0394 ${titleize(
        inventoryKey,
      )}`}</IntakeOrOuttakeHeader>
    )
  })

  return (
    <Grid columns="equal" data-test="intake-outtake-preview-screen">
      <OrderSummary order={order} intakeOrOuttake={intakeOrOuttake} />
      <Grid.Row>
        <Grid.Column>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell colSpan="5">Line Items</Table.HeaderCell>
              </Table.Row>
              <Table.Row>
                <Table.HeaderCell>Name</Table.HeaderCell>
                <Table.HeaderCell>Order Qty</Table.HeaderCell>
                {inventoryValueHeaders}
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {sortedLineItems &&
                sortedLineItems.map((lineItem, i) => {
                  return (
                    <IntakeOuttakeLineItemRow
                      key={`intake-outtake-lineitem-row-${lineItem.id}`}
                      lineItem={lineItem}
                      sortedLineItems={sortedLineItems}
                      index={i}
                      intakeOrOuttake={intakeOrOuttake}
                      order={order}
                      onNegativeCount={() => dispatch({ id: lineItem.id, value: true })}
                      onNoNegativeCount={() => dispatch({ id: lineItem.id, value: false })}
                    />
                  )
                })}
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}
