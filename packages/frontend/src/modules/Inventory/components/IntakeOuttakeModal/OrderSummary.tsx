import { Box, S } from 'components'
import { Order } from 'generated/graphql'
import { IntakeOrOuttakeType, IntakeOuttakeDestinationMap } from 'modules/Inventory'
import { getOrderActionColor, ORDER_ACTION_DESC, ORDER_STATUS_DESC } from 'modules/Order'
import React, { Fragment } from 'react'

interface Props {
  order: Order
  intakeOrOuttake: IntakeOrOuttakeType
}

export function OrderSummary({ order, intakeOrOuttake }: Props) {
  const { action, site, status } = order
  const isTransferIntake = order?.action === 'INTERNAL_TRANSFER' && intakeOrOuttake === 'INTAKE'

  const licensedLocation = site?.licensedLocation
  const warehouseName = order?.warehouse?.site?.name ?? 'ERROR'

  const [name, city, zip] = [licensedLocation?.name, licensedLocation?.city, licensedLocation?.zip]

  const licensedLocationName = `${name}, ${city} ${zip}`

  const fromName = isTransferIntake
    ? warehouseName
    : IntakeOuttakeDestinationMap[intakeOrOuttake].from === 'licensedLocation'
    ? licensedLocationName
    : warehouseName

  const toName = isTransferIntake
    ? licensedLocationName
    : IntakeOuttakeDestinationMap[intakeOrOuttake].to === 'licensedLocation'
    ? licensedLocationName
    : warehouseName

  const actionDesc = action && ORDER_ACTION_DESC[action]
  const statusDesc = status && ORDER_STATUS_DESC[status]

  return (
    <Fragment>
      <S.Grid.Row>
        <S.Grid.Column>
          <S.Header>
            {`${order?.organization?.name ?? 'Error'} dba ${order?.organization?.doingBusinessAs ?? 'Error'}`}
          </S.Header>
        </S.Grid.Column>
      </S.Grid.Row>
      <S.Grid.Row>
        <S.Grid.Column>
          <S.Header>Action: {actionDesc}</S.Header>
        </S.Grid.Column>
        <S.Grid.Column>
          <S.Header>Status: {statusDesc}</S.Header>
        </S.Grid.Column>
      </S.Grid.Row>
      <S.Grid.Row>
        <S.Grid.Column>
          <Box color={getOrderActionColor(action)}>
            <S.Header>
              {fromName} <S.Icon name="arrow right" size="tiny" />
              {toName}
            </S.Header>
          </Box>
        </S.Grid.Column>
      </S.Grid.Row>
      <S.Grid.Row>
        <S.Grid.Column>Admin Notes: {order.adminNotes || '---'}</S.Grid.Column>
        <S.Grid.Column>Internal Notes: {order.internalNotes || '---'}</S.Grid.Column>
        <S.Grid.Column>Delivery Notes: {order.notes || '---'}</S.Grid.Column>
      </S.Grid.Row>
    </Fragment>
  )
}
