import { H2, Loading, S } from 'components'
import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { InventoryItem, Order, SkuBatch, useGetOrderQuery, useLineItemIntakeOuttakeMutation } from 'generated/graphql'
import { IntakeOrOuttakeType } from 'modules/Inventory'
import logger from 'modules/Logger'
import { getEncodedWarehouseIdFromKey, getWarehouseKeyFromLicensedLocationId, WarehouseKey } from 'modules/Warehouse'
import React, { Fragment, useState } from 'react'
import { titleize } from 'utils'
import { IntakeOuttakePreviewScreen, IntakeOuttakeSummaryScreen } from '.'

interface Props {
  orderId: string
  isOpen: boolean
  onOpen: () => void
  onClose: () => void
  onSuccess?: () => void
  transactionType: IntakeOrOuttakeType
}

export function IntakeOuttakeModal({ orderId, isOpen, onOpen, onClose, transactionType: intakeOrOuttake }: Props) {
  const [stage, setStage] = useState<'preview' | 'success'>('preview')
  const [disableFormButton, setDisableFormButton] = useState<boolean>(false)
  const [submissionError, setSubmissionError] = useState<string | undefined>(undefined)

  // fetch order (and warehouses)
  const { data, loading, error, ...getOrder } = useGetOrderQuery({ variables: { id: orderId } })
  const order = data?.getOrder

  const alreadyProcessed =
    (order?.intaken && intakeOrOuttake === 'INTAKE') || (order?.outtaken && intakeOrOuttake === 'OUTTAKE')

  const isTransferIntake = order?.action === 'INTERNAL_TRANSFER' && intakeOrOuttake === 'INTAKE'

  const warehouseId = isTransferIntake
    ? getEncodedWarehouseIdFromKey(
        getWarehouseKeyFromLicensedLocationId(order?.site?.licensedLocation?.id as string) as WarehouseKey,
      )
    : order?.warehouse?.id

  const [intakeOuttake, { data: mutationData, loading: mutationLoading }] = useLineItemIntakeOuttakeMutation()
  const processedItems = mutationData && mutationData.lineItemIntakeOuttake

  const handleSubmit = async () => {
    if (!warehouseId) {
      setSubmissionError(
        `Invalid warehouse ID${
          isTransferIntake ? '. Reselect destination warehouse, otherwise contact #software-support' : ''
        }`,
      )
      return
    }

    if (alreadyProcessed) {
      setSubmissionError('Order already processed')
      return
    }

    const _lineItems = order ? order.lineItems : []
    const lineItems = _lineItems.map((lineItem) => {
      const { id: lineItemId, skuBatch, quantity } = lineItem
      const { sku } = skuBatch as SkuBatch

      const [{ id: skuBatchId }] = [skuBatch as SkuBatch]
      const organizationId = sku.organization.id

      return { lineItemId, quantity, organizationId, skuBatchId }
    })
    try {
      const { errors: mutationError } = await intakeOuttake({
        variables: { input: { lineItems, transactionType: intakeOrOuttake, warehouseId, orderId } },
        refetchQueries: ['AllAdminOrdersPage', 'AllAdminOrders'],
      })

      if (!mutationError) {
        setStage('success')
      }
    } catch (er) {
      logger.error(er)
      setSubmissionError(er.message)
    }
  }

  const destinationName = isTransferIntake ? order?.site?.licensedLocation?.name : order?.warehouse?.site?.name

  // 1. load order and display lineItems, show proposed inventory changes
  // 2. show loading spinner
  // 3. show confirmation screen
  return (
    <S.Modal closeIcon open={isOpen} onClose={onClose} onOpen={onOpen} data-test="intake-outtake-modal">
      {!order || loading ? (
        <S.Segment>
          <Loading />
        </S.Segment>
      ) : (
        <Fragment>
          <S.Modal.Header>
            <H2>
              {`${titleize(intakeOrOuttake)} ${intakeOrOuttake === 'OUTTAKE' ? 'from' : 'to'} ${destinationName},`}
            </H2>
            <Text display="inline-block" fontWeight="normal">
              {`${order?.name} #${order?.number}`}
            </Text>
          </S.Modal.Header>
          <S.Modal.Content>
            {error ? (
              // <ErrorBox error={error} />
              <Box color="red">Oops... failed to fetch order</Box>
            ) : (
              <S.Grid>
                <S.Grid.Row className="order-container">
                  <S.Grid.Column>
                    {mutationLoading ? (
                      <Loading />
                    ) : stage === 'preview' ? (
                      <IntakeOuttakePreviewScreen
                        order={order as Order}
                        intakeOrOuttake={intakeOrOuttake}
                        onNegativeCount={() => setDisableFormButton(true)}
                        onNoNegativeCounts={() => setDisableFormButton(false)}
                      />
                    ) : (
                      stage === 'success' &&
                      processedItems && (
                        <IntakeOuttakeSummaryScreen
                          order={order as Order}
                          intakeOrOuttake={intakeOrOuttake}
                          updatedInventoryItems={processedItems as InventoryItem[]}
                        />
                      )
                    )}
                  </S.Grid.Column>
                </S.Grid.Row>
                {submissionError && (
                  <S.Grid.Row>
                    <S.Grid.Column>
                      <S.Message
                        negative
                        header="Oops, something happened..."
                        content={submissionError}
                        onDismiss={() => setSubmissionError(undefined)}
                      />
                    </S.Grid.Column>
                  </S.Grid.Row>
                )}
                <S.Divider />
                {stage === 'preview' && (
                  <Fragment>
                    <S.Button icon="refresh" onClick={() => getOrder.refetch()} loading={loading} disabled={loading} />
                    <S.Button
                      content={`${titleize(String(intakeOrOuttake))} Order`}
                      color="green"
                      onClick={handleSubmit}
                      disabled={disableFormButton || mutationLoading || alreadyProcessed}
                      loading={mutationLoading || (!order && !error)}
                    />
                  </Fragment>
                )}
                <S.Button content={stage === 'preview' ? 'Cancel' : 'Close'} color="grey" onClick={onClose} />
              </S.Grid>
            )}
          </S.Modal.Content>
        </Fragment>
      )}
    </S.Modal>
  )
}
