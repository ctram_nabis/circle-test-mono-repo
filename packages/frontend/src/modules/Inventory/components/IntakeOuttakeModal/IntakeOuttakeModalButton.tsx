import { S } from 'components'
import { Order, Scalars } from 'generated/graphql'
import { ORDER_ACTION_TO_INTAKE_OUTTAKE } from 'modules/Inventory/constants'
import React, { Fragment, useState } from 'react'
import { IntakeOuttakeModal } from './IntakeOuttakeModal'

interface ButtonProps {
  orderId: Scalars['ID']
  disabled?: boolean
}

export function IntakeModalButton({ orderId, disabled }: ButtonProps) {
  const [showModal, setShowModal] = useState<boolean>(false)
  const onOpen = () => setShowModal(true)
  const onClose = () => setShowModal(false)
  return (
    <Fragment>
      {showModal && (
        <IntakeOuttakeModal
          isOpen={showModal}
          transactionType="INTAKE"
          orderId={orderId}
          onClose={onClose}
          onOpen={onOpen}
          onSuccess={onClose}
        />
      )}
      <S.Button disabled={disabled} icon="sign in" label="Intake" color="teal" onClick={onOpen} />
    </Fragment>
  )
}

export function OuttakeModalButton({ orderId, disabled }: ButtonProps) {
  const [showModal, setShowModal] = useState<boolean>(false)
  const onOpen = () => setShowModal(true)
  const onClose = () => setShowModal(false)
  return (
    <Fragment>
      {showModal && (
        <IntakeOuttakeModal
          isOpen={showModal}
          transactionType="OUTTAKE"
          orderId={orderId}
          onClose={onClose}
          onOpen={onOpen}
          onSuccess={onClose}
        />
      )}
      <S.Button
        data-test="intake-outtake-modal-button"
        disabled={disabled}
        icon="sign out"
        label="Outtake"
        color="violet"
        onClick={onOpen}
      />
    </Fragment>
  )
}

interface Props {
  order: Order
}

export function IntakeOuttakeModalButtons({ order }: Props) {
  const { action, intaken, outtaken } = order

  const availableActions = ORDER_ACTION_TO_INTAKE_OUTTAKE[action]

  return (
    <Fragment>
      {availableActions.length > 0 ? (
        <Fragment>
          {availableActions.map((transactionType) => {
            if (transactionType === 'INTAKE') {
              return <IntakeModalButton orderId={order.id} key={`${order.id}-intake`} disabled={intaken} />
            } else if (transactionType === 'OUTTAKE') {
              return <OuttakeModalButton orderId={order.id} key={`${order.id}-outtake`} disabled={outtaken} />
            } else {
              return null
            }
          })}
        </Fragment>
      ) : null}
    </Fragment>
  )
}
