import { S } from 'components'
import { InventoryItem, Order } from 'generated/graphql'
import { IntakeOrOuttakeType, sortLineItems } from 'modules/Inventory'
import React from 'react'
import { Grid, Table } from 'semantic-ui-react'
import { IntakeOuttakeInventoryItemRow } from './IntakeOuttakeInventoryItemRow'
import { OrderSummary } from './OrderSummary'

interface Props {
  order: Order
  intakeOrOuttake: IntakeOrOuttakeType
  updatedInventoryItems: InventoryItem[]
}

export function IntakeOuttakeSummaryScreen({ intakeOrOuttake, order, updatedInventoryItems }: Props) {
  const { lineItems = [] } = order
  const sortedLineItems = sortLineItems(lineItems)

  return (
    <Grid columns="equal" data-test="intake-outtake-summary-screen">
      <OrderSummary order={order} intakeOrOuttake={intakeOrOuttake} />
      <Grid.Row>
        <Grid.Column>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell colSpan="3">Line Items</Table.HeaderCell>
              </Table.Row>
              <Table.Row>
                <Table.HeaderCell>Name</Table.HeaderCell>
                <Table.HeaderCell>Order Qty</Table.HeaderCell>
                <Table.HeaderCell>{`\u0394 Counted`}</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {sortedLineItems.map((lineItem, i) => {
                const inventoryItem = updatedInventoryItems.find((item) => item.skuBatch.id === lineItem.skuBatch?.id)
                if (!inventoryItem) {
                  return (
                    <Table.Row>
                      <S.Message negative header="Oops... something went wrong" content="InventoryItem not found" />
                    </Table.Row>
                  )
                }
                const inventoryLedgerItem = inventoryItem.inventoryLedgerItems.find(
                  (ledgerItem) => ledgerItem?.lineItem?.id === lineItem.id,
                )
                if (!inventoryLedgerItem) {
                  return (
                    <Table.Row>
                      <S.Message
                        negative
                        header="Oops... something went wrong"
                        content="InventoryLedgerItem not found"
                      />
                    </Table.Row>
                  )
                }
                return (
                  <IntakeOuttakeInventoryItemRow
                    key={`intake-outtake-summary-row-for-${lineItem.id}`}
                    index={i}
                    intakeOrOuttake={intakeOrOuttake}
                    inventoryItem={inventoryItem}
                    inventoryLedgerItem={inventoryLedgerItem}
                    lineItem={lineItem}
                    order={order}
                    sortedLineItems={sortedLineItems}
                  />
                )
              })}
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}
