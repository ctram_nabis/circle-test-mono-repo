import { Box, ErrorBox, S } from 'components'
import { LineItem, Order } from 'generated/graphql'
import { getInventoryValueRulesForOrderActionAndIsIntake, IntakeOrOuttakeType } from 'modules/Inventory/constants'
import { getWarehouseKeyFromWarehouseId } from 'modules/Warehouse'
import React from 'react'
import { Link } from 'react-router-dom'

interface Props {
  lineItem: LineItem
  sortedLineItems: LineItem[]
  intakeOrOuttake: IntakeOrOuttakeType
  order: Order
  index: number
  onNegativeCount?: () => void
  onNoNegativeCount?: () => void
}

export const IntakeOuttakeLineItemRow: React.FC<Props> = ({
  lineItem,
  sortedLineItems,
  intakeOrOuttake,
  index: i,
  order,
  onNegativeCount,
  onNoNegativeCount,
}) => {
  const { name, skuBatch, quantity } = lineItem
  const { id: skuBatchId, inventoryValues } = skuBatch || {}
  const { action, warehouse } = order

  const linkToSkuBatchPage = `inventory/${skuBatch?.id}`
  const skuBatchCode = `${skuBatch?.sku?.code ?? 'ERROR'}: ${skuBatch?.batch?.code ?? '---'}`
  const lineItemDescription = `${name}, ${skuBatchCode}`

  const warehouseId = warehouse?.id

  if (!lineItem || !sortedLineItems || !warehouseId) {
    return <S.Message neagtive header="Oops, something went wrong..." content="Please contact nabis engineering" />
  }

  const warehouseKey = getWarehouseKeyFromWarehouseId(warehouseId)

  // TODO: clean this up
  if (!warehouseKey) {
    return <ErrorBox error={'Oops, something went wrong...'} />
  }

  const inventoryKeysAndMultipliers = getInventoryValueRulesForOrderActionAndIsIntake({ action, intakeOrOuttake })
  const totalQtyForSkuBatchSoFar = sortedLineItems
    .slice(0, i)
    .filter((li) => li?.skuBatch?.id === skuBatchId)
    .reduce((acc, li) => {
      const _quantity = li.quantity || 0
      return acc + _quantity
    }, 0)

  const inventoryValueCells = inventoryKeysAndMultipliers.map(([inventoryKey, multiplier], k) => {
    const inventoryVal = inventoryValues?.[warehouseKey][inventoryKey]
    const valueSoFar = inventoryVal + totalQtyForSkuBatchSoFar * multiplier
    const newValue = valueSoFar + quantity * multiplier

    const isNegative = newValue < 0

    // bubble up to parent
    if (isNegative) {
      if (onNegativeCount) {
        onNegativeCount()
      }
    } else {
      if (onNoNegativeCount) {
        onNoNegativeCount()
      }
    }

    return (
      <S.Table.Cell key={`inventoryKey-${k}`}>
        <Box color={isNegative ? 'red' : 'green'}>
          {valueSoFar} <S.Icon name="arrow right" size="small" />
          {newValue}
        </Box>
      </S.Table.Cell>
    )
  })

  return (
    <S.Table.Row data-test="intake-outtake-line-item-row">
      <S.Table.Cell>
        <Link to={linkToSkuBatchPage} target="_blank">
          {lineItemDescription}
        </Link>
      </S.Table.Cell>
      <S.Table.Cell>{`${intakeOrOuttake === 'INTAKE' ? '+' : '-'}${quantity}`}</S.Table.Cell>
      {inventoryValueCells}
    </S.Table.Row>
  )
}
