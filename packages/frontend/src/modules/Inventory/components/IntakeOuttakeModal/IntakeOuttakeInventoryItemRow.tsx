import { Box } from 'components/core'
import { InventoryItem, InventoryLedgerItem, LineItem, Order } from 'generated/graphql'
import { IntakeOrOuttakeType } from 'modules/Inventory'
import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import { Icon, Message, Table } from 'semantic-ui-react'
import { titleize } from 'utils'

interface Props {
  index: number
  lineItem: LineItem
  sortedLineItems: LineItem[]
  inventoryItem: InventoryItem
  inventoryLedgerItem: InventoryLedgerItem
  intakeOrOuttake: IntakeOrOuttakeType
  order: Order
}

export function IntakeOuttakeInventoryItemRow({
  lineItem,
  index: i,
  intakeOrOuttake,
  inventoryItem,
  inventoryLedgerItem: matchingLedgerItem,
  order,
  sortedLineItems,
}: Props) {
  const { id: orderId } = order
  const { id, name, skuBatch, quantity } = lineItem
  const { id: skuBatchId } = skuBatch || {}

  const isIntake = intakeOrOuttake === 'INTAKE'

  // lineItems with same skuBatchId that we've already traversed
  const matchingLineItemsRenderedBeforeThis = sortedLineItems
    .slice(0, i)
    .filter((li) => li?.skuBatch?.id === skuBatchId)

  const totalQtyForSkuBatchSoFar = matchingLineItemsRenderedBeforeThis.reduce((acc, li) => {
    const _quantity = li.quantity || 0
    return acc + _quantity
  }, 0)

  const totalQtyForSkuBatchOnOrder = sortedLineItems
    .filter((li) => li?.skuBatch?.id === skuBatchId)
    .reduce((acc, li) => {
      const _quantity = li.quantity || 0
      return acc + _quantity
    }, 0)

  // add check values match the inventory ledger item
  // sort desc, get first
  const { counted, inventoryLedgerItems = [] } = inventoryItem

  // ledgerItems with same orderId as processed order
  const relevantLedgerItems = inventoryLedgerItems
    .filter((ledgerItem) => {
      return ledgerItem.order?.id === orderId && ledgerItem.transactionType === intakeOrOuttake
    })
    .sort((a, b) => {
      const [aDate, bDate] = [a.createdAt, b.createdAt]
      return aDate < bDate ? 1 : aDate && bDate && aDate > bDate ? 1 : 0
    })

  // for a unique lineItem, index will be 0, eg the latest ledgerItem
  // for duplicates we will grab the next index
  const firstLedgerItem = relevantLedgerItems[relevantLedgerItems.length - 1]
  const lastLedgerItem = relevantLedgerItems[0]

  const { startCounted: trueStartCounted } = firstLedgerItem
  const { endCounted: trueEndCounted } = lastLedgerItem

  const { startCounted: rowStartCounted, endCounted: rowEndCounted, transactionType } = matchingLedgerItem
  const quantityMultiplier = isIntake ? 1 : -1

  // start and end for display
  const calculatedStartCountedForRow = trueStartCounted + totalQtyForSkuBatchSoFar * quantityMultiplier
  const calculatedEndCountedForRow = calculatedStartCountedForRow + quantity * quantityMultiplier
  // for validating
  const calculatedEndCountedForOrder = trueStartCounted + totalQtyForSkuBatchOnOrder * quantityMultiplier

  /**
   * do some validating, if something goes wrong engineering should be contacted
   * TODO: replace contact with a rollback function
   */
  const rowCountDoesntMatch =
    calculatedStartCountedForRow !== rowStartCounted || calculatedEndCountedForRow !== rowEndCounted
  const countDoesntMatch = trueEndCounted !== calculatedEndCountedForOrder
  const ledgerDoesntMatch = trueEndCounted !== counted
  const transactionTypeDoesntMatch = transactionType !== intakeOrOuttake
  const somethingWentWrong = countDoesntMatch || transactionTypeDoesntMatch

  let unexpectedValueFlag: string | undefined
  if (somethingWentWrong) {
    unexpectedValueFlag = `${ledgerDoesntMatch ? 'Ledger count does not match item count!' : ''}
    ${rowCountDoesntMatch ? 'The count for this row does not add up correctly!' : ''}
    ${countDoesntMatch ? 'The final count does not add up correctly!' : ''} ${
      transactionTypeDoesntMatch ? `Transaction type doesnt match ${titleize(intakeOrOuttake)}!` : ''
    } Please contact engineering@nabis.com`
  }

  const _skuBatch = lineItem?.skuBatch
  const skuBatchCode = `${skuBatch?.sku?.code ?? 'ERROR'}: ${skuBatch?.batch?.code ?? '---'}`
  const linkToSkuBatchPage = `inventory/${_skuBatch?.id}`

  return (
    <Fragment >
      <Table.Row key={`updated-inventory-row-${id}`} data-test="intake-outtake-inventory-item-row">
        <Table.Cell>
          <Link to={linkToSkuBatchPage} target="_blank">
            {`${name}, ${skuBatchCode}`}
          </Link>
        </Table.Cell>
        <Table.Cell>{`${isIntake ? '+' : '-'}${quantity}`}</Table.Cell>
        <Table.Cell>
          <Box color={somethingWentWrong ? 'error' : 'success'}>
            {calculatedStartCountedForRow}
            {` `}
            <Icon name="arrow right" />
            {calculatedEndCountedForRow}
          </Box>
        </Table.Cell>
      </Table.Row>
      {unexpectedValueFlag && <Message error content={unexpectedValueFlag} />}
    </Fragment>
  )
}
