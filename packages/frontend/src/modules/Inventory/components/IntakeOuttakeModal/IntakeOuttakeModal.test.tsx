import { MockedProvider } from '@apollo/react-testing'
import { mount } from 'enzyme'
import React from 'react'
import { IntakeOuttakeModal } from './IntakeOuttakeModal'
// import { SkuBatchDropdown } from './SkuBatchDropdown'

describe('IntakeOuttakeModal container', () => {
  describe('IntakeOuttakeModal', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <IntakeOuttakeModal />
        </MockedProvider>,
      )
    })

    /**
     * TODO: simulate form input change, corresponding state change
     */

    /**
     * TODO: simulate button click handler, make sure jest mock fn is called
     */
  })

  // describe('SkuBatchDropdown', () => {
  // it('should render without crashing', () => {
  //   shallow(<SkuBatchDropdown />)
  // })
  // })
})
