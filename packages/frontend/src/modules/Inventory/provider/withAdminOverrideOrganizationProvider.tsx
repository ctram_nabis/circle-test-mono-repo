import { AdminOverrideOrganizationContext } from 'modules/Inventory/provider/AdminOverrideOrganizationContext'
import React from 'react'

export const withAdminOverrideOrganizationProvider = <P extends object>(Component: React.ComponentType<P>) =>
  function WrappedComponent(props: P) {
    const [adminOverrideOrganizationId, setAdminOverrideOrganizationId] = React.useState<string>('All Organizations')

    return (
      <AdminOverrideOrganizationContext.Provider
        value={{ adminOverrideOrganizationId, setAdminOverrideOrganizationId }}
      >
        <Component {...props} />
      </AdminOverrideOrganizationContext.Provider>
    )
  }
