import { Organization } from 'generated/graphql'
import { AdminOverrideOrganizationContext } from 'modules/Inventory/provider/AdminOverrideOrganizationContext'
import { useOrganization } from 'modules/Organization/useOrganization'
import { useContext } from 'react'

export function useAdminOverrideOrganizationContext() {
  const {
    canCreate,
    isNabisDeliveries,
    isOrgAdmin,
    isOrgOwner,
    orgRole,
    orgRoles,
    organization,
    setOrganizationId,
  } = useOrganization()

  const { adminOverrideOrganizationId, setAdminOverrideOrganizationId } = useContext(AdminOverrideOrganizationContext)

  return {
    canCreate,
    isNabisDeliveries,
    isOrgAdmin,
    isOrgOwner,
    orgRole,
    orgRoles,
    organization,
    setOrganizationId,
    adminOverrideOrganizationId,
    setAdminOverrideOrganizationId,
  }
}

export const useAdminOverrideOrganization = () => {
  const { organization, ...rest } = useAdminOverrideOrganizationContext()
  return { ...rest, organization: organization as Organization } // Force cast to get non-undefined value
}
