import React from 'react'

export interface AdminOverrideOrganizationContextType {
  adminOverrideOrganizationId: string
  setAdminOverrideOrganizationId: (id: string) => void
}

export const AdminOverrideOrganizationContext = React.createContext<AdminOverrideOrganizationContextType>({
  adminOverrideOrganizationId: 'All Organizations',
  setAdminOverrideOrganizationId: () => {},
})
