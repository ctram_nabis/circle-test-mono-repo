import { InventoryClassEnum } from 'generated/graphql'
import titleize from 'utils/formatters/titleize'

export const INVENTORY_CLASS_ARRAY = [
  'FLOWER',
  'EDIBLES',
  'PRE_ROLL',
  'CONCENTRATE',
  'CARTRIDGE',
  'DISPOSABLE',
  'TOPICAL',
  'OTHER',
]

export const INVENTORY_CLASS: Record<InventoryClassEnum, InventoryClassEnum> = {
  FLOWER: 'FLOWER',
  EDIBLES: 'EDIBLES',
  PRE_ROLL: 'PRE_ROLL',
  CONCENTRATE: 'CONCENTRATE',
  CARTRIDGE: 'CARTRIDGE',
  DISPOSABLE: 'DISPOSABLE',
  TOPICAL: 'TOPICAL',
  OTHER: 'OTHER',
}

export const INVENTORY_CLASS_SELECT_OPTIONS = Object.keys(INVENTORY_CLASS).map((value) => ({
  text: titleize(value),
  value,
}))
