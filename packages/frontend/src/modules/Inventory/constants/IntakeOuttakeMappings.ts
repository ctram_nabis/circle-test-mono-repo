import { OrderActionEnum } from 'generated/graphql'

export type IntakeOrOuttakeType = 'INTAKE' | 'OUTTAKE'
export type InventoryValueKeys = 'allocated' | 'available' | 'counted' | 'incoming' | 'outgoing'

export const ORDER_ACTION_TO_INTAKE_OUTTAKE: Record<OrderActionEnum, IntakeOrOuttakeType[]> = {
  DELIVERY_TO_RETAILER: ['OUTTAKE'],
  PICKUP_FROM_BRAND: ['INTAKE'],
  RETURN_FROM_RETAILER: ['INTAKE'],
  PICKUP_FROM_NABIS: ['OUTTAKE'],
  DROPOFF_TO_NABIS: ['INTAKE'],
  DELIVERY_TO_BRAND: ['OUTTAKE'],
  INTERNAL_TRANSFER: ['INTAKE', 'OUTTAKE'],
  PAYMENT_PICKUP_FROM_RETAILER: [],
  OTHER: ['INTAKE', 'OUTTAKE'],
}

const OrderActionValueMap = {
  DELIVERY_TO_RETAILER: {
    OUTTAKE: [
      ['outgoing', -1],
      ['available', 0],
      ['counted', -1],
    ],
  },
  PICKUP_FROM_BRAND: {
    INTAKE: [
      ['incoming', -1],
      ['available', 1],
      ['counted', 1],
    ],
  },
  PICKUP_FROM_NABIS: {
    OUTTAKE: [
      ['available', -1],
      ['counted', -1],
    ],
  },
  DROPOFF_TO_NABIS: {
    INTAKE: [
      ['incoming', -1],
      ['available', 1],
      ['counted', 1],
    ],
  },
  DELIVERY_TO_BRAND: {
    OUTTAKE: [
      ['available', -1],
      ['counted', -1],
    ],
  },
  INTERNAL_TRANSFER: {
    INTAKE: [
      ['incoming', -1],
      ['available', 0],
      ['counted', 1],
    ],
    OUTTAKE: [
      ['outgoing', -1],
      ['available', 0],
      ['counted', -1],
    ],
  },
  RETURN_FROM_RETAILER: {},
  PAYMENT_PICKUP_FROM_RETAILER: {},
  OTHER: {},
}

export const IntakeOuttakeDestinationMap = {
  INTAKE: {
    to: 'warehouse',
    from: 'licensedLocation',
  },
  OUTTAKE: {
    to: 'licensedLocation',
    from: 'warehouse',
  },
}

interface IntakeActionProps {
  action: OrderActionEnum
  intakeOrOuttake: IntakeOrOuttakeType
}

export const getInventoryValueRulesForOrderActionAndIsIntake = ({
  action,
  intakeOrOuttake,
}: IntakeActionProps): Array<[string, -1 | 0 | 1]> => OrderActionValueMap[action][intakeOrOuttake] || []
