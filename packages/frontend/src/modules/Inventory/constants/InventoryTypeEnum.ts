import { InventoryTypeEnum } from 'generated/graphql'

export const INVENTORY_TYPES_ARRAY = ['CANNABIS', 'NON_CANNABIS']

export const INVENTORY_TYPES: Record<InventoryTypeEnum, InventoryTypeEnum> = {
  CANNABIS: 'CANNABIS',
  NON_CANNABIS: 'NON_CANNABIS',
}

export const INVENTORY_TYPE_SELECT_OPTIONS: Array<{ key: string; text: string; value: InventoryTypeEnum }> = [
  {
    key: INVENTORY_TYPES.CANNABIS,
    text: 'Cannabis',
    value: INVENTORY_TYPES.CANNABIS,
  },
  {
    key: INVENTORY_TYPES.NON_CANNABIS,
    text: 'Non-Cannabis',
    value: INVENTORY_TYPES.NON_CANNABIS,
  },
]
