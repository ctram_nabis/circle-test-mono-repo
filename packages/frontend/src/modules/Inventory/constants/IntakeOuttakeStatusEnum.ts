export const INTAKE_OUTTAKE_STATUS = {
  INTAKE: 'INTAKE',
  OUTTAKE: 'OUTTAKE',
  NOT_INTAKE: 'NOT_INTAKE',
  NOT_OUTTAKE: 'NOT_OUTTAKE',
}

export const INTAKE_OUTTAKE_STATUS_SELECT_OPTIONS = [
  { key: 'INTAKE', value: 'INTAKE', text: 'Intaken' },
  { key: 'OUTTAKE', value: 'OUTTAKE', text: 'Outtaken' },
  { key: 'NOT_INTAKE', value: 'NOT_INTAKE', text: 'Not Intaken' },
  { key: 'NOT_OUTTAKE', value: 'NOT_OUTTAKE', text: 'Not Outtaken' },
]
