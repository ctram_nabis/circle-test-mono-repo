import styled from 'styled-components'
import colors from './variables/colors'

const Title = styled.div`
  color: ${colors.primary_text};
  font-size: 32px;
  font-weight: 600;
  margin: 10px 0px;
  @media (max-width: 700px) {
    font-size: 24px;
    margin: 5px 0px;
  }
`

export default Title
