import styled from 'styled-components'
import colors from './variables/colors'

const Link = styled.span`
  color: ${colors.primary_green};
  cursor: pointer;
  font-weight: 400;
  margin: 0px 5px;
  text-decoration: underline;
`

export default Link
