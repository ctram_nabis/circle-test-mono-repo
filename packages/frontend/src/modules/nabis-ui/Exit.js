import styled from 'styled-components'
import colors from './variables/colors'

const Exit = styled.div`
  -moz-transition: all 0.1s ease-in;
  -o-transition: all 0.1s ease-in;
  -webkit-transition: all 0.1s ease-in;
  cursor: pointer;
  font-size: 30px;
  font-weight: 300;
  line-height: 30px;
  text-align: right;
  &:hover {
    color: ${colors.secondary_text};
  }
`

export default Exit
