import sort from 'fast-sort'
import React from 'react'
import { Dropdown as SemanticDropdown } from 'semantic-ui-react'

const getDefaultSortByValueFromOption = (o) => {
  // For making sure that alphabetic sorting is case-insensitive
  if (typeof o.text === 'string') return o.text.toLowerCase()

  // As a fallback, o.value is sometimes an object, sometimes an ID
  // TODO: need to refactor the parent components that pass inconsistent value fields
  return o.value
}

const Dropdown = SemanticDropdown

Dropdown.defaultProps = {
  ...SemanticDropdown.defaultProps,
  selectOnBlur: false,
}

export const DropdownWithSort = (props) => {
  const { options = [], noneOption = [false], sorted = true, sortBy = getDefaultSortByValueFromOption } = props

  const sortedOptions = sorted ? sort(options).asc((o) => sortBy(o)) : options

  const [noneOptionBool, noneOptionAtStart, noneOptionObjectOverride = {}] = noneOption

  const noneOptionObject = {
    key: `${props.name}-null-option`,
    value: null,
    text: 'None',
    ...noneOptionObjectOverride,
  }

  const sortedWithOrWithoutNone = noneOptionBool
    ? noneOptionAtStart
      ? [noneOptionObject, ...sortedOptions]
      : [...sortedOptions, noneOptionObject]
    : sortedOptions

  const mergeProps = {
    ...props,
    options: sortedWithOrWithoutNone,
  }

  return <SemanticDropdown {...mergeProps} />
}

export default Dropdown
