import styled from 'styled-components'

export const SetBlueBold = (component) => styled(component)`
  color: #1298c9 !important;
  background: #f7fdff;
`
export const SetBlue = (component) => styled(component)`
  color: #1298c9 !important;
  font-weight: normal !important;
  background: #f7fdff;
`
export const SetBlueBackground = (component) => styled(component)`
  background: #f7fdff;
`

export const SetGreenBold = (component) => styled(component)`
  color: #0eae38 !important;
  background: #fafffa;
`
export const SetGreen = (component) => styled(component)`
  color: #0eae38 !important;
  font-weight: normal !important;
  background: #fafffa;
`
export const SetGreenBackground = (component) => styled(component)`
  background: #fafffa;
`

export const SetGreyBold = (component) => styled(component)`
  color: grey !important;
  background: #fafafa;
`
export const SetGrey = (component) => styled(component)`
  color: grey !important;
  font-weight: normal !important;
  background: #fafafa;
`
export const SetGreyBackground = (component) => styled(component)`
  background: #fafafa;
`

export const SetOrangeBold = (component) => styled(component)`
  color: #ff9b0f !important;
  background: #fff9f0;
`
export const SetOrange = (component) => styled(component)`
  color: #ff9b0f !important;
  font-weight: normal !important;
  background: #fff9f0;
`
export const SetOrangeBackground = (component) => styled(component)`
  background: #fff9f0;
`

export const SetRed = (component) => styled(component)`
  color: #e32d2d;
  background: #fffafa;
`
