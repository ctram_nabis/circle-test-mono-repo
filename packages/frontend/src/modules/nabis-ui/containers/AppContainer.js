import styled from 'styled-components'
import colors from '../variables/colors'

const AppContainer = styled.div`
  background-color: ${colors.lighter_grey};
  display: flex;
  height: 100%;
  justify-content: center;
  padding: 15px 10px;
  width: 100%;
  overflow: auto;
`

export default AppContainer
