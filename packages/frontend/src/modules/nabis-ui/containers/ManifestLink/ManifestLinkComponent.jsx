import React from 'react'

import { Icon } from 'semantic-ui-react'
import styled from 'styled-components'

import { Error } from '../../../nabis-ui'

const ActionLink = styled.a`
  cursor: pointer;
  text-decoration: underline;
`

// interface ManifestLinkComponentProps {
//   id: string
//   manifestGDriveFileId: string
//   refetchQueries: string[]
// }

export class ManifestLinkComponent extends React.Component {
  state = { generating: false }

  handleGenerate = async (orderId, refetchQueries) => {
    this.setState({ generating: true })
    try {
      await this.props.generateManifest(orderId, refetchQueries)
      await this.props.hook()
    } catch (error) {
      this.setState({ error: error.message })
    } finally {
      this.setState({ generating: false })
    }
  }

  render() {
    const { error, generating } = this.state
    const { manifestGDriveFileId: fileId, orderId, refetchQueries } = this.props

    if (generating) {
      return <Icon color="green" loading name="spinner" />
    }

    if (error) {
      return <Error>{error}</Error>
    }

    if (!fileId) {
      return <ActionLink onClick={() => this.handleGenerate(orderId, refetchQueries)}>Generate</ActionLink>
    }

    return (
      <ActionLink target="_blank" href={`https://drive.google.com/open?id=${fileId}`}>
        <Icon size="large" name="file pdf outline" />
      </ActionLink>
    )
  }
}
