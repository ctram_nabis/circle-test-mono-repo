import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  width: 100%;
  max-width: 1600px;
  margin-bottom: 12px;
`

export default Wrapper
