import styled from 'styled-components'
import colors from '../variables/colors'

const OnboardingContainer = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100vh;
  padding: 30px 15px;
  width: 100%;
  background: ${colors.lighter_grey};
`

export default OnboardingContainer
