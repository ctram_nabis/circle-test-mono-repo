import styled from 'styled-components'
import colors from './variables/colors'

const InlineError = styled.span`
  color: ${colors.primary_orange};
`

export default InlineError
