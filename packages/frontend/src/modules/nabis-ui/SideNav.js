import styled from 'styled-components'
import colors from './variables/colors'
// import { SideNav as SSideNav } from 'semantic-ui-react'

const SideNav = styled.div`
  -webkit-user-select: none; /* Safari */
  -moz-user-select: none; /* Firefox */
  -ms-user-select: none; /* IE10+/Edge */
  user-select: none; /* Standard */
  background-color: ${colors.primary_green};
  border-right: 1px solid ${colors.divider_grey};
  display: flex;
  flex-direction: column;
  flex: 0 0 80px;
  min-height: 400px;
  overflow-y: auto;
`

// SideNav.Item = styled.div`
//   -moz-transition: all 0.2s ease-in;
//   -o-transition: all 0.2s ease-in;
//   -webkit-transition: all 0.2s ease-in;
//   align-items: center;
//   border-bottom: 1px dotted ${colors.dark_grey};
//   cursor: pointer;
//   display: flex;
//   flex-direction: column;
//   flex: 0 0 80px;
//   justify-content: center;
//   &:hover {
//     background-color: ${colors.primary_dark_green};
//   }
// `

// SideNav.Icon = styled.img`
//   height: 30px;
//   margin: 3px;
//   width: auto;
// `

// SideNav.Label = styled.div`
//   color: white;
//   font-size: 13px;
//   font-weight: 400;
// `

// SideNav.Footer = styled.div`
//   display: flex;
//   flex-direction: column;
//   flex: 1 0 0;
//   justify-content: flex-end;
// `

// SideNav.FooterItem = styled(SideNav.Item)`
//   border-bottom: none;
//   border-top: 1px dotted ${colors.divider_grey};
// `

export default SideNav
