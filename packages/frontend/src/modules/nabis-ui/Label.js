import styled from 'styled-components'
import colors from './variables/colors'

const Label = styled.div`
  color: ${colors.primary_text};
  font-size: 20px;
  font-weight: 400;
  margin: 10px 0px 2px;
  @media (max-width: 700px) {
    font-size: 16px;
  }
`

export default Label
