import styled from 'styled-components'

const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 700px;
  @media (max-width: 700px) {
    width: 100%;
  }
`

export default Form
