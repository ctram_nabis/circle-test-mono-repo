import styled from 'styled-components'
import colors from './variables/colors'

const Whisper = styled.div`
  color: ${colors.secondary_text};
  font-size: 16px;
  font-weight: 400;
`

export default Whisper
