import styled from 'styled-components'
import { Header } from 'semantic-ui-react'

const PageHeader = styled(Header)`
  font-size: 3.5em !important;
`

export default PageHeader
