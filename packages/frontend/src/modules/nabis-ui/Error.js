import styled from 'styled-components'
import colors from './variables/colors'

export const Error = styled.div`
  color: ${colors.primary_orange};
`

export default Error
