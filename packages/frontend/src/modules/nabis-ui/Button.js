import React from 'react'
import { Button as SemanticButton } from 'semantic-ui-react'
import hoistNonReactStatics from 'hoist-non-react-statics'

const WrappedButton = (props) => {
  const { color, error } = props

  const mergeProps = {
    color: error ? 'red' : color,
    ...props,
  }

  return <SemanticButton {...mergeProps} />
}

hoistNonReactStatics(WrappedButton, SemanticButton)

export default WrappedButton
