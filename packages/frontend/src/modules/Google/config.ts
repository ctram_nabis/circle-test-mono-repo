import { LatLngLiteral } from '@google/maps'
import { isEnv } from 'utils/isHelpers'

const analytics = isEnv({
  production: 'UA-125404889-1',
  staging: 'UA-131148352-1',
  fallback: 'UA-131148352-1',
})

const apiKey = isEnv({
  production: 'AIzaSyCEslhBqTpEZYvGg8neg5kdu1izNnfIpFM',
  staging: 'AIzaSyB9ZkMrkLRIahjdMVQexV54CiO9mLWqUI0',
  fallback: 'AIzaSyBXpq530P70c0iiCSJ2L6i_cfplT8iTXZY',
})

const defaultCenter: LatLngLiteral = { lat: 37.735501, lng: -122.20032700000002 }

export default {
  analytics,
  maps: {
    version: '3',
    libraries: ['geometry', 'drawing', 'places'],
    apiKey,
    defaultCenter,
  },
}
