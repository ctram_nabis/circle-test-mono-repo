import { GoogleAPI, GoogleApiWrapper, Map, Marker } from 'google-maps-react'
import React from 'react'

import { Loading } from 'components'
import config from 'config'

import { MapProps } from './types'

interface Props extends MapProps {
  google: GoogleAPI
}

const defaultCoords = config.google.maps.defaultCenter

export const MapBase = ({ google, zoom = 10, coords = defaultCoords }: Props) => (
  <Map google={google} zoom={zoom} initialCenter={defaultCoords} center={coords}>
    <Marker position={coords} />
  </Map>
)

export const MapOption3 = GoogleApiWrapper({
  apiKey: config.google.maps.apiKey,
  libraries: config.google.maps.libraries,
  version: config.google.maps.version,
  LoadingContainer: <Loading />,
})(MapBase)
