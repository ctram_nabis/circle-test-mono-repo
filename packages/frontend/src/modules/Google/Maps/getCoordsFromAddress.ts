import logger from 'modules/Logger'
import { AddressProps, getFullAddress } from 'utils'
import { googleMapsClient } from './client'
import { Coordinates } from './types'

export async function getCoordsFromAddress(address: string | AddressProps): Promise<void | Coordinates> {
  try {
    let fullAddress

    if (typeof address === 'object') {
      fullAddress = getFullAddress(address)
    } else {
      fullAddress = address
    }
    const result = await googleMapsClient.geocode({ address: fullAddress }).asPromise()
    return result.json.results[0].geometry.location
  } catch (err) {
    return logger.error(`Geocode was not successful for the following reason: ${err}`)
  }
}
