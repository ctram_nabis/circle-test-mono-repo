import React from 'react'

import { MapOption1 } from './MapOption1'
import { MapOption2 } from './MapOption2'
import { MapOption3 } from './MapOption3'
import { MapProps } from './types'

export const MapComponent = ({ coords }: MapProps) => (
  <>
    <MapOption1 key="map1" coords={coords} />
    <MapOption2 key="map2" coords={coords} />
    <MapOption3 key="map3" coords={coords} />
  </>
)
