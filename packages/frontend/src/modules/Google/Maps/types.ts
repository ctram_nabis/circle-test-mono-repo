export type Coordinates = google.maps.LatLngLiteral | google.maps.LatLng

export interface MapProps {
  coords?: Coordinates
  zoom?: number
}
