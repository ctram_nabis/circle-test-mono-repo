import { Loading } from 'components'
import config from 'config'
import React from 'react'
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from 'react-google-maps'
import { compose, defaultProps } from 'recompose'
import { Container } from 'semantic-ui-react'
import { MapProps } from './types'

const MapBase = ({ coords, zoom }: MapProps) => (
  <GoogleMap defaultZoom={zoom} defaultCenter={coords} center={coords}>
    <Marker position={coords} />
  </GoogleMap>
)

export const MapOption1 = compose<MapProps, MapProps>(
  defaultProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${config.google.maps.apiKey}&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <Loading />,
    containerElement: <Container height={400} />,
    mapElement: <Container height={400} />,
    coords: config.google.maps.defaultCenter,
    zoom: 10,
  }),
  withScriptjs,
  withGoogleMap,
)(MapBase)
