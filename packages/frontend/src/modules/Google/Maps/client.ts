import GoogleMaps from '@google/maps'
import config from 'config'

export const googleMapsClient = GoogleMaps.createClient({
  key: config.google.maps.apiKey,
  Promise,
})
