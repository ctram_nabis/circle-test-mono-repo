import { GoogleMap, Marker, useLoadScript } from '@react-google-maps/api'
import { ErrorBox, Loading } from 'components'
import config from 'config'
import React from 'react'
import { MapProps } from './types'

const defaultCoords = config.google.maps.defaultCenter

export const MapOption2 = ({ coords = defaultCoords, zoom = 10 }: MapProps) => {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: config.google.maps.apiKey,
    libraries: config.google.maps.libraries,
    version: config.google.maps.version,
  })

  if (!isLoaded) return <Loading />

  if (loadError) return <ErrorBox error="Google maps couldn't load" />

  return (
    <GoogleMap zoom={zoom} center={coords}>
      <Marker position={coords} />
    </GoogleMap>
  )
}
