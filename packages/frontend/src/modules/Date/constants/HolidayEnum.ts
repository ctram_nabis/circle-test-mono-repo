import moment from 'moment'

export const stringHolidays = [
  // Thanksgiving
  '2019-11-28',
  // Black Friday
  '2019-11-29',
  // TODO: remove me after, ad hoc request from #software_support
  '2019-12-02',
  // Xmas
  '2019-12-25',
  // NYE
  '2019-12-31',
  // NY
  '2020-01-01',
]

export const jsDateHolidays = stringHolidays.map((holiday) => new Date(holiday))

export const momentHolidays = stringHolidays.map((holiday) => moment(holiday))

export const momentHolidaysUtc = stringHolidays.map((holiday) => moment(holiday).utc())

export const momentHolidaysCalifornia = stringHolidays.map((holiday) => moment(holiday).tz('America/Los_Angeles'))

export const momentHolidaysCaliforniaUtc = stringHolidays.map((holiday) =>
  moment(holiday)
    .tz('America/Los_Angeles')
    .utc(),
)
