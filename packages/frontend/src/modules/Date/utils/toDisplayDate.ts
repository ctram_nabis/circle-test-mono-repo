export const toDisplayDate = (date: Date): Date => {
  // XXX: Order.date on the database is stored in UTC, but whatever year, month, day
  // should be considered the local date. For example, on db date value is '2020-01-01T00:00:00Z'
  // A local user in time zone -8 expects an order's date on the UI to be '2020-01-01T00:00:00-8', NOT
  // '2019-01-01T00:00:00-8' -- for now, we take the UTC date and extract the year, month, day and use
  // for the date picker
  const [year, month, day] = [date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()]
  return new Date(year, month, day, 0, 0)
}
