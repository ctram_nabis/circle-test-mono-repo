import moment from 'moment'
import { momentHolidaysUtc } from '../constants'

export const isValidBookingDate = (d: Date, isAdmin: boolean = false): boolean => {
  let isHoliday = false

  momentHolidaysUtc.forEach((m) => {
    if (m.isSame(moment(d).utc(), 'day')) {
      isHoliday = true
    }
  })

  if (isHoliday) {
    return false
  }

  if (isAdmin) {
    return !!d
  }

  if (d.getDay() !== 0 && d.getDay() !== 6) {
    return !!d
  }

  return false
}
