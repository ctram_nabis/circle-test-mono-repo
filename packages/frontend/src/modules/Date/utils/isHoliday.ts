import moment, { Moment } from 'moment'
import { momentHolidaysCaliforniaUtc } from '../constants'

export const isMomentHoliday = (date: Moment) => {
  let _isHoliday = false
  momentHolidaysCaliforniaUtc.forEach((m) => {
    if (m.isSame(moment(date).utc(), 'day')) {
      _isHoliday = true
    }
  })
  return _isHoliday
}
