import moment from 'moment'
import { isMomentHoliday } from './isHoliday'
import { isMomentWeekend } from './isWeekend'

export const getMinimumBookableDate = (
  isAdmin: boolean = false,
  filter?: (d: Date) => boolean,
  isCanndescent = false,
): moment.Moment => {
  const now = moment().tz('America/Los_Angeles')
  const whatDayInCalifornia = now.day()

  if (isAdmin) {
    return moment(0)
  }

  const noon = now
    .clone()
    .set('hour', 12)
    .set('minutes', 0)

  const canndescentCutoff = now
    .clone()
    .set('hour', 12)
    .set('minutes', 30)

  const isAfterNoon = now.isAfter(noon, 'minutes')
  const isAfterCanndescentCutoff = now.isAfter(canndescentCutoff, 'minutes')

  const cursor = now.clone()

  let businessDaysToAdd = isAfterNoon ? 3 : 2

  const addDay = () => {
    cursor.add(1, 'day')
  }

  const stepForward = () => {
    const nextDay = cursor.clone().add(1, 'day')
    const weekend = isMomentWeekend(nextDay)
    const holiday = isMomentHoliday(nextDay)
    // a filtered day is an unavailable business day
    const filtered = filter && filter(nextDay.toDate())
    if (filtered && !(weekend || holiday)) {
      // so we count this towards lead time
      businessDaysToAdd -= 1
      addDay()
      // and we skip ahead another day
      stepForward()
    } else if (weekend || holiday) {
      addDay()
    } else {
      businessDaysToAdd -= 1
      addDay()
    }
  }

  // On Thursday, non-canndescent brands are cutoff after 12pm
  if (whatDayInCalifornia === 4 && !isCanndescent) {
    businessDaysToAdd = isAfterNoon ? 3 : 2
  } else if (whatDayInCalifornia === 4 && isCanndescent) {
    businessDaysToAdd = 1
  }

  // On Friday, canndescent is cutoff after 12:30pm
  if (whatDayInCalifornia === 5 && isCanndescent && !isAfterCanndescentCutoff) {
    businessDaysToAdd = 1
  }

  while (businessDaysToAdd > 0) {
    stepForward()
  }

  return cursor
    .clone()
    .set('hour', 0)
    .set('minutes', 0)
}
