import { Moment } from 'moment'

export const toJSDateUtc = (date: Moment) => {
  // XXX: Order.date on the database is stored in UTC, but whatever year, month, day
  // should be considered the local date. For example, on db date value is '2020-01-01T00:00:00Z'
  // A local user in time zone -8 expects an order's date on the UI to be '2020-01-01T00:00:00-8', NOT
  // '2019-01-01T00:00:00-8' -- for now, we take the UTC date and extract the year, month, day and use
  // for the date picker
  const utc = date.clone().utc()
  const [year, month, day, hours, minutes] = [utc.year(), utc.month(), utc.date(), utc.hour(), utc.minute()]
  return new Date(year, month, day, hours, minutes)
}
