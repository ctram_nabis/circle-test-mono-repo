import moment from 'moment'

export const isDateBlacklisted = (date: Date, blacklist: string[], isExempt: boolean): boolean => {
  if (isExempt) {
    return false
  }

  const isBlacklisted = Boolean(blacklist.find((dateString) => moment(date).isSame(moment(dateString), 'day')))

  return isBlacklisted
}
