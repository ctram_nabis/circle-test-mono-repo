import { Moment } from 'moment'

export const isWeekend = (d: Date): boolean => d.getUTCDay() === 0 || d.getUTCDay() === 6

export const isMomentWeekend = (d: Moment): boolean => d.isoWeekday() === 6 || d.isoWeekday() === 7
