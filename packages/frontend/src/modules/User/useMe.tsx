// tslint:disable strict-type-predicates -- chris
/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */

import { FullScreenLoader } from 'components'
import { useMeQuery } from 'generated/graphql'
import { AUTH_TOKEN } from 'modules/App'
import { ErrorPage } from 'modules/App/containers'
import { useAuthToken } from 'modules/Auth'
import logger from 'modules/Logger'
import { Storage } from 'modules/Storage'
import { AuthUser } from 'modules/User/types'
import React, { useEffect } from 'react'

export const UserContext = React.createContext<{
  user?: AuthUser
  isAuthenticated: boolean
  clearUser: () => void
}>({
  isAuthenticated: !!Storage.loadString(AUTH_TOKEN),
  clearUser: () => {},
})

export function useUserContext() {
  const context = React.useContext(UserContext)
  if (context === undefined) throw new Error('useUserContext must be used within UserProvider')
  return context
}

export function useMe() {
  const { isAuthenticated, user, clearUser } = useUserContext()
  return { isAuthenticated, user: user as AuthUser, isAdmin: user?.isAdmin, clearUser }
}

const UserProvider = <P extends any>(props: React.PropsWithChildren<P>) => {
  const [user, setUser] = React.useState<AuthUser | undefined>()
  const { token } = useAuthToken()
  // const signOut = useSignOut()
  // const router = useRouter()
  // const pathname = router.location.pathname
  const isAuthenticated = !!token

  const { data, loading = false, error } = useMeQuery({
    skip: !isAuthenticated,
    fetchPolicy: 'network-only',
  })

  // useEffect(() => {
  //   if (token && token !== 'undefined') {
  //     logger.info(`USER PROVIDER ------------  getUser`)
  //     getUser()
  //   } else {
  //     if (pathname !== 'sign-in') {
  //       signOut()
  //     }
  //     logger.info(`USER PROVIDER ------------  no token provided, not fetching user`)
  //   }
  // }, [getUser, token, pathname, signOut])

  useEffect(() => {
    if (token && token !== 'undefined' && data) {
      setUser(data?.viewer?.me as AuthUser)
    }
  }, [token, data])

  if (error) {
    logger.error(`USER PROVIDER ------------  apollo error`, error)
    // alert('Something is wrong. Try resetting your cookies.')
  }

  const _user = (data?.viewer?.me || user) as AuthUser

  const clearUser = () => setUser(undefined)

  return (
    <UserContext.Provider value={{ user: _user, isAuthenticated, clearUser }}>
      {error ? <ErrorPage /> : loading && !_user ? <FullScreenLoader /> : props.children}
    </UserContext.Provider>
  )
}

export const withUserProvider = <P extends any>(Component: React.ComponentType<P>) =>
  function WrappedComponent(props: P) {
    return (
      <UserProvider>
        <Component {...props} />
      </UserProvider>
    )
  }
