import { useMe } from 'modules/User'
import React from 'react'

export function withMeHook(Component) {
  return function WrappedComponent(props) {
    const me = useMe()
    return <Component {...props} me={me} />
  }
}
