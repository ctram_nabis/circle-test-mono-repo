import { Container, Form, H5 } from 'components'
import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { ErrorBox } from 'components/ErrorBox'
import { ResetPasswordInput, useResetPasswordMutation, useValidateTokenMutation } from 'generated/graphql'
import { GraphQLError } from 'graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import logger from 'modules/Logger'
import { useRouter } from 'modules/Navigation'
import React, { useEffect, useState } from 'react'
import { Card } from 'semantic-ui-react'
import { oc } from 'ts-optchain'
import { ErrorType } from 'types'

export const ResetPassword = () => {
  const [resetPassword] = useResetPasswordMutation()
  const [isTokenValid, setIsTokenValid] = useState<boolean | undefined>(undefined)
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()
  const router = useRouter()
  const { email, token } = router.query

  const [validateToken, validateTokenResponse] = useValidateTokenMutation()

  type ResetPasswordInputWithoutEmailToken = Omit<ResetPasswordInput, 'email' | 'token'>

  const validationSchema = createValidationSchema<ResetPasswordInputWithoutEmailToken>({
    newPassword: validators.newPassword,
    confirmPassword: validators.passwordConfirmation,
  })

  if (!validateTokenResponse.called && email && token) {
    validateToken({
      variables: {
        email,
        token,
      },
    })
  }

  useEffect(() => {
    const returnedToken = oc(validateTokenResponse).data.validateToken.token('')
    const isValid = Boolean(returnedToken)

    setIsTokenValid(isValid)

    logger.debug('[Reset Token]: ', returnedToken)
    logger.debug('[isValid]: ', isValid)

    const errors = validateTokenResponse.error?.graphQLErrors as GraphQLError[]
    if (errors) {
      setRequestErrors(errors)
      logger.error('ResetPassword: Error validating Token: ', errors)

      router.push('/')
    }
  }, [validateTokenResponse, router])

  const { values, errors: formErrors, handleBlur, handleChange, handleSubmit } = useForm<typeof validationSchema>({
    validationSchema,
    isInitialValid: false,
    initialValues: {
      confirmPassword: '',
      newPassword: '',
    },
    onSubmit: async (inputs) => {
      try {
        const { errors } = await resetPassword({
          variables: {
            input: {
              email,
              token,
              ...inputs,
            },
          },
        })

        if (errors) {
          setRequestErrors(errors)
          logger.error('[Error Resetting Password]: ', errors)
        } else {
          // TODO: Change this to a typed path
          router.push('/my-organizations')
        }
      } catch (err) {
        if (Array.isArray(err?.graphQLErrors)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors.message)
        }
      }
    },
  })

  const fields = {
    password: createFormField({
      control: 'input',
      required: true,
      label: 'New Password *',
      name: 'newPassword',
      onChange: handleChange,
      onBlur: handleBlur,
      placeholder: 'Password',
      type: 'password',
      autoComplete: 'new-password',
      values,
      errors: formErrors,
      key: 'newPassword',
    }),
    confirmPassword: createFormField({
      control: 'input',
      required: true,
      label: 'Confirm New Password *',
      name: 'confirmPassword',
      onChange: handleChange,
      onBlur: handleBlur,
      placeholder: 'Confirm password',
      type: 'password',
      autoComplete: 'confirm-password',
      values,
      errors: formErrors,
      key: 'confirmPassword',
    }),
  }

  return (
    <Container middle center>
      <Box maxWidth="65%" margin="auto">
        <H5>Changing password for {email}</H5>
        <Text>Try combining 3 or 4 of your favorite words to make a strong passphrase</Text>
        <Card fluid>
          <Card.Content>
            <Form id="reset-password" onSubmit={handleSubmit}>
              <ErrorBox errors={requestErrors} error={catchError} />
              {fields.password}
              {fields.confirmPassword}
              <Button
                disabled={!isTokenValid}
                loading={validateTokenResponse.loading}
                title="Submit"
                type="submit"
                color="green"
                form="reset-password"
                name="check"
              />
            </Form>
          </Card.Content>
        </Card>
      </Box>
    </Container>
  )
}
