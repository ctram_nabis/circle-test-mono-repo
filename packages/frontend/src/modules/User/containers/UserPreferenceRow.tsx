import { S } from 'components'
import { UserPreferenceFragmentFragment, useUpdateUserPreferenceMutation } from 'generated/graphql'
import logger from 'modules/Logger'
import React, { useState } from 'react'

/**
 * @zane - Calling this will update 'receiveOrderUpdateEmails' for the userPreference
 * using the value from `data'.
 */

interface Props {
  userPreference: UserPreferenceFragmentFragment
}

export const UserPreferenceRow = ({ userPreference }: Props) => {
  const [isChecked, toggleChecked] = useState<boolean>(userPreference.receiveOrderUpdateEmails || false)
  const [updateUserPreference] = useUpdateUserPreferenceMutation()
  const toggleEmailSubscription = async (_userPreference: UserPreferenceFragmentFragment, data: S.CheckboxProps) => {
    try {
      toggleChecked(!isChecked)
      await updateUserPreference({
        variables: {
          organizationId: _userPreference.organization.id,
          userId: _userPreference.user.id,
          receiveOrderUpdateEmails: data.checked,
        },
      })
    } catch (error) {
      toggleChecked(!isChecked)
      logger.error(error)
    }
  }
  return (
    <S.TableRow key={userPreference.id}>
      <S.TableCell>{userPreference.organization.name}</S.TableCell>
      <S.TableCell textAlign="right">
        <S.Checkbox toggle checked={isChecked} onChange={(_e, d) => toggleEmailSubscription(userPreference, d)} />
      </S.TableCell>
    </S.TableRow>
  )
}
