import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { Form } from 'components/Form'
import { H3 } from 'components/H3'
import { Label } from 'components/Label'
import { S } from 'components/SemanticComponents'
import { UpdateUserInput, useUpdateUserMutation } from 'generated/graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import logger from 'modules/Logger'
import { ChangePasswordModal } from 'modules/User/containers/ChangePasswordModal'
import { UserPreferenceRow } from 'modules/User/containers/UserPreferenceRow'
import { useMe } from 'modules/User/useMe'
import React, { useState } from 'react'
import styled from 'styled-components'
import { ErrorType } from 'types'

const Wrapper = styled(Box)`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  width: 100%;
  max-width: 1600px;
  margin-bottom: 12px;
`

const TextButton = styled(Button)`
  &.ui.button.basic.green {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
`

export const MyAccount = () => {
  const { user } = useMe()
  const [updateUser] = useUpdateUserMutation()
  // const [changePassword] = useChangeUserPasswordMutation()
  // const [updateUserPreference] = useUpdateUserPreferenceMutation()
  const [successMessage, showSuccessMessage] = useState<string | false>(false)
  const [, setRequestErrors] = useState<ErrorType[]>()
  const [isChangingPassword, setIsChangingPassword] = useState<boolean>(false)

  const validationSchema = createValidationSchema({
    firstName: validators.name,
    lastName: validators.name,
    address1: validators.string,
    address2: validators.string,
    city: validators.string,
    state: validators.string,
    zip: validators.string,
    phone: validators.string,
  })

  const {
    handleSubmit: handleAccountSubmit,
    errors: accountErrors,
    handleBlur: handleAccountBlur,
    handleChange: handleAccountChange,
    isSubmitting: isAccountSubmitting,
    values: accountValues,
  } = useForm<typeof validationSchema>({
    validationSchema,
    enableReinitialize: true,
    initialValues: {
      firstName: user?.firstName || '',
      lastName: user?.lastName || '',
      address1: user?.address1 || '',
      address2: user?.address2 || '',
      city: user?.city || '',
      state: user?.state || '',
      zip: user?.zip || '',
      phone: user?.phone || '',
    },
    onSubmit: async (input) => {
      try {
        const response = await updateUser({
          variables: {
            input: {
              id: user?.id,
              ...input,
            } as UpdateUserInput,
          },
        })

        if (response.errors) {
          return setRequestErrors(response.errors)
        }
        return showSuccessMessage('Account Updated!')
      } catch (error) {
        logger.error(error)
      }
    },
  })

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        control: 'input',
        type: 'text',
        size: 'large',
        label: 'First Name',
        labelStyles: { bg: 'transparent' },
        placeholder: 'John',
        name: 'firstName',
        autoComplete: 'on',
        values: accountValues,
        errors: accountErrors,
        onChange: handleAccountChange,
        onBlur: handleAccountBlur,
        required: false,
        key: 'firstName',
      }),
      createFormField({
        control: 'input',
        type: 'text',
        size: 'large',
        label: 'Last Name',
        labelStyles: { bg: 'transparent' },
        placeholder: 'Smith',
        name: 'lastName',
        autoComplete: 'on',
        values: accountValues,
        errors: accountErrors,
        onChange: handleAccountChange,
        onBlur: handleAccountBlur,
        required: false,
        key: 'lastName',
      }),
    ],
    [
      createFormField({
        control: 'input',
        type: 'text',
        size: 'large',
        label: 'Address 1',
        labelStyles: { bg: 'transparent' },
        placeholder: '1234 Main St.',
        name: 'address1',
        autoComplete: 'on',
        values: accountValues,
        errors: accountErrors,
        onChange: handleAccountChange,
        onBlur: handleAccountBlur,
        required: false,
        key: 'address1',
      }),
      createFormField({
        control: 'input',
        type: 'text',
        size: 'large',
        label: 'Address 2',
        labelStyles: { bg: 'transparent' },
        placeholder: 'Suite 123',
        name: 'address2',
        autoComplete: 'on',
        values: accountValues,
        errors: accountErrors,
        onChange: handleAccountChange,
        onBlur: handleAccountBlur,
        required: false,
        key: 'address2',
      }),
    ],
    [
      createFormField({
        control: 'input',
        type: 'text',
        size: 'large',
        label: 'City',
        labelStyles: { bg: 'transparent' },
        placeholder: 'San Francisco',
        name: 'city',
        autoComplete: 'on',
        values: accountValues,
        errors: accountErrors,
        onChange: handleAccountChange,
        onBlur: handleAccountBlur,
        required: false,
        key: 'city',
      }),
      createFormField({
        control: 'input',
        type: 'text',
        size: 'large',
        label: 'State',
        labelStyles: { bg: 'transparent' },
        placeholder: 'CA',
        name: 'state',
        autoComplete: 'on',
        values: accountValues,
        errors: accountErrors,
        onChange: handleAccountChange,
        onBlur: handleAccountBlur,
        required: false,
        key: 'state',
      }),
      createFormField({
        control: 'input',
        type: 'text',
        size: 'large',
        label: 'Zip Code',
        labelStyles: { bg: 'transparent' },
        placeholder: '94103',
        name: 'zip',
        autoComplete: 'on',
        values: accountValues,
        errors: accountErrors,
        onChange: handleAccountChange,
        onBlur: handleAccountBlur,
        required: false,
        key: 'zip',
      }),
    ],
    [
      createFormField({
        control: 'input',
        type: 'phone',
        size: 'large',
        label: 'Phone',
        labelStyles: { bg: 'transparent' },
        placeholder: '',
        name: 'phone',
        autoComplete: 'on',
        values: accountValues,
        errors: accountErrors,
        onChange: handleAccountChange,
        onBlur: handleAccountBlur,
        required: false,
        key: 'phone',
      }),
    ],
  ]

  return (
    <S.Container>
      <Box width="80%" m="0 auto">
        <Box mb={20}>
          <H3 mt={10} mb={10} textAlign="center" fontSize={32} fontWeight={600}>
            My Account
          </H3>
        </Box>
        {successMessage && (
          <S.Message color="green" onDismiss={() => showSuccessMessage(false)} content={successMessage} />
        )}
        <Wrapper>
          <Form width="100%" id="my-account-form" onSubmit={handleAccountSubmit}>
            <S.Card fluid>
              {user?.isAdmin && <S.Label color="green">Admin User</S.Label>}
              <S.Card.Content>
                <Form.Field>
                  <Label>Email</Label>
                  {user?.email || '-'}
                </Form.Field>
                {fieldsInGroups.map((fields, i) => (
                  // TODO: maybe create a helper function?
                  <S.FormGroup key={`account-formGroup-${i}`} widths="equal">
                    {fields}
                  </S.FormGroup>
                ))}

                <Text textAlign="center">
                  <Button
                    loading={isAccountSubmitting}
                    title="Save"
                    type="submit"
                    form="my-account-form"
                    color="green"
                    name="check"
                  />
                </Text>
              </S.Card.Content>
            </S.Card>
          </Form>
        </Wrapper>
        <Wrapper>
          <S.Card fluid>
            <S.Card.Content>
              <Label>Password</Label>
              <Box m="14px">
                <TextButton
                  color="green"
                  basic
                  type="button"
                  title="Change Password"
                  onClick={() => setIsChangingPassword(true)}
                />
              </Box>
            </S.Card.Content>
          </S.Card>
        </Wrapper>
        {user?.userPreferences && (
          <Wrapper>
            <S.Card fluid>
              <S.Card.Content>
                <Label>Email Notifications</Label>
                <Box m="14px">
                  <S.Table>
                    <S.TableHeader>
                      <S.TableRow>
                        <S.TableHeaderCell>Organization</S.TableHeaderCell>
                        <S.TableHeaderCell textAlign="right">On/Off</S.TableHeaderCell>
                      </S.TableRow>
                    </S.TableHeader>
                    <S.TableBody>
                      {user?.userPreferences?.map((userPreference) => (
                        <UserPreferenceRow userPreference={userPreference} key={userPreference.id} />
                      ))}
                    </S.TableBody>
                  </S.Table>
                </Box>
              </S.Card.Content>
            </S.Card>
          </Wrapper>
        )}
      </Box>
      {isChangingPassword && (
        <ChangePasswordModal
          onClose={() => setIsChangingPassword(false)}
          onSuccess={() => {
            setIsChangingPassword(false)
            showSuccessMessage('Password Changed!')
          }}
        />
      )}
    </S.Container>
  )
}
