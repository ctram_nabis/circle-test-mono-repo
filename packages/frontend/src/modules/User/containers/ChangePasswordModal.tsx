import { Box, Button, ErrorBox, Form, P, S } from 'components'
import { useChangeUserPasswordMutation } from 'generated/graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import logger from 'modules/Logger'
import React, { useState } from 'react'
import { ErrorType } from 'types'
interface Props {
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: () => void
  open?: boolean
  trigger?: JSX.Element
}
export const ChangePasswordModal = (props: Props) => {
  const { onClose, onFailure, onSuccess, onOpen, open = true, trigger } = props
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()

  const [changePassword] = useChangeUserPasswordMutation()

  const validationSchema = createValidationSchema({
    currentPassword: validators.password,
    newPassword: validators.newPassword,
    confirmPassword: validators.passwordConfirmation,
  })

  const { handleChange, handleBlur, values, errors, isSubmitting, handleSubmit } = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      currentPassword: '',
      newPassword: '',
      confirmPassword: '',
    },
    onSubmit: async (inputs) => {
      try {
        await changePassword({
          variables: {
            password: inputs.currentPassword,
            newPassword: inputs.newPassword,
            confirmPassword: inputs.confirmPassword,
          },
        })
        return onSuccess && onSuccess()
      } catch (err) {
        logger.error(err)
        const errorsToSet = err?.graphQLErrors
        if (Array.isArray(errorsToSet)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors.message)
        }
        if (onFailure) {
          onFailure(err?.graphQLErrors)
        }
      }
    },
  })

  const changePasswordFields: JSX.Element[][] = [
    [
      createFormField({
        control: 'input',
        type: 'password',
        size: 'large',
        label: 'Current Password *',
        labelStyles: { bg: 'transparent' },
        placeholder: '',
        name: 'currentPassword',
        values,
        errors,
        onChange: handleChange,
        onBlur: handleBlur,
        required: true,
        key: 'currentPassword',
      }),
    ],
    [
      <Box width="100%" key="divider">
        <S.Divider fitted />
      </Box>,
    ],
    [
      createFormField({
        control: 'input',
        type: 'password',
        size: 'large',
        label: 'New Password *',
        labelStyles: { bg: 'transparent' },
        placeholder: '',
        name: 'newPassword',
        values,
        errors,
        onChange: handleChange,
        onBlur: handleBlur,
        required: true,
        key: 'newPassword',
      }),
    ],
    [
      createFormField({
        control: 'input',
        type: 'password',
        size: 'large',
        label: 'Confirm Password *',
        labelStyles: { bg: 'transparent' },
        placeholder: '',
        name: 'confirmPassword',
        values,
        errors,
        onChange: handleChange,
        onBlur: handleBlur,
        required: true,
        key: 'confirmPassword',
      }),
    ],
  ]

  return (
    <S.Modal size="tiny" closeIcon open={open} onOpen={onOpen} onClose={onClose} trigger={trigger}>
      <S.Header icon="lock" content="Change Password" />
      <S.Modal.Content>
        <P>Confirm your old password to change to a new password.</P>
        <ErrorBox errors={requestErrors} error={catchError} />
        <Form onSubmit={handleSubmit} id="change-password">
          {changePasswordFields.map((field, i) => (
            <S.FormGroup key={`password-formGroup-${i}`} widths="equal">
              {field}
            </S.FormGroup>
          ))}
        </Form>
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button name="remove" onClick={onClose} title="Cancel" type="button" />
        <Button
          loading={isSubmitting}
          title="Change Password"
          color="green"
          type="submit"
          name="check"
          form="change-password"
        />
      </S.Modal.Actions>
    </S.Modal>
  )
}
