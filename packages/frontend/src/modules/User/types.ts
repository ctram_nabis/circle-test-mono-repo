import { User } from 'generated/graphql'

export type RoleName = 'admin' | 'manager' | 'user'

export type AuthUser = User
