import { ErrorBox, Pagination } from 'components'
import { S } from 'components/SemanticComponents'
import { Batch, BatchStatusEnum, Scalars, useInventoryBatchesTabQuery } from 'generated/graphql'
import { usePageInfo } from 'hooks'
import { FeedbackType } from 'modules/Inventory/containers/InventoryPage'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React, { useEffect, useState } from 'react'
import { Loader, Segment, Table } from 'semantic-ui-react'
import { BatchesHeaders } from '../components/BatchesHeaders'
import { BatchRow } from '../components/BatchRow'

interface Props {
  adminOverrideOrganizationId?: Scalars['ID']
  onFeedback: (f: FeedbackType) => void
}

export function AllBatchesTab({ adminOverrideOrganizationId, onFeedback }: Props) {
  const { organization } = useOrganization()
  const { isAdmin } = useMe()
  const organizationId = isAdmin ? adminOverrideOrganizationId : organization.id

  const [isArchived, setIsArchived] = useState<boolean>(false)
  const [search, setSearch] = useState<string>('')
  const [targetStatus, setTargetStatus] = useState<BatchStatusEnum>('PASSED')
  const [activeBatches, setActiveBatches] = useState<Record<string, Partial<Batch>>>({})

  const { pageInfo, setPage } = usePageInfo()

  const { data, loading, error, ...allInventoryBatches } = useInventoryBatchesTabQuery({
    variables: {
      isArchived,
      organizationId,
      pageInfo,
      search: search.trim(),
    },
  })

  useEffect(() => {
    allInventoryBatches.refetch()
  }, [allInventoryBatches, isArchived, pageInfo])

  const batches = data?.viewer?.allBatches?.results ?? []
  const totalNumPages = data?.viewer?.allBatches?.pageInfo?.totalNumPages ?? 0

  const deselectAll = () => {
    setActiveBatches({})
  }

  const selectBatch = (batchId: string) => {
    const isSelected = activeBatches[batchId]
    const batch = batches.find((_b) => _b.id === batchId)

    if (isSelected) {
      delete activeBatches[batchId]
    } else {
      activeBatches[batchId] = batch as Partial<Batch>
    }

    setActiveBatches(activeBatches)
  }
  const COL_SPAN_LENGTH = '9'
  return (
    <>
      {batches && batches.length > 0 && (
        <Pagination
          onPageChange={(_e, d) => setPage(d.activePage)}
          activePage={pageInfo.page}
          totalNumPages={totalNumPages}
          siblingRange={3}
        />
      )}

      <Table attached celled selectable structured>
        <BatchesHeaders
          adminOverrideOrganizationId={adminOverrideOrganizationId}
          activeBatches={activeBatches}
          onFeedback={onFeedback}
          targetStatus={targetStatus}
          search={search}
          onChangeSearch={setSearch}
          isArchived={isArchived}
          onChangeIsArchived={setIsArchived}
          onChangeBatchStatus={setTargetStatus}
          onDeselectAll={deselectAll}
        />
        <Table.Body>
          {loading ? (
            <Table.Row>
              <Table.Cell colSpan={COL_SPAN_LENGTH}>
                <Segment placeholder className="flex-style">
                  <S.Header>
                    <Loader active inline="centered" size="large">
                      Loading...
                    </Loader>
                  </S.Header>
                </Segment>
              </Table.Cell>
            </Table.Row>
          ) : error ? (
            <Table.Row>
              <Table.Cell colSpan={COL_SPAN_LENGTH}>
                <Segment placeholder className="flex-style">
                  <S.Header>
                    <ErrorBox errors={[error]} />
                  </S.Header>
                </Segment>
              </Table.Cell>
            </Table.Row>
          ) : batches && batches.length > 0 ? (
            batches.map((batch, i) => (
              <BatchRow
                adminOverrideOrganizationId={adminOverrideOrganizationId}
                key={batch.id || `batch-row-${i}`}
                isArchived={isArchived}
                batch={batch as Batch}
                onFeedback={onFeedback}
                onSelectBatch={selectBatch}
              />
            ))
          ) : (
            <Table.Cell colSpan={COL_SPAN_LENGTH}>
              <Segment placeholder className="flex-style">
                <S.Header>No data available</S.Header>
              </Segment>
            </Table.Cell>
          )}
        </Table.Body>
      </Table>
      {batches && batches.length > 0 && (
        <Pagination
          onPageChange={(_e, d) => setPage(d.activePage)}
          activePage={pageInfo.page}
          totalNumPages={totalNumPages}
          siblingRange={3}
        />
      )}
    </>
  )
}
