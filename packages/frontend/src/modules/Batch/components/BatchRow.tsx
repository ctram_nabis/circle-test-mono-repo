import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { S } from 'components/SemanticComponents'
import {
  Batch,
  Scalars,
  SkuBatch,
  useDeleteBatchMutation,
  useRestoreBatchMutation,
  useUpdateSkuBatchMutation,
} from 'generated/graphql'
import { FeedbackType } from 'modules/Inventory/containers/InventoryPage'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import moment from 'moment'
import React, { Fragment, useState } from 'react'
import styled from 'styled-components'
import { EditBatchModalButton } from './EditBatchModalButton'

const TextButton = styled(Button)`
  &.ui.button.basic.green {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    margin-left: 5px;
    text-decoration: none;
  }
  white-space: nowrap;
`
interface Props {
  adminOverrideOrganizationId?: Scalars['ID']
  isArchived: boolean
  batch: Batch
  onFeedback: (f: FeedbackType) => void
  onSelectBatch: (batchId: string) => void
}

export function BatchRow({ isArchived, batch, onFeedback, adminOverrideOrganizationId }: Props) {
  const { user } = useMe()
  const { canCreate } = useOrganization()
  const { skuBatches = [] } = batch
  const [updateBatch] = useUpdateSkuBatchMutation()
  const [deleteBatch] = useDeleteBatchMutation()
  const [restoreBatch] = useRestoreBatchMutation()
  const refetchQueries = ['InventoryBatchesTab']

  const [isLoadingUnassign, setIsLoadingUnassign] = useState(false)
  const [isLoadingArchived, setIsLoadingArchived] = useState(false)

  const { id, license, code, expirationDate, manufacturingDate, manifestGDriveFileId, status } = batch

  const handleArchive = async () => {
    if (!window.confirm(`Are you sure you want to archive this  (${code})`)) return

    setIsLoadingArchived(true)

    const res = await deleteBatch({ variables: { id }, refetchQueries })

    onFeedback({
      error: Boolean(res.errors),
      success: !Boolean(res.errors),
      header: 'Archive Batch',
      content: res.errors ? res.errors[0].message : `${code} was archived!`,
    })

    setIsLoadingArchived(false)
  }

  const handleRestore = async () => {
    const res = await restoreBatch({ variables: { batchId: id }, refetchQueries })

    const hasError = Boolean(res.errors)

    onFeedback({
      error: hasError,
      success: !hasError,
      header: 'Archive Batch',
      content: res.errors ? res.errors[0].message : `${code} was un-archived!`,
    })
  }

  const handleUnassign = async (_skuBatch: SkuBatch) => {
    setIsLoadingUnassign(true)

    const { sku } = _skuBatch

    if (!sku) {
      onFeedback({
        success: false,
        error: true,
        header: 'Unassign Batch',
        content: 'Data error, product was not loaded',
      })
      return
    }

    const res = await updateBatch({
      variables: {
        batchId: null,
        skuId: sku.id,
        skuBatchId: _skuBatch.id,
      },
    })

    if (res.errors) {
      onFeedback({
        success: false,
        error: true,
        header: 'Unassign Batch',
        content: res && res.errors[0].message,
      })
    } else {
      onFeedback({
        error: false,
        success: true,
        header: 'Unassign Batch',
        content: `${batch && batch.code} was unassigned from ${sku && sku.code}!`,
      })
    }

    setIsLoadingUnassign(false)
  }

  const showActions = canCreate.inventory || user.isAdmin
  const disableButtons = !showActions

  const batchDescription =
    user.isAdmin && !adminOverrideOrganizationId ? `${code ?? 'ERROR'}, ${batch?.organization?.name ?? 'ERROR'}` : code

  return (
    <S.Table.Row>
      <S.Table.Cell>
        <Box>
          {/* {user.isAdmin && <S.Checkbox onClick={() => onSelectBatch(id)} checked={active} />} */}
          {batchDescription}
        </Box>
      </S.Table.Cell>
      <S.Table.Cell>
        {skuBatches &&
          skuBatches.map((_skuBatch) => {
            const { sku, id: skuBatchId } = _skuBatch || {}
            const { code: productCode = '', name = '' } = sku || {}
            return (
              sku && (
                <Box key={skuBatchId} display="flex" justifyContent="space-between">
                  {`${productCode} - ${name}`}
                  {canCreate.inventory && user?.isAdmin && (
                    <TextButton
                      onClick={() => _skuBatch && handleUnassign(_skuBatch)}
                      title="Unassign"
                      basic
                      loading={isLoadingUnassign}
                      disabled={isLoadingUnassign}
                      color="green"
                    />
                  )}
                </Box>
              )
            )
          })}
      </S.Table.Cell>
      <S.Table.Cell>{status || '---'}</S.Table.Cell>
      <S.Table.Cell>{license ? license.licenseNumber : '---'}</S.Table.Cell>
      <S.Table.Cell>
        {moment(manufacturingDate)
          .tz('America/Los_Angeles')
          .format('L') || '---'}
      </S.Table.Cell>
      <S.Table.Cell>
        {moment(expirationDate)
          .tz('America/Los_Angeles')
          .format('L') || '---'}
      </S.Table.Cell>
      <S.Table.Cell>
        <Box>
          {// TODO: replace <a />
          manifestGDriveFileId ? (
            <a
              rel="noopener noreferrer"
              target="_blank"
              href={`https://drive.google.com/open?id=${manifestGDriveFileId}`}
            >
              <S.Icon size="large" name="file pdf outline" />
            </a>
          ) : (
            <S.Popup
              content={<em>COA has not been uploaded, please contact ops@nabis.com</em>}
              trigger={<S.Icon name="warning" color="yellow" />}
            />
          )}
        </Box>
      </S.Table.Cell>
      {showActions && (
        <S.Table.Cell>
          <Box>
            {isArchived ? (
              <S.Button icon="undo" onClick={() => handleRestore()} />
            ) : (
              <Fragment>
                {user.isAdmin && <EditBatchModalButton batchId={id} />}
                <S.Button
                  icon={disableButtons ? 'lock' : 'archive'}
                  disabled={disableButtons || isLoadingArchived}
                  loading={isLoadingArchived}
                  onClick={() => handleArchive()}
                />
              </Fragment>
            )}
          </Box>
        </S.Table.Cell>
      )}
    </S.Table.Row>
  )
}
