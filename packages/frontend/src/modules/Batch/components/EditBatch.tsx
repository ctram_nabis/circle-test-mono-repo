import { Loading, S } from 'components'
import { Batch, License, Maybe, Organization, Scalars, useGetBatchQuery } from 'generated/graphql'
import { BatchForm } from 'modules/Batch/components/BatchForm'
import React, { Fragment } from 'react'

type BatchQueryType = Pick<
  Batch,
  'id' | 'code' | 'manifestGDriveFileId' | 'manufacturingDate' | 'expirationDate' | 'status' | 'testDate'
> & { organization: Maybe<Pick<Organization, 'id'>>; license: Maybe<Pick<License, 'id' | 'licenseNumber'>> }

interface Props {
  batchId: string
  setParentBatchId?: (id: Scalars['ID']) => void
  onSuccess?: () => void
}

export function EditBatch({ batchId, setParentBatchId, onSuccess }: Props) {
  const { data, loading, error } = useGetBatchQuery({ variables: { id: batchId } })
  const batch = data?.getBatch

  const getInitialFormValues = ({
    code,
    manifestGDriveFileId,
    manufacturingDate,
    expirationDate,
    license,
  }: BatchQueryType) => ({
    code: code ?? undefined,
    manifestGDriveFileId: manifestGDriveFileId ?? undefined,
    manufacturingDate: manufacturingDate ?? undefined,
    expirationDate: expirationDate ?? undefined,
    licenseId: license?.id ?? undefined,
  })

  return (
    <Fragment>
      <Loading loading={loading} />
      {error && <S.Message negative header="Oops... something went wrong" content={error?.message} />}
      {/* <ErrorBox error={error} /> */}
      {!loading && batch && (
        <BatchForm
          onSuccess={onSuccess}
          isAddForm={false}
          initialValues={getInitialFormValues(batch)}
          batchId={batch.id}
          setParentBatchId={setParentBatchId}
        />
      )}
    </Fragment>
  )
}
