import React from 'react'

import { Modal } from 'semantic-ui-react'
import { AddBatch } from './AddBatch'

interface Props {
  isOpen: boolean
  onOpen: () => void
  onClose: () => void
  onSuccess?: () => void
}

export function AddBatchModal({ isOpen, onOpen, onClose, onSuccess }: Props) {
  return (
    <Modal
      open={isOpen}
      closeIcon
      onClose={onClose}
      onOpen={onOpen}
      header="Add Batch"
      content={<AddBatch onSuccess={onSuccess} />}
    />
  )
}
