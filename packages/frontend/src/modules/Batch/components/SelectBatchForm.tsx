import { Button, ErrorBox, Label, S } from 'components'
import { Scalars, useAllBatchesQuery } from 'generated/graphql'
import { useAdminOverrideOrganization } from 'modules/Inventory/provider/useAdminOverrideOrganization'
import React, { Fragment, SyntheticEvent, useState } from 'react'
import { DropdownProps } from 'semantic-ui-react'
interface Props {
  setParentBatchId: (batchId: Scalars['ID']) => void
  setIsSelecting: (value: boolean) => void
}

export function SelectBatchForm({ setParentBatchId, setIsSelecting }: Props) {
  const { adminOverrideOrganizationId, organization } = useAdminOverrideOrganization()
  const [inputError, setInputError] = useState<string | undefined>(undefined)
  const [batchId, setBatchId] = useState<Scalars['ID'] | undefined>(undefined)

  const organizationId =
    adminOverrideOrganizationId !== 'All Organizations' ? adminOverrideOrganizationId : organization.id
  const { data, error, loading } = useAllBatchesQuery({ variables: { organizationId } })

  const batches = data?.viewer?.allBatches?.results ?? []
  const batchOptions = batches
    .sort((a, b) => (a?.code === b?.code ? 0 : a?.code && b?.code && a?.code > b?.code ? 1 : -1))
    .map((batch) => ({
      key: batch.id,
      text: `${batch.code}`,
      value: batch.id,
    }))

  return (
    <Fragment>
      <ErrorBox error={inputError || error} />
      <S.Grid padded>
        <S.GridRow columns={2}>
          <S.GridColumn>
            <Label fontSize={18} fontWeight="bold">
              Select an Existing Batch
            </Label>
          </S.GridColumn>
          <S.GridColumn textAlign="right">
            <Button name="plus" color="green" title="Create a new batch" onClick={() => setIsSelecting(false)} />
          </S.GridColumn>
        </S.GridRow>
        <S.GridRow columns={1}>
          <S.Dropdown
            fluid
            placeholder="Choose one..."
            selection
            loading={loading}
            disabled={loading}
            value={batchId}
            onChange={(_e: SyntheticEvent, _data: DropdownProps) => {
              const id = _data.value as string
              setBatchId(id)
            }}
            options={batchOptions}
          />
        </S.GridRow>
        <S.GridRow columns={1}>
          <Button
            title="Next"
            disabled={!batchId}
            onClick={() => {
              if (batchId) {
                setParentBatchId(batchId)
              }
              setInputError('Please select a batch...')
            }}
          />
        </S.GridRow>
      </S.Grid>
    </Fragment>
  )
}
