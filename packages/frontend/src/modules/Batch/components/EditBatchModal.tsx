import React from 'react'

import { Scalars } from 'generated/graphql'
import { Modal } from 'semantic-ui-react'
import { EditBatch } from './EditBatch'

interface Props {
  isOpen: boolean
  onOpen: () => void
  onClose: () => void
  onSuccess?: () => void
  batchId: Scalars['ID']
}

export function EditBatchModal({ batchId, isOpen, onOpen, onClose, onSuccess }: Props) {
  return (
    <Modal
      closeIcon
      open={isOpen}
      onClose={onClose}
      onOpen={onOpen}
      header="Edit Batch"
      content={<EditBatch batchId={batchId} onSuccess={onSuccess} />}
    />
  )
}
