import { Button } from 'components'
import React, { Fragment, useState } from 'react'
import { AddBatchModal } from './AddBatchModal'

export function AddBatchModalButton() {
  const [showModal, setShowModal] = useState<boolean>(false)

  const onClose = () => setShowModal(false)
  const onOpen = () => setShowModal(true)

  return (
    <Fragment>
      {showModal && <AddBatchModal isOpen={showModal} onClose={onClose} onOpen={onOpen} onSuccess={onClose} />}
      <Button onClick={() => setShowModal(true)} icon="plus" label="Batch" color="green" />
    </Fragment>
  )
}
