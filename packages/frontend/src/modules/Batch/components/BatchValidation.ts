import * as Yup from 'yup'

const REQUIRED_MESSAGE = ' * Required'

export const batchValidationSchema = {
  code: Yup.string()
    .trim()
    .required(REQUIRED_MESSAGE),
  manifestGDriveFileId: Yup.string(),
  manufacturingDate: Yup.date().required(REQUIRED_MESSAGE),
  expirationDate: Yup.date().required(REQUIRED_MESSAGE),
  licenseId: Yup.string()
    .trim()
    .required(REQUIRED_MESSAGE),
}

export type BatchFormInputType = Yup.InferType<typeof batchValidationSchema>
