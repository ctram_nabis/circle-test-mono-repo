import { S } from 'components'
import { Box } from 'components/core'
import { Batch, BatchStatusEnum, Scalars } from 'generated/graphql'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React from 'react'
import { AddBatchModalButton } from './AddBatchModalButton'

// const BATCH_STATUSES = [
//   {
//     key: 'passed',
//     text: 'passed',
//     value: 'PASSED',
//   },
//   {
//     key: 'failed',
//     text: 'failed',
//     value: 'FAILED',
//   },
//   {
//     key: 'sampled',
//     text: 'sampled',
//     value: 'SAMPLED',
//   },
//   {
//     key: 'quarantine',
//     text: 'quarantine',
//     value: 'QUARANTINE',
//   },
// ]

interface Props {
  adminOverrideOrganizationId?: Scalars['ID']
  isArchived: boolean
  search?: string
  onChangeSearch: (s: string) => void
  onChangeIsArchived: (b: boolean) => void
  onFeedback: (f: any) => void
  activeBatches: Record<string, Partial<Batch>>
  targetStatus: BatchStatusEnum
  onChangeBatchStatus: (v: BatchStatusEnum) => void
  onDeselectAll: () => void
}

export function BatchesHeaders({ isArchived, search, onChangeSearch, onChangeIsArchived }: Props) {
  const { user } = useMe()
  const { canCreate } = useOrganization()

  // TODO: add batch status updater
  // const [bulkUpdateBatchStatus] = useBulkUpdateBatchStatusMutation()

  const handleUpdateSearch = (_e: React.MouseEvent<HTMLElement, MouseEvent>, { value }: S.SearchProps) => {
    if (value) {
      onChangeSearch(value)
    } else {
      onChangeSearch('')
    }
  }

  // const handleBulkUpdate = async () => {
  //   const batches: UpdateBatchInput[] = Object.values(activeBatches).map(
  //     (batch) =>
  //       ({
  //         id: batch.id,
  //       } as UpdateBatchInput),
  //   )

  //   try {
  //     await bulkUpdateBatchStatus({
  //       variables: {
  //         batches,
  //         targetStatus,
  //       },
  //     })

  //     onFeedback({
  //       error: false,
  //       success: true,
  //       header: 'Updated Batches',
  //       content: `${batches.length} updated with status: ${targetStatus}`,
  //     })
  //   } catch (error) {
  //     onFeedback({
  //       error: true,
  //       success: false,
  //       header: 'Updated Batches',
  //       content: error,
  //     })
  //   }
  // }

  const showActions = canCreate.inventory || user.isAdmin

  return (
    <S.Table.Header>
      <S.Table.Row>
        <S.Table.HeaderCell width={3} colSpan="1">
          <S.Search
            value={search || ''}
            size="small"
            placeholder="Search..."
            open={false}
            onSearchChange={handleUpdateSearch}
          />
        </S.Table.HeaderCell>
        <S.Table.HeaderCell colSpan={1}>{canCreate.inventory && <AddBatchModalButton />}</S.Table.HeaderCell>
        <S.Table.HeaderCell colSpan="4">
          {/* {user.isAdmin && (
            <Box>
              <S.ButtonGroup color="pink">
                <S.Button
                  color="pink"
                  onClick={() => handleBulkUpdate()}
                  disabled={Object.values(activeBatches).length === 0}
                >
                  {`Update status to "${targetStatus.toLowerCase()}"`}
                </S.Button>
                <S.Dropdown
                  color="pink"
                  className="button icon"
                  floating
                  options={BATCH_STATUSES}
                  trigger={<Box />}
                  onChange={(_e, data) => {
                    const value = data.value as BatchStatusEnum
                    onChangeBatchStatus(value)
                  }}
                />
              </S.ButtonGroup>
              <S.Popup
                compact
                content="Deselect All"
                trigger={<S.Button onClick={onDeselectAll}>{Object.values(activeBatches).length}</S.Button>}
              />
            </Box>
          )} */}
        </S.Table.HeaderCell>
        <S.Table.HeaderCell colSpan={2}>
          <S.Checkbox
            toggle
            label="Archived"
            onChange={() => onChangeIsArchived(!isArchived)}
            defaultChecked={isArchived}
          />
        </S.Table.HeaderCell>
      </S.Table.Row>
      <S.Table.Row>
        <S.Table.HeaderCell>
          <Box>Code</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={250}>Linked Products</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={90}>Status</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={120}>License</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={75}>MFG</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={75}>Expires</Box>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>
          <Box width={30}>COA</Box>
        </S.Table.HeaderCell>
        {showActions && (
          <S.Table.HeaderCell>
            <Box width={90}>
              <S.Icon name="settings" />
            </Box>
          </S.Table.HeaderCell>
        )}
      </S.Table.Row>
    </S.Table.Header>
  )
}
