import React, { Fragment, useState } from 'react'

import { ErrorBox, H4, Loading, S } from 'components'
import { Text } from 'components/core/src/Text'
import { Scalars, useGetBatchQuery } from 'generated/graphql'
import { TitleholderDetails } from 'modules/SkuBatch/components'

interface Props {
  batchId: Scalars['ID']
  titleholderId?: string
  setTitleholderId: any
  organization?: any
}

export function BatchDetails({ batchId, titleholderId, setTitleholderId, organization }: Props) {
  const [batch, setBatch] = useState<any>({})
  const { loading, error } = useGetBatchQuery({
    variables: { id: batchId },
    onCompleted: (data) => {
      const _batch = data?.getBatch

      setBatch(_batch)
    },
  })

  return (
    <Fragment>
      <Loading loading={loading} />
      <ErrorBox error={error} />
      {!loading && !error && (
        <S.Grid padded columns={3}>
          <S.Grid.Row>
            <S.Grid.Column>
              <Text fontWeight="bold">Code: {batch?.code}</Text>
            </S.Grid.Column>
            <S.Grid.Column>
              Certificate of Analysis
              {batch && batch.manifestGDriveFileId ? (
                <a
                  rel="noopener noreferrer"
                  target="_blank"
                  href={`https://drive.google.com/open?id=${batch.manifestGDriveFileId}`}
                >
                  <S.Icon size="large" name="file pdf outline" />
                </a>
              ) : (
                <S.Popup
                  header="Missing COA"
                  content="Cannabis products require a COA for delivery to a retailer"
                  trigger={<S.Icon name="warning" color="yellow" />}
                />
              )}
            </S.Grid.Column>
          </S.Grid.Row>
          <S.Grid.Row>
            <S.Grid.Column>Manufactured: {batch?.manufacturingDate}</S.Grid.Column>
            <S.Grid.Column>Expires: {batch?.expirationDate}</S.Grid.Column>
          </S.Grid.Row>
          <S.Grid.Row>
            <S.Grid.Column>
              <H4>License:</H4>
            </S.Grid.Column>
          </S.Grid.Row>
          <S.Grid.Row>
            <S.Grid.Column>License Number: {batch?.license?.licenseNumber}</S.Grid.Column>
            <S.Grid.Column>Entity: {batch?.license?.legalEntityName}</S.Grid.Column>
            <S.Grid.Column>DBA: {batch?.license?.nickname}</S.Grid.Column>
          </S.Grid.Row>
          <S.Grid.Row>
            <S.Grid.Column>Issued: {batch?.license?.issuanceDate}</S.Grid.Column>
            <S.Grid.Column>Expires: {batch?.license?.expirationDate}</S.Grid.Column>
          </S.Grid.Row>
          <S.Grid.Row>
            <TitleholderDetails
              titleholderId={titleholderId}
              setTitleholderId={setTitleholderId}
              organization={organization}
            />
          </S.Grid.Row>
        </S.Grid>
      )}
    </Fragment>
  )
}
