import { Scalars } from 'generated/graphql'
import React from 'react'
import { BatchForm } from './BatchForm'

interface Props {
  setParentBatchId?: (id: Scalars['ID']) => void
  onSuccess?: () => void
}

export function AddBatch({ setParentBatchId, onSuccess }: Props) {
  return <BatchForm onSuccess={onSuccess} isAddForm={true} setParentBatchId={setParentBatchId} />
}
