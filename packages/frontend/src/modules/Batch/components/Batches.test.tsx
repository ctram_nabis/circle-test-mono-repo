import { MockedProvider } from '@apollo/react-testing'
import { mount } from 'enzyme'
import React from 'react'
import { AllBatchesTab } from '../containers/AllBatchesTab'

describe('Batches container', () => {
  describe('AllBatchesTab', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <AllBatchesTab />
        </MockedProvider>,
      )
    })
  })
})
