import React, { useState } from 'react'

import { Button, Loading, S } from 'components'
import {
  CreateBatchInput,
  Scalars,
  useAllLicensesQuery,
  useCreateBatchMutation,
  useEditBatchMutation,
  useUploadBatchFileMutation,
} from 'generated/graphql'
import {
  createFormDatePicker,
  createFormDropdown,
  createFormField,
  createValidationSchema,
  useForm,
  validators,
} from 'modules/Forms'
import { useAdminOverrideOrganization } from 'modules/Inventory/provider/useAdminOverrideOrganization'
import logger from 'modules/Logger'
import moment from 'moment'
import { Box } from 'theme-ui'
// import { ErrorType } from 'types'

const EMPTY_FORM_VALS = {
  code: '',
  manifestGDriveFileId: '',
  manufacturingDate: moment(),
  expirationDate: moment().add(1, 'year'),
  licenseId: '',
}

const INLINE_REQUIRED_MSG = ' * Required'

const validationSchema = createValidationSchema<Omit<CreateBatchInput, 'organizationId'>>({
  code: validators.string.required(INLINE_REQUIRED_MSG),
  manifestGDriveFileId: validators.string,
  manufacturingDate: validators.date.required(INLINE_REQUIRED_MSG),
  expirationDate: validators.date.required(INLINE_REQUIRED_MSG),
  licenseId: validators.string,
})

interface InitialValues {
  code?: string
  manifestGDriveFileId?: string
  manufacturingDate?: Scalars['DateTime']
  expirationDate?: Scalars['DateTime']
  licenseId?: string
}

interface Props {
  onSuccess?: () => void
  batchId?: string
  initialValues?: InitialValues
  isAddForm: boolean
  setParentBatchId?: (id: Scalars['ID']) => void
}

export function BatchForm({ batchId, initialValues, isAddForm, setParentBatchId, onSuccess }: Props) {
  const { adminOverrideOrganizationId, organization } = useAdminOverrideOrganization()

  const organizationId =
    adminOverrideOrganizationId !== 'All Organizations' ? adminOverrideOrganizationId : organization.id

  const [showFileUpload, setShowFileUpload] = useState<boolean>(!Boolean(initialValues?.manifestGDriveFileId))
  const [
    ,
    // catchError
    setCatchError,
  ] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<any[]>()

  const { data: licensesData, loading: loadingLicenses } = useAllLicensesQuery({
    variables: { organizationId: organization.id },
  })

  const licenses = licensesData?.viewer?.allLicenses?.organizationLicenses ?? []
  const licenseOptions = licenses.map((license) => ({
    key: license.id,
    text: license.licenseNumber,
    value: license.id,
  }))

  const [uploadFile, { loading: uploadingFile, error: uploadFileError }] = useUploadBatchFileMutation()
  const [updateMutation, { loading: updateLoading, error: updateError }] = useEditBatchMutation()
  const [createMutation, { loading: createLoading, error: createError }] = useCreateBatchMutation()

  const [
    loading,
    // error
  ] = [isAddForm ? createLoading : updateLoading, isAddForm ? createError : updateError]

  const { errors, handleBlur, handleChange, handleSubmit, values, isSubmitting, ...form } = useForm<
    typeof validationSchema
  >({
    validationSchema,
    initialValues: { ...EMPTY_FORM_VALS, ...initialValues },
    onSubmit: async (inputs) => {
      try {
        // don't submit while uploading
        if (uploadingFile) {
          return
        }

        const refetchQueries = ['InventoryBatchesTab', 'InventorySkuBatchesTab']

        if (isAddForm) {
          const payload = {
            ...inputs,
            organizationId,
          }

          // Delete empty string values in payload because GQL no likey
          Object.keys(payload).forEach((key) => {
            if (payload[key] === '') {
              delete payload[key]
            }
          })

          const createData = await createMutation({
            variables: { input: payload },
            refetchQueries,
          })

          if (createData.errors) {
            setRequestErrors(createData.errors)
            return
          }

          if (setParentBatchId) {
            const newId = createData?.data?.createBatch?.id
            if (newId) {
              setParentBatchId(newId)
            } else {
              throw new Error('missing batch id')
            }
          }
          if (onSuccess) {
            onSuccess()
          }
        } else {
          if (!batchId) {
            throw new Error('Failed to submit update, no skuId was provided')
          }
          const payload = {
            ...inputs,
            id: batchId,
          }

          await updateMutation({ variables: { input: payload }, refetchQueries })

          if (setParentBatchId) {
            setParentBatchId(batchId)
          }
          if (onSuccess) {
            onSuccess()
          }
        }
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) return setRequestErrors(err?.graphQLErrors)
        return setCatchError(err?.graphQLErrors?.message)
      }
    },
  })

  const fields: JSX.Element[] = [
    createFormField({
      name: 'code',
      type: 'text',
      label: 'Code',
      placeholder: 'Batch #',
      value: values.code,
      error: Boolean(errors.code),
      onChange: handleChange,
      onBlur: handleBlur,
      required: true,
    }),
    createFormDatePicker({
      errors,
      label: 'Manufacturing Date',
      name: 'manufacturingDate',
      onBlur: handleBlur,
      onChange: (date) => form.setFieldValue('manufacturingDate', date),
      required: true,
      selected: moment(values.manufacturingDate).toDate(),
      values,
    }),
    createFormDatePicker({
      errors,
      label: 'Expiration Date',
      name: 'expirationDate',
      onBlur: handleBlur,
      onChange: (date) => form.setFieldValue('expirationDate', date),
      required: true,
      selected: moment(values.expirationDate).toDate(),
      values,
    }),
    createFormDropdown({
      errors,
      label: 'Batch License',
      loading: loadingLicenses,
      name: 'licenseId',
      onBlur: handleBlur,
      onChange: (_e, { value }) => form.setFieldValue('licenseId', value),
      options: licenseOptions,
      placeholder: 'Select license...',
      selection: true,
      type: 'text',
      value: values.licenseId as string,
    }),
  ]

  const uploadField = createFormField({
    name: 'manifestGDriveFileId',
    type: 'file',
    label: 'Certificate of Analysis',
    error: Boolean(errors.manifestGDriveFileId) || Boolean(uploadFileError),
    // bug with e.target.files: https://github.com/microsoft/TypeScript/issues/31816
    onChange: async (e: any, data: any) => {
      const file = e.target.files[0]

      if (file) {
        const uploadFileData = await uploadFile({
          variables: {
            inputFile: file,
            id: organizationId,
          },
        })
        const fileId = uploadFileData?.data?.uploadBatchFile
        if (fileId) {
          form.setFieldValue('manifestGDriveFileId', fileId)
        } else {
          throw new Error('failed to upload COA')
        }
      }
    },
    onBlur: handleBlur,
    required: false,
    loading: uploadingFile,
    disabled: uploadingFile,
  })

  return (
    <Box>
      <Loading loading={loading} />
      {!loading &&
        requestErrors &&
        requestErrors.map((err) => <S.Message key={err.message} error visible content={err.message} />)}
      <S.Grid padded>
        <S.Grid.Row>
          <S.Grid.Column>
            <S.Form>
              <S.Form.Group widths="equal">{fields}</S.Form.Group>
              <S.Segment>
                {showFileUpload ? (
                  uploadField
                ) : (
                  <Box>
                    {initialValues && initialValues?.manifestGDriveFileId && (
                      <a
                        target="_blank"
                        rel="noopener noreferrer"
                        href={`https://drive.google.com/open?id=${initialValues.manifestGDriveFileId}`}
                      >
                        <S.Icon size="large" name="file pdf outline" />
                      </a>
                    )}
                    <S.Button
                      key="reveal-upload"
                      icon="pencil"
                      label="Replace COA"
                      onClick={() => setShowFileUpload(true)}
                    />
                  </Box>
                )}
              </S.Segment>
              <Button
                color="green"
                name="check"
                loading={loading}
                disabled={loading}
                title={isAddForm ? 'Create Batch' : 'Update Batch'}
                onClick={() => handleSubmit()}
              />
            </S.Form>
          </S.Grid.Column>
        </S.Grid.Row>
      </S.Grid>
    </Box>
  )
}
