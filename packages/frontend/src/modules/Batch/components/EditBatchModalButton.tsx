import { S } from 'components/SemanticComponents'
import { Scalars } from 'generated/graphql'
import React, { Fragment, useState } from 'react'
import { EditBatchModal } from './EditBatchModal'

interface Props {
  batchId: Scalars['ID']
}

export function EditBatchModalButton({ batchId }: Props) {
  const [showModal, setShowModal] = useState<boolean>(false)

  const onClose = () => setShowModal(false)
  const onOpen = () => setShowModal(true)

  return (
    <Fragment>
      {showModal && (
        <EditBatchModal isOpen={showModal} batchId={batchId} onClose={onClose} onSuccess={onClose} onOpen={onOpen} />
      )}
      <S.Button onClick={() => setShowModal(true)} icon="pencil" color="yellow" />
    </Fragment>
  )
}
