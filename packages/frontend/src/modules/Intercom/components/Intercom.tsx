import config from 'config'
import { IS_PRODUCTION } from 'modules/App'
import { useUserContext } from 'modules/User'
import React from 'react'
import IntercomComponent from 'react-intercom'

export const Intercom = () => {
  const { user } = useUserContext()

  if (!IS_PRODUCTION || !user) {
    return null
  }

  const { id: userId, email, phone, isAdmin, firstName, lastName } = user || {}
  const displayName = `${firstName} ${lastName}`

  const userPayload = {
    user_id: userId,
    email: email || '',
    phone: phone || '',
    isAdmin: isAdmin || false,
  }

  if (window.FS) {
    window.FS.identify(user.id)
    window.FS.setUserVars({ displayName, ...userPayload })
  }

  return <IntercomComponent appID={config.intercom.appId} {...{ name: displayName, ...userPayload }} />
}
