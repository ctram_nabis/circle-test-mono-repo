import { Container } from 'components/Container'
import { Box } from 'components/core/src/Box'
import { ErrorBox } from 'components/ErrorBox'
import { PageHeader } from 'components/PageHeader'
import { S } from 'components/SemanticComponents'
import config from 'config'
import { Organization, useAccessibleBrandsQuery } from 'generated/graphql'
import { useOrganization } from 'modules/Organization'
import React, { useState } from 'react'
import styled from 'styled-components'
import { ErrorType } from 'types'
import { ReportsPageDropdown } from './ReportsPageDropdown'
import { Report, ReportsTable } from './ReportsTable'

// TODO: REPLACE WITH COMPONENT
const Wrapper = styled(Box)`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  width: 100%;
  margin-bottom: 12px;
`

export const ReportsPage = () => {
  const { organization } = useOrganization()
  const [selectedBrand, setSelectedBrand] = useState<Partial<Organization> | undefined>(undefined)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [catchError, setCatchError] = useState<string | undefined>(undefined)
  const [requestErrors, setRequestErrors] = useState<ErrorType[] | undefined>(undefined)
  const [reportRows, setReportRows] = useState<Report[] | undefined>(undefined)

  const { data, error, loading } = useAccessibleBrandsQuery({
    variables: {
      orgId: organization.id,
    },
  })

  const accessibleBrands = data?.viewer?.accessibleBrands

  const getReport = async (id: string) => {
    try {
      const graphQLConfigure = config.apollo.GRAPHQL_URL || ''
      const brandReportData = graphQLConfigure.replace('/graphql/admin', `/report/${id}`)

      const headers = new Headers()
      const token = `Basic ${Buffer.from(config.app.apiKey + ':').toString('base64')}`
      headers.set('Authorization', token)
      const response = await fetch(brandReportData, { headers })
      const { rows } = await response.json()

      setReportRows(rows)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
      setCatchError(error)
    }
  }

  const selectBrandReport = (brand: Partial<Organization> | null) => {
    if (!brand) {
      setIsLoading(false)
      setRequestErrors(['Unable to find report'])
      return
    }

    setReportRows([])
    setSelectedBrand(brand)
    setIsLoading(true)
    getReport(brand?.id || '')
  }
  const isData = (reportRows || []).length > 0
  return (
    <Container>
      <Wrapper>
        <S.Loader active={loading} inline="centered" size="large" content="Loading Reports..." />
        <ErrorBox error={error} />
        {!loading && !error && (
          <S.Container>
            <S.Grid>
              <S.GridRow>
                <S.GridColumn>
                  <PageHeader>Reports (Beta)</PageHeader>
                </S.GridColumn>
              </S.GridRow>
              <S.GridRow>
                <S.GridColumn>
                  <ReportsPageDropdown
                    brands={accessibleBrands}
                    onChange={selectBrandReport}
                    selectedBrand={selectedBrand}
                    loading={loading || isLoading}
                    isData={isData}
                  />
                </S.GridColumn>
              </S.GridRow>
              <S.GridRow>
                <S.GridColumn>
                  {!loading && (
                    <>
                      <ErrorBox errors={requestErrors} error={catchError} />
                      <ReportsTable
                        rows={reportRows}
                        isData={isData}
                        loading={isLoading}
                        selectedBrand={selectedBrand}
                      />
                    </>
                  )}
                </S.GridColumn>
              </S.GridRow>
            </S.Grid>
          </S.Container>
        )}
      </Wrapper>
    </Container>
  )
}
