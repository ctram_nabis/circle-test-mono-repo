import { MockedProvider } from '@apollo/react-testing'
import { shallow } from 'enzyme'
import React from 'react'
import { ReportsPage } from './ReportsPage'
import { ReportsPageDropdown } from './ReportsPageDropdown'
import { ReportsTable } from './ReportsTable'

describe('`Reports` container', () => {
  it('`ReportsPage` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <ReportsPage />
      </MockedProvider>,
    )
  })
  it('`ReportsPageDropdown` renders without crashing', () => {
    const onChange = jest.fn()
    shallow(
      <MockedProvider>
        <ReportsPageDropdown brands={[]} isLoading={false} selectedBrand={null} onChange={onChange} />
      </MockedProvider>,
    )
  })
  it('`ReportsTable` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <ReportsTable />
      </MockedProvider>,
    )
  })
})
