import { Button, Dropdown, S } from 'components'
import config from 'config'
import { Organization } from 'generated/graphql'
import fileDownload from 'js-file-download'
import React from 'react'
interface Props {
  brands?: Array<Partial<Organization>>
  loading: boolean
  onChange: (brand: Partial<Organization> | null) => void
  selectedBrand?: Partial<Organization> | undefined
  isData?: boolean
}

export const ReportsPageDropdown = ({ brands, loading, onChange, selectedBrand, isData }: Props) => {
  const options = (brands || []).map<S.DropdownItemProps>((brand) => ({
    value: brand.id,
    text: brand.name,
  }))

  const handleOnChange = (id: string) => {
    if (id) {
      const brandOptions = brands || []
      const selectedReport = brandOptions.find((_brand) => _brand.id === id) || null

      if (onChange) {
        onChange(selectedReport)
      }
    }
  }

  const handleCsvDownloadClick = async () => {
    const graphQLConfigure = config.apollo.GRAPHQL_URL || ''
    const id = (selectedBrand && selectedBrand.id) || ''
    const brandReportCsv = graphQLConfigure.replace('/graphql/admin', `/report-csv/${id}`)

    const headers = new Headers()
    const token = `Basic ${Buffer.from(config.app.apiKey + ':').toString('base64')}`
    headers.set('Authorization', token)

    const response = await fetch(brandReportCsv, { headers })
    const responseToText = await response.text()
    return fileDownload(responseToText, `${(selectedBrand && selectedBrand.name) || ''}.csv`)
  }

  return (
    <>
      <Dropdown
        mr={5}
        placeholder="Select a brand"
        search
        selection
        loading={loading}
        options={options}
        onChange={(_e, d) => handleOnChange(d.value as string)}
        value={selectedBrand?.id}
        text={selectedBrand?.name}
      />
      <Button
        onClick={handleCsvDownloadClick}
        disabled={!selectedBrand?.id || !isData}
        title="Download this table as a CSV"
      />
    </>
  )
}
