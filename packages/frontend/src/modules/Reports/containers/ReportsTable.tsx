import { Ribbon, S } from 'components'
import { Organization } from 'generated/graphql'
import React from 'react'

interface Props {
  rows?: Report[]
  loading?: boolean
  isData?: boolean
  selectedBrand?: Partial<Organization> | undefined
}
export interface Report {
  retailer: string
  seller: string
  creator: string
  order: number
  status: string
  paymentStatus: string
  action: string
  date: string
  strain: string
  batchNumber: number
  units: number
  pricePerUnit: string
}
export const ReportsTable = ({ loading, rows, isData, selectedBrand }: Props) => (
  <S.Table celled padded selectable unstackable verticalAlign="middle" style={{ minWidth: 'max-content' }}>
    <S.Table.Header>
      <S.Table.Row>
        <S.Table.HeaderCell>
          <Ribbon color="green">Customer</Ribbon>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>Seller</S.Table.HeaderCell>
        <S.Table.HeaderCell>Booked by</S.Table.HeaderCell>
        <S.Table.HeaderCell>Order</S.Table.HeaderCell>
        <S.Table.HeaderCell>Status</S.Table.HeaderCell>
        <S.Table.HeaderCell>Payment Status</S.Table.HeaderCell>
        <S.Table.HeaderCell>Action</S.Table.HeaderCell>
        <S.Table.HeaderCell>Date</S.Table.HeaderCell>
        <S.Table.HeaderCell>Strain</S.Table.HeaderCell>
        <S.Table.HeaderCell>Batch Number</S.Table.HeaderCell>
        <S.Table.HeaderCell>Units</S.Table.HeaderCell>
        <S.Table.HeaderCell>Price per unit</S.Table.HeaderCell>
      </S.Table.Row>
    </S.Table.Header>
    <S.Table.Body>
      {loading && (
        <S.Table.Row>
          <S.Table.Cell>
            <S.Dimmer active={loading} inverted>
              <S.Loader active={loading} />
            </S.Dimmer>
          </S.Table.Cell>
        </S.Table.Row>
      )}

      {isData ? (
        (rows || []).map((row: Report, i) => (
          <S.Table.Row key={`reportsTableRow-${i + 1}`}>
            <S.Table.Cell>{row.retailer}</S.Table.Cell>
            <S.Table.Cell>{row.seller}</S.Table.Cell>
            <S.Table.Cell>{row.creator}</S.Table.Cell>
            <S.Table.Cell>{row.order}</S.Table.Cell>
            <S.Table.Cell>{row.status}</S.Table.Cell>
            <S.Table.Cell>{row.paymentStatus}</S.Table.Cell>
            <S.Table.Cell>{row.action}</S.Table.Cell>
            <S.Table.Cell>{row.date}</S.Table.Cell>
            <S.Table.Cell>{row.strain}</S.Table.Cell>
            <S.Table.Cell>{row.batchNumber}</S.Table.Cell>
            <S.Table.Cell>{row.units}</S.Table.Cell>
            <S.Table.Cell>{row.pricePerUnit}</S.Table.Cell>
          </S.Table.Row>
        ))
      ) : (
        <S.Table.Row>
          <S.Table.Cell colSpan={12}>
            {selectedBrand
              ? 'No data! If this looks like a mistake, contact engineering@getnabis.com.'
              : 'Please select a brand in the dropdown in the upper left corner.'}
          </S.Table.Cell>
        </S.Table.Row>
      )}
    </S.Table.Body>
  </S.Table>
)
