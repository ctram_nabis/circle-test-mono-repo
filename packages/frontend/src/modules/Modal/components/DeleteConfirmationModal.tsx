import { Button, ErrorBox } from 'components'
import { logger } from 'modules/Logger'
import React, { useEffect, useState } from 'react'
import { useToggle } from 'react-use'
import { Header, Modal } from 'semantic-ui-react'
import { ErrorType } from 'types'

interface Props {
  delete: () => any
  isArchiveText?: boolean
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: () => void
  open?: boolean
  trigger?: JSX.Element
}

export const DeleteConfirmationModal = (props: Props) => {
  const { onClose, onFailure, onOpen, isArchiveText = false, onSuccess, open = true, trigger } = props

  const [requestErrors, setRequestErrors] = useState()
  const [isOpen, toggleModal] = useToggle(open)

  useEffect(() => {
    return isOpen ? onOpen && onOpen() : onClose && onClose()
  }, [isOpen, onOpen, onClose])

  const confirmModal = async () => {
    try {
      const response = await props.delete()

      if (response && response.errors) {
        setRequestErrors(response.errors)
        if (onFailure) {
          onFailure(response.errors)
        }
      }

      // close the modal when successful deletion
      toggleModal(false)
      return onSuccess && onSuccess()
    } catch (error) {
      logger.error('Delete error: ', error)
      setRequestErrors([error])
      if (onFailure) {
        onFailure([error])
      }
    }
  }
  const deleteText = isArchiveText ? 'Archive' : 'Delete'
  return (
    <Modal
      closeIcon
      open={isOpen}
      onOpen={() => toggleModal(true)}
      onClose={() => toggleModal(false)}
      trigger={trigger}
    >
      <Header icon="x" content={`Confirm ${deleteText}`} />
      <Modal.Content>
        <ErrorBox errors={requestErrors} />
        Are you sure you wish to {deleteText.toLowerCase()} this item?
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => toggleModal(false)} name="undo" title="Cancel" />
        <Button color="red" onClick={confirmModal} name="x" title={deleteText} />
      </Modal.Actions>
    </Modal>
  )
}
