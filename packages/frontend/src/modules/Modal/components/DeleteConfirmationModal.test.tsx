import { shallow } from 'enzyme'
import React from 'react'
import { DeleteConfirmationModal } from './DeleteConfirmationModal'

describe('DeleteConfirmationModal component', () => {
  it('should render without crashing', () => {
    shallow(<DeleteConfirmationModal />)
  })

  /**
   * TODO: simulate form input change, corresponding state change
   */

  /**
   * TODO: simulate button click handler, make sure jest mock fn is called
   */
})
