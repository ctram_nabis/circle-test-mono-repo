import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { P } from 'components/P'
import { Pagination } from 'components/Pagination'
import { Ribbon } from 'components/Ribbon'
import { S } from 'components/SemanticComponents'
import { AllDraftsDocument, DeleteDraftDocument } from 'generated/graphql'
import idx from 'idx'
import cloneDeep from 'lodash/cloneDeep'
import debounce from 'lodash/debounce'
import { DeleteConfirmationModal } from 'modules/Modal'
import PageHeader from 'modules/nabis-ui/PageHeader'
import { withOrganizationHook } from 'modules/Order/helpers/withOrganizationHook'
import moment from 'moment-timezone'
import queryString from 'query-string'
import React from 'react'
import { graphql, withApollo } from 'react-apollo'
import { withRouter } from 'react-router'
import { compose } from 'recompose'
import { GridColumn } from 'semantic-ui-react'
import styled from 'styled-components'
import { oc } from 'ts-optchain'
import { isEmpty, isOrderValid } from 'utils'
import { useGetVarsFromURL } from '../../../utils/helpers/getVarsFromUrl'

const NoDraftsContainer = styled(Box)`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 100%;
`

const NoDraftsText = styled(Box)`
  margin: 20px 0;
`

const DraftNameContainer = styled(Box)`
  width: 100%;
`

const DraftName = styled(Box)`
  float: left;
`

const DraftToolTip = styled(Box)`
  float: right;
`

const OrderGridColumn = styled(GridColumn)`
  display: flex !important;
  justify-content: space-between !important;
  align-items: center !important;
`
class DraftsPage extends React.Component<any, any> {
  debounceRefetchDrafts: (page: number) => any

  constructor(props) {
    super(props)

    this.state = {
      loadingDrafts: false,
      refetchingDrafts: false,
      filters: {
        // Pagination Info
        numItemsPerPage: 25,
        orderBy: [
          {
            attribute: 'createdAt',
            order: 'DESC',
          },
        ],
        page: 1,
        totalNumItems: 0,
        totalNumPages: 0,
      },
    }
    this.debounceRefetchDrafts = (value) => debounce(this.refetchDrafts(value), 300)
  }

  areThereParams = () => {
    const { location } = this.props
    const qs = queryString.parse(location?.search)

    return Object.keys(qs).length === 0
  }

  componentDidMount() {
    this.setState({ loadingDrafts: true })
  }

  componentDidUpdate(_prevProps, prevState) {
    if (!this.props.data.loading) {
      const canCreate = this.props?.canCreate?.order
      if (prevState.canCreate !== canCreate) {
        this.setState({ canCreate })
      }

      if (prevState?.loadingDrafts) {
        this.setState({ loadingDrafts: false })
      }

      if (prevState?.refetchingDrafts) {
        this.setState({ refetchingDrafts: false })
      }
    }
  }

  handleFilterChange(filter, value) {
    const newFilters = cloneDeep(this.state.filters)
    newFilters[filter] = value

    this.debounceRefetchDrafts(value)()
  }

  refetchDrafts(page) {
    return () => {
      const {
        match: {
          params: { alias },
        },
        data,
        history,
      } = this.props

      const cleanQueryString = (() => {
        const qs: any = {}
        if (page) {
          qs.page = page
        }
        return qs
      })()

      const newQueryString = queryString.stringify(cleanQueryString)
      history.push(`/${alias}/app/drafts${newQueryString ? '?' : ''}${newQueryString}`)
      this.setState({ refetchingDrafts: true })

      data.refetch()
    }
  }

  getPageNumbers(currentPage, totalPageNumbers) {
    const newPageNumberRange: number[] = []

    const minNumTemp = Math.floor(currentPage / 10) * 10
    const minNum = Math.max(minNumTemp, 1)
    const maxNum = Math.min(minNumTemp + 10, totalPageNumbers)

    for (let i = minNum; i <= maxNum; i++) {
      newPageNumberRange.push(i)
    }

    return newPageNumberRange
  }

  goToOrder(draft) {
    const { history, organization } = this.props
    history.push({
      pathname: `/${organization.alias}/app/orders/new`,
      state: {
        draftId: draft.id,
        editDraft: true,
      },
    })
  }

  copyOrder(draft) {
    const { history, organization } = this.props
    history.push({
      pathname: `/${organization.alias}/app/orders/new`,
      state: {
        draftId: draft.id,
        editDraft: false,
      },
    })
  }

  goToNewOrderPage() {
    const { history, organization } = this.props
    history.push({
      pathname: `/${organization.alias}/app/orders/new`,
    })
  }

  handlePageChange = (e: Event, data: any) => {
    this.handleFilterChange('page', data.activePage)
  }

  renderActionButtons(d) {
    return (
      <Box>
        <S.Button color="yellow" size="tiny" icon="edit" onClick={() => this.goToOrder(d)} label="Edit" />
        <S.Button color="orange" size="tiny" icon="copy" onClick={() => this.copyOrder(d)} label="Copy" />
        <DeleteConfirmationModal
          open={false}
          delete={() => this.props.deleteDraft({ id: d.id })}
          trigger={<S.Button color="red" size="tiny" icon="delete" label="Delete" />}
        />
      </Box>
    )
  }

  renderHeader(paginate) {
    return (
      <S.Table.Header>
        {paginate && this.renderPaginationComponent()}
        <S.Table.Row>
          <S.Table.HeaderCell>
            <Ribbon color="green">Draft Name</Ribbon>
          </S.Table.HeaderCell>
          <S.Table.HeaderCell>Last Updated</S.Table.HeaderCell>
          <S.Table.HeaderCell>Created At</S.Table.HeaderCell>
          {this.state.canCreate && <S.Table.HeaderCell>Actions</S.Table.HeaderCell>}
        </S.Table.Row>
      </S.Table.Header>
    )
  }

  renderDraftTooltip(draft) {
    const errors = isOrderValid({ order: draft.body }, draft.body)
    return (
      <S.Popup
        trigger={
          <S.Icon
            color={errors.length > 0 ? 'red' : 'green'}
            name={errors.length > 0 ? 'exclamation circle' : 'check circle'}
          />
        }
      >
        {errors.length > 0 ? (
          <S.Popup.Content>
            <Text fontWeight="bold">Draft has the following errors:</Text>
            {errors.map((e, i) => {
              return (
                <Box key={i}>
                  <em
                    style={{
                      color: 'red',
                      fontSize: '.85em',
                    }}
                  >
                    {e}
                  </em>
                </Box>
              )
            })}
          </S.Popup.Content>
        ) : (
          <S.Popup.Content>
            <Text fontWeight="bold">Ready to submit!</Text>
          </S.Popup.Content>
        )}
      </S.Popup>
    )
  }

  renderTableRow(d) {
    return (
      <S.Table.Row key={d.id}>
        <S.Table.Cell>
          <DraftNameContainer>
            <DraftName>{d.body.name || '--------'}</DraftName>
            <DraftToolTip>{this.renderDraftTooltip(d)}</DraftToolTip>
          </DraftNameContainer>
        </S.Table.Cell>
        <S.Table.Cell>{moment(d.updatedAt).format('lll')}</S.Table.Cell>
        <S.Table.Cell>{moment(d.createdAt).format('lll')}</S.Table.Cell>
        {this.state.canCreate ? <S.Table.Cell>{this.renderActionButtons(d)}</S.Table.Cell> : ''}
      </S.Table.Row>
    )
  }

  renderPaginationComponent() {
    const pageInfo = oc(this).props.data.viewer.allDrafts.pageInfo()
    const page = pageInfo && 'page' in pageInfo ? pageInfo.page : null
    const totalNumPages = pageInfo && 'totalNumPages' in pageInfo ? pageInfo.totalNumPages : null

    return (
      <S.Table.Row>
        <S.Table.HeaderCell colSpan={10}>
          <Pagination
            onPageChange={(e, data) => this.handlePageChange(e as Event, data as object)}
            activePage={page}
            totalNumPages={totalNumPages}
            siblingRange={3}
          />
        </S.Table.HeaderCell>
      </S.Table.Row>
    )
  }

  renderPanesComponents() {
    const allDrafts = oc(this).props.data.viewer.allDrafts.results([])
    const nullDrafts = oc(this).props.data.viewer.me.drafts([])

    const panes = [
      {
        menuItem: `${idx(this, (_) => _.props.organization.name)}`,
        render: () => (
          <S.Table celled selectable unstackable verticalAlign="middle" sortable color="green">
            {this.renderHeader(true)}
            <S.Table.Body>{this.loaderOrBody(allDrafts.map((d) => this.renderTableRow(d)))}</S.Table.Body>
            <S.Table.Footer>{this.renderPaginationComponent()}</S.Table.Footer>
          </S.Table>
        ),
      },
      {
        menuItem: 'Other',
        render: () => (
          <S.Table celled selectable unstackable verticalAlign="middle" sortable color="green">
            {this.renderHeader(false)}
            <S.Table.Body>{this.loaderOrBody(nullDrafts.map((d) => this.renderTableRow(d)))}</S.Table.Body>
            <S.Table.Footer />
          </S.Table>
        ),
      },
    ]

    return panes
  }

  loaderOrBody(Component) {
    const { refetchingDrafts } = this.state || {}

    if (refetchingDrafts) {
      return (
        <S.Table.Row>
          <S.Table.Cell>
            <S.Dimmer active={refetchingDrafts} inverted>
              <S.Loader active={refetchingDrafts} />
            </S.Dimmer>
          </S.Table.Cell>
        </S.Table.Row>
      )
    } else {
      return Component
    }
  }

  render() {
    const { data } = this.props

    const { loadingDrafts } = this.state

    const definitelyNoDrafts = isEmpty(data?.viewer?.allDrafts?.results) && this.areThereParams()
    const renderComponent = (
      <S.Grid columns={1} padded>
        <S.Loader active={loadingDrafts} inline="centered" size="large" content="Loading Drafts..." />
        {!loadingDrafts && (
          <>
            <S.GridRow>
              <GridColumn>
                <OrderGridColumn>
                  <PageHeader>Drafts</PageHeader>
                  {this.state.canCreate && !definitelyNoDrafts && (
                    <Box>
                      <Button positive centered onClick={() => this.goToNewOrderPage()} title="New Order" name="plus" />
                    </Box>
                  )}
                </OrderGridColumn>
              </GridColumn>
            </S.GridRow>
            {definitelyNoDrafts ? (
              <NoDraftsContainer>
                <NoDraftsText>
                  <Text textAlign="center">
                    <P>Looks like you don't have any drafts yet.</P>
                    <P>If you save the order before it's complete, return here to find the draft.</P>
                  </Text>
                </NoDraftsText>
                {this.state.canCreate && (
                  <Button positive centered onClick={() => this.goToNewOrderPage()} title="New Order" name="plus" />
                )}
              </NoDraftsContainer>
            ) : (
              <S.GridRow>
                <GridColumn>
                  <S.Tab panes={this.renderPanesComponents()} />
                </GridColumn>
              </S.GridRow>
            )}
          </>
        )}
      </S.Grid>
    )

    return <S.Container>{renderComponent}</S.Container>
  }
}

const DraftsPageWithApollo: any = compose(
  withOrganizationHook,
  graphql(AllDraftsDocument, {
    options: (props: any) => {
      const {
        urlParams: {
          pageInfo: { numItemsPerPage, orderBy, page },
        },
        organization: { id: organizationId },
      } = props
      const parsedOrderBy = orderBy.filter((orderFilter) => orderFilter.attribute === 'createdAt')

      return {
        fetchPolicy: 'network-only',
        variables: {
          organizationId,
          pageInfo: {
            numItemsPerPage,
            orderBy: parsedOrderBy,
            page: parseInt(page),
          },
        },
      }
    },
  }),
  graphql(DeleteDraftDocument, {
    props: ({ mutate }) =>
      ({
        deleteDraft: (input) => {
          if (mutate) {
            mutate({
              refetchQueries: ['AllDrafts'],
              variables: {
                input,
              },
            })
          }
        },
      } as any),
  }),
  withApollo,
)(DraftsPage)

function DraftsPageHOC(props) {
  return <DraftsPageWithApollo urlParams={useGetVarsFromURL()} {...props} />
}

export default withRouter(DraftsPageHOC)
