import { AUTH_TOKEN } from 'modules/App/constants'

export const getToken = () => {
  const token = localStorage.getItem(AUTH_TOKEN)
  return token
}

export const saveToken = (token: string) => {
  localStorage.setItem(AUTH_TOKEN, token)
}

export const deleteToken = () => {
  localStorage.clear()
  localStorage.removeItem(AUTH_TOKEN)
}
