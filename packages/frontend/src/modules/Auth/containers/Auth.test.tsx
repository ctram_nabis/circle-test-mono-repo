import { MockedProvider } from '@apollo/react-testing'
import { mount, shallow } from 'enzyme'
import React from 'react'
import { ForgotMyModal } from './ForgotMyModal'
import { MyOrganizations } from './MyOrganizations'
import { NewOrganization } from './NewOrganization'
import { NewPassword } from './NewPassword'
import { OrganizationPage } from './OrganizationPage'
import { SignIn } from './SignIn'
import { SignUp } from './SignUp'

describe('Onboarding container', () => {
  describe('ForgotMyModal', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <ForgotMyModal />
        </MockedProvider>,
      )
    })
  })

  describe('MyOrganizations', () => {
    it('should render without crashing', () => {
      shallow(<MyOrganizations />)
    })
  })

  describe('NewOrganization', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <NewOrganization />
        </MockedProvider>,
      )
    })
  })

  describe('NewPassword', () => {
    it('should render without crashing', () => {
      shallow(<NewPassword />)
    })
  })

  describe('OrganizationPage', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <OrganizationPage />
        </MockedProvider>,
      )
    })
  })

  describe('SignIn', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <SignIn />
        </MockedProvider>,
      )
    })
  })

  describe('SignUp', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <SignUp />
        </MockedProvider>,
      )
    })
  })
})
