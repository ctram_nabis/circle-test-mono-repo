import { Container, PageHeader, S, Wrapper } from 'components'
import { Text } from 'components/core/src/Text'
import { useRouter } from 'modules/Navigation'
import { OrganizationInviteRow, OrganizationRow } from 'modules/Organization'
import { useMe } from 'modules/User'
import React from 'react'

const NABIS_DELIVERIES_ORG_NAME = 'Nabione, Inc. (Deliveries)'

export function MyOrganizations() {
  const router = useRouter()
  const { user } = useMe()
  const { organizations, organizationInvites } = user || {}

  const nabisDeliveries = organizations && organizations.find((org) => org.name === NABIS_DELIVERIES_ORG_NAME)

  const sortedOrgs =
    organizations &&
    organizations
      .filter((org) => org.name !== NABIS_DELIVERIES_ORG_NAME)
      .sort((a, b) => {
        const [aOrgName, bOrgName] = [a.name, b.name]

        return aOrgName === bOrgName ? 0 : aOrgName > bOrgName ? 1 : -1
      })

  if (nabisDeliveries && sortedOrgs) {
    sortedOrgs.unshift(nabisDeliveries)
  }

  const pendingInvites = organizationInvites && organizationInvites.filter((invite) => invite.accepted === null)

  return (
    <Container col middle center>
      <PageHeader>My Organizations</PageHeader>
      <Wrapper width="auto">
        <S.ItemGroup divided link>
          {!sortedOrgs || sortedOrgs.length < 1 ? (
            <>
              <Text center>Looks like you don't have any organizations yet. </Text>
              <Text center>Please add a new organization.</Text>
            </>
          ) : (
            sortedOrgs.map((org, i) => <OrganizationRow key={`org-row-${i}`} organization={org} />)
          )}
        </S.ItemGroup>

        <S.Card centered onClick={() => router.push('/new-organization')} data-test="card-add-new-organization">
          <S.Card.Content textAlign="center">
            <S.Icon name="plus" />
            Add New
          </S.Card.Content>
        </S.Card>
        {pendingInvites && pendingInvites.length > 0 && (
          <>
            <PageHeader>Invitations</PageHeader>
            <S.Card fluid>
              <S.Card.Content>
                <S.Table celled padded selectable unstackable verticalAlign="middle" sortable color="green">
                  <S.Table.Header>
                    <S.Table.Row>
                      <S.Table.HeaderCell>
                        <S.Label ribbon color="green">
                          Organization
                        </S.Label>
                      </S.Table.HeaderCell>
                      <S.Table.HeaderCell>Sender</S.Table.HeaderCell>
                      <S.Table.HeaderCell>Actions</S.Table.HeaderCell>
                    </S.Table.Row>
                  </S.Table.Header>

                  <S.Table.Body>
                    {pendingInvites.map((invite) => (
                      <OrganizationInviteRow key={invite.id} invitation={invite} />
                    ))}
                  </S.Table.Body>
                </S.Table>
              </S.Card.Content>
            </S.Card>
          </>
        )}
      </Wrapper>
    </Container>
  )
}
