import { useAuthToken } from 'modules/Auth/useAuthToken'
import React from 'react'
import { Redirect } from 'react-router-dom'

export function AuthSwitch() {
  const { token } = useAuthToken()

  if (token) {
    return <Redirect to="/my-organizations" />
  }
  return <Redirect to="/sign-in" />
}
