import { Button } from 'components/Button'
import { Container } from 'components/Container'
import { Box } from 'components/core/src/Box'
import { ErrorBox } from 'components/ErrorBox'
import { Form } from 'components/Form'
import { PageHeader } from 'components/PageHeader'
import { LoginUserInput, useSignInMutation } from 'generated/graphql'
import { useError, useErrors } from 'hooks'
import { useAuthToken } from 'modules/Auth'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import logger from 'modules/Logger'
import { useRouter } from 'modules/Navigation'
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { ForgotMyModal } from './ForgotMyModal'

const TextButton = styled(Button)`
  &.ui.button.basic.black {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-align: left;
    text-decoration: none;
  }
`

export const SignIn = () => {
  const router = useRouter()
  const { token, setToken } = useAuthToken()
  const [errors, setErrors] = useErrors('SignIn')
  const [error, setError] = useError('SignIn')
  const [forgotModalOpen, setForgotModalOpen] = useState<boolean>(false)

  const [signInMutation] = useSignInMutation({
    refetchQueries: ['MeQuery'],
  })

  const validationSchema = createValidationSchema<LoginUserInput>({
    email: validators.email,
    password: validators.password,
  })

  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: async (inputs) => {
      try {
        const response = await signInMutation({
          variables: {
            input: {
              ...inputs,
            },
          },
        })

        if (response.errors) {
          logger.error(`SignIn ------ response.errors`, response.errors)
          setErrors(response.errors)
          return
        }

        const newToken = response.data?.loginUser?.token

        if (newToken) {
          setToken(newToken)
        } else {
          logger.error('SignIn --- attempted to sign in, but no token provided')
          setError('Failed to sign in. Please try again or contact support')
        }
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) return setErrors(err?.graphQLErrors)
        return setError(err?.graphQLErrors.message)
      }
    },
  })

  useEffect(() => {
    if (token) {
      router.push('/my-organizations')
    }
  }, [token, router])

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        control: 'input',
        type: 'email',
        size: 'large',
        label: 'Email',
        labelStyles: { width: '100%', bg: 'transparent' },
        placeholder: 'Email',
        name: 'email',
        values: form.values,
        errors: form.errors,
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        required: true,
        key: 'email',
      }),
      createFormField({
        control: 'input',
        type: 'password',
        size: 'large',
        label: 'Password',
        labelStyles: { width: '100%', bg: 'transparent' },
        placeholder: 'Password',
        name: 'password',
        values: form.values,
        errors: form.errors,
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        required: true,
        key: 'password',
      }),
    ],
  ]

  return (
    <Container center middle>
      <Box width="80%" display="flex" flexDirection="column">
        <PageHeader>Sign into your account</PageHeader>
        <ErrorBox errors={errors} error={error} />
        <Form onSubmit={form.handleSubmit} id="sign-in">
          {fieldsInGroups}
          <TextButton
            type="button"
            basic
            color="black"
            onClick={(e: { preventDefault: () => void }) => {
              e.preventDefault()
              setForgotModalOpen(true)
            }}
            title="Forgot your password?"
          />
          <Button
            positive
            type="submit"
            fluid
            form="sign-in"
            size="large"
            style={{ margin: '30px 0 10px' }}
            width="100%"
            title="Sign in"
          />
        </Form>
      </Box>
      {forgotModalOpen && (
        <ForgotMyModal
          email={form.values.email}
          // onSuccess={() => setForgotModalOpen(false)}
          onClose={() => setForgotModalOpen(false)}
        />
      )}
    </Container>
  )
}
