import React from 'react'
import { useSetState } from 'react-use'
import { Button, Container, Form, Header, Input, Label } from 'semantic-ui-react'
import { Box } from 'theme-ui'

interface Props {
  error?: boolean
  handleChange: () => void
  handleSubmit: () => void
}

export function NewPassword(props: Props) {
  const { error, handleChange, handleSubmit } = props

  const [state] = useSetState({
    new_password: '',
    confirm_password: '',
  })

  return (
    <Container>
      <Form onSubmit={handleSubmit} id="new-password">
        {error}
        <Box>
          <Header>Enter your new password</Header>
          <p>Choose a password that is unique and memorable.</p>
        </Box>
        <Label>New Password</Label>
        <Input
          id="new_password"
          type="password"
          size="large"
          defaultValue={''}
          value={state.new_password}
          required
          onChange={handleChange}
        />
        <Label>Confirm Password</Label>
        <Input
          id="confirm_password"
          type="password"
          size="large"
          defaultValue={''}
          value={state.confirm_password}
          required
          onChange={handleChange}
        />
      </Form>
      <Button
        type="submit"
        form="new-password"
        positive
        size="large"
        style={{ margin: '30px 0 10px' }}
        label="Update Password"
      />
    </Container>
  )
}
