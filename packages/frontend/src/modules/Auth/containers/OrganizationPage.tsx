import { Button } from 'components/Button'
import { Container } from 'components/Container'
import { Box } from 'components/core/src/Box'
import { Label } from 'components/Label'
import { PageHeader } from 'components/PageHeader'
import { S } from 'components/SemanticComponents'
import { License, useDeleteLicenseMutation } from 'generated/graphql'
import cloneDeep from 'lodash/cloneDeep'
import { AddEditLicensesModal } from 'modules/License'
import { useRouter } from 'modules/Navigation'
import { createAddressForLicense, useOrganization } from 'modules/Organization'
import React, { useState } from 'react'
import { useToggle } from 'react-use'
import styled from 'styled-components'
import { OrganizationPageLicenseRow } from '../components'
import { EditOrganizationForm } from './EditOrganizationForm'

export const TextButton = styled(Button)`
  &.ui.button.basic.green {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
  &.ui.button.basic.red {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
`

export const OrganizationPage = () => {
  const router = useRouter()
  const { organization } = useOrganization()

  const [isEditMode, setEditMode] = useState<boolean>(false)
  const [isLicenseModalOpen, setLicenseModalOpen] = useToggle(false)
  const [licenses, setLicenses] = useState(organization.licenses)

  const [deleteLicenseMutation] = useDeleteLicenseMutation({
    refetchQueries: ['GetOrganization', 'AllLicenses'],
  })

  const handleDeleteLicense = async (index: number, input: { id: string }) => {
    const currentLicenses = licenses || []
    const newLicenses = currentLicenses.slice(0)
    newLicenses.splice(index, 1)
    setLicenses(newLicenses)
    await deleteLicenseMutation({ variables: { input: { id: input.id } } })
  }

  const updateLicenses = (license: License) => {
    const clonedLicenses = cloneDeep(licenses || [])
    const licenseIndex = clonedLicenses.findIndex((lic) => lic?.id === license.id)
    clonedLicenses[licenseIndex] = license
    setLicenses(clonedLicenses)
  }

  const { alias = '', name = '', address1, address2, city, state, zip, phone, doingBusinessAs, owner, members } =
    organization || {}

  const _organization = {
    id: organization.id,
    address1: organization.address1,
    address2: organization.address2,
    city: organization.city,
    state: organization.state,
    zip: organization.zip,
    phone: organization.phone,
  }

  const address = createAddressForLicense(_organization, organization.owner)

  const { firstName, lastName, email } = owner || {}

  return (
    <Container center middle display="flex" flexDirection="column">
      <Box width="70%">
        <PageHeader textAlign="center">
          Organization:
          <Box mr={10} ml={10} display="inline-block">
            <Button color="green" size="large" onClick={() => router.push(`/${alias}/app/dashboard`)} title={name} />
          </Box>
          {!isEditMode && <TextButton basic color="green" title="Edit" onClick={() => setEditMode(true)} />}
        </PageHeader>

        <S.Segment>
          {isEditMode ? (
            <EditOrganizationForm
              organization={organization}
              onSuccess={() => setEditMode(false)}
              onCancel={() => setEditMode(false)}
            />
          ) : (
            <S.Form>
              <S.Card fluid>
                <S.Card.Content>
                  <S.Form.Group widths="equal">
                    <S.Form.Field>
                      <Label>Owner</Label>
                      {firstName} {lastName} ({email})
                    </S.Form.Field>
                    <S.Form.Field>
                      <Label># of Members</Label>
                      {members && members.length}
                    </S.Form.Field>
                  </S.Form.Group>
                  <S.Divider />
                  <S.Form.Group widths="equal">
                    <S.Form.Field>
                      <Label htmlFor="name">Company Name</Label>
                      {name || '-'}
                    </S.Form.Field>
                    <S.Form.Field>
                      <Label htmlFor="alias">Alias</Label>
                      {alias || '-'}
                    </S.Form.Field>
                  </S.Form.Group>
                  <S.Form.Group widths="equal">
                    <S.Form.Field>
                      <Label htmlFor="address1">Address 1</Label> {address1 || '-'}
                    </S.Form.Field>
                    <S.Form.Field>
                      <Label htmlFor="address2">Address 2</Label> {address2 || '-'}
                    </S.Form.Field>
                  </S.Form.Group>
                  <S.Form.Group widths="equal">
                    <S.Form.Field>
                      <Label htmlFor="city">City</Label> {city || '-'}
                    </S.Form.Field>
                    <S.Form.Field>
                      <Label htmlFor="state">State</Label> {state || '-'}
                    </S.Form.Field>
                    <S.Form.Field>
                      <Label htmlFor="zip">Zip Code</Label> {zip || '-'}
                    </S.Form.Field>
                  </S.Form.Group>
                  <S.Form.Field>
                    <Label htmlFor="phone">Phone</Label> {phone || '-'}
                  </S.Form.Field>
                  <S.Form.Field>
                    <Label htmlFor="doingBusinessAs">Doing business as (DBA):</Label> {doingBusinessAs || '-'}
                  </S.Form.Field>
                </S.Card.Content>
              </S.Card>
            </S.Form>
          )}
        </S.Segment>

        <S.Segment>
          <S.Card fluid>
            <S.Card.Content>
              <S.Form.Field>
                <Label>Licenses</Label>
                <S.Container>
                  {licenses &&
                    licenses.map(
                      (license, index) =>
                        license && (
                          <OrganizationPageLicenseRow
                            key={`org-license-${index}`}
                            index={index}
                            license={license}
                            address={address}
                            organization={organization}
                            licensesLength={licenses.length}
                            updateLicenses={updateLicenses}
                            handleDeleteLicense={handleDeleteLicense}
                          />
                        ),
                    )}

                  <S.Divider />

                  <Button
                    color="green"
                    name="plus"
                    title="Add New License"
                    size="small"
                    onClick={() => setLicenseModalOpen(true)}
                  />
                  {isLicenseModalOpen && (
                    <AddEditLicensesModal
                      action="ADD"
                      address={address}
                      organization={_organization}
                      onClose={() => setLicenseModalOpen(false)}
                      onSuccess={(license: License) => {
                        const clonedLicenses = cloneDeep(licenses || [])
                        clonedLicenses.push(license)
                        setLicenses(clonedLicenses)
                        setLicenseModalOpen(false)
                      }}
                    />
                  )}
                </S.Container>
              </S.Form.Field>
            </S.Card.Content>
          </S.Card>
        </S.Segment>
      </Box>
    </Container>
  )
}
