import { Button, ErrorBox, P, S } from 'components'
import { Form } from 'components/Form'
import { CreateTokenSendEmailMutationVariables, useCreateTokenSendEmailMutation } from 'generated/graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import logger from 'modules/Logger'
import React, { useState } from 'react'
import { ErrorType } from 'types'

const INLINE_REQUIRED_MSG = ' * Required'
const validationSchema = createValidationSchema<CreateTokenSendEmailMutationVariables>({
  email: validators.string.required(INLINE_REQUIRED_MSG),
})
interface Props {
  email: string
  trigger?: JSX.Element
  open?: boolean
  onOpen?: () => void
  onClose?: () => void
  onSuccess?: () => void
  onFailure?: () => void
}

export function ForgotMyModal(props: Props) {
  const { email, trigger, onOpen, open = true, onClose, onFailure, onSuccess } = props
  const [createTokenSendEmailMutation] = useCreateTokenSendEmailMutation()
  const [success, setSuccess] = useState()
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()

  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      email: email || '',
    },
    onSubmit: async (inputs) => {
      try {
        const { errors } = await createTokenSendEmailMutation({
          variables: {
            ...inputs,
          },
        })

        if (errors) {
          setRequestErrors(errors)
          return onFailure && onFailure()
        } else {
          setSuccess(true)
          return onSuccess && onSuccess()
        }
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) return setRequestErrors(err?.graphQLErrors)
        return setCatchError(err?.graphQLErrors?.message)
      }
    },
  })

  const fields = {
    email: createFormField({
      control: 'input',
      size: 'large',
      labelStyles: { width: '100%', bg: 'transparent' },
      required: true,
      label: 'Email',
      name: 'email',
      onChange: form.handleChange,
      onBlur: form.handleBlur,
      placeholder: 'Email',
      type: 'text',
      values: form.values,
      errors: form.errors,
      disabled: form.isSubmitting,
    }),
  }

  const closeModal = () => {
    return onClose && onClose()
  }

  return (
    <S.Modal onOpen={onOpen} open={open} onClose={onClose} closeIcon trigger={trigger} size="small">
      <S.Header>
        <S.Icon name="user" />
        Need help with your password?
      </S.Header>
      <S.Modal.Content>
        {success ? (
          <S.Segment placeholder textAlign="center">
            <S.Header>
              <S.Icon name="check circle" color="green" />
              Email sent
            </S.Header>
          </S.Segment>
        ) : (
          <Form id="forgot-password" onSubmit={form.submitForm}>
            <S.Grid>
              <ErrorBox errors={requestErrors} error={catchError} />
              <S.Grid.Row columns={2}>
                <S.Grid.Column verticalAlign="middle">{fields.email}</S.Grid.Column>
                <S.Grid.Column verticalAlign="middle">
                  <P mt={20}>Enter the email you use for Nabis, and we'll help you create a new password.</P>
                </S.Grid.Column>
              </S.Grid.Row>
            </S.Grid>
          </Form>
        )}
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button onClick={closeModal} name="x" title={success ? 'Close' : 'Cancel'} disabled={form.isSubmitting} />
        {!success && (
          <Button
            type="submit"
            disabled={form.isSubmitting}
            loading={form.isSubmitting}
            name="mail"
            title="Reset Password"
            form="forgot-password"
          />
        )}
      </S.Modal.Actions>
    </S.Modal>
  )
}
