import { Button, ErrorBox, Label, PageHeader, S } from 'components'
import { Text } from 'components/core/src/Text'
import { useFormik } from 'formik'
import { CreateOrganizationInput, License, useCreateOrganizationMutation } from 'generated/graphql'
import cloneDeep from 'lodash/cloneDeep'
import { createFormField } from 'modules/Forms'
import { AddEditLicensesModal, CreateOrUpdateLicenseValidationSchema } from 'modules/License'
import { useRouter } from 'modules/Navigation'
import { createAddressForLicense, useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import { useToggle } from 'react-use'
import { ChangeEvent } from 'types'
import urlSlug from 'url-slug'
import * as Yup from 'yup'
import { NewOrganizationLicenseRow } from '../components'

const FORM_KEYS = {
  name: 'name',
  alias: 'alias',
  address1: 'address1',
  address2: 'address2',
  city: 'city',
  state: 'state',
  zip: 'zip',
  phone: 'phone',
  doingBusinessAs: 'doingBusinessAs',
  licenses: 'licenses',
}

const INLINE_REQUIRED_MSG = ' * Required'

const OrgValidationSchema = {
  name: Yup.string()
    .trim()
    .required(INLINE_REQUIRED_MSG),
  alias: Yup.string()
    .trim()
    .required(INLINE_REQUIRED_MSG),
  address1: Yup.string()
    .trim()
    .required(INLINE_REQUIRED_MSG),
  address2: Yup.string().trim(),
  city: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  state: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  zip: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
  phone: Yup.string().trim(),
  doingBusinessAs: Yup.string()
    .required(INLINE_REQUIRED_MSG)
    .trim(),
}

export const CreateOrgValidationSchema = Yup.object().shape({
  ...OrgValidationSchema,
  licenses: Yup.array()
    .of(CreateOrUpdateLicenseValidationSchema)
    .required(' * Please add at least one license that belongs to your organization.'),
})

export const NewOrganization = () => {
  const { setOrganizationId } = useOrganization()
  const router = useRouter()
  const me = useMe()
  const [requestErrors, setRequestErrors] = useState()
  const [open, setOpen] = useToggle(false)

  const [createOrganizationMutation] = useCreateOrganizationMutation({
    refetchQueries: ['AllOrganizations', 'GetUser'],
  })

  // type InputValues = Omit<Organization, 'id'>
  // TODO: REPLACE WITH useForm and createValidationSchema helper
  const form = useFormik({
    initialValues: {
      name: '',
      alias: '',
      address1: '',
      address2: '',
      city: '',
      state: 'CA',
      zip: '',
      phone: '',
      doingBusinessAs: '',
      licenses: [],
    },
    onSubmit: async (inputs) => {
      const response = await createOrganizationMutation({
        variables: {
          input: {
            ...inputs,
          } as CreateOrganizationInput,
        },
      })

      if (response.errors) {
        setRequestErrors(response.errors)
        return
      }

      // after successful creation and returning of alias, redirect to the new org's dashboard

      const changedOrganization = response?.data?.createOrganization?.changedOrganization

      if (changedOrganization) {
        setOrganizationId(changedOrganization.id)
        router.push(`/${changedOrganization.alias}/app/dashboard`)
        return
      }
    },
    validationSchema: CreateOrgValidationSchema,
  })

  const handleNameChange = (event: ChangeEvent) => {
    const value = event.target.value
    const alias = cleanAlias(value)

    form.setValues({
      ...form.values,
      [FORM_KEYS.name]: value,
      [FORM_KEYS.alias]: alias,
    })
  }

  const handleAliasChange = (event: ChangeEvent) => {
    const value = event.target.value

    form.setFieldValue(FORM_KEYS.alias, cleanAlias(value))
  }

  const cleanAlias = (str: string) => {
    if (!str) return ''

    return urlSlug(str, {
      camelCase: false,
    })
  }

  const addLicense = (license: Partial<License>) => {
    const newLicenses = (cloneDeep(form.values.licenses) || []) as Array<Partial<License>>
    newLicenses.push(license)
    form.setFieldValue(FORM_KEYS.licenses, newLicenses)
    form.setFieldTouched(FORM_KEYS.licenses, true)
  }

  const editLicense = (license: Partial<License>, index: number) => {
    const newLicenses = (cloneDeep(form.values.licenses) || []) as Array<Partial<License>>
    newLicenses[index] = license
    form.setFieldValue(FORM_KEYS.licenses, newLicenses)
    form.setFieldTouched(FORM_KEYS.licenses, true)
  }

  const deleteLicense = (index: number) => {
    const newLicenses = (cloneDeep(form.values.licenses) || []) as Array<Partial<License>>
    newLicenses.splice(index, 1)
    form.setFieldValue(FORM_KEYS.licenses, newLicenses)
    form.setFieldTouched(FORM_KEYS.licenses, true)
  }

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        required: true,
        label: 'Company Name',
        name: 'name',
        onChange: handleNameChange,
        onBlur: form.handleBlur,
        placeholder: 'Nabione LLC',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Alias',
        name: 'alias',
        onChange: handleAliasChange,
        onBlur: form.handleBlur,
        placeholder: 'nabione',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'Address 1',
        name: 'address1',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: '1234 Main St.',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: false,
        label: 'Address 2',
        name: 'address2',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Suite 123',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'City',
        name: 'city',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'San Francisco',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'State',
        name: 'state',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'CA',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Zip Code',
        name: 'zip',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: '94103',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: false,
        label: 'Phone',
        name: 'phone',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: '123-456-7890',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'Doing business as (DBA):',
        name: 'doingBusinessAs',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Nabis',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
  ]

  const address = createAddressForLicense(form.values, me)

  const licenses: License[] = cloneDeep(form.values.licenses)

  return (
    <S.Container>
      <PageHeader>Create an organization</PageHeader>
      <ErrorBox errors={requestErrors} />
      <S.Segment>
        <S.Form id="new-organization-form" onSubmit={form.handleSubmit}>
          <S.Card fluid>
            <S.Card.Content>
              {fieldsInGroups.map((fields, i) => (
                <S.Form.Group key={`formGroup-${i}`} widths="equal">
                  {fields}
                </S.Form.Group>
              ))}
            </S.Card.Content>
          </S.Card>
          <S.Card fluid>
            <S.Card.Content>
              <Label>Licenses</Label>
              {/* TODO: Use ErrorBox */}
              {form.errors.licenses && <Text color="error">{form.errors.licenses}</Text>}
              {licenses &&
                licenses.map(
                  (license, index) =>
                    license && (
                      <NewOrganizationLicenseRow
                        index={index}
                        license={license}
                        address={address}
                        values={form.values}
                        editLicense={editLicense}
                        deleteLicense={deleteLicense}
                        licensesLength={licenses.length}
                      />
                    ),
                )}

              <S.Divider />

              <Button name="plus" type="button" title="Add New License" size="small" onClick={() => setOpen(true)} />
              {open && (
                <AddEditLicensesModal
                  address={address}
                  action="ADD"
                  organization={form.values}
                  returnLicense={(_license) => {
                    addLicense(_license)
                    setOpen(false)
                  }}
                  onClose={() => setOpen(false)}
                />
              )}
            </S.Card.Content>
          </S.Card>
          <S.Button
            content="Cancel"
            type="button"
            onClick={(e) => {
              e.preventDefault()
              router.push('/')
            }}
          />
          <S.Button
            positive
            form="new-organization-form"
            type="submit"
            loading={form.isSubmitting}
            disabled={form.isSubmitting}
          >
            Submit
          </S.Button>
        </S.Form>
      </S.Segment>
    </S.Container>
  )
}
