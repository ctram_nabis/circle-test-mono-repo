import { Button } from 'components/Button'
import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { ErrorBox } from 'components/ErrorBox'
import { Form } from 'components/Form'
import { S } from 'components/SemanticComponents'

import { CreateUserInput, useSignUpMutation } from 'generated/graphql'
import { GraphQLError } from 'graphql'
import { useAuthToken } from 'modules/Auth/useAuthToken'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import { logger } from 'modules/Logger'
import { useRouter } from 'modules/Navigation'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import styled from 'styled-components'
import { ErrorType } from 'types'
// TODO: switch all out for chris components
export const Wrapper = styled(Box)`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100%;
  padding: 30px 15px;
  width: 100%;
`

export const CloseIcon = styled(Button)`
  &.ui.medium.button {
    background: transparent;
  }
`

const Title = styled(Box)`
  font-size: 32px;
  font-weight: 600;
  margin: 10px 0px;
  @media (max-width: 700px) {
    font-size: 24px;
    margin: 5px 0px;
  }
`

const TextButton = styled(Button)`
  &.ui.button.basic.green {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
`
export function SignUp() {
  const router = useRouter()
  const { isAuthenticated } = useMe()
  const { setToken } = useAuthToken()
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()

  const { invitationId, email } = router.query

  const [signUpMutation, { error }] = useSignUpMutation({
    refetchQueries: ['MeQuery'],
  })

  // redirect to dashboard if attempting signup and logged in
  if (isAuthenticated) {
    router.push('/my-organizations')
  }

  interface Extras {
    confirmPassword: string
  }

  const validationSchema = createValidationSchema<CreateUserInput & Extras>({
    firstName: validators.name,
    lastName: validators.name,
    email: validators.email,
    phone: validators.phoneNumber,
    password: validators.newPassword,
    confirmPassword: validators.passwordSignUpConfirmation,
    invitationId: validators.string,
  })

  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      firstName: '',
      lastName: '',
      email: decodeURIComponent(email || ''),
      phone: '',
      password: '',
      confirmPassword: '',
      invitationId,
    },
    onSubmit: async (inputs) => {
      try {
        const inputsToSend = { ...inputs }
        delete inputsToSend.confirmPassword
        const response = await signUpMutation({
          variables: {
            input: inputsToSend,
          },
        })

        // set errors if there are any
        if (response.errors) {
          setRequestErrors(response.errors)
          return
        }
        const token = response.data?.createUser?.token
        if (token) {
          setToken(token)
          router.push('/my-organizations')
        }
      } catch (err) {
        logger.error(err)
        setCatchError(err?.graphQLErrors.message)
      }
    },
  })

  const formFields = {
    firstName: createFormField({
      type: 'text',
      size: 'large',
      label: 'First Name',
      placeholder: 'First Name',
      name: 'firstName',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      required: true,
    }),
    lastName: createFormField({
      type: 'text',
      size: 'large',
      label: 'Last Name',
      placeholder: 'Last Name',
      name: 'lastName',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      required: true,
    }),
    email: createFormField({
      type: 'email',
      size: 'large',
      label: 'Email',
      placeholder: 'Email',
      name: 'email',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      required: true,
    }),
    phone: createFormField({
      type: 'text',
      size: 'large',
      label: 'Phone',
      placeholder: 'Phone',
      name: 'phone',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      required: true,
    }),
    password: createFormField({
      type: 'password',
      size: 'large',
      label: 'Password',
      placeholder: 'Password',
      name: 'password',
      autoComplete: 'new-password',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      required: true,
    }),

    confirmPassword: createFormField({
      type: 'password',
      size: 'large',
      label: 'Confirm Password',
      placeholder: 'Confirm Password',
      name: 'confirmPassword',
      autoComplete: 'new-password',
      values: form.values,
      errors: form.errors,
      onChange: form.handleChange,
      required: true,
    }),
  }

  return (
    <S.Container>
      <Wrapper>
        <Box width="60%">
          <Form mb={20} id="sign-up" onSubmit={form.handleSubmit}>
            <Box width="100%" display="flex" justifyContent="flex-end">
              <CloseIcon
                type="button"
                onClick={(e: { preventDefault: () => void }) => {
                  e.preventDefault()
                  router.push('/')
                }}
                name="x"
                size="medium"
              />
            </Box>
            <ErrorBox errors={(error?.graphQLErrors as GraphQLError[]) || requestErrors} error={catchError} />
            <Title>Create an account</Title>
            <S.Grid columns={2} stretched>
              <S.GridRow>
                <S.GridColumn>{formFields.firstName}</S.GridColumn>
                <S.GridColumn>{formFields.lastName}</S.GridColumn>
              </S.GridRow>
            </S.Grid>
            {formFields.email}
            {formFields.phone}
            {formFields.password}
            {formFields.confirmPassword}
            <Button
              positive
              fluid
              form="sign-up"
              type="submit"
              loading={form.isSubmitting}
              disabled={form.isSubmitting}
              title="Create Account"
            />
          </Form>
          <Text textAlign="center" m={10}>
            Already have an account? <TextButton basic color="green" title="Log In" onClick={() => router.push('/')} />
          </Text>
        </Box>
      </Wrapper>
    </S.Container>
  )
}
