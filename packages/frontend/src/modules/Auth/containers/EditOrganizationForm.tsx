import { Button, ErrorBox, Label, S, Text } from 'components'
import { Organization, UpdateOrganizationInput, useUpdateOrganizationMutation } from 'generated/graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import logger from 'modules/Logger'
import { useRouter } from 'modules/Navigation'
import React, { useState } from 'react'
import { ErrorType } from 'types'

const INLINE_REQUIRED_MSG = ' * Required'

const validationSchema = createValidationSchema<UpdateOrganizationInput>({
  id: validators.string.required(INLINE_REQUIRED_MSG),
  name: validators.string.required(INLINE_REQUIRED_MSG),
  alias: validators.string.required(INLINE_REQUIRED_MSG),
  address1: validators.string.required(INLINE_REQUIRED_MSG),
  address2: validators.string,
  city: validators.string.required(INLINE_REQUIRED_MSG),
  state: validators.string.required(INLINE_REQUIRED_MSG),
  zip: validators.string.required(INLINE_REQUIRED_MSG),
  phone: validators.string,
  doingBusinessAs: validators.string.required(INLINE_REQUIRED_MSG),
})

interface Props {
  organization: Partial<Organization>
  onSuccess: () => void
  onCancel: () => void
}

export const EditOrganizationForm = ({ organization, onSuccess, onCancel }: Props) => {
  const { push } = useRouter()
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()
  const [updateOrganizationMutation] = useUpdateOrganizationMutation({
    refetchQueries: ['AllOrganizations', 'GetUser', 'GetOrganization'],
  })

  const onFailure = (_errors: ErrorType[]) => {}

  const {
    id = '',
    alias = '',
    name = '',
    address1,
    address2,
    city,
    state,
    zip,
    phone,
    doingBusinessAs,
    owner,
    members,
  } = organization || {}

  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      id,
      alias: alias || '',
      name: name || '',
      address1: address1 || '',
      address2: address2 || '',
      city: city || '',
      state: state || '',
      zip: zip || '',
      phone: phone || '',
      doingBusinessAs: doingBusinessAs || '',
    },
    onSubmit: async (inputs) => {
      try {
        const { data, errors } = await updateOrganizationMutation({
          variables: {
            input: inputs,
          },
        })

        if (errors) {
          setRequestErrors(errors)
          onFailure(errors)
          return
        } else {
          push(`/${data?.updateOrganization?.changedOrganization?.alias}/app/edit`)
          return onSuccess && onSuccess()
        }
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors?.message)
        }
      }
    },
  })

  // type InputValues = typeof form.values

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        required: true,
        label: 'Company Name',
        name: 'name',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Nabione, Inc.',
        type: 'text',
        values: form.values,
        errors: form.errors,
        disabled: form.isSubmitting,
      }),
      createFormField({
        required: true,
        label: 'Alias',
        name: 'alias',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'nabione',
        type: 'text',
        values: form.values,
        errors: form.errors,
        disabled: form.isSubmitting,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'Address 1',
        name: 'address1',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: '1234 Main St.',
        type: 'text',
        values: form.values,
        errors: form.errors,
        disabled: form.isSubmitting,
      }),
      createFormField({
        required: false,
        label: 'Address 2',
        name: 'address2',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Suite 123',
        type: 'text',
        values: form.values,
        errors: form.errors,
        disabled: form.isSubmitting,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'City',
        name: 'city',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'San Francisco',
        type: 'text',
        values: form.values,
        errors: form.errors,
        disabled: form.isSubmitting,
      }),
      createFormField({
        required: true,
        label: 'State',
        name: 'state',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'CA',
        type: 'text',
        values: form.values,
        errors: form.errors,
        disabled: form.isSubmitting,
      }),
      createFormField({
        required: true,
        label: 'Zip Code',
        name: 'zip',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: '94103',
        type: 'text',
        values: form.values,
        errors: form.errors,
        disabled: form.isSubmitting,
      }),
    ],
    [
      createFormField({
        required: true,
        label: 'Phone',
        name: 'phone',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: '123-456-7890',
        type: 'text',
        values: form.values,
        errors: form.errors,
        disabled: form.isSubmitting,
      }),
      createFormField({
        required: true,
        label: 'Doing business as (DBA):',
        name: 'doingBusinessAs',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Nabis',
        type: 'text',
        values: form.values,
        errors: form.errors,
        disabled: form.isSubmitting,
      }),
    ],
  ]
  const { firstName, lastName, email } = owner || {}
  return (
    <S.Form onSubmit={form.handleSubmit} id="edit-organization">
      <S.Card fluid>
        <S.Card.Content>
          <ErrorBox errors={requestErrors} error={catchError} />
          <S.Form.Group widths="equal">
            <S.Form.Field>
              <Label>Owner</Label>
              {firstName} {lastName} ({email})
            </S.Form.Field>
            <S.Form.Field>
              <Label># of Members</Label>
              {members && members.length}
            </S.Form.Field>
          </S.Form.Group>
          <S.Divider />
          {fieldsInGroups.map((fields, i) => (
            <S.Form.Group key={`formGroup-${i}`} widths="equal">
              {fields}
            </S.Form.Group>
          ))}
          <S.Grid>
            <S.GridRow>
              <S.GridColumn>
                <Text textAlign="center">
                  <Button
                    color="green"
                    type="submit"
                    form="edit-organization"
                    disabled={form.isSubmitting}
                    loading={form.isSubmitting}
                    title={form.isSubmitting ? 'Saving...' : 'Save'}
                  />
                  <Button onClick={onCancel} type="button" title="Cancel" />
                </Text>
              </S.GridColumn>
            </S.GridRow>
          </S.Grid>
        </S.Card.Content>
      </S.Card>
    </S.Form>
  )
}
