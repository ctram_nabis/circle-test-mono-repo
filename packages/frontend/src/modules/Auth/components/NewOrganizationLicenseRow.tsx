import { S } from 'components'
import { Button } from 'components/Button'
import { AddEditLicensesModal } from 'modules/License'
import React from 'react'
import { useToggle } from 'react-use'
import styled from 'styled-components'

const TextButton = styled(Button)`
  &.ui.button.basic.green {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
`

export const NewOrganizationLicenseRow = ({
  index,
  license,
  address,
  values,
  editLicense,
  deleteLicense,
  licensesLength,
}) => {
  const [open, setOpen] = useToggle(false)

  return (
    <S.Grid columns={16} data-test="new-organization-license-row">
      <S.Grid.Row verticalAlign="bottom">
        <S.Grid.Column width={6}>
          <>
            {index + 1}. {license?.nickname}
          </>
        </S.Grid.Column>
        <S.Grid.Column width={4}>
          <>({license?.licenseNumber})</>
        </S.Grid.Column>
        <S.Grid.Column width={4}>
          <TextButton type="button" onClick={() => setOpen(true)} title="Edit" color="green" basic />
          {open && (
            <AddEditLicensesModal
              action="EDIT"
              address={address}
              license={license}
              onClose={() => setOpen(false)}
              organization={values}
              returnLicense={(_license) => {
                editLicense(_license, index)
                setOpen(false)
              }}
            />
          )}
          {licensesLength > 1 && (
            <S.Button type="button" color="red" onClick={() => deleteLicense(index)}>
              Delete
            </S.Button>
          )}
        </S.Grid.Column>
      </S.Grid.Row>
    </S.Grid>
  )
}
