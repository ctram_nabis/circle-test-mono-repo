import { S } from 'components'
import { License, Organization } from 'generated/graphql'
import { AddEditLicensesModal } from 'modules/License'
import { DeleteConfirmationModal } from 'modules/Modal'
import { AddressPOC } from 'modules/Organization'
import React from 'react'
import { useToggle } from 'react-use'
import { TextButton } from '../containers/OrganizationPage'

interface Props {
  index: number
  license: License
  address: AddressPOC
  organization: Partial<Organization>
  licensesLength: number
  handleDeleteLicense: (index: number, input: { id: string }) => Promise<any>
  updateLicenses: (license: Partial<License>) => void
}

export const OrganizationPageLicenseRow = ({
  index,
  license,
  address,
  organization,
  licensesLength,
  handleDeleteLicense,
  updateLicenses,
}: Props) => {
  const [isAddEditOpen, setIsAddEditOpen] = useToggle(false)
  const [isDeleteOpen, setIsDeleteOpen] = useToggle(false)

  return (
    <S.Grid columns={16}>
      <S.GridRow verticalAlign="bottom">
        <S.GridColumn width={6}>
          <>
            {index + 1}. {license?.nickname}
          </>
        </S.GridColumn>
        <S.GridColumn width={4}>
          <>{license?.licenseNumber}</>
        </S.GridColumn>
        <S.GridColumn width={6}>
          <TextButton color="green" title="Edit" basic onClick={() => setIsAddEditOpen(true)} />
          {isAddEditOpen && (
            <AddEditLicensesModal
              action="EDIT"
              address={address}
              license={license}
              onClose={() => setIsAddEditOpen(false)}
              onSuccess={(_license) => {
                updateLicenses(_license)
              }}
              organization={organization}
            />
          )}
          {licensesLength > 1 && <TextButton color="red" title="Delete" basic onClick={() => setIsDeleteOpen(true)} />}
          {isDeleteOpen && (
            <DeleteConfirmationModal
              delete={() => handleDeleteLicense(index, { id: license.id })}
              onSuccess={() => setIsDeleteOpen(false)}
              onClose={() => setIsDeleteOpen(false)}
            />
          )}
        </S.GridColumn>
      </S.GridRow>
    </S.Grid>
  )
}
