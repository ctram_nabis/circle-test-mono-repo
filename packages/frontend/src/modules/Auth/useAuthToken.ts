import { AuthContext } from 'modules/Auth/useAuthContext'
import React from 'react'

export const useAuthToken = () => {
  const { token, setToken } = React.useContext(AuthContext)
  return { token, setToken }
}
