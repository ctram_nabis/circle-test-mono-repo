/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */

// tslint:disable strict-type-predicates -- chris
import { AUTH_TOKEN } from 'modules/App'
import { useStorage } from 'modules/Storage'
import React, { useEffect } from 'react'

// AUTH CONTEXT

export const AuthContext = React.createContext<{
  token: string | null
  setToken: React.Dispatch<React.SetStateAction<string | null>>
}>({
  token: localStorage.getItem(AUTH_TOKEN),
  setToken: () => {
    throw new Error(`setToken - you need to setToken to a func before using`)
  },
})

const AuthProvider = (props: React.PropsWithChildren<any>) => {
  const [token, setToken] = useStorage(AUTH_TOKEN)

  useEffect(() => {
    if (token) {
      setToken(token)
    } else {
      setToken(null)
    }
  }, [token, setToken])

  // const updateToken = useCallback(
  //   (_token: string) => {
  //     logger.info(`AUTH PROVIDER ------------  setToken`, _token)
  //     setToken(_token)
  //   },
  //   [setToken],
  // )

  return <AuthContext.Provider value={{ token, setToken }}>{props.children}</AuthContext.Provider>
}

export const withAuthProvider = <P extends any>(Component: React.ComponentType<P>) =>
  function WrappedComponent(props: P) {
    return (
      <AuthProvider>
        <Component {...props} />
      </AuthProvider>
    )
  }
