import { AgingReportsPage } from 'modules/Admin/containers'
import { MyOrganizations, NewOrganization, OrganizationPage, SignIn, SignUp } from 'modules/Auth/containers'
import { DashboardPage } from 'modules/Dashboard'
import DraftsPage from 'modules/Drafts/containers/DraftsPage'
import { DriversVehiclesPage } from 'modules/Driver'
import { InventoryPageRootWithOverrideWrapper } from 'modules/Inventory/containers/InventoryPage'
import { OrderChangelog } from 'modules/Order'
import OrdersListPageWithApollo from 'modules/Order/containers/OrdersList/OrdersListPageWithApollo'
import AddOrderPage from 'modules/Order/containers/OrdersPage/AddOrderPage'
import EditOrderPage from 'modules/Order/containers/OrdersPage/EditOrderPage'
import { ReportsPage } from 'modules/Reports'
import { SitesPage } from 'modules/Site'
import { SkuBatchPage } from 'modules/SkuBatch'
import { TeamPage } from 'modules/Team'
import { MyAccount } from 'modules/User/containers/MyAccount'
import { ResetPassword } from 'modules/User/containers/ResetPassword'
import AdminOrdersPageApollo from '../Order/containers/AdminOrdersPage/AdminOrdersPageApollo'
// TODO: DEPRECATED TEMPORARILY - FUTURE DROPOFFS
// import { FutureOrdersContainer } from 'modules/Admin/containers'

export interface RouteConfig {
  path?: string | string[]
  component: any
  exact?: boolean
  sideNav?: boolean
  requireAuth?: boolean
  requireOrg?: boolean
}

export const routes: Record<string, RouteConfig> = {
  // AUTH ROUTES
  SignIn: {
    path: '/sign-in',
    component: SignIn,
    requireAuth: false,
    requireOrg: false,
    sideNav: false,
  },
  SignUp: {
    path: '/signup',
    component: SignUp,
    requireAuth: false,
    requireOrg: false,
    sideNav: false,
  },

  // USER ROUTES
  MyAccount: {
    path: '/account',
    component: MyAccount,
    requireAuth: true,
    exact: true,
    requireOrg: false,
    sideNav: false,
  },
  ResetPassword: {
    path: '/resetpassword/:token/:email',
    component: ResetPassword,
    requireAuth: false,
    requireOrg: false,
    sideNav: false,
  },

  // ROOT ORGANIZATION ROUTES
  MyOrganizations: {
    path: '/my-organizations',
    component: MyOrganizations,
    requireAuth: true,
    requireOrg: false,
    sideNav: false,
  },
  NewOrganization: {
    path: '/new-organization',
    component: NewOrganization,
    requireAuth: true,
    requireOrg: false,
    sideNav: false,
  },
  OrganizationPage: {
    path: '/:alias/app/edit',
    component: OrganizationPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: false,
  },
  OutstandingInvoices: {
    path: '/:alias/app/admin-outstanding-invoices',
    component: AgingReportsPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  AgingReports: {
    path: '/:alias/app/aging-reports',
    component: AgingReportsPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  AddressBook: {
    path: '/:alias/app/addresses',
    component: SitesPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  Dashboard: {
    path: '/:alias/app/dashboard',
    component: DashboardPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  DriversVehicles: {
    path: '/:alias/app/drivers-vehicles',
    component: DriversVehiclesPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  Reports: {
    path: '/:alias/app/reports',
    component: ReportsPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  OrderChangelog: {
    path: '/:alias/app/orders/:id/changelog',
    component: OrderChangelog,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  Orders: {
    path: '/:alias/app/orders',
    exact: true,
    sideNav: true,
    component: OrdersListPageWithApollo,
    requireOrg: true,
    requireAuth: true,
  },
  AdminOrders: {
    path: '/:alias/app/admin-orders',
    exact: true,
    sideNav: true,
    requireAuth: true,
    component: AdminOrdersPageApollo,
  },
  NewOrder: {
    path: '/:alias/app/orders/new',
    component: AddOrderPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
    exact: true,
  },
  EditOrder: {
    path: '/:alias/app/orders/:orderId',
    component: EditOrderPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },

  // inventory
  Inventory: {
    exact: true,
    path: '/:alias/app/inventory',
    component: InventoryPageRootWithOverrideWrapper,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  AdminInventory: {
    exact: true,
    path: '/:alias/app/admin-inventory',
    component: InventoryPageRootWithOverrideWrapper,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  SkuBatchPage: {
    exact: true,
    path: ['/:alias/app/inventory/:skuBatchId', '/:alias/app/admin-inventory/:skuBatchId'],
    component: SkuBatchPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  AdminSkuBatchPage: {
    exact: true,
    path: '/:alias/app/admin-inventory/:skuBatchId',
    component: SkuBatchPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
  Drafts: {
    path: '/:alias/app/drafts',
    component: DraftsPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
    exact: true,
  },
  Team: {
    path: '/:alias/app/team',
    component: TeamPage,
    requireOrg: true,
    requireAuth: true,
    sideNav: true,
  },
}

export const routeNames = Object.keys(routes)

export type RouteName = keyof typeof routes
