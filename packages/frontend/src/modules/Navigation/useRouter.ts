// Thanks To: https://usehooks.com/useRouter/

import queryString from 'query-string'
import { useMemo } from 'react'
import { useHistory, useLocation, useParams, useRouteMatch } from 'react-router-dom'

export function useRouter() {
  const params = useParams()
  const location = useLocation()
  const history = useHistory()
  const match = useRouteMatch()

  // Return our custom router object
  // Memoize so that a new object is only returned if something changes
  return useMemo(() => {
    return {
      // For convenience add push(), replace(), pathname at top level
      push: history.push,
      replace: history.replace,
      pathname: location.pathname,
      queryString,
      // Merge params and parsed query string into single "query" object
      // so that they can be used interchangeably.
      // Example: /:topic?sort=popular -> { topic: "react", sort: "popular" }
      query: {
        ...(queryString.parse(location.search) as any), // Convert string to object
        ...params,
      },
      // Include match, location, history objects so we have
      // access to extra React Router functionality if needed.
      match,
      location,
      history,
    }
  }, [params, match, location, history])
}

/**
 * # Usage
 * function MyComponent() {
 *   # Get the router object
 *   const router = useRouter()
 *
 *   # Get value from query string (?postId=123) or route param (/:postId)
 *   console.log(router.query.postId)
 *
 *   # Get current pathname
 *   console.log(router.pathname)
 *
 *   # Navigate with with router.push()
 *   return onClick={(e) => router.push('/about')}>About</button>
 * }
 */
