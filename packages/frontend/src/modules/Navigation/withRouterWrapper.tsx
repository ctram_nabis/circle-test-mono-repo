import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'

export const withRouterWrapper = (Component: React.ComponentType<any>) => (props: any) => (
  <Router>
    <Component {...props} />
  </Router>
)
