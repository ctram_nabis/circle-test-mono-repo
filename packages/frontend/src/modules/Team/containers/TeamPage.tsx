import { Button, CenterView, ErrorBox, Grid, Message, PageHeader, S } from 'components'
import { useAllRolesQuery, useGetOrganizationUsersQuery, User } from 'generated/graphql'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import { EditTeammateModal } from './EditTeammateModal'
import { InviteTeammateModal } from './InviteTeammateModal'
import { TeamHeaders } from './TeamHeaders'
import { TeamMember } from './TeamMember'

export enum ModalEnum {
  EDIT = 'EDIT',
  ADD = 'ADD',
}
export const TeamPage = () => {
  const { organization, isOrgOwner, isOrgAdmin } = useOrganization()
  const { isAdmin } = useMe()
  const organizationUsers = useGetOrganizationUsersQuery({
    variables: { organizationId: organization?.id },
  })
  const [modalOpen, setModalOpen] = useState<ModalEnum | undefined>(undefined)
  const [showInviteSentMessage, setShowInviteSentMessage] = useState(false)
  const [openTeammate, setOpenTeammate] = useState<Partial<User> | undefined>(undefined)
  const { data, loading, error } = useAllRolesQuery()

  const allRoles = data?.viewer?.allRoles
  const members = organizationUsers?.data?.getOrganization?.members

  const handleSuccess = () => {
    onModalClose()
    setShowInviteSentMessage(true)
  }

  const onModalOpen = (modal: ModalEnum, _teammate?: Partial<User>) => {
    setOpenTeammate(_teammate)
    setModalOpen(modal)
  }
  const onModalClose = () => {
    setModalOpen(undefined)
    setOpenTeammate(undefined)
  }
  return (
    <S.Container>
      <S.Loader active={loading} inline="centered" size="large" content="Loading Team..." />
      <ErrorBox error={error} />
      {!loading && !error && (
        <Grid>
          <S.GridRow>
            <S.GridColumn>
              <CenterView>
                <PageHeader fontSize="3.5em">Team</PageHeader>
                {showInviteSentMessage && <Message success header="Invite sent!" />}
                {(isOrgOwner || isOrgAdmin || isAdmin) && (
                  <Button
                    color="green"
                    name="plus"
                    title="Invite Team Member"
                    onClick={() => onModalOpen(ModalEnum.ADD)}
                  />
                )}
              </CenterView>
            </S.GridColumn>
          </S.GridRow>
          <S.GridRow>
            <S.GridColumn>
              <S.Table celled selectable unstackable verticalAlign="middle" sortable color="green">
                <TeamHeaders isOrgAdmin={isOrgOwner || isOrgAdmin || isAdmin} />
                <S.Table.Body>
                  {(members || []).map(
                    (member) =>
                      member && (
                        <TeamMember
                          key={member.id}
                          onModalOpen={onModalOpen}
                          member={member}
                          organization={organization}
                          allRoles={allRoles}
                        />
                      ),
                  )}
                </S.Table.Body>
              </S.Table>
            </S.GridColumn>
          </S.GridRow>
        </Grid>
      )}
      {modalOpen === 'EDIT' && (
        <EditTeammateModal teammate={openTeammate} onClose={onModalClose} onSuccess={onModalClose} />
      )}
      {modalOpen === 'ADD' && (
        <InviteTeammateModal onClose={onModalClose} onSuccess={handleSuccess} organization={organization} />
      )}
    </S.Container>
  )
}
