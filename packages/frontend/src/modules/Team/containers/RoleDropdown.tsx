import { Dropdown } from 'components'
import { CreateUserRoleInput, Role, useCreateUserRoleMutation, User } from 'generated/graphql'
import React from 'react'
interface Props {
  allRoles?: Role[] | undefined
  disabled?: boolean
  user: Partial<User> | undefined
}

export const RoleDropdown = (props: Props) => {
  const { user, disabled, allRoles = [] } = props

  const [createUserRoleMutation, { loading }] = useCreateUserRoleMutation({
    refetchQueries: ['GetOrganizationUsers'],
  })

  // below can be refactored but wanted to get rid of oc
  const userRoleId = user && user.roles && user.roles[0].id

  const updateRole = (input: CreateUserRoleInput) => {
    // roleId not inputted OR the existing user role has already been set, so no need to update the role
    if (!input.roleId || userRoleId === input.roleId) return

    return createUserRoleMutation({ variables: { input } })
  }

  const roleOptions = allRoles.map((role) => ({
    key: role.id,
    value: role.id,
    text: `${role.name} - ${role.description}`,
  }))

  return (
    <Dropdown
      loading={loading}
      disabled={disabled || loading}
      placeholder="Select"
      fluid
      selection
      options={roleOptions}
      value={userRoleId}
      onChange={(_, role) =>
        updateRole({
          userId: user?.id || '',
          roleId: role.value as string,
        })
      }
    />
  )
}
