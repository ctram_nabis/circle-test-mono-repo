import { logger } from '@sentry/utils'
import { Button, ErrorBox, Form, S } from 'components'
import { UpdateUserInput, User, useUpdateUserMutation } from 'generated/graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import React, { useState } from 'react'
import { ErrorType } from 'types'

const INLINE_REQUIRED_MSG = ' * Required'

const validationSchema = createValidationSchema<UpdateUserInput>({
  id: validators.string.required(INLINE_REQUIRED_MSG),
  email: validators.email.required(INLINE_REQUIRED_MSG),
  firstName: validators.string.required(INLINE_REQUIRED_MSG),
  lastName: validators.string.required(INLINE_REQUIRED_MSG),
  phone: validators.string.required(INLINE_REQUIRED_MSG),
})

interface Props {
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: () => void
  open?: boolean
  teammate?: Partial<User> | undefined | null
  trigger?: JSX.Element
}

export const EditTeammateModal = (props: Props) => {
  const { onClose, onFailure, onOpen, onSuccess, open = true, teammate, trigger } = props

  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState()
  const [isOpen, setIsOpen] = useState<boolean>(open)

  const [updateUserMutation] = useUpdateUserMutation({
    refetchQueries: ['GetOrganizationUsers'],
  })

  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      id: teammate?.id || '',
      email: teammate?.email || '',
      firstName: teammate?.firstName || '',
      lastName: teammate?.lastName || '',
      phone: teammate?.phone || '',
    },
    onSubmit: async (inputs) => {
      try {
        const response = await updateUserMutation({
          variables: {
            input: inputs,
          },
        })

        if (response.errors) {
          setRequestErrors(response.errors)
          return onFailure && onFailure(response.errors)
        }
        setIsOpen(false)
        return onSuccess && onSuccess()
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors?.message)
        }
        return onFailure && onFailure(err)
      }
    },
  })

  type InputValues = typeof form.values

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        required: true,
        label: 'Email*',
        name: 'email',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'bob@example.com',
        type: 'email',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'First Name*',
        name: 'firstName',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Bob',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Last Name*',
        name: 'lastName',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Smith',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Phone*',
        name: 'phone',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: '(555) 555-5555',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
  ]

  const openModal = () => {
    setIsOpen(true)
    return onOpen && onOpen()
  }

  const closeModal = () => {
    setIsOpen(false)
    return onClose && onClose()
  }

  return (
    <S.Modal closeIcon open={isOpen} onOpen={openModal} onClose={closeModal} trigger={trigger}>
      <S.Header icon="edit" content="Edit Teammate" />
      <S.Modal.Content>
        <h3>Enter in your teammate details.</h3>
        <ErrorBox errors={requestErrors} error={catchError} />
        <Form id="edit-teammate-form" onSubmit={form.handleSubmit}>
          {fieldsInGroups.map((fields, i) => (
            <Form.Group key={`formGroup-${i}`}>{fields}</Form.Group>
          ))}
        </Form>
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button type="button" onClick={closeModal} name="x" title="Cancel" />
        <Button
          color="yellow"
          loading={form.isSubmitting}
          disabled={form.isSubmitting}
          form="edit-teammate-form"
          type="submit"
          name="check"
          title="Update"
        />
      </S.Modal.Actions>
    </S.Modal>
  )
}
