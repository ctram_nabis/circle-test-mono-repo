import { MockedProvider } from '@apollo/react-testing'
import { mount, shallow } from 'enzyme'
import React from 'react'
import { EditTeammateModal } from './EditTeammateModal'
import { InviteTeammateModal } from './InviteTeammateModal'
import { RoleDropdown } from './RoleDropdown'
import { TeamHeaders } from './TeamHeaders'
import { TeamMember } from './TeamMember'
import { TeamPage } from './TeamPage'

describe('Team container', () => {
  describe('AddEditTeammateModal', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <EditTeammateModal teammate={{}} />
        </MockedProvider>,
      )
    })
  })

  describe('InviteTeammateModal', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <InviteTeammateModal />
        </MockedProvider>,
      )
    })
  })

  describe('RoleDropdown', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <RoleDropdown user={{ roles: [{}] }} />
        </MockedProvider>,
      )
    })
  })

  describe('TeamHeaders', () => {
    it('should render without crashing', () => {
      shallow(<TeamHeaders />)
    })
  })

  describe('TeamMember', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <TeamMember member={{}} />
        </MockedProvider>,
      )
    })
  })

  describe('TeamPage', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <TeamPage />
        </MockedProvider>,
      )
    })
  })
})
