import { Ribbon, S } from 'components'
import React from 'react'
interface Props {
  isOrgAdmin?: boolean | null
}

export const TeamHeaders = ({ isOrgAdmin }: Props) => {
  return (
    <S.Table.Header>
      <S.Table.Row>
        <S.Table.HeaderCell>
          <Ribbon color="green">Team Member</Ribbon>
        </S.Table.HeaderCell>
        <S.Table.HeaderCell>First Name</S.Table.HeaderCell>
        <S.Table.HeaderCell>Last Name</S.Table.HeaderCell>
        <S.Table.HeaderCell>Phone</S.Table.HeaderCell>
        <S.Table.HeaderCell>Role</S.Table.HeaderCell>
        {isOrgAdmin && <S.Table.HeaderCell>Actions</S.Table.HeaderCell>}
      </S.Table.Row>
    </S.Table.Header>
  )
}
