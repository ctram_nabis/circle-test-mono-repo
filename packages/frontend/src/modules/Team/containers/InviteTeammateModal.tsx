import { Button, ErrorBox, Form, FormField, S } from 'components'
import { CreateOrganizationInviteInput, Organization, useCreateOrganizationInviteMutation } from 'generated/graphql'
import { createValidationSchema, useForm, validators } from 'modules/Forms'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import { ErrorType } from 'types'
import { formatEmail } from 'utils'
interface Props {
  open?: boolean
  onOpen?: () => void
  onClose?: () => void
  onSuccess?: () => void
  onFailure?: (errors: ErrorType[]) => void
  organization?: Partial<Organization> | undefined | null
  trigger?: JSX.Element
}
const INLINE_REQUIRED_MSG = ' * Required'

const validationSchema = createValidationSchema<CreateOrganizationInviteInput>({
  email: validators.email.required(INLINE_REQUIRED_MSG),
  organizationId: validators.string.required(INLINE_REQUIRED_MSG),
  senderId: validators.string.required(INLINE_REQUIRED_MSG),
})

export const InviteTeammateModal = (props: Props) => {
  const { user } = useMe()
  const { organization } = useOrganization()
  const { onClose, onFailure, onOpen, onSuccess, open = true, trigger } = props

  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState()

  const [createOrganizationInviteMutation] = useCreateOrganizationInviteMutation({
    refetchQueries: ['GetOrganizationUsers'],
  })

  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      email: '',
      senderId: user?.id,
      organizationId: organization?.id,
    },
    onSubmit: async (inputs: CreateOrganizationInviteInput) => {
      try {
        const response = await createOrganizationInviteMutation({
          variables: {
            input: {
              ...inputs,
              email: formatEmail(inputs.email),
            },
          },
        })

        if (response.errors) {
          setRequestErrors(response.errors)
          return onFailure && onFailure(response.errors)
        }

        return onSuccess && onSuccess()
      } catch (err) {
        if (Array.isArray(err?.graphQLErrors)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors?.message)
        }
        return onFailure && onFailure([err])
      }
    },
  })

  const openModal = () => {
    return onOpen && onOpen()
  }

  const closeModal = () => {
    return onClose && onClose()
  }

  return (
    <S.Modal closeIcon open={open} onOpen={openModal} onClose={closeModal} trigger={trigger}>
      <S.Header icon="plus" content="Invite Team Member" />
      <S.Modal.Content>
        <Form id="invite-teammate-form" onSubmit={form.handleSubmit}>
          <ErrorBox errors={requestErrors} error={catchError} />
          <FormField
            label="Email"
            name="email"
            type="text"
            required
            placeholder="susan@example.com"
            onChange={form.handleChange}
            onBlur={form.handleBlur}
            values={form.values}
            errors={form.errors}
          />
        </Form>
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button name="x" type="button" onClick={closeModal} title="Cancel" />
        <Button color="yellow" type="submit" form="invite-teammate-form" name="check" title="Invite" />
      </S.Modal.Actions>
    </S.Modal>
  )
}
