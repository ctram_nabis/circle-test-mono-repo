import { S, Small, Text } from 'components'
import { Organization, Role, useDeleteOrganizationUserMutation, User } from 'generated/graphql'
import { DeleteConfirmationModal } from 'modules/Modal'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import { formatPhone } from 'utils'
import { RoleDropdown } from './RoleDropdown'
import { ModalEnum } from './TeamPage'

interface Props {
  member?: Partial<User> | undefined
  organization?: Partial<Organization> | null
  allRoles?: Role[] | undefined
  onModalOpen: (modal: ModalEnum, member?: Partial<User>) => void
}

export const TeamMember = (props: Props) => {
  const [isDeleteOpen, setDeleteOpen] = useState<true | undefined>(undefined)
  const { user, isAdmin } = useMe()
  const { isOrgOwner, isOrgAdmin } = useOrganization()
  const { member, allRoles, onModalOpen, organization } = props
  const canEdit = isOrgOwner || isOrgAdmin || isAdmin
  const [deleteOrganizationUserMutation] = useDeleteOrganizationUserMutation({
    refetchQueries: ['GetOrganizationUsers'],
  })

  const handleDeleteOrganizationUser = () =>
    deleteOrganizationUserMutation({
      variables: {
        input: {
          organizationId: organization?.id || '',
          userId: member?.id || '',
        },
      },
    })

  return (
    <S.Table.Row>
      <S.Table.Cell>
        <Text fontWeight="bold">{member?.email}</Text>
        <Small fontSize={11}>{organization?.owner?.id === member?.id && 'Organization Owner'}</Small>
      </S.Table.Cell>
      <S.Table.Cell>{member?.firstName}</S.Table.Cell>
      <S.Table.Cell>{member?.lastName}</S.Table.Cell>
      <S.Table.Cell>{member?.phone && formatPhone(member?.phone)}</S.Table.Cell>
      <S.Table.Cell>
        <RoleDropdown disabled={(user && user.id === member?.id) || !canEdit} allRoles={allRoles} user={member} />
      </S.Table.Cell>
      {canEdit && (
        <S.Table.Cell>
          <S.Button
            color="yellow"
            size="tiny"
            icon="edit"
            label="Edit"
            onClick={() => onModalOpen(ModalEnum.EDIT, member)}
          />

          {user && user.id !== member?.id && canEdit && (
            <S.Button color="red" size="tiny" icon="delete" label="Delete" onClick={() => setDeleteOpen(true)} />
          )}
        </S.Table.Cell>
      )}
      {isDeleteOpen && (
        <DeleteConfirmationModal
          onSuccess={() => setDeleteOpen(undefined)}
          onClose={() => setDeleteOpen(undefined)}
          delete={handleDeleteOrganizationUser}
        />
      )}
    </S.Table.Row>
  )
}
