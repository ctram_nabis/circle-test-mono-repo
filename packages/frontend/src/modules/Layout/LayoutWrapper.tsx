import { AppContainer } from 'components/AppContainer'
import { Body } from 'components/Body'
import { Content } from 'components/Content'
import { FullScreenLoader } from 'components/FullScreenLoader'
import { NavBar } from 'components/NavBar'
import { SideNav } from 'components/SideNav'
import { IS_PRODUCTION } from 'modules/App'
import { getToken } from 'modules/Auth/tokenHandlers'
import { Intercom } from 'modules/Intercom'
import { useRouter } from 'modules/Navigation'
import { OrganizationContext } from 'modules/Organization'
import { useUserContext } from 'modules/User'
import React, { useContext } from 'react'
import ReactGA from 'react-ga'
import { Redirect } from 'react-router-dom'

interface Props {
  children?: React.ReactNode
  requireAuth?: boolean
  sideNav?: boolean
  requireOrg?: boolean
}

export const LayoutWrapper = ({ requireAuth, children, sideNav, requireOrg }: Props) => {
  const { location, query } = useRouter()
  const token = getToken()
  const { organization, setOrganizationId } = useContext(OrganizationContext)
  const { user } = useUserContext()
  const { alias } = query as { alias: string | undefined }

  if (IS_PRODUCTION) {
    ReactGA.pageview(location.pathname + location.search)
  }

  if (requireAuth && (!token || !user)) {
    return <Redirect to="/sign-in" />
  }

  if ((requireOrg && alias && !organization) || organization?.alias !== alias) {
    const _organization = user?.organizations.find((org) => org.alias === alias)

    if (_organization) {
      setOrganizationId(_organization.id)
    } else {
      // return <Redirect to="/my-organizations" />
    }
  }

  return (
    <AppContainer>
      <NavBar />
      <Body>
        {sideNav && <SideNav />}
        <FullScreenLoader loading={!organization && requireOrg}>
          <Content>{children}</Content>
        </FullScreenLoader>
      </Body>
      <Intercom />
    </AppContainer>
  )
}
