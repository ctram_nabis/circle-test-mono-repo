import { S } from 'components'
import { Box } from 'components/core/src/Box'
import { HelpIcon } from 'components/HelpIcon'
import React from 'react'
import { TitleholderDropdown } from './TitleholderDropdown'

interface Props {
  titleholderId?: string
  setTitleholderId: any
  organization?: any
}

const TITLEHOLDER_TOOLTIP =
  'If you are a brand that works with partner manufacturers, your products may belong to multiple entities. Which license holding entity holds legal title to this product? I.E. who owns it?'

export function TitleholderDetails({ titleholderId, setTitleholderId, organization }: Props) {
  return (
    <S.Grid.Column width={16}>
      <S.Message attached>
        <S.Message.Header>
          <Box row center>
            <HelpIcon message={TITLEHOLDER_TOOLTIP} color="shade4" />
            <Box ml={2}>Who holds title to this product?</Box>
          </Box>
        </S.Message.Header>
      </S.Message>
      <S.Segment className="attached fluid segment">
        <TitleholderDropdown
          titleholderId={titleholderId}
          setTitleholderId={setTitleholderId}
          defaultAsOrg
          organization={organization}
        />
      </S.Segment>
    </S.Grid.Column>
  )
}
