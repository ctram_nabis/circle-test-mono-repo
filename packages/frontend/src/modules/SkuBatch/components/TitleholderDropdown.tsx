import { Box } from 'components/core/src/Box'
import { Dropdown } from 'components/Dropdown'
import { ErrorBox } from 'components/ErrorBox'
import { Loading } from 'components/Loading'
import { S } from 'components/SemanticComponents'
import { useGetTitleholdersQuery } from 'generated/graphql'
// import { useRouter } from 'modules/Navigation'
import { ActionLink } from 'modules/Order/containers/AdminOrdersPage/AdminOrdersPage'
import React, { useState } from 'react'
import { TITLEHOLDER_LABEL } from '../constants'

const mapSelectOptions = (elem) => ({
  key: elem.id,
  value: elem.id,
  text:
    elem.type === 'ORGANIZATION'
      ? `${elem.titleholderOrganization?.name} (Organization)`
      : `${elem.titleholderLicense?.legalEntityName} (${elem.titleholderLicense?.licenseNumber})`,
})

interface Props {
  titleholderId?: string
  setTitleholderId: any
  organization?: any
  defaultAsOrg?: boolean
}

export function TitleholderDropdown({
  titleholderId,
  setTitleholderId,
  organization: productOrganization,
  defaultAsOrg = false,
}: Props) {
  const [options, setOptions] = useState<any>([])
  // const router = useRouter()

  const { loading, error } = useGetTitleholdersQuery({
    variables: { organizationId: productOrganization?.id },
    skip: !productOrganization?.id,
    onCompleted: (d) => {
      const titleholders = d?.viewer?.getTitleholders
      const _options = (titleholders || []).map(mapSelectOptions)

      if (defaultAsOrg) {
        const productOrgId = productOrganization.id
        const orgTitleholder = (titleholders || []).find((t) => t?.titleholderOrganization?.id === productOrgId)
        if (orgTitleholder?.id) setTitleholderId(orgTitleholder.id)
      }

      setOptions(_options)
    },
  })

  return (
    <>
      <Loading loading={loading} />
      <ErrorBox error={error} />
      {!loading && !error && (
        <>
          <Dropdown
            search
            fluid
            selection
            placeholder={`Choose ${TITLEHOLDER_LABEL}...`}
            loading={loading}
            disabled={loading}
            value={titleholderId}
            onChange={(_e: any, _data: any) => {
              // TODO: fix event types
              const id = _data.value as string

              setTitleholderId(id)
            }}
            options={options}
          />
          <Box mt={3}>
            <S.Message>
              <S.Message.Header>Need to add or edit a seller?</S.Message.Header>
              <S.Message.Content>
                Go to {productOrganization?.name}'s{' '}
                <ActionLink
                  href={`/${productOrganization?.alias}/app/edit`}
                  target="_blank"
                  rel="noopener noreferrer"
                  // onClick={() => router.push(`/${productOrganization?.alias}/app/edit`)}
                >
                  Organization Settings
                </ActionLink>{' '}
                to create and edit licenses!
              </S.Message.Content>
            </S.Message>
          </Box>
        </>
      )}
    </>
  )
}
