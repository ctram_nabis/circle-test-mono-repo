import { Button, ErrorBox, H2 } from 'components'
import { Box } from 'components/core'
import { Scalars, useCreateSkuBatchMutation, useGetSkuQuery } from 'generated/graphql'
import { BatchDetails } from 'modules/Batch/components'
import logger from 'modules/Logger'
import { ProductDetails } from 'modules/Sku/components'
import { TitleholderDetails } from 'modules/SkuBatch/components'
import React, { Fragment, useState } from 'react'
import { ErrorType } from 'types'

interface Props {
  batchId: Scalars['ID'] | null
  productId: Scalars['ID']
  onSuccess?: () => void
}

export function SkuBatchConfirm({ batchId, productId, onSuccess }: Props) {
  const [createSkuBatch, { loading: _loading }] = useCreateSkuBatchMutation()
  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()

  const [titleholderId, setTitleholderId] = useState()

  const { data, loading, error } = useGetSkuQuery({ variables: { id: productId } })
  const sku = data && data.getSku

  const handleCreateSkuBatch = async () => {
    try {
      const response = await createSkuBatch({
        variables: {
          skuId: productId,
          batchId,
          titleholderId,
        },
        refetchQueries: ['InventorySkuBatchesTab'],
      })
      if (response?.errors) {
        setRequestErrors(response?.errors)
      } else {
        return onSuccess && onSuccess()
      }
    } catch (err) {
      logger.error(err)
      if (Array.isArray(err?.graphQLErrors)) return setRequestErrors(err?.graphQLErrors)
      return setCatchError(err?.graphQLErrors?.message)
    }
  }
  return (
    <Box p={10}>
      <ErrorBox errors={requestErrors} error={catchError} />
      <H2 mb="10px">Sku:</H2>
      <ProductDetails sku={sku} loading={loading} error={error} />
      {batchId ? (
        <Fragment>
          <H2 my="10px">Batch:</H2>
          <BatchDetails
            batchId={batchId}
            titleholderId={titleholderId}
            setTitleholderId={setTitleholderId}
            organization={sku?.organization}
          />
        </Fragment>
      ) : (
        <Fragment>
          <H2 my="10px">Batch: none</H2>
          <TitleholderDetails
            titleholderId={titleholderId}
            setTitleholderId={setTitleholderId}
            organization={sku?.organization}
          />
        </Fragment>
      )}

      <Button
        title="Create"
        icon="check"
        color="green"
        disabled={_loading}
        loading={_loading}
        onClick={handleCreateSkuBatch}
      />
    </Box>
  )
}
