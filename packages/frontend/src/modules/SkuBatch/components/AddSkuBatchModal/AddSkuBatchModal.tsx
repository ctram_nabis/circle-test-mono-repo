import { Button, ErrorBox, H2, S } from 'components'
import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { Scalars } from 'generated/graphql'
import { AddBatch, SelectBatchForm } from 'modules/Batch/components'
import { AddProduct, SelectProductForm } from 'modules/Sku/components'
import React, { Fragment, useEffect, useState } from 'react'
import { SkuBatchConfirm } from './SkuBatchConfirm'

/**
 * S.Modal which supports multiple flows for creating a skubatch
 *
 * Batch flow
 *  |-> select an existing batch <SelectBatchForm />
 *  |-> create a new batch <AddBatch />
 *  |-> set batch as null
 *
 * Product flow
 *  |-> select an existing product <SelectProductForm />
 *  |-> create a new product <AddProduct />
 *
 * SkuBatch
 *  |-> confirm and create SkuBatch <SkuBatchConfirm />
 *
 * Can go Batch -> Product => SkuBatch or
 *        Product -> Batch => SkuBatch
 *
 * Important to note Product (SKU) is required, but batch is not
 *
 */

type ScreensType = 'PRODUCT' | 'BATCH' | 'SKUBATCH'

interface Props {
  isOpen: boolean
  onOpen: () => void
  onClose: () => void
}

export function AddSkuBatchModal({ isOpen, onClose, onOpen }: Props) {
  const [batchId, setBatchId] = useState<string | null | undefined>(undefined)
  const [productId, setProductId] = useState<string | undefined>(undefined)

  const [prevScreen, setPrevScreen] = useState<ScreensType | undefined>()
  const [screen, setScreen] = useState<ScreensType>('BATCH')
  const [isSelecting, setIsSelecting] = useState<boolean>(true)

  const batchIsSet = batchId !== undefined
  const productIsSet = productId !== undefined

  const showBatchScreen = screen === 'BATCH' && !batchIsSet
  const showProductScreen = screen === 'PRODUCT' && !productIsSet
  const showSkuBatchScreen = screen === 'SKUBATCH' && batchIsSet && productIsSet

  const onBatchSuccess = (id: Scalars['ID']) => {
    setPrevScreen('BATCH')
    setBatchId(id)
  }
  const onProductSuccess = (id: Scalars['ID']) => {
    setPrevScreen('PRODUCT')
    setProductId(id)
  }

  useEffect(() => {
    if (prevScreen === 'BATCH' && batchIsSet) {
      if (!productIsSet) {
        setScreen('PRODUCT')
      } else {
        setScreen('SKUBATCH')
      }
    }
    if (prevScreen === 'PRODUCT' && productId) {
      if (!batchIsSet) {
        setScreen('BATCH')
      } else {
        setScreen('SKUBATCH')
      }
    }
  }, [batchId, productId, prevScreen, setScreen, productIsSet, batchIsSet])

  useEffect(() => {
    setIsSelecting(true)
  }, [screen])

  const handleBackClick = () => {
    if (prevScreen) {
      if (prevScreen === 'BATCH') {
        setBatchId(undefined)
      }
      if (prevScreen === 'PRODUCT') {
        setProductId(undefined)
      }
      setScreen(prevScreen)
      setPrevScreen(undefined)
    }
  }
  const isSelectingOrCreating = isSelecting ? 'Select' : 'Create'
  const stepHeader = showSkuBatchScreen ? 'Confirm' : isSelectingOrCreating
  const currentPageHeader = showBatchScreen ? 'Batch' : 'Product'
  return (
    <S.Modal open={isOpen} closeOnDimmerClicker={false} closeIcon onOpen={onOpen} onClose={onClose}>
      <S.Modal.Header>
        <Box display="flex" justifyContent="space-between">
          <H2>
            Create a SKU:{' '}
            <Text display="inline-block" fontWeight="normal">
              {stepHeader} {!showSkuBatchScreen && currentPageHeader}
            </Text>
          </H2>
          {!batchIsSet && !productIsSet && (
            <Button
              icon="shuffle"
              onClick={() => setScreen(screen === 'BATCH' ? 'PRODUCT' : 'BATCH')}
              label={`Start with ${screen === 'BATCH' ? 'product' : 'batch'}`}
            />
          )}
        </Box>
      </S.Modal.Header>
      {showBatchScreen ? (
        // Batch components
        isSelecting ? (
          <Box mx={4} my={2}>
            <SelectBatchForm setParentBatchId={onBatchSuccess} setIsSelecting={setIsSelecting} />
          </Box>
        ) : (
          <Fragment>
            <S.Header>Create a new batch</S.Header>
            <Button name="hand pointer" title="Select an existing batch" onClick={() => setIsSelecting(true)} />
            <AddBatch setParentBatchId={onBatchSuccess} />
          </Fragment>
        )
      ) : showProductScreen ? (
        // Product components
        isSelecting ? (
          <Box mx={4} my={2}>
            <SelectProductForm setParentProductId={onProductSuccess} setIsSelecting={setIsSelecting} />
          </Box>
        ) : (
          <Fragment>
            <Button name="hand pointer" title="Select an existing product" onClick={() => setIsSelecting(true)} />
            <AddProduct setParentProductId={onProductSuccess} />
          </Fragment>
        )
      ) : showSkuBatchScreen ? (
        // Final screen
        <SkuBatchConfirm productId={productId!} batchId={batchId!} onSuccess={onClose} />
      ) : (
        <ErrorBox />
      )}
      <S.Modal.Actions>
        {prevScreen && <S.Button icon="arrow left" onClick={() => handleBackClick()} />}
        <Button icon="x" title="Cancel" onClick={() => onClose()} />
      </S.Modal.Actions>
    </S.Modal>
  )
}
