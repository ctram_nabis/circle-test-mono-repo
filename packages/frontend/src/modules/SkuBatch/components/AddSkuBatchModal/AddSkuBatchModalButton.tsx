import React, { Fragment, useState } from 'react'

import { Button } from 'components'
import { AddSkuBatchModal } from './AddSkuBatchModal'

export function AddSkuBatchModalButton() {
  const [showModal, setShowModal] = useState<boolean>(false)

  return (
    <Fragment>
      {showModal && (
        <AddSkuBatchModal isOpen={showModal} onOpen={() => setShowModal(true)} onClose={() => setShowModal(false)} />
      )}
      <Button icon="plus" label="SKU" color="green" onClick={() => setShowModal(true)} data-test="button-add-sku" />
    </Fragment>
  )
}
