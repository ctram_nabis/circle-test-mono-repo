import { Button, H3, H5, S } from 'components'
import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { SkuBatch, useUpdateSkuBatchTitleholderMutation } from 'generated/graphql'
import { useAdminOverrideOrganization } from 'modules/Inventory/provider/useAdminOverrideOrganization'
import { TITLEHOLDER_LABEL } from 'modules/SkuBatch/constants'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import { TitleholderDropdown } from '../TitleholderDropdown'

interface Props {
  isOpen: boolean
  onOpen?: () => void
  onClose: () => void
  skuBatch: SkuBatch
}

export function TitleholderModal({ isOpen, onClose, onOpen, skuBatch }: Props) {
  const [titleholderId, setTitleholderId] = useState(skuBatch?.titleholder?.id)

  const [updateSkuBatchTitleholderMutation, { data, loading, error }] = useUpdateSkuBatchTitleholderMutation({
    variables: {
      skuBatchId: skuBatch?.id,
      titleholderId,
    },
    refetchQueries: ['InventorySkuBatchesTab'],
  })

  const { user } = useMe()
  const { adminOverrideOrganizationId } = useAdminOverrideOrganization()

  const { sku: product, batch } = skuBatch || {}
  const productName = product && product.name
  const productDescription =
    user.isAdmin && (!adminOverrideOrganizationId || adminOverrideOrganizationId === 'All Organizations')
      ? `${productName}, ${product?.organization?.name ?? 'err'}`
      : productName

  const skuBatchCode = `${product ? product.code : 'ERROR'}: ${batch ? batch.code : '---'}`

  return (
    <S.Modal open={isOpen} closeOnDimmerClicker={false} closeIcon onOpen={onOpen} onClose={onClose}>
      <S.Modal.Header>
        <Box display="flex" justifyContent="space-between">
          <H3>Update {TITLEHOLDER_LABEL}</H3>
        </Box>
      </S.Modal.Header>

      <S.Modal.Content>
        {error && <S.Message error content={error.message} />}
        {data?.updateSkuBatchTitleholder?.id && (
          <S.Message success header={`${TITLEHOLDER_LABEL} successfully updated!`} />
        )}
        <H5>SKU</H5>
        <Box row center mb={3}>
          <Text>{productDescription}</Text>
          <Box ml={2}>
            <S.Label>{skuBatchCode}</S.Label>
          </Box>
        </Box>

        <H5 mb={1}>{TITLEHOLDER_LABEL}</H5>
        <TitleholderDropdown
          titleholderId={titleholderId}
          setTitleholderId={setTitleholderId}
          organization={product?.organization}
        />
      </S.Modal.Content>

      <S.Modal.Actions>
        <Button
          color="green"
          title="Update"
          disabled={loading || !titleholderId}
          loading={loading}
          onClick={() => updateSkuBatchTitleholderMutation()}
        />
        <Button icon="x" title="Cancel" disabled={loading} loading={loading} onClick={() => onClose()} />
      </S.Modal.Actions>
    </S.Modal>
  )
}
