import { Box } from 'components/core/src/Box'
import { P } from 'components/P'
import { S } from 'components/SemanticComponents'
import {
  Scalars,
  SkuBatch,
  useDeleteSkuBatchMutation,
  useRestoreSkuBatchMutation,
  useUpdateSkuMutation,
} from 'generated/graphql'
import { CorrectionModalButton } from 'modules/Inventory/components/CorrectionModal'
import { FeedbackType } from 'modules/Inventory/containers/InventoryPage'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React from 'react'
import { Link } from 'react-router-dom'
import { Table } from 'semantic-ui-react'
import styled from 'styled-components'
import { ExpandedType } from '../containers/AllSkuBatchesTab'
import { AdminBatchDropdownToggle } from './AdminBatchDropdownToggle'
import { TitleholderModalToggle } from './TitleholderModalToggle'

// TODO: replace with reusable or make reusable
const [IncomingCell, AvailableCell, CountedCell, OutgoingCell, OversoldCell] = [
  styled(Table.Cell)`
    white-space: nowrap;
    color: #1298c9;
    background: #f7fdff;
  `,
  styled(Table.Cell)`
    white-space: nowrap;
    color: #0eae38;
    background: #fafffa;
  `,
  styled(Table.Cell)`
    white-space: nowrap;
    color: grey;
    background: #fafafa;
  `,
  styled(Table.Cell)`
    white-space: nowrap;
    color: #ff9b0f;
    background: #fff9f0;
  `,
  styled(Table.Cell)`
    white-space: nowrap;
    color: #e32d2d;
    background: #fffafa;
  `,
]

const refetchQueries = ['AllInventory', 'AllAdminInventory', 'AllBatches']

interface Props {
  adminOverrideOrganizationId?: Scalars['ID']
  key: string
  expanded: ExpandedType
  skuBatch: SkuBatch
  isArchived: boolean
  onFeedback: (f: FeedbackType) => void
}

export function SkuBatchRow({ expanded, isArchived, onFeedback, skuBatch, adminOverrideOrganizationId }: Props) {
  const { user } = useMe()
  const { canCreate } = useOrganization()

  const router = useRouter()

  const [updateSkuMutation] = useUpdateSkuMutation()
  const [restoreSkuBatchMutation] = useRestoreSkuBatchMutation()
  const [deleteSkuBatchMutation] = useDeleteSkuBatchMutation()

  const { sku: product, batch } = skuBatch || {}
  const productName = product && product.name
  const productDescription =
    user.isAdmin && !adminOverrideOrganizationId
      ? `${productName}, ${product?.organization?.name ?? 'err'}`
      : productName

  const skuBatchCode = `${product ? product.code : 'ERROR'}: ${batch ? batch.code : '---'}`

  const onUndoButtonClick = async () => {
    const undoProductConfirmText =
      'It looks like the whole product is archived. Are you sure you want to restore this SKU? This will restore the entire product.'

    // if parent product is archived, it needs to be unarchived
    if (skuBatch.deletedAt) {
      const { errors } = await restoreSkuBatchMutation({ variables: { id: skuBatch.id }, refetchQueries })

      if (errors) {
        onFeedback({
          content: `There was an issue un-archiving ${product.name || ''}, ${skuBatchCode}`,
          error: true,
          header: 'Un-Archive SKU',
          success: false,
        })
      }
      onFeedback({
        content: `${product.name || ''}, ${skuBatchCode} has been un-archived!`,
        error: false,
        header: 'Un-Archive SKU',
        success: true,
      })
      return
    }

    if (product && product.isArchived) {
      if (!window.confirm(undoProductConfirmText)) {
        return
      }

      const { errors } = await updateSkuMutation({
        variables: { input: { id: product.id, isArchived: false } },
        refetchQueries,
      })

      if (errors) {
        onFeedback({
          content: `There was an issue un-archiving ${product.name || ''}, ${skuBatchCode}`,
          error: true,
          header: 'Un-Archive Product',
          success: false,
        })
      }

      onFeedback({
        content: `${product.name || ''}, ${skuBatchCode} has been un-archived!`,
        error: false,
        header: 'Un-Archive Product',
        success: true,
      })
    }

    // if the row is soft deleted, we need to restore it
  }

  const onArchiveButtonClick = async () => {
    const archiveConfirmText = `Are you sure you want to archive this SKU? (${skuBatch?.sku?.code ?? 'err'})`

    if (!window.confirm(archiveConfirmText)) {
      return
    }

    const { errors } = await deleteSkuBatchMutation({
      variables: { id: skuBatch.id },
      refetchQueries,
    })

    if (errors) {
      onFeedback({
        content: `There was an issue archiving ${skuBatch?.sku?.name ?? 'err'}`,
        error: true,
        header: 'Archive SKU',
        success: false,
      })
    } else {
      onFeedback({
        content: `${skuBatch?.sku?.code ?? 'err'} has been archived!`,
        error: false,
        header: 'Archive SKU',
        success: true,
      })
    }
  }

  const renderInventoryValueCells = () => {
    const prependQuantityWithSign = (quantity: number, sign: string) => {
      if (!quantity || quantity === 0) {
        return '0'
      }
      return `${sign}${quantity.toString()}`
    }

    const { id, inventoryValues } = skuBatch

    const { LA, OAK } = inventoryValues

    const [totalCount, totalIncoming, totalOutgoing, totalAvailable] = [
      LA.counted + OAK.counted,
      LA.incoming + OAK.incoming,
      LA.outgoing + OAK.outgoing,
      LA.available + OAK.available,
    ]

    const [counted, incoming, outgoing, available] = [
      [
        <CountedCell key="sku-total-counted">
          <strong>{totalCount}</strong>
        </CountedCell>,
      ],
      [
        <IncomingCell key={`sku-total-incoming-${id}`}>
          <strong>{prependQuantityWithSign(totalIncoming, '+')}</strong>
        </IncomingCell>,
      ],
      [
        <OutgoingCell key={`sku-total-outgoing-${id}`}>
          <strong>{prependQuantityWithSign(totalOutgoing, '-')}</strong>
        </OutgoingCell>,
      ],
      [
        totalAvailable < 0 ? (
          <OversoldCell key={`sku-total-available-${id}`}>
            <strong>{totalAvailable}</strong>
          </OversoldCell>
        ) : (
          <AvailableCell key={`sku-total-available-${id}`}>
            <strong>{totalAvailable}</strong>
          </AvailableCell>
        ),
      ],
    ]

    if (expanded.oak) {
      counted.push(
        <CountedCell key={`sku-oak-counted-${id}`}>
          <P>{OAK.counted}</P>
        </CountedCell>,
      )
      incoming.push(
        <IncomingCell key={`sku-oak-incoming-${id}`}>
          <P>{prependQuantityWithSign(OAK.incoming, '+')}</P>
        </IncomingCell>,
      )
      outgoing.push(
        <OutgoingCell key={`sku-oak-outgoing-${id}`}>
          <P>{prependQuantityWithSign(OAK.outgoing, '-')}</P>
        </OutgoingCell>,
      )
      if (OAK.available < 0) {
        available.push(
          <OversoldCell key={`sku-oak-available-${id}`}>
            <P>{OAK.available}</P>
          </OversoldCell>,
        )
      } else {
        available.push(
          <AvailableCell key={`sku-oak-available-${id}`}>
            <P>{OAK.available}</P>
          </AvailableCell>,
        )
      }
    }

    if (expanded.la) {
      counted.push(
        <CountedCell key={`sku-la-counted-${id}`}>
          <P>{LA.counted}</P>
        </CountedCell>,
      )
      incoming.push(
        <IncomingCell key={`sku-la-incoming-${id}`}>
          <P>{prependQuantityWithSign(LA.incoming, '+')}</P>
        </IncomingCell>,
      )
      outgoing.push(
        <OutgoingCell key={`sku-la-outgoing-${id}`}>
          <P>{prependQuantityWithSign(LA.outgoing, '-')}</P>
        </OutgoingCell>,
      )
      if (LA.available < 0) {
        available.push(
          <OversoldCell key={`sku-la-available-${id}`}>
            <P>{LA.available}</P>
          </OversoldCell>,
        )
      } else {
        available.push(
          <AvailableCell key={`sku-la-available-${id}`}>
            <P>{LA.available}</P>
          </AvailableCell>,
        )
      }
    }

    return [...counted, ...incoming, ...outgoing, ...available]
  }

  const showActions = canCreate.inventory || user.isAdmin
  const disableButtons = !showActions

  const linkToSkuBatchPage = `${router.pathname}/${skuBatch.id}`

  return (
    <Table.Row data-test="sku-batch-row">
      <Table.Cell>
        <Link to={linkToSkuBatchPage} target="_blank">
          {skuBatchCode}
        </Link>
      </Table.Cell>
      <Table.Cell>{productDescription}</Table.Cell>
      <Table.Cell>
        {user.isAdmin ? (
          <AdminBatchDropdownToggle skuBatch={skuBatch} onFeedback={onFeedback} />
        ) : (
          <p>{(batch && batch.code) || '---'}</p>
        )}
      </Table.Cell>
      <Table.Cell>
        {batch?.license && (
          <>
            {batch?.license?.legalEntityName}
            <br />
            <small>{batch?.license?.licenseNumber}</small>
          </>
        )}
      </Table.Cell>
      <Table.Cell>
        <TitleholderModalToggle skuBatch={skuBatch} />
      </Table.Cell>
      <Table.Cell>
        <Box center>
          {batch ? (
            // TODO: replace <a />
            batch.manifestGDriveFileId ? (
              <a
                target="_blank"
                rel="noopener noreferrer"
                href={`https://drive.google.com/open?id=${batch.manifestGDriveFileId}`}
              >
                <S.Icon size="large" name="file pdf outline" />
              </a>
            ) : (
              <S.Popup
                content={<em>COA has not been uploaded, please contact ops@nabis.com</em>}
                trigger={<S.Icon name="warning" color="yellow" />}
              />
            )
          ) : (
            '---'
          )}
        </Box>
      </Table.Cell>
      {renderInventoryValueCells()}
      {showActions && (
        <Table.Cell>
          {user.isAdmin && <CorrectionModalButton skuBatch={skuBatch} />}
          {canCreate.inventory &&
            (!isArchived ? (
              <S.Popup
                content={`This will archive SKU: ${skuBatch?.sku?.code ??
                  'err'}, so all SKU-Batch pairs related to ${skuBatch?.sku?.code ?? 'err'} will be archived.`}
                trigger={
                  <S.Button
                    basic
                    color="yellow"
                    icon={disableButtons ? 'lock' : 'archive'}
                    disabled={disableButtons}
                    onClick={() => onArchiveButtonClick()}
                  />
                }
              />
            ) : (
              <S.Button icon="undo" onClick={() => onUndoButtonClick()} />
            ))}
        </Table.Cell>
      )}
    </Table.Row>
  )
}
