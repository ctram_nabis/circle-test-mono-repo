import { Box } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { S } from 'components/SemanticComponents'
import { SkuBatch } from 'generated/graphql'
import { FeedbackType } from 'modules/Inventory'
import React, { Fragment, useState } from 'react'
import styled from 'styled-components'
import { AdminBatchDropdown } from './AdminBatchDropdown'

const StyledText = styled(Text)`
  display: inline-block;
  width: 95px;
  overflow-wrap: break-word;
`

interface Props {
  skuBatch: SkuBatch
  onFeedback: (f: FeedbackType) => void
}

export function AdminBatchDropdownToggle({ onFeedback, skuBatch }: Props) {
  const [showDropdown, setShowDropdown] = useState<boolean>()
  const { batch } = skuBatch

  const onClose = () => setShowDropdown(false)
  const onError = () => setTimeout(onClose, 2000)

  return (
    <Fragment>
      <Box inline={true}>
        {showDropdown ? (
          <Box inline paddingRight="1">
            <AdminBatchDropdown
              skuBatch={skuBatch}
              onBlur={onClose}
              onError={onError}
              onFeedback={onFeedback}
              onSuccess={onClose}
            />
          </Box>
        ) : (
          <StyledText>{batch?.code || '---'}</StyledText>
        )}
        <S.Button
          size="tiny"
          icon={showDropdown ? 'x' : 'pencil'}
          basic
          onClick={() => setShowDropdown(!showDropdown)}
        />
      </Box>
    </Fragment>
  )
}
