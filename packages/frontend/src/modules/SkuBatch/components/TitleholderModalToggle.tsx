import { Box } from 'components/core/src/Box'
import { S } from 'components/SemanticComponents'
import { SkuBatch } from 'generated/graphql'
import React, { Fragment, useState } from 'react'
import { TitleholderModal } from './TitleholderModal/TitleholderModal'

interface Props {
  skuBatch: SkuBatch
  // onFeedback?: (f: FeedbackType) => void
}

export function TitleholderModalToggle({
  // onFeedback,
  skuBatch,
}: Props) {
  const [showModal, setShowModal] = useState<boolean>()
  const {
    // batch,
    titleholder,
  } = skuBatch

  const onClose = () => setShowModal(false)
  // const onError = () => setTimeout(onClose, 2000)

  let titleholderDetails

  if (titleholder) {
    const isLicense = titleholder.type === 'LICENSE'
    titleholderDetails = {
      name: isLicense ? titleholder?.titleholderLicense?.legalEntityName : titleholder?.titleholderOrganization?.name,
      licenseNumber: isLicense ? titleholder?.titleholderLicense?.licenseNumber : '',
      // : titleholder?.titleholderOrganization?.licenseNumber,
    }
  }

  return (
    <Fragment>
      <Box space="between">
        {showModal && <TitleholderModal isOpen onClose={onClose} skuBatch={skuBatch} />}

        <Box>
          {titleholderDetails && (
            <>
              {titleholderDetails?.name}
              <br />
              <small>{titleholderDetails?.licenseNumber}</small>
            </>
          )}
        </Box>

        <S.Button size="tiny" icon={showModal ? 'x' : 'pencil'} basic onClick={() => setShowModal(!showModal)} />
      </Box>
    </Fragment>
  )
}
