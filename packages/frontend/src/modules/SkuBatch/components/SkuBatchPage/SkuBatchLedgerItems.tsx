import { Loading, Pagination } from 'components'
import { InventoryLedgerItem, useAllInventoryItemsQuery } from 'generated/graphql'
import { usePageInfo } from 'hooks'
import { useRouter } from 'modules/Navigation'
import React, { Fragment, useEffect } from 'react'
import { Header, Segment, Table } from 'semantic-ui-react'
import { FiltersState } from '../../containers/SkuBatchInventoryLedgerTabs'
import { SkuBatchLedgerItemHeader } from './SkuBatchLedgerItemHeader'
import { SkuBatchLedgerItemRow } from './SkuBatchLedgerItemRow'

const COLSPAN = '7'

interface Props {
  warehouseId: string
  filter?: (key: string, value: string) => void
  filters: FiltersState
}

export function SkuBatchLedgerItems({ warehouseId, filters }: Props) {
  const router = useRouter()
  const { pageInfo, setPage } = usePageInfo()

  const skuBatchId = router.query.skuBatchId

  const { data, loading, error, ...allInventoryItems } = useAllInventoryItemsQuery({
    skip: !skuBatchId,
    variables: {
      skuBatchId,
      warehouseId,
      pageInfo: { ...filters, ...pageInfo },
    },
  })

  useEffect(() => {
    allInventoryItems.refetch()
  }, [allInventoryItems, pageInfo])

  const items = data?.viewer?.allInventoryItems?.results ?? []
  const totalNumPages = data?.viewer?.allInventoryItems?.pageInfo?.totalNumPages ?? 0

  return (
    <Fragment>
      <Loading loading={loading} />
      {/* <ErrorBox error={error} /> */}
      {items.length > 0 && (
        <Pagination
          onPageChange={(_e, d) => setPage(d.activePage)}
          activePage={pageInfo.page}
          totalNumPages={totalNumPages}
          siblingRange={3}
        />
      )}
      <Table attached celled selectable>
        <SkuBatchLedgerItemHeader />
        <Table.Body>
          {loading || error ? null : items.length > 0 ? (
            items.map((item, i) => <SkuBatchLedgerItemRow item={item as InventoryLedgerItem} key={`ledger-${i}`} />)
          ) : (
            <Table.Row>
              <Table.Cell colSpan={COLSPAN}>
                <Segment placeholder className="flex-style">
                  <Header>No data available</Header>
                </Segment>
              </Table.Cell>
            </Table.Row>
          )}
        </Table.Body>
      </Table>
    </Fragment>
  )
}
