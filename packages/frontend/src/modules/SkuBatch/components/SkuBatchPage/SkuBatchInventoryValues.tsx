import { InventoryValues } from 'generated/graphql'
import React, { FunctionComponent } from 'react'
import { Grid, Header } from 'semantic-ui-react'
import styled, { AnyStyledComponent } from 'styled-components'
import { getColor, lightColor } from 'system'

const SubHeader = styled(Header)`
  margin: 0 0 0 0 !important;
`

const makeCountSubHeader = (component: AnyStyledComponent | FunctionComponent) => styled(component)`
  font-size: 1em !important;
  margin-bottom: 1em !important;
`
const makeTopHeader = (component: AnyStyledComponent | FunctionComponent) => styled(component)`
  margin-top: 1em !important;
  padding-bottom: 0.1em !important;
  margin-bottom: 0.5em !important;
  font-size: 1em !important;
  border-bottom: 0.05em dashed #d1d1d1 !important;
`

const BlueGridColumn = styled(Grid.Column)`
  color: ${(props) => getColor('accent1', props)} !important;
  background: ${(props) => lightColor('accent1')(props)} !important;
`
const BlueCountHeader = styled(SubHeader)`
  color: ${(props) => getColor('accent1', props)} !important;
  background: ${(props) => lightColor('accent1')(props)} !important;
`
const BlueHeader = makeTopHeader(BlueCountHeader)
const BlueCountSubHeader = makeCountSubHeader(BlueCountHeader)

const GreenGridColumn = styled(Grid.Column)`
  color: ${(props) => getColor('success', props)} !important;
  background: ${(props) => lightColor('success')(props)} !important;
`
const GreenCountHeader = styled(SubHeader)`
  color: ${(props) => getColor('success', props)} !important;
  background: ${(props) => lightColor('success')(props)} !important;
`
const GreenHeader = makeTopHeader(GreenCountHeader)
const GreenCountSubHeader = makeCountSubHeader(GreenCountHeader)

const GreyGridColumn = styled(Grid.Column)`
  color: ${(props) => getColor('shade4', props)} !important;
  background: ${(props) => lightColor('shade4')(props)} !important;
`
const GreyCountHeader = styled(SubHeader)`
  color: ${(props) => getColor('shade4', props)} !important;
  background: ${(props) => lightColor('shade4')(props)} !important;
`
const GreyHeader = makeTopHeader(GreyCountHeader)
const GreyCountSubHeader = makeCountSubHeader(GreyCountHeader)

const OrangeGridColumn = styled(Grid.Column)`
  color: ${(props) => getColor('accent3', props)} !important;
  background: ${(props) => lightColor('accent3')(props)} !important;
`
const OrangeCountHeader = styled(SubHeader)`
  color: ${(props) => getColor('accent3', props)} !important;
  background: ${(props) => lightColor('accent3')(props)} !important;
`
const OrangeHeader = makeTopHeader(OrangeCountHeader)
const OrangeCountSubHeader = makeCountSubHeader(OrangeCountHeader)

interface Props {
  inventoryValues: InventoryValues
}

export const SkuBatchInventoryValues = ({ inventoryValues }: Props) => {
  const { LA, OAK } = inventoryValues

  const [countedTotal, outgoingTotal, incomingTotal, availableTotal] = [
    OAK.counted + LA.counted,
    OAK.outgoing + LA.outgoing,
    OAK.incoming + LA.incoming,
    OAK.available + LA.available,
  ]

  return (
    <Grid.Row divided>
      <GreyGridColumn>
        <GreyHeader as="h4">Counted</GreyHeader>
        <GreyCountHeader as="h3">
          <strong>{countedTotal} Total</strong>
        </GreyCountHeader>
        <GreyCountSubHeader>{`${OAK.counted} OAK | ${LA.counted} LA`}</GreyCountSubHeader>
      </GreyGridColumn>
      <BlueGridColumn>
        <BlueHeader as="h4">Incoming</BlueHeader>
        <BlueCountHeader as="h3">
          <strong>{incomingTotal} Total</strong>
        </BlueCountHeader>
        <BlueCountSubHeader>{`${OAK.incoming} OAK | ${LA.incoming} LA`}</BlueCountSubHeader>
      </BlueGridColumn>
      <OrangeGridColumn>
        <OrangeHeader as="h4">Outgoing</OrangeHeader>
        <OrangeCountHeader as="h3">
          <strong>{outgoingTotal} Total</strong>
        </OrangeCountHeader>
        <OrangeCountSubHeader>{`${OAK.outgoing} OAK | ${LA.outgoing} LA`}</OrangeCountSubHeader>
      </OrangeGridColumn>
      <GreenGridColumn>
        <GreenHeader as="h4">Available</GreenHeader>
        <GreenCountHeader as="h3">
          <strong>{availableTotal} Total</strong>
        </GreenCountHeader>
        <GreenCountSubHeader>{`${OAK.available} OAK | ${LA.available} LA`}</GreenCountSubHeader>
      </GreenGridColumn>
    </Grid.Row>
  )
}
