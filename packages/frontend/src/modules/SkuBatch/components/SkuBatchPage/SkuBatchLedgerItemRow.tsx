import { Box } from 'components/core/src/Box'
import { S } from 'components/SemanticComponents'
import { InventoryLedgerItem } from 'generated/graphql'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import moment from 'moment-timezone'
import React from 'react'
import { Table } from 'semantic-ui-react'
import styled from 'styled-components'
import { getColor, lightColor } from 'system'

const GreenCell = styled(Table.Cell)`
  color: ${(props) => getColor('success', props)} !important;
  background: ${(props) => lightColor('success')(props)} !important;
`
const RedCell = styled(Table.Cell)`
  color: ${(props) => getColor('error', props)} !important;
  background: ${(props) => lightColor('error')(props)} !important;
`

interface Props {
  item: InventoryLedgerItem
}

export const SkuBatchLedgerItemRow = ({ item }: Props) => {
  const { endCounted, notes, order, startCounted, transactionType, updatedAt } = item
  const { organization } = useOrganization()
  const router = useRouter()

  const changeCounted = endCounted - startCounted
  const changeQtyString = `${changeCounted && changeCounted >= 0 ? '+' : ''}${changeCounted}`

  const countMessage = (
    <Box>
      <strong>
        {`${startCounted} `}
        <S.Icon name="arrow right" />
        {endCounted}
        {` (${changeQtyString})`}
      </strong>
    </Box>
  )

  const ColorCell = changeCounted && changeCounted < 0 ? RedCell : GreenCell

  const goToOrder = (orderId: string) => {
    router.push({
      pathname: `/${organization.alias}/app/orders/${orderId}`,
      state: { orderId, editDraft: false },
    })
  }

  return (
    <Table.Row>
      <Table.Cell>{moment(updatedAt).format('MM/DD/YY, HH:mma')}</Table.Cell>
      <ColorCell>{countMessage}</ColorCell>
      <ColorCell>
        <strong>{transactionType}</strong>
      </ColorCell>
      {/** TODO: this <a /> needs to be replaced */}
      <Table.Cell>
        {order?.id ? (
          <a onClick={() => goToOrder(order.id)} href="#/">
            {order.number}
          </a>
        ) : (
          <S.Icon name="exclamation circle" color="red" />
        )}
      </Table.Cell>
      <Table.Cell>{notes ? notes : '---'}</Table.Cell>
    </Table.Row>
  )
}
