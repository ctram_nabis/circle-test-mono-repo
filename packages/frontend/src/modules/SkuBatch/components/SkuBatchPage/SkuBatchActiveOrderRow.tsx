import { S } from 'components'
import { Box } from 'components/core/src/Box'
import { Link } from 'components/Link'
import { ManifestLink } from 'components/ManifestLink'
import { Order } from 'generated/graphql'
import { useRouter } from 'modules/Navigation'
import {
  ORDER_ACTION_DESC,
  ORDER_ACTIONS,
  ORDER_ACTIONS_INCOMING,
  ORDER_ACTIONS_OUTGOING,
  ORDER_STATUS_DESC,
} from 'modules/Order/constants'
import { useOrganization } from 'modules/Organization'
import { getWarehouseKeyFromLicensedLocationId, getWarehouseKeyFromWarehouseId } from 'modules/Warehouse'
import moment from 'moment-timezone'
import React from 'react'
import { Table } from 'semantic-ui-react'
import styled from 'styled-components'
import { getColor, lightColor } from 'system'
import { oc } from 'ts-optchain'
import { unwrap } from 'utils'

const GreyCell = styled(Table.Cell)`
  color: ${(props) => getColor('shade5', props)};
  background-color: ${lightColor('shade5')};
`
const BlueCell = styled(Table.Cell)`
  color: ${(props) => getColor('accent1', props)};
  background-color: ${lightColor('accent1')};
`
const OrangeCell = styled(Table.Cell)`
  color: ${(props) => getColor('accent2', props)};
  background-color: ${lightColor('accent2')};
`

interface Props {
  order: Order
  skuBatchId: string
  refetchForManifestGenerate: (v: any) => Promise<any>
}

export const SkuBatchActiveOrderRow = ({ order, skuBatchId }: Props) => {
  const router = useRouter()
  const { action, date, id, manifestGDriveFileId, number: orderNumber, status } = order
  const { organization } = useOrganization()
  const { alias } = organization

  const matchingLineItems =
    oc(order)
      .lineItems([])
      .filter((lineItem) => oc(lineItem).skuBatch.id() === skuBatchId) || []
  const totalQuantity = matchingLineItems.reduce((total, lineItem) => total + lineItem.quantity, 0)

  // transfer grey, incoming blue, outgoing orange
  const ColorCell =
    action === ORDER_ACTIONS.INTERNAL_TRANSFER
      ? GreyCell
      : ORDER_ACTIONS_INCOMING.includes(action)
      ? BlueCell
      : ORDER_ACTIONS_OUTGOING.includes(action)
      ? OrangeCell
      : Table.Cell

  const renderCount = () => {
    const warehouseId = unwrap(oc(order).warehouse.id(''))
    const fromWarehouse = getWarehouseKeyFromWarehouseId(warehouseId)

    if (action === 'INTERNAL_TRANSFER') {
      const licensedLocationId = unwrap(oc(order).site.licensedLocation.id(''))
      const toWarehouse = getWarehouseKeyFromLicensedLocationId(licensedLocationId)

      return (
        <Box>
          <Box>
            <strong>+{totalQuantity}</strong>
            <strong> {toWarehouse}</strong>
          </Box>
          <Box>
            <strong>-{totalQuantity}</strong>
            <strong> {fromWarehouse}</strong>
          </Box>
        </Box>
      )
    }

    if (ORDER_ACTIONS_INCOMING.includes(action)) {
      return (
        <Box>
          <strong>+{totalQuantity}</strong>
          <strong> {fromWarehouse}</strong>
        </Box>
      )
    } else if (ORDER_ACTIONS_OUTGOING.includes(action)) {
      return (
        <Box>
          <strong>-{totalQuantity}</strong>
          <strong> {fromWarehouse}</strong>
        </Box>
      )
    }

    return <Box>{`${totalQuantity}`}</Box>
  }

  const warehouseName = order.warehouse?.site?.name
  const locName = order.site?.licensedLocation?.name

  const onManifestGenerateSuccess = () => {}

  return (
    <Table.Row key={id}>
      <Table.Cell>{moment(date).format('MM/DD/YY')}</Table.Cell>
      <Table.Cell>
        {order?.id ? (
          <Link
            onClick={() => {
              router.push({
                pathname: `/${alias}/app/orders/${order.id}`,
                state: {
                  orderId: order.id,
                  editDraft: false,
                },
              })
            }}
          >
            {orderNumber}
          </Link>
        ) : (
          <S.Icon name="exclamation circle" color="red" />
        )}
      </Table.Cell>
      <ColorCell>{warehouseName}</ColorCell>
      <ColorCell>{locName}</ColorCell>
      <ColorCell>{renderCount()}</ColorCell>
      <ColorCell>{ORDER_ACTION_DESC[action]}</ColorCell>
      <Table.Cell>{ORDER_STATUS_DESC[status]}</Table.Cell>
      <Table.Cell textAlign="center">
        <ManifestLink
          manifestGDriveFileId={manifestGDriveFileId}
          orderId={id}
          onSuccess={onManifestGenerateSuccess}
          refetchQueries={['ActiveOrdersForSkuBatch']}
        />
      </Table.Cell>
    </Table.Row>
  )
}
