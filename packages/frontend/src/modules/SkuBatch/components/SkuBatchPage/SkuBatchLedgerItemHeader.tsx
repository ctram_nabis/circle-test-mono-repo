import { Icon } from 'components'
import { Box } from 'components/core'
import React from 'react'
import { Table } from 'semantic-ui-react'

export const SkuBatchLedgerItemHeader = () => (
  <Table.Header>
    <Table.Row>
      <Table.HeaderCell>Time</Table.HeaderCell>
      <Table.HeaderCell>
        <Box>
          {` Start `}
          <Icon name="arrowRight" />
          End ({`\u0394 Quantity`})
        </Box>
      </Table.HeaderCell>
      <Table.HeaderCell>Type</Table.HeaderCell>
      <Table.HeaderCell>Order</Table.HeaderCell>
      <Table.HeaderCell>Notes</Table.HeaderCell>
    </Table.Row>
  </Table.Header>
)
