import React from 'react'
import { Table } from 'semantic-ui-react'

export const SkuBatchActiveOrderHeader = () => (
  <Table.Header>
    <Table.Row>
      <Table.HeaderCell>Date</Table.HeaderCell>
      <Table.HeaderCell>Number</Table.HeaderCell>
      <Table.HeaderCell>Nabis Warehouse</Table.HeaderCell>
      <Table.HeaderCell>Pickup/Dropoff Location</Table.HeaderCell>
      <Table.HeaderCell>{`\u0394 Quantity`}</Table.HeaderCell>
      <Table.HeaderCell>Type</Table.HeaderCell>
      <Table.HeaderCell>Status</Table.HeaderCell>
      <Table.HeaderCell textAlign="center">Manifest</Table.HeaderCell>
    </Table.Row>
  </Table.Header>
)
