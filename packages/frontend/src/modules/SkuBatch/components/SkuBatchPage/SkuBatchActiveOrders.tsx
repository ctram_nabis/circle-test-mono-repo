import { Order, useActiveOrdersForSkuBatchQuery } from 'generated/graphql'
import React from 'react'
import { Header, Loader, Message, Segment, Table } from 'semantic-ui-react'
import { SkuBatchActiveOrderHeader } from './SkuBatchActiveOrderHeader'
import { SkuBatchActiveOrderRow } from './SkuBatchActiveOrderRow'

const COLSPAN = '8'
interface Props {
  skuBatchId: string
}

export const SkuBatchActiveOrders = ({ skuBatchId }: Props) => {
  const { data, loading, error, refetch } = useActiveOrdersForSkuBatchQuery({ variables: { skuBatchId } })

  const orders = data && data.getActiveOrdersForSkuBatch

  return (
    <Table celled>
      <SkuBatchActiveOrderHeader />
      <Table.Body>
        <Loader active={loading} inline="centered" size="large">
          Loading...
        </Loader>
        {/* <ErrorBox error={error} /> */}

        {!loading && !error && orders && orders.length < 1 ? (
          <Table.Row>
            <Table.Cell colSpan={COLSPAN}>
              <Segment placeholder className="flex-style">
                <Header>No data available</Header>
              </Segment>
            </Table.Cell>
          </Table.Row>
        ) : orders ? (
          orders.map((order) => (
            <SkuBatchActiveOrderRow
              key={order.id}
              order={order as Order}
              refetchForManifestGenerate={refetch}
              skuBatchId={skuBatchId}
            />
          ))
        ) : (
          error && <Message negative content="Oops, something went wrong..." />
        )}
      </Table.Body>
    </Table>
  )
}
