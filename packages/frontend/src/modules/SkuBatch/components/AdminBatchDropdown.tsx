import { Dropdown } from 'components'
import { Box } from 'components/core/src/Box'
import { Batch, Sku, SkuBatch, useAllBatchesForSkuBatchModalQuery, useUpdateSkuBatchMutation } from 'generated/graphql'
import { FeedbackType } from 'modules/Inventory'
import { useMe } from 'modules/User'
import React, { useState } from 'react'

const getReassignBatchText = (batch?: Batch, sku?: Sku) => {
  return `Are you sure you want to assign BATCH:${!Boolean(batch) ? 'None' : batch?.code} to Product:${sku?.code}?`
}

interface Props {
  skuBatch: SkuBatch
  onFeedback: (f: FeedbackType) => void
  onSuccess?: () => void
  onError?: () => void
  onBlur?: () => void
}

export const AdminBatchDropdown = ({ onBlur, onError, onFeedback, onSuccess, skuBatch }: Props) => {
  const [submissionError, setSubmissionError] = useState<boolean>(false)

  const { user } = useMe()
  const { batch, sku: product } = skuBatch

  const organizationId = product.organization.id

  const { data, loading, error } = useAllBatchesForSkuBatchModalQuery({
    variables: { organizationId },
  })

  const batches = data?.viewer?.allBatches?.results ?? []

  const [updateSkuBatchMutation, { loading: mutationLoading }] = useUpdateSkuBatchMutation()

  if (!batches || !batches.length || !user.isAdmin) {
    return <p>{batch && batch.code ? batch.code : '---'}</p>
  }

  const handleDropdownChange = async (_batchId: string) => {
    const selectedBatch = batches.find((_batch) => _batch.id === _batchId) as Batch | undefined
    const reassignWarnText =
      _batchId === 'none'
        ? `Are you sure you want to unassign ${batch?.code} from Product: ${product?.code}`
        : getReassignBatchText(selectedBatch as Batch, product)

    if (!window.confirm(reassignWarnText)) {
      return
    }

    const { code, id: batchId } = selectedBatch || {}

    try {
      if (!batchId && _batchId !== 'none') {
        throw new Error('Oops, something went wrong')
      }

      const { errors } = await updateSkuBatchMutation({
        variables: {
          batchId: _batchId !== 'none' ? batchId : null,
          skuBatchId: skuBatch.id,
          skuId: product.id,
        },
        refetchQueries: ['AllInventory', 'AllAdminInventory', 'AllBatches'],
      })

      if (errors) {
        setSubmissionError(true)
        onFeedback({
          content: 'Validation error',
          error: true,
          header: 'Failed to assign a SKU a Batch',
          success: false,
        })
      } else {
        onFeedback({
          content: `${product && product.code} has been assigned ${code || 'None'}!`,
          error: false,
          header: 'Assign SKU a Batch',
          success: true,
        })

        if (onSuccess) {
          onSuccess()
        }
      }
    } catch (error) {
      setSubmissionError(true)
      onFeedback({
        content: error.message,
        error: true,
        header: 'Failed to assign a SKU a Batch',
        success: false,
      })
      if (onError) {
        onError()
      }
    }
  }

  const batchOptions = batches
    .filter((possibleDuplicate, i, arr) => arr.map((_batch) => _batch.id).indexOf(possibleDuplicate.id) === i)
    .map((_batch, i) => ({
      key: _batch.id || `batch-option-${i}`,
      text: `${_batch.code}`,
      value: _batch.id,
    }))

  if (error) {
    return <Box color="red">{error.message}</Box>
  } else if (submissionError) {
    return <Box color="red">Error assigning</Box>
  }

  return (
    <Dropdown
      onBlur={onBlur}
      search
      compact
      className="icon"
      icon="dropdown"
      value={batch?.id}
      loading={loading || !batches || mutationLoading}
      options={[...batchOptions, { key: 'none', text: 'None', value: 'none' }]}
      placeholder="Select a batch"
      onChange={async (_e: React.SyntheticEvent<HTMLElement, Event>, _data: any) => {
        handleDropdownChange(_data.value as string)
      }}
    />
  )
}
