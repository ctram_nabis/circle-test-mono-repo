import { CSVExportButton } from 'components'
import { Button } from 'components/Button'
import { Popup } from 'components/Popup'
import { useAllSkuBatchesForCsvQuery } from 'generated/graphql'
import { FeedbackType } from 'modules/Inventory'
import { useOrganization } from 'modules/Organization'
import React from 'react'
import { oc } from 'ts-optchain'

interface Props {
  onFeedback: (f: FeedbackType) => void
  adminOverrideOrganizationId?: string
  disabled: boolean
}

export function SkuBatchCSVExportButton({ onFeedback, adminOverrideOrganizationId, disabled }: Props) {
  const { organization } = useOrganization()
  const { id: organizationId } = organization

  const { data: skuBatchesCSVData, error: skuBatchesCSVErrors } = useAllSkuBatchesForCsvQuery({
    fetchPolicy: 'network-only',
    variables: {
      isArchived: false,
      organizationId:
        adminOverrideOrganizationId && adminOverrideOrganizationId !== 'All Organizations'
          ? adminOverrideOrganizationId
          : organizationId,
      pageInfo: {
        page: 1,
        numItemsPerPage: 5000,
        orderBy: [
          {
            attribute: 'createdAt',
            order: 'DESC',
          },
        ],
      },
    },
  })

  const getSkusForCsv = () => {
    if (skuBatchesCSVErrors) {
      onFeedback({
        header: 'Error while exporting for CSV',
        content: `${skuBatchesCSVErrors[0].message}`,
        error: true,
        success: false,
      })
    }

    const raw = oc(skuBatchesCSVData).viewer.allSkuBatches.results([])

    return raw.map((s) => {
      const {
        inventoryValues: { LA, OAK },
        batch,
      } = s
      const {
        sku: { code, name, unit, pricePerUnit, lastUpdate },
      } = s

      const [totalCount, totalIncoming, totalOutgoing] = [
        LA.counted + OAK.counted,
        LA.incoming + OAK.incoming,
        LA.outgoing + OAK.outgoing,
      ]
      const totalAllocated = totalIncoming - totalOutgoing
      const totalAvailable = totalCount + totalAllocated

      return {
        code,
        name,
        batch_code: batch ? `${batch.code}` : '',
        unit,
        price_per_unit: pricePerUnit,
        total_count: totalCount,
        oakland_count: OAK.counted,
        la_count: LA.counted,
        total_incoming: totalIncoming,
        total_outgoing: totalOutgoing,
        total_available: totalAvailable,
        oakland_incoming: OAK.incoming,
        oakland_outgoing: OAK.outgoing,
        oakland_available: OAK.available,
        la_incoming: LA.incoming,
        la_outgoing: LA.outgoing,
        la_available: LA.available,
        lastUpdate: new Date(lastUpdate).toUTCString(),
      }
    })
  }

  if (disabled) {
    return (
      <Popup
        trigger={
          <Button basic color="green" floated="right">
            Export CSV
          </Button>
        }
        content="Select an organization first!"
        position="bottom center"
      />
    )
  }

  return <CSVExportButton data={[]} fetchFn={getSkusForCsv} />
}
