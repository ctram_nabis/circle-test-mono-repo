import { Box } from 'components/core/src/Box'
import { HelpIcon } from 'components/HelpIcon'
import { S } from 'components/SemanticComponents'
import { OrderByEnum, OrderByInput, PageInfoInput, Scalars } from 'generated/graphql'
import { DEFAULT_PAGE_INFO } from 'hooks'
import { FeedbackType } from 'modules/Inventory/containers/InventoryPage'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React from 'react'
import { Table } from 'semantic-ui-react'
import styled from 'styled-components'
import { ExpandableKeys, ExpandedType } from '../containers/AllSkuBatchesTab'
import { AddSkuBatchModalButton } from './AddSkuBatchModal'
import { SkuBatchCSVExportButton } from './SkuBatchCSVExportButton'
import { TITLEHOLDER_LABEL } from '../constants/index'

const [
  BlueBoldHeaderCell,
  BlueHeaderCell,
  GreenBoldHeaderCell,
  GreenHeaderCell,
  GreyBoldHeaderCell,
  GreyHeaderCell,
  OrangeBoldHeaderCell,
  OrangeHeaderCell,
] = [
  styled(Table.HeaderCell)`
    white-space: nowrap;
    color: #1298c9 !important;
  `,
  styled(Table.HeaderCell)`
    white-space: nowrap;
    color: #1298c9 !important;
    font-weight: normal !important;
  `,

  styled(Table.HeaderCell)`
    white-space: nowrap;
    color: #0eae38 !important;
  `,
  styled(Table.HeaderCell)`
    white-space: nowrap;
    color: #0eae38 !important;
    font-weight: normal !important;
  `,

  styled(Table.HeaderCell)`
    white-space: nowrap;
    color: grey !important;
  `,
  styled(Table.HeaderCell)`
    white-space: nowrap;
    color: grey !important;
    font-weight: normal !important;
  `,

  styled(Table.HeaderCell)`
    white-space: nowrap;
    color: #ff9b0f !important;
  `,

  styled(Table.HeaderCell)`
    white-space: nowrap;
    color: #ff9b0f !important;
    font-weight: normal !important;
  `,
]

const ORDERBY_NEXT_ORDER: Record<OrderByEnum | 'none', OrderByEnum | 'none'> = {
  ASC: 'DESC',
  DESC: 'none',
  none: 'ASC',
}

const SUBHEADER_LOCATIONS: ExpandableKeys[] = ['oak', 'la']

const SORTABLE_HEADER_NAMES = {
  'batch.code': 'Batch',
  'sku.code': 'Code',
  'sku.name': 'Name',
  'batch.license.legalEntityName': 'Manufacturer',
  'skuBatch.titleholder': TITLEHOLDER_LABEL,
}

type SortableKeys = keyof typeof SORTABLE_HEADER_NAMES

interface Props {
  adminOverrideOrganizationId?: Scalars['ID']
  onFeedback: (value: FeedbackType) => void
  search?: string
  pageInfo: PageInfoInput
  onChangeOrderBy: (orderBy: OrderByInput[]) => void
  onChangeSearch: (value: string) => void
  expanded: ExpandedType
  onChangeExpanded: (input: Partial<Record<ExpandableKeys, boolean>>) => void
  isArchived: boolean
  onChangeIsArchived: (b: boolean) => void
}

export function SkuBatchesHeaders({
  adminOverrideOrganizationId,
  onFeedback,
  search,
  onChangeSearch,
  pageInfo,
  onChangeOrderBy,
  expanded,
  onChangeExpanded,
  isArchived,
  onChangeIsArchived,
}: Props) {
  const { canCreate } = useOrganization()
  const { user, isAdmin } = useMe()

  // TODO: event typewas bugging out
  const updateSearch = (_e: any, { value }: S.SearchProps) => {
    if (value) {
      onChangeSearch(value)
    } else {
      onChangeSearch('')
    }
  }

  const renderInventorySubHeaders = () => {
    const [counted, incoming, outgoing, available] = [
      [<GreyBoldHeaderCell key="inventory-header-total-counted">Total</GreyBoldHeaderCell>],
      [<BlueBoldHeaderCell key="inventory-header-total-incoming">Total</BlueBoldHeaderCell>],
      [<OrangeBoldHeaderCell key="inventory-header-total-outgoing">Total</OrangeBoldHeaderCell>],
      [<GreenBoldHeaderCell key="inventory-header-total-available">Total</GreenBoldHeaderCell>],
    ]

    SUBHEADER_LOCATIONS.filter((k) => expanded[k]).forEach((_location, i) => {
      counted.push(<GreyHeaderCell key={`inventory-${i}-counted`}>{_location.toUpperCase()}</GreyHeaderCell>)
      incoming.push(<BlueHeaderCell key={`inventory-${i}-incoming`}>{_location.toUpperCase()}</BlueHeaderCell>)
      outgoing.push(<OrangeHeaderCell key={`inventory-${i}-outgoing`}>{_location.toUpperCase()}</OrangeHeaderCell>)
      available.push(<GreenHeaderCell key={`inventory-${i}-available`}>{_location.toUpperCase()}</GreenHeaderCell>)
    })
    return <Table.Row>{[...counted, ...incoming, ...outgoing, ...available]}</Table.Row>
  }

  const handleToggleSort = (_attribute: SortableKeys) => {
    const currentOrderBy = pageInfo?.orderBy
    const { order: currentOrder } = currentOrderBy?.find((o) => o?.attribute === _attribute) || { order: 'none' }
    // TODO: Verify that this is doing what it's supposed to (OrderByEnum)
    const newOrder = ORDERBY_NEXT_ORDER[currentOrder ?? 'none']
    if (newOrder === 'none') {
      const newOrderBy = currentOrderBy?.filter((orderObj) => orderObj?.attribute !== _attribute)
      onChangeOrderBy((newOrderBy as OrderByInput[]) ?? DEFAULT_PAGE_INFO.orderBy)
    } else {
      onChangeOrderBy([{ attribute: _attribute, order: newOrder }])
    }
  }

  const renderSortButton = (column: SortableKeys) => {
    const { order: sortOrder } = pageInfo?.orderBy?.find((o) => o?.attribute === column) || {}
    const setName = () => {
      if (sortOrder === 'ASC') {
        return 'sort up'
      } else if (sortOrder === 'DESC') {
        return 'sort down'
      }
      return 'sort'
    }
    const name = setName()
    return <S.Icon name={name} onClick={() => handleToggleSort(column)} />
  }

  const renderSortableHeader = (column: SortableKeys, spanLength: number) => (
    <Table.HeaderCell rowSpan={spanLength.toString()} onClick={() => handleToggleSort(column)}>
      <Box width={150}>
        {SORTABLE_HEADER_NAMES[column]}
        {renderSortButton(column)}
      </Box>
    </Table.HeaderCell>
  )

  const renderUnsortableHeader = (header: string, spanLength: number) => (
    <Table.HeaderCell rowSpan={spanLength.toString()}>
      <Box width={150}>{header}</Box>
    </Table.HeaderCell>
  )

  const numInventoryCol = Object.values(expanded).filter((_) => _).length + 1
  const showActions = canCreate.inventory || user.isAdmin

  return (
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell colSpan="1">
          <S.Search
            value={search || ''}
            size="small"
            placeholder="Search..."
            open={false}
            onSearchChange={updateSearch}
          />
        </Table.HeaderCell>
        <Table.HeaderCell colSpan={showActions ? 3 : 2}>
          {canCreate.inventory && <AddSkuBatchModalButton />}
        </Table.HeaderCell>
        <Table.HeaderCell colSpan={numInventoryCol * 3}>
          {canCreate.csvExportInventory && (
            <SkuBatchCSVExportButton
              onFeedback={onFeedback}
              adminOverrideOrganizationId={adminOverrideOrganizationId}
              disabled={Boolean(
                isAdmin && (!adminOverrideOrganizationId || adminOverrideOrganizationId === 'All Organizations'),
              )}
            />
          )}
        </Table.HeaderCell>
        <Table.HeaderCell colSpan={numInventoryCol + 2}>
          <S.Checkbox
            toggle
            onChange={() => onChangeIsArchived(!isArchived)}
            defaultChecked={isArchived}
            label="Archived"
          />
        </Table.HeaderCell>
      </Table.Row>
      <Table.Row>
        {renderSortableHeader('sku.code', 3)}
        {renderSortableHeader('sku.name', 3)}
        {renderSortableHeader('batch.code', 3)}
        {renderUnsortableHeader('Manufacturer', 3)}
        {renderUnsortableHeader(TITLEHOLDER_LABEL, 3)}
        <Table.HeaderCell rowSpan="3">COA</Table.HeaderCell>
        <Table.HeaderCell colSpan={numInventoryCol * 4} textAlign="center">
          <S.ButtonGroup width="1">
            <S.Button basic={!expanded.oak} color="grey" onClick={() => onChangeExpanded({ oak: !expanded.oak })}>
              OAK
            </S.Button>
            <S.Button basic={!expanded.la} color="grey" onClick={() => onChangeExpanded({ la: !expanded.la })}>
              LA
            </S.Button>
          </S.ButtonGroup>
        </Table.HeaderCell>
        {showActions && (
          <Table.HeaderCell rowSpan={3}>
            <Box alignContent="center">
              <S.Icon name="settings" />
            </Box>
          </Table.HeaderCell>
        )}
      </Table.Row>
      <Table.Row>
        <GreyBoldHeaderCell colSpan={numInventoryCol}>
          Counted <HelpIcon message="Counted is all inventory on hand and in storage" color="shade4" />
        </GreyBoldHeaderCell>
        <BlueBoldHeaderCell colSpan={numInventoryCol}>
          Incoming{' '}
          <HelpIcon
            message={'These values represent the total quantity of a SKU on active replenishment orders'}
            color="accent1"
          />
        </BlueBoldHeaderCell>
        <OrangeBoldHeaderCell colSpan={numInventoryCol}>
          Outgoing{' '}
          <HelpIcon
            message="These values represent the total quantity of a SKU on active retail delivery and outbound transfer orders"
            color="accent2"
          />
        </OrangeBoldHeaderCell>
        <GreenBoldHeaderCell colSpan={numInventoryCol}>
          Available{' '}
          <HelpIcon
            message="(Available = Counted - Outgoing) Cells in green have available inventory, while cells in red are oversold"
            color="success"
          />
        </GreenBoldHeaderCell>
      </Table.Row>
      {renderInventorySubHeaders()}
    </Table.Header>
  )
}
