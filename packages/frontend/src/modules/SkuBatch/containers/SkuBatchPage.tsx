import React, { Fragment, useState } from 'react'
import { Box } from 'components/core/src/Box'
import { Button } from 'components/Button'
import { ErrorBox } from 'components/ErrorBox'
import { Loading } from 'components/Loading'
import { S } from 'components/SemanticComponents'
import {
  SkuBatch,
  useDeleteSkuBatchMutation,
  useGetSkuBatchQuery,
  useRestoreSkuBatchMutation,
  useUpdateSkuMutation,
} from 'generated/graphql'
import { CorrectionModalButton } from 'modules/Inventory'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import { Container, Divider, Grid, Header } from 'semantic-ui-react'
import styled from 'styled-components'
import { SkuBatchActiveOrders } from '../components/SkuBatchPage/SkuBatchActiveOrders'
import { SkuBatchInventoryValues } from '../components/SkuBatchPage/SkuBatchInventoryValues'
import { SkuBatchInventoryLedgerTabs } from './SkuBatchInventoryLedgerTabs'

const InlineHeader = styled(Header)`
  display: inline;
`

const InlineButton = styled(Button)`
  display: inline;
  margin-left: 1em !important;
  padding: auto auto 0.2em auto;
  position: relative;
  top: -0.2em;
`

const TopMarginContainer = styled(Container)`
  margin: 2em auto 0 auto !important;
`

const SkuBatchHeader = styled(Header)`
  margin: 0 0 0.2em 0 !important;
`

const SubHeader = styled(Header)`
  margin: 0 0 0 0 !important;
`

export const SkuBatchPage = () => {
  const [showActiveOrders, showHideActiveOrders] = useState(true)
  const [showLedgerItems, showHideLedgerItems] = useState(true)

  const router = useRouter()
  const { user, isAdmin } = useMe()
  const { canCreate } = useOrganization()

  const { skuBatchId } = router.query

  const [updateSkuMutation] = useUpdateSkuMutation()
  const [restoreSkuBatchMutation] = useRestoreSkuBatchMutation()
  const [deleteSkuBatchMutation] = useDeleteSkuBatchMutation()
  const { data, loading, error } = useGetSkuBatchQuery({ variables: { id: skuBatchId } })

  const skuBatch = data && data.getSkuBatch
  const product = skuBatch && skuBatch.sku
  const batch = skuBatch && skuBatch.batch
  const inventoryValues = skuBatch && skuBatch.inventoryValues

  const [skuCode, skuName] = product ? [product.code, product.name] : ['err', 'err']
  const skuBatchCode = `${skuCode}: ${batch && batch.code ? batch.code : '---'}`

  const refetchQueries = ['GetSkuBatch']

  const onUndoButtonClick = async () => {
    const undoProductConfirmText =
      'It looks like the whole product is archived. Are you sure you want to restore this SKU? This will restore the entire product.'

    // if parent product is archived, it needs to be unarchived
    if (skuBatch?.deletedAt) {
      await restoreSkuBatchMutation({ variables: { id: skuBatchId }, refetchQueries })
      return
    }

    if (product && product.isArchived) {
      if (!window.confirm(undoProductConfirmText)) {
        return
      }

      await updateSkuMutation({
        variables: { input: { id: product.id, isArchived: false } },
        refetchQueries,
      })
    }

    // if the row is soft deleted, we need to restore it
  }

  const onArchiveButtonClick = async () => {
    const archiveConfirmText = `Are you sure you want to archive this SKU? (${skuBatch?.sku?.code}`

    if (!window.confirm(archiveConfirmText)) {
      return
    }

    if (!skuBatchId) {
      return
    }

    await deleteSkuBatchMutation({
      variables: { id: skuBatchId },
      refetchQueries,
    })
  }

  const disableButtons = !user.isAdmin && !canCreate.inventory

  return (
    <Fragment>
      <Loading loading={loading} />
      <ErrorBox error={error} />
      {!loading && !error && (
        <TopMarginContainer>
          {isAdmin && <CorrectionModalButton skuBatch={skuBatch as SkuBatch} />}
          {canCreate.inventory &&
            (!skuBatch?.deletedAt ? (
              <S.Popup
                content={`This will archive SKU: ${skuBatch?.sku?.code}, so all SKU-Batch pairs related to ${skuBatch?.sku?.code} will be archived.`}
                trigger={
                  <S.Button
                    basic
                    color="yellow"
                    icon={disableButtons ? 'lock' : 'archive'}
                    disabled={Boolean(disableButtons)}
                    onClick={() => onArchiveButtonClick()}
                  />
                }
              />
            ) : (
              <S.Button icon="undo" onClick={() => onUndoButtonClick()} />
            ))}
          <Divider />
          <Grid columns="equal">
            <Grid.Row>
              <Grid.Column>
                <SkuBatchHeader as="h1">
                  SKU: {skuName}
                  {skuBatch?.deletedAt && '(Archived)'}
                </SkuBatchHeader>
                <SubHeader as="h4" color="grey">
                  {skuBatchCode}
                </SubHeader>
              </Grid.Column>
            </Grid.Row>

            {inventoryValues && <SkuBatchInventoryValues inventoryValues={inventoryValues} />}
            <Divider />

            {/* start active orders */}
            <Grid.Row>
              <Grid.Column>
                <Box center>
                  <InlineHeader as="h2">Active Orders</InlineHeader>
                  <InlineButton onClick={() => showHideActiveOrders(!showActiveOrders)} size="small">
                    {showActiveOrders ? 'hide' : 'show'}
                  </InlineButton>
                </Box>
              </Grid.Column>
            </Grid.Row>
            {showActiveOrders && (
              <Grid.Row>
                <Grid.Column>
                  <SkuBatchActiveOrders skuBatchId={skuBatchId} />
                </Grid.Column>
              </Grid.Row>
            )}

            {/* start ledger */}
            <Grid.Row>
              <Grid.Column>
                <Box center>
                  <InlineHeader as="h2">Inventory Ledger</InlineHeader>
                  <InlineButton onClick={() => showHideLedgerItems(!showLedgerItems)} size="small">
                    {showLedgerItems ? 'hide' : 'show'}
                  </InlineButton>
                </Box>
              </Grid.Column>
            </Grid.Row>

            {showLedgerItems && (
              <Grid.Row>
                <Grid.Column>
                  <SkuBatchInventoryLedgerTabs />
                </Grid.Column>
              </Grid.Row>
            )}
          </Grid>
        </TopMarginContainer>
      )}
    </Fragment>
  )
}
