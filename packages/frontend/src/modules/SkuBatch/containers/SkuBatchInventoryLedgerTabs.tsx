import { S } from 'components'
import { WAREHOUSES } from 'modules/Warehouse'
import React, { useState } from 'react'
import { SkuBatchLedgerItems } from '../components/SkuBatchPage'

interface OrderBy {
  attribute: string
  order: 'ASC' | 'DESC'
}

export interface FiltersState {
  numItemsPerPage: number
  orderBy: OrderBy[]
  page: number
}

export const SkuBatchInventoryLedgerTabs = () => {
  const [filters, setFilters] = useState<FiltersState>({
    numItemsPerPage: 25,
    orderBy: [
      {
        attribute: 'createdAt',
        order: 'DESC',
      },
    ],
    page: 1,
  })

  const filter = (key: string, value: string | number) =>
    setFilters((prevState) => ({
      ...prevState,
      [key]: value,
    }))

  return (
    <S.Tab
      panes={[
        {
          menuItem: 'Oakland',
          render: () => (
            <SkuBatchLedgerItems filter={filter} filters={filters} warehouseId={WAREHOUSES.OAK.encodedWarehouseId} />
          ),
        },
        {
          menuItem: 'LA',
          render: () => (
            <SkuBatchLedgerItems filter={filter} filters={filters} warehouseId={WAREHOUSES.LA.encodedWarehouseId} />
          ),
        },
      ]}
      className="inv-tabs"
    />
  )
}
