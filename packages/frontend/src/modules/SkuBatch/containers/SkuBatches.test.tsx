import { MockedProvider } from '@apollo/react-testing'
import { mount, shallow } from 'enzyme'
import React from 'react'
import { SkuBatchCSVExportButton } from '../components/SkuBatchCSVExportButton'
import { SkuBatchesHeaders } from '../components/SkuBatchesHeaders'
import { SkuBatchRow } from '../components/SkuBatchRow'
import { AllSkuBatchesTab } from './AllSkuBatchesTab'

describe('SkuBatches container', () => {
  describe('AllSkuBatchesTab', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <AllSkuBatchesTab />
        </MockedProvider>,
      )
    })
  })

  describe('SkuBatchCSVExportButton', () => {
    it('should render without crashing', () => {
      shallow(<SkuBatchCSVExportButton />)
    })

    /**
     * TODO: simulate button click handler, make sure jest mock fn is called
     */
  })

  describe('SkuBatchesHeaders', () => {
    it('should render without crashing', () => {
      shallow(<SkuBatchesHeaders expanded={{ oak: false, la: false }} pageInfo={{ orderBy: [] }} />)
    })
  })

  describe('SkuBatchRow', () => {
    it('should render without crashing', () => {
      shallow(<SkuBatchRow skuBatch={{ inventoryValues: { LA: {}, OAK: {} } }} expanded={{ oak: false, la: false }} />)
    })
  })
})
