import { ErrorBox, Pagination, S } from 'components'
import { Scalars, SkuBatch, useInventorySkuBatchesTabQuery } from 'generated/graphql'
import { usePageInfo } from 'hooks'
import { FeedbackType } from 'modules/Inventory/containers/InventoryPage'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React, { useEffect, useState } from 'react'
import { SkuBatchesHeaders } from '../components/SkuBatchesHeaders'
import { SkuBatchRow } from '../components/SkuBatchRow'

const COL_SPAN_LENGTH = '17'
export type ExpandableKeys = 'la' | 'oak'
export type ExpandedType = Record<ExpandableKeys, boolean>

interface Props {
  adminOverrideOrganizationId?: Scalars['ID']
  onFeedback: (f: FeedbackType) => void
}

export function AllSkuBatchesTab({ onFeedback, adminOverrideOrganizationId }: Props) {
  const { organization } = useOrganization()
  const { isAdmin } = useMe()
  const organizationId = isAdmin ? adminOverrideOrganizationId : organization.id

  const [expanded, setExpanded] = useState<ExpandedType>({ la: false, oak: false })
  const [isArchived, setIsArchived] = useState<boolean>(false)
  const [search, setSearch] = useState<string>('')
  const { pageInfo, setPage, setOrderBy } = usePageInfo()

  const { data, loading, error, ...allInventorySkuBatches } = useInventorySkuBatchesTabQuery({
    variables: {
      isArchived,
      organizationId,
      pageInfo,
      search: search.trim(),
    },
  })

  useEffect(() => {
    allInventorySkuBatches.refetch()
  }, [allInventorySkuBatches, isArchived, pageInfo])

  const skuBatches = data?.viewer?.allSkuBatches?.results ?? []
  const totalNumPages = data?.viewer?.allSkuBatches?.pageInfo?.totalNumPages ?? 1

  return (
    <>
      <Pagination
        onPageChange={(_e, d) => setPage(d.activePage)}
        activePage={pageInfo.page}
        totalNumPages={totalNumPages}
        siblingRange={3}
      />
      <S.Table attached celled selectable structured data-test="all-sku-batches-tab-table">
        <SkuBatchesHeaders
          adminOverrideOrganizationId={adminOverrideOrganizationId}
          pageInfo={pageInfo}
          onChangeOrderBy={setOrderBy}
          search={search}
          onChangeSearch={setSearch}
          expanded={expanded}
          onChangeExpanded={(input: Partial<Record<ExpandableKeys, boolean>>) => {
            setExpanded({ ...expanded, ...input })
          }}
          isArchived={isArchived}
          onChangeIsArchived={setIsArchived}
          onFeedback={onFeedback}
        />
        <S.Table.Body>
          {loading ? (
            <S.Table.Row>
              <S.Table.Cell colSpan={COL_SPAN_LENGTH}>
                <S.Segment placeholder className="flex-style">
                  <S.Header>
                    <S.Loader active inline="centered" size="large">
                      Loading...
                    </S.Loader>
                  </S.Header>
                </S.Segment>
              </S.Table.Cell>
            </S.Table.Row>
          ) : error ? (
            <S.Table.Row>
              <S.Table.Cell colSpan={COL_SPAN_LENGTH}>
                <S.Segment placeholder className="flex-style">
                  <S.Header>
                    <ErrorBox errors={[error]} />
                  </S.Header>
                </S.Segment>
              </S.Table.Cell>
            </S.Table.Row>
          ) : skuBatches && skuBatches.length > 0 ? (
            skuBatches.map((skuBatch, i) => (
              <SkuBatchRow
                adminOverrideOrganizationId={adminOverrideOrganizationId}
                key={`skuBatchRow-${skuBatch?.id ?? i}`}
                expanded={expanded}
                skuBatch={skuBatch as SkuBatch}
                isArchived={isArchived}
                onFeedback={onFeedback}
              />
            ))
          ) : (
            <S.Table.Cell colSpan={COL_SPAN_LENGTH}>
              <S.Segment placeholder className="flex-style">
                <S.Header>No data available</S.Header>
              </S.Segment>
            </S.Table.Cell>
          )}
        </S.Table.Body>
      </S.Table>
      {skuBatches && skuBatches.length > 0 && (
        <Pagination
          onPageChange={(_e, d) => setPage(d.activePage)}
          activePage={pageInfo.page}
          totalNumPages={totalNumPages}
          siblingRange={3}
        />
      )}
    </>
  )
}
