import { MockedProvider } from '@apollo/react-testing'
import { mount, shallow } from 'enzyme'
import React from 'react'
import { SkuBatchActiveOrderHeader } from '../components/SkuBatchPage/SkuBatchActiveOrderHeader'
import { SkuBatchActiveOrderRow } from '../components/SkuBatchPage/SkuBatchActiveOrderRow'
import { SkuBatchActiveOrders } from '../components/SkuBatchPage/SkuBatchActiveOrders'
import { SkuBatchInventoryValues } from '../components/SkuBatchPage/SkuBatchInventoryValues'
import { SkuBatchLedgerItemHeader } from '../components/SkuBatchPage/SkuBatchLedgerItemHeader'
import { SkuBatchLedgerItemRow } from '../components/SkuBatchPage/SkuBatchLedgerItemRow'
import { SkuBatchLedgerItems } from '../components/SkuBatchPage/SkuBatchLedgerItems'
import { SkuBatchInventoryLedgerTabs } from './SkuBatchInventoryLedgerTabs'
import { SkuBatchPage } from './SkuBatchPage'

describe('SkuBatchPage container', () => {
  describe('SkuBatchActiveOrder', () => {
    it('should render without crashing', () => {
      shallow(<SkuBatchActiveOrderRow order={{ warehouse: { id: '' } }} />)
    })
  })

  describe('SkuBatchActiveOrderHeader', () => {
    it('should render without crashing', () => {
      shallow(<SkuBatchActiveOrderHeader />)
    })
  })

  describe('SkuBatchActiveOrders', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <SkuBatchActiveOrders />
        </MockedProvider>,
      )
    })
  })

  describe('SkuBatchInventoryValues', () => {
    it('should render without crashing', () => {
      shallow(<SkuBatchInventoryValues inventoryValues={{}} />)
    })
  })

  describe('SkuBatchLedgerItem', () => {
    it('should render without crashing', () => {
      shallow(<SkuBatchLedgerItemRow item={{}} />)
    })
  })

  describe('SkuBatchLedgerItemHeader', () => {
    it('should render without crashing', () => {
      shallow(<SkuBatchLedgerItemHeader />)
    })
  })

  describe('SkuBatchLedgerItems', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <SkuBatchLedgerItems />
        </MockedProvider>,
      )
    })
  })

  describe('SkuBatchPage', () => {
    it('should render without crashing', () => {
      mount(
        <MockedProvider>
          <SkuBatchPage />
        </MockedProvider>,
      )
    })
  })

  describe('SkuBatchTabs', () => {
    it('should render without crashing', () => {
      shallow(<SkuBatchInventoryLedgerTabs />)
    })
  })
})
