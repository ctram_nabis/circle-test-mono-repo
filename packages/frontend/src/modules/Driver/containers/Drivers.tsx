import { Button } from 'components/Button'
import { CenterView } from 'components/CenterView'
import { PageHeader } from 'components/PageHeader'
import { Ribbon } from 'components/Ribbon'
import { S } from 'components/SemanticComponents'
import { Driver } from 'generated/graphql'
import React, { Fragment, useState } from 'react'
import { AddDriverModal, EditDriverModal } from './AddEditDriverModal'
import { DriversRow } from './DriversRow'

interface Props {
  drivers: Array<Partial<Driver>>
}

export enum ModalEnum {
  EDIT = 'EDIT',
  ADD = 'ADD',
}

export const Drivers = (props: Props) => {
  const { drivers } = props
  const [modalOpen, setModalOpen] = useState<ModalEnum | undefined>(undefined)
  const [openDriver, setOpenDriver] = useState<Partial<Driver> | undefined>(undefined)

  const onModalOpen = (modal: ModalEnum, _driver?: Partial<Driver>) => {
    setOpenDriver(_driver)
    setModalOpen(modal)
  }
  const onModalClose = () => {
    setOpenDriver(undefined)
    setModalOpen(undefined)
  }

  return (
    <Fragment>
      <S.GridRow>
        <S.GridColumn>
          <CenterView>
            <PageHeader>Drivers</PageHeader>
            <Button name="plus" title="New Driver" color="green" onClick={() => onModalOpen(ModalEnum.ADD)} />
          </CenterView>
        </S.GridColumn>
      </S.GridRow>
      <S.GridRow>
        <S.GridColumn>
          <S.Table celled padded selectable unstackable verticalAlign="middle" sortable color="green">
            <S.Table.Header>
              <S.Table.Row>
                <S.Table.HeaderCell>
                  <Ribbon color="green">First Name</Ribbon>
                </S.Table.HeaderCell>
                <S.Table.HeaderCell>Last Name</S.Table.HeaderCell>
                <S.Table.HeaderCell>DL</S.Table.HeaderCell>
                <S.Table.HeaderCell>Actions</S.Table.HeaderCell>
              </S.Table.Row>
            </S.Table.Header>
            <S.Table.Body>
              {drivers.map((driver, i) => (
                <DriversRow key={i} driver={driver} onModalOpen={onModalOpen} />
              ))}
            </S.Table.Body>
          </S.Table>
        </S.GridColumn>
      </S.GridRow>
      {modalOpen === 'EDIT' && <EditDriverModal driver={openDriver} onClose={onModalClose} onSuccess={onModalClose} />}
      {modalOpen === 'ADD' && <AddDriverModal onClose={onModalClose} onSuccess={onModalClose} />}
    </Fragment>
  )
}
