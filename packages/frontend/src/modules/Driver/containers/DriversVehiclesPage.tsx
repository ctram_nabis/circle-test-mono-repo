import { Box, ErrorBox, Grid, S } from 'components'
import { useAllDriversVehiclesQuery } from 'generated/graphql'
import { Vehicles } from 'modules/Vehicle'
import React from 'react'
import { Drivers } from './Drivers'

export const DriversVehiclesPage = () => {
  const { data, loading, error } = useAllDriversVehiclesQuery()

  const drivers = data?.viewer?.allDrivers || []
  const vehicles = data?.viewer?.allVehicles || []

  return (
    <S.Container>
      <Box pb={30} bg="shade1">
        <S.Container>
          <S.Loader active={loading} inline="centered" size="large" content="Loading Drivers and Vehicles..." />
          <ErrorBox header="Oops there was a problem!" error={error} />
          {!error && !loading && (
            <Grid>
              <Drivers drivers={drivers} />
              <Vehicles vehicles={vehicles} />
            </Grid>
          )}
        </S.Container>
      </Box>
    </S.Container>
  )
}
