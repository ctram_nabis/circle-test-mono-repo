import { Button, ErrorBox, Form, P, S } from 'components'
import { Driver, UpdateDriverInput, useUpdateDriverMutation } from 'generated/graphql'
import { createFormField, createValidationSchema, useForm, validators } from 'modules/Forms'
import { logger } from 'modules/Logger'
import React, { useState } from 'react'
import { ErrorType } from 'types'

const INLINE_REQUIRED_MSG = ' * Required'
const validationSchema = createValidationSchema<UpdateDriverInput>({
  id: validators.string.required(INLINE_REQUIRED_MSG),
  firstName: validators.string.required(INLINE_REQUIRED_MSG),
  lastName: validators.string.required(INLINE_REQUIRED_MSG),
  driversLicense: validators.string.required(INLINE_REQUIRED_MSG),
})
interface Props {
  driver?: Partial<Driver>
  onClose?: () => void
  onFailure?: (errors: ErrorType[]) => void
  onOpen?: () => void
  onSuccess?: () => void
  open?: boolean
}

export function EditDriverModal(props: Props) {
  const { driver, onClose, onFailure, onOpen, onSuccess, open = true } = props

  const [catchError, setCatchError] = useState<string>()
  const [requestErrors, setRequestErrors] = useState<ErrorType[]>()
  const [isOpen, setIsOpen] = useState(open)

  const [updateDriverMutation] = useUpdateDriverMutation({
    refetchQueries: ['AllDriversVehicles'],
  })

  const handleClose = () => {
    setIsOpen(false)
    if (onClose) onClose()
  }

  const handleOpen = () => {
    setIsOpen(true)
    if (onOpen) onOpen()
  }

  const form = useForm<typeof validationSchema>({
    validationSchema,
    initialValues: {
      id: driver?.id || '',
      firstName: driver?.firstName || '',
      lastName: driver?.lastName || '',
      driversLicense: driver?.driversLicense || '',
    },
    onSubmit: async (inputs) => {
      try {
        const response = await updateDriverMutation({
          variables: {
            input: inputs,
          },
        })

        if (response.errors) {
          setRequestErrors(response.errors)
          if (onFailure) onFailure(response.errors)
          return
        } else {
          return onSuccess && onSuccess()
        }
      } catch (err) {
        logger.error(err)
        if (Array.isArray(err?.graphQLErrors)) {
          setRequestErrors(err?.graphQLErrors)
        } else {
          setCatchError(err?.graphQLErrors?.message)
        }
        return onFailure && onFailure(err.message)
      }
    },
  })

  const fieldsInGroups: JSX.Element[][] = [
    [
      createFormField({
        required: true,
        label: 'First Name*',
        name: 'firstName',
        key: 'firstName',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Philip',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'Last Name',
        name: 'lastName',
        key: 'lastName',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Witham',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
      createFormField({
        required: true,
        label: 'License*',
        name: 'driversLicense',
        key: 'driversLicense',
        onChange: form.handleChange,
        onBlur: form.handleBlur,
        placeholder: 'Z1234567',
        type: 'text',
        values: form.values,
        errors: form.errors,
      }),
    ],
  ]

  return (
    <S.Modal closeIcon open={isOpen} onOpen={handleOpen} onClose={handleClose}>
      <S.Header icon="edit" content="Update Driver" />
      <S.Modal.Content>
        <P>Enter in the driver information</P>
        <Form id="edit-driver-form" onSubmit={form.handleSubmit}>
          <ErrorBox errors={requestErrors} error={catchError} />
          {fieldsInGroups.map((fields, i) => (
            <S.FormGroup key={`formGroup-${i}`} widths="equal">
              {fields}
            </S.FormGroup>
          ))}
        </Form>
      </S.Modal.Content>
      <S.Modal.Actions>
        <Button name="x" onClick={handleClose} title="Cancel" />
        <Button
          name="check"
          color="yellow"
          title="Update"
          form="edit-driver-form"
          disabled={form.isSubmitting}
          loading={form.isSubmitting}
          type="submit"
        />
      </S.Modal.Actions>
    </S.Modal>
  )
}
