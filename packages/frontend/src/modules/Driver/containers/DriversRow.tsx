import { S } from 'components'
import { Driver, useDeleteDriverMutation } from 'generated/graphql'
import { DeleteConfirmationModal } from 'modules/Modal'
import React, { useState } from 'react'
import { ModalEnum } from './Drivers'
interface Props {
  driver: Partial<Driver>
  onModalOpen: (modal: ModalEnum, driver: Partial<Driver>) => void
}
export const DriversRow = ({ driver, onModalOpen }: Props) => {
  const [isDeleteOpen, setDeleteOpen] = useState<true | undefined>(undefined)
  const { firstName, lastName, driversLicense } = driver
  const [deleteDriverMutation, { loading }] = useDeleteDriverMutation({
    refetchQueries: ['AllDriversVehicles'],
  })

  const handleDeleteDriver = async () => {
    if (!driver || !driver.id) return
    await deleteDriverMutation({
      variables: {
        input: {
          id: driver.id,
        },
      },
    })
  }

  return (
    <>
      <S.Table.Row>
        <S.Table.Cell>{firstName}</S.Table.Cell>
        <S.Table.Cell>{lastName}</S.Table.Cell>
        <S.Table.Cell>{driversLicense}</S.Table.Cell>
        <S.Table.Cell>
          <S.Button
            color="yellow"
            size="tiny"
            icon="edit"
            label="Edit"
            disabled={loading}
            onClick={() => onModalOpen(ModalEnum.EDIT, driver)}
          />
          <S.Button
            color="red"
            size="tiny"
            icon="delete"
            label="Delete"
            disabled={loading}
            onClick={() => setDeleteOpen(true)}
          />
        </S.Table.Cell>
      </S.Table.Row>
      {isDeleteOpen && (
        <DeleteConfirmationModal
          delete={handleDeleteDriver}
          onSuccess={() => setDeleteOpen(undefined)}
          onClose={() => setDeleteOpen(undefined)}
        />
      )}
    </>
  )
}
