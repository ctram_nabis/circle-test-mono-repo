import { MockedProvider } from '@apollo/react-testing'
import { shallow } from 'enzyme'
import React from 'react'

import { Box } from 'components'
import { AddVehicleModal } from '../../Vehicle/containers/AddEditVehicleModal'
import { VehiclesRow } from '../../Vehicle/containers/VehiclesRow'
import { AddDriverModal } from './AddEditDriverModal'
import { DriversRow } from './DriversRow'
import { DriversVehiclesPage } from './DriversVehiclesPage'

describe('`DriversVehicles` container', () => {
  it('`AddEditDriverModal` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <AddDriverModal />
      </MockedProvider>,
    )
  })
  it('`AddEditVehicleModal` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <AddVehicleModal />
      </MockedProvider>,
    )
  })
  it('`DriversRow` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <DriversRow driver={{ id: '', firstName: '', driversLicense: '' }} />
      </MockedProvider>,
    )
  })

  it('`DriversVehiclesPage` renders without crashing', () => {
    shallow(
      <MockedProvider>
        <DriversVehiclesPage />
      </MockedProvider>,
    )
  })
  it('`VehiclesRow` renders without crashing', () => {
    const onModalOpen = jest.fn()
    shallow(
      <MockedProvider>
        <VehiclesRow vehicle={{ id: '' }} />
      </MockedProvider>,
    )
  })
})
