import { Scalars } from 'generated/graphql'

interface WarehouseMeta {
  licensedLocationId: string
  warehouseId: string
  encodedLicensedLocationId: string
  encodedWarehouseId: string
}

const oaklandLicensedLocationId = '8defbafe-906f-41ea-b40f-1b920dfdf40f'
const oaklandWarehouseId = '6ceafe22-c127-11e8-a355-529269fb1459'
const laLicensedLocationId = 'b191a9ca-d6b2-4d47-b7b4-966225e10210'
const laWarehouseId = '85ae34f3-b9c7-415b-b75e-a85e612c5bf5'

export type WarehouseKey = 'LA' | 'OAK'

export const WAREHOUSES: Record<WarehouseKey, WarehouseMeta> = {
  OAK: {
    licensedLocationId: oaklandLicensedLocationId,
    warehouseId: oaklandWarehouseId, // NABIONE
    encodedLicensedLocationId: btoa('LicensedLocation:' + oaklandLicensedLocationId),
    encodedWarehouseId: btoa('Warehouse:' + oaklandWarehouseId), // NABIONE
  },
  LA: {
    licensedLocationId: laLicensedLocationId,
    warehouseId: laWarehouseId, // ZASP
    encodedLicensedLocationId: btoa('LicensedLocation:' + laLicensedLocationId),
    encodedWarehouseId: btoa('Warehouse:' + laWarehouseId), // ZASP
  },
}

export const NABIS_ORGANIZATION_TO_ID = {
  DELIVERIES: btoa('Organization:960f4b56-c5db-41d6-824d-8563e98d850e'),
  SALES: btoa('Organization:6ba4e5bb-c4d7-425c-af85-0db2382508a9'),
}

export const getWarehouseOptions = () => [WAREHOUSES.LA, WAREHOUSES.OAK]
export const getWarehouseIdFromKey = (key: WarehouseKey | string) => WAREHOUSES[key]?.warehouseId
export const getEncodedWarehouseIdFromKey = (key: WarehouseKey | string) => WAREHOUSES[key]?.encodedWarehouseId
export const getLicensedLocationIdFromKey = (key: WarehouseKey | string) => WAREHOUSES[key]?.licensedLocationId
export const getEncodedLicensedLocationIdFromKey = (key: WarehouseKey | string) =>
  WAREHOUSES[key]?.encodedLicensedLocationId

export const getWarehouseKeyFromWarehouseId = (
  warehouseId: string,
  encoded: boolean = true,
): Scalars['ID'] | undefined => {
  for (const key in WAREHOUSES) {
    if (WAREHOUSES.hasOwnProperty(key)) {
      const warehouse = WAREHOUSES[key as WarehouseKey]
      if (encoded) {
        if (warehouse.encodedWarehouseId === warehouseId) {
          return key as WarehouseKey
        }
      } else {
        if (warehouse.warehouseId === warehouseId) {
          return key as WarehouseKey
        }
      }
    }
  }
  return undefined
}

export const getWarehouseKeyFromLicensedLocationId = (
  licensedLocationId: string,
  encoded: boolean = true,
): Scalars['ID'] | undefined => {
  for (const key in WAREHOUSES) {
    if (WAREHOUSES.hasOwnProperty(key)) {
      const warehouse = WAREHOUSES[key as WarehouseKey]
      if (encoded) {
        if (warehouse.encodedLicensedLocationId === licensedLocationId) {
          return key as WarehouseKey
        }
      } else {
        if (warehouse.licensedLocationId === licensedLocationId) {
          return key as WarehouseKey
        }
      }
    }
  }
  return undefined
}
