import { AppContainer, H1, Image, P, S } from 'components'
import { Button } from 'components/Button'
import { Box, Text } from 'components/core'
import { useRouter } from 'modules/Navigation'
import React from 'react'

export const PageNotFound = () => {
  const router = useRouter()

  return (
    <AppContainer center middle>
      <S.Grid centered>
        <S.GridRow columns="2">
          <S.GridColumn>
            <H1>Nabis</H1>
            <Box>
              <P fontWeight="bold">404. That's an error.</P>
              <Text>The page you requested does not exist, or you do not have permission to view it.</Text>
            </Box>
          </S.GridColumn>
          <S.GridColumn bg="accent1">
            <Image name="error" />
          </S.GridColumn>
        </S.GridRow>
        <S.GridRow centered columns="1">
          <S.GridColumn>
            <Button title="Get me out of here" color="grey" onClick={() => router.push('/')} />
          </S.GridColumn>
        </S.GridRow>
      </S.Grid>
    </AppContainer>
  )
}
