import { AppContainer, H1, Image, P, S } from 'components'
import { Box, Text } from 'components/core'
import { deleteToken } from 'modules/Auth/tokenHandlers'
import React from 'react'

export const ErrorPage = () => {
  // const [_token, setToken] = useStorage(AUTH_TOKEN)

  return (
    <AppContainer center middle>
      <S.Grid centered>
        <S.GridRow columns="2">
          <S.GridColumn>
            <H1>Nabis</H1>
            <Box>
              <Text>Server or internet is down.</Text>
              <P>Please try again later.</P>
            </Box>
          </S.GridColumn>
          <S.GridColumn bg="accent1">
            <Image name="error" />
          </S.GridColumn>
        </S.GridRow>
        <S.GridRow>
          <S.Button
            onClick={() => {
              deleteToken()
              window.location.reload(true)
            }}
          >
            Refresh
          </S.Button>
        </S.GridRow>
      </S.Grid>
    </AppContainer>
  )
}
