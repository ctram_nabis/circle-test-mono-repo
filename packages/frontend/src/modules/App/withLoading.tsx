/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */
import { Loading } from 'components'
import * as React from 'react'

export interface WithLoadingProps {
  loading?: boolean
}

export const withLoading = <P extends object>(Component: React.ComponentType<P>): React.FC<P & WithLoadingProps> => ({
  loading,
  ...props
}: WithLoadingProps) => (loading ? <Loading /> : <Component {...(props as P)} />)
