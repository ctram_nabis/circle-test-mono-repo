import { isEnv, isProd } from 'utils/isHelpers'
import { IS_PRODUCTION } from './constants'

export default {
  landingPageUrl: 'https://www.nabis.com/',
  logging: {
    level: isProd('info', 'debug'),
    apolloLogging: !IS_PRODUCTION,
  },
  apiKey: isEnv({
    production: 'nabis-api-key-uh-what-r-u-doingggg',
    staging: 'nabis-api-key-ohhhh-yeahhhhh',
    fallback: 'nabis-api-key-lolol',
  }),
}
