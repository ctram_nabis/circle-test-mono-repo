// tslint:disable: strict-type-predicates

const { REACT_APP_ENV, SECURE, NODE_ENV } = process.env

export const AUTH_TOKEN = 'AUTH_TOKEN'
export const THEME_STATE = 'THEME_STATE'
export const PLATFORM = typeof document !== 'undefined' ? 'web' : 'mobile'
export const IS_SECURE = SECURE

export const APP_ENV = NODE_ENV || REACT_APP_ENV || 'development'
export const IS_PRODUCTION = APP_ENV === 'production'
export const IS_DEV = !IS_PRODUCTION
