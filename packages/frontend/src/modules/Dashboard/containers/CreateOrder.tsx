import { Box, Button, S } from 'components'
import React from 'react'
import { CreateOrderCard } from './components'
interface Props {
  goToPage: (path: string) => void
}

export const CreateOrder = ({ goToPage }: Props) => (
  <CreateOrderCard fluid color="green" centered>
    <S.Grid columns={1} divided stackable padded textAlign="center">
      <S.GridRow>
        <S.GridColumn>
          <S.Card.Content>
            <Box pb={40}>
              <S.Icon name="plus" size="huge" color="grey" />
              <S.Icon name="arrow circle right" size="big" color="grey" />
              <S.Icon name="truck" size="huge" color="grey" />
              <S.Icon name="arrow circle right" size="big" color="grey" />
              <S.Icon name="checkmark" size="huge" color="grey" />
            </Box>
            <Button positive size="huge" onClick={() => goToPage('orders/new')} title="Create New Order" />
          </S.Card.Content>
        </S.GridColumn>
      </S.GridRow>
    </S.Grid>
  </CreateOrderCard>
)
