import { Button, Container, PageHeader, Ribbon, S } from 'components'
import { useAllOrdersSkusQuery } from 'generated/graphql'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import React from 'react'
import { dollarFormat } from 'utils'
import { CreateOrder } from './CreateOrder'
import { DashboardGridColumn } from './DashboardGridColumn'

export const DashboardPage = () => {
  const router = useRouter()
  const { alias } = router.query
  const { organization, canCreate } = useOrganization()
  const organizationId = organization?.id

  const { data, loading, error } = useAllOrdersSkusQuery({
    skip: !organizationId,
    variables: {
      organizationId,
    },
  })

  const goToPage = (route: string) => {
    router.push(`/${alias}/app/${route}`)
  }

  const allSkus = data?.viewer?.allSkus?.results
  const dashboard = data?.viewer?.getDashboardData

  return (
    <Container>
      <S.Container>
        <S.Container>
          <S.Loader active={loading} inline="centered" size="large" content="Loading Dashboard..." />
          {/* <ErrorBox errors={[error]} /> */}
          {!loading && !error && (
            <S.Grid columns={1} padded>
              <S.GridRow>
                <PageHeader>{organization.name}</PageHeader>
              </S.GridRow>
              <S.GridRow>
                <S.CardGroup itemsPerRow={2} stackable>
                  <S.Card raised fluid color="green">
                    <S.Grid columns={3} divided stackable padded>
                      <S.GridRow>
                        <DashboardGridColumn
                          textAlign="center"
                          headingTitle={dashboard?.totalUnscheduled || 0}
                          meta="Deliveries"
                          description="Unscheduled"
                        />
                        <DashboardGridColumn
                          textAlign="center"
                          headingTitle={dashboard?.totalScheduled || 0}
                          meta="Deliveries"
                          description="Scheduled"
                        />

                        <DashboardGridColumn
                          textAlign="center"
                          headingTitle={dashboard?.totalDelivered || 0}
                          meta="Deliveries"
                          description="Completed"
                        />
                      </S.GridRow>
                    </S.Grid>
                    <S.Card.Content extra textAlign="center">
                      <Button
                        title="View Order History"
                        name="truck"
                        basic
                        color="green"
                        onClick={() => goToPage('orders')}
                      />
                    </S.Card.Content>
                  </S.Card>

                  <S.Card raised fluid color="green">
                    <S.Grid>
                      <S.GridRow>
                        <DashboardGridColumn
                          headingTitle={dollarFormat(dashboard?.totalGMVCollected || 0)}
                          meta="For Completed Deliveries"
                          description="Approximate cash on-hand to be remitted"
                          meta2="Less Nabis Fees"
                        />
                      </S.GridRow>
                    </S.Grid>
                  </S.Card>
                  <S.Card raised fluid color="green">
                    <S.Grid>
                      <S.GridRow>
                        <S.GridColumn>
                          <S.Card.Content style={{ minHeight: '150px' }}>
                            <S.Table celled padded selectable unstackable verticalAlign="middle">
                              <S.Table.Header>
                                <S.Table.Row>
                                  <S.Table.HeaderCell>
                                    <Ribbon color="green">SKU Name</Ribbon>
                                  </S.Table.HeaderCell>
                                  <S.Table.HeaderCell>Code</S.Table.HeaderCell>
                                </S.Table.Row>
                              </S.Table.Header>
                              <S.Table.Body>
                                {allSkus &&
                                  allSkus.map((sku) => (
                                    <S.Table.Row key={sku.id}>
                                      <S.Table.Cell>{sku.name}</S.Table.Cell>
                                      <S.Table.Cell>{sku.code}</S.Table.Cell>
                                    </S.Table.Row>
                                  ))}
                              </S.Table.Body>
                            </S.Table>
                          </S.Card.Content>
                        </S.GridColumn>
                      </S.GridRow>
                    </S.Grid>
                  </S.Card>
                  {canCreate.order && <CreateOrder goToPage={goToPage} />}
                </S.CardGroup>
              </S.GridRow>
            </S.Grid>
          )}
        </S.Container>
      </S.Container>
    </Container>
  )
}
