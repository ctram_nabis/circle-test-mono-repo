import { H4 } from 'components/H4'
import styled from 'styled-components'
// TODO: SWITCH OUT WITH CHRIS COMPONENTS ONMCE MERGED
export const HeaderWithPadding = styled(H4)`
  font-size: 4.5em !important;
  font-weight: 500 !important;
  padding: 25px 0 !important;
`
