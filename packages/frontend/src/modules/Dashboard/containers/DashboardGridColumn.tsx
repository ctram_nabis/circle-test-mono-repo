import { S } from 'components'
import React from 'react'
import { HeaderWithPadding } from './components/HeaderWithPadding'

interface Props {
  textAlign?: 'center' | 'left' | 'right'
  headingTitle: string | number
  meta: string
  meta2?: string
  description: string
}

export const DashboardGridColumn = ({ textAlign = 'center', headingTitle, meta, meta2, description }: Props) => (
  <S.GridColumn>
    <S.Card.Content textAlign={textAlign}>
      <S.Card.Header>
        <HeaderWithPadding>{headingTitle}</HeaderWithPadding>
      </S.Card.Header>
      <S.Card.Meta>{meta}</S.Card.Meta>
      <S.Card.Description>
        <S.Header size="medium">{description}</S.Header>
      </S.Card.Description>
      {meta2 && <S.Card.Meta>{meta2}</S.Card.Meta>}
    </S.Card.Content>
  </S.GridColumn>
)
