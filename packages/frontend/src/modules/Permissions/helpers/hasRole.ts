import { UserRole } from 'generated/graphql'
import { RoleName } from 'modules/User'

export const hasRole = (userRoles: Array<Partial<UserRole>>, allowedRoleNameEnumArray: RoleName[]) => {
  return userRoles.find((userRole) => allowedRoleNameEnumArray.includes(userRole.role?.name as RoleName))
}
