import React from 'react'
import { Redirect } from 'react-router-dom'
import { useOrganization } from './useOrganization'

export const withOrg = <P extends any>(Component: React.ComponentType<P>) =>
  function WrappedComponent(props: P) {
    const { organization } = useOrganization()
    return !organization ? <Redirect to="/my-organizations" /> : <Component {...props} />
  }
