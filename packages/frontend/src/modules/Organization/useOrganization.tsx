import { Organization, UserRole } from 'generated/graphql'
import { OrganizationContext } from 'modules/Organization/OrganizationContext'
import { hasRole } from 'modules/Permissions'
import { RoleName, useUserContext } from 'modules/User'
import { NABIS_ORGANIZATION_TO_ID } from 'modules/Warehouse'
import { useContext } from 'react'

export function useOrganizationContext() {
  const { organization, setOrganizationId } = useContext(OrganizationContext)
  const { user } = useUserContext()
  const { userRoles, isAdmin = false } = user || {}

  // const orgRoles: UserRole[] = userRoles?.filter((userRole) => userRole.organization?.id === organization?.id) || []
  // TODO: replace line below with above and migrate userRole -> organizationUser
  const orgRoles: UserRole[] = userRoles ?? []
  const isOrgOwner = organization?.owner.id === user?.id
  const isOrgAdmin: boolean = isAdmin || Boolean(orgRoles.find((_orgRole) => _orgRole.role?.name === 'admin'))
  const orgRole = orgRoles.find((role) => role?.organization?.id === organization?.id)
  const isNabisDeliveries = organization?.id === NABIS_ORGANIZATION_TO_ID.DELIVERIES

  const shield = (roleNames: RoleName[]): boolean => isAdmin || Boolean(orgRoles && hasRole(orgRoles, roleNames))
  const canCreate = {
    inventory: shield(['admin', 'manager']),
    sku: shield(['admin', 'manager']),
    batch: shield(['admin', 'manager']),
    skuBatch: shield(['admin', 'manager']),
    order: shield(['admin', 'manager', 'user']),
    user: shield(['admin', 'manager']),
    licenseLocation: shield(['admin', 'manager', 'user']),
    license: shield(['admin', 'manager']),
    csvExport: shield(['admin', 'manager']),
    csvExportInventory: shield(['admin', 'manager', 'user']),
  }

  return {
    canCreate,
    isNabisDeliveries,
    isOrgAdmin,
    isOrgOwner,
    orgRole,
    orgRoles,
    organization,
    setOrganizationId,
  }
}

export const useOrganization = () => {
  const { organization, ...rest } = useOrganizationContext()
  return { ...rest, organization: organization as Organization } // Force cast to get non-undefined value
}
