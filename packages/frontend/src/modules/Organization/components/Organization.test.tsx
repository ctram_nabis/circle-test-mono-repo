import { shallow } from 'enzyme'
import React from 'react'
import { OrganizationInviteRow } from './OrganizationInviteRow'
import { OrganizationRow } from './OrganizationRow'

describe('Orders container', () => {
  describe('OrganizationInviteRow', () => {
    it('should render without crashing', () => {
      shallow(<OrganizationInviteRow />)
    })
  })

  describe('OrganizationRow', () => {
    it('should render without crashing', () => {
      shallow(<OrganizationRow />)
    })
  })
})
