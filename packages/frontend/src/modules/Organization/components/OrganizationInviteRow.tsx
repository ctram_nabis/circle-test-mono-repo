import { S } from 'components'
import { OrganizationInvite, useUpdateOrganizationInviteMutation } from 'generated/graphql'
import { useRouter } from 'modules/Navigation'
import React from 'react'
import { useOrganization } from '../useOrganization'

interface Props {
  invitation: OrganizationInvite
}

export const OrganizationInviteRow = ({ invitation }: Props) => {
  const router = useRouter()
  const { setOrganizationId } = useOrganization()
  const [updateInvitationMutation, { loading }] = useUpdateOrganizationInviteMutation()

  const { organization, sender } = invitation

  const handleUpdateInvitation = async (accepted: boolean) => {
    await updateInvitationMutation({
      variables: {
        input: {
          id: invitation.id,
          accepted,
        },
      },
    })

    if (accepted) {
      setOrganizationId(invitation.organization.id)
      router.push(`/${invitation.organization.alias}/app/dashboard`)
    }
  }

  return (
    <S.Table.Row>
      <S.Table.Cell>
        <>{organization.name}</>
      </S.Table.Cell>
      <S.Table.Cell>
        <>{sender.email}</>
      </S.Table.Cell>
      <S.Table.Cell>
        <S.Button
          loading={loading}
          disabled={loading}
          color="green"
          size="small"
          icon="check"
          title="Accept"
          onClick={() => handleUpdateInvitation(true)}
        />
        <S.Button
          loading={loading}
          disabled={loading}
          color="red"
          size="small"
          icon="x"
          title="Reject"
          onClick={() => handleUpdateInvitation(false)}
        />
      </S.Table.Cell>
    </S.Table.Row>
  )
}
