import { Button } from 'components/Button'
import { S } from 'components/SemanticComponents'
import { Organization } from 'generated/graphql'
import { Images } from 'images'
import { useRouter } from 'modules/Navigation'
import React from 'react'
import { Item } from 'semantic-ui-react'
import styled from 'styled-components'

const ActionContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  margin-left: 35px;
`

const ItemImage = styled(Item.Image)`
  padding: 8px;
`

interface Props {
  organization: Organization
}

export const OrganizationRow = ({ organization }: Props) => {
  const router = useRouter()

  const { alias, name, doingBusinessAs, owner } = organization

  const goToDashBoard = () => router.push(`${alias}/app/dashboard`)

  return (
    <S.Item data-test="organization-row">
      <ItemImage size="tiny" src={Images.nabis_logo_icon} onClick={goToDashBoard} />
      <S.Item.Content verticalAlign="middle" onClick={goToDashBoard}>
        <S.Item.Header as="a">{name}</S.Item.Header>
        <S.Divider fitted />
        <S.Item.Description>
          <S.Header as="h5">DBA: {doingBusinessAs || '---'}</S.Header>
          Owner: {(owner && owner.email) || '---'}
        </S.Item.Description>
      </S.Item.Content>
      <ActionContainer>
        <Button color="yellow" onClick={() => router.push(`/${alias}/app/edit`)} title="Edit" />
      </ActionContainer>
    </S.Item>
  )
}
