import { Organization, User } from 'generated/graphql'

export interface AddressPOC {
  address1?: string | null
  address2?: string | null
  city?: string | null
  state?: string | null
  zip?: string | null
  contactName?: string | null
  contactPhone?: string | null
}

export function createAddressForLicense(organization: Partial<Organization>, user?: Partial<User> | null): AddressPOC {
  const { address1, address2, city, state, zip, owner, phone: contactPhone } = organization

  const { firstName, lastName } = user || owner || {}
  const contactName = `${firstName} ${lastName}`
  return {
    address1,
    address2,
    city,
    state,
    zip,
    contactName,
    contactPhone,
  }
}
