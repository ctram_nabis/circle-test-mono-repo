import { FullScreenLoader } from 'components/FullScreenLoader'
import { Organization, useOrganizationLazyQuery } from 'generated/graphql'
import { useMe } from 'modules/User'
import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { OrganizationContext } from './OrganizationContext'

export const withOrganizationProvider = <P extends object>(Component: React.ComponentType<P>) =>
  function WrappedComponent(props: P) {
    const { alias } = useParams()
    const { user } = useMe()
    const userOrg = user?.organizations?.find((org) => org.alias === alias)
    const defaultOrgId = userOrg?.id
    const [organizationId, setOrganizationId] = React.useState<string | undefined>(defaultOrgId)
    const [organization, setOrganization] = React.useState<Organization | undefined>(undefined)

    const [organizationQuery, { data, loading }] = useOrganizationLazyQuery()

    useEffect(() => {
      if (organizationId) {
        organizationQuery({
          variables: {
            id: organizationId,
          },
        })
      }
    }, [alias, organizationId, organizationQuery])

    useEffect(() => {
      const _organization = data?.getOrganization as Organization
      setOrganization(_organization)
    }, [setOrganization, data])

    return (
      <OrganizationContext.Provider value={{ organization, setOrganizationId }}>
        {loading && !alias ? <FullScreenLoader /> : <Component {...props} />}
      </OrganizationContext.Provider>
    )
  }
