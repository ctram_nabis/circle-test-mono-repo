import { Organization } from 'generated/graphql'
import React from 'react'

export interface OrganizationContextType {
  organization?: Organization
  setOrganizationId: (id: string) => void
}

export const OrganizationContext = React.createContext<OrganizationContextType>({ setOrganizationId: () => {} })
