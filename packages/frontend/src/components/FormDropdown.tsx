import { FormikErrors } from 'formik'
import React from 'react'
import { ErrorType } from 'types'
import { View } from './core/src/View'
import { Dropdown, DropdownProps } from './Dropdown'
import { ErrorBox } from './ErrorBox'
import { Field } from './Field'
import { Label } from './Label'

export interface FormDropdownProps<Values> extends DropdownProps {
  label?: string
  name: keyof Values
  errors?: FormikErrors<Values>
  errorMsg?: ErrorType
  required?: boolean
}

export const FormDropdown = <T extends object>({ errors, label, name, errorMsg, ...props }: FormDropdownProps<T>) => {
  return (
    <Field>
      <View my={2} flex={1} display="flex" flexDirection="column">
        {label && <Label htmlFor={name as string}>{label}</Label>}
        <Dropdown {...props} />
        <ErrorBox error={errorMsg} errors={errors && (errors[name] as string[])} />
      </View>
    </Field>
  )
}
