import { Box, BoxProps } from 'components/core/src/Box'
import { Text } from 'components/core/src/Text'
import { Spinner, SpinnerProps } from 'components/Spinner'
import React from 'react'
import styled from 'styled-components'

const Container = styled(Box)``

Container.defaultProps = {
  center: true,
  middle: true,
}

interface Props extends SpinnerProps {
  loading?: boolean
  containerProps?: BoxProps
  label?: string
}

export const Loading = ({ loading = true, label, containerProps, ...props }: Props) =>
  loading ? (
    <Container {...containerProps}>
      <Spinner {...props} />
      {label && <Text>{label}</Text>}
    </Container>
  ) : null
