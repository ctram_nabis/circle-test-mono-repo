import styled from 'styled-components'

import { styleView, ViewProps } from 'system/styled'

export const TextInput = styled.input<ViewProps>(styleView)
