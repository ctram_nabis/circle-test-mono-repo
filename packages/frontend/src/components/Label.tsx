import { styleText, TextProps } from 'components/core'
import styled from 'styled-components'

export const Label = styled.label<TextProps>(styleText)

Label.defaultProps = {
  fontWeight: 'bold',
  fontSize: [1, 2],
  display: 'block',
  my: [1, 2],
}
