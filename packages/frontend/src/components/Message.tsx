import React from 'react'
import styled from 'styled-components'
import { styleView, ViewProps } from 'system'
import { S } from './SemanticComponents'

export const Message = styled(S.Message)<ViewProps>(styleView)

export type MessageProps = React.ComponentProps<typeof Message>

Message.defaultProps = {}
