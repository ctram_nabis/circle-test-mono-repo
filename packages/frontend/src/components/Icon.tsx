/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */

import { styleView, ViewProps } from 'components/core'
import { icons } from 'components/Icons'
import React from 'react'
import * as Feather from 'react-feather'
import styled from 'styled-components'
import { Color, colors } from 'system/theme'

export const iconSizes = {
  tiny: 16,
  small: 20,
  regular: 24,
  medium: 32,
  large: 36,
  xlarge: 48,
  jumbo: 64,
}

export type Icon = keyof typeof icons
export type IconSize = keyof typeof iconSizes

export type IconProps = Omit<Feather.Props, 'color'> & {
  name?: Icon
  size?: IconSize
  color?: Color
  fillColor?: Color
} & ViewProps

export const Icon = ({
  name = 'circle',
  size = 'regular',
  color = 'text',
  fillColor = 'transparent',
  ...props
}: IconProps) => {
  const strokeIconColor = colors[color]
  const fillIconColor = colors[fillColor]
  const iconSize = iconSizes[size]
  const iconComponent = icons[name]

  const IconComponent = styled(iconComponent)<ViewProps>(styleView)

  return <IconComponent {...props} color={strokeIconColor} fill={fillIconColor} size={iconSize} />
}

Icon.displayName = 'Icon'
