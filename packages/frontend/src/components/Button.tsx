// TODO: This is temporary until Chris' work has been pushed up. Looked on the 7 branch.
// TODO: Unfortunately the right now there is the button that i would like to use that doesn't break the build,
// TODO: so creating this here
// TODO: So that i won't conflict wioth someone else and i won't be blocked.
// TODO: I know this in't the best solution

import { S } from 'components/SemanticComponents'
import React from 'react'

export type ButtonProps = S.ButtonProps

interface Props extends ButtonProps {
  iconProps?: S.IconProps
  name?: S.IconProps['name']
  title?: string
}

export const Button = ({ name, title, iconProps, ...props }: Props) => (
  <S.Button {...props}>
    {name && <S.Icon name={name} {...iconProps} />}
    {title || props.children}
  </S.Button>
)
