import React from 'react'
import styled from 'styled-components'
import { styleView, ViewProps } from '../system'
import { S } from './SemanticComponents'

export const Grid = styled(S.Grid)<ViewProps>(styleView)

export type GridProps = React.ComponentProps<typeof Grid>

Grid.defaultProps = {}
