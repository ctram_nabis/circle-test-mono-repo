import React from 'react'

import { ErrorLabel } from 'components/ErrorLabel'
import { Message } from 'components/Message'
import logger from 'modules/Logger'
import { oc } from 'ts-optchain'
import { ErrorType } from 'types'

export interface Props {
  header?: string
  errors?: ErrorType[]
  error?: ErrorType
}

export class ErrorBox extends React.Component<Props, { hasError: boolean }> {
  constructor(props: Props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError(_error: ErrorType) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true }
  }

  componentDidCatch(error: ErrorType, errorInfo: any) {
    // You can also log the error to an error reporting service
    logger.error(error, errorInfo)
  }

  render() {
    const { errors, error, header } = this.props
    if (!errors && !error) return null

    const errorStrings = oc(errors)([]).map((err) => (typeof err !== 'string' ? err.message : err))

    return (
      <Message error visible header={header}>
        <ErrorLabel error={error} />
        {errorStrings.map((err, i) => (
          <ErrorLabel key={`error-${i}`} error={`- ${err}`} />
        ))}
      </Message>
    )
  }
}
