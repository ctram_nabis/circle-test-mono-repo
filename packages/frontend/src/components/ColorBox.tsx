import { Box } from 'components/core/src/Box'
import styled from 'styled-components'
import { Color } from 'system/theme'
import { getColor, lightColor } from 'system/utils'

export interface ColorBoxProps {
  info?: boolean
  warning?: boolean
  success?: boolean
  error?: boolean
}

export const ColorBox = styled(Box)<ColorBoxProps>((props) => {
  const { info, warning, success, error } = props
  let color: Color = 'shade7'

  if (info) color = 'info'
  if (warning) color = 'warning'
  if (success) color = 'success'
  if (error) color = 'error'

  const borderColor = getColor(color, props)
  const backgroundColor = lightColor(color)(props)

  return {
    borderWidth: '1px',
    borderStyle: 'solid',
    borderColor,
    backgroundColor,
  }
})
