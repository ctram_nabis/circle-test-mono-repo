import { Text } from 'components/core'
import styled from 'styled-components'
import { styleText, TextProps } from 'components/core/src/Text'

export const H3 = styled(Text)<TextProps>(styleText)

H3.displayName = 'H3'
H3.defaultProps = {
  fontSize: [4, 5],
  lineHeight: 1.125,
}
