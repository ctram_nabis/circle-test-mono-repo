import { styleView, ViewProps } from 'components/core'
import { ImageName, Images } from 'images'
import React from 'react'
import styled from 'styled-components'
import { S } from './SemanticComponents'

export interface ImageProps extends Omit<S.StrictImageProps, 'color' | 'verticalAlign' | 'size'>, ViewProps {
  onClick?: () => void
  name?: ImageName
  src?: string
  alt?: string
}

const Img: React.FC<ImageProps> = styled(S.Image)<ViewProps>(styleView)

export const Image: React.FC<ImageProps> = ({ name, ...props }) => <Img src={name && Images[name]} {...props} />
