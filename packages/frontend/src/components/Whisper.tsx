import { Box } from 'components/core/src/Box'
import styled from 'styled-components'
import { getColor } from 'system'

export const Whisper = styled(Box)`
  color: ${(props) => getColor('shade6', props)};
  font-size: 16px;
  font-weight: 400;
`
