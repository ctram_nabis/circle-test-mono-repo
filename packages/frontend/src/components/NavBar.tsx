import { Button } from 'components/Button'
import { View } from 'components/core/src/View'
import { Dropdown } from 'components/Dropdown'
import { Image } from 'components/Image'
import { S } from 'components/SemanticComponents'
import { useSignOut } from 'hooks/useSignOut'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import { useMe } from 'modules/User'
import React from 'react'
import styled from 'styled-components'
import { oc } from 'ts-optchain'

const NavContainer = styled(View)``
NavContainer.defaultProps = {
  height: 50,
}

const NavContainerInner = styled(View)``
NavContainerInner.defaultProps = {
  alignItems: 'center',
  display: 'flex',
  flex: 1,
  py: 1,
  position: 'fixed',
  top: 0,
  left: 0,
  right: 0,
  zIndex: 1000,
  height: 50,
  bg: 'white',
  border: '1px solid rgba(0,0,0,0.15)',
}

const NavItemContainer = styled(View)``
NavItemContainer.defaultProps = {
  alignItems: 'center',
  display: 'flex',
  flex: '1 0 0',
  justifyContent: 'flex-end',
}

const NavItem = styled(View)``
NavItem.defaultProps = {
  p: 2,
}

export const NavBar = () => {
  const router = useRouter()
  const { user, isAuthenticated } = useMe()
  const { organization, canCreate } = useOrganization() // orgRole orgRole keeps getting set to undefined
  const userRole = user?.roles[0] // switched to this once working orgRole?.role
  const signOut = useSignOut()

  const getNavBarButton = () => {
    switch (router?.match?.path) {
      case '/account':
      case `/${organization.alias}/app/edit`:
        return (
          <NavItem onClick={() => router.push(`/my-organizations`)}>
            <Button color="green">My Organizations</Button>
          </NavItem>
        )
      case '/my-organizations':
        return (
          <NavItem onClick={() => router.push(`/account`)}>
            <Button color="green">My Account</Button>
          </NavItem>
        )
      default:
        return (
          canCreate.order && (
            <NavItem onClick={() => router.push(`/${oc(organization).alias()}/app/orders/new`)}>
              <Button color="green">New Order</Button>
            </NavItem>
          )
        )
    }
  }

  const NavBarLogo = () => (
    <Image name="nabis_logo" height={40} width={150} p={2} pr={0} pl={0} onClick={() => router.replace('/')} />
  )

  return (
    <NavContainer>
      <NavContainerInner>
        <NavBarLogo />
        {isAuthenticated && (
          <>
            {user && organization ? (
              <>
                <S.Label>{organization.name}</S.Label>
                <NavItemContainer>
                  <S.Popup
                    content={userRole?.description}
                    trigger={<S.Label data-test="label-user-role">{userRole?.name}</S.Label>}
                    position="bottom center"
                  />
                  {getNavBarButton()}
                  <NavItem>
                    <Dropdown
                      text={user.firstName && user.lastName ? `${user.firstName} ${user.lastName}` : 'User'}
                      pointing="top right"
                      button
                      background="transparent"
                    >
                      <Dropdown.Menu>
                        <Dropdown.Item text="Account Settings" onClick={() => router.push('/account')} />
                        <Dropdown.Item
                          text="Organization Settings"
                          onClick={() => router.push(`/${organization.alias}/app/edit`)}
                        />
                        <Dropdown.Item text="My Organizations" onClick={() => router.replace('/my-organizations')} />
                        <Dropdown.Divider />
                        <Dropdown.Item text="Help Center" onClick={() => window.open('https://nabis.zendesk.com/')} />
                        <Dropdown.Item text="Sign Out" onClick={() => signOut()} />
                      </Dropdown.Menu>
                    </Dropdown>
                  </NavItem>
                </NavItemContainer>
              </>
            ) : (
              <NavItemContainer>
                <NavItem>
                  <Button
                    color="green"
                    onClick={() => router.push(router?.match?.path === '/account' ? '/my-organizations' : '/account')}
                    title={router?.match?.path === '/account' ? 'My Organizations' : 'My Account'}
                  />
                </NavItem>
                <NavItem>
                  <Button onClick={() => signOut()} title="Sign Out" basic color="black" />
                </NavItem>
              </NavItemContainer>
            )}
          </>
        )}
      </NavContainerInner>
    </NavContainer>
  )
}
