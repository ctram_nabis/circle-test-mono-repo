import { Box, Text } from 'components/core'
import { FormikErrors } from 'formik'
import React from 'react'
import { ErrorType } from 'types'

interface Props {
  error?: ErrorType | FormikErrors<object>
  errors?: ErrorType[]
}

export const ErrorLabel: React.FC<Props> = ({ error, errors, ...props }) => {
  if (error) {
    return (
      <Text color="error" {...props}>
        {error}
      </Text>
    )
  }
  if (errors) {
    return (
      <Box>
        {errors.map((err, i) => (
          <Text key={`error-${i}`} color="error" {...props}>
            {err}
          </Text>
        ))}
      </Box>
    )
  }
  return null
}
