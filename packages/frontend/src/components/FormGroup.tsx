import { styleView, ViewProps } from 'components/core'
import styled from 'styled-components'
import { S } from './SemanticComponents'

// export interface FormGroupProps extends S.StrictFormGroupProps, ViewProps {}
export type FormGroupProps = S.StrictFormGroupProps & ViewProps
export const FormGroup = styled(S.Form.Group)<FormGroupProps>(styleView)
