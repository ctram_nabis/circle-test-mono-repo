import { ViewProps } from 'components/core/src/View'
import React from 'react'
import styled from 'styled-components'
import { styleView } from 'system/styled'
import { S } from './SemanticComponents'

export const ButtonGroup = styled(S.ButtonGroup)<ViewProps>(styleView)

export type ButtonGroupProps = React.ComponentProps<typeof ButtonGroup>

ButtonGroup.defaultProps = {
  // display: 'flex',
  // flexDirection: 'row',
}
