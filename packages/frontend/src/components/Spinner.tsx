/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */
import { iconSizes } from 'components'
import { View, ViewProps } from 'components/core/src/View'
import React from 'react'
import styled, { keyframes } from 'styled-components'
import { Color } from 'system/theme'

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`

const SpinnerBase = styled(View)`
  border-style: solid;
  border-radius: 50%;
  animation: ${spin} 1s linear infinite;
`

export interface SpinnerProps extends Omit<ViewProps, 'size'> {
  baseColor?: Color
  thickness?: number
  size?: keyof typeof iconSizes
}

export const Spinner = ({
  color = 'accent1',
  baseColor = 'transparent',
  thickness = 2,
  size = 'regular',
  ...props
}: SpinnerProps) => {
  const iconSize = iconSizes[size]

  return (
    <SpinnerBase
      {...props}
      height={iconSize}
      width={iconSize}
      borderWidth={thickness}
      borderBottomColor={baseColor}
      borderRightColor={color}
      borderTopColor={color}
      borderLeftColor={color}
    />
  )
}

Spinner.displayName = 'Spinner'
