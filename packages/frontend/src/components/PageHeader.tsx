import styled from 'styled-components'
import { styleText, TextProps } from 'system'

export const PageHeader = styled.h1<TextProps>(styleText)

PageHeader.displayName = 'PageHeader'
PageHeader.defaultProps = {
  fontSize: 4,
  lineHeight: 1.3,
}
