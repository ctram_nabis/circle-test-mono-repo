import { Box } from 'components/core/src/Box'
import styled from 'styled-components'

export const Col = styled(Box)``

Col.displayName = 'Col'
Col.defaultProps = {
  px: 3,
  flex: '1 1 auto',
}
