import { styleView, ViewProps } from 'components/core'
import React from 'react'
import styled from 'styled-components'
import { S } from './SemanticComponents'

export const D = styled(S.Dropdown)<ViewProps>(styleView)

export const Dropdown = (props: DropdownProps) => {
  return <D {...props} />
}

export const DropdownMenu = styled(S.DropdownMenu)<ViewProps>(styleView)
export const DropdownItem = styled(S.DropdownItem)<ViewProps>(styleView)
export const DropdownDivider = styled(S.DropdownDivider)<ViewProps>(styleView)

Dropdown.Menu = DropdownMenu
Dropdown.Item = DropdownItem
Dropdown.Divider = DropdownDivider

export interface DropdownProps extends S.StrictDropdownProps, ViewProps {}
export interface DropdownMenuProps extends S.StrictDropdownMenuProps, ViewProps {}
export interface DropdownItemProps extends S.StrictDropdownItemProps, ViewProps {}
export interface DropdownDividerProps extends S.StrictDropdownDividerProps, ViewProps {}
