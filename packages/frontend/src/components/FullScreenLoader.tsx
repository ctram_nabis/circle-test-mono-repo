/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */
import React from 'react'

import { Fullscreen } from 'components/FullScreen'
import { Spinner } from 'components/Spinner'
import { Box, Text } from './core'

interface FullScreenLoaderProps extends React.PropsWithChildren<any> {
  loading?: boolean
  loadingText?: string
}

export const FullScreenLoader = ({ loading = true, loadingText, children = null }: FullScreenLoaderProps) =>
  !loading ? (
    children
  ) : (
    <Fullscreen>
      <Box center middle>
        <Spinner />{' '}
        {loadingText && (
          <Text p={2} bold>
            {loadingText}
          </Text>
        )}
      </Box>
    </Fullscreen>
  )
