import React from 'react'
import styled from 'styled-components'
import { styleView, ViewProps } from 'system'
import { S } from './SemanticComponents'

export const Popup = styled(S.Popup)<ViewProps>(styleView)

export type PopupProps = React.ComponentProps<typeof Popup>

Popup.defaultProps = {
  // display: 'flex',
  // flexDirection: 'row',
}

export const PopupContent = styled(S.Popup.Content)<ViewProps>(styleView)

export type PopupContentProps = React.ComponentProps<typeof PopupContent>

PopupContent.defaultProps = {
  // display: 'flex',
  // flexDirection: 'row',
}
