import styled from 'styled-components'
import { styleView, ViewProps } from 'system/styled'

export const Svg = styled.svg<ViewProps>(styleView)

Svg.defaultProps = {
  size: 24,
}
