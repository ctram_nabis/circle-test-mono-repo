import React from 'react'
import styled from 'styled-components'
import { styleView, ViewProps } from 'system'
import { S } from './SemanticComponents'

export const TextArea = styled(S.TextArea)<ViewProps>(styleView)

export type TextAreaProps = React.ComponentProps<typeof TextArea>

TextArea.defaultProps = {}
