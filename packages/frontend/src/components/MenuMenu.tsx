import { styleView, ViewProps } from 'components/core/src/View'
import styled from 'styled-components'
import { S } from './SemanticComponents'

export interface MenuMenuProps extends Omit<S.StrictMenuMenuProps, 'position' | 'size'>, ViewProps {}
export const MenuMenu = styled(S.MenuMenu)<MenuMenuProps>(styleView)
