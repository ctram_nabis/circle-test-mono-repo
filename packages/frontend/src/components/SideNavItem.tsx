import { Icon } from 'components/Icon'
import { MenuItem } from 'components/MenuItem'
import { IconName } from 'components/Icons'
import { useRouter } from 'modules/Navigation'
import { useOrganization } from 'modules/Organization'
import React from 'react'
import styled from 'styled-components'
import { darkColor, getColor } from 'system/utils'
import { Box, Text } from './core'

export interface NavItemProps {
  label: string
  icon: IconName
  route: string
}

export const StyledMenuItem = styled(MenuItem)<{ isCurrentRoute: boolean }>`
  background-color: ${({ isCurrentRoute, ...props }) =>
    isCurrentRoute ? darkColor('success', 0.1)(props) : getColor('success', props)};
`

export const SideNavItem = (props: NavItemProps) => {
  const { route, icon, label } = props
  const router = useRouter()
  const { organization } = useOrganization()

  const { alias } = organization || {}
  const orgRoute = `/${alias}/app/${route}`
  const isCurrentRoute = router.pathname === orgRoute

  return (
    <StyledMenuItem isCurrentRoute={isCurrentRoute} onClick={() => router.push(orgRoute)} data-test="side-nav-item">
      <Box py={2} height={80} justifyContent="center" alignItems="center" display="flex" flexDirection="column">
        <Icon name={icon} color="white" mb={2} />
        <Box px={1}>
          <Text color="white" fontWeight="bold" fontSize={12} lineHeight={1} textAlign="center">
            {label}
          </Text>
        </Box>
      </Box>
    </StyledMenuItem>
  )
}
