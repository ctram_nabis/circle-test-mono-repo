import { ViewProps } from 'components/core/src/View'
import React from 'react'
import styled from 'styled-components'
import { S } from './SemanticComponents'

export const Input = styled(S.Input)<ViewProps>``

export type InputProps = React.ComponentProps<typeof Input>

Input.defaultProps = {
  // display: 'block',
  // width: '100%',
  // p: 1,
  // appearance: 'none',
  // fontSize: 'inherit',
  // lineHeight: 'inherit',
  // border: 'none',
  // borderRadius: 4,
  // color: 'inherit',
  // bg: 'accent1',
}
