import React from 'react'
import styled from 'styled-components'
import { styleView, ViewProps } from 'system'
import { S } from './SemanticComponents'

export const Tab = styled(S.Tab)<ViewProps>(styleView)

export type TabProps = React.ComponentProps<typeof Tab>

Tab.defaultProps = {}
