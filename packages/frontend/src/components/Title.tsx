import { Text } from 'components/core/src/Text'
import styled from 'styled-components'
import { styleText, TextProps } from 'system'

export const Title = styled(Text)<TextProps>(styleText)

Title.displayName = 'Title'
Title.defaultProps = {
  fontSize: 1,
  lineHeight: 1.3,
}
