import { Box } from 'components/core/src/Box'
import styled from 'styled-components'

export const Screen = styled(Box)``

Screen.defaultProps = {
  display: 'flex',
  width: '100%',
  height: '100%',
}
