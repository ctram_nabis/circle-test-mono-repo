import { Text } from 'components/core'
import styled from 'styled-components'
import { styleText, TextProps } from 'components/core/src/Text'

export const H2 = styled(Text)<TextProps>(styleText)

H2.displayName = 'H2'
H2.defaultProps = {
  fontSize: [5, 6],
  lineHeight: 1.125,
}
