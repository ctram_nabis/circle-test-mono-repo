import { Button, ButtonProps } from 'components/Button'
import { Box, BoxProps } from 'components/core/src/Box'
import React from 'react'
import styled from 'styled-components'

const ExitWrapper = styled(Box)`
  opacity: 0.4;
  position: absolute;
  right: 5px;
  transition: all 0.2s ease-in;
  z-index: 10000;
  &:hover {
    opacity: 1;
  }
`

type Props = ButtonProps & { containerProps?: BoxProps }

export const Exit = ({ containerProps, ...props }: Props) => (
  <ExitWrapper {...containerProps}>
    <Button size="small" name="x" {...props} />
  </ExitWrapper>
)
