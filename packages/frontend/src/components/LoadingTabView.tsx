import { Box } from 'components/core'
import { Screen } from 'components/Screen'
import React from 'react'

export const LoadingTabView = () => (
  <Screen center middle>
    <Box>Loading...</Box>
  </Screen>
)
