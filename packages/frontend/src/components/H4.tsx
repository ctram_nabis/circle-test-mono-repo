import { Text } from 'components/core'
import styled from 'styled-components'
import { styleText, TextProps } from 'components/core/src/Text'

export const H4 = styled(Text)<TextProps>(styleText)

H4.displayName = 'H4'
H4.defaultProps = {
  fontSize: [3, 4],
  lineHeight: 1.125,
}
