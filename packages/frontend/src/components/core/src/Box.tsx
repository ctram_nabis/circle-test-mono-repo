import styled from 'styled-components'
import { styleView, View, ViewProps } from './View'

export const Box = styled(View)<ViewProps>(styleView)

export type BoxProps = ViewProps
