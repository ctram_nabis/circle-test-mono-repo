/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */
import css, { CSSObject } from '@styled-system/css'
import styled from 'styled-components'
import {
  color,
  display,
  DisplayProps,
  flexbox,
  FlexboxProps,
  position,
  PositionProps,
  space,
  SpaceProps,
  TextColorProps,
  textStyle,
  TextStyleProps,
  typography,
  TypographyProps,
  variant,
} from 'styled-system'

import compose from './compose'

export interface TextProps
  extends SpaceProps,
    FlexboxProps,
    DisplayProps,
    PositionProps,
    TextColorProps,
    TypographyProps,
    TextStyleProps {
  __css?: TextProps & CSSObject
  __themeKey?: string
  className?: string
  center?: boolean
  inline?: boolean
  underline?: boolean
  clickable?: boolean
  bold?: boolean
}

export const styleText = compose(
  space,
  flexbox,
  position,
  color,
  display,
  variant,
  typography,
  textStyle,
  (p: TextProps) => p.__css && css(p.__css),
  (p: TextProps) => p.bold && { fontWeight: 'bold' },
  (p: TextProps) => p.center && { alignItems: 'center', textAlign: 'center' },
  (p: TextProps) => p.inline && { display: 'inline' },
  (p: TextProps) => p.underline && { textDecoration: 'underline' },
  (p: TextProps) => p.clickable && { cursor: 'pointer' },
)

export const Text: React.FC<TextProps> = styled.div<TextProps>(styleText)
