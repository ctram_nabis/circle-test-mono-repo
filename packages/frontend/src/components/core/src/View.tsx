/**
 *
 *
 * NOTE: Created By Chris Bull - ✋ Do not modify unless asking
 *
 *
 */
import css, { CSSObject } from '@styled-system/css'
import React, { ReactNode } from 'react'
import styled from 'styled-components'
import {
  background,
  BackgroundProps,
  border,
  borderColor,
  BorderProps,
  borders,
  BordersProps,
  buttonStyle,
  ButtonStyleProps,
  color,
  colorStyle,
  ColorStyleProps,
  flexbox,
  FlexboxProps,
  grid,
  GridProps,
  layout,
  LayoutProps,
  position,
  PositionProps,
  shadow,
  ShadowProps,
  space,
  SpaceProps,
  TextStyleProps,
  variant,
} from 'styled-system'
import { Color, Theme } from 'system/theme'
import { SxProps } from 'theme-ui'
import compose from './compose'

export interface ViewProps
  extends PositionProps,
    ButtonStyleProps,
    TextStyleProps,
    ColorStyleProps,
    BackgroundProps,
    ShadowProps,
    BorderProps,
    LayoutProps,
    GridProps,
    FlexboxProps,
    BordersProps,
    SpaceProps,
    SxProps {
  type?: string
  __css?: ViewProps & CSSObject
  __themeKey?: string
  className?: string
  theme?: Theme
  color?: Color
  // backgroundColor?: Color // -- dont know if this actually works
  borderColor?: Color
  bg?: Color
  css?: CSSObject
  center?: boolean
  middle?: boolean
  row?: boolean
  col?: boolean
  space?: 'between' | 'around' | 'evenly'
  inline?: boolean
  clickable?: boolean
  children?: ReactNode
  themeKey?: string
  cssStyle?: CSSObject
  variant?: string
  stretch?: boolean
}

export type OmitHeightWidth<T> = Omit<T, 'width' | 'height'>

export const styleView = compose(
  space,
  color,
  layout,
  flexbox,
  border,
  borders,
  borderColor,
  background,
  position,
  grid,
  shadow,
  buttonStyle,
  colorStyle,
  variant,
  (props: ViewProps) => props.sx && css(props.sx),
  (props: ViewProps) => props.css && css(props.css),
  (props: ViewProps) => props.row && { display: 'flex', flexDirection: 'row' },
  (props: ViewProps) => props.col && { display: 'flex', flexDirection: 'column' },
  (props: ViewProps) => props.center && { display: 'flex', alignItems: 'center' },
  (props: ViewProps) => props.middle && { display: 'flex', justifyContent: 'center' },
  (props: ViewProps) => props.stretch && { display: 'flex', alignItems: 'stretch' },
  (props: ViewProps) => props.space && { display: 'flex', justifyContent: `space-${props.space}` },
  (props: ViewProps) => props.inline && { display: 'inline' },
  (props: ViewProps) => props.clickable && { cursor: 'pointer' },
)

export const View = styled.div<ViewProps>(
  {
    boxSizing: 'border-box',
    margin: 0,
    minWidth: 0,
  },
  styleView,
)

export const TextBox = (props: ViewProps) => <View {...props} />
