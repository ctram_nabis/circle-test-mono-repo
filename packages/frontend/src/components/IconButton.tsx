import { Button } from 'components/Button'
import { Icon, IconProps } from 'components/Icon'
import React from 'react'

export type IconButtonProps = Omit<React.ComponentProps<typeof Button>, 'size'> & {
  name: IconProps['name']
  size: IconProps['size']
}

export const IconButton = ({ name, size, color, ...props }: IconButtonProps) => (
  <Button width={size} height={size} {...props}>
    <Icon name={name} size={size} />
  </Button>
)

IconButton.defaultProps = {
  size: 32,
}
