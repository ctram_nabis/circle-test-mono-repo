import { Text } from 'components/core'
import styled from 'styled-components'
import { styleText, TextProps } from 'components/core/src/Text'

export const H6 = styled(Text)<TextProps>(styleText)

H6.displayName = 'H6'
H6.defaultProps = {
  fontSize: [1, 2],
  lineHeight: 1.125,
}
