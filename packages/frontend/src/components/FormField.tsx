import { Field, Input, InputProps, TextArea, TextAreaProps } from 'components'
import { View } from 'components/core/src/View'
import { ViewProps } from 'components/core/src/View'
import { ErrorLabel } from 'components/ErrorLabel'
import { Label } from 'components/Label'
import { FormikErrors, FormikTouched } from 'formik'
import React from 'react'
import { StrictInputProps } from 'semantic-ui-react'
import { ChangeData, ChangeEvent } from 'types'

type ValueOption = string | number | Date | boolean

export interface FormFieldProps<Values extends object> extends StrictInputProps {
  autoComplete?: string
  disableAutoComplete?: boolean
  errorMsg?: string
  errors?: FormikErrors<Values>
  fieldStyles?: ViewProps
  label?: string
  labelStyles?: ViewProps
  name: keyof Values
  onBlur?: (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, data: InputProps | TextAreaProps) => void
  onChange?: (e: ChangeEvent, d: ChangeData) => void
  placeholder?: string
  required?: boolean
  textarea?: boolean
  type: 'text' | 'email' | 'password' | 'radio' | 'checkbox' | 'number' | 'file'
  value?: ValueOption
  values?: Values
  touched?: FormikTouched<Values>
  options?: Array<{ name: string; value: string }>
}

export const FormField = <T extends object>({
  autoComplete,
  disableAutoComplete,
  errorMsg,
  errors,
  fieldStyles,
  label,
  labelStyles,
  name,
  value,
  values,
  textarea = false,
  ...props
}: FormFieldProps<T>) => {
  return (
    <Field {...fieldStyles}>
      <View my={2} flex={1} display="flex" flexDirection="column">
        {label && <Label {...labelStyles}>{label}</Label>}
        {textarea ? (
          <TextArea
            autoComplete={autoComplete || (!disableAutoComplete && name)}
            value={(values && values[name]) || value}
            {...props}
          />
        ) : (
          <Input
            name={name}
            autoComplete={autoComplete || (!disableAutoComplete && name)}
            value={(values && values[name]) || value}
            {...props}
          />
        )}
        <ErrorLabel error={errors && errors[name]} />
      </View>
    </Field>
  )
}
