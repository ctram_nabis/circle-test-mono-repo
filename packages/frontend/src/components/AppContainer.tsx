import { View } from 'components/core/src/View'
import styled from 'styled-components'
import { styleView } from 'system/styled'

export const AppContainer = styled(View)(styleView)

AppContainer.defaultProps = {
  flexDirection: 'column',
  flex: '1 0',
  bg: 'shade1',
  minHeight: '100vh',
  display: 'flex',
}
