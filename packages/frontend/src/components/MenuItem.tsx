import { styleView, ViewProps } from 'components/core/src/View'
import styled from 'styled-components'
import { S } from './SemanticComponents'

export interface MenuItemProps extends Omit<S.StrictMenuItemProps, 'position' | 'color' | 'size'>, ViewProps {}
export const MenuItem: React.FC<MenuItemProps> = styled(S.MenuItem)<ViewProps>(
  styleView,
  (props) => props.onClick && { cursor: 'pointer' },
)

MenuItem.defaultProps = {
  borderBottom: '1px dotted #AAAAAA',
}
