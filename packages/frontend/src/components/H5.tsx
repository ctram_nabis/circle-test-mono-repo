import { Text } from 'components/core'
import styled from 'styled-components'
import { styleText, TextProps } from 'components/core/src/Text'

export const H5 = styled(Text)<TextProps>(styleText)

H5.displayName = 'H5'
H5.defaultProps = {
  fontSize: [2, 3],
  lineHeight: 1.125,
  fontWeight: 600,
}
