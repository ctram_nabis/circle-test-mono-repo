import { Box } from 'components/core'
import { Visible } from 'components/Visible'
import React from 'react'

export const MessageBox = () => (
  <Visible>
    <Box>
      <Box />
      <Box />
    </Box>
  </Visible>
)
