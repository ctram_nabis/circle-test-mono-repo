import React from 'react'
import styled from 'styled-components'
import { styleView, ViewProps } from 'system'
import { S } from './SemanticComponents'

export const Field = styled(S.FormField)<ViewProps>(styleView)

export type FieldProps = React.ComponentProps<typeof Field>
