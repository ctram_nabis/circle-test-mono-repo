import { View } from 'components/core/src/View'
import styled from 'styled-components'

export const Row = styled(View)``

Row.displayName = 'Row'
Row.defaultProps = {
  mx: -3,
  flexWrap: 'nowrap',
  display: 'flex',
  flexDirection: 'row',
  space: 'between',
}
