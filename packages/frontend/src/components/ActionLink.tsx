import React from 'react'
import { TextProps } from 'system'

interface Props extends Omit<TextProps, 'color'> {
  children?: React.ReactNode
}

export const ActionLink = ({ children, ...props }: Props) => <a {...props}>{children}</a>
