import { View } from 'components/core/src/View'
import styled from 'styled-components'

export const Wrapper = styled(View)``

Wrapper.displayName = 'Wrapper'
Wrapper.defaultProps = {
  col: true,
  bg: 'white',
  borderRadius: 3,
  boxShadow: `0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)`,
  padding: 5,
  width: '100%',
  maxWidth: 1200,
}
