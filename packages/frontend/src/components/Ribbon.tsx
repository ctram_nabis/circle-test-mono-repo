import { styleView, ViewProps } from 'components/core/src/View'
import { S } from 'components/SemanticComponents'
import styled from 'styled-components'

export const Ribbon = styled(S.Label)<ViewProps>(styleView)

Ribbon.defaultProps = {
  ribbon: true,
}
