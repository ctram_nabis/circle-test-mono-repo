import { Text } from 'components/core/src/Text'
import styled from 'styled-components'
import { styleText, TextProps } from 'system'

export const P = styled(Text)<TextProps>(styleText)

P.displayName = 'P'
P.defaultProps = {
  fontSize: [2, 3],
}
