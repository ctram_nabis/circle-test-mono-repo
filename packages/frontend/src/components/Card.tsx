import styled from 'styled-components'
import { styleView, View, ViewProps } from './core'

export const Card: React.FC<ViewProps> = styled(View)<ViewProps>(styleView)

Card.defaultProps = {
  col: true,
  bg: 'white',
  borderRadius: 3,
  boxShadow: `0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)`,
  padding: 5,
  width: '100%',
  maxWidth: 500,
}
