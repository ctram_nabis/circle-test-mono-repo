import { Box } from 'components/core/src/Box'
import styled from 'styled-components'
import { getColor } from 'system'

export const Link = styled(Box)`
  color: ${(props) => getColor('accent1', props)};
  cursor: pointer;
  display: inline;
  font-weight: 400;
  margin: 0px 5px;
  text-decoration: underline;
`
