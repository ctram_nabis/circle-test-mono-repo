import { Text } from 'components/core/src/Text'
import styled from 'styled-components'
import { styleText, TextProps } from 'system'

export const Small = styled(Text)<TextProps>(styleText)

Small.displayName = 'Small'
Small.defaultProps = {
  fontSize: [1, 2],
  lineHeight: 1.3,
}
