import { View } from 'components/core/src/View'
import styled from 'styled-components'

export const Content = styled(View)``

Content.defaultProps = {
  display: 'flex',
  flex: 1,
  py: 20,
  mx: 2,
  overflowX: 'auto',
}
