import React from 'react'
import styled from 'styled-components'
import { styleView, ViewProps } from 'system'
import { S } from './SemanticComponents'

export interface LocalPaginationProps {
  activePage?: number | string
  onPageChange: (event?: Event, data?: any) => void
  totalNumPages: number | string
  siblingRange?: number
}

export const StyledPagination = styled(S.Pagination)<ViewProps>(styleView)

export type PaginationProps = React.ComponentProps<typeof StyledPagination> & LocalPaginationProps

export const Pagination = (props: PaginationProps) => {
  const { activePage = 1, totalNumPages = 0, onPageChange } = props

  return totalNumPages > 1 ? (
    <StyledPagination
      totalPages={totalNumPages}
      activePage={activePage}
      onPageChange={(_e, d) => onPageChange && onPageChange(_e, d)}
      // TODO: Commented out below because we are adding unnecessary props to DOM element. Console warnings!
      // {...props}
    />
  ) : null
}
