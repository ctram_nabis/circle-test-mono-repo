import { Box } from 'components/core/src/Box'
import { useMe } from 'modules/User'
import React, { useState } from 'react'
import styled, { keyframes } from 'styled-components'
import { getColor } from 'system'
import { S } from './SemanticComponents'

const fadeIn = keyframes`
  from { opacity: 0; }
  to { opacity: 1; }
`

const Container = styled(S.Container)`
  display: inline-block;
  position: relative;
  padding: 4px;
  width: 40px;
  height: 40px;
`

const Image = styled.img`
  background-color: ${(props) => getColor('shade4', props)};
  border-radius: 50%;
  cursor: pointer;
  height: 32px;
  width: 32px;
`

const Tooltip = styled(Box)`
  top: -20px;
  left: 5px;
  animation: ${fadeIn} 0.3s;
  white-space: nowrap;
  font-size: 12px;
  color: white;
  position: absolute;
  background-color: #555;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 8px 0px;
  padding: 2px 5px;
  border-radius: 5px;
`

export const ProfilePhoto = () => {
  const [hover, setHover] = useState(false)
  const { user } = useMe()

  return (
    <Container onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)}>
      {user?.profilePicture ? (
        <Image src={user?.profilePicture} />
      ) : (
        <Box height="32" width="32" borderRadius="16" bg="accent1" center middle color="white">
          {user?.firstName?.substr(0, 1)}
          {user?.lastName?.substr(0, 1)}
        </Box>
      )}
      {hover && <Tooltip>{`${user?.firstName} ${user?.lastName}`}</Tooltip>}
    </Container>
  )
}
