import { ReactDatePickerProps as DatePickerProps } from 'react-datepicker'
export { default as DatePicker } from 'react-datepicker'

export type ReactDatePickerProps = DatePickerProps
