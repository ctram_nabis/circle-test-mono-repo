import { H1, P, Screen } from 'components'
import { Box } from 'components/core'
import React from 'react'

interface Props {
  error: string
}

export const ErrorTab = ({ error }: Props) => (
  <Screen center middle>
    <Box>
      <H1>Ooops, something went wrong!</H1>
      <P>{error}</P>
    </Box>
  </Screen>
)
