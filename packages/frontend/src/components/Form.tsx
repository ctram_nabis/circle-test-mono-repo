import { styleView, ViewProps } from 'components/core'
import React from 'react'
import styled from 'styled-components'
import { FormGroup } from './FormGroup'
import { S } from './SemanticComponents'

export interface FormProps extends ViewProps, Omit<S.StrictFormProps, 'size'> {
  id?: string
}

const SForm = styled(S.Form)<ViewProps>(styleView)
export const Form = (props: FormProps) => {
  return <SForm {...props} />
}

Form.Field = styled(S.FormField)<ViewProps>(styleView)
Form.Group = FormGroup

Form.defaultProps = {}

export default Form
