import { Text } from 'components/core'
import { styleText, TextProps } from 'components/core/src/Text'
import styled from 'styled-components'

export const H1 = styled(Text)<TextProps>(styleText)

H1.displayName = 'H1'
H1.defaultProps = {
  fontSize: [6, 7],
  lineHeight: 1.125,
}
