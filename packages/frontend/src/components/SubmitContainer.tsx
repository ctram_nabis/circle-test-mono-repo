import { Box } from 'components/core/src/Box'
import styled from 'styled-components'
export const SubmitContainer = styled(Box)`
  flex: 0 0 50px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 20px 0px;
`
