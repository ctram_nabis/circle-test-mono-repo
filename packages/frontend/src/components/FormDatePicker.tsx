import { Field } from 'components'
import React from 'react'

import { DatePicker, ReactDatePickerProps } from 'components/DatePicker'
import { ErrorLabel } from 'components/ErrorLabel'
import { Label } from 'components/Label'
import { Scalars } from 'generated/graphql'
import { SharedFormikProps } from 'modules/Forms'

export type FormDatePickerProps<Values extends object> = SharedFormikProps<Values, Scalars['DateTime']> &
  Omit<ReactDatePickerProps, 'name' | 'value'>

/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */

export const FormDatePicker = <T extends object>({
  errorMsg,
  errors,
  label,
  values,
  onChange,
  name,
  ...props
}: FormDatePickerProps<T>) => (
  <Field>
    {label && <Label>{label}</Label>}
    <DatePicker name={name as string} value={values[name]} onSelect={onChange} onChange={onChange} {...props} />
    <ErrorLabel error={(errors && errors[name]) || errorMsg} />
  </Field>
)
