import { NavItemProps, SideNavItem } from 'components/SideNavItem'
import { useMe } from 'modules/User'
import React from 'react'
import styled from 'styled-components'
import { View } from './core'

const navItems: NavItemProps[] = [
  {
    label: 'Home',
    icon: 'home',
    route: 'dashboard',
  },
  {
    label: 'Orders',
    icon: 'truck',
    route: 'orders',
  },
  {
    icon: 'edit',
    label: 'Drafts',
    route: 'drafts',
  },
  {
    icon: 'archive',
    label: 'Inventory',
    route: 'inventory',
  },
  {
    icon: 'clipboard',
    label: 'Aging Reports',
    route: 'aging-reports',
  },
  {
    icon: 'users',
    label: 'Team',
    route: 'team',
  },
  {
    icon: 'book',
    label: 'Address Book',
    route: 'addresses',
  },
]

const adminNavItems: NavItemProps[] = [
  {
    icon: 'star',
    label: 'Admin Orders',
    route: 'admin-orders',
  },
  {
    icon: 'star',
    label: 'Admin Inventory',
    route: 'admin-inventory',
  },
  {
    icon: 'star',
    label: 'Admin Invoices',
    route: 'admin-outstanding-invoices',
  },
  {
    icon: 'star',
    label: 'Drivers & Vehicles',
    route: 'drivers-vehicles',
  },
]

const SideNavContainer = styled(View)``
SideNavContainer.defaultProps = {
  bg: 'success',
  width: 75,
  flexDirection: 'column',
  overflowY: 'auto',
}

const SideNavInner = styled(View)``
SideNavInner.defaultProps = {
  display: 'flex',
  flexDirection: 'column',
  width: 75,
  bg: 'success',
  position: 'fixed',
  bottom: 0,
  left: 0,
  top: 50,
  overflowY: 'auto',
}

export const SideNav = () => {
  const { user } = useMe()

  return (
    <SideNavContainer>
      <SideNavInner>
        {navItems.map((item, i) => (
          <SideNavItem key={`nav-item-${i}`} {...item} />
        ))}
        {user?.isAdmin && adminNavItems.map((item, i) => <SideNavItem key={`nav-item-${i}`} {...item} />)}
      </SideNavInner>
    </SideNavContainer>
  )
}
