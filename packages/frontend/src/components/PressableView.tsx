import { Box, BoxProps } from 'components/core'
import posed from 'react-pose'

export const PressableView = posed<BoxProps>(Box)({
  pressable: true,
  init: { scale: 1 },
  press: { scale: 0.8 },
})
