import { Button, Popup } from 'components'
import { ExportToCsv } from 'export-to-csv'
import moment from 'moment'
import React, { useState } from 'react'

interface CSVExportButtonProps {
  data: any[]
  fetchFn: () => any
  fileName?: string
}

export function CSVExportButton({ data, fetchFn, fileName }: CSVExportButtonProps) {
  const [loading, setLoading] = useState(false)

  const csvExport = (_data) => {
    if (!_data.length) return

    const dateStr = `${moment().format('MMDDYYYY')}`

    const csvExporter = new ExportToCsv({
      filename: fileName ? `${fileName}-${dateStr}` : `export-${dateStr}`,
      useBom: true,
      useKeysAsHeaders: true,
    })

    csvExporter.generateCsv(_data)
  }

  const handleClick = async () => {
    setLoading(true)
    if (data.length === 0 && fetchFn) {
      const fetchedData = await fetchFn()
      csvExport(fetchedData)
      setLoading(false)
    } else {
      csvExport(data)
      setLoading(false)
    }
  }

  return (
    <Popup
      trigger={
        <Button basic color="green" floated="right" onClick={handleClick} loading={loading} disabled={loading}>
          Export CSV
        </Button>
      }
      content="You might have to disable your ad-blocker if you have not already!"
      position="bottom center"
    />
  )
}
