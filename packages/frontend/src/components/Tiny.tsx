import { Text } from 'components/core/src/Text'
import styled from 'styled-components'
import { styleText, TextProps } from 'system'

export const Tiny = styled(Text)<TextProps>(styleText)

Tiny.displayName = 'Tiny'
Tiny.defaultProps = {
  fontSize: [0, 1],
  lineHeight: 1.3,
}
