import { View } from 'components/core/src/View'
import styled from 'styled-components'

export const Body = styled(View)``

Body.defaultProps = {
  display: 'flex',
  flex: 1,
  position: 'relative',
}
