import { Icon } from 'components/Icon'
import { Popup, PopupContent } from 'components/Popup'
import React from 'react'
import { Color } from 'system'

export const HelpIcon = ({ message, color }: { message: string; color: Color }) => {
  return (
    <Popup trigger={<Icon name="helpCircle" color={color} size="tiny" />}>
      <PopupContent>
        <em style={{ color: `${color}` }}>{message}</em>
      </PopupContent>
    </Popup>
  )
}
