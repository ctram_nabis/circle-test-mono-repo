/**
 *
 *
 * NOTES: Created By Chris Bull - ✋ Please do not modify unless asking
 *
 *
 */

import { Box } from 'components/core/src/Box'
import styled from 'styled-components'

export const Fullscreen = styled(Box)``

Fullscreen.defaultProps = {
  position: 'absolute',
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  alignItems: 'center',
  justifyContent: 'center',
  flex: 1,
  display: 'flex',
  bg: 'background',
}
