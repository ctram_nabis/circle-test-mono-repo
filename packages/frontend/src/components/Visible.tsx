import * as React from 'react'
import { branch, compose, renderComponent, withHandlers, withState } from 'recompose'

interface Props {
  isVisible?: boolean
  trigger?: JSX.Element
  children: React.ReactNode
}

interface InnerProps extends Props {
  visible: boolean
  setVisible: (value: boolean) => void
  onClick: () => void
}

export const withVisible = compose<InnerProps, Props>(
  withState('visible', 'setVisible', true),
  withHandlers({
    onClick: (props: InnerProps) => () => {
      props.setVisible(true)
    },
  }),
  branch<InnerProps>(
    (props) => !props.visible,
    renderComponent<InnerProps>((props) => props.trigger || null),
  ),
)

export const Visible = compose<Props, Props>(withVisible)((props: any) => props.children)
