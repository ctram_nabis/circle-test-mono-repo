import { styleView, ViewProps } from 'components/core/src/View'
import React from 'react'
import styled from 'styled-components'
import { S } from './SemanticComponents'

import { MenuHeader } from './MenuHeader'
import { MenuItem } from './MenuItem'
import { MenuMenu } from './MenuMenu'

export interface MenuProps extends Omit<S.MenuProps, 'size' | 'color' | 'width'>, ViewProps {}

const M = styled(S.Menu)<ViewProps>(styleView)
export const Menu = (props: MenuProps) => <M {...props} />

Menu.Header = MenuHeader
Menu.Item = MenuItem
Menu.Menu = MenuMenu
