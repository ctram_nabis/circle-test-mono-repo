import { ErrorBox } from 'components/ErrorBox'
import { Loading } from 'components/Loading'
import React from 'react'
import { ErrorType } from 'types'

interface Props {
  loading?: boolean
  error?: ErrorType
  errors?: ErrorType[]
  errorHeader?: string
}

export const LoadingErrorWrapper: React.FC<Props> = ({ loading, error, errors, errorHeader, ...props }) => (
  <>
    <Loading loading={loading} />
    <ErrorBox error={error} errors={errors} header={errorHeader} />
    {!loading && !error && !errors && props.children}
  </>
)
