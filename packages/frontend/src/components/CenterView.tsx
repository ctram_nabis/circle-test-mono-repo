import { Box } from 'components/core/src/Box'
import styled from 'styled-components'

export const CenterView = styled(Box)`
  display: flex !important;
  justify-content: space-between !important;
  align-items: center !important;
`
