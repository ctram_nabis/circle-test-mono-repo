import { ErrorBox, Loading, S } from 'components'
import { useGenerateManifestMutation } from 'generated/graphql'
import logger from 'modules/Logger'
import React, { useState } from 'react'
import { ErrorType } from 'types'

interface Props {
  onSuccess: () => void
  manifestGDriveFileId?: string | null
  orderId: string
  refetchQueries?: string[]
}

export function ManifestLink({ manifestGDriveFileId: fileId, orderId, refetchQueries, onSuccess }: Props) {
  const [responseError, setResponseError] = useState<ErrorType[]>()

  // const [fileUrl, setFileUrl] = useState(`https://drive.google.com/open?id=${fileId}`)

  const [generateManifestMutation, { loading, error }] = useGenerateManifestMutation({ refetchQueries })

  const handleGenerate = async () => {
    try {
      const { data, errors } = await generateManifestMutation({ variables: { orderId } })

      const res = data?.generateManifest || false

      if (errors) {
        logger.error(`ResponseError: Failed to generate manifest link for order: ${orderId}`, errors)
        return setResponseError(errors)
      }

      if (res) {
        // TODO: instead of immediately setting file url, what to do. Depends on infra
        // setFileUrl(res)
        onSuccess()
      }
    } catch (err) {
      logger.error(`CatchError: Failed to generate manifest link for order: ${orderId}`, err)
      setResponseError([err])
    }
  }

  if (error) {
    logger.error(`GraphQLError: Failed to generate manifest link for order: ${orderId}`, error)
  }

  if (loading) {
    return <Loading />
  }

  if (fileId) {
    return (
      <S.Button
        size="large"
        color="green"
        basic
        // onClick={() => window.open(fileUrl, '_blank')}
        icon="file pdf outline"
      />
    )
  }

  if (responseError || error) {
    return (
      <S.Button onClick={() => handleGenerate()}>
        <S.Icon name="file pdf outline" color="red" />
        <small>Error!</small>
        <small>Retry?</small>
      </S.Button>
    )
  }

  return (
    <S.Button onClick={() => handleGenerate()} icon basic color="green">
      <S.Icon name="file pdf outline" color="green" />
      <small>Generate</small>
      <ErrorBox error={responseError || error ? 'Error!' : undefined} />
    </S.Button>
  )
}
