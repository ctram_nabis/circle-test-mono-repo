import { View } from 'components/core/src/View'
import styled from 'styled-components'

export const Container = styled(View)``

Container.defaultProps = {
  width: '100%',
  maxWidth: 1200,
  p: 1,
  margin: '0 auto',
}
