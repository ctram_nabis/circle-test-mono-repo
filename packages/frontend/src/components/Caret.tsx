import { Box } from 'components/core/src/Box'
import styled from 'styled-components'

export const Caret = styled(Box)`
  display: inline-block;
  width: 0;
  height: 0;
  margin-left: 2px;
  vertical-align: middle;
  border-top: 4px dashed;
  border-right: 4px solid transparent;
  border-left: 4px solid transparent;
`
