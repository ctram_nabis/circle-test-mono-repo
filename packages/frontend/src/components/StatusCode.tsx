import styled from 'styled-components'
import { getColor } from 'system'

export const StatusCode = styled.span`
  color: ${(props) => getColor('shade6', props)};
  font-weight: 600;
  margin-right: 5px;
`
