import { Button } from 'components/Button'
import React from 'react'
import styled from 'styled-components'

const GreenTextButton = styled(Button)`
  &.ui.button.basic.green {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
`
const RedTextButton = styled(Button)`
  &.ui.button.basic.red {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    text-decoration: underline;
  }
`

const BlackTextButton = styled(Button)`
  &.ui.button.basic.black {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    margin-left: 5px;
    text-decoration: underline;
  }
`

const YellowTextButton = styled(Button)`
  &.ui.button.basic.yellow {
    &:hover {
      box-shadow: none !important;
    }
    box-shadow: none !important;
    padding: 0;
    margin-left: 5px;
    text-decoration: underline;
  }
`

const textButtonDictionary = {
  black: BlackTextButton,
  red: RedTextButton,
  green: GreenTextButton,
  yellow: YellowTextButton,
}

export const TextButton = ({ color = 'black', ...props }) => {
  const TextButtonComponent = textButtonDictionary[color]
  return <TextButtonComponent basic color={color} {...props} />
}
