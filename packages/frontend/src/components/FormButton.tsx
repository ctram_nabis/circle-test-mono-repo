import React from 'react'
import styled from 'styled-components'
import { styleView, ViewProps } from './core'
import { S } from './SemanticComponents'

export interface FormButtonProps extends Omit<S.StrictFormButtonProps, 'size' | 'color' | 'width'>, ViewProps {
  title?: string
}
export const SFormButton: React.FC<FormButtonProps> = styled(S.FormButton)<ViewProps>(styleView)

export const FormButton: React.FC<FormButtonProps> = ({ title, ...props }) => {
  return <SFormButton label={title} {...props} />
}

FormButton.defaultProps = {
  size: 'large',
  fluid: true,
  my: 3,
}
