import { styleView, ViewProps } from 'components/core/src/View'
import styled from 'styled-components'
import { S } from './SemanticComponents'

export interface MenuHeaderProps extends Omit<S.StrictMenuHeaderProps, 'color' | 'size'>, ViewProps {}
export const MenuHeader: React.FC<MenuHeaderProps> = styled(S.MenuHeader)<ViewProps>(styleView)
