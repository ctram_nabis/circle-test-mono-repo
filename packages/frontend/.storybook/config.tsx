import { withA11y } from '@storybook/addon-a11y'
import { withKnobs } from '@storybook/addon-knobs'
import { addDecorator, configure } from '@storybook/react'
import React from 'react'

// const user = fixtures.user()
// addDecorator(themeDecorator)
// addDecorator((storyFn) => <MeContext.Provider value={user.getUser}>{storyFn()}</MeContext.Provider>)
// addDecorator((storyFn) => (
//   <OrganizationContext.Provider value={user.getUser.organizations[0]}>{storyFn()}</OrganizationContext.Provider>
// ))
addDecorator(withA11y)
addDecorator(withKnobs)
configure(require.context('../src', true, /\.stories\.(mdx|[tj]sx?)$/), module)
