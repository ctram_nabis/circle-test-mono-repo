// module.exports = {
//   globals: {
//     'ts-jest': {
//       diagnostics: false,
//     },
//   },
//   moduleDirectories: ['node_modules', 'src'],
//   moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
//   moduleNameMapper: {
//     '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga|css|less|htaccess|ico)$':
//       'identity-obj-proxy',
//   },
//   preset: 'ts-jest',
//   roots: ['<rootDir>/src'],
//   setupFilesAfterEnv: ['<rootDir>/setupEnzyme.ts', '<rootDir>/setupTests.ts'],
//   snapshotSerializers: ['enzyme-to-json/serializer'],
//   testEnvironment: 'jsdom',
//   testPathIgnorePatterns: ['\\.snap$', '<rootDir>/node_modules/'],
//   testRegex: '(/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$',
//   transform: {
//     '^.+\\.tsx?$': 'ts-jest',
//   },
// }

// module.exports = {
//   "moduleFileExtensions": ["ts", "tsx", "js", "jsx", "json", "node"],
//   "roots": ["<rootDir>/src"],
//   "setupTestFrameworkScriptFile": "<rootDir>/src/setupEnzyme.ts",
//   "snapshotSerializers": ["enzyme-to-json/serializer"],
//   "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
//   "transform": {
//     "^.+\\.tsx?$": "ts-jest"
//   }
// }

module.exports = {
  preset: 'ts-jest',
}
