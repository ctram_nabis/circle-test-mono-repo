import { Color } from '../src/system/theme'
import Maybe from 'graphql/tsutils/Maybe'

declare module 'styled-system' {
  interface ColorProps {
    backgroundColor?: Color
    bg?: Color
    color?: Color
    borderColor?: Color
  }

  interface BackgroundProps {
    backgroundColor?: Color
  }

  interface CustomTextProps {
    color?: Color
  }

  interface TextColorProps extends CustomTextProps {}

  interface BordersProps extends ColorProps {}

  interface BorderColorProps extends ColorProps {}
}
