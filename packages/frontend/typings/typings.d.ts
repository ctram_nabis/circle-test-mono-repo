interface Fullstory {
  identify(uid: string): void
  setUserVars(userVars: { displayName: string; email?: string } & Record<string, any>): void
  getCurrentSessionURL(): string
  log(msg: string): void
  log(level: string, msg: string): void
  shutdown(): void
  restart(): void
  consent(consent: boolean): void
}

declare var FS: Fullstory
