import { Theme } from '../src/system/theme'

declare module 'styled-components' {
  interface DefaultTheme extends Theme {}

  declare module 'styled-components/primitives'
}
