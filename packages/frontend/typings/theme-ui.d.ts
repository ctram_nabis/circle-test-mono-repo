import { Theme as CustomTheme } from '../src/system/theme'

declare module 'theme-ui' {
  interface Theme extends CustomTheme {}
}
