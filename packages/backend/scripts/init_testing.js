#!/usr/bin/env node
require('babel-core/register')

const program = require('commander')
const { Client } = require('pg')
const { exec } = require('child_process')
const colors = require('colors')
const path = require('path')

const config = require('../src/config/testing').default

const schemaPath = path.join(__dirname, '..', 'src', 'connections', 'postgres', 'migrations', 'schema', 'schema.sql')

function logMainStep(str) {
  console.log()
  console.log(` * ${str}`.white)
}

function logStepSuccess(str) {
  console.log(`   ✅ ${str}`.green)
}

function logStep(str) {
  console.log(`   > ${str}`.yellow)
}

function logStepError(str) {
  console.log(`   ❌ ${str}`.red)
}

function asyncExec(command) {
  return new Promise((resolve, reject) => {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        logStepError(stderr)
        reject(error)
        return
      }
      resolve(stdout)
    })
  })
}

const DEFAULT_ENVIRONMENT = 'testing'
const DEFAULT_PASSWORD = config.postgres.logistics.password
const DEFAULT_USER = config.postgres.logistics.username
const DEFAULT_HOST = config.postgres.logistics.host
const DEFAULT_DATABASE = config.postgres.logistics.database
const DEFAULT_PORT = config.postgres.logistics.port

program
  .option('-h, --host <host>', 'database server host or socket directory (default: "local socket")', DEFAULT_HOST)
  .option('-p, --port <port>', `database server port (default: "${DEFAULT_PORT}")`, DEFAULT_PORT)
  .option('-d, --database <database>', `database name (default: "${DEFAULT_DATABASE}")`, DEFAULT_DATABASE)
  .option('-U, --username <username>', `database user name (default: "${DEFAULT_USER}")`, DEFAULT_USER)
  .option('-W, --password <password>', 'database password', DEFAULT_PASSWORD)
  .option('-e, --environment <environment>', `environment (default: "${DEFAULT_ENVIRONMENT}")`, DEFAULT_ENVIRONMENT)

program.parse(process.argv)

const getConnectionString = ({ username, password, host }) => `postgres://${username}:${password}@${host}/postgres`

const tryCreateDatabase = async (client, shouldDropFirst, database) => {
  try {
    if (shouldDropFirst) {
      logStep(`Dropping database "${database}" if it exists...`)
      try {
        await client.query(`DROP DATABASE ${database}`)
      } catch (error2) {
        logStepError(`Error dropping database: ${error2}`)
      }
    }
    logStep(`Creating database "${database}"...`)
    await client.query(`CREATE DATABASE ${database}`)
  } catch (error) {
    throw error
  }
}

const getPostgresClient = async (opts) => {
  logMainStep('Database connection')
  logStep(`Connecting to database "${opts.database}"...`)

  const connectionString = getConnectionString(opts)
  const client = new Client(connectionString)

  // connect to postgres db
  try {
    await client.connect()
  } catch (error) {
    logStepError(`Error connecting to database:${error}`)
    throw error
  }

  logStepSuccess('Connection successful!')

  return client
}

const createTestingDatabase = async (client, { database }) => {
  logMainStep('Creating database:')

  // create the database and ignore any errors, for example if it already exists.
  try {
    await tryCreateDatabase(client, true, database)
  } catch (error) {
    logStepError(`Error creating database: ${error}\n`)
    throw error
  }

  logStepSuccess('Database creation successful!')
}

const importSchema = async (client, { database }) => {
  logMainStep('Schema creation:')
  logStep(`Importing schema into ${database} from path "${schemaPath}"`)

  try {
    try {
      await asyncExec(`psql -h ${DEFAULT_HOST} -U "postgres" testing -c "DROP SCHEMA ${database}"`)
    } catch (error2) {
      logStepError('Error dropping schema')
    }
    await asyncExec(`psql -h ${DEFAULT_HOST} -U "postgres" testing -c "CREATE SCHEMA ${database}"`)
    await asyncExec(`psql -h ${DEFAULT_HOST} -U "postgres" testing < ${schemaPath}`)
    logStepSuccess('Done importing schema!')
  } catch (error) {
    logStepError('Unable to import schema')
  }
}

const compileConfig = async ({ environment }) =>
  new Promise((resolve, reject) => {
    logMainStep('Compiling database config.json:')

    const command = `NODE_ENV=${environment} node ./src/config/compile_config.js`

    exec(command, (error, stdout, stderr) => {
      if (error) {
        logStepError(`Error compiling config.json: ${stderr}`)
        reject(error)
        return
      }
      logStepSuccess('Successfully compiled config.json')
      resolve(stdout)
    })
  })

const migrateDatabase = async ({ environment }) =>
  new Promise((resolve, reject) => {
    logMainStep('Migrating database:')
    const command = 'dotenv -e .env.test npx sequelize db:migrate --debug'
    exec(command, (error, stdout, stderr) => {
      if (error) {
        logStepError(`Error migrating database: \n${stderr}`)
        reject(error)
        return
      }
      logStepSuccess('Successfully migrated database')
      resolve(stdout)
    })
  })

const main = async () => {
  const opts = program.opts()
  let client
  try {
    client = await getPostgresClient(opts)
    await createTestingDatabase(client, opts)
    await importSchema(client, opts)
    await compileConfig(opts)
    await migrateDatabase(opts)
  } catch (error) {
    // safely catch any errors so we can close the socket afterwards
  }
  await client.end()
}

main()
