# Nabis Backend

### Description

Nabis backend repository.

### Getting Started

#### 1. Run `npm install` or `yarn`

- Installs all the dependencies for this app.

- ⚠️ Use `nvm` to switch to version 8 of Node.js if you are having installation issues. Once you have `nvm` installed, run `nvm install 8 && nvm use 8`

#### 2. Install PostgreSQL

- For Hombrew installation, follow the instructions [here](https://www.codementor.io/engineerapart/getting-started-with-postgresql-on-mac-osx-are8jcopb).

- 💡 Alternatively, non-CLI installation is available via [Postgress.app](https://postgresapp.com), which adds a toggle for the local database server to the Menu Bar.

- Ask a teammate for their `~/.pgpass` file, which allows you to connect to some databases that require a password.

  - ⚠️ If you are getting a permissions error, try running `chmod 600 ~/.pgpass` ([more information](https://stackoverflow.com/questions/16786011/postgresql-pgpass-not-working).

- 💡 Install and use a GUI/Client like [Postico](https://eggerapps.at/postico/).

#### 3. Create database and user role

Script method:

1. Copy the contents of the bash script below and paste into `~/.bash_profile`.

2. Save the edited file and run `source ~/.bash_profile` to load the new changes.

3. Run `nabis-prod-get`

- This sets up the proper user role, resets and creates a local database, dumps production data into that databse, and adds a "uuid" extension.

- Run `yarn migrate` in case you run into any schema/connection issues. Alternatively, you can run the proper Sequelize's CLI commands (listed in the Sequelize section).

```bash
# ~/.bash_profile

# Install nvm
export NVM_DIR="$HOME/.nvm"
[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"                           # This loads nvm
[ -s "/usr/local/opt/nvm/etc/bash_completion" ] && . "/usr/local/opt/nvm/etc/bash_completion" # This loads nvm bash_completion

# Frequently used database commands
alias nabis-reset-db='echo "DROP DATABASE IF EXISTS \"Logistics\"; DROP USER \"Nabis\";" | psql'
alias nabis-setup-db='echo "CREATE DATABASE \"Logistics\"; CREATE USER \"Nabis\" WITH SUPERUSER;" | psql'
alias nabis-dump-prod="pg_dump -h nabis-prod-2.cnwlqcoumxfz.us-west-2.rds.amazonaws.com -U Nabis -d Logistics -f \"db-backup.$(date +"%Y%m%d").sql\" --no-owner --no-privileges --clean --verbose --if-exists;"
alias nabis-upload-local="psql -h localhost -d Logistics -U Nabis -v ON_ERROR_STOP=1 -f ./db-backup.$(date +"%Y%m%d").sql"
alias nabis-add-uuid='psql -h localhost -d Logistics -U Nabis -c "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";"'

function nabis-local-up() {
  nabis-reset-db
  nabis-setup-db
  nabis-upload-local
  nabis-add-uuid
}

function nabis-prod-get() {
  nabis-dump-prod
  nabis-local-up
}

function nabis-sql-up() {
  nabis-setup-db
  psql -h localhost -U Nabis -d Logistics -f $@
  nabis-add-uuid
}

# Shortcuts for editing this file
alias bo="code ~/.bash_profile"
alias bs="source ~/.bash_profile"

# Shortcut for git
alias gfp="git fetch && git pull --rebase"
```

Manual method:

1. Run `psql postgres`, followed by `CREATE DATABASE "Logistics";`

   - Creates a new database locally called `Logistics`.

2. In the same PostgreSQL shell, run `CREATE USER "Nabis" WITH CREATEDB;`

   - Creates a new user role for database management.

#### 3. Ask for the following `.env` files and place them in the root of your cloned repository:

- `.env`
- `.env.test`
- `.env.staging`
- `.env.production`

#### 4. Run `npm run dev` || (`npm run buildw` && `npm run start`)

- Builds app on save into `dist` folder and then starts development server at `http://localhost:8080/graphql`.

### Development

1. For work on a new feature, create a new branch from `develop`.

2. Rebase/merge `develop` branch into new feature branch, and resolve any merge conflicts.

3. Run `git pull`

   - Pulls down most recent changes from server to local environment.

4. Make development changes.

5. Run `git add -A`

6. Run `git commit -m "<commit_message>"`

7. Run `git push origin <feature_branch>`

8. To merge feature branch into `develop`, submit a [pull request](https://bitbucket.org/getnabis/nabis-backend). It requires at least one administrator to review and approve changes before merging.

- 💡 VSCode has an awesome extension called `Git Graph`, which will allow you to visualize your branches/commits.

_Note: CI/CD pipeline coming soon._

### Testing

#### Unit Tests

- Run `npm run init:db` or `yarn init:db` to create the local `testing` database.
- Run unit tests via `npm run test:watch` / `yarn test:watch` or `npm run test` / `yarn test`.

#### Integration Tests

- Uses Cypress, see their [docs](https://docs.cypress.io/) for more information.
- To properly run the server for integration testing, instead of running `npm run dev`, run `npm run start-test`
- You must also run `npm run cypress` in your cloned `integration-testing` repository.

```bash
cd nabis-backend # be inside the root directory
virtualenv venv  # use a python virtualenv to manage dependencies
source ./venv/bin/activate # start the damn virtualenv
python test/test.py --url=http://localhost:8080 # point to the URL that you wanna test
deactivate # close the virtualenv when you're done
```

### [Sequelize CLI](http://docs.sequelizejs.com/manual/migrations.html)

To use Sequelize with environment variables:

```
$ npx sequelize [command]
```

Sequelize CLI guide:

```
Sequelize CLI [Node: 11.13.0, CLI: 5.4.0, ORM: 4.38.0]

sequelize [command]

Commands:
  sequelize db:migrate                        Run pending migrations
  sequelize db:migrate:schema:timestamps:add  Update migration table to have timestamps
  sequelize db:migrate:status                 List the status of all migrations
  sequelize db:migrate:undo                   Reverts a migration
  sequelize db:migrate:undo:all               Revert all migrations ran
  sequelize db:seed                           Run specified seeder
  sequelize db:seed:undo                      Deletes data from the database
  sequelize db:seed:all                       Run every seeder
  sequelize db:seed:undo:all                  Deletes data from the database
  sequelize db:create                         Create database specified by configuration
  sequelize db:drop                           Drop database specified by configuration
  sequelize init                              Initializes project
  sequelize init:config                       Initializes configuration
  sequelize init:migrations                   Initializes migrations
  sequelize init:models                       Initializes models
  sequelize init:seeders                      Initializes seeders
  sequelize migration:generate                Generates a new migration file
  sequelize model:generate                    Generates a model and its migration
  sequelize seed:generate                     Generates a new seed file

Options:
  --help     Show help
  --version  Show version number
```

#### Example:

```
$ npx sequelize migration:generate --name <name your migration>
```

This will give you a migration file that you can edit which gives you instructions
on how to use the file. Then, use:

```
$ npx sequelize db:migrate
```

will add you changes.

_Important_: this will not work if you have not built code using `npm run buildnw`

#### Tutorial:

_Purpose_: In this tutorial, a PostgreSQL table will be created in the database,
called testtable.

You need to compile your code base and the Sequelize config file by running:

```
$ npm run buildnw
```

Now, you can generate a model using:

```
$ npx sequelize model:generate --name testtable --attributes id:UUID
```

This will generate two files: a table file, and a migration.

`src/connections/postgres/models/testtable.js`

`src/connections/postgres/migrations/<date string>-create-testtable.js`

You want to edit those files to match the following sample files:

[Table](src/connections/postgres/sample/testtable.js)

[Migration](src/connections/postgres/sample/20190426191037-create-testtable-SAMPE-CODE.js)

It's time to migrate the files using:

```
$ npx sequelize db:migrate
```

If you want to remove this migration, run:

```
$ npx sequelize db:migrate:undo
```

Tip: If you'd like to run `debug` mode on any of the sequelize commands, you can attach the debugger via the commandline like this:

```bash
node debug ./node_modules/.bin/sequelize db:migrate
```

### Migrate to Staging and Production

You need to create the environment variables from AWS per environment. Once,
you've created those, you can run the following:

```
$ node migrateDB.js [location of JSON file you created]
```

This will transpile a bash script that you'll run to auto migrate to which ever
environment file you passed it.

### Copy Location to Local

```
$ node migrateDB [location of JSON file you created] -cp
```
