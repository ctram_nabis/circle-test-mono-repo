/*       */
import cors from 'cors'
import http from 'http'
import express from 'express'
import httpError from 'http-errors'
import graphqlHTTP from './express-graphql'
import { graphqlUploadExpress } from 'graphql-upload'
import jwt from 'express-jwt'
import gmaps from '@google/maps'
import basicAuth from 'express-basic-auth'
import * as CSV from 'csv-string'
import config from './config'
import logger from './logging'
import compression from 'compression'
import db from './connections/postgres'
import { processRequest, authenticate, getToken } from './middleware'
import { AdminGQLSchema, ClientGQLSchema } from './schema'
import { SendGridAPI } from './connections/send-grid'
import { MailChimpAPI } from './connections/mailchimp'
import { GDriveAPI } from './connections/gdrive'
import { unpackId } from './schema/graphql-helpers/contrib/relay/resolvers'

const reportQuery = `
SELECT
  -- Licensed Location does not exist for some older orders
  -- some need to be deleted and have had names edited to contain 'DELETE NABIS'
  -- in that case fallback to either Site or License
  (CASE
    WHEN "LicensedLocation".name ILIKE '%delete nabis%' OR "LicensedLocation".name ILIKE '%suspended%' THEN
      COALESCE("Site".name, "License".nickname)
    WHEN "LicensedLocation".name IS NULL THEN
      COALESCE("Site".name, "License".nickname)
    ELSE
      "LicensedLocation".name
    END
  ) AS "retailer",
  "Org1".name AS "organization",
  "Org2".name AS "seller",
  COALESCE("User".email, 'n/a') "creator",
	"Order".number AS "order",
	"Order".status,
	"Order"."paymentStatus",
  "Order".action,
  TO_CHAR("Order".date, 'MM/DD/YYYY') AS "date",
  "LineItem".name "strain",
  -- Some SkuBatches are missing Batch
	COALESCE("Batch".code, lineitembatch.code) AS "batchNumber",
	"LineItem".quantity AS "units",
	"LineItem"."pricePerUnit" AS "pricePerUnit",
	"LineItem"."id" AS "lineItemId",
	"Order".notes,
  "Order"."adminNotes",
  "Order".discount AS "orderDiscount",
	"Sku"."inventoryCategory",
  "Sku"."inventoryClass",
  "LicensedLocation".id AS "retailer-uuid",
  "License"."licenseNumber" AS "licensedLocationNumber",
  "LineItem".code AS "skuCode",
  "Sku"."isArchived" AS "skuIsArchived", "SkuBatch"."isArchived" AS "skuBatchIsArchived",
	(
		SELECT
			"name"
		FROM
			"Site"
		WHERE
      "id" = "Warehouse"."siteId") AS "sourceWarehouse",
  "Order"."daysTillPaymentDue",
  "Order"."gmv",
  CONCAT("Site".address1, ' ', "Site".address2, chr(10), "Site".city, ' ', "Site"."state", ' ', "Site".zip) AS "siteAddress"
FROM
  "LineItem"
  JOIN "Organization" AS "Org1" ON "Org1".id = "LineItem"."organizationId"
  LEFT JOIN "SkuBatch" ON "SkuBatch".id = "LineItem"."skuBatchId"
  LEFT JOIN "Batch" ON "Batch".id = "SkuBatch"."batchId"
  -- If SkuBatch.batchId is empty, need to fall back on LineItem.batchId
  LEFT JOIN "Batch" AS lineitembatch ON lineitembatch.id = "LineItem"."batchId"
  JOIN "Order" ON "Order".id = "LineItem"."orderId"
  LEFT JOIN "User" ON "User".id = "Order"."creatorId"
  LEFT JOIN "LicensedLocation" ON "LicensedLocation".id = "Order"."licensedLocationId"
  LEFT JOIN "Sku" ON "Sku".id = "LineItem"."skuId"
  LEFT JOIN "Warehouse" ON "Order"."warehouseId" = "Warehouse".id
  JOIN "Site" ON "Order"."siteId" = "Site".id
  LEFT JOIN "Titleholder" ON "Titleholder".id = "SkuBatch"."titleholderId"
  LEFT JOIN "Organization" AS "Org2" ON "Org2".id = "Titleholder"."organizationId"
  LEFT JOIN "License" ON "License".id = (
    SELECT lic1.id FROM "License" AS lic1
    WHERE "LicensedLocation".id = lic1."licensedLocationId"
    ORDER BY lic1.id DESC LIMIT 1
  )
WHERE
  "LineItem"."organizationId" = :id
  AND "Order"."isArchived" = false
ORDER BY
  "Order".date DESC,
  "Order".number DESC;
`

const start = (worker) => {
  logger.info(`Starting worker ${worker ? worker.id : 0}`)
  logger.info(`Environment: ${config.nodeEnv}`)

  const logisticsDB = db.base

  // Verify database connections
  logisticsDB.authenticate()

  // Sync database models
  // logisticsDB.sync({
  //   force: false,
  //   alter: false,
  // });

  // Initialize app
  const app = express()
  app.use(compression())
  app.use(cors())

  // Initialize Google Drive
  const googleDriveClient = new GDriveAPI({
    clientEmail: config.google.serviceAccount.clientEmail,
    privateKey: config.google.serviceAccount.privateKey,
  })
  // Initialize Google Maps
  const googleMapsClient = gmaps.createClient({
    key: config.google.maps.apiKey,
  })
  // Initialize SendGrid
  const sendGridClient = new SendGridAPI({
    key: config.sendGrid.apiKey,
  })
  // Initialize MailChimp
  const mailChimpClient = new MailChimpAPI({
    key: config.mailChimp.apiKey,
  })

  // Set request context
  const context = {
    connections: {
      sequelize: db.sequelize,
      logisticsDB: logisticsDB.db.models,
      googleDriveClient,
      googleMapsClient,
      sendGridClient,
      mailChimpClient,
    },
  }

  app.use(processRequest(context))

  // Create server
  const server = http.createServer(app)

  // Errors
  const formatError = (err) => {
    logger.error(err.stack)
    const formattedErr = {
      message: err.message,
      ...err,
    }
    if (err && err.originalError && err.originalError.status) {
      formattedErr.status = err.originalError.status
    }
    if (err && err.originalError && err.originalError.code) {
      formattedErr.code = err.originalError.code
    }
    if (err && err.originalError) {
      formattedErr.name = err.originalError.name || err.name
    } else if (err && err.name) {
      formattedErr.name = err.originalName || err.name
    }
    // if (err && err.stack) {
    //   formattedErr.stack = err.stack;
    // }
    return formattedErr
  }

  app.use('/health', (req, res) => {
    logisticsDB
      .authenticate()
      .then((pinged) => {
        if (pinged) {
          return res.status(200).send('OK')
        }
        throw httpError(500, 'Failed health check')
      })
      .catch((err) => {
        logger.error(err.message)
        res.status(500).send(err.message)
      })
  })

  // Nabis API
  app.use(
    '/graphql/admin',
    jwt({
      secret: config.auth.secret,
      requestProperty: 'auth',
      credentialsRequired: false,
      getToken,
    }),
  )
  app.use('/graphql/admin', authenticate)
  app.use(
    '/graphql/admin',
    graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 10 }),
    graphqlHTTP((req) => ({
      schema: AdminGQLSchema,
      graphiql: true,
      pretty: false,
      context: req.context,
      formatError,
    })),
  )

  // Client API
  app.use(
    '/graphql/:clientId',
    jwt({
      secret: config.auth.secret,
      requestProperty: 'auth',
      credentialsRequired: false,
      getToken,
    }),
  )
  app.use('/graphql/:clientId', authenticate)
  app.use(
    '/graphql/:clientId',
    graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 10 }),
    graphqlHTTP((req) => ({
      schema: ClientGQLSchema,
      graphiql: true,
      pretty: false,
      context: req.context,
      formatError,
    })),
  )

  app.get(
    '/report/:id',
    basicAuth({
      users: {
        [config.nabisApiKey]: '',
      },
    }),
    (req, res) => {
      const selectedBrandId = unpackId(req.params.id)
      db.sequelize
        .query(reportQuery, {
          replacements: { id: selectedBrandId },
          type: db.Sequelize.QueryTypes.SELECT,
        })
        .then((rows) => res.json({ rows }))
    },
  )

  app.get(
    '/report-csv/:id',
    basicAuth({
      users: {
        [config.nabisApiKey]: '',
      },
    }),
    (req, res) => {
      const selectedBrandId = unpackId(req.params.id)
      db.sequelize
        .query(reportQuery, {
          replacements: { id: selectedBrandId },
          type: db.Sequelize.QueryTypes.SELECT,
        })
        .then((rows) => {
          const headers = [
            'retailer',
            'seller',
            'organization',
            'creator',
            'order',
            'status',
            'paymentStatus',
            'action',
            'date',
            'strain',
            'batchNumber',
            'units',
            'pricePerUnit',
            'notes',
            'adminNotes',
            'inventoryCategory',
            'inventoryClass',
            'retailer-uuid',
            'skuCode',
            'sourceWarehouse',
            'daysTillPaymentDue',
            'gmv',
            'skuIsArchived',
            'skuBatchIsArchived',
            'siteAddress',
            'licensedLocationNumber',
            'orderDiscount',
            'lineItemId',
          ]
          const csvRows = rows.map((r) => headers.map((h) => r[h]))
          const csv = CSV.stringify([headers].concat(csvRows))
          res.send(csv)
        })
    },
  )

  app.use('/', (req, res) => {
    res.status(200).send('Welcome to the Nabis API.')
  })

  server.listen(config.port, () => {
    // Only works when server is clustered (i.e. not in 'dev').
    logger.info(`GraphQL Server is now running on http://localhost:${config.port}/graphql`)
  })

  return app
}

export default start
