export const IS_PROD = process.env.NODE_ENV === 'production'
export const IS_DEV = process.env.NODE_ENV === 'development'
export const IS_TEST = ['testing', 'integration'].includes(process.env.NODE_ENV)

export const WAREHOUSES = {
  LA: {
    licensedLocationId: 'b191a9ca-d6b2-4d47-b7b4-966225e10210',
    warehouseId: '85ae34f3-b9c7-415b-b75e-a85e612c5bf5', // ZASP
  },
  OAK: {
    licensedLocationId: '8defbafe-906f-41ea-b40f-1b920dfdf40f',
    warehouseId: '6ceafe22-c127-11e8-a355-529269fb1459', // NABIONE
  },
}

export const WAREHOUSE_ID_TO_KEY = {
  '6ceafe22-c127-11e8-a355-529269fb1459': 'OAK',
  '85ae34f3-b9c7-415b-b75e-a85e612c5bf5': 'LA',
}

export const NABIS_ORGANIZATIONS = {
  DELIVERIES: '960f4b56-c5db-41d6-824d-8563e98d850e',
  SALES: '6ba4e5bb-c4d7-425c-af85-0db2382508a9',
}

export const NABIS_USERS = {
  DELIVERIES: 'a20bfa42-151d-4965-8038-de49a5860a65', // deliveries@nabis.com
  SALES: '93cd5e52-aea8-412e-94d7-6545ff711f47', // sales@getnabis.com
}

export const NABIS_USERS_ID_TO_KEY = {
  '93cd5e52-aea8-412e-94d7-6545ff711f47': 'SALES', // sales@getnabis.com
  'a20bfa42-151d-4965-8038-de49a5860a65': 'DELIVERIES', // deliveries@nabis.com
}

export const QUERY_AFTER_LINE_ITEMS_DATE = new Date('July 25, 2019 00:00:00')

export const ORDER_ACTIONS = {
  DELIVERY_TO_BRAND: 'DELIVERY_TO_BRAND',
  DELIVERY_TO_RETAILER: 'DELIVERY_TO_RETAILER',
  DROPOFF_TO_NABIS: 'DROPOFF_TO_NABIS',
  INTERNAL_TRANSFER: 'INTERNAL_TRANSFER',
  OTHER: 'OTHER',
  PAYMENT_PICKUP_FROM_RETAILER: 'PAYMENT_PICKUP_FROM_RETAILER',
  PICKUP_FROM_BRAND: 'PICKUP_FROM_BRAND',
  PICKUP_FROM_NABIS: 'PICKUP_FROM_NABIS',
  RETURN_FROM_RETAILER: 'RETURN_FROM_RETAILER',
}

export const ORDER_STATUS_PARSED = {
  APPROVAL_NEEDED: 'Approval Needed',
  CANCELLED: 'Cancelled',
  DELAYED: 'Delayed',
  DELIVERED: 'Delivered',
  DELIVERED_WITH_EDITS: 'Delivered with Edits',
  DRAFT: 'Draft',
  REJECTED: 'Rejected',
  SCHEDULED: 'Scheduled',
  TRANSFERRING: 'Transferring',
  UNSCHEDULED: 'Unscheduled',
}

export const ORDER_STATUS = {
  APPROVAL_NEEDED: 'APPROVAL_NEEDED',
  CANCELLED: 'CANCELLED',
  DELAYED: 'DELAYED',
  DELIVERED: 'DELIVERED',
  DELIVERED_WITH_EDITS: 'DELIVERED_WITH_EDITS',
  DRAFT: 'DRAFT',
  REJECTED: 'REJECTED',
  SCHEDULED: 'SCHEDULED',
  TRANSFERRING: 'TRANSFERRING',
  UNSCHEDULED: 'UNSCHEDULED',
}

export const TRANSACTION_TYPES = {
  ALLOCATION: 'ALLOCATION',
  CORRECTION: 'CORRECTION',
  INTAKE: 'INTAKE',
  OUTTAKE: 'OUTTAKE',
}
