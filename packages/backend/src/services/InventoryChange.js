export default class InventoryChange {
  constructor(skuBatchId, warehouseId, amount) {
    if (!skuBatchId) {
      throw new Error()
    }
    if (!warehouseId) {
      throw new Error()
    }
    if (!amount) {
      throw new Error()
    }
    this.skuBatchId = skuBatchId
    this.warehouseId = warehouseId
    this.amount = amount
  }
}
