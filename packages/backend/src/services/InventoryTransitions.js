const AVAILABLE_INCREASE = 'AVAILABLE_INCREASE'
const AVAILABLE_DECREASE = 'AVAILABLE_DECREASE'
const DELIVERED_INCREASE = 'DELIVERED_INCREASE'
const DELIVERED_DECREASE = 'DELIVERED_DECREASE'
const EXIT_INCREASE = 'EXIT_INCREASE'
// const EXIT_DECREASE = 'EXIT_DECREASE'
const INCOMING_INCREASE = 'INCOMING_INCREASE'
const INCOMING_DECREASE = 'INCOMING_DECREASE'
const MANUAL_INTERVENTION_INCREASE = 'MANUAL_INTERVENTION_INCREASE'
const MANUAL_INTERVENTION_DECREASE = 'MANUAL_INTERVENTION_DECREASE'
const OUTGOING_INCREASE = 'OUTGOING_INCREASE'
const OUTGOING_DECREASE = 'OUTGOING_DECREASE'
const PACKED_INCREASE = 'PACKED_INCREASE'
const PACKED_DECREASE = 'PACKED_DECREASE'
const PROMISED_INCREASE = 'PROMISED_INCREASE'
const PROMISED_DECREASE = 'PROMISED_DECREASE'
const QUARANTINE_INCREASE = 'QUARANTINE_INCREASE'
const QUARANTINE_DECREASE = 'QUARANTINE_DECREASE'
const RESERVED_INCREASE = 'RESERVED_INCREASE'
const RESERVED_DECREASE = 'RESERVED_DECREASE'

const KEYS = {
  available: 'available',
  delivered: 'delivered',
  exit: 'exit',
  incoming: 'incoming',
  manual_intervention: 'manual_intervention',
  outgoing: 'outgoing',
  packed: 'packed',
  promised: 'promised',
  quarantine: 'quarantine',
  reserved: 'reserved',
}

export default {
  PROMISED: {
    key: 'PROMISED',
    to: {
      key: KEYS.promised,
      transaction: PROMISED_INCREASE,
    },
  },

  PROMISED_TO_INCOMING: {
    key: 'PROMISED_TO_INCOMING',
    from: {
      key: KEYS.promised,
      transaction: PROMISED_DECREASE,
    },
    to: {
      key: KEYS.incoming,
      transaction: INCOMING_INCREASE,
    },
  },

  INCOMING_TO_AVAILABLE: {
    key: 'INCOMING_TO_AVAILABLE',
    from: {
      key: KEYS.incoming,
      transaction: INCOMING_DECREASE,
    },
    to: {
      key: KEYS.available,
      transaction: AVAILABLE_INCREASE,
    },
  },
  INCOMING_TO_QUARANTINE: {
    key: 'INCOMING_TO_QUARANTINE',
    from: {
      key: KEYS.incoming,
      transaction: INCOMING_DECREASE,
    },
    to: {
      key: KEYS.quarantine,
      transaction: QUARANTINE_INCREASE,
    },
  },
  INCOMING_TO_MANUAL_INTERVENTION: {
    key: 'INCOMING_TO_MANUAL_INTERVENTION',
    from: {
      key: KEYS.incoming,
      transaction: INCOMING_DECREASE,
    },
    to: {
      key: 'manual_intervention',
      transaction: MANUAL_INTERVENTION_INCREASE,
    },
  },

  QUARANTINE_TO_EXIT: {
    key: 'QUARANTINE_TO_EXIT',
    from: {
      key: KEYS.quarantine,
      transaction: QUARANTINE_DECREASE,
    },
    to: {
      key: KEYS.exit,
      transaction: EXIT_INCREASE,
    },
  },
  QUARANTINE_TO_AVAILABLE: {
    key: 'QUARANTINE_TO_AVAILABLE',
    from: {
      key: KEYS.quarantine,
      transaction: QUARANTINE_DECREASE,
    },
    to: {
      key: KEYS.available,
      transaction: AVAILABLE_INCREASE,
    },
  },

  AVAILABLE_TO_RESERVED: {
    key: 'AVAILABLE_TO_RESERVED',
    from: {
      key: KEYS.available,
      transaction: AVAILABLE_DECREASE,
    },
    to: {
      key: KEYS.reserved,
      transaction: RESERVED_INCREASE,
    },
  },
  RESERVED_TO_PACKED: {
    key: 'RESERVED_TO_PACKED',
    from: {
      key: KEYS.reserved,
      transaction: RESERVED_DECREASE,
    },
    to: {
      key: KEYS.packed,
      transaction: PACKED_INCREASE,
    },
  },
  RESERVED_TO_AVAILABLE: {
    key: 'RESERVED_TO_AVAILABLE',
    from: {
      key: KEYS.reserved,
      transaction: RESERVED_DECREASE,
    },
    to: {
      key: KEYS.available,
      transaction: AVAILABLE_INCREASE,
    },
  },
  PACKED_TO_MANUAL_INTERVENTION: {
    key: 'PACKED_TO_MANUAL_INTERVENTION',
    from: {
      key: KEYS.packed,
      transaction: PACKED_DECREASE,
    },
    to: {
      key: KEYS.manual_intervention,
      transaction: MANUAL_INTERVENTION_INCREASE,
    },
  },
  PACKED_TO_OUTGOING: {
    key: 'PACKED_TO_OUTGOING',
    from: {
      key: KEYS.packed,
      transaction: PACKED_DECREASE,
    },
    to: {
      key: KEYS.outgoing,
      transaction: OUTGOING_INCREASE,
    },
  },

  OUTGOING_TO_DELIVERED: {
    key: 'OUTGOING_TO_DELIVERED',
    from: {
      key: KEYS.outgoing,
      transaction: OUTGOING_DECREASE,
    },
    to: {
      key: KEYS.delivered,
      transaction: DELIVERED_INCREASE,
    },
  },
  OUTGOING_TO_MANUAL_INTERVENTION: {
    key: 'OUTGOING_TO_MANUAL_INTERVENTION',
    from: {
      key: KEYS.outgoing,
      transaction: OUTGOING_DECREASE,
    },
    to: {
      key: KEYS.manual_intervention,
      transaction: MANUAL_INTERVENTION_INCREASE,
    },
  },

  DELIVERED_TO_MANUAL_INTERVENTION: {
    key: 'DELIVERED_TO_MANUAL_INTERVENTION',
    from: {
      key: KEYS.delivered,
      transaction: DELIVERED_DECREASE,
    },
    to: {
      key: KEYS.manual_intervention,
      transaction: MANUAL_INTERVENTION_INCREASE,
    },
  },

  MANUAL_INTERVENTION_TO_AVAILABLE: {
    key: 'MANUAL_INTERVENTION_TO_AVAILABLE',
    from: {
      key: KEYS.manual_intervention,
      transaction: MANUAL_INTERVENTION_DECREASE,
    },
    to: {
      key: KEYS.available,
      transaction: AVAILABLE_INCREASE,
    },
  },
  MANUAL_INTERVENTION_TO_RESERVED: {
    key: 'MANUAL_INTERVENTION_TO_RESERVED',
    from: {
      key: KEYS.manual_intervention,
      transaction: MANUAL_INTERVENTION_DECREASE,
    },
    to: {
      key: KEYS.reserved,
      transaction: RESERVED_INCREASE,
    },
  },
  MANUAL_INTERVENTION_TO_PACKED: {
    key: 'MANUAL_INTERVENTION_TO_PACKED',
    from: {
      key: KEYS.manual_intervention,
      transaction: MANUAL_INTERVENTION_DECREASE,
    },
    to: {
      key: KEYS.packed,
      transaction: PACKED_INCREASE,
    },
  },
  MANUAL_INTERVENTION_TO_OUTGOING: {
    key: 'MANUAL_INTERVENTION_TO_OUTGOING',
    from: {
      key: KEYS.manual_intervention,
      transaction: MANUAL_INTERVENTION_DECREASE,
    },
    to: {
      key: KEYS.outgoing,
      transaction: OUTGOING_INCREASE,
    },
  },
  MANUAL_INTERVENTION_TO_EXIT: {
    key: 'MANUAL_INTERVENTION_TO_EXIT',
    from: {
      key: KEYS.manual_intervention,
      transaction: MANUAL_INTERVENTION_DECREASE,
    },
    to: {
      key: KEYS.exit,
      transaction: EXIT_INCREASE,
    },
  },
}
