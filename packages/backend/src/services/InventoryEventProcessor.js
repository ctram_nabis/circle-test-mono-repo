import InventoryItemService from './InventoryItemService'

class InventoryEventProcessor {
  constructor() {
    this.inventoryItemService = new InventoryItemService()
  }

  dispatch(transitionType, changes) {
    const promises = changes.map((change) => {
      return Promise.all([
        transitionType.from && this.dispatchFrom(transitionType, change),
        this.dispatchTo(transitionType, change),
      ])
    })
    return Promise.all(promises)
  }

  dispatchFrom(transitionType, change) {
    return this.inventoryItemService.performUpdate(transitionType.from.key, transitionType.from.transaction, change)
  }

  dispatchTo(transitionType, change) {
    return this.inventoryItemService.performUpdate(transitionType.to.key, transitionType.to.transaction, change)
  }
}

export default InventoryEventProcessor
