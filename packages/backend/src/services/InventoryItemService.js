import Service from './Service'

class InventoryItemService extends Service {
  performUpdate = async (inventoryKey, transactionKey, change) => {
    const { amount, skuBatchId, warehouseId } = change

    const inventoryItem = await this.findInventoryItem(skuBatchId, warehouseId)
    if (!inventoryItem) {
      // serious error!
      return null
    }

    // save before
    const before = { ...inventoryItem }

    // perform update
    inventoryItem[inventoryKey] = amount
    const after = await inventoryItem.save()

    return this.createTransaction({
      type: transactionKey,
      before,
      after,
      inventoryItem,
      amount,
    })
  }

  findInventoryItem = (skuBatchId, warehouseId) => {
    const { InventoryItem } = this.getModels()
    return InventoryItem.findOne({
      skuBatchId,
      warehouseId,
    })
  }

  createTransaction = (payload) => {
    const { Transaction } = this.getModels()
    return Transaction.create(payload)
  }
}

export default InventoryItemService
