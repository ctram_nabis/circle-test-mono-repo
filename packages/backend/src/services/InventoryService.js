import InventoryEventProcessor from './InventoryEventProcessor'
import Transitions from './InventoryTransitions'

class InventoryService {
  constructor() {
    this.processor = new InventoryEventProcessor()
  }

  createAvailableToReserved(changes) {
    return this.processor.dispatch(Transitions.AVAILABLE_TO_RESERVED, changes)
  }

  createDeliveredToManualIntervention(changes) {
    return this.processor.dispatch(Transitions.DELIVERED_TO_MANUAL_INTERVENTION, changes)
  }

  createIncomingToAvailable(changes) {
    return this.processor.dispatch(Transitions.INCOMING_TO_AVAILABLE, changes)
  }

  createIncomingToManualIntervention(changes) {
    return this.processor.dispatch(Transitions.INCOMING_TO_MANUAL_INTERVENTION, changes)
  }

  createIncomingToQuarantine(changes) {
    return this.processor.dispatch(Transitions.INCOMING_TO_MANUAL_INTERVENTION, changes)
  }

  createManualInterventionToAvailable(changes) {
    return this.processor.dispatch(Transitions.MANUAL_INTERVENTION_TO_AVAILABLE, changes)
  }

  createManualInterventionToExit(changes) {
    return this.processor.dispatch(Transitions.MANUAL_INTERVENTION_TO_EXIT, changes)
  }

  createManualInterventionToOutgoing(changes) {
    return this.processor.dispatch(Transitions.MANUAL_INTERVENTION_TO_OUTGOING, changes)
  }

  createManualInterventionToPacked(changes) {
    return this.processor.dispatch(Transitions.MANUAL_INTERVENTION_TO_PACKED, changes)
  }

  createManualInterventionToReserved(changes) {
    return this.processor.dispatch(Transitions.MANUAL_INTERVENTION_TO_RESERVED, changes)
  }

  createOutgoingToDelivered(changes) {
    return this.processor.dispatch(Transitions.OUTGOING_TO_DELIVERED, changes)
  }

  createOutgoingToManualIntervention(changes) {
    return this.processor.dispatch(Transitions.OUTGOING_TO_MANUAL_INTERVENTION, changes)
  }

  createPackedToManualIntervention(changes) {
    return this.processor.dispatch(Transitions.PACKED_TO_MANUAL_INTERVENTION, changes)
  }

  createPackedToOutgoing(changes) {
    return this.processor.dispatch(Transitions.PACKED_TO_OUTGOING, changes)
  }

  createPromised(changes) {
    return this.processor.dispatch(Transitions.PROMISED, changes)
  }

  createPromisedToIncoming(changes) {
    return this.processor.dispatch(Transitions.PROMISED_TO_INCOMING, changes)
  }

  createQuarantineToAvailable(changes) {
    return this.processor.dispatch(Transitions.QUARANTINE_TO_AVAILABLE, changes)
  }

  createQuarantineToExit(changes) {
    return this.processor.dispatch(Transitions.QUARANTINE_TO_EXIT, changes)
  }

  createReservedToAvailable(changes) {
    return this.processor.dispatch(Transitions.RESERVED_TO_AVAILABLE, changes)
  }

  createReservedToPacked(changes) {
    return this.processor.dispatch(Transitions.RESERVED_TO_PACKED, changes)
  }
}

export default InventoryService
