import db from '../connections/postgres'

class Service {
  constructor() {
    this.db = db
  }

  getModels() {
    return this.db.base.db.models
  }
}

export default Service
