/*       */

import { GraphQLInputObjectType } from 'graphql'
import has from 'lodash/has'

import buildField from './buildField'

export default function buildInputType({ registry, definition, descriptions = {} }) {
  const fields = definition.fields.map((field) => {
    if (has(field, 'name.value') && descriptions[field.name.value]) {
      field.description = descriptions[field.name.value]
    }
    return field
  })

  const buildFields = () =>
    fields.reduce(
      (previous, fieldDefinition) => ({
        ...previous,
        ...buildField(registry, fieldDefinition),
      }),
      {},
    )

  /**
   *  Whenever a type is being built apply the middleware to wrap the type and its fields.
   *  Compose works from the bottom up so injectResolvers will be called first.
   */
  return registry._wrapInputType(
    new GraphQLInputObjectType({
      name: definition.name.value,
      description: descriptions.__this__ || null,
      fields: buildFields,
    }),
  )
}
