/*       */

import getType from './getType'

import buildInputValue from './buildInputValue'

export default function buildField(registry, fieldDefinition) {
  const fieldType = fieldDefinition.type

  const args = fieldDefinition.arguments
    ? fieldDefinition.arguments.reduce(
        (previous, inputValueDefinition) => ({
          ...previous,
          ...buildInputValue(registry, inputValueDefinition),
        }),
        {},
      )
    : []

  const field = {
    type: getType(registry, fieldType),
    description: null,
    args,
  }

  return {
    [fieldDefinition.name.value]: field,
  }
}
