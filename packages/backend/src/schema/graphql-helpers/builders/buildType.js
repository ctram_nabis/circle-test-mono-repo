/*       */

import { GraphQLObjectType } from 'graphql'

import { compose } from '../functools'

import injectResolvers from './injectResolvers'

import buildField from './buildField'

export default function buildType(registry, definition, resolvers = {}) {
  const interfaces = () =>
    definition['interfaces'].map((namedType) => registry.getInterface(namedType['name']['value']))
  const fields = definition['fields']

  const buildFields = () => {
    return fields.reduce((previous, fieldDefinition) => {
      return {
        ...previous,
        ...buildField(registry, fieldDefinition),
      }
    }, {})
  }

  return new GraphQLObjectType({
    name: definition['name']['value'],
    fields: () =>
      compose(
        injectResolvers(resolvers),
        registry._wrapTypeFields,
      )(buildFields),
    interfaces,
  })
}
