import { GraphQLScalarType } from 'graphql'

export default function buildScalar(config) {
  return new GraphQLScalarType({
    name: config.definition.name.value,
    description: config.description,
    serialize: config.serialize || ((val) => val),
    parseValue: config.parseValue || ((val) => val),
    parseLiteral: config.parseLiteral || ((val) => val),
  })
}
