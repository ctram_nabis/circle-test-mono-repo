import { GraphQLEnumType } from 'graphql'

export default function buildEnum(config) {
  const descriptions = config.descriptions || {}
  const values = config.definition.values.reduce((acc, val) => {
    const valName = val.name.value
    const enumVal = {
      value: valName,
    }
    if (descriptions[valName]) {
      enumVal.description = descriptions[valName]
    }
    return {
      ...acc,
      [valName]: enumVal,
    }
  }, {})
  const name = config.definition.name.value
  return new GraphQLEnumType({
    name,
    description: descriptions.__this__ || `Values for the ${name} enum`,
    values,
  })
}
