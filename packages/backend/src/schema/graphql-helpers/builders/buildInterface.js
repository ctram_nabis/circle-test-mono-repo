/*       */

import { GraphQLInterfaceType } from 'graphql'

import buildField from './buildField'

export default function buildInterface(registry, definition, resolveType) {
  const buildFields = () => {
    return definition['fields'].reduce((previous, fieldDefinition) => {
      return {
        ...previous,
        ...buildField(registry, fieldDefinition),
      }
    }, {})
  }

  return new GraphQLInterfaceType({
    name: definition['name']['value'],
    fields: buildFields,
    resolveType,
  })
}
