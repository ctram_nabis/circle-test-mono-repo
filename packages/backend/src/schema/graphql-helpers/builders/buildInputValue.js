/*       */

import getType from './getType'

export default function buildInputValue(registry, inputValueDefinition) {
  return {
    [inputValueDefinition['name']['value']]: {
      type: getType(registry, inputValueDefinition.type),
    },
  }
}
