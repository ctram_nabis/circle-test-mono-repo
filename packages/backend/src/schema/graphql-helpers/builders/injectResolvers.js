/*       */

import { resolveThunk } from '../functools'

export default function injectResolversMiddleware(resolvers) {
  return (fieldThunk) => {
    const fields = resolveThunk(fieldThunk)

    return Object.keys(fields).reduce((previous, key) => {
      const field = fields[key]

      return {
        ...previous,
        [key]: {
          ...field,
          resolve: resolvers[key] || field.resolve,
        },
      }
    }, {})
  }
}
