/*       */

import {
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLEnumType,
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLString,
  parse,
} from 'graphql'

import * as builders from './builders'
import mergeTypes from './builders/mergeTypes'

const identity = (obj) => obj

const parseSpec = (spec) => {
  try {
    const ast = parse(spec)

    if (ast.definitions.length !== 1) {
      throw new Error(`Documents must contain exactly one definition (found: ${ast.definitions.length})`)
    }
    return ast.definitions[0]
  } catch (error) {
    throw new Error(`Couldn't parse spec: ${spec}`)
  }
}

export default class Registry {
  constructor(middleware = null) {
    this.middleware = middleware
    this.types = {}
    this.interfaces = {}
    this.mutations = []
    this.addType(GraphQLID)
    this.addType(GraphQLInt)
    this.addType(GraphQLBoolean)
    this.addType(GraphQLFloat)
    this.addType(GraphQLString)

    if (middleware && middleware.wrapTypeFields) {
      this._wrapTypeFields = middleware.wrapTypeFields
    } else {
      this._wrapTypeFields = identity
    }

    if (middleware && middleware.wrapMutations) {
      this._wrapMutations = middleware.wrapMutations
    } else {
      this._wrapMutations = identity
    }

    if (middleware && middleware.wrapInputType) {
      this._wrapInputType = middleware.wrapInputType
    } else {
      this._wrapInputType = identity
    }
  }

  addType(obj) {
    this.types[obj.name] = obj
  }

  buildType(spec, resolvers = {}) {
    return builders.buildType(this, parseSpec(spec), resolvers)
  }

  createType(spec, resolvers = {}) {
    const built = builders.buildType(this, parseSpec(spec), resolvers)
    this.addType(built)
    return built
  }

  createInputType(opts) {
    const built = builders.buildInputType({
      registry: this,
      definition: parseSpec(opts.spec),
      ...opts,
    })
    this.addType(built)
    return built
  }

  createEnum(opts) {
    const built = builders.buildEnum({
      registry: this,
      definition: parseSpec(opts.spec),
      ...opts,
    })
    this.addType(built)
    return built
  }

  addInterface(obj) {
    this.interfaces[obj.name] = obj
    this.types[obj.name] = obj
  }

  buildInterface(spec, resolveType) {
    return builders.buildInterface(this, parseSpec(spec), resolveType)
  }

  createInterface(spec, resolveType) {
    const built = builders.buildInterface(this, parseSpec(spec), resolveType)
    this.addInterface(built)
    return built
  }

  addConnection(types) {
    this.addType(types.connectionType)
    this.addType(types.edgeType)
  }

  getType(name) {
    return this.types[name]
  }

  getInterface(name) {
    return this.interfaces[name]
  }

  createScalar(opts) {
    const built = builders.buildScalar({
      registry: this,
      definition: parseSpec(opts.spec),
      ...opts,
    })
    this.addType(built)
    return built
  }

  addMutations(mutations) {
    this.mutations = [...this.mutations, mutations]
  }

  createMutations(spec, resolvers = {}) {
    const built = this._wrapMutations(builders.buildType(this, parseSpec(spec), resolvers))
    this.addMutations(built)
    return built
  }

  getMutationType() {
    /* Returns a merged aggregate type of all registered mutations */
    return mergeTypes('Mutation', ...this.mutations)
  }
}
