/*       */

import { toGlobalId, fromGlobalId } from 'graphql-relay'
import get from 'lodash/get'
import set from 'lodash/set'
import { compose } from '../../functools'

export const resolve = (...middlewares) => {
  return compose(...middlewares)
}

export const globalId = (fn) => (obj, args, context, info) => {
  if (typeof fn === 'function') {
    return toGlobalId(info.parentType.name, fn(obj))
  } else if (typeof fn === 'string') {
    return toGlobalId(info.parentType.name, obj[fn])
  }
  return toGlobalId(info.parentType.name, obj[info.fieldName])
}

export const unpackId = (input) => {
  if (!input) {
    return null
  }
  const { id } = fromGlobalId(input)
  return id || null
}

export const unmask = (...fields) => (executorFn) => (obj, args, context, info) =>
  executorFn(
    obj,
    {
      ...args,
      ...fields.reduce((prev, field) => {
        if (typeof field === 'function') {
          field(args)
        } else {
          const globalized = get(args, field)
          if (globalized) {
            const deglobalized = fromGlobalId(globalized)
            const { id, type } = deglobalized
            set(args, field, id)
          }
        }

        return {
          ...prev,
          ...args,
        }
      }, {}),
    },
    context,
    info,
  )

unmask.array = (...fields) => (input) =>
  fields.reduce(
    (prev, field) => ({
      ...prev,
      [field]: input[field].map((id) => unpackId(id)),
    }),
    {},
  )

unmask.inputArray = (...fields) => (executorFn) => (obj, args, context, info) =>
  executorFn(
    obj,
    {
      ...args,
      input: [
        ...args.input.map((item) => {
          return {
            ...fields.reduce((prev, field) => {
              if (typeof field === 'function') {
                field(item)
              } else {
                const globalized = get(item, field)
                if (globalized) {
                  const deglobalized = fromGlobalId(globalized)
                  const { id, type } = deglobalized
                  set(item, field, id)
                }
              }

              return {
                ...prev,
                ...item,
              }
            }, {}),
          }
        }),
      ],
    },
    context,
    info,
  )
