import httpError from 'http-errors'
import get from 'lodash/get'
import logger from '../../../../logging'

const authUser = (fn) => async (obj, args, context, info) => {
  const db = context.connections.logisticsDB
  if (!context.user) {
    logger.error(`Request ${context.id}`, 'In resolver middleware: authUser()')
    throw httpError(
      401,
      `User is not authorized to access path: ${info.path && info.path.key ? info.path.key : info.path}`,
    )
  }

  context.user = await db.User.findByPk(context.user.id, {
    include: [{ association: 'roles' }, { association: 'organizations' }],
  })

  try {
    return fn(obj, args, context, info)
  } catch (e) {
    throw e
  }
}

const authAdminUser = (fn) => (obj, args, context, info) => {
  if (!get(context, 'user.isAdmin')) {
    logger.error(`Request ${context.id}`, 'In resolver middleware: authAdminUser()')
    throw httpError(
      401,
      `User is not authorized to access path: ${info.path && info.path.key ? info.path.key : info.path}`,
    )
  }
  try {
    return fn(obj, args, context, info)
  } catch (e) {
    throw e
  }
}

const authClient = (fn) => (obj, args, context, info) => {
  if (!context.client) {
    logger.error(`Request ${context.id}`, 'In resolver middleware: authClient()')
    throw httpError(
      401,
      `Client is not authorized to access path: ${info.path && info.path.key ? info.path.key : info.path}`,
    )
  }
  try {
    return fn(obj, args, context, info)
  } catch (e) {
    throw e
  }
}

export { authUser, authAdminUser, authClient }
