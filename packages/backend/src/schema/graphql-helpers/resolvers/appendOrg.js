const appendOrg = (fn) => (obj, args, context, info) => {
  args.input.organizationId = context.client.id
  try {
    return fn(obj, args, context, info)
  } catch (e) {
    throw e
  }
}

export default appendOrg
