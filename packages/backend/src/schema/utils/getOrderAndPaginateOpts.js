// Add Pagination and Ordering
const getOrderAndPaginateOpts = (pageInfo = {}) => {
  const { page = 1, numItemsPerPage = 50, orderBy = [] } = pageInfo

  const orderOpts = {}
  const paginateOpts = {}

  /**
   * Order
   */
  if (orderBy && orderBy.length) {
    const order = orderBy.reduce((acc, val) => {
      const orderByListTuple = [...val.attribute.split('.'), val.order]
      acc.push(orderByListTuple)
      return acc
    }, [])

    orderOpts.order = order
  } else {
    // Default
    orderOpts.order = [['createdAt', 'DESC']]
  }

  /**
   * Paginate
   */
  paginateOpts.offset = (page - 1) * numItemsPerPage
  paginateOpts.limit = numItemsPerPage

  const newOpts = {
    orderOpts,
    paginateOpts,
  }
  return newOpts
}

export default getOrderAndPaginateOpts
