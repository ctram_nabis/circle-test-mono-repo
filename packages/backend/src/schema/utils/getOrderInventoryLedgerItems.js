import Sequelize from 'sequelize'

export const getOrderInventoryLedgerItems = async (db, args) => {
  const { orderId, transactionType } = args

  const result = await db.InventoryLedgerItem.findAll({
    where: {
      orderId: orderId,
      transactionType,
    },
    attributes: [Sequelize.fn('DISTINCT', Sequelize.col('orderId')), 'orderId', 'transactionType'],
  })

  return result
}
