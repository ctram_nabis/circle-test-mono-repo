export const getOrganizationSubscribers = async (organizationId, ctx) => {
  const { Organization, UserPreference } = ctx.connections.logisticsDB

  const organization = await Organization.findOne({
    include: [
      {
        association: Organization.Users,
      },
    ],
    where: { id: organizationId },
  })

  if (!organization) {
    throw new Error(`No organization found for id: ${organizationId}`)
  }

  const { users } = organization

  const allOrgUsersPreferences = await UserPreference.findAll({
    where: {
      organizationId,
      receiveOrderUpdateEmails: true,
    },
  })

  const subscriberUserIds = allOrgUsersPreferences.map(({ userId }) => userId)

  const usersToEmail = users.reduce(
    (acc, { email, id }) => (subscriberUserIds.includes(id) ? [...acc, email] : acc),
    [],
  )

  const unsubscribedUsers = users.filter(({ email }) => !usersToEmail.includes(email))

  console.log({ usersToEmail, numAllOrgUsers: users.length, numUsersToEmail: usersToEmail.length, unsubscribedUsers })

  return usersToEmail
}

export default getOrganizationSubscribers
