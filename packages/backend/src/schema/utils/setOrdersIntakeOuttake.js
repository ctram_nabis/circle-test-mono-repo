import Sequelize from 'sequelize'

const setOrdersIntakeOuttake = async (orders, db) => {
  const orderIds = orders.map((o) => o.id)

  const getRelatedInventoryLedgerItems = async (transactionType) => {
    const result = await db.InventoryLedgerItem.findAll({
      where: {
        orderId: orderIds,
        transactionType,
      },
      attributes: [Sequelize.fn('DISTINCT', Sequelize.col('orderId')), 'orderId', 'transactionType'],
    }).reduce((acc, ili) => {
      const { orderId } = ili
      acc[orderId] = ili
      return acc
    }, {})

    return result
  }

  const intakenRes = await getRelatedInventoryLedgerItems('INTAKE')
  const outtakenRes = await getRelatedInventoryLedgerItems('OUTTAKE')

  const newOrdersResults = orders.map((o) => {
    const _o = o
    _o.intaken = Boolean(intakenRes[_o.id])
    _o.outtaken = Boolean(outtakenRes[_o.id])
    return _o
  })

  return newOrdersResults
}

export default setOrdersIntakeOuttake
