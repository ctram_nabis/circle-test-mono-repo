import uniq from 'lodash/uniq'
import { toGlobalId } from 'graphql-relay'

import config from '../../config'
import { IS_PROD } from '../../constants'
import logger from '../../logging'
import buildAddress from '../../utils/buildAddress'
import dollarFormat from '../../utils/dollarFormat'
import { toChangeLogDate } from '../../utils/parseDate'
import titleize from '../../utils/titleize'

import { getOrganizationSubscribers } from './getOrganizationSubscribers'

const sharedStyles = 'border: 1px solid #dddddd; text-align: left; padding: 8px;'

const statusEmailTemplate = {
  UNSCHEDULED: '08290ea5-a9a3-4a0b-bf12-d2a6dd11a79b',
  SCHEDULED: '811cffcc-6afa-451b-9cd7-d53f141c1769',
  TRANSFERRING: 'd67b20fa-11aa-4345-b154-13734ca07414',
  DELAYED: 'c0650204-2502-4c21-ae6f-5ee06fb07b0a',
  CANCELLED: '1f74e6a5-24f5-4a03-9eb2-6b734ae0c74f',
  REJECTED: 'ad1968fa-9810-40bf-8d5e-aa3b4def4de3',
  DELIVERED: 'e14b1f3d-a4c1-4270-ad08-196ae36e81e4',
  DELIVERED_WITH_EDITS: '50b87cb8-bf3d-42fc-b320-0b350956ecb7',
}

// request coming from new order mutation
export const generateOrderConfirmationEmail = async (order, org, ctx) => {
  const {
    number: orderNumber,
    status,
    paymentStatus,
    gmv,
    exciseTax,
    gmvCollected,
    exciseTaxCollected,
    manifestGDriveFileId,
    invoicesS3FileLink,
    notes,
    adminNotes,
    date,
    creator,
    site,
    siteId,
    timeWindow,
  } = order
  const isNewOrder = status === 'UNSCHEDULED'

  const { name: orgName, alias: orgAlias } = org

  const _site = site || (await ctx.connections.logisticsDB.Site.findByPk(siteId))
  const { name: siteName, pocName, pocPhoneNumber, pocEmail, siteCategory } = _site

  const orgSubscribers = await getOrganizationSubscribers(org.id, ctx)

  const { email, lastName, firstName } = creator

  const usersToEmail = ['deliveries@nabis.com', ...orgSubscribers]

  const manifestLink = invoicesS3FileLink
    ? `<a href="${invoicesS3FileLink}">Shipping Manifest (PDF)</a>`
    : manifestGDriveFileId
    ? `<a href="https://drive.google.com/file/d/${manifestGDriveFileId}">Shipping Manifest (PDF)</a>`
    : ''

  const orderUrl = `${config.nabis.baseUrl}/${orgAlias}/app/orders/${toGlobalId('Order', order.id)}`
  const unsubscribeUrl = `${config.nabis.baseUrl}/account`

  const templateId = statusEmailTemplate[status || 'UNSCHEDULED']

  const uniqueEmails = uniq(usersToEmail)

  logger.debug(`ORDER_UPDATE_EMAIL would send to`, { uniqueEmails })

  const to = IS_PROD ? uniqueEmails : ['engineering+test@nabis.com']
  // To dynamically add email to body: https://github.com/sendgrid/sendgrid-nodejs/issues/660
  const emailSubKey = 'email'
  const personalizations = to.map((_email) => ({
    to: _email,
    substitutions: {
      [emailSubKey]: _email,
    },
  }))
  const from = 'deliveries@nabis.com'
  const subject = `[Nabis Deliveries] 📦 ${
    isNewOrder ? 'NEW' : status
  } - Order: ${orderNumber} (${orgName}) - ${siteName}`
  const html = `
  <div>
    <a href="${orderUrl}">Order: ${orderNumber}</a><br />
    ${manifestLink}

    <div>Submitted By: <strong>${firstName} ${lastName} (${email})</strong></div>

    <br />

    <div>Order Status: <strong>${status}</strong></div>
    <div>Order Delivery Date: ${toChangeLogDate(date)}</div>
    <div>Delivery Window: ${timeWindow || ''}</div>
    <div>Order Payment Status: <strong>${paymentStatus}</strong></div>

    <br />

    <b>Notes:</b>
    <div>${notes || 'None'}</div>

    <br />

    <b>Admin Notes:</b>
    <div>${adminNotes || 'None'}</div>

    <br />

    <div>${titleize(siteCategory)}:</div>
    <div><strong>${siteName}</strong></div>
    <div>${buildAddress(_site)}</div>

    <br />

    <div>Contact 📞:</div>
    <div>${pocName}</div>
    <div>${pocPhoneNumber} | <a href="mailto:${pocEmail}">${pocEmail}</a></div>

    <br />
    <hr />
    <br />

    <table>
      <tr>
        <th style="${sharedStyles}"><u><b>Payment Information</b></u></th>
        <th style="${sharedStyles}">Invoiced</th>
        ${!isNewOrder ? `<th style="${sharedStyles}">Collected</th>` : ''}
      </tr>
      <tr style="background-color: #dddddd">
        <td style="${sharedStyles}">Gross Merchandise Value (GMV)</td>
        <td style="${sharedStyles}">${dollarFormat(gmv)}</td>
        ${!isNewOrder ? `<td style="${sharedStyles}">${dollarFormat(gmvCollected)}</td>` : ''}
      </tr>
      <tr>
        <td style="${sharedStyles}">Excise Tax</td>
        <td style="${sharedStyles}">${dollarFormat(exciseTax)}</td>
        ${!isNewOrder ? `<td style="${sharedStyles}">${dollarFormat(exciseTaxCollected)}</td>` : ''}
      </tr>
      <tr style="background-color: #dddddd">
        <td style="${sharedStyles}"><b>Total</b></td>
        <td style="${sharedStyles}"><b>${dollarFormat(gmv + exciseTax)}</b></td>
        ${
          !isNewOrder
            ? `<td style="${sharedStyles}"><b>${dollarFormat(gmvCollected + exciseTaxCollected)}</b></td>`
            : ''
        }
      </tr>
    </table>

    <br />
    <hr />
    <br />

    <div>
      You are receiving this message because {{${emailSubKey}}} is listed as a member linked to the organization.
      To view and manage your notifications, visit <a href="${unsubscribeUrl}">${unsubscribeUrl}</a>
    </div>
  </div>
`

  logger.debug({ personalizations, from, subject, html, templateId })

  return { personalizations, from, subject, html, templateId }
}
