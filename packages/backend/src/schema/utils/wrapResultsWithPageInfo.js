import idx from 'idx'

const wrapResultsWithPageInfo = async (dbModel, opts, { pageInfo }) => {
  const defaultPageInfo = {
    numItemsPerPage: 50,
    orderBy: [
      {
        attribute: 'createdAt',
        order: 'DESC',
      },
    ],
    page: 1,
  }

  const totalNumItems = await dbModel.count(opts)
  const totalNumPages = Math.ceil(
    totalNumItems / (idx(pageInfo, (_) => _.numItemsPerPage) || defaultPageInfo.numItemsPerPage),
  )

  const pageInfoReturn = {
    ...defaultPageInfo,
    ...pageInfo,
    totalNumItems,
    totalNumPages,
  }

  return pageInfoReturn
}

export default wrapResultsWithPageInfo
