import { resolve } from '../graphql-helpers'

export const registerSiteTypes = (registry) => {
  // Enums

  registry.createEnum({
    spec: `
      enum SiteCategoryEnum {
        RETAILER
        MANUFACTURER
        CULTIVATOR
        CONSUMER
        DISTRIBUTOR
        MICROBUSINESS
        TESTING
        OTHER
      }
    `,
  })

  // Object Types

  registry.createType(
    `
    type Site {
      id: ID!
      name: String!
      pocName: String
      pocPhoneNumber: String
      pocEmail: String
      address1: String
      address2: String
      city: String
      state: String
      zip: String
      lat: Float
      lng: Float
      siteCategory: SiteCategoryEnum!
      organization: Organization!
      licenses: [License!]!
      inventoryItems: [InventoryItem!]!
      licensedLocation: LicensedLocation
      licensedLocationId: ID!
      createdAt: DateTime!
      updatedAt: DateTime!
    }
  `,
    {
      organization: resolve()((obj, _args, context, _info) => {
        return context.connections.logisticsDB.Organization.findOne({
          where: { id: obj.organizationId },
        })
      }),
      licenses: resolve()((obj, _args, context, _info) => {
        return context.connections.logisticsDB.License.findAll({
          where: {
            siteId: obj.id,
          },
          order: [['createdAt', 'ASC']],
        })
      }),
      inventoryItems: resolve()((obj, _args, context, _info) => {
        return context.connections.logisticsDB.InventoryItem.findAll({
          where: {
            siteId: obj.id,
          },
          order: [['createdAt', 'ASC']],
        })
      }),
      licensedLocation: resolve()((obj, _args, context) => {
        return context.connections.logisticsDB.LicensedLocation.findOne({
          where: { id: obj.licensedLocationId },
        })
      }),
    },
  )

  registry.createType(`
    type CreateSitePayload {
      changedSite: Site!
    }
  `)

  registry.createType(`
    type UpdateSitePayload {
      changedSite: Site!
    }
  `)

  registry.createType(`
    type DeleteSitePayload {
      changedSite: Site!
    }
  `)

  // Input Types

  registry.createInputType({
    spec: `
      input CreateSiteInput {
        name: String!
        pocName: String
        pocPhoneNumber: String
        pocEmail: String
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        lat: Float
        lng: Float
        siteCategory: SiteCategoryEnum!
        licenses: [CreateLicenseInput]
        organizationId: ID!
        licensedLocationId: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateSiteInput {
        id: ID!
        name: String
        pocName: String
        pocPhoneNumber: String
        pocEmail: String
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        lat: Float
        lng: Float
        siteCategory: SiteCategoryEnum
        organizationId: ID
        licensedLocationId: ID
        licenses: [CreateLicenseInput!]
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteSiteInput {
        id: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input CreateLicensedLocationInput {
        name: String!
        address1: String!
        address2: String
        city: String!
        state: String!
        zip: String!
        lat: Float
        lng: Float
        siteCategory: SiteCategoryEnum!
        licenses: [CreateLicenseInput!]!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateLicensedLocationInput {
        id: ID!
        name: String
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        lat: Float
        lng: Float
        siteCategory: SiteCategoryEnum
        licenses: [CreateLicenseInput]
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteLicensedLocationInput {
        id: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UnarchiveLicensedLocationInput {
        id: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input FullSiteInput {
        id: ID!
        name: String!
        pocName: String!
        pocPhoneNumber: String!
        pocEmail: String!
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        lat: Float
        lng: Float
        siteCategory: SiteCategoryEnum
      }
    `,
  })
}
