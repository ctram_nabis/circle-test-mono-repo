import { resolve } from '../graphql-helpers'

const registerLicenseTypes = (registry) => {
  registry.createEnum({
    spec: `
      enum LicenseCategoryEnum {
        ADULT_USE
        MEDICINAL
        COMBINED
      }
    `,
  })

  registry.createEnum({
    spec: `
      enum LicenseTypeEnum {
        RETAILER
        RETAILER_NONSTOREFRONT
        DISTRIBUTOR
        DISTRIBUTOR_TRANSPORT_ONLY
        MICROBUSINESS
        TESTING
        CANNABIS_EVENTS
        MANUFACTURER
        CULTIVATOR
      }
    `,
  })

  registry.createType(
    `
    type License {
      id: ID!
      nickname: String
      category: LicenseCategoryEnum!
      type: LicenseTypeEnum!
      licenseNumber: String!
      legalEntityName: String!
      issuanceDate: DateTime
      expirationDate: DateTime
      contactName: String!
      contactPhone: String!
      contactEmail: String
      address1: String!
      address2: String
      city: String!
      state: String!
      zip: String!
      organization: Organization!
      site: Site!
    }
  `,
    {
      organization: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.Organization.findByPk(obj.organizationId)
      }),
      site: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.Site.findByPk(obj.siteId)
      }),
    },
  )

  registry.createType(`
    type CreateLicensePayload {
      changedLicense: License!
    }
  `)

  registry.createType(`
    type UpdateLicensePayload {
      changedLicense: License!
    }
  `)

  registry.createType(`
    type DeleteLicensePayload {
      changedLicense: License!
    }
  `)

  registry.createType(`
    type AllLicensesPayload {
      organizationLicenses: [License!]!
    }
  `)

  registry.createInputType({
    spec: `
      input CreateLicenseInput {
        nickname: String!
        category: LicenseCategoryEnum!
        type: LicenseTypeEnum!
        licenseNumber: String!
        legalEntityName: String!
        issuanceDate: DateTime
        expirationDate: DateTime
        contactName: String!
        contactPhone: String!
        contactEmail: String
        address1: String!
        address2: String
        city: String!
        state: String!
        zip: String!
        organizationId: ID
        siteId: ID
        licensedLocationId: ID
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateLicenseInput {
        id: ID!
        nickname: String
        category: LicenseCategoryEnum
        type: LicenseTypeEnum
        licenseNumber: String
        legalEntityName: String
        issuanceDate: DateTime
        expirationDate: DateTime
        contactName: String
        contactPhone: String
        contactEmail: String
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        organizationId: ID
        siteId: ID
        licensedLocationId: ID
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteLicenseInput {
        id: ID!
      }
    `,
  })
}

export { registerLicenseTypes }
