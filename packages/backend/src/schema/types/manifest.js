const registerManifestTypes = (registry) => {
  registry.createType(`
    type GenerateManifestPayload {
      apSummaryGDriveFileId: String
      invoicesS3FileLink: String
    }
  `)

  registry.createInputType({
    spec: `
      input GenerateManifestInput {
        order: FullOrderInput!
      }
    `,
  })
}

export { registerManifestTypes }
