const registerOnfleetTypes = (registry) => {
  registry.createInputType({
    spec: `
    type UploadToOnfleetInput {
      orders: [ID!]!
    }
  `,
  })

  registry.createType(`
    type UploadToOnfleetPayload {
      ok: Boolean!
      errors: JSON
    }
  `)
}

export { registerOnfleetTypes }
