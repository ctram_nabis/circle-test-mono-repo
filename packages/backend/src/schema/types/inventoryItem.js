import { resolve } from '../graphql-helpers'

const registerInventoryItemTypes = (registry) => {
  registry.createEnum({
    spec: `
      enum InventoryStorageType {
        INVENTORY
        QUARANTINE
      }
    `,
  })

  registry.createType(
    `
    type InventoryItem {
      id: ID!
      counted: Float!
      allocated: Float!
      promised: Float!
      storageType: InventoryStorageType!
      createdAt: DateTime!
      updatedAt: DateTime!
      skuBatch: SkuBatch!
      warehouse: Warehouse!
      organization: Organization!
      inventoryLedgerItems: [InventoryLedgerItem!]!
    }
  `,
    {
      inventoryLedgerItems: resolve()(({ id }, _args, context) =>
        context.connections.logisticsDB.InventoryLedgerItem.findAll({
          order: [['createdAt', 'DESC']],
          where: {
            inventoryItemId: id,
          },
        }),
      ),
      organization: resolve()(({ organizationId }, _args, context) =>
        context.connections.logisticsDB.Organization.findOne({
          where: {
            id: organizationId,
          },
        }),
      ),
      skuBatch: resolve()(({ skuBatchId }, _args, context) =>
        context.connections.logisticsDB.SkuBatch.findOne({
          paranoid: false,
          where: {
            id: skuBatchId,
          },
        }),
      ),
      warehouse: resolve()(({ warehouseId }, _args, context) =>
        context.connections.logisticsDB.Warehouse.findOne({
          where: {
            id: warehouseId,
          },
        }),
      ),
    },
  )

  registry.createType(`
    type CreateInventoryItemPayload {
      changedInventoryItem: InventoryItem!
    }
  `)

  registry.createType(`
    type UpdateInventoryItemPayload {
      changedInventoryItem: InventoryItem!
    }
  `)

  registry.createType(`
    type DeleteInventoryItemPayload {
      changedInventoryItem: InventoryItem!
    }
  `)

  registry.createType(`
    type InventoryRow {
      skuBatch: SkuBatch!
      totalQuantity: Float!
      lastUpdatedAt: DateTime!
    }
  `)

  registry.createInputType({
    spec: `
      input UpdateInventoryItemInput {
        id: ID!
        counted: Float
        allocated: Float
        promised: Float
        storageType: InventoryStorageType
        skuBatchId: ID
        warehouseId: ID
        organizationId: ID
      }
    `,
  })

  registry.createInputType({
    spec: `
      input CorrectInventoryItemInput {
        id: ID!
        endCounted: Float!
        transactionType: InventoryTransactionType!
        notes: String!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteInventoryItemInput {
        id: ID!
      }
    `,
  })
}

export { registerInventoryItemTypes }
