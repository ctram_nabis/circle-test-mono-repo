import { WAREHOUSES } from '../../../constants'
import { getCounted } from './getCounted'
import { getExternalIncomingCount } from './getExternalIncomingCount'
import { getOutgoingCount } from './getOutgoingCount'
import { getTransferIncomingCount } from './getTransferIncomingCount'

export const getInventoryValues = async (obj, _args, context) => {
  const response = {}

  const variables = { skuBatchId: obj.id, context }

  const [totalCounted, outgoingCount, transferIncomingCount, externalIncomingCount] = await Promise.all([
    getCounted(variables),
    getOutgoingCount(variables),
    getTransferIncomingCount(variables),
    getExternalIncomingCount(variables),
  ])

  /**
   * @chrisbull - Chris - Ops has requested that we no longer add brand deliveries as available
   * inventory and we are adding back allocated with both incoming and allocated
   */

  Object.keys(WAREHOUSES).forEach((key) => {
    const { warehouseId } = WAREHOUSES[key]

    const counted = totalCounted[warehouseId]
    const outgoing = outgoingCount[warehouseId]

    const incomingTransfers = transferIncomingCount[warehouseId]
    const incomingFromBrand = externalIncomingCount[warehouseId]

    const incoming = incomingTransfers + incomingFromBrand

    // Calulated Values
    const allocated = incoming - outgoing

    // updated, remove all incoming from available
    const available = counted - outgoing

    response[key] = {
      allocated,
      available,
      counted,
      incoming,
      outgoing,
    }
  })

  return response
}
