import { Op } from 'sequelize'
import { WAREHOUSES, QUERY_AFTER_LINE_ITEMS_DATE, ORDER_STATUS, ORDER_ACTIONS } from '../../../constants'

export const getTransferIncomingCount = async ({ skuBatchId, context }) => {
  const response = {}

  const { LineItem, Order } = context.connections.logisticsDB

  const activeLineItems = await LineItem.findAll({
    where: { skuBatchId },
    include: [
      {
        model: Order,
        as: 'order',
        where: {
          isArchived: false,
          date: { [Op.gte]: QUERY_AFTER_LINE_ITEMS_DATE },
          action: {
            [Op.in]: [ORDER_ACTIONS.INTERNAL_TRANSFER],
          }, // orders with inventory arriving at warehouse
          status: {
            [Op.in]: [
              ORDER_STATUS.SCHEDULED,
              ORDER_STATUS.UNSCHEDULED,
              ORDER_STATUS.TRANSFERRING,
              ORDER_STATUS.DELAYED,
              ORDER_STATUS.DELIVERED,
              ORDER_STATUS.DELIVERED_WITH_EDITS,
            ],
          }, // orders yet to be delivered
          intaken: false,
          isExcluded: false,
        },
      },
    ],
  })

  Object.keys(WAREHOUSES).forEach((key) => {
    const { warehouseId } = WAREHOUSES[key]
    response[warehouseId] = 0
  })

  activeLineItems.forEach(({ quantity, order }) => {
    let warehouseId
    const { licensedLocationId } = order

    // if the orders licensed location (eg. destination) is target warehouse, add
    Object.keys(WAREHOUSES).forEach((key) => {
      const warehouse = WAREHOUSES[key]
      if (warehouse.licensedLocationId === licensedLocationId) {
        warehouseId = warehouse.warehouseId
      }
    })

    response[warehouseId] += quantity
  })

  return response
}
