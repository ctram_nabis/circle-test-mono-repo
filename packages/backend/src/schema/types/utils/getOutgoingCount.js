import { Op } from 'sequelize'
import { WAREHOUSES, QUERY_AFTER_LINE_ITEMS_DATE, ORDER_STATUS, ORDER_ACTIONS } from '../../../constants'

export const getOutgoingCount = async ({ skuBatchId, context }) => {
  const response = {}

  const { LineItem, Order } = context.connections.logisticsDB

  const activeLineItems = await LineItem.findAll({
    where: { skuBatchId },
    include: [
      {
        model: Order,
        as: 'order',
        where: {
          isArchived: false,
          date: { [Op.gte]: QUERY_AFTER_LINE_ITEMS_DATE },
          action: {
            [Op.in]: [
              ORDER_ACTIONS.DELIVERY_TO_BRAND,
              ORDER_ACTIONS.DELIVERY_TO_RETAILER,
              ORDER_ACTIONS.PICKUP_FROM_NABIS,
              ORDER_ACTIONS.INTERNAL_TRANSFER,
              ORDER_ACTIONS.OTHER,
            ],
          }, // orders with inventory leaving warehouse
          status: {
            [Op.in]: [
              ORDER_STATUS.SCHEDULED,
              ORDER_STATUS.UNSCHEDULED,
              ORDER_STATUS.TRANSFERRING,
              ORDER_STATUS.DELAYED,
              ORDER_STATUS.DELIVERED,
              ORDER_STATUS.DELIVERED_WITH_EDITS,
            ],
          }, // orders yet to be delivered
          outtaken: false,
          isExcluded: false,
        },
      },
    ],
  })

  Object.keys(WAREHOUSES).forEach((key) => {
    const { warehouseId } = WAREHOUSES[key]
    response[warehouseId] = 0
  })

  activeLineItems.forEach(({ quantity, order }) => {
    // default to OAK if there is no warehouseId associated
    const { warehouseId = WAREHOUSES.OAK.warehouseId } = order
    response[warehouseId] += quantity
  })

  return response
}
