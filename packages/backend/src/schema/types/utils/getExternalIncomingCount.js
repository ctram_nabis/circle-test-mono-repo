import { Op } from 'sequelize'
import { WAREHOUSES, QUERY_AFTER_LINE_ITEMS_DATE, ORDER_STATUS, ORDER_ACTIONS } from '../../../constants'

export const getExternalIncomingCount = async ({ skuBatchId, context }) => {
  const response = {}

  const { LineItem, Order } = context.connections.logisticsDB

  const activeLineItems = await LineItem.findAll({
    where: { skuBatchId },
    include: [
      {
        model: Order,
        as: 'order',
        where: {
          isArchived: false,
          date: { [Op.gte]: QUERY_AFTER_LINE_ITEMS_DATE },
          action: {
            [Op.in]: [
              ORDER_ACTIONS.PICKUP_FROM_BRAND,
              ORDER_ACTIONS.RETURN_FROM_RETAILER,
              ORDER_ACTIONS.DROPOFF_TO_NABIS,
              ORDER_ACTIONS.OTHER,
            ],
          }, // orders with inventory arriving at warehouse
          status: {
            [Op.in]: [
              ORDER_STATUS.SCHEDULED,
              ORDER_STATUS.UNSCHEDULED,
              ORDER_STATUS.TRANSFERRING,
              ORDER_STATUS.DELAYED,
              ORDER_STATUS.DELIVERED,
              ORDER_STATUS.DELIVERED_WITH_EDITS,
            ],
          }, // orders yet to be delivered
          intaken: false,
          isExcluded: false,
        },
      },
    ],
  })

  Object.keys(WAREHOUSES).forEach((key) => {
    const { warehouseId } = WAREHOUSES[key]
    response[warehouseId] = 0
  })

  activeLineItems.forEach(({ quantity, order }) => {
    // default to OAK if there is no warehouseId associated
    const { warehouseId = WAREHOUSES.OAK.warehouseId } = order
    response[warehouseId] += quantity
  })

  return response
}
