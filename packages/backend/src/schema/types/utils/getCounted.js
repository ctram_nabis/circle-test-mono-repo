import { WAREHOUSES } from '../../../constants'

export const getCounted = async ({ skuBatchId, context }) => {
  const response = {}

  const inventoryItems = await context.connections.logisticsDB.InventoryItem.findAll({
    where: { skuBatchId },
  })

  Object.keys(WAREHOUSES).forEach((key) => {
    const { warehouseId } = WAREHOUSES[key]
    response[warehouseId] = 0
  })

  inventoryItems.forEach(({ warehouseId = WAREHOUSES.OAK.warehouseId, counted = 0 }) => {
    response[warehouseId] += counted
  })

  return response
}
