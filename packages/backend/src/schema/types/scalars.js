import { GraphQLUpload } from 'graphql-upload'

const parseDate = require('../../utils/parseDate').parseDate

function registerScalarTypes(registry) {
  registry.createScalar({
    spec: 'scalar AnyObject',
    description: `An arbitrary object. AnyObject's are not type checked
                  and are always returned in their entirety.`,
    serialize: (value) => value,
    parseValue: (value) => value,
    parseLiteral: (ast) => ast.value,
  })
  registry.createScalar({
    spec: 'scalar JSON',
    description: `An arbitrary object. JSON objects are not type checked
                  and are always returned in their entirety.`,
    serialize: (value) => value,
    parseValue: (value) => value,
    parseLiteral: (ast) => ast.value,
  })
  registry.createScalar({
    spec: 'scalar Secret',
    description: `
      The 'Secret' scalar type represents textual data, represented as UTF-8 that should not
      be readable. The Secret type is often used to hide sensitive information such as passwords
      and credentials.
    `,
  })
  registry.createScalar({
    spec: 'scalar DateTime',
    description: `The \`DateTime\` type represents a single point in time and is stored with
                  millisecond precision plus a time zone specified by ISO 8601. All types come
                  preloaded with createdAt and modifiedAt timestamps that are automatically managed
                  for you.`,
    parseValue: (value) => parseDate(value),
  })
  registry.addType(GraphQLUpload)
}

export { registerScalarTypes }
