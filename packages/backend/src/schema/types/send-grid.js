// import logger from '../../logging';

const registerSendGridTypes = (registry) => {
  // Object Types

  registry.createType(`
    type SendSendGridStatusUpdateEmailPayload {
      success: Boolean!
    }
  `)

  // Input Types

  registry.createInputType({
    spec: `
      input SendSendGridStatusUpdateInput {
        orderId: ID!
      }
    `,
  })
}

export { registerSendGridTypes }
