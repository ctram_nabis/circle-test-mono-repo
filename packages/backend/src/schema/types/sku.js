import { Op } from 'sequelize'
import { resolve } from '../graphql-helpers'
import { getInventoryValues } from './utils/getInventoryValues'

const registerSkuTypes = (registry) => {
  // Enums
  registry.createEnum({
    spec: `
      enum BatchStatusEnum {
        QUARANTINE
        SAMPLED
        PASSED
        FAILED
      }
    `,
  })

  registry.createEnum({
    spec: `
      enum UnitOfMeasureEnum {
        EACH
        MILLIGRAM
        GRAM
        KILOGRAM
        OUNCE
        POUND
        METRIC_TONS
      }
    `,
  })

  registry.createEnum({
    spec: `
      enum InventoryTypeEnum {
        CANNABIS
        NON_CANNABIS
      }
    `,
  })

  registry.createEnum({
    spec: `
      enum InventoryClassEnum {
        FLOWER
        EDIBLES
        PRE_ROLL
        CONCENTRATE
        CARTRIDGE
        DISPOSABLE
        TOPICAL
        OTHER
      }
    `,
  })

  registry.createEnum({
    spec: `
      enum InventoryCategoryEnum {
        BUD
        SHAKE
        INDOOR
        TRIM
        GREENHOUSE
        SUN_GROWN
        COOKIE
        BROWNIE
        GUMMY
        MARSHMALLOW
        TAFFY
        MINT
        HARD_CANDY
        BUTTER
        HONEY
        SODA
        JUICE
        TEA
        WATER
        CHIPS
        ICE_CREAM
        TINCTURES
        SUBLINGUAL
        PRE_ROLLS
        LEAFLETTES
        INFUSED_PRE_ROLLS
        BUTANE_HASH_OIL
        CO2_OIL
        CONCENTRATE
        SOLVENTLESS
        PAX
        THREAD_510
        OTHER_BATTERY_TYPES
        PEN
        TRANSDERMAL
        PATCH
        LIP_BALM
        CREAM
        SPRAY
        INHALER
        DISSOLVABLE_POWDER
        MIXED
        OTHER
      }
    `,
  })

  // Object Types

  registry.createType(
    `
    type Sku {
      id: ID!
      code: String!
      name: String!
      unit: String!
      pricePerUnit: Float
      isArchived: Boolean
      isExciseTaxable: Boolean
      organization: Organization!
      inventoryType: InventoryTypeEnum!
      inventoryClass: InventoryClassEnum!
      inventoryCategory: InventoryCategoryEnum!
      inventoryItems: [InventoryItem]
      totalCount: Float!
      totalCountLA: Float!
      totalCountOak: Float!
      lastUpdate: DateTime
      batches: [Batch]
      skuBatches: [SkuBatch]
    }
  `,
    {
      skuBatches: resolve()((obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.SkuBatch.findAll({
          where: { skuId: obj.id },
        })
      }),
      organization: resolve()((obj, _args, context) =>
        context.connections.logisticsDB.Organization.findByPk(obj.organizationId),
      ),
      batches: resolve()((obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.SkuBatch.findAll({
          where: { skuId: obj.id },
          include: [
            {
              as: 'batch',
              model: db.Batch,
            },
          ],
        }).then((res) => {
          const bs = res.map((r) => r.batch)
          return bs
        })
      }),
      inventoryItems: resolve()((obj, _args, context) =>
        context.connections.logisticsDB.InventoryItem.findAll({
          where: {
            skuId: obj.id,
          },
          order: [['createdAt', 'ASC']],
        }),
      ),
      totalCount: resolve()((obj, _args, context) =>
        context.connections.logisticsDB.InventoryItem.findAll({
          where: {
            skuId: obj.id,
          },
        }).then((results) => results.reduce((a, i) => a + parseInt(i.counted, 10), 0)),
      ),
      totalCountLA: resolve()((obj, _args, context) =>
        context.connections.logisticsDB.InventoryItem.findAll({
          where: {
            skuId: obj.id,
            '$warehouse.site.name$': { [Op.iLike]: '%zasp%' },
          },
          include: [
            {
              model: context.connections.logisticsDB.Warehouse,
              as: 'warehouse',
              include: [
                {
                  model: context.connections.logisticsDB.Site,
                  as: 'site',
                },
              ],
            },
          ],
        }).then((results) => results.reduce((a, i) => a + parseInt(i.counted, 10), 0)),
      ),
      totalCountOak: resolve()((obj, _args, context) =>
        context.connections.logisticsDB.InventoryItem.findAll({
          where: {
            skuId: obj.id,
            '$warehouse.site.name$': { [Op.iLike]: '%nab%' },
          },
          include: [
            {
              model: context.connections.logisticsDB.Warehouse,
              as: 'warehouse',
              include: [
                {
                  model: context.connections.logisticsDB.Site,
                  as: 'site',
                },
              ],
            },
          ],
        }).then((results) => results.reduce((a, i) => a + parseInt(i.counted, 10), 0)),
      ),
      lastUpdate: resolve()((obj, _args, context) =>
        context.connections.logisticsDB.InventoryItem.findAll({
          where: {
            skuId: obj.id,
            '$warehouse.site.name$': { [Op.iLike]: '%nab%' },
          },
          include: [
            {
              model: context.connections.logisticsDB.Warehouse,
              as: 'warehouse',
              include: [
                {
                  model: context.connections.logisticsDB.Site,
                  as: 'site',
                },
              ],
            },
          ],
        }).then((results) =>
          Math.max(
            parseInt(
              results.map((ii) => Date.parse(ii.updatedAt)),
              10,
            ),
          ),
        ),
      ),
    },
  )

  registry.createType(
    `
    type Batch {
      id: ID!
      code: String!
      testDate: DateTime
      deletedAt: DateTime
      status: BatchStatusEnum
      expirationDate: DateTime!
      manufacturingDate: DateTime!
      manifestGDriveFileId: String
      organization: Organization!
      license: License
      skus: [Sku!]!
      skuBatches: [SkuBatch!]!
      createdAt: DateTime!
    }
  `,
    {
      license: resolve()((obj, _args, context) => {
        return context.connections.logisticsDB.License.findByPk(obj.licenseId)
      }),
      organization: resolve()((obj, _args, context) => {
        return context.connections.logisticsDB.Organization.findByPk(obj.organizationId)
      }),
      skuBatches: resolve()((obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.SkuBatch.findAll({
          where: { batchId: obj.id },
        })
      }),
      skus: resolve()((obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.SkuBatch.findAll({
          include: [
            {
              as: 'sku',
              model: db.Sku,
            },
          ],
          where: { batchId: obj.id },
        }).then((res) => {
          const bs = res.map((r) => r.sku)
          return bs
        })
      }),
    },
  )

  // TODO: will setting these to required break anything?
  registry.createType(
    `
    type Values {
      allocated: Float!
      available: Float!
      counted: Float!
      incoming: Float!
      outgoing: Float!
    }
  `,
  )

  // TODO: will setting these to required break anything?
  registry.createType(
    `
    type InventoryValues {
      LA: Values!
      OAK: Values!
    }
  `,
  )

  registry.createType(
    `
    type SkuBatch {
      id: ID!
      sku: Sku!
      batch: Batch
      createdAt: DateTime!
      updatedAt: DateTime!
      deletedAt: DateTime
      inventoryValues: InventoryValues!
      inventoryItems: [InventoryItem]
      titleholder: Titleholder!
    }
  `,
    {
      titleholder: resolve()(async (obj, _args, context) => {
        const { titleholderId } = obj

        const db = context.connections.logisticsDB

        const titleholder = await db.Titleholder.findByPk(titleholderId)

        return titleholder
      }),
      batch: resolve()((obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.Batch.findByPk(obj.batchId)
      }),
      inventoryItems: resolve()(async (obj, _args, context) => {
        const db = context.connections.logisticsDB
        const items = await db.InventoryItem.findAll({
          where: { skuBatchId: obj.id },
        })
        return items
      }),
      inventoryValues: async (obj, _args, context) => {
        return getInventoryValues(obj, _args, context)
      },
      sku: resolve()((obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.Sku.findByPk(obj.skuId, {
          paranoid: false,
        })
      }),
    },
  )

  registry.createType(`
    type CreateSkuPayload {
      changedSku: Sku!
    }
  `)

  registry.createType(`
    type UpdateSkuPayload {
      changedSku: Sku!
    }
  `)

  registry.createType(`
    type DeleteSkuPayload {
      changedSku: Sku!
    }
  `)

  registry.createType(`
    type BatchInfo {
      results: [Batch!]!
      pageInfo: PageInfo!
    }
  `)

  registry.createType(`
    type SkuInfo {
      results: [Sku!]!
      pageInfo: PageInfo!
    }
  `)

  registry.createType(`
    type SkuBatchInfo {
      results: [SkuBatch!]!
      pageInfo: PageInfo!
    }
  `)

  // Input Types
  registry.createInputType({
    spec: `
      input FullSkuInput {
        id: ID!
        code: String!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input CreateSkuInput {
        name: String!
        code: String!
        isExciseTaxable: Boolean = true
        inventoryType: InventoryTypeEnum!
        inventoryClass: InventoryClassEnum!
        inventoryCategory: InventoryCategoryEnum!
        organizationId: ID!
        isSample: Boolean = false
        pricePerUnit: Float = 0
        unit: String!
        isArchived: Boolean
      }
    `,
  })

  registry.createInputType({
    spec: `
      input SkuForCreateSkuBatchInput {
        id: ID
        name: String!
        code: String!
        isExciseTaxable: Boolean
        inventoryType: InventoryTypeEnum!
        inventoryClass: InventoryClassEnum!
        inventoryCategory: InventoryCategoryEnum!
        organizationId: ID!
        isSample: Boolean
        pricePerUnit: Float
        unit: String
        isArchived: Boolean
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateSkuInput {
        id: ID!
        name: String
        code: String
        isExciseTaxable: Boolean
        inventoryType: InventoryTypeEnum
        inventoryClass: InventoryClassEnum
        inventoryCategory: InventoryCategoryEnum
        organizationId: ID
        isSample: Boolean
        pricePerUnit: Float
        unit: String
        batchId: ID
        isArchived: Boolean
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteSkuInput {
        id: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input CreateBatchInput {
        code: String!
        manifestGDriveFileId: String
        manufacturingDate: DateTime
        expirationDate: DateTime
        organizationId: ID!
        licenseId: ID
        status: BatchStatusEnum
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateBatchInput {
        id: ID!
        code: String
        manifestGDriveFileId: String
        manufacturingDate: DateTime
        expirationDate: DateTime
        licenseId: ID
        skuBatchId: ID
        status: BatchStatusEnum
      }
    `,
  })
}

export { registerSkuTypes }
