const registerPaginationTypes = (registry) => {
  // Enums

  registry.createEnum({
    spec: `
      enum OrderByEnum {
        ASC
        DESC
      }
    `,
  })

  // Object Types

  registry.createType(`
    type PageInfo {
      page: Int
      numItemsPerPage: Int
      orderBy: [OrderBy!]!
      totalNumItems: Int
      totalNumPages: Int
    }
  `)

  registry.createType(`
    type OrderBy {
      attribute: String
      order: OrderByEnum
    }
  `)

  // Input Types

  registry.createInputType({
    descriptions: {
      numItemsPerPage: 'Limit',
      page: 'Offset. Starts with 1',
    },
    spec: `
      input PageInfoInput {
        page: Int! = 0
        numItemsPerPage: Int! = 50
        orderBy: [OrderByInput!]
      }
    `,
  })

  registry.createInputType({
    spec: `
      input OrderByInput {
        attribute: String!
        order: OrderByEnum!
      }
    `,
  })
}

export { registerPaginationTypes }
