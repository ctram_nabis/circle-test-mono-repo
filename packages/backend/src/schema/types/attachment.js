const registerAttachmentTypes = (registry) => {
  registry.createType(`
    type Attachment {
      id: ID!
      name: String!
      gDriveFileId: String!
    }
  `)
  registry.createType(`
    type AllAttachmentsPayload {
      results: [Attachment]
      pageInfo: PageInfo
    }
  `)
  registry.createType(`
    type AttachmentPayload {
      changedAttachment: Attachment!
    }
  `)
  registry.createInputType({
    spec: `
      input UpdateAttachmentInput {
        id: ID!
        name: String
        gDriveFileId: String
      }
    `,
  })
  registry.createInputType({
    spec: `
      input FullAttachmentInput {
        id: ID
        name: String
        gDriveFileId: String
      }
    `,
  })
}

export { registerAttachmentTypes }
