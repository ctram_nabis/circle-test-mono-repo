import { resolve } from '../graphql-helpers'

const registerSalesTypes = (registry) => {
  // Object Types

  registry.createType(
    `
    type Sales {
      gmv: Float!
      exciseTax: Float!
      day: DateTime!
      month: DateTime!
      year: DateTime!
      organization: Organization!
    }
  `,
    {
      organization: resolve()((obj, _args, context, _info) => {
        return context.connections.logisticsDB.Organization.findByPk(obj.organizationId)
      }),
    },
  )
}

export { registerSalesTypes }
