const registerDriverTypes = (registry) => {
  registry.createType(`
    type Driver {
      id: ID!
      firstName: String!
      lastName: String
      driversLicense: String!
    }
  `)

  registry.createType(`
    type CreateDriverPayload {
      changedDriver: Driver!
    }
  `)

  registry.createType(`
    type UpdateDriverPayload {
      changedDriver: Driver!
    }
  `)

  registry.createType(`
    type DeleteDriverPayload {
      changedDriver: Driver!
    }
  `)

  registry.createInputType({
    spec: `
      input CreateDriverInput {
        firstName: String!
        lastName: String!
        driversLicense: String!
    }`,
  })

  registry.createInputType({
    spec: ` input UpdateDriverInput {
        id: ID!
        firstName: String!
        lastName: String!
        driversLicense: String!
    }`,
  })

  registry.createInputType({
    spec: ` input DeleteDriverInput {
        id: ID!
    }`,
  })

  registry.createInputType({
    spec: `input FullDriverInput {
        id: ID!
        firstName: String!
        lastName: String!
        driversLicense: String!
      }`,
  })
}

export { registerDriverTypes }
