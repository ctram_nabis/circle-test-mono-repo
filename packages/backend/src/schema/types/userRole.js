import { resolve } from '../graphql-helpers'

const registerUserRoleTypes = (registry) => {
  // Object Types

  registry.createType(
    `
    type UserRole {
      id: ID!
      organization: Organization
      role: Role
      user: User
    }
  `,
    {
      organization: resolve()(({ organizationId }, _args, ctx) =>
        ctx.connections.logisticsDB.Organization.findByPk(organizationId),
      ),
      user: resolve()(({ userId }, _args, ctx) => ctx.connections.logisticsDB.User.findByPk(userId)),
      role: resolve()(({ roleId }, _args, ctx) => ctx.connections.logisticsDB.Role.findByPk(roleId)),
    },
  )

  registry.createType(`
    type CreateUserRolePayload {
      changedUserRole: UserRole!
    }
  `)

  registry.createType(`
    type UpdateUserRolePayload {
      changedUserRole: UserRole!
    }
  `)

  registry.createType(`
    type DeleteUserRolePayload {
      changedUserRole: UserRole!
    }
  `)

  // Input Types

  registry.createInputType({
    spec: `
      input CreateUserRoleInput {
        userId: ID!
        roleId: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateUserRoleInput {
        id: ID!
        userId: ID!
        roleId: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteUserRoleInput {
        userId: ID!
        roleId: ID!
      }
    `,
  })
}

export { registerUserRoleTypes }
