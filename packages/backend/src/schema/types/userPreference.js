import { resolve } from '../graphql-helpers'

const registerUserPreferenceTypes = (registry) => {
  // Object Types

  registry.createType(
    `
    type UserPreference {
      id: ID!
      organization: Organization!
      user: User!
      receiveOrderUpdateEmails: Boolean
    }
  `,
    {
      organization: resolve()(async ({ id }, _args, { connections: { logisticsDB: db } }) => {
        const userPreference = await db.UserPreference.findOne({
          where: {
            id,
          },
        })

        const organization = await db.Organization.findOne({
          where: {
            id: userPreference.organizationId,
          },
        })

        return organization
      }),
      user: resolve()(async ({ id }, _args, { connections: { logisticsDB: db } }) => {
        const userPreference = await db.UserPreference.findOne({
          where: {
            id,
          },
        })

        const user = await db.User.findOne({
          where: {
            id: userPreference.userId,
          },
        })

        return user
      }),
    },
  )
}

export { registerUserPreferenceTypes }
