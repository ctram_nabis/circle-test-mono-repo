import { Registry } from '../graphql-helpers'

const registerVehicleTypes = (registry) => {
  // Object Types

  registry.createType(`
    type Vehicle {
      id: ID!
      name: String
      make: String
      model: String
      licensePlate: String
      year: Int
    }
  `)

  registry.createType(`
    type CreateVehiclePayload {
      changedVehicle: Vehicle!
    }
  `)

  registry.createType(`
    type UpdateVehiclePayload {
      changedVehicle: Vehicle!
    }
  `)

  registry.createType(`
    type DeleteVehiclePayload {
      changedVehicle: Vehicle!
    }
  `)

  // Input Types

  registry.createInputType({
    spec: `
      input CreateVehicleInput {
        name: String
        make: String
        model: String
        licensePlate: String
        year: Int
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateVehicleInput {
        id: ID!
        name: String
        make: String
        model: String
        licensePlate: String
        year: Int
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteVehicleInput {
        id: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input FullVehicleInput {
        id: ID
        name: String
        make: String
        model: String
        licensePlate: String
        year: Int
      }
    `,
  })
}

export { registerVehicleTypes }
