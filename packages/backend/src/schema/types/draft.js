import { resolve } from '../graphql-helpers'

const registerDraftTypes = (registry) => {
  registry.createType(
    `
    type Draft {
      id: ID!
      creator: User!
      body: JSON
      organization: Organization!
      createdAt: DateTime!
      updatedAt: DateTime!
    }
  `,
    {
      creator: resolve()((obj, _args, context) => context.connections.logisticsDB.User.findByPk(obj.creatorId)),
      organization: resolve()((obj, _args, context) =>
        context.connections.logisticsDB.Organization.findByPk(obj.organizationId),
      ),
    },
  )

  registry.createType(`
    type AllDraftsPayload {
      results: [Draft!]
      pageInfo: PageInfo
    }
  `)

  registry.createType(`
    type CreateDraftPayload {
      createdDraft: Draft!
    }
  `)

  registry.createType(`
    type UpdateDraftPayload {
      changedDraft: Draft!
    }
  `)

  registry.createType(`
    type DeleteDraftPayload {
      deletedDraft: Draft!
    }
  `)

  registry.createInputType({
    spec: `
      input CreateDraftInput {
        organizationId: ID!
        body: JSON!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateDraftInput {
        id: ID!
        body: JSON!
        organizationId: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteDraftInput {
        id: ID!
      }
    `,
  })
}

export { registerDraftTypes }
