import { fromGlobalId } from 'graphql-relay'
import idx from 'idx'
import { NABIS_ORGANIZATIONS } from '../../constants'
import { resolve } from '../graphql-helpers'

const registerOrderTypes = (registry) => {
  // Enums

  registry.createEnum({
    spec: `
      enum OrderStatusEnum {
        DRAFT
        UNSCHEDULED
        SCHEDULED
        TRANSFERRING
        DELIVERED
        DELIVERED_WITH_EDITS
        DELAYED
        CANCELLED
        REJECTED
      }
    `,
  })

  registry.createEnum({
    spec: `
      enum OrderActionEnum {
        DELIVERY_TO_RETAILER
        RETURN_FROM_RETAILER
        PICKUP_FROM_BRAND
        DELIVERY_TO_BRAND
        DROPOFF_TO_NABIS
        PICKUP_FROM_NABIS
        PAYMENT_PICKUP_FROM_RETAILER
        INTERNAL_TRANSFER
        OTHER
      }
    `,
  })

  registry.createEnum({
    spec: `
      enum OrderPaymentStatusEnum {
        UNPAID
        COD_PAID
        NET_TERMS_PAID
        REMITTED
        SELF_COLLECTED
      }
    `,
  })

  // Object Types

  registry.createType(
    `
    type Order {
      id: ID!
      assignee: User
      action: OrderActionEnum!
      adminNotes: String
      createdAt: DateTime!
      creator: User
      collectWithOrder: Order
      date: DateTime!
      daysTillPaymentDue: Int
      discount: Float!
      driver: Driver
      edited: Boolean
      exciseTax: Float!
      exciseTaxCollected: Float
      estimatedArrivalTimeBefore: DateTime
      estimatedArrivalTimeAfter: DateTime
      gmv: Float!
      gmvCollected: Float!
      intaken: Boolean!
      internalNotes: String
      inventoryLedgerItems: [InventoryLedgerItem!]!
      irn: String
      isArchived: Boolean!
      licensedLocation: LicensedLocation!
      lineItems: [LineItem!]!
      manifestGDriveFileId: String
      apSummaryGDriveFileId: String
      invoicesS3FileLink: String
      name: String!
      notes: String
      number: Int!
      orderSubscribers: [OrderSubscriber!]!
      organization: Organization!
      orgLicenseNum: String
      outtaken: Boolean!
      paymentStatus: OrderPaymentStatusEnum!
      site: Site!
      siteLicenseNum: String
      status: OrderStatusEnum!
      timeWindow: String
      vehicle: Vehicle
      warehouse: Warehouse
      warehouseId: ID!
      surcharge: Float!
      hasUnalterablePaymentTerms: Boolean
      mustPayPreviousBalance: Boolean
    }
  `,
    {
      driver: resolve()(({ driverId }, _args, context) =>
        context.connections.logisticsDB.Driver.findOne({
          where: { id: driverId },
        }),
      ),
      organization: resolve()(({ organizationId }, _args, context) =>
        context.connections.logisticsDB.Organization.findOne({
          where: { id: organizationId },
        }),
      ),
      vehicle: resolve()(({ vehicleId }, _args, context) =>
        context.connections.logisticsDB.Vehicle.findOne({
          where: { id: vehicleId },
        }),
      ),
      site: resolve()(({ siteId }, _args, context) =>
        context.connections.logisticsDB.Site.findOne({
          where: { id: siteId },
        }),
      ),
      lineItems: resolve()(async ({ id }, _args, { connections, user }) => {
        const { LineItem, Order, SkuBatch } = connections.logisticsDB
        const { isAdmin, organizations } = user
        const { DELIVERIES, SALES } = NABIS_ORGANIZATIONS
        const isNabisUser = organizations.includes(DELIVERIES) || organizations.includes(SALES)
        if (isAdmin || isNabisUser) {
          return LineItem.findAll({
            order: [['createdAt', 'ASC']],
            where: {
              orderId: id,
            },
          })
        } else {
          const orderWithAllLineItems = await Order.findByPk(id, {
            include: [
              {
                association: Order.LineItems,
                include: [
                  {
                    association: LineItem.SkuBatch,
                    include: [{ association: SkuBatch.Sku }, { association: SkuBatch.Batch }],
                  },
                ],
              },
            ],
          })

          const { lineItems: unfilteredLineItems } = orderWithAllLineItems
          const userOrganizationIds = organizations.map(({ id: organizationId }) => organizationId)
          const accessibleLineItems = unfilteredLineItems.filter(({ skuBatch }) =>
            userOrganizationIds.includes(idx(skuBatch, (_) => _.sku.organizationId)),
          )

          return accessibleLineItems
        }
      }),
      warehouse: resolve()(({ warehouseId }, _args, context) =>
        context.connections.logisticsDB.Warehouse.findOne({
          where: { id: warehouseId },
        }),
      ),
      inventoryLedgerItems: resolve()(({ id }, args, context) => {
        const where = { orderId: id }
        if (args.transactionType) {
          where.transactionType = args.transactionType
        }
        return context.connections.logisticsDB.InventoryLedgerItem.findAll({
          where,
          order: [['createdAt', 'ASC']],
        })
      }),
      creator: resolve()(({ creatorId }, _args, context) =>
        context.connections.logisticsDB.User.findOne({
          where: { id: creatorId },
        }),
      ),
      orderSubscribers: resolve()((obj, _args, context, info) => {
        let organization

        if (info.variableValues.organizationId) organization = fromGlobalId(info.variableValues.organizationId)

        if (!idx(organization, (_) => _.id)) return []

        return context.connections.logisticsDB.OrderSubscriber.findAll({
          where: {
            orderId: obj.id,
            subscriberId: organization.id,
          },
        })
      }),
      collectWithOrder: resolve()(({ collectWithOrderId }, _args, context) =>
        context.connections.logisticsDB.Order.findOne({
          where: { id: collectWithOrderId },
        }),
      ),

      assignee: resolve()(({ assigneeId }, _args, context) =>
        context.connections.logisticsDB.User.findOne({
          where: { id: assigneeId },
        }),
      ),
    },
  )

  registry.createType(`
    type AllOrdersPayload {
      results: [Order!]
      pageInfo: PageInfo
    }
  `)

  registry.createType(`
    type CreateOrderPayload {
      changedOrder: Order!
    }
  `)

  registry.createType(`
    type UpdateOrderPayload {
      changedOrder: Order!
    }
  `)

  registry.createType(`
    type DeleteOrderPayload {
      changedOrder: Order!
    }
  `)

  registry.createType(`
    type AllAdminOrdersPayload {
      results: [Order!]
      pageInfo: PageInfo
    }
  `)

  registry.createType(`
    type OrderSubscriber {
      id: ID!
      orderId: ID!
      subscriberId: ID!
      manifestGDriveFileId: String
      invoicesS3FileLink: String
      order: Order!
    }
  `)

  registry.createType(
    `
    type OrderChange {
      id: ID
      order: Order!
      createdAt: DateTime!
      submitter: User!
      organization: Organization!
      description: String
      addedLineItems: [LineItemChange!]
      removedLineItems: [LineItemChange!]
    }
  `,
    {
      addedLineItems: resolve()(({ id }, _args, context) =>
        context.connections.logisticsDB.LineItemChange.findAll({
          where: {
            orderChangeId: id,
            type: 'ADDED',
          },
        }),
      ),
      order: resolve()(({ orderId }, _args, context) =>
        context.connections.logisticsDB.Order.findOne({
          where: { id: orderId },
        }),
      ),
      organization: resolve()(({ organizationId }, _args, context) =>
        context.connections.logisticsDB.Organization.findOne({
          where: { id: organizationId },
        }),
      ),
      removedLineItems: resolve()(({ id }, _args, context) =>
        context.connections.logisticsDB.LineItemChange.findAll({
          where: {
            orderChangeId: id,
            type: 'REMOVED',
          },
        }),
      ),
      submitter: resolve()(({ submitterId }, _args, context) =>
        context.connections.logisticsDB.User.findOne({
          where: { id: submitterId },
        }),
      ),
    },
  )

  // Input Types

  registry.createInputType({
    spec: `
      input CreateOrderInput {
        name: String!
        date: DateTime!
        timeWindow: String
        action: OrderActionEnum!
        daysTillPaymentDue: Int
        orgLicenseNum: String
        siteLicenseNum: String
        notes: String
        status: OrderStatusEnum!
        internalNotes: String
        gmv: Float!
        irn: String
        discount: Float
        exciseTax: Float
        gmvCollected: Float
        exciseTaxCollected: Float
        paymentStatus: OrderPaymentStatusEnum!
        adminNotes: String
        lineItems: [CreateLineItemInput!]!
        driverId: ID
        organizationId: ID!
        vehicleId: ID
        siteId: ID!
        warehouseId: ID!
        creatorId: ID
        licensedLocationId: ID!
        assigneeId: ID
        surcharge: Float
        hasUnalterablePaymentTerms: Boolean
        mustPayPreviousBalance: Boolean
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateOrderInput {
        id: ID!
        name: String
        date: DateTime
        estimatedArrivalTimeBefore: DateTime
        estimatedArrivalTimeAfter: DateTime
        timeWindow: String
        action: OrderActionEnum
        daysTillPaymentDue: Int
        orgLicenseNum: String
        siteLicenseNum: String
        notes: String
        status: OrderStatusEnum
        gmv: Float
        irn: String
        internalNotes: String
        discount: Float
        exciseTax: Float
        gmvCollected: Float
        exciseTaxCollected: Float
        paymentStatus: OrderPaymentStatusEnum
        adminNotes: String
        driverId: ID
        organizationId: ID
        vehicleId: ID
        site: UpdateSiteInput
        warehouseId: ID
        lineItems: [UpsertLineItemInput]
        isArchived: Boolean
        assigneeId: ID
        surcharge: Float
        hasUnalterablePaymentTerms: Boolean
        mustPayPreviousBalance: Boolean
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteOrderInput {
        id: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input FullOrderInput {
        id: ID!
        name: String!
        number: Int!
        date: DateTime!
        timeWindow: String
        action: OrderActionEnum!
        orgLicenseNum: String
        siteLicenseNum: String
        notes: String
        status: OrderStatusEnum
        gmv: Float!
        discount: Float!
        exciseTax: Float!
        gmvCollected: Float!
        exciseTaxCollected: Float!
        paymentStatus: OrderPaymentStatusEnum!
        driver: FullDriverInput
        organization: FullOrganizationInput!
        vehicle: FullVehicleInput
        adminNotes: String
        site: FullSiteInput!
        lineItems: [FullLineItemInput]!
        warehouse: WarehouseInput
        surcharge: Float
        hasUnalterablePaymentTerms: Boolean
        mustPayPreviousBalance: Boolean
      }
    `,
  })
}

export { registerOrderTypes }
