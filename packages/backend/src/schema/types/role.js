import { resolve } from '../graphql-helpers'

const registerRoleTypes = (registry) => {
  // Object Types

  registry.createType(
    `
    type Role {
      id: ID!
      name: String!
      description: String
      permissions: [String!]!
      organization: Organization
    }
  `,
    {
      organization: resolve()(({ organizationId }, _args, context) => {
        return context.connections.logisticsDB.Organization.findByPk(organizationId)
      }),
    },
  )

  registry.createType(`
    type CreateRolePayload {
      changedRole: Role!
    }
  `)

  registry.createType(`
    type UpdateRolePayload {
      changedRole: Role!
    }
  `)

  registry.createType(`
    type DeleteRolePayload {
      changedRole: Role!
    }
  `)

  // Input Types

  registry.createInputType({
    spec: `
      input CreateRoleInput {
        name: String!
        permissions: [String!]!
        organizationId: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateRoleInput {
        id: ID!
        organizationId: ID
        name: String
        permissions: [String!]
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteRoleInput {
        id: ID!
      }
    `,
  })
}

export { registerRoleTypes }
