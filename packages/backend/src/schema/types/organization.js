import { resolve } from '../graphql-helpers'

const registerOrganizationTypes = (registry) => {
  // Object Types

  registry.createType(
    `
    type Organization {
      id: ID!
      name: String!
      alias: String!
      address1: String!
      address2: String
      city: String!
      state: String
      zip: String
      phone: String
      doingBusinessAs: String
      manifestGDriveFolderId: String
      owner: User!
      orders: [Order!]!
      members: [User!]!
      licenses: [License!]!
      sites: [Site!]!
      inventoryItems: [InventoryItem!]!
      inventoryLedgerItems: [InventoryLedgerItem!]!
    }
  `,
    {
      owner: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.User.findByPk(obj.ownerId)
      }),
      orders: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.Order.findAll({
          where: {
            organizationId: obj.id,
          },
        })
      }),
      members: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.User.findAll({
          include: [
            {
              model: context.connections.logisticsDB.Organization,
              as: 'organizations',
              where: {
                id: obj.id,
              },
            },
          ],
        })
      }),
      licenses: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.License.findAll({
          where: {
            organizationId: obj.id,
          },
          order: [['createdAt', 'ASC']],
        })
      }),
      sites: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.Site.findAll({
          where: {
            organizationId: obj.id,
          },
          order: [['createdAt', 'ASC']],
        })
      }),
      inventoryItems: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.InventoryItem.findAll({
          where: {
            organizationId: obj.id,
          },
          order: [['createdAt', 'ASC']],
        })
      }),
      inventoryLedgerItems: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.InventoryLedgerItem.findAll({
          where: {
            organizationId: obj.id,
          },
          order: [['createdAt', 'ASC']],
        })
      }),
    },
  )

  registry.createType(`
    type CreateOrganizationPayload {
      changedOrganization: Organization!
    }
  `)

  registry.createType(`
    type UpdateOrganizationPayload {
      changedOrganization: Organization!
    }
  `)

  registry.createType(`
    type DeleteOrganizationPayload {
      changedOrganization: Organization!
    }
  `)

  // Input Types

  registry.createInputType({
    spec: `
      input CreateOrganizationInput {
        name: String!
        alias: String!
        address1: String!
        address2: String
        city: String!
        state: String!
        zip: String!
        phone: String
        doingBusinessAs: String!
        licenses: [CreateLicenseInput!]!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateOrganizationInput {
        id: ID!
        alias: String
        name: String
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        phone: String
        doingBusinessAs: String
        manifestGDriveFolderId: String
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteOrganizationInput {
        id: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input FullOrganizationInput {
        id: ID!
        name: String!
        alias: String
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        phone: String
        owner: FullUserInput!
        doingBusinessAs: String!
      }
    `,
  })
}

export { registerOrganizationTypes }
