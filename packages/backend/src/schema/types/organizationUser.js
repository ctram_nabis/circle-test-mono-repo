import { resolve } from '../graphql-helpers'

const registerOrganizationUserTypes = (registry) => {
  // Object Types

  registry.createType(
    `
    type OrganizationUser {
      id: ID!
      user: User!
      organization: Organization!
    }
  `,
    {
      user: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.User.findByPk(obj.userId)
      }),
      organization: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.Organization.findByPk(obj.organizationId)
      }),
    },
  )

  registry.createType(`
    type CreateOrganizationUserPayload {
      changedOrganizationUser: OrganizationUser!
    }
  `)

  registry.createType(`
    type UpdateOrganizationUserPayload {
      changedOrganizationUser: OrganizationUser!
    }
  `)

  registry.createType(`
    type DeleteOrganizationUserPayload {
      changedOrganizationUser: OrganizationUser!
    }
  `)

  // Input Types

  registry.createInputType({
    spec: `
      input CreateOrganizationUserInput {
        userId: ID!
        organizationId: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateOrganizationUserInput {
        id: ID!
        userId: ID
        organizationId: ID
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteOrganizationUserInput {
        userId: ID!
        organizationId: ID!
      }
    `,
  })
}

export { registerOrganizationUserTypes }
