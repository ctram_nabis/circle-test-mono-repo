import { resolve } from '../graphql-helpers'

const registerInventoryLedgerItemTypes = (registry) => {
  registry.createEnum({
    spec: `
      enum InventoryTransactionType {
        INTAKE
        OUTTAKE
        CORRECTION
        ALLOCATION
      }
    `,
  })

  registry.createType(
    `
    type InventoryLedgerItem {
      id: ID!
      transactionType: InventoryTransactionType!
      startCounted: Float!
      endCounted: Float!
      notes: String
      createdAt: DateTime
      updatedAt: DateTime
      inventoryItem: InventoryItem!
      order: Order
      lineItem: LineItem
      creator: User!
      organization: Organization!
    }
  `,
    {
      creator: resolve()((obj, _args, context, _info) => {
        return context.connections.logisticsDB.User.findOne({
          where: {
            id: obj.creatorId,
          },
        })
      }),
      inventoryItem: resolve()((obj, _args, context, _info) => {
        return context.connections.logisticsDB.InventoryItem.findOne({
          where: {
            id: obj.inventoryItemId,
          },
        })
      }),
      lineItem: resolve()((obj, _args, context, _info) => {
        return context.connections.logisticsDB.LineItem.findByPk(obj.lineItemId)
      }),
      order: resolve()((obj, _args, context, _info) => {
        return context.connections.logisticsDB.Order.findOne({
          where: {
            id: obj.orderId,
          },
        })
      }),
      organization: resolve()((obj, _args, context, _info) => {
        return context.connections.logisticsDB.Organization.findOne({
          where: {
            id: obj.organizationId,
          },
        })
      }),
    },
  )

  registry.createType(`
    type CreateInventoryLedgerItemPayload {
      changedInventoryItem: InventoryLedgerItem!
    }
  `)

  registry.createType(`
    type BulkCreateInventoryLedgerItemPayload {
      changedInventoryItems: [InventoryLedgerItem!]!
    }
  `)

  registry.createType(`
    type UpdateInventoryLedgerItemPayload {
      changedInventoryItem: InventoryLedgerItem!
    }
  `)

  registry.createType(`
    type InventoryLedgerItemGroup {
      id: ID!
      group: [InventoryLedgerItem!]!
    }
  `)

  registry.createType(`
    type InventoryItemInfo {
      results: [InventoryLedgerItem!]!
      pageInfo: PageInfo!
    }
  `)

  registry.createInputType({
    spec: `
      input CreateInventoryLedgerItemInput {
        transactionType: InventoryTransactionType!
        startCounted: Float!
        endCounted: Float!
        startAllocated: Float!
        endAllocated: Float!
        startPromised: Float!
        endPromised: Float!
        notes: String
        inventoryItemId: ID!
        orderId: ID
        lineItemId: ID
        creatorId: ID!
        organizationId: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      type LineItemIntakeOuttakeRow {
        lineItemId: ID!
        quantity: Float!
        skuBatchId: ID!
        organizationId: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input LineItemIntakeOuttakeInput {
        lineItems: [LineItemIntakeOuttakeRow!]!
        transactionType: String!
        warehouseId: ID!
        orderId: ID!
      }
    `,
  })
}

export { registerInventoryLedgerItemTypes }
