import { resolve } from '../graphql-helpers'

import { NABIS_USERS_ID_TO_KEY } from '../../constants'

const registerUserTypes = (registry) => {
  // Object Types

  registry.createType(
    `
    type User {
      id: ID!
      email: String
      firstName: String
      lastName: String
      address1: String
      address2: String
      city: String
      state: String
      zip: String
      phone: String
      profilePicture: String
      isAdmin: Boolean
      organizations: [Organization!]!
      organizationInvites: [OrganizationInvite!]!
      assigned: [Order!]!
      isArchived: Boolean
      userRoles: [UserRole!]!
      roles: [Role!]!
      drafts: [Draft!]!
      userPreferences: [UserPreference!]
    }
  `,
    {
      assigned: resolve()(({ id }, _args, ctx) =>
        ctx.connections.logisticsDB.Order.findAll({
          where: {
            assigneeId: id,
          },
        }),
      ),
      drafts: resolve()(async ({ id }, _args, ctx) =>
        ctx.connections.logisticsDB.Draft.findAll({
          where: {
            body: { organization: null },
            creatorId: id,
            organizationId: null,
          },
        }),
      ),
      organizationInvites: resolve()(({ email }, _args, ctx) =>
        ctx.connections.logisticsDB.OrganizationInvite.findAll({
          where: {
            email,
          },
        }),
      ),
      organizations: resolve()(({ id }, _args, ctx) =>
        ctx.connections.logisticsDB.Organization.findAll({
          include: [
            {
              as: 'users',
              model: ctx.connections.logisticsDB.User,
              where: {
                id: id,
              },
            },
          ],
        }),
      ),

      roles: resolve()(async ({ id }, _args, ctx) => {
        const user = await ctx.connections.logisticsDB.User.findByPk(id, {
          include: [{ association: 'roles' }],
        })

        return user.roles
      }),
      userPreferences: resolve()(async ({ id }, _args, ctx) => {
        const { connections, user } = ctx
        if (NABIS_USERS_ID_TO_KEY[user.id] === 'DELIVERIES') {
          return null
        }
        return connections.logisticsDB.UserPreference.findAll({
          where: {
            userId: id,
          },
        })
      }),
      userRoles: resolve()(({ id }, _args, ctx) =>
        ctx.connections.logisticsDB.UserRole.findAll({ where: { userId: id } }),
      ),
    },
  )

  registry.createType(`
    type LoginUserPayload {
      token: String!
      user: User!
    }
  `)

  registry.createType(`
    type CreateUserPayload {
      token: String!
      changedUser: User!
    }
  `)

  registry.createType(`
    type UpdateUserPayload {
      changedUser: User!
    }
  `)

  registry.createType(`
    type DeleteUserPayload {
      changedUser: User!
    }
  `)

  // Input Types

  registry.createInputType({
    spec: `
      input LoginUserInput {
        email: String!
        password: String!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input CreateUserInput {
        email: String!
        password: String!
        firstName: String
        lastName: String
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        phone: String
        profilePicture: String
        invitationId: String
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateUserInput {
        id: ID!
        email: String
        password: String
        firstName: String
        lastName: String
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        phone: String
        profilePicture: String
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteUserInput {
        id: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input ResetPasswordInput {
        email: String!
        token: String!
        confirmPassword: String!
        newPassword: String!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input FullUserInput {
        id: ID!
        email: String!
        firstName: String
        lastName: String
        address1: String
        address2: String
        city: String
        state: String
        zip: String
        phone: String
        profilePicture: String
        isAdmin: Boolean
      }
    `,
  })
}

export { registerUserTypes }
