import { resolve } from '../graphql-helpers'

const registerOrganizationInviteTypes = (registry) => {
  // Object Types

  registry.createType(
    `
    type OrganizationInvite {
      id: ID!
      email: String!
      organization: Organization!
      sender: User!
      accepted: Boolean
      respondedAt: DateTime
    }
  `,
    {
      organization: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.Organization.findByPk(obj.organizationId)
      }),
      sender: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.User.findByPk(obj.senderId)
      }),
    },
  )

  registry.createType(`
    type CreateOrganizationInvitePayload {
      changedOrganizationInvite: OrganizationInvite!
    }
  `)

  registry.createType(`
    type UpdateOrganizationInvitePayload {
      changedOrganizationInvite: OrganizationInvite!
    }
  `)

  registry.createType(`
    type DeleteOrganizationInvitePayload {
      changedOrganizationInvite: OrganizationInvite!
    }
  `)

  // Input Types

  registry.createInputType({
    spec: `
      input CreateOrganizationInviteInput {
        email: String!
        organizationId: ID!
        senderId: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateOrganizationInviteInput {
        id: ID!
        email: String
        organizationId: ID
        senderId: ID
        accepted: Boolean
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteOrganizationInviteInput {
        id: ID!
      }
    `,
  })
}

export { registerOrganizationInviteTypes }
