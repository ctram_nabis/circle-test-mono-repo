import idx from 'idx'
import { resolve } from '../graphql-helpers'

const registerLineItemTypes = (registry) => {
  registry.createType(
    `
    type LineItem {
      id: ID!
      code: String!
      name: String!
      unit: String!
      quantity: Float!
      pricePerUnit: Float
      order: Order!
      skuBatch: SkuBatch
      organization: Organization
      isExciseTaxable: Boolean!
      isSample: Boolean!
      oversold: Boolean!
      discount: Float!
    }
  `,
    {
      order: resolve()(({ orderId }, _args, ctx) => ctx.connections.logisticsDB.Order.findByPk(orderId)),
      skuBatch: resolve()(({ skuBatchId }, _args, ctx) => {
        const isAdmin = idx(ctx, (_) => _.user.isAdmin)

        return ctx.connections.logisticsDB.SkuBatch.findByPk(
          skuBatchId,
          // Shows archived + unarchived skuBatch's for Nabis Admins only
          isAdmin && { paranoid: false },
        )
      }),
      organization: resolve()(({ organizationId }, _args, ctx) =>
        ctx.connections.logisticsDB.Organization.findByPk(organizationId),
      ),
    },
  )

  registry.createEnum({
    spec: `
      enum LineItemChangeType {
        ADDED
        REMOVED
      }
    `,
  })

  registry.createType(
    `
    type LineItemChange {
      id: ID!
      isExciseTaxable: Boolean!
      isSample: Boolean!
      lineItemId: ID!
      name: String
      orderChange: OrderChange!
      oversold: Boolean
      pricePerUnit: Float
      quantity: Float!
      skuBatch: SkuBatch
      skuBatchId: ID!
      type: LineItemChangeType!
      discount: Float!
    }
  `,
    {
      skuBatch: resolve()(({ skuBatchId }, _args, ctx) => ctx.connections.logisticsDB.SkuBatch.findByPk(skuBatchId)),
    },
  )

  registry.createType(`
    type CreateLineItemPayload {
      changedLineItem: LineItem!
    }
  `)

  registry.createType(`
    type UpdateLineItemPayload {
      changedLineItem: LineItem!
    }
  `)

  registry.createType(`
    type DeleteLineItemPayload {
      changedLineItem: LineItem!
    }
  `)

  registry.createInputType({
    spec: `
      input CreateLineItemInput {
        code: String
        name: String
        unit: String
        quantity: Float! = 0
        pricePerUnit: Float! = 0
        orderId: ID
        skuBatchId: ID!
        organizationId: ID
        isExciseTaxable: Boolean! = true
        isSample: Boolean = false
        oversold: Boolean = false
        discount: Float! = 0
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateLineItemInput {
        id: ID!
        code: String
        name: String
        unit: String
        quantity: Float
        pricePerUnit: Float
        orderId: ID
        skuBatchId: ID
        organizationId: ID
        isExciseTaxable: Boolean
        isSample: Boolean
        oversold: Boolean
        discount: Float
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpsertLineItemInput {
        id: ID
        code: String
        name: String
        unit: String
        quantity: Float
        pricePerUnit: Float
        orderId: ID
        skuBatchId: ID
        organization: UpdateOrganizationInput
        isExciseTaxable: Boolean
        isSample: Boolean
        oversold: Boolean
        discount: Float
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteLineItemInput {
        id: ID!
      }
    `,
  })

  registry.createInputType({
    spec: `
      input FullLineItemInput {
        id: ID!
        code: String!
        name: String!
        unit: String!
        quantity: Float!
        pricePerUnit: Float!
        isExciseTaxable: Boolean!
        isSample: Boolean
        oversold: Boolean
        discount: Float!
      }
    `,
  })
}

export { registerLineItemTypes }
