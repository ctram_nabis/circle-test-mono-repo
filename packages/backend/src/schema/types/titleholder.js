import { resolve } from '../graphql-helpers'

const registerTitleholderTypes = (registry) => {
  // Enum Types
  registry.createEnum({
    spec: `
      enum TitleholderType {
        LICENSE
        ORGANIZATION
      }
    `,
  })

  // Object Types
  registry.createType(
    `
    type Titleholder {
      id: ID!
      organization: Organization
      titleholderLicense: License
      titleholderOrganization: Organization
      type: TitleholderType!
      createdAt: DateTime!
      updatedAt: DateTime!
    }
  `,
    {
      organization: resolve()((obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.Organization.findByPk(obj.organizationId)
      }),
      titleholderLicense: resolve()(async (obj, _args, context) => {
        const { type, titleholderLicenseId } = obj

        if (type === 'ORGANIZATION' || !titleholderLicenseId) return null

        const db = context.connections.logisticsDB

        const license = await db.License.findByPk(titleholderLicenseId)

        return license
      }),
      titleholderOrganization: resolve()(async (obj, _args, context) => {
        const { type, titleholderOrganizationId } = obj

        if (type === 'LICENSE' || !titleholderOrganizationId) return null

        const db = context.connections.logisticsDB

        const organization = await db.Organization.findByPk(titleholderOrganizationId)

        return organization
      }),
    },
  )

  registry.createType(`
    type CreateTitleholderPayload {
      changedTitleholder: Titleholder!
    }
  `)

  registry.createType(`
    type UpdateTitleholderPayload {
      changedTitleholder: Titleholder!
    }
  `)

  registry.createType(`
    type DeleteTitleholderPayload {
      changedTitleholder: Titleholder!
    }
  `)

  registry.createInputType({
    spec: `
      input CreateTitleholderInput {
        organizationId: ID!
        titleholderLicenseId: ID
        titleholderOrganizationId: ID
      }
    `,
  })

  registry.createInputType({
    spec: `
      input UpdateTitleholderInput {
        id: ID!
        titleholderLicenseId: ID
        titleholderOrganizationId: ID
      }
    `,
  })

  registry.createInputType({
    spec: `
      input DeleteTitleholderInput {
        id: ID!
      }
    `,
  })
}

export { registerTitleholderTypes }
