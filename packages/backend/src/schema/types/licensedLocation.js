import { resolve } from '../graphql-helpers'

const registerLicensedLocationTypes = (registry) => {
  registry.createEnum({
    spec: `
      enum VerificationStatusEnum {
        VERFIED
        UNVERIFIED
        REJECTED
      }
    `,
  })

  registry.createType(
    `
    type LicensedLocation {
      id: ID!
      name: String
      address1: String
      address2: String
      city: String
      state: String
      zip: String
      lat: Float
      lng: Float
      siteCategory: SiteCategoryEnum
      licenses: [License]
      warehouse: Warehouse
      verificationStatus: VerificationStatusEnum
      submittedBy: User
      isArchived: Boolean
    }
  `,
    {
      licenses: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.License.findAll({
          where: {
            licensedLocationId: obj.id,
          },
          order: [['createdAt', 'ASC']],
        })
      }),
      warehouse: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.Warehouse.findOne({
          where: {
            id: obj.warehouseId,
          },
        })
      }),
      user: resolve()((obj, args, context, info) => {
        return context.connections.logisticsDB.User.findOne({
          where: {
            id: obj.submittedById,
          },
        })
      }),
    },
  )

  registry.createType(`
    type AllLicensedLocationsPayload {
      results: [LicensedLocation!]
      pageInfo: PageInfo
    }
  `)
}

export { registerLicensedLocationTypes }
