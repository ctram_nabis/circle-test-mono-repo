import logger from '../../logging'
import { resolve } from '../graphql-helpers'

const registerWarehouseTypes = (registry) => {
  registry.createType(
    `
    type Warehouse {
      id: ID!
      license: License!
      site: Site!
      isArchived: Boolean
    }
  `,
    {
      license: resolve()((obj, args, context, info) => {
        // logger.info(`Request ${context.id}`, 'In warehouse.license()')
        return context.connections.logisticsDB.License.findOne({
          where: { id: obj.licenseId },
        })
      }),
      site: resolve()((obj, args, context, info) => {
        // logger.info(`Request ${context.id}`, 'In warehouse.site()')
        return context.connections.logisticsDB.Site.findOne({
          where: { id: obj.siteId },
        })
      }),
    },
  )

  registry.createInputType({
    spec: `
      input WarehouseInput {
        id: ID!
      }
    `,
  })
}

export { registerWarehouseTypes }
