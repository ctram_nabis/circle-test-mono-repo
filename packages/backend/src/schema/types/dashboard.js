const registerDashboardDataTypes = (registry) => {
  registry.createType(`
    type DashboardData {
      totalDelivered: Int!
      totalScheduled: Int!
      totalUnscheduled: Int!
      totalGMVCollected: Float!
      totalExciseTaxCollected: Float!
    }
  `)
}

export { registerDashboardDataTypes }
