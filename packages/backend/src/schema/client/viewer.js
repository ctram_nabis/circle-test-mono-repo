import { resolve } from '../graphql-helpers'
import { authClient } from '../graphql-helpers/contrib/relay'

const registerViewerTypes = (registry) => {
  registry.createType(
    `
    type Viewer {
      me: Organization
      allOrders: [Order]
      allSkus: [Sku]
      allArchivedSkus: [Sku]
    }
  `,
    {
      me: resolve(authClient)((obj, args, context, info) => {
        const { client } = context
        if (client) {
          return context.connections.logisticsDB.Organization.findByPk(client.id)
        }
      }),
      allOrders: resolve(authClient)((obj, args, context, info) => {
        return context.connections.logisticsDB.Order.findAll({
          where: {
            organizationId: context.client.id,
          },
        })
      }),
      allSkus: resolve(authClient)((obj, args, context, info) => {
        return context.connections.logisticsDB.Sku.findAll({
          where: {
            organizationId: context.client.id,
            isArchived: false,
          },
        })
      }),
      allArchivedSkus: resolve(authClient)((obj, args, context, info) => {
        return context.connections.logisticsDB.Sku.findAll({
          where: {
            organizationId: context.client.id,
            isArchived: true,
          },
        })
      }),
    },
  )
}

export { registerViewerTypes }
