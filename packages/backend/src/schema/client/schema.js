import { GraphQLSchema } from 'graphql'
import { Registry } from '../graphql-helpers'
import { processGraphQL } from '../graphql-helpers/contrib/relay'
import {
  registerScalarTypes,
  registerDraftTypes,
  registerOrganizationTypes,
  registerOrganizationUserTypes,
  registerRoleTypes,
  registerUserRoleTypes,
  registerOrderTypes,
  registerVehicleTypes,
  registerLineItemTypes,
  registerSkuTypes,
  registerTitleholderTypes,
  registerSiteTypes,
  registerLicenseTypes,
  registerLicensedLocationTypes,
  registerWarehouseTypes,
  registerOrganizationInviteTypes,
  registerAttachmentTypes,
  registerInventoryItemTypes,
  registerInventoryLedgerItemTypes,
  registerDriverTypes,
  registerPaginationTypes,
  registerUserTypes,
  registerUserPreferenceTypes,
} from '../types'
import { registerQueryType } from './query'
import { registerMutationType } from './mutation'
import { registerViewerTypes } from './viewer'

const registry = new Registry(processGraphQL)
registerScalarTypes(registry)
registerDraftTypes(registry)
registerQueryType(registry)
registerMutationType(registry)
registerViewerTypes(registry)
registerOrganizationTypes(registry)
registerOrganizationUserTypes(registry)
registerRoleTypes(registry)
registerUserRoleTypes(registry)
registerOrderTypes(registry)
registerVehicleTypes(registry)
registerDriverTypes(registry)
registerLineItemTypes(registry)
registerSkuTypes(registry)
registerTitleholderTypes(registry)
registerSiteTypes(registry)
registerLicenseTypes(registry)
registerLicensedLocationTypes(registry)
registerWarehouseTypes(registry)
registerOrganizationInviteTypes(registry)
registerAttachmentTypes(registry)
registerInventoryItemTypes(registry)
registerInventoryLedgerItemTypes(registry)
registerPaginationTypes(registry)
registerUserTypes(registry)
registerUserPreferenceTypes(registry)

const GQLSchema = new GraphQLSchema({
  query: registry.getType('Query'),
  mutation: registry.getType('Mutation'),
})

export default GQLSchema
