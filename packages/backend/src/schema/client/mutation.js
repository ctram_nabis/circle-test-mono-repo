import { resolve } from '../graphql-helpers'
import { authClient, unmask } from '../graphql-helpers/contrib/relay'
import { unpackId } from '../graphql-helpers/contrib/relay/resolvers'
import appendOrg from '../graphql-helpers/resolvers/appendOrg'

const registerMutationType = (registry) => {
  registry.createType(
    `
    type Mutation {
      createOrder(input: CreateOrderInput!) : CreateOrderPayload
      updateOrder(input: UpdateOrderInput!) : UpdateOrderPayload
      deleteOrder(input: DeleteOrderInput!) : DeleteOrderPayload

      createLineItem(input: CreateLineItemInput!) : CreateLineItemPayload
      updateLineItem(input: UpdateLineItemInput!) : UpdateLineItemPayload
      deleteLineItem(input: DeleteLineItemInput!) : DeleteLineItemPayload

      createSku(input: CreateSkuInput!) : CreateSkuPayload
      updateSku(input: UpdateSkuInput!) : UpdateSkuPayload
      deleteSku(input: DeleteSkuInput!) : DeleteSkuPayload
    }
  `,
    {
      createOrder: resolve(
        authClient,
        appendOrg,
        unmask('input.driverId', 'input.vehicleId'),
      )((obj, args, context, info) => {
        const { input } = args
        delete input.driverId
        delete input.vehicleId

        // Create order (w. pricing)
        return context.connections.logisticsDB.Order.create(input).then((changedOrder) => ({
          changedOrder,
        }))
      }),
      updateOrder: resolve(
        authClient,
        appendOrg,
        unmask('input.id', 'input.driverId', 'input.vehicleId'),
      )((obj, args, context, info) => {
        const { id, ...inputs } = args.input

        const db = context.connections.logisticsDB

        const handleSequelizeManyToManyQuirks = () => {
          const unmaskedAttachmentIds = (inputs.attachments || []).map((a) => unpackId(a.id))
          delete inputs.attachments
          return () =>
            db.Order.findByPk(id, {
              include: [{ association: db.Order.Attachments }],
            }).then((o) => {
              o.setAttachments(unmaskedAttachmentIds)
              return o
            })
        }

        const addAttachmentsTheSequelizeWay = handleSequelizeManyToManyQuirks()

        return db.Order.update(inputs, {
          where: { id },
          include: [
            {
              model: db.Order.Attachments,
              association: db.Order.Attachments,
            },
          ],
        })
          .then(addAttachmentsTheSequelizeWay)
          .then((order) => ({
            changedOrder: order,
          }))
      }),
      deleteOrder: resolve(
        authClient,
        unmask('input.id'),
      )((obj, args, context, info) => {
        let data
        return context.connections.logisticsDB.Order.findByPk(args.input.id)
          .then((res) => {
            data = res
            return context.connections.logisticsDB.Order.destroy({
              where: {
                id: args.input.id,
              },
            })
          })
          .then(() => ({
            changedOrder: data,
          }))
      }),

      createLineItem: resolve(
        authClient,
        unmask('input.skuId'),
      )((obj, args, context, info) => {
        return context.connections.logisticsDB.LineItem.create(args.input).then((changedLineItem) => ({
          changedLineItem,
        }))
      }),
      updateLineItem: resolve(
        authClient,
        unmask('input.id', 'input.skuId'),
      )((obj, args, context, info) => {
        const { id, ...inputs } = args.input
        return context.connections.logisticsDB.LineItem.update(inputs, {
          where: {
            id,
          },
        }).then(() => ({
          changedLineItem: context.connections.logisticsDB.LineItem.findByPk(id),
        }))
      }),
      deleteLineItem: resolve(
        authClient,
        unmask('input.id'),
      )((obj, args, context, info) => {
        let data
        return context.connections.logisticsDB.LineItem.findByPk(args.input.id)
          .then((res) => {
            data = res
            return context.connections.logisticsDB.LineItem.destroy({
              where: {
                id: args.input.id,
              },
            })
          })
          .then(() => ({
            changedLineItem: data,
          }))
      }),

      createSku: resolve(
        authClient,
        appendOrg,
      )((obj, args, context, info) => {
        return context.connections.logisticsDB.Sku.create(args.input).then((changedSku) => ({
          changedSku,
        }))
      }),
      updateSku: resolve(
        authClient,
        appendOrg,
        unmask('input.id'),
      )((obj, args, context, info) => {
        const { id, ...inputs } = args.input
        return context.connections.logisticsDB.Sku.update(inputs, {
          where: {
            id,
          },
        }).then(() => ({
          changedSku: context.connections.logisticsDB.Sku.findByPk(id),
        }))
      }),
      deleteSku: resolve(
        authClient,
        unmask('input.id'),
      )((obj, args, context, info) => {
        let data
        return context.connections.logisticsDB.Sku.findByPk(args.input.id)
          .then((res) => {
            data = res
            return context.connections.logisticsDB.Sku.destroy({
              where: {
                id: args.input.id,
              },
            })
          })
          .then(() => ({
            changedSku: data,
          }))
      }),
    },
  )
}

export { registerMutationType }
