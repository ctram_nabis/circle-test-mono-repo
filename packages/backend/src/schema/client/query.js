import { Registry, resolve } from '../graphql-helpers'
import { authClient, unmask } from '../graphql-helpers/contrib/relay'
import logger from '../../logging'

const registerQueryType = (registry) => {
  registry.createType(
    `
    type Query {
      environment: String
      viewer: Viewer
      getOrder(id: ID!): Order
      getLineItem(id: ID!): LineItem
      getSku(id: ID!): Sku
    }
  `,
    {
      environment: resolve()((obj, args, context, info) => process.env.NODE_ENV),
      viewer: resolve()((obj, args, context, info) => ({})),
      getOrder: resolve(
        authClient,
        unmask('id'),
      )((obj, args, context, info) => {
        return context.connections.logisticsDB.Order.findByPk(args.id)
      }),
      getLineItem: resolve(
        authClient,
        unmask('id'),
      )((obj, args, context, info) => {
        return context.connections.logisticsDB.LineItem.findByPk(args.id)
      }),
      getSku: resolve(
        authClient,
        unmask('id'),
      )((obj, args, context, info) => {
        return context.connections.logisticsDB.Sku.findByPk(args.id)
      }),
    },
  )
}

export { registerQueryType }
