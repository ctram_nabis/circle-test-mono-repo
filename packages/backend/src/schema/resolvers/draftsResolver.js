import { unmask, authUser } from '../graphql-helpers/contrib/relay'
import { resolve } from '../graphql-helpers'

export const draftsTypes = `
    createDraft(input: CreateDraftInput!) : CreateDraftPayload
    updateDraft(input: UpdateDraftInput!) : UpdateDraftPayload
    deleteDraft(input: DeleteDraftInput!) : DeleteDraftPayload
  `
export const drafts = {
  createDraft: resolve(
    authUser,
    unmask('input.organizationId'),
  )(async (_obj, args, context) => {
    delete args.input.body.status
    const createdDraft = await context.connections.logisticsDB.Draft.create({
      ...args.input,
      creatorId: context.user.id,
    })
    return { createdDraft }
  }),

  updateDraft: resolve(
    authUser,
    unmask('input.id', 'input.organizationId'),
  )(async (_obj, args, context) => {
    const changedDraft = await context.connections.logisticsDB.Draft.update(args.input, {
      where: { id: args.input.id },
    })
    return { changedDraft }
  }),

  deleteDraft: resolve(
    authUser,
    unmask('input.id'),
  )(async (_obj, args, context) => {
    const deletedDraft = await context.connections.logisticsDB.Draft.destroy({
      where: { id: args.input.id },
    })
    return { deletedDraft }
  }),
}
