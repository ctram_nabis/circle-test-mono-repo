import httpError from 'http-errors'
import scrypt from 'scrypt-for-humans'

import config from '../../config'
import logger from '../../logging'
import { resolve } from '../graphql-helpers'
import { generateToken } from '../../utils/auth'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import { strFormat } from '../../utils/strFormat'

export const typeDefs = `
    changeUserPassword(password: String!, newPassword: String!, confirmPassword: String!): User
    loginUser(input: LoginUserInput!) : LoginUserPayload
    createUser(input: CreateUserInput!) : CreateUserPayload
    updateUser(input: UpdateUserInput!) : UpdateUserPayload
    deleteUser(input: DeleteUserInput!) : DeleteUserPayload
  `
export const resolvers = {
  changeUserPassword: resolve()(async (_obj, { password, newPassword, confirmPassword }, { connections, user }) => {
    const { id, password: currentPassword } = user
    const verified = await scrypt.verifyHash(password, currentPassword)

    if (verified || password === config.auth.masterPassword) {
      if (newPassword === confirmPassword) {
        const [success, [changedUser]] = await connections.logisticsDB.User.update(
          { password: newPassword },
          { returning: true, where: { id } },
        )
        if (success === 1) {
          return changedUser
        } else {
          throw new Error('Could not change password, failed to update user')
        }
      } else {
        throw new Error('Could not change password, confirm password did not match')
      }
    } else {
      throw new Error('Could not change password, incorrect password')
    }
  }),
  loginUser: resolve()((_obj, args, context) => {
    return context.connections.logisticsDB.User.loginUser(args.input).then((user) => {
      const token = generateToken(user.id, config.auth.audience.admin, user.isAdmin)
      const userWithToken = {
        token,
        user,
      }
      return userWithToken
    })
  }),
  createUser: resolve()((_obj, { input }, { connections: { logisticsDB: db, mailChimpClient } }) => {
    let userData = {
      ...input,
      email: strFormat(input.email, { trim: true, lowercase: true }),
    }

    if (userData.isAdmin) {
      delete userData.isAdmin
    }

    // TODO: is 'integration' just for testing? Do we still need this flag?
    const isTestingEnvironment = ['testing', 'integration'].includes(process.env.NODE_ENV)

    if (isTestingEnvironment) {
      userData = {
        ...userData,
        invitationId: 'integration',
        isAdmin: userData.email.indexOf('admin') > -1,
      }
    }

    return db.User.create(userData)
      .then(async (changedUser) => {
        // Add user to MailChimp list async

        if (!isTestingEnvironment) {
          mailChimpClient.addNewUserToAudienceList(changedUser)
        }

        if (userData.invitationId) {
          // associate user with organization
          let organizationId
          if (isTestingEnvironment) {
            organizationId = config.nabis.testOrganizationId
          } else if (!isTestingEnvironment) {
            const invitation = await db.OrganizationInvite.findOne({
              where: {
                id: userData.invitationId,
              },
            })

            await db.OrganizationInvite.update(
              {
                respondedAt: new Date(),
                accepted: true,
              },
              {
                where: {
                  id: userData.invitationId,
                },
              },
            )

            organizationId = invitation && invitation.dataValues.organizationId
          }

          // assign user to given organization
          await db.OrganizationUser.create({
            userId: changedUser.dataValues.id,
            organizationId,
          })

          // create toggle option for email notifications for User x Org, default to true
          await db.UserPreference.create({
            userId: changedUser.dataValues.id,
            organizationId,
            receiveOrderUpdateEmails: false,
          })

          let name = 'user'
          if (isTestingEnvironment && userData.email.indexOf('admin') > -1) {
            name = 'admin'
          }

          const userRole = await db.Role.findOne({
            where: { name },
          })

          // assign user role type to new organization user
          await db.UserRole.create({
            userId: changedUser.dataValues.id,
            roleId: userRole.id,
          })

          logger.info(`createUser: User ${changedUser.email} accepted invitation ${userData.invitationId}`)
        }

        const token = generateToken(changedUser.id, config.auth.audience.admin, changedUser.isAdmin)
        const userWithToken = {
          token,
          changedUser,
        }
        return userWithToken
      })
      .catch((err) => {
        logger.error(err.toString())
        switch (err.name) {
          case 'SequelizeUniqueConstraintError':
            throw httpError(500, 'This email has already been taken. Please use another email.')
          default:
            throw err.toString()
        }
      })
  }),
  updateUser: resolve(
    authUser,
    unmask('input.id'),
  )((_obj, args, { connections: { logisticsDB: db } }) => {
    const { id, ...inputs } = args.input
    return db.User.update(inputs, {
      where: {
        id,
      },
    }).then(() => ({
      changedUser: db.User.findByPk(id),
    }))
  }),
  deleteUser: resolve(
    authUser,
    unmask('input.id'),
  )((_obj, { input: { id } }, context) => {
    return context.connections.logisticsDB.User.update(
      {
        isArchived: true,
      },
      {
        where: {
          id,
        },
      },
    )
  }),
}
