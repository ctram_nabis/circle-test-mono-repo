import { toGlobalId } from 'graphql-relay'
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import { nearestWarehouseId } from '../../utils/warehouseFinder'

export const typeDefs = `
    createLicensedLocation(input: CreateLicensedLocationInput!): ID
    updateLicensedLocation(input: UpdateLicensedLocationInput!): Boolean
    deleteLicensedLocation(input: DeleteLicensedLocationInput!): Boolean
    unarchiveLicensedLocation(input: UnarchiveLicensedLocationInput!): Boolean
  `
export const resolvers = {
  createLicensedLocation: resolve(authUser)(async (_obj, args, context) => {
    console.log('create licensed location', {
      input: args.input,
    })

    const data = args.input
    data.submittedById = context.user.id

    const db = context.connections.logisticsDB
    if (!data.warehouseId) {
      const _nearestWarehouseId = await nearestWarehouseId(args.input.zip, db)

      data.warehouseId = _nearestWarehouseId
    }
    return db.LicensedLocation.create(data, {
      include: [
        {
          association: db.LicensedLocation.Licenses,
        },
      ],
    }).then((ll) => toGlobalId('LicensedLocation', ll.id))
  }),

  updateLicensedLocation: resolve(
    authUser,
    unmask('input.id'),
  )(async (_obj, args, context, _info) => {
    const db = context.connections.logisticsDB
    const oldLicLoc = await db.LicensedLocation.findOne({
      where: { id: args.input.id },
    })

    if (args.input.zip !== oldLicLoc.zip) {
      const _nearestWarehouseId = await nearestWarehouseId(args.input.zip, db)

      args.input.warehouseId = _nearestWarehouseId
    }

    return db.LicensedLocation.update(args.input, {
      where: {
        id: args.input.id,
      },
    })[0]
  }),

  unarchiveLicensedLocation: resolve(
    authUser,
    unmask('input.id'),
  )(async (_obj, { input }, context) => {
    const { LicensedLocation } = context.connections.logisticsDB
    // updating rather than destroying to keep it consistant with the archiving skuBatch method
    const { id } = input || {}
    const [success] = await LicensedLocation.update(
      {
        isArchived: false,
        deletedAt: null,
      },
      {
        returning: true,
        where: {
          id,
        },
      },
    )
    if (success !== 1) {
      throw new Error('UnArchiving licenced location was unsuccessful')
    }

    return true
  }),

  deleteLicensedLocation: resolve(
    authUser,
    unmask('input.id'),
  )(async (_obj, { input }, context) => {
    const { LicensedLocation } = context.connections.logisticsDB
    // updating rather than destroying to keep it consistant with the archiving skuBatch method
    const { id } = input || {}
    const [success] = await LicensedLocation.update(
      {
        isArchived: true,
        deletedAt: new Date(),
      },
      {
        returning: true,
        where: {
          id,
        },
      },
    )
    if (success !== 1) {
      throw new Error('Archive licenced location was unsuccessful')
    }

    return true
  }),
}
