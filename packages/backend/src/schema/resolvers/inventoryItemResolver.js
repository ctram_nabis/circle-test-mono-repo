import { unmask, authUser } from '../graphql-helpers/contrib/relay'
import { resolve } from '../graphql-helpers'
import logger from '../../logging'

export const inventoryItemTypeDefs = `
  createInventoryItem(organizationId: ID!, skuBatchId: ID!, skuId: ID, warehouseId: ID!): InventoryItem
`

export const inventoryItemResolvers = {
  createInventoryItem: resolve(
    authUser,
    unmask('organizationId', 'skuBatchId', 'warehouseId'),
  )(async (_obj, { organizationId, skuBatchId, warehouseId }, context) => {
    const { InventoryItem } = context.connections.logisticsDB
    try {
      const _createdInventoryItem = await InventoryItem.create({
        allocated: 0,
        counted: 0,
        organizationId,
        promised: 0,
        skuBatchId,
        storageType: 'INVENTORY',
        warehouseId,
      })
      return _createdInventoryItem
    } catch (error) {
      logger.error(error)
      throw error
    }
  }),
}
