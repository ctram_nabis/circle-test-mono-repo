import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    createEmptySkuBatch(skuId: ID!) : SkuBatch
    createSku(input: CreateSkuInput!) : CreateSkuPayload
    updateSku(input: UpdateSkuInput!) : UpdateSkuPayload
    deleteSku(input: DeleteSkuInput!) : DeleteSkuPayload
    restoreSku(skuId: ID!): Sku
    createSkuAndSkuBatch(input: CreateSkuInput!) : CreateSkuPayload
  `
export const resolvers = {
  createEmptySkuBatch: resolve(
    authUser,
    unmask('skuId'),
  )(async (obj, args, context) => {
    const { SkuBatch } = context.connections.logisticsDB
    return SkuBatch.create({ skuId: args.skuId })
  }),
  createSku: resolve(
    authUser,
    unmask('input.organizationId'),
  )(async (_obj, args, context) => {
    const db = context.connections.logisticsDB

    // Create SKU
    const changedSku = await db.Sku.create(args.input)

    return { changedSku }
  }),

  createSkuAndSkuBatch: resolve(
    authUser,
    unmask('input.organizationId'),
    unmask('input.batchId'),
  )(async (_obj, args, context) => {
    const db = context.connections.logisticsDB
    const batchId = args.input.batchId ? args.input.batchId : null

    // Create SKU (aka Product)
    const changedSku = await db.Sku.create(args.input)

    // Create SkuBatch (aka Sku = Product x Batch)
    await db.SkuBatch.create({
      skuId: changedSku.id,
      batchId,
    })

    return { changedSku }
  }),

  updateSku: resolve(
    authUser,
    unmask('input.id', 'input.organizationId'),
  )(async (_obj, args, context) => {
    const { id, ...inputs } = args.input
    const db = context.connections.logisticsDB

    const changedSku = await db.Sku.update(inputs, {
      where: {
        id,
      },
    })

    return { changedSku }
  }),

  restoreSku: resolve(
    authUser,
    unmask('skuId'),
  )(async (_obj, args, ctx) => {
    const { Sku } = ctx.connections.logisticsDB
    const { skuId: id } = args

    const changedSku = await Sku.update(
      {
        isArchived: false,
      },
      {
        where: {
          id,
        },
      },
    )
    return changedSku
  }),

  deleteSku: resolve(
    authUser,
    unmask('input.id'),
  )(async (_obj, args, context) => {
    const db = context.connections.logisticsDB
    const skuId = args.input.id
    const changedSku = await db.Sku.update(
      {
        isArchived: true,
      },
      {
        where: {
          id: skuId,
        },
      },
    )

    return { changedSku }
  }),
}
