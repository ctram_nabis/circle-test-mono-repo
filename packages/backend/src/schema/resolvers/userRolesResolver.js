import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const userRolesTypes = `
    createUserRole(input: CreateUserRoleInput!) : User
    deleteUserRole(input: DeleteUserRoleInput!) : User
  `
export const userRoles = {
  createUserRole: resolve(
    authUser,
    unmask('input.userId', 'input.roleId'),
  )(async (_obj, args, context) => {
    const { User, UserRole } = context.connections.logisticsDB
    const { userId, roleId } = args.input

    const find = await UserRole.findAll({ where: { userId } })
    if (!find.length) await UserRole.create({ userId, roleId })
    else await find[0].update({ roleId })

    return User.findByPk(userId)
  }),
  deleteUserRole: resolve(
    authUser,
    unmask('input.userId', 'input.roleId'),
  )(async (_obj, args, context) => {
    const { User, UserRole } = context.connections.logisticsDB
    const { userId, roleId } = args.input

    const userrole = await UserRole.find({ where: { userId, roleId } })
    await userrole.destroy()

    return User.findByPk(userId)
  }),
}
