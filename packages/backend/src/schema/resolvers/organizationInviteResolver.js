import httpError from 'http-errors'
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import config from '../../config'
import { strFormat } from '../../utils/strFormat'

export const typeDefs = `
    createOrganizationInvite(input: CreateOrganizationInviteInput!) : CreateOrganizationInvitePayload
    updateOrganizationInvite(input: UpdateOrganizationInviteInput!) : UpdateOrganizationInvitePayload
    deleteOrganizationInvite(input: DeleteOrganizationInviteInput!) : DeleteOrganizationInvitePayload
  `
export const resolvers = {
  createOrganizationInvite: resolve(
    authUser,
    unmask('input.organizationId', 'input.senderId'),
  )(async (_obj, args, context) => {
    const {
      sendGridClient,
      logisticsDB: { OrganizationInvite, OrganizationUser, Organization, User },
    } = context.connections

    // Returns the invite if it already exists
    const { organizationId } = args.input
    const email = strFormat(args.input.email, { trim: true, lowercase: true })
    /**
     * TODO: Handle resending of invites better plus front-end invite modal button should have a Resend button if invite
     * already exists. Plus, resending invite should be of a different endpoint altogether
     */
    const pendingInvites = await OrganizationInvite.findAll({
      where: { organizationId, email, accepted: null },
    })
    if (pendingInvites && pendingInvites.length > 0) {
      // throw httpError(400, 'An invite to that user has already been sent.')
      // Prepare new invite by destroying all existing pending invites
      await OrganizationInvite.destroy({
        where: {
          id: pendingInvites.map((invite) => invite.id),
        },
      })
    }

    // Checks to see if that user is already part of the organization
    const user = await User.findOne({
      where: { email },
    })
    if (user) {
      const { id: userId } = user.dataValues
      const userAlreadyExistsInOrganization = await OrganizationUser.findOne({
        where: { organizationId, userId },
      })
      if (userAlreadyExistsInOrganization) {
        throw httpError(400, 'A user with that email is already part of the organization.')
      }
    }

    // ensure that the sender exists in the databaes
    const senderUser = await User.findOne({
      where: { id: args.input.senderId },
    })

    if (!senderUser) {
      // should never happen
      throw httpError(500, 'A fatal error has occured.')
    }
    const senderUserName = `${senderUser.dataValues.firstName} ${senderUser.dataValues.lastName}`

    // create the invitation / compose the invitation email
    const organizationInvite = await OrganizationInvite.create({
      ...args.input,
      email,
    })

    const organization = await Organization.findByPk(organizationId)

    // encode unique identifier

    const invitationId = organizationInvite.dataValues.id
    const emailEncoded = encodeURIComponent(email)
    const signUpUrl = `${config.nabis.baseUrl}/signup?invitationId=${invitationId}&email=${emailEncoded}`

    // Send an email to the user
    const { name: organizationName } = organization.dataValues
    const emailOptions = {
      from: 'noreply@getnabis.com',
      to: email,
      subject: `Invitation to join the ${organizationName} organization`,
      html: `
              <div>
                Hi there!
                <br /><br />
                Nabis is a leading distributor of cannabis products. ${senderUserName} has sent you an invitation to join
                ${organizationName} on Nabis's dashboard.
                <br /><br />

                Please <a href="${signUpUrl}">click here to accept the invitation here</a>.
              </div>
            `,
    }

    sendGridClient.sendEmail(emailOptions).then()

    return {
      changedOrganizationInvite: organizationInvite,
    }
  }),
  updateOrganizationInvite: resolve(
    authUser,
    unmask('input.id', 'input.organizationId', 'input.senderId'),
  )(async (_obj, args, { connections: { logisticsDB: db } }) => {
    const { id, ...inputs } = args.input

    // Update the organization invite row
    if (inputs.accepted !== null) {
      inputs.respondedAt = new Date()
    }

    // ternary: if email is truthy, pass to string formatter
    const email = inputs.email && strFormat(inputs.email, { trim: true, lowercase: true })
    const updateObj = email ? { ...inputs, email } : inputs

    await db.OrganizationInvite.update(updateObj, {
      where: {
        id,
      },
    })

    const changedOrganizationInvite = await db.OrganizationInvite.findByPk(id)

    // Add the user to the organization if the invite was accepted
    if (inputs.accepted) {
      const { organizationId, email: inviteEmail } = changedOrganizationInvite.dataValues
      const user = await db.User.findOne({
        where: { email: inviteEmail },
      })

      const { id: userId } = user

      await db.OrganizationUser.create({
        userId,
        organizationId,
      })

      await db.UserPreference.create({
        userId,
        organizationId,
        receiveOrderUpdateEmails: false,
      })

      const userRole = await db.Role.findOne({
        where: {
          name: 'user',
        },
      })
      await db.UserRole.create({
        userId,
        roleId: userRole.id,
      })
    }

    return { changedOrganizationInvite }
  }),
  deleteOrganizationInvite: resolve(
    authUser,
    unmask('input.id'),
  )(async (_obj, args, context) => {
    const { OrganizationInvite } = context.connections.logisticsDB
    const { id } = args.input

    const invitation = await OrganizationInvite.findByPk(id)

    // Update the organization with proper timestamps
    const update = {
      respondedAt: new Date(),
      accepted: false,
    }
    await invitation.update(update)
    await invitation.destroy()

    return {
      changedOrganizationInvite: invitation,
    }
  }),
}
