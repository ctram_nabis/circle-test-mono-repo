import config from '../../config'
import { NABIS_ORGANIZATIONS, NABIS_USERS } from '../../constants'
import { unmask, authUser } from '../graphql-helpers/contrib/relay'
import { resolve } from '../graphql-helpers'

export const typeDefs = `
    createOrganization(input: CreateOrganizationInput!) : CreateOrganizationPayload
    updateOrganization(input: UpdateOrganizationInput!) : UpdateOrganizationPayload
    deleteOrganization(input: DeleteOrganizationInput!) : DeleteOrganizationPayload
  `
export const resolvers = {
  createOrganization: resolve(
    authUser,
    unmask('input.ownerId'),
  )(async (_obj, args, context) => {
    const { logisticsDB: db } = context.connections
    const { input } = args
    const userId = context.user.id

    const org = await db.Organization.create(
      {
        ...input,
        ownerId: input.ownerId || userId,
      },
      {
        include: [
          {
            association: db.Organization.Licenses,
          },
        ],
      },
    )

    await db.OrganizationUser.create({
      userId,
      organizationId: org.id,
    })

    // create toggle option for email notifications for User x Org, default to true
    await db.UserPreference.create({
      userId,
      organizationId: org.id,
      receiveOrderUpdateEmails: true,
    })

    // create a titleholder for the new org
    await db.Titleholder.create({
      organizationId: org.id,
      titleholderOrganizationId: org.id,
      type: 'ORGANIZATION',
    })

    if (config.nodeEnv !== 'testing' && userId !== NABIS_USERS.DELIVERIES) {
      try {
        await db.OrganizationUser.create({
          userId: NABIS_USERS.DELIVERIES,
          organizationId: org.id,
        })

        // create toggle option for email notifications for DeliveriesUser x Org, default to true
        await db.UserPreference.create({
          userId: NABIS_USERS.DELIVERIES,
          organizationId: org.id,
          receiveOrderUpdateEmails: true,
        })

        await db.OrganizationOrganization.create({
          superorganizationId: NABIS_ORGANIZATIONS.DELIVERIES,
          suborganizationId: org.id,
        })
      } catch (error) {
        console.error(error)
        throw error
      }
    }

    if (!context.user.roles.length) {
      const adminRole = await db.Role.findOne({
        where: { name: 'admin' },
      })

      await db.UserRole.create({
        userId,
        roleId: adminRole.id,
        organizationId: org.id,
      })

      console.log(`createOrganization: assigned ${context.user.email} - ${adminRole.name}`)
    }

    return { changedOrganization: org }
  }),
  updateOrganization: resolve(
    authUser,
    unmask('input.id', 'input.ownerId'),
  )(async (_obj, args, context) => {
    const { id, ...inputs } = args.input
    const changedOrganization = await context.connections.logisticsDB.Organization.update(inputs, {
      plain: true,
      returning: true,
      where: {
        id,
      },
    })
    return { changedOrganization: changedOrganization[1] }
  }),
  // deleteOrganization: resolve(
  //   authUser,
  //   unmask('input.id'),
  // )(async (_obj, args, { connections: { logisticsDB: db } }) => {
  //   const org = await db.Organization.findOne({
  //     where: { id: args.input.id },
  //   })
  //   if (!org) {
  //     throw new Error('No organization exists for that id')
  //   }
  //   const deletedOrg = await db.Organization.destroy({
  //     where: {
  //       id: args.input.id,
  //     },
  //   })
  //   return { changedOrganization: deletedOrg }
  // }),
}
