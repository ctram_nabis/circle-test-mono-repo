import { Op, Sequelize } from 'sequelize'
import logger from '../../logging'
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import getOrderAndPaginateOpts from '../utils/getOrderAndPaginateOpts'
import wrapResultsWithPageInfo from '../utils/wrapResultsWithPageInfo'
import moment from 'moment'

export const outstandingInvoicesViewerTypes = `
    allOutstandingInvoices(
      startDate: String
      endDate: String
      startDueDate: String
      endDueDate: String
      organizationId: ID
      orderNumber: String
      pageInfo: PageInfoInput
      all: Boolean
      warehouse: String
      dispensary: String
      orgName: String
    ): AllOrdersPayload
  `

export const outstandingInvoicesViewer = {
  allOutstandingInvoices: resolve(
    authUser,
    unmask('organizationId'),
  )(async (obj, args, context) => {
    logger.info(`Request ${context.id}`, 'In viewer.allOutstandingInvoices()')

    /**
     * TODO: Restore original functionality. Need to research and enable search by:
     * searching by warehouse
     */

    const db = context.connections.logisticsDB

    let startDate = new Date('01-01-2018')
    let endDate = new Date(`01-01-${new Date().getFullYear() + 10}`)

    if (args.startDate) {
      const dateMoment = moment(args.startDate).utc()

      if (dateMoment.isValid()) {
        startDate = dateMoment
          .clone()
          .startOf('day')
          .toDate()
      }
    }

    if (args.endDate) {
      const dateMoment = moment(args.endDate).utc()

      if (dateMoment.isValid()) {
        endDate = dateMoment
          .clone()
          .endOf('day')
          .toDate()
      }
    }

    const where = {
      isArchived: false,
      [Op.and]: [
        {
          paymentStatus: 'UNPAID',
        },
        {
          status: ['DELIVERED', 'DELIVERED_WITH_EDITS'],
        },
        {
          action: 'DELIVERY_TO_RETAILER',
        },
        {
          daysTillPaymentDue: {
            [Op.not]: null,
          },
        },
        {
          date: {
            [Op.between]: [startDate, endDate],
          },
        },
      ],
    }

    if (args.orderNumber && args.orderNumber.trim()) {
      where[Op.and].push(
        Sequelize.where(Sequelize.cast(Sequelize.col('Order.number'), 'varchar'), {
          [Op.iLike]: `${args.orderNumber.trim()}%`,
        }),
      )
    }

    const startDueDate = args.startDueDate || startDate
    const endDueDate = args.endDueDate || endDate

    if (args.startDueDate || args.endDueDate) {
      where[Op.and].push(
        Sequelize.where(Sequelize.literal('date + interval \'1 day\' * "daysTillPaymentDue"'), {
          [Op.between]: [startDueDate, endDueDate],
        }),
      )
    }

    if (args.organizationId) {
      where[Op.and].push({
        organizationId: args.organizationId,
      })

      const { user } = context

      if (
        !user.organizations ||
        !user.organizations.length ||
        !user.organizations.find((o) => o.id === args.organizationId)
      ) {
        return []
      }

      if (user.roles.length && user.roles[0].name === 'user') {
        where[Op.or] = [{ creatorId: user.id }, { assigneeId: user.id }]
      } else if (user.roles.length && user.roles[0].name === 'viewer') {
        where[Op.or] = [{ creatorId: user.id }, { assigneeId: user.id }]
      }
    }

    const opts = {
      include: [
        // 0
        {
          association: db.Order.Creator,
        },
        // 1
        {
          association: db.Order.Warehouse,
          include: [
            // 0
            {
              association: db.Warehouse.Site,
              include: [
                // 0
                {
                  association: db.Site.LicensedLocation,
                },
              ],
            },
          ],
        },
        // 2
        {
          as: 'organization',
          association: db.Order.Organization,
        },
        // 3
        {
          as: 'site',
          association: db.Order.Site,
        },
      ],
      where,
    }

    /**
     * TODO: This warehouse search is tough!! Come back later and implement
     */
    // // => 1
    // if (args.warehouse) {
    //   console.log({ warehouse: args.warehouse })
    //   opts.include[1].where = {
    //     '$warehouse.site.licensedLocation.name$': {
    //       [Op.iLike]: `%${args.warehouse}%`,
    //     },
    //   }
    // }

    // => 2
    if (args.orgName && args.orgName.trim()) {
      if (!args.organizationId) {
        // For Admin Outstanding Invoices
        opts.include[2].where = {
          '$organization.name$': {
            [Op.iLike]: `%${args.orgName.trim()}%`,
          },
        }
      } else {
        // For Aging Reports
        where[Op.and].push({
          name: {
            [Op.iLike]: `${args.orgName.trim()}%`,
          },
        })
      }
    }

    // => 3
    if (args.dispensary && args.dispensary.trim()) {
      opts.include[3].where = {
        '$site.name$': {
          [Op.iLike]: `%${args.dispensary.trim()}%`,
        },
      }
    }

    const orderOpts = {
      order: Sequelize.literal('date + interval \'1 day\' * "daysTillPaymentDue" asc'),
    }

    let paginateOpts = {}

    if (!args.all) {
      const { paginateOpts: paginateOptions } = getOrderAndPaginateOpts(args.pageInfo)
      paginateOpts = paginateOptions
    }

    const results = await db.Order.findAll({
      ...opts,
      ...orderOpts,
      ...paginateOpts,
    })

    const pageInfo = wrapResultsWithPageInfo(db.Order, { ...opts, ...orderOpts }, args)

    return {
      pageInfo,
      results,
    }
  }),
}
