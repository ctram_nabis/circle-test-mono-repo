import reduce from 'lodash/reduce'

import logger from '../../logging'
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    createRu(input: CreateRuInput!): String
    updateRu(input: UpdateRuInput!): Boolean
    deleteRu(id: ID!): Boolean
  `
export const resolvers = {
  createRu: resolve(
    authUser,
    unmask('input.organizationId', 'input.licenseId'),
  )((obj, args, context) => {
    logger.info(`Request ${context.id}`, 'In mutation.createRu()')
    const db = context.connections.logisticsDB
    return db.Ru.create(args.input).then((result) => result.dataValues.id)
  }),

  updateRu: resolve(
    authUser,
    unmask('input.organizationId'),
  )((obj, args, context) => {
    logger.info(`Request ${context.id}`, 'In mutation.updateRu()')
    const db = context.connections.logisticsDB
    return db.Ru.update(args.input, {
      where: { id: args.input.id },
    }).then(() => true)
  }),

  deleteRu: resolve(authUser)((obj, args, context) => {
    logger.info(`Request ${context.id}`, 'In mutation.deleteRu()')
    const db = context.connections.logisticsDB
    const ruId = args.id
    return db.Ru.findByPk(ruId)
      .then(async (res) => {
        const skuIds = JSON.parse(
          JSON.stringify(
            await db.Sku.findAll({
              where: { ruId },
            }),
          ),
        )
        const flatIds = reduce(
          skuIds,
          (accum, s) => {
            accum.push(s.id)
            return accum
          },
          [],
        )
        await db.InventoryItem.destroy({
          where: { id: flatIds },
        })
        await db.Sku.destroy({
          where: { id: flatIds },
        })
        return Promise.resolve(true)
      })
      .then(() => {
        db.Ru.destroy({ where: { id: ruId } })
      })
      .then(() => true)
  }),
}
