import { cloneDeep, get, find } from 'lodash'

import idx from 'idx'

import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import { unpackId } from '../graphql-helpers/contrib/relay/resolvers'
import { generateManifest } from './manifestResolver'
import { autoAssignWarehouseByLicensedLocation } from '../../utils/inventoryHelpers'
import titleizeDaysTillPaymentDue from '../../utils/titleizeDaysTillPaymentDue'
import { toChangeLogDate } from '../../utils/parseDate'
import dollarFormat from '../../utils/dollarFormat'
import { validateOrder } from '../../utils/validateOrder'
import { generateOrderConfirmationEmail } from '../utils/generateOrderConfirmationEmail'

import { NABIS_ORGANIZATIONS, ORDER_ACTIONS, ORDER_STATUS_PARSED } from '../../constants'

const adaptLineItemToLineItemChanges = (
  { name, skuBatchId, quantity, pricePerUnit, isExciseTaxable, isSample, id, organizationId, discount },
  changeType,
) => {
  if (!['ADDED', 'REMOVED'].includes(changeType)) {
    throw new Error('LineItemChangeType must be either ADDED or REMOVED')
  }

  return {
    name: name,
    skuBatchId: skuBatchId,
    quantity: quantity,
    pricePerUnit: pricePerUnit,
    isExciseTaxable: isExciseTaxable,
    isSample: isSample,
    organizationId,
    type: changeType,
    lineItemId: id,
    discount: discount,
  }
}

const orderAttributes = [
  {
    column: 'date',
    title: 'order date',
    format: (d) => toChangeLogDate(d),
  },
  {
    column: 'daysTillPaymentDue',
    title: 'payment terms',
    format: titleizeDaysTillPaymentDue,
  },
  {
    column: 'gmvCollected',
    title: 'GMV collected',
    format: dollarFormat,
  },
  {
    column: 'exciseTaxCollected',
    title: 'excise tax collected',
    format: dollarFormat,
  },
  {
    column: 'notes',
    title: 'order notes',
  },
  {
    column: 'assignee',
    title: 'order assigned',
    format: (d) => (d && d.email) || 'N/A',
  },
  {
    column: 'discount',
    title: 'order discount',
    format: dollarFormat,
  },
  {
    column: 'surcharge',
    title: 'order surcharge',
    format: dollarFormat,
  },
  {
    column: 'hasUnalterablePaymentTerms',
    title: 'unalterable payment terms',
  },
  {
    column: 'mustPayPreviousBalance',
    title: 'pay previous balance prior to delivery',
  },
]

export const typeDefs = `
    createOrder(input: CreateOrderInput!) : CreateOrderPayload
    updateOrder(input: UpdateOrderInput!) : UpdateOrderPayload
    deleteOrder(input: DeleteOrderInput!) : DeleteOrderPayload
  `

export const resolvers = {
  createOrder: resolve(
    authUser,
    unmask(
      'input.driverId',
      'input.organizationId',
      'input.vehicleId',
      'input.siteId',
      'input.warehouseId',
      'input.licensedLocationId',
    ),
  )(async (_obj, args, context) => {
    const { input } = args

    const { logisticsDB, sendGridClient, sequelize } = context.connections

    const handleSequelizeManyToManyQuirks = () => {
      const unmaskedAttachmentIds = (input.attachments || []).map((a) => unpackId(a.id))
      input.lineItems = (input.lineItems || []).map((_li) => {
        const li = _li
        if (_li.id) {
          li.id = unpackId(_li.id)
        }
        if (_li.skuId) {
          li.skuId = unpackId(_li.skuId)
        }
        if (_li.batchId) {
          li.batchId = unpackId(_li.batchId)
        }
        if (_li.skuBatchId) {
          li.skuBatchId = unpackId(_li.skuBatchId)
        }
        if (_li.organizationId) {
          li.organizationId = unpackId(_li.organizationId)
        }
        return li
      })
      delete input.attachments
      return (order) => {
        order.setAttachments(unmaskedAttachmentIds)
        return order
      }
    }

    const addAttachmentsTheSequelizeWay = handleSequelizeManyToManyQuirks()

    const errors = await validateOrder(args, context)
    if (errors.length) {
      throw errors
    }

    // autoAssign warehouse if null
    if (!input.warehouseId) {
      input.warehouseId = await autoAssignWarehouseByLicensedLocation(logisticsDB, input.licensedLocationId)
    }

    const { id: creatorId, isAdmin } = context.user

    const createOrderPayload = { ...cloneDeep(input), creatorId }

    const overrideOrganizationIdForNabisTransferPayload =
      input.action === ORDER_ACTIONS.INTERNAL_TRANSFER && isAdmin
        ? { ...createOrderPayload, organizationId: NABIS_ORGANIZATIONS.DELIVERIES }
        : createOrderPayload

    const createOrderPromise = logisticsDB.Order.create(overrideOrganizationIdForNabisTransferPayload, {
      include: [
        {
          association: logisticsDB.Order.LineItems,
        },
        {
          association: logisticsDB.Order.Attachments,
        },
        {
          association: logisticsDB.Order.Subscribers,
        },
      ],
    }).then(addAttachmentsTheSequelizeWay)

    const getOrgPromise = logisticsDB.Organization.findOne({
      where: { id: input.organizationId },
      include: [
        {
          association: logisticsDB.Organization.Users,
          include: [
            {
              association: logisticsDB.User.Roles,
            },
          ],
        },
      ],
    })

    const [changedOrder, org] = await Promise.all([createOrderPromise, getOrgPromise])
    const { googleDriveClient } = context.connections

    const lineItemChanges = (idx(args, (_) => _.input.lineItems) || []).map((li) =>
      adaptLineItemToLineItemChanges(li, 'ADDED'),
    )

    await logisticsDB.OrderChange.create(
      {
        orderId: changedOrder.id,
        submitterId: context.user.id,
        organizationId: org.id,
        description: `Created order #${changedOrder.number}`,
        lineItemChanges,
      },
      {
        include: [
          {
            association: logisticsDB.OrderChange.LineItemChanges,
          },
        ],
      },
    )

    await logisticsDB.OrderChange.create({
      orderId: changedOrder.id,
      submitterId: context.user.id,
      organizationId: org.id,
      description: `Initial order date: ${toChangeLogDate(changedOrder.date)}`,
    })

    // add subscribers
    const orderOrganizationIds = (idx(changedOrder, (_) => _.lineItems) || []).map((li) => li.organizationId)
    // subscriber is an org with lineItems on the order that !== the submitting org
    const subscriberIds = orderOrganizationIds.reduce((accum, orgId) => {
      const outsideOfOrg = orgId !== changedOrder.organizationId
      const fresh = !(accum.indexOf(orgId) > -1)
      if (outsideOfOrg && fresh) {
        return [...accum, orgId]
      }
      return accum
    }, [])

    const subscriberPromises = subscriberIds.map(async (subscriberId) => {
      logisticsDB.OrderSubscriber.create({
        orderId: changedOrder.id,
        subscriberId,
      })
    })

    await Promise.all(subscriberPromises)

    // do api stuff after data is in db
    // Generate GDrive file async.
    const { invoicesS3FileLink } = await googleDriveClient.resolvers.generateManifestHook(
      changedOrder,
      logisticsDB,
      googleDriveClient,
      sequelize,
    )

    // Notify our team that an order is created
    const ord = {
      ...changedOrder.dataValues,
      creator: context.user || {},
      invoicesS3FileLink,
    }

    const email = await generateOrderConfirmationEmail(ord, org, context)

    sendGridClient.sendEmail(email)

    return { changedOrder }
  }),
  updateOrder: resolve(
    authUser,
    unmask(
      'input.id',
      'input.site.id',
      'input.driverId',
      'input.vehicleId',
      'input.assigneeId',
      'input.warehouseId',
      'input.organization.id',
      'input.site.licensedLocationId',
    ),
  )(async (_obj, args, context) => {
    const db = context.connections.logisticsDB

    console.log(args)

    const unmaskLineItemIds = () => {
      get(args, 'input.lineItems', []).map((_li) => {
        const li = _li
        if (_li.id) {
          li.id = unpackId(_li.id)
        }
        if (_li.skuId) {
          li.skuId = unpackId(_li.skuId)
        }
        if (_li.batchId) {
          li.batchId = unpackId(_li.batchId)
        }
        if (_li.skuBatchId) {
          li.skuBatchId = unpackId(_li.skuBatchId)
        }
        if (get(_li, 'organization.id')) {
          li.organizationId = unpackId(_li.organization.id)
        }
        li.orderId = args.input.id
        return li
      })
    }

    unmaskLineItemIds()
    const errors = await validateOrder(args, context, true)
    if (errors.length) {
      throw errors
    }

    const handleSequelizeManyToManyQuirks = () => {
      const unmaskedAttachmentIds = (idx(args, (_) => _.input.attachments) || []).map((a) => unpackId(a.id))
      delete args.input.attachments
      return db.Order.findOne({
        where: { id: args.input.id },
        include: [
          { association: db.Order.Attachments },
          { association: db.Order.Subscribers },
          { association: db.Order.Site },
          { association: 'assignee' },
        ],
      }).then(async (o) => {
        o.setAttachments(unmaskedAttachmentIds)

        // autoAssign warehouse if null
        if (!o.warehouseId) {
          o.warehouseId = await autoAssignWarehouseByLicensedLocation(db, o.licensedLocationId)
        }

        // Save warehouse preference to master dispensary list
        const siteLicensedLocationId = idx(o, (_) => _.site.licensedLocationId)
        if (siteLicensedLocationId) {
          db.LicensedLocation.update({ warehouseId: args.input.warehouseId }, { where: { id: siteLicensedLocationId } })
        }

        return o
      })
    }

    const editOrder = db.Order.findOne({
      where: { id: args.input.id },
      include: [{ association: db.Order.LineItems }, { association: db.Order.Site }, { association: 'assignee' }],
    }).then((order) => {
      const siteLicensedLocationIdFromInput = idx(args, (_) => _.input.site.licensedLocationId)

      const oldOrder = order.toJSON()
      return (
        order
          // Blake: following line is to ensure order.licensedLocationId is in sync with order.site.licensedLocationId (only if site info is in args.input)
          .update({ ...args.input, licensedLocationId: siteLicensedLocationIdFromInput })
          .then(handleSequelizeManyToManyQuirks)
          .then(async (_newOrder) => {
            const newOrder = await db.Order.findOne({
              where: { id: _newOrder.id },
              include: [
                {
                  association: db.Order.LineItems,
                },
                { association: db.Order.Site },
                { association: 'assignee' },
              ],
            })
            return {
              oldOrder,
              newOrder,
            }
          })
      )
    })

    const hasSite = !!idx(args, (_) => _.input.site.id)
    const editSite = hasSite
      ? db.Site.findOne({
          where: { id: args.input.site.id },
        }).then((site) => {
          const oldSite = site.toJSON()
          return site.update(args.input.site).then((newSite) => ({
            oldSite,
            newSite,
          }))
        })
      : Promise.resolve()

    const notInNewLineItems = (input) => !args.input.lineItems.find((li) => li.id === input.id)

    const hasLineItems = !!get(args, 'input.lineItems')
    const getCurrentLineItems = db.LineItem.findAll({
      where: { orderId: args.input.id },
    })

    const deleteLineItems = !hasLineItems
      ? Promise.resolve()
      : getCurrentLineItems.then((currentLineItems) => {
          const deleting = currentLineItems.filter(notInNewLineItems)
          const promises = deleting.map((li) =>
            db.LineItem.destroy({
              where: { id: li.id },
            }),
          )
          return Promise.all(promises).then(() => deleting)
        })

    // Without manually offsetting the create time, Sequelize will not
    // guarantee the order of creation time. Users will definitely report
    // unexpectedly out of order line items as a bug.
    // The 12ms offset ensures the line items order is correct.
    const ensureCorrectOrder = (n) =>
      // eslint-disable-next-line promise/param-names
      new Promise((go) => setTimeout(go, n * 12))

    const createLineItems = Promise.all(
      (args.input.lineItems || [])
        .filter((li) => !li.id)
        .map((li, i) => ensureCorrectOrder(i).then(() => db.LineItem.create(li))),
    )

    const updateLineItems = !hasLineItems
      ? Promise.resolve()
      : getCurrentLineItems.then((removed) => {
          const updating = (args.input.lineItems || []).filter((li) => !!li.id)
          const promises = updating.map((li) =>
            db.LineItem.update(li, {
              where: { id: li.id },
            }),
          )
          const getNewlyUpdatedLineItems = () =>
            db.LineItem.findAll({
              where: {
                id: updating.map((li) => li.id),
              },
            })
          return Promise.all(promises)
            .then(getNewlyUpdatedLineItems)
            .then((res) => ({
              removed,
              added: res,
            }))
        })

    const [{ oldOrder, newOrder }, siteRes, deletedRes, createdRes, updatedRes] = await Promise.all([
      editOrder,
      editSite,
      deleteLineItems,
      createLineItems,
      updateLineItems,
    ])

    const newAdminNotes = newOrder.adminNotes
    const oldAdminNotes = oldOrder.adminNotes

    if (newAdminNotes && newAdminNotes !== oldAdminNotes && args.input.adminNotes) {
      await db.OrderChange.create({
        orderId: newOrder.id,
        submitterId: context.user.id,
        description: `%••%AdminNotes%••%
        ${oldOrder.adminNotes}
        %••% ${newOrder.adminNotes}`,
      })
    }
    const newStatus = idx(args, (_) => _.input.status)
    const parsedNewStatus = ORDER_STATUS_PARSED[newStatus]
    if (parsedNewStatus) {
      await db.OrderChange.create({
        orderId: newOrder.id,
        submitterId: context.user.id,
        description: `Set order status to ${parsedNewStatus}`,
      })
    }

    const logOrderChange = async ({ column, title, format = (x) => x }) => {
      const oldVal = format(oldOrder[column])
      const newVal = format(newOrder[column])
      const shouldLogChange = oldVal !== newVal

      if (shouldLogChange) {
        await db.OrderChange.create({
          orderId: newOrder.id,
          submitterId: context.user.id,
          description: `Set ${title} to ${newVal}`,
        })
      }
    }

    orderAttributes.forEach(logOrderChange)

    if (siteRes) {
      const { oldSite, newSite } = siteRes
      const siteAttributes = ['name', 'address1', 'pocName', 'pocPhoneNumber', 'pocEmail']
      const changedSiteAttributes = siteAttributes.filter((col) => oldSite[col] !== newSite[col])
      if (changedSiteAttributes.length > 0) {
        await db.OrderChange.create({
          orderId: newOrder.id,
          submitterId: context.user.id,
          description: "Changed location's address or point of contact",
        })
      }
    }

    const includeLineItemsAssociation = {
      include: [
        {
          association: db.OrderChange.LineItemChanges,
        },
      ],
    }

    const setOrderEditFlagTrue = (orderId) => {
      db.Order.update(
        {
          edited: true,
        },
        {
          where: {
            id: orderId,
          },
        },
      )
    }

    const hasRemovedLineItems = deletedRes && deletedRes.length > 0
    if (hasRemovedLineItems) {
      await db.OrderChange.create(
        {
          orderId: newOrder.id,
          submitterId: context.user.id,
          description: 'Removed order contents',
          lineItemChanges: deletedRes.map((li) => adaptLineItemToLineItemChanges(li, 'REMOVED')),
        },
        includeLineItemsAssociation,
      )
      setOrderEditFlagTrue(newOrder.id)
    }

    const hasAddedLineItems = createdRes && createdRes.length > 0
    if (hasAddedLineItems) {
      await db.OrderChange.create(
        {
          orderId: newOrder.id,
          submitterId: context.user.id,
          description: 'Added order contents',
          lineItemChanges: createdRes.map((li) => adaptLineItemToLineItemChanges(li, 'ADDED')),
        },
        includeLineItemsAssociation,
      )
      setOrderEditFlagTrue(newOrder.id)
    }

    if (updatedRes) {
      const lineItemsAfter = updatedRes.added.filter((postUpdate) => {
        const preUpdate = find(updatedRes.removed, (l) => l.id === postUpdate.id)
        const hasChanged =
          postUpdate.name !== preUpdate.name ||
          postUpdate.skuBatchId !== preUpdate.skuBatchId ||
          postUpdate.quantity !== preUpdate.quantity ||
          postUpdate.pricePerUnit !== preUpdate.pricePerUnit ||
          postUpdate.isExciseTaxable !== preUpdate.isExciseTaxable ||
          postUpdate.isSample !== preUpdate.isSample ||
          postUpdate.discount !== preUpdate.discount
        return hasChanged
      })
      const lineItemsBefore = updatedRes.removed.filter((preUpdate) =>
        find(lineItemsAfter, (postUpdate) => postUpdate.id === preUpdate.id),
      )
      const hasUpdatedLineItems = lineItemsBefore.length > 0
      if (hasUpdatedLineItems) {
        const updatedLineItemChanges = lineItemsBefore
          .map((li) => adaptLineItemToLineItemChanges(li, 'REMOVED'))
          .concat(lineItemsAfter.map((li) => adaptLineItemToLineItemChanges(li, 'ADDED')))
        await db.OrderChange.create(
          {
            orderId: newOrder.id,
            submitterId: context.user.id,
            description: 'Updated order contents',
            lineItemChanges: updatedLineItemChanges,
          },
          includeLineItemsAssociation,
        )
        setOrderEditFlagTrue(newOrder.id)
      }
    }

    // check for pre-existing relations
    const existingSubs = await db.OrderSubscriber.findAll({
      where: { orderId: newOrder.id },
    })
    const existingSubIds = existingSubs.map((subscriber) => subscriber.subscriberId)

    // reduce incoming lineItems to the unique and new subscriber orgs
    const _orgIds = idx(newOrder, (_) => _.lineItems).map((li) => li.organizationId)

    const subscriberIds = _orgIds.reduce((accum, orgId) => {
      const outsideOfOrg = orgId !== newOrder.organizationId
      const fresh = !(accum.indexOf(orgId) > -1) // so subscriberIds is distinct
      const dbFresh = !(existingSubIds.indexOf(orgId) > -1)
      if (outsideOfOrg && fresh && dbFresh) {
        return [...accum, orgId]
      }
      return accum
    }, [])

    const subscriberPromises = subscriberIds.map(async (subscriberId) => {
      db.OrderSubscriber.create({
        orderId: newOrder.id,
        subscriberId,
      })
    })

    await Promise.all(subscriberPromises)

    // generate manifest after all the data stuff is done
    await generateManifest(args.input.id, context)
    return {
      changedOrder: newOrder,
    }
  }),

  deleteOrder: resolve(
    authUser,
    unmask('input.id'),
  )(async (_obj, { input }, context) => {
    const { Order, OrderChange } = context.connections.logisticsDB

    const { id } = input || {}

    const [success, [changedOrder]] = await Order.update(
      {
        isArchived: true,
      },
      {
        returning: true,
        where: {
          id,
        },
      },
    )

    if (success !== 1) {
      throw new Error('Archive order was unsuccessful')
    }

    await OrderChange.create({
      description: `Archived order #${changedOrder.number}`,
      orderId: changedOrder.id,
      submitterId: context.user.id,
    })

    return { changedOrder }
  }),
}
