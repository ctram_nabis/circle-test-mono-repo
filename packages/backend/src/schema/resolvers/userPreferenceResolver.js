import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    updateUserPreference(
      userId: ID!
      organizationId: ID!
      receiveOrderUpdateEmails: Boolean
    ): UserPreference
  `

export const resolvers = {
  updateUserPreference: resolve(
    authUser,
    unmask('userId'),
    unmask('organizationId'),
  )(async (_obj, args, ctx) => {
    const db = ctx.connections.logisticsDB
    const { receiveOrderUpdateEmails, organizationId, userId } = args

    const existingUserPreference = await db.UserPreference.findOne({
      where: {
        organizationId,
        userId,
      },
    })

    if (existingUserPreference) {
      return db.UserPreference.update(
        {
          receiveOrderUpdateEmails,
        },
        {
          where: {
            organizationId,
            userId,
          },
        },
      )
    } else {
      throw new Error('no UserPreference record exists for given userId and organizationId')
    }
  }),
}
