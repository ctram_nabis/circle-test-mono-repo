import { Op } from 'sequelize'

import { resolve } from '../graphql-helpers'
import { authUser, unmask, authAdminUser } from '../graphql-helpers/contrib/relay'
import logger from '../../logging'

import getOrderAndPaginateOpts from '../utils/getOrderAndPaginateOpts'
import wrapResultsWithPageInfo from '../utils/wrapResultsWithPageInfo'

const ALL_ADMIN_BATCHES = async (_o, { isArchived = false, pageInfo, search, organizationId }, context) => {
  logger.info(`Request ${context.id}`, 'In query.allAdminBatches()')
  const { Batch } = context.connections.logisticsDB

  const where = organizationId ? { isArchived, organizationId } : { isArchived }

  if (search && search.length > 0) {
    where.code = { [Op.iLike]: `%${search}%` }
  }

  const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(pageInfo)
  const results = await Batch.findAll({
    order: [['createdAt', 'DESC']],
    paranoid: true,
    returning: true,
    where,
    ...paginateOpts,
  })

  const _pageInfo = await wrapResultsWithPageInfo(
    Batch,
    {
      where,
      ...orderOpts,
      ...paginateOpts,
    },
    { pageInfo },
  )

  return {
    pageInfo: _pageInfo,
    results,
  }
}

export const batchViewerTypes = `
    allBatches(
      isArchived: Boolean
      organizationId: ID
      pageInfo: PageInfoInput
      search: String
      isInventoryTab: Boolean
    ): BatchInfo
    allAdminBatches(
      isArchived: Boolean
    ): BatchInfo
  `
export const batchViewer = {
  allAdminBatches: resolve(authAdminUser)((o, args, ctx) => ALL_ADMIN_BATCHES(o, args, ctx)),
  allBatches: resolve(
    authUser,
    unmask('organizationId'),
  )(async (o, args, context) => {
    logger.info(`Request ${context.id}`, 'In query.allBatches()')
    if (context.user.isAdmin && args.isInventoryTab) {
      return ALL_ADMIN_BATCHES(o, args, context)
    }

    if (!context.user.isAdmin && !args.organizationId) {
      throw new Error('You must provide an organizationId unless you are a nabis admin')
    }

    const { Batch } = context.connections.logisticsDB

    const { isArchived = false, organizationId, search, pageInfo } = args

    const opts = {
      include: [{ association: Batch.License }],
      paranoid: true,
      returning: true,
      where: organizationId ? { isArchived, organizationId } : { isArchived },
    }

    if (search && search.length > 0) {
      opts.where.code = { [Op.iLike]: `%${search}%` }
    }

    const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(pageInfo)
    const results = await Batch.findAll({
      ...opts,
      ...orderOpts,
      ...paginateOpts,
    })

    const _pageInfo = await wrapResultsWithPageInfo(
      Batch,
      {
        ...opts,
        ...orderOpts,
      },
      args,
    )

    return {
      pageInfo: _pageInfo,
      results,
    }
  }),
}
