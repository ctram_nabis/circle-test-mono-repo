import logger from '../../logging'
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

import config from '../../config'

export const typeDefs = `
    createSite(input: CreateSiteInput!) : CreateSitePayload
    updateSite(input: UpdateSiteInput!) : UpdateSitePayload
    deleteSite(input: DeleteSiteInput!) : DeleteSitePayload
  `
export const resolvers = {
  createSite: resolve(
    authUser,
    unmask('input.organizationId', 'input.licensedLocationId'),
  )(async (_obj, args, context) => {
    logger.debug('createSite input:', { site: args.input })

    const { logisticsDB } = context.connections
    const { Site, LicensedLocation } = logisticsDB

    const newSite = args.input
    const userId = context.user.id

    const creatingPublicSite = !newSite.organizationId
    if (creatingPublicSite) {
      newSite.organizationId = config.publicSitesOrganizationId
    }

    let changedSite
    let createdLicensedLocation
    let licensedLocation

    try {
      changedSite = await Site.create(newSite)
    } catch (error) {
      console.error('createSite unable to create the site', error)
      throw error
    }

    const preExistingLicensedLocation = await LicensedLocation.findOne({
      where: { id: newSite.licensedLocationId },
    })

    if (!preExistingLicensedLocation) {
      try {
        createdLicensedLocation = await LicensedLocation.create({
          name: newSite.name,
          address1: newSite.address1,
          address2: newSite.address2,
          city: newSite.city,
          state: newSite.state,
          zip: newSite.zip,
          lat: newSite.lat,
          lng: newSite.lng,
          siteCategory: newSite.siteCategory,
          submittedById: userId,
          verificationStatus: 'UNVERIFIED',
        })
      } catch (error) {
        console.error('createSite unable to create licensedLocation', error)
        throw error
      }
      licensedLocation = createdLicensedLocation
      console.log('createSite Created a new LicensedLocation', createdLicensedLocation)
    } else {
      licensedLocation = preExistingLicensedLocation
      console.log('createSite unable to create, LicensedLocation already exists')
    }

    changedSite.licensedLocation = licensedLocation
    return { changedSite }
  }),
  updateSite: resolve(
    authUser,
    unmask('input.id', 'input.organizationId', 'input.licensedLocationId'),
  )((_obj, args, context) => {
    const { id, ...inputs } = args.input
    return context.connections.logisticsDB.Site.update(inputs, {
      where: {
        id,
      },
    }).then(() => ({
      changedSite: context.connections.logisticsDB.Site.findByPk(id),
    }))
  }),
  deleteSite: resolve(
    authUser,
    unmask('input.id'),
  )((_obj, args, context) => {
    let data
    return context.connections.logisticsDB.Site.findByPk(args.input.id)
      .then((res) => {
        data = res
        return context.connections.logisticsDB.Site.destroy({
          where: {
            id: args.input.id,
          },
        })
      })
      .then(() => ({
        changedSite: data,
      }))
  }),
}
