import httpError from 'http-errors'
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import { generateOrderConfirmationEmail } from '../utils/generateOrderConfirmationEmail'

export const sendSendGridEmail = async (args, context) => {
  const { orderId } = args.input
  const { logisticsDB: db, sendGridClient } = context.connections

  try {
    const order = await db.Order.findByPk(orderId, {
      include: [
        {
          association: db.Order.Creator,
        },
        { association: db.Order.Site },
      ],
    })

    const organization = await db.Organization.findByPk(order.organizationId, {
      include: [
        {
          association: db.Organization.Users,
          include: [
            {
              association: db.User.Roles,
            },
          ],
        },
      ],
    })

    const sendGridEmail = await generateOrderConfirmationEmail(order, organization, context)

    if (sendGridEmail && sendGridEmail.templateId) {
      const successfullySentEmails = await sendGridClient.sendEmail(sendGridEmail)

      if (successfullySentEmails) {
        return { success: true }
      }
    }

    return { success: false }
  } catch (error) {
    throw httpError(500, `Send Email Confirmation Error: ${error}`)
  }
}

export const typeDefs = `
    sendSendGridStatusUpdateEmail (input: SendSendGridStatusUpdateInput!) : SendSendGridStatusUpdateEmailPayload
  `

export const resolvers = {
  sendSendGridStatusUpdateEmail: resolve(
    authUser,
    unmask('input.orderId'),
  )((_obj, args, context) => sendSendGridEmail(args, context)),
}
