// TODO: DELETE ME AS WE SHOULDN'T BE CREATING LINEITEMS FROM THE FRONTENT
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    createLineItem(input: CreateLineItemInput!) : CreateLineItemPayload
    updateLineItem(input: UpdateLineItemInput!) : UpdateLineItemPayload
    deleteLineItem(input: DeleteLineItemInput!) : DeleteLineItemPayload
  `
export const resolvers = {
  createLineItem: resolve(
    authUser,
    unmask('input.skuId', 'input.orderId'),
  )((obj, args, context) =>
    context.connections.logisticsDB.LineItem.create(args.input).then((changedLineItem) => ({
      changedLineItem,
    })),
  ),
  updateLineItem: resolve(
    authUser,
    unmask('input.id', 'input.skuId', 'input.orderId', 'input.organizationId'),
  )((obj, args, context) => {
    const { id, ...inputs } = args.input
    return context.connections.logisticsDB.LineItem.update(inputs, {
      where: {
        id,
      },
    }).then(() => ({
      changedLineItem: context.connections.logisticsDB.LineItem.findByPk(id),
    }))
  }),
  deleteLineItem: resolve(
    authUser,
    unmask('input.id'),
  )((obj, args, context) => {
    let data
    return context.connections.logisticsDB.LineItem.findByPk(args.input.id)
      .then((res) => {
        data = res
        return context.connections.logisticsDB.LineItem.destroy({
          where: {
            id: args.input.id,
          },
        })
      })
      .then(() => ({
        changedLineItem: data,
      }))
  }),
}
