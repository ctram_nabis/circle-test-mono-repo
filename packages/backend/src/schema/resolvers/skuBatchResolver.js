import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    restoreSkuBatch(id: ID!): SkuBatch
    deleteSkuBatch(id: ID!): SkuBatch
    createSkuBatch(batchId: ID, skuId: ID!, titleholderId: ID!): SkuBatch
    updateSkuBatch(skuBatchId: ID!, batchId: ID, skuId: ID!): Boolean
    updateSkuBatchTitleholder(skuBatchId: ID!, titleholderId: ID!): SkuBatch
  `
export const resolvers = {
  restoreSkuBatch: resolve(
    authUser,
    unmask('id'),
  )(async (_obj, args, context) => {
    const { SkuBatch } = context.connections.logisticsDB
    const [skuBatch] = await SkuBatch.restore({
      returning: true,
      // paranoid: true,
      where: { id: args.id },
    })
    return skuBatch
  }),

  deleteSkuBatch: resolve(
    authUser,
    unmask('id'),
  )(async (_obj, args, context) => {
    const { SkuBatch } = context.connections.logisticsDB
    const [skuBatch] = await SkuBatch.destroy({
      returning: true,
      // paranoid: true,
      where: { id: args.id },
    })
    return skuBatch
  }),

  createSkuBatch: resolve(
    authUser,
    unmask('skuId', 'batchId', 'titleholderId'),
  )(async (_obj, { skuId, batchId = null, titleholderId }, context) => {
    const { SkuBatch } = context.connections.logisticsDB

    if (!skuId) {
      throw new Error('SkuId is required to create a skuBatch')
    }

    const newSkuBatch = await SkuBatch.create({
      batchId: batchId,
      skuId: skuId,
      titleholderId,
    })

    return newSkuBatch
  }),

  updateSkuBatch: resolve(
    authUser,
    unmask('batchId', 'skuId', 'skuBatchId'),
  )(async (_obj, { batchId = null, skuId, skuBatchId }, context) => {
    const { SkuBatch } = context.connections.logisticsDB

    if (!skuId) {
      throw new Error('A skuId is required to update a SkuBatch')
    }

    let skuBatches
    if (!skuBatchId) {
      skuBatches = await SkuBatch.findAll({
        where: {
          batchId,
          skuId,
        },
      })

      if (!skuBatches.length) {
        await SkuBatch.create({
          batchId,
          skuId,
        })
      }
      return true
    }

    skuBatches = await SkuBatch.findByPk(skuBatchId)
    await skuBatches.update({
      batchId,
      skuId,
    })
    return true
  }),

  updateSkuBatchTitleholder: resolve(
    authUser,
    unmask('skuBatchId', 'titleholderId'),
  )(async (_obj, { skuBatchId, titleholderId }, context) => {
    const { SkuBatch } = context.connections.logisticsDB

    const [_success, [updatedSkuBatch]] = await SkuBatch.update(
      {
        titleholderId,
      },
      {
        returning: true,
        where: {
          id: skuBatchId,
        },
      },
    )

    return updatedSkuBatch
  }),
}
