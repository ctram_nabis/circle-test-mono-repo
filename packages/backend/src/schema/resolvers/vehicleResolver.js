import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    createVehicle(input: CreateVehicleInput!) : CreateVehiclePayload
    updateVehicle(input: UpdateVehicleInput!) : UpdateVehiclePayload
    deleteVehicle(input: DeleteVehicleInput!) : DeleteVehiclePayload
  `
export const resolvers = {
  createVehicle: resolve(authUser)((obj, args, context) =>
    context.connections.logisticsDB.Vehicle.create(args.input).then((changedVehicle) => ({
      changedVehicle,
    })),
  ),
  updateVehicle: resolve(
    authUser,
    unmask('input.id'),
  )((obj, args, context) => {
    const { id, ...inputs } = args.input
    return context.connections.logisticsDB.Vehicle.update(inputs, {
      where: {
        id,
      },
    }).then(() => ({
      changedVehicle: context.connections.logisticsDB.Vehicle.findByPk(id),
    }))
  }),
  deleteVehicle: resolve(
    authUser,
    unmask('input.id'),
  )((obj, args, context) => {
    let data
    return context.connections.logisticsDB.Vehicle.findByPk(args.input.id)
      .then((res) => {
        data = res
        return context.connections.logisticsDB.Vehicle.destroy({
          where: {
            id: args.input.id,
          },
        })
      })
      .then(() => ({
        changedVehicle: data,
      }))
  }),
}
