import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    createDriver(input: CreateDriverInput!) : CreateDriverPayload
    updateDriver(input: UpdateDriverInput!) : UpdateDriverPayload
    deleteDriver(input: DeleteDriverInput!) : DeleteDriverPayload
  `
export const resolvers = {
  createDriver: resolve(authUser)(async (obj, args, context) => {
    const { Driver } = context.connections.logisticsDB
    const changedDriver = await Driver.create(args.input)

    return {
      changedDriver,
    }
  }),

  updateDriver: resolve(
    authUser,
    unmask('input.id'),
  )(async (obj, args, context) => {
    const { Driver } = context.connections.logisticsDB
    const { id, ...inputs } = args.input
    const driver = await Driver.findByPk(id)
    if (!driver) {
      throw new Error('Could not find driver with id')
    }

    const changedDriver = await driver.update(inputs)
    return { changedDriver }
  }),

  deleteDriver: resolve(
    authUser,
    unmask('input.id'),
  )(async (obj, args, context) => {
    const { Driver } = context.connections.logisticsDB
    const { id } = args.input

    const driver = await Driver.findByPk(id)
    if (!driver) {
      throw new Error('Could not find driver with id')
    }

    const { firstName, driversLicense } = driver

    await driver.destroy()

    return {
      changedDriver: {
        id,
        firstName,
        driversLicense,
      },
    }
  }),
}
