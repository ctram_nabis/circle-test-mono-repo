import { resolve } from '../graphql-helpers'
import { unmask } from '../graphql-helpers/contrib/relay'

export const queryOrderForManifest = (id, { logisticsDB }) =>
  logisticsDB.Order.findByPk(id, {
    include: [
      {
        association: logisticsDB.Order.Driver,
      },
      {
        association: logisticsDB.Order.Organization,
        include: [{ association: logisticsDB.Organization.Licenses, as: 'licenses' }, { association: 'owner' }],
      },
      {
        association: logisticsDB.Order.Vehicle,
      },
      {
        association: logisticsDB.Order.Site,
        include: [
          {
            association: logisticsDB.Site.LicensedLocation,
            include: [
              {
                as: 'licenses',
                association: logisticsDB.LicensedLocation.Licenses,
              },
            ],
          },
        ],
      },
      {
        association: logisticsDB.Order.LineItems,
        include: [
          {
            association: logisticsDB.LineItem.SkuBatch,
            paranoid: false,
            include: [
              {
                association: 'titleholder',
                include: [
                  {
                    association: 'titleholderOrganization',
                    include: [
                      {
                        association: 'owner',
                      },
                    ],
                  },
                  {
                    association: 'titleholderLicense',
                  },
                ],
              },
              {
                association: logisticsDB.SkuBatch.Batch,
                include: [
                  {
                    association: logisticsDB.Batch.License,
                  },
                ],
              },
              {
                association: logisticsDB.SkuBatch.Sku,
                include: [
                  {
                    association: logisticsDB.Sku.Organization,
                    include: [
                      {
                        association: 'owner',
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        association: logisticsDB.Order.LicensedLocations,
        include: [
          {
            association: logisticsDB.LicensedLocation.Licenses,
          },
        ],
      },
      {
        association: logisticsDB.Order.Warehouse,
        include: [
          { association: logisticsDB.Warehouse.License, as: 'license' },
          { association: logisticsDB.Warehouse.Site, as: 'site' },
        ],
      },
      {
        association: 'creator',
      },
      {
        association: 'assignee',
      },
    ],
    paranoid: false,
    nest: true,
  })

export const generateManifest = async (id, context) => {
  const { user } = context
  const { logisticsDB, googleDriveClient, sequelize } = context.connections

  const order = await queryOrderForManifest(id, { logisticsDB })
  const json = await googleDriveClient.resolvers.generateMyMans(order, logisticsDB, googleDriveClient, sequelize, user)

  return json
}

export const typeDefs = `
  generateManifest(orderId: ID!): GenerateManifestPayload
  getOrderSubscriber(orderId: ID!, organizationId: ID!): OrderSubscriber
`

export const resolvers = {
  generateManifest: resolve(unmask('orderId'))((_obj, args, context) => generateManifest(args.orderId, context)),
  getOrderSubscriber: resolve(unmask('orderId', 'organizationId'))(
    async (_obj, { orderId, organizationId }, context) => {
      // ⚠️ Return iff the user is a member of the organization
      const { organizations } = context.user

      const memberOfOrganizationIds = organizations.map((org) => org.id)

      const isMemberOfOrg = memberOfOrganizationIds.includes(organizationId)

      if (!isMemberOfOrg) {
        throw new Error('Not authorized to access this order subscriber')
      }

      const subscriber = await context.connections.logisticsDB.OrderSubscriber.findOne({
        where: {
          orderId,
          subscriberId: organizationId,
        },
      })

      return subscriber
    },
  ),
}
