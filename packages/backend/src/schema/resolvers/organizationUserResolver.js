import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    createOrganizationUser(input: CreateOrganizationUserInput!) : CreateOrganizationUserPayload
    updateOrganizationUser(input: UpdateOrganizationUserInput!) : UpdateOrganizationUserPayload
    deleteOrganizationUser(input: DeleteOrganizationUserInput!) : DeleteOrganizationUserPayload
  `
export const resolvers = {
  createOrganizationUser: resolve(
    authUser,
    unmask('input.userId', 'input.organizationId'),
  )(async (_obj, args, ctx) => {
    const createdOrgUser = await ctx.connections.logisticsDB.OrganizationUser.create(args.input)
    return createdOrgUser
  }),
  updateOrganizationUser: resolve(
    authUser,
    unmask('input.id', 'input.userId', 'input.organizationId'),
  )(async (_obj, args, ctx) => {
    const { id, ...inputs } = args.input
    const updatedOrganizationUser = await ctx.connections.logisticsDB.OrganizationUser.update(inputs, {
      where: {
        id,
      },
    })

    return { changedOrganizationUser: updatedOrganizationUser }
  }),
  deleteOrganizationUser: resolve(
    authUser,
    unmask('input.userId', 'input.organizationId'),
  )(async (_obj, args, { connections: { logisticsDB: db } }) => {
    // Retrieve the organization user reference
    const { userId, organizationId } = args.input
    const organizationUser = await db.OrganizationUser.findOne({
      where: { userId, organizationId },
    })

    // Delete the organization user reference
    await db.OrganizationUser.destroy({
      where: { userId, organizationId },
    })

    return { changedOrganizationUser: organizationUser }
  }),
}
