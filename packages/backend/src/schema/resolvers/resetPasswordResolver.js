import crypto from 'crypto'
import httpError from 'http-errors'
import jwt from 'jsonwebtoken'
import config from '../../config'
import logger from '../../logging'
import { generateToken } from '../../utils/auth'
import { resolve } from '../graphql-helpers'

export const resetPasswordTypes = `
    createTokenSendEmail(email: String) : String
    validateToken(email: String, token: String) : LoginUserPayload
    resetPassword(input: ResetPasswordInput!) : LoginUserPayload
  `
export const resetPassword = {
  createTokenSendEmail: resolve()(async (object, args, context) => {
    const { email: _email } = args
    const email = _email.toLowerCase()

    try {
      const user = await context.connections.logisticsDB.User.findOne({
        where: { email },
      })

      try {
        const [payload, options] = [
          {
            exp: Math.floor(Date.now() / 1000) + 86400, // 24 hours in seconds
            id: user.id,
            email: user.email,
          },
          {
            subject: user.id,
            audience: config.auth.audience.admin,
            issuer: config.auth.issuer,
          },
        ]

        const beforeHash = String(user.password) + String(user.createdAt)
        const secret = await crypto.createHmac('sha256', beforeHash).digest('hex')

        const token = await jwt.sign(payload, secret, options)

        try {
          const resetUrl = `${config.nabis.baseUrl}/resetpassword/${token}/${user.email}`
          const emailPayload = {
            to: (user && user.email) || 'philip+emailfail@nabis.com',
            from: 'noreply@nabis.com',
            subject: 'Password reset request',
            html: `
                <div>
                  <div>
                    <h2>Reset your password?</h2>
                    <p>If you requested a password reset, click the button below. If you didn't make this request, ignore this email</p>
                  </div>
                  <div>
                    <a href=${resetUrl}
                      style="
                        text-decoration:none;
                        background-color: #0EAE38;
                        border-style:none;
                        border:0;
                        padding:0;
                        margin:0;
                        font-size:12px;
                        font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;
                        color:#ffffff;
                        text-decoration:none;
                        border-radius:4px;
                        padding:8px 17px;
                        border:1px solid #0EAE38;
                        display:inline-block;
                        font-weight:bold"
                      target="_blank"
                    ><strong>Reset Password</strong></a>
                  </div>
                </div>
              `,
            templateId: '47afefdd-88f1-4ba1-b4c1-9e9e047e34ee',
          }

          await context.connections.sendGridClient.sendEmail(emailPayload)
        } catch (error) {
          logger.error('Failed to send password reset email', error)
          throw error
        }

        return token
      } catch (error) {
        logger.error('Failed to create token', error)
        throw error
      }
    } catch (error) {
      logger.error(`Could not find user with email: ${email}`, error)
      throw httpError(403, 'No user found with that email.')
    }
  }),
  validateToken: resolve()(async (obj, args, context) => {
    try {
      const { email: _email, token } = args
      const email = _email.toLowerCase()

      const _u = await context.connections.logisticsDB.User.findOne({
        where: { email },
      })

      const beforeHash = String(_u.password) + String(_u.createdAt)
      const secret = await crypto.createHmac('sha256', beforeHash).digest('hex')
      const decoded = jwt.verify(token, secret)

      const user = await context.connections.logisticsDB.User.findOne({
        where: { id: decoded.id },
      })

      return { token, user }
    } catch (error) {
      logger.error(error)
      throw error
    }
  }),
  resetPassword: resolve()(async (obj, { input }, context) => {
    const { email: _email, token, newPassword, confirmPassword } = input || {}
    const email = _email.toLowerCase()

    if (newPassword !== confirmPassword) {
      throw new Error(`Passwords do not match`)
    }

    try {
      const _u = await context.connections.logisticsDB.User.findOne({
        where: { email },
      })

      const beforeHash = String(_u.password) + String(_u.createdAt)
      const secret = crypto.createHmac('sha256', beforeHash).digest('hex')

      const decoded = jwt.verify(token, secret)

      const user = await context.connections.logisticsDB.User.findOne({
        where: { id: decoded.id },
      })

      await context.connections.logisticsDB.User.update(
        {
          password: newPassword,
        },
        {
          options: {
            newPassword,
          },
          where: { id: user.id },
        },
      )

      await context.connections.logisticsDB.User.findOne({
        where: { id: user.id },
      })

      const loggedInUser = await context.connections.logisticsDB.User.loginUser({
        email,
        password: newPassword,
      })

      const newToken = generateToken(loggedInUser.id, config.auth.audience.admin, loggedInUser.isAdmin)

      return { user: loggedInUser, token: newToken }
    } catch (error) {
      logger.error(error)
      throw error
    }
  }),
}
