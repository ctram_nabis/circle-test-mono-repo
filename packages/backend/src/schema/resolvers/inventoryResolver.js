import { resolve } from '../graphql-helpers'
import { unmask, authAdminUser } from '../graphql-helpers/contrib/relay'
import { unpackId } from '../graphql-helpers/contrib/relay/resolvers'
import logger from '../../logging'

const floor = (num) => (num < 0 ? 0 : num)

const decodeLineItemIds = ({ lineItemId, organizationId, skuBatchId }) => ({
  lineItemId: unpackId(lineItemId),
  organizationId: unpackId(organizationId),
  skuBatchId: unpackId(skuBatchId),
})

const findAllInventoryItems = async ({ db, skuBatchId, warehouseId }) => {
  const where = {
    skuBatchId,
    warehouseId,
  }

  const inventoryItems = await db.InventoryItem.findAll({
    include: [
      {
        association: db.InventoryItem.SkuBatch,
      },
      {
        association: db.InventoryItem.InventoryLedgerItems,
      },
    ],
    order: [['createdAt', 'asc']], // in case of dups get the first one
    paranoid: false,
    where,
  })

  return inventoryItems
}

const validateIntakeOutake = async ({ lineItems, index, db, warehouseId }) => {
  const lineItem = lineItems[index]
  // find inventoryItem using lineItem
  const { organizationId, skuBatchId } = decodeLineItemIds(lineItem)

  if (!lineItem.quantity) {
    throw new Error('LineItem quantity is missing')
  } else if (typeof lineItem.quantity !== 'number') {
    throw new Error('LineItem quantity must be a number')
  }
  if (!skuBatchId) {
    throw new Error(`skuBatchId not provided for lineItem, ${lineItem.code}`)
  }
  if (!organizationId) {
    throw new Error('organizationId not provided')
  }

  let existingInventoryItems
  try {
    existingInventoryItems = await findAllInventoryItems({
      db,
      skuBatchId,
      warehouseId,
    })
  } catch (error) {
    logger.error('inventoryResolver/validateIntakeOutake: An error occurred in findAllInventoryItems', { error })
    throw error
  }

  // note that this is impossible due to a database constraint!
  if (existingInventoryItems > 1) {
    throw new Error(`SkuBatch with id ${skuBatchId} has multiple Inventory Items for the same warehouse`)
  }
}

const executeRun = async ({ lineItems, index, db, warehouseId, transactionType, orderId, context }) => {
  const lineItem = lineItems[index]
  // find inventoryItem using lineItem
  const { lineItemId, organizationId, skuBatchId } = decodeLineItemIds(lineItem)

  let inventoryToUpdate

  const existingInventoryItems = await findAllInventoryItems({
    db,
    skuBatchId,
    warehouseId,
  })

  if (existingInventoryItems.length > 1) {
    // multiple inventory items found for the same warehouse, this is bad!
    throw new Error('error: duplicate inventory found')
  } else if (existingInventoryItems.length === 1) {
    // found an existing inventory item
    inventoryToUpdate = existingInventoryItems[0]
  } else {
    // if we find no inventory item, we need to create it `on intake`????

    // create inventory item
    const createdInventoryItem = await db.InventoryItem.create({
      allocated: 0,
      counted: 0,
      organizationId,
      promised: 0,
      skuBatchId,
      storageType: 'INVENTORY',
      warehouseId,
    })

    inventoryToUpdate = createdInventoryItem
  }

  const startCounted = inventoryToUpdate.counted
  const delta = Math.abs(lineItem.quantity)
  const endCounted =
    transactionType === 'INTAKE' ? inventoryToUpdate.counted + delta : inventoryToUpdate.counted - delta

  // update inventory with correct final count
  await inventoryToUpdate.update({
    counted: floor(endCounted),
  })

  // create ledger
  await db.InventoryLedgerItem.create({
    creatorId: context.user.id,
    endAllocated: 0,
    endCounted: floor(endCounted),
    endPromised: 0,
    inventoryItemId: inventoryToUpdate.id,
    lineItemId,
    notes: '',
    orderId,
    organizationId,
    startAllocated: 0,
    startCounted: floor(startCounted),
    startPromised: 0,
    transactionType,
  })

  return inventoryToUpdate
}

export const typeDefs = `
    correctInventoryItem(input: CorrectInventoryItemInput!) : UpdateInventoryItemPayload
    lineItemIntakeOuttake(input: LineItemIntakeOuttakeInput!): [InventoryItem]
  `
export const resolvers = {
  correctInventoryItem: resolve(
    authAdminUser,
    unmask('input.id'),
  )(async (_o, args, context) => {
    const db = context.connections.logisticsDB

    const { id, endCounted, transactionType, notes } = args.input

    const initialInventoryItem = await db.InventoryItem.findByPk(id)
    if (!initialInventoryItem) {
      throw new Error('InventoryItemId does not exist in database')
    }
    const { counted: startCounted } = initialInventoryItem

    // Error message, as it is below, is required for inventory item correction unit test to pass
    if (typeof notes === 'string' ? !notes.trim() : !notes) {
      throw new Error('notes are required')
    }

    // Update InventoryItem sync
    await db.InventoryItem.update(
      {
        counted: endCounted,
      },
      {
        where: { id },
      },
    )

    // Find updated InventoryItem to get rest of the fields sync
    const updatedInventoryItem = await db.InventoryItem.findOne({
      paranoid: false,
      where: { id },
    })

    // Create InventoryLedgerItem sync
    await db.InventoryLedgerItem.create({
      creatorId: context.user.id,
      endCounted,
      inventoryItemId: updatedInventoryItem.id,
      notes,
      organizationId: updatedInventoryItem.organizationId,
      startCounted,
      transactionType,
    })

    return { changedInventoryItem: updatedInventoryItem }
  }),
  lineItemIntakeOuttake: resolve(
    authAdminUser,
    unmask('input.warehouseId', 'input.orderId'),
  )(async (_o, args, context) => {
    const db = context.connections.logisticsDB

    const { transactionType, lineItems: _lineItems, warehouseId, orderId } = args.input
    // ensure same order on ledger as frontend will expect
    const lineItems = _lineItems.sort((a, b) => {
      const aId = a.skuBatchId
      const bId = b.skuBatchId
      return aId === bId ? (a.lineItemId > b.lineItemId ? 1 : -1) : aId && bId && aId > bId ? 1 : -1
    })

    const warehouse = await db.Warehouse.findByPk(warehouseId, { include: [{ association: db.Warehouse.Site }] })

    if (!warehouse) {
      throw new Error('WarehouseId does not exist in the database')
    }

    const getUpdateOrderFlagValue = () => {
      if (transactionType === 'INTAKE') {
        return { intaken: true }
      }
      if (transactionType === 'OUTTAKE') {
        return { outtaken: true }
      } else {
        const errorMessage = `transactionType must be either INTAKE or OUTTAKE, got ${transactionType} instead`
        logger.error(errorMessage)
        throw new Error(errorMessage)
      }
    }

    const updateOrderFlagValue = getUpdateOrderFlagValue()

    try {
      const updatedInventoryItems = []
      for (let i = 0, len = lineItems.length; i < len; i += 1) {
        // eslint-disable-next-line no-useless-catch
        // validates that the intake will work. if something is wrong, it throws an error
        // eslint-disable-next-line no-await-in-loop
        await validateIntakeOutake({
          context,
          db,
          index: i,
          lineItems,
          orderId,
          transactionType,
          warehouseId,
        })
      }

      for (let i = 0, len = lineItems.length; i < len; i += 1) {
        // we want this to execute synchronously
        // eslint-disable-next-line no-await-in-loop
        const inventoryItem = await executeRun({
          context,
          db,
          index: i,
          lineItems,
          orderId,
          transactionType,
          warehouseId,
        })
        updatedInventoryItems.push(inventoryItem)
      }

      // update intaken, outtaken flags on order
      await db.Order.update(updateOrderFlagValue, { where: { id: orderId } })
      await db.OrderChange.create({
        description: `Performed ${transactionType.toLowerCase()} ${transactionType === 'OUTTAKE' ? 'from' : 'to'} ${
          warehouse.site.name
        }`,
        orderId,
        submitterId: context.user.id,
      })
      return updatedInventoryItems
    } catch (error) {
      logger.error(error)
      throw error
    }
  }),
}
