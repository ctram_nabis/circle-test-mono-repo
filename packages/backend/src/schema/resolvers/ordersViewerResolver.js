import moment from 'moment'
import { Op } from 'sequelize'

import config from '../../config'
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import wrapResultsWithPageInfo from '../utils/wrapResultsWithPageInfo'
import getOrderAndPaginateOpts from '../utils/getOrderAndPaginateOpts'

export const ordersViewerTypes = `
    allOrders (
      organizationId: ID!
      number: String
      status: OrderStatusEnum
      paymentStatus: OrderPaymentStatusEnum
      start: DateTime
      end: DateTime
      site: String
      creator: String
      pageInfo: PageInfoInput
    ): AllOrdersPayload
  `
export const ordersViewer = {
  allOrders: resolve(
    authUser,
    unmask('organizationId'),
  )(async (obj, args, context) => {
    const db = context.connections.logisticsDB
    // set creatorId & assigneeId based on context
    const { user } = context

    if (
      !user.organizations ||
      !user.organizations.length ||
      !user.organizations.find((o) => o.id === args.organizationId)
    ) {
      return []
    }

    let getIds = []
    const and = { isArchived: false }
    if (user.roles.length && (user.roles[0].name === 'user' || user.roles[0].name === 'viewer')) {
      and.organizationId = args.organizationId
      and[Op.or] = [{ creatorId: user.id }, { assigneeId: user.id }]
    } else {
      and[Op.or] = [
        {
          organizationId:
            context.user.isAdmin && !args.organizationId
              ? {
                  [Op.ne]: config.nabis.demoOrganizationId,
                }
              : args.organizationId,
        },
      ]

      getIds = await db.OrderSubscriber.findAll({
        where: { subscriberId: args.organizationId },
      }).then((orderSubs) => orderSubs.map((o) => o.orderId))
    }
    const opts = { where: { ...and } }

    // This should be refactored out as resolver middleware
    const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(args.pageInfo)

    if (getIds && getIds.length) {
      opts.where[Op.or] = opts.where[Op.or] || []
      opts.where[Op.or].push({ id: { [Op.in]: getIds } })
    }

    Object.keys(args).forEach((arg) => {
      switch (arg) {
        case 'end':
        case 'site':
        case 'start':
        case 'number':
        case 'status':
        case 'creator':
        case 'paymentStatus':
          {
            opts.where[Op.and] = opts.where[Op.and] || []
            const ands = opts.where[Op.and]

            if (arg === 'start') {
              ands.push({
                date: { [Op.gte]: moment(args[arg]).startOf('day') },
              })
            } else if (arg === 'end') {
              ands.push({
                date: { [Op.lte]: moment(args[arg]).endOf('day') },
              })
            } else if (arg === 'number') {
              ands.push({ number: { [Op.eq]: Number(args[arg]) } })
            } else if (arg === 'creator') {
              opts.include = opts.include || []
              opts.include.push({
                association: 'creator',
                where: {
                  [Op.or]: {
                    email: { [Op.iLike]: `%${args[arg]}%` },
                    lastName: { [Op.iLike]: `%${args[arg]}%` },
                    firstName: { [Op.iLike]: `%${args[arg]}%` },
                  },
                },
              })
            } else if (arg === 'site') {
              opts.include = opts.include || []
              opts.include.push({
                association: 'site',
                where: {
                  name: { [Op.iLike]: `%${args[arg]}%` },
                },
              })
            } else ands.push({ [arg]: { [Op.eq]: args[arg] } })
          }
          break
        default:
      }
    })

    if (opts.where[Op.and] && !opts.where[Op.and].length) delete opts.where[Op.and]
    return db.Order.findAll({
      ...opts,
      ...orderOpts,
      ...paginateOpts,
    }).then((results) => {
      const pageInfo = wrapResultsWithPageInfo(db.Order, { ...opts, ...orderOpts }, args)
      return {
        results,
        pageInfo,
      }
    })
  }),
}
