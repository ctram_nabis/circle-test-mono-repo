import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    createRole(input: CreateRoleInput!) : CreateRolePayload
    updateRole(input: UpdateRoleInput!) : UpdateRolePayload
    deleteRole(input: DeleteRoleInput!) : DeleteRolePayload
  `
export const resolvers = {
  createRole: resolve(
    authUser,
    unmask('input.organizationId'),
  )((obj, args, context) =>
    context.connections.logisticsDB.Role.create(args.input).then((changedRole) => ({
      changedRole,
    })),
  ),
  updateRole: resolve(
    authUser,
    unmask('input.id', 'input.organizationId'),
  )((obj, args, context) => {
    const { id, ...inputs } = args.input
    return context.connections.logisticsDB.Role.update(inputs, {
      where: {
        id,
      },
    }).then(() => ({
      changedRole: context.connections.logisticsDB.Role.findByPk(id),
    }))
  }),
  deleteRole: resolve(
    authUser,
    unmask('input.id'),
  )((obj, args, context) => {
    let data
    return context.connections.logisticsDB.Role.findByPk(args.input.id)
      .then((res) => {
        data = res
        return context.connections.logisticsDB.Role.destroy({
          where: {
            id: args.input.id,
          },
        })
      })
      .then(() => ({
        changedRole: data,
      }))
  }),
}
