import { Op } from 'sequelize'

// import logger from '../../logging';
import { resolve } from '../graphql-helpers'
import { authUser, unmask, authAdminUser } from '../graphql-helpers/contrib/relay'

import getOrderAndPaginateOpts from '../utils/getOrderAndPaginateOpts'
import wrapResultsWithPageInfo from '../utils/wrapResultsWithPageInfo'

const ALL_ADMIN_SKUS = async (_o, args, context) => {
  const { organizationId, search, isArchived, pageInfo } = args
  const { Sku } = context.connections.logisticsDB

  const opts = { where: { isArchived: isArchived || false } }
  if (organizationId) {
    opts.where.organizationId = organizationId
  }
  if (search && search.length > 0) {
    opts.where[Op.or] = [
      { name: { [Op.iLike]: `%${search}%` } },
      { code: { [Op.iLike]: `%${search}%` } },
      // { '$batch.code$': { [Op.iLike]: `%${search}%` } },
    ]

    // not working :/
    // opts.include = [{
    //   model: context.connections.logisticsDB.Batch,
    //   as: 'batch',
    //   require: true,
    // }];
  }
  const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(pageInfo)
  const results = await Sku.findAll({
    ...opts,
    ...orderOpts,
    ...paginateOpts,
  })
  const _pageInfo = wrapResultsWithPageInfo(
    Sku,
    {
      ...opts,
      ...orderOpts,
    },
    args,
  )
  return {
    pageInfo: _pageInfo,
    results,
  }
}

export const skuViewerTypes = `
    allSkus(
      search: String
      organizationId: ID
      isArchived: Boolean
      pageInfo: PageInfoInput
      isInventoryTab: Boolean = false
    ): SkuInfo

    allAdminSkus (
      search: String
      organizationId: ID
      isArchived: Boolean
      pageInfo: PageInfoInput
    ): SkuInfo

    accessibleSkus(orgId: ID!): [Sku]
  `
export const skuViewer = {
  accessibleSkus: resolve(
    authUser,
    unmask('orgId'),
  )((_o, args, context) => {
    const db = context.connections.logisticsDB

    // this repeats a lot of code from accessibleBrands, but gotta ship it,
    // so YOLO. TODO: make it DRY.
    const findSubBrandIds = db.OrganizationOrganization.findAll({
      where: { superorganizationId: args.orgId },
    })

    const ensureSubBrandArray = (orgorgs) =>
      orgorgs.length >= 2 ? orgorgs.map((orgorg) => orgorg.suborganizationId) : [args.orgId]

    const findSubBrands = (subBrandIds) =>
      db.Organization.findAll({
        where: { id: { [Op.in]: subBrandIds } },
      })

    const findSkusForSubBrands = (subBrands) =>
      db.Sku.findAll({
        where: {
          organizationId: {
            [Op.in]: subBrands.map((b) => b.id),
          },
        },
      })

    return findSubBrandIds
      .then(ensureSubBrandArray)
      .then(findSubBrands)
      .then(findSkusForSubBrands)
  }),
  allAdminSkus: resolve(authAdminUser, unmask('organizationId'))((o, args, ctx) => ALL_ADMIN_SKUS(o, args, ctx)),
  allSkus: resolve(
    authUser,
    unmask('organizationId'),
  )(async (o, args, context) => {
    const { organizationId, search, isArchived, pageInfo } = args
    const { Sku } = context.connections.logisticsDB
    const { user } = context

    if (user.isAdmin && args.isInventoryTab) {
      return ALL_ADMIN_SKUS(o, args, context)
    }

    if (!user.isAdmin && !args.organizationId) {
      throw new Error('You must provide an organizationId unless you are a nabis admin')
    }

    const opts = { where: { isArchived: isArchived || false } }
    if (organizationId) {
      opts.where.organizationId = organizationId
    }
    if (search && search.length > 0) {
      opts.where[Op.or] = [{ name: { [Op.iLike]: `%${search}%` } }, { code: { [Op.iLike]: `%${search}%` } }]
    }
    const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(pageInfo)
    const results = await Sku.findAll({
      ...opts,
      ...orderOpts,
      ...paginateOpts,
    })
    const _pageInfo = wrapResultsWithPageInfo(
      Sku,
      {
        ...opts,
        ...orderOpts,
      },
      args,
    )
    return {
      pageInfo: _pageInfo,
      results,
    }
  }),
}
