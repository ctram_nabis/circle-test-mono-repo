import moment from 'moment'
import { Op } from 'sequelize'

import config from '../../config'
import { resolve } from '../graphql-helpers'
import wrapResultsWithPageInfo from '../utils/wrapResultsWithPageInfo'
import getOrderAndPaginateOpts from '../utils/getOrderAndPaginateOpts'
import { authAdminUser, unmask } from '../graphql-helpers/contrib/relay'
import { ORDER_ACTIONS } from '../../constants'

module.exports = {
  adminOrdersViewerTypes: `
  allAdminOrders (
    organizationId: ID
    orderBy: String
    direction: String
    search: String
    action: OrderActionEnum
    status: OrderStatusEnum
    paymentStatus: OrderPaymentStatusEnum
    warehouseId: ID
    driverId: ID
    vehicleId: ID
    start: DateTime
    end: DateTime
    pageInfo: PageInfoInput
    transactionTypes: [String]
  ): AllAdminOrdersPayload
  `,
  adminOrdersViewer: {
    allAdminOrders: resolve(
      authAdminUser,
      unmask('organizationId', 'warehouseId', 'driverId', 'vehicleId'),
    )(async (_obj, args, context) => {
      const db = context.connections.logisticsDB

      // This should be refactored out as resolver middleware
      const opts = {
        paranoid: false,
        where: {
          isArchived: false,
          organizationId:
            context.user.isAdmin && !args.organizationId
              ? {
                  [Op.ne]: config.nabis.demoOrganizationId,
                }
              : args.organizationId,
        },
      }

      Object.keys(args).forEach((arg) => {
        switch (arg) {
          case 'end':
          case 'start':
          case 'action':
          case 'search':
          case 'status':
          case 'driverId':
          case 'vehicleId':
          case 'warehouseId':
          case 'paymentStatus':
          case 'transactionTypes':
            {
              opts.where[Op.and] = opts.where[Op.and] || []
              const ands = opts.where[Op.and]

              if (arg === 'start') {
                ands.push({
                  date: {
                    [Op.gte]: moment(args.start)
                      .utc()
                      .startOf('day'),
                  },
                })
              } else if (arg === 'end') {
                ands.push({
                  date: {
                    [Op.lte]: moment(args.end)
                      .utc()
                      .endOf('day'),
                  },
                })
              } else if (arg === 'transactionTypes') {
                if (args.transactionTypes.includes('INTAKE')) {
                  ands.push({ intaken: true })
                }
                if (args.transactionTypes.includes('OUTTAKE')) {
                  ands.push({ outtaken: true })
                }
                if (args.transactionTypes.includes('EDITED')) {
                  ands.push({ edited: true })
                }
                if (args.transactionTypes.includes('NOT_INTAKE')) {
                  ands.push({ intaken: false })
                }
                if (args.transactionTypes.includes('NOT_OUTTAKE')) {
                  ands.push({ outtaken: false })
                }
                if (args.transactionTypes.includes('NOT_EDITED')) {
                  ands.push({ edited: false })
                }

                // determine which Orders we want to display based on the filter
                const desiredTransactionTypes = []
                const outtakeOnlySupportedActions = [
                  ORDER_ACTIONS.DELIVERY_TO_BRAND,
                  ORDER_ACTIONS.DELIVERY_TO_RETAILER,
                  ORDER_ACTIONS.PICKUP_FROM_NABIS,
                ]

                const intakeOnlySupportedActions = [
                  ORDER_ACTIONS.PICKUP_FROM_BRAND,
                  ORDER_ACTIONS.RETURN_FROM_RETAILER,
                  ORDER_ACTIONS.DROPOFF_TO_NABIS,
                ]

                const intakeAndOuttakeSupportedActions = [ORDER_ACTIONS.INTERNAL_TRANSFER, ORDER_ACTIONS.OTHER]

                if (
                  (args.transactionTypes.indexOf('INTAKE') > -1 || args.transactionTypes.indexOf('NOT_INTAKE') > -1) &&
                  (args.transactionTypes.indexOf('OUTTAKE') > -1 || args.transactionTypes.indexOf('NOT_OUTTAKE') > -1)
                ) {
                  desiredTransactionTypes.push(...intakeAndOuttakeSupportedActions)
                  desiredTransactionTypes.push(...intakeOnlySupportedActions)
                  desiredTransactionTypes.push(...outtakeOnlySupportedActions)
                } else if (
                  args.transactionTypes.indexOf('OUTTAKE') > -1 ||
                  args.transactionTypes.indexOf('NOT_OUTTAKE') > -1
                ) {
                  desiredTransactionTypes.push(...intakeAndOuttakeSupportedActions)
                  desiredTransactionTypes.push(...outtakeOnlySupportedActions)
                } else if (
                  args.transactionTypes.indexOf('INTAKE') > -1 ||
                  args.transactionTypes.indexOf('NOT_INTAKE') > -1
                ) {
                  desiredTransactionTypes.push(...intakeAndOuttakeSupportedActions)
                  desiredTransactionTypes.push(...intakeOnlySupportedActions)
                } else {
                  // the catchall supports every action
                  desiredTransactionTypes.push(
                    ...intakeAndOuttakeSupportedActions,
                    ...intakeOnlySupportedActions,
                    ...outtakeOnlySupportedActions,
                  )
                }

                ands.push({
                  action: { [Op.in]: desiredTransactionTypes },
                })
              } else if (arg === 'search') {
                const searchingNumber = Number(args.search) && !Number.isNaN(Number(args.search))
                if (searchingNumber) {
                  ands.push({ number: { [Op.eq]: Number(args.search) } })
                } else {
                  ands.push({
                    [Op.or]: {
                      '$organization.doingBusinessAs$': { [Op.iLike]: `%${args.search}%` },
                      '$organization.name$': { [Op.iLike]: `%${args.search}%` },
                      '$site.name$': { [Op.iLike]: `%${args.search}%` },
                      name: { [Op.iLike]: `%${args.search}%` },
                    },
                  })

                  opts.include = opts.include || []
                  opts.include.push(
                    {
                      as: 'organization',
                      model: db.Organization,
                      required: true,
                    },
                    {
                      as: 'site',
                      model: db.Site,
                      required: true,
                    },
                  )
                }
              } else {
                ands.push({ [arg]: { [Op.eq]: args[arg] } })
              }
            }
            break
          default:
        }
      })

      if (opts.where[Op.and] && !opts.where[Op.and].length) {
        delete opts.where[Op.and]
      }

      const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(args.pageInfo)

      const results = await db.Order.findAll({
        ...opts,
        ...orderOpts,
        ...paginateOpts,
      })

      const pageInfo = await wrapResultsWithPageInfo(db.Order, { ...opts, ...orderOpts }, args)

      return {
        pageInfo,
        results,
      }
    }),
  },
}
