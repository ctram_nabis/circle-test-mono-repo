import { Op } from 'sequelize'
import { fromGlobalId } from 'graphql-relay'

import logger from '../../logging'
import { resolve } from '../graphql-helpers'
import { authUser } from '../graphql-helpers/contrib/relay'
import wrapResultsWithPageInfo from '../utils/wrapResultsWithPageInfo'
import getOrderAndPaginateOpts from '../utils/getOrderAndPaginateOpts'

export const draftsViewerTypes = `
    allDrafts (
      organizationId: ID!
      pageInfo: PageInfoInput
    ): AllDraftsPayload
  `
export const draftsViewer = {
  allDrafts: resolve(authUser)(async (obj, args, context) => {
    logger.info(`Request ${context.id}`, 'In viewer.allDrafts()')
    const db = context.connections.logisticsDB
    const { user } = context

    if (
      !user.organizations ||
      !user.organizations.length ||
      !user.organizations.find((o) => o.id === fromGlobalId(args.organizationId).id)
    ) {
      return []
    }

    const and = {
      [Op.or]: [
        { organizationId: fromGlobalId(args.organizationId).id },
        { body: { organization: { id: args.organizationId } } },
      ],
    }

    if (user.roles.length && user.roles[0].name === 'user') {
      and.creatorId = user.id
    } else if (user.roles.length && user.roles[0].name === 'viewer') {
      return []
    }

    const opts = { where: { ...and } }
    const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(args.pageInfo)
    return db.Draft.findAll({
      ...opts,
      ...orderOpts,
      ...paginateOpts,
    }).then(async (results) => {
      const pageInfo = await wrapResultsWithPageInfo(db.Draft, { ...opts, ...orderOpts }, args)

      return {
        results,
        pageInfo,
      }
    })
  }),
}
