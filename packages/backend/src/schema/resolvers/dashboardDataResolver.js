import { resolve } from '../graphql-helpers'
import { unmask, authUser } from '../graphql-helpers/contrib/relay'
import { Op } from 'sequelize'

export const dashboardDataTypes = `getDashboardData(organizationId: ID!): DashboardData`

export const dashboardData = {
  getDashboardData: resolve(
    authUser,
    unmask('organizationId'),
  )(async (_obj, args, context) => {
    const { organizationId } = args
    const { Order } = context.connections.logisticsDB

    const totalGMVCollected = await Order.sum('gmvCollected', {
      where: {
        organizationId,
        paymentStatus: {
          [Op.in]: ['COD_PAID', 'NET_TERMS_PAID'],
        },
      },
    })

    const totalExciseTaxCollected = await Order.sum('exciseTaxCollected', {
      where: {
        organizationId,
        paymentStatus: {
          [Op.in]: ['COD_PAID', 'NET_TERMS_PAID'],
        },
      },
    })

    const totalDelivered = await Order.count({
      where: {
        organizationId,
        status: {
          [Op.in]: ['DELIVERED', 'DELIVERED_WITH_EDITS'],
        },
      },
    })

    const totalUnscheduled = await Order.count({
      where: {
        organizationId,
        status: 'UNSCHEDULED',
      },
    })

    const totalScheduled = await Order.count({
      where: {
        organizationId,
        status: 'SCHEDULED',
      },
    })

    return {
      totalDelivered,
      totalScheduled,
      totalUnscheduled,
      totalGMVCollected,
      totalExciseTaxCollected,
    }
  }),
}
