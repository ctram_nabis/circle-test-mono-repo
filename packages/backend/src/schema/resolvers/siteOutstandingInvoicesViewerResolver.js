import { Op } from 'sequelize'

import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import idx from 'idx'

export const siteOutstandingInvoicesViewerTypes = `
    getAllSiteOutstandingInvoices(id:ID!): [Order]
  `
export const siteOutstandingInvoicesViewer = {
  getAllSiteOutstandingInvoices: resolve(
    authUser,
    unmask('id'),
  )((obj, args, context) => {
    const db = context.connections.logisticsDB

    const findSite = db.Site.findByPk(args.id, {
      include: [
        {
          association: db.Site.LicensedLocation,
        },
      ],
    }).then((site) => {
      if (!site) return null
      return {
        location: idx(site, (_) => _.licensedLocation.id),
      }
    })

    return findSite.then((site) => {
      if (!site) return []

      return db.Order.findAll({
        where: {
          [Op.and]: [
            {
              paymentStatus: 'UNPAID',
            },
            {
              status: ['DELIVERED', 'DELIVERED_WITH_EDITS'],
            },
            {
              action: 'DELIVERY_TO_RETAILER',
            },
            {
              daysTillPaymentDue: {
                [Op.not]: null,
              },
            },
          ],
        },
        include: [
          {
            association: db.Order.Site,
            include: [
              {
                association: db.Site.LicensedLocation,
              },
            ],
          },
        ],
      }).then((os) =>
        os.filter((o) => o && o.site && o.site.licensedLocation && o.site.licensedLocation.id === site.location),
      )
    })
  }),
}
