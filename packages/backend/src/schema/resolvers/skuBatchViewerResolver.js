import { Op } from 'sequelize'

import { resolve } from '../graphql-helpers'
import { authUser, unmask, authAdminUser } from '../graphql-helpers/contrib/relay'

import getOrderAndPaginateOpts from '../utils/getOrderAndPaginateOpts'
import wrapResultsWithPageInfo from '../utils/wrapResultsWithPageInfo'

const ALL_ADMIN_SKUBATCHES = (_o, args, context) => {
  const db = context.connections.logisticsDB
  const { isArchived, organizationId, pageInfo, search } = args
  const skuWhere = {}
  const opts = {
    include: [
      {
        as: 'sku',
        model: db.Sku,
        required: true,
      },
      {
        as: 'batch',
        model: db.Batch,
      },
    ],
    returning: true,
  }

  if (isArchived) {
    opts.where = {
      [Op.or]: [{ deletedAt: { [Op.ne]: null } }, { '$sku.isArchived$': true }],
    }
    opts.paranoid = false
  } else {
    skuWhere.isArchived = false
    opts.paranoid = true
  }

  if (organizationId) {
    skuWhere.organizationId = organizationId
  }

  if (search && search.length > 0) {
    skuWhere[Op.or] = [
      {
        name: { [Op.iLike]: `%${search}%` },
      },
      {
        code: { [Op.iLike]: `%${search}%` },
      },
    ]
  }

  opts.include[0].where = skuWhere
  const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(pageInfo)
  return db.SkuBatch.findAll({
    ...opts,
    ...orderOpts,
    ...paginateOpts,
  }).then((results) => {
    const _pageInfo = wrapResultsWithPageInfo(
      db.SkuBatch,
      {
        ...opts,
        ...orderOpts,
      },
      args,
    )
    return {
      pageInfo: _pageInfo,
      results,
    }
  })
}

export const skuBatchViewerTypes = `
    allSkuBatches(
      isArchived: Boolean
      organizationId: ID
      pageInfo: PageInfoInput
      search: String
      isInventoryTab: Boolean
    ): SkuBatchInfo

    allAdminSkuBatches (
      isArchived: Boolean
      pageInfo: PageInfoInput
      organizationId: ID
      search: String
    ): SkuBatchInfo

    accessibleSkuBatches(
      brandId: ID
      orgId: ID!
      pageInfo: PageInfoInput
      search: String
    ): SkuBatchInfo
  `
export const skuBatchViewer = {
  accessibleSkuBatches: resolve(
    authUser,
    unmask('orgId', 'brandId'),
  )(async (_o, args, context) => {
    const { brandId, orgId, search, pageInfo } = args
    const db = context.connections.logisticsDB

    let subBrandIds
    if (!brandId) {
      const subBrands = await db.OrganizationOrganization.findAll({
        where: { superorganizationId: orgId },
      })

      subBrandIds = subBrands.length >= 2 ? subBrands.map((sub) => sub.suborganizationId) : [orgId]
    } else {
      subBrandIds = [brandId]
    }

    const skus = {
      as: 'sku',
      model: db.Sku,
      where: {
        isArchived: false,
        organizationId: {
          [Op.in]: subBrandIds,
        },
      },
    }

    if (search && search.length > 0) {
      skus.where[Op.or] = [
        {
          name: { [Op.iLike]: `%${search}%` },
        },
        {
          code: { [Op.iLike]: `%${search}%` },
        },
      ]
    }

    const opts = {
      include: [skus],
      // paranoid: true
      returning: true,
    }
    const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(pageInfo)

    return db.SkuBatch.findAll({
      ...opts,
      ...orderOpts,
      ...paginateOpts,
    }).then((results) => {
      const _pageInfo = wrapResultsWithPageInfo(
        db.SkuBatch,
        {
          ...opts,
          ...orderOpts,
        },
        args,
      )

      return {
        pageInfo: _pageInfo,
        results,
      }
    })
  }),

  allAdminSkuBatches: resolve(
    authAdminUser,
    unmask('organizationId'),
  )((obj, args, context) => ALL_ADMIN_SKUBATCHES(obj, args, context)),

  allSkuBatches: resolve(
    authUser,
    unmask('organizationId'),
  )((o, args, context) => {
    const { user } = context
    if (user.isAdmin && args.isInventoryTab) {
      return ALL_ADMIN_SKUBATCHES(o, args, context)
    }

    if (!user.isAdmin && !args.organizationId) {
      throw new Error('You must provide an organizationId unless you are a nabis admin')
    }

    const db = context.connections.logisticsDB
    const { isArchived, organizationId, pageInfo, search } = args

    const opts = {
      include: [
        {
          as: 'sku',
          model: db.Sku,
          required: true,
        },
        {
          as: 'batch',
          model: db.Batch,
        },
      ],
      returning: true,
    }

    if (organizationId) {
      opts.include[0].where = {
        organizationId,
      }
    }

    if (isArchived) {
      opts.where = {
        [Op.or]: [{ deletedAt: { [Op.ne]: null } }, { '$sku.isArchived$': true }],
      }
      opts.paranoid = false
    } else {
      opts.include[0].where.isArchived = false
      opts.paranoid = true
    }

    if (search && search.length > 0) {
      opts.include[0].where[Op.or] = [{ name: { [Op.iLike]: `%${search}%` } }, { code: { [Op.iLike]: `%${search}%` } }]
    }

    const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(pageInfo)
    return db.SkuBatch.findAll({
      ...opts,
      ...orderOpts,
      ...paginateOpts,
    }).then((results) => {
      const _pageInfo = wrapResultsWithPageInfo(
        db.SkuBatch,
        {
          ...opts,
          ...orderOpts,
        },
        args,
      )
      return {
        pageInfo: _pageInfo,
        results,
      }
    })
  }),
}
