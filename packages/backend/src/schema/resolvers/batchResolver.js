import { createWriteStream } from 'fs'
import uuid from 'uuid/v1'

import logger from '../../logging'
import { resolve as gqlResolve, unpackId } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

import config from '../../config'

export const typeDefs = `
    uploadBatchFile(inputFile: Upload!, organizationId: ID!): String
    createBatch(input: CreateBatchInput!): Batch
    bulkUpdateBatchStatus(batches: [UpdateBatchInput]!, targetStatus: BatchStatusEnum): Boolean
    deleteBatch(id: ID!): Boolean
    editBatch(input: UpdateBatchInput!): Boolean
    restoreBatch(batchId: ID!): Batch
  `
export const resolvers = {
  uploadBatchFile: gqlResolve(
    authUser,
    unmask('organizationId'),
  )((_obj, { inputFile, organizationId }, context) => {
    logger.info(`Request ${context.id}`, 'In mutation.uploadBatch()')

    const gd = context.connections.googleDriveClient

    const writeToFileSystem = ({ createReadStream, filename }) =>
      new Promise((resolve) => {
        const tempName = `${uuid()}-${filename}`
        const path = `${config.workingDir}/templates/temp/${tempName}`
        const done = () => resolve({ path, name: filename })
        createReadStream()
          .pipe(createWriteStream(path))
          .on('finish', done)
      })

    const uploadToGDrive = ({ path, name }) =>
      gd.upsertFileAndFolder(path, name, organizationId, context.connections.logisticsDB)

    const receiveFile = inputFile

    if (receiveFile) {
      return receiveFile.then(writeToFileSystem).then(uploadToGDrive)
    } else {
      throw new Error('No file received')
    }
  }),

  createBatch: gqlResolve(
    authUser,
    unmask('input.organizationId', 'input.licenseId', 'input.skuId'),
  )(async (_obj, { input }, context) => {
    const { Batch } = context.connections.logisticsDB

    const batch = await Batch.create(input)

    return batch
  }),

  deleteBatch: gqlResolve(
    authUser,
    unmask('id'),
  )(async (_obj, { id }, context) => {
    logger.info(`Request ${context.id}`, 'In mutation.deleteBatch()')
    const db = context.connections.logisticsDB
    const [archivedBatch] = await db.Batch.update(
      { isArchived: true },
      {
        where: { id },
      },
    )
    return archivedBatch === 1
  }),

  bulkUpdateBatchStatus: gqlResolve(authUser)(async (_obj, { batches, targetStatus }, context) => {
    logger.info(`Request ${context.id}`, 'In mutation.bulkUpdateBatch()')
    const { Batch } = context.connections.logisticsDB
    const toUpdate = batches.filter((batch) => batch.status !== targetStatus)
    const promises = toUpdate.map((batch) =>
      Batch.update(
        {
          status: targetStatus,
        },
        {
          where: {
            id: unpackId(batch.id),
          },
        },
      ),
    )
    await Promise.all(promises)
    return true
  }),

  editBatch: gqlResolve(
    authUser,
    unmask('input.id', 'input.licenseId', 'input.skuBatchId'),
  )(async (_obj, { input }, context) => {
    logger.info(`Request ${context.id}`, 'In mutation.editBatch()')

    const { id, ...inputs } = input

    const { Batch } = context.connections.logisticsDB

    await Batch.update(inputs, { where: { id } })

    return true
  }),

  restoreBatch: gqlResolve(
    authUser,
    unmask('batchId'),
  )(async (_o, { batchId }, ctx) => {
    const { Batch } = ctx.connections.logisticsDB

    const batch = await Batch.findByPk(batchId, { paranoid: false })

    if (!batch) {
      throw new Error('Batch not found!')
    } else {
      if (!batch.isArchived) {
        throw new Error('Batch is already restored!')
      } else {
        await batch.update({ isArchived: false })
        return batch
      }
    }
  }),
}
