import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

export const typeDefs = `
    createLicense(input: CreateLicenseInput!) : CreateLicensePayload
    updateLicense(input: UpdateLicenseInput!) : UpdateLicensePayload
    deleteLicense(input: DeleteLicenseInput!) : DeleteLicensePayload
  `
export const resolvers = {
  createLicense: resolve(
    authUser,
    unmask('input.organizationId', 'input.siteId', 'input.licensedLocationId'),
  )(async (obj, args, context) => {
    const license = await context.connections.logisticsDB.License.create(args.input)

    if (args.input && args.input.organizationId) {
      // create a titleholder for the new org license
      await context.connections.logisticsDB.Titleholder.create({
        organizationId: args.input.organizationId,
        titleholderLicenseId: license.id,
        type: 'LICENSE',
      })
    }

    return { changedLicense: license }
  }),
  updateLicense: resolve(
    authUser,
    unmask('input.id', 'input.organizationId', 'input.licensedLocationId'),
  )((obj, args, context) => {
    const { id, ...inputs } = args.input
    return context.connections.logisticsDB.License.update(inputs, {
      where: {
        id,
      },
    }).then(() => ({
      changedLicense: context.connections.logisticsDB.License.findByPk(id),
    }))
  }),
  deleteLicense: resolve(
    authUser,
    unmask('input.id'),
  )((obj, args, context) => {
    let data
    return context.connections.logisticsDB.License.findByPk(args.input.id)
      .then((res) => {
        data = res
        return context.connections.logisticsDB.License.destroy({
          where: {
            id: args.input.id,
          },
        })
      })
      .then(() => ({
        changedLicense: data,
      }))
  }),
}
