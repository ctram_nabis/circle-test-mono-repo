import { createWriteStream } from 'fs'
import uuid from 'uuid/v1'

import { resolve as gqlResolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'

import config from '../../config'

export const typeDefs = `
    uploadAttachment(input: Upload!, organizationId: ID!) : AttachmentPayload
    updateAttachment(input: UpdateAttachmentInput!) : AttachmentPayload
    deleteAttachment(id: ID!, orgId: ID!) : Boolean
  `
export const resolvers = {
  uploadAttachment: gqlResolve(
    authUser,
    unmask('organizationId'),
  )((obj, args, context, info) => {
    const { organizationId } = args
    const { Attachment } = context.connections.logisticsDB

    const writeToFileSystem = ({ createReadStream, filename }) =>
      new Promise((resolve) => {
        const tempName = `${uuid()}-${filename}`
        const path = `${config.workingDir}/templates/temp/${tempName}`
        const done = () => resolve({ path, name: filename })
        createReadStream()
          .pipe(createWriteStream(path))
          .on('finish', done)
      })

    const uploadToGDrive = ({ path, name }) =>
      context.connections.googleDriveClient
        .upsertFileAndFolder(path, name, organizationId, context.connections.logisticsDB)
        .then((gDriveFileId) => ({
          gDriveFileId,
          name,
        }))

    const saveToDb = ({ gDriveFileId, name }) =>
      Attachment.create({
        gDriveFileId,
        name,
        organizationId,
      })

    const sendResponse = ({ id, name, gDriveFileId }) => ({
      changedAttachment: { id, name, gDriveFileId },
    })

    const receiveFile = args.input.file
    return receiveFile
      .then(writeToFileSystem)
      .then(uploadToGDrive)
      .then(saveToDb)
      .then(sendResponse)
  }),
  updateAttachment: gqlResolve(
    authUser,
    unmask('input.organizationId', 'input.id'),
  )((obj, args, context) =>
    context.connections.logisticsDB.Attachment.update(
      {
        name: args.input.name,
      },
      {
        where: {
          id: args.input.id,
        },
      },
    ).then((attachment) => ({
      changedAttachment: {
        id: attachment.id,
        name: attachment.name,
        gDriveFileId: attachment.gDriveFileId,
      },
    })),
  ),
  deleteAttachment: gqlResolve(
    authUser,
    unmask('orgId', 'id'),
  )((obj, args, context) => {
    const db = context.connections.logisticsDB

    const byCorrectIds = {
      where: { id: args.id, organizationId: args.orgId },
    }
    return db.Attachment.destroy(byCorrectIds).then(() => true)
  }),
}
