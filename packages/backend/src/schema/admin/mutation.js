import parser from 'parse-address'
import request from 'request-promise-native'
import Sequelize from 'sequelize'
import config from '../../config'
import logger from '../../logging'
import titleizeDaysTillPaymentDue from '../../utils/titleizeDaysTillPaymentDue'
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import { unpackId } from '../graphql-helpers/contrib/relay/resolvers'
import { resolvers as batchResolvers, typeDefs as batchResolverTypeDefs } from '../resolvers/batchResolver'
import { drafts, draftsTypes } from '../resolvers/draftsResolver'
import { resolvers as driverResolvers, typeDefs as driverTypeDefs } from '../resolvers/driverResolver'
import { inventoryItemResolvers, inventoryItemTypeDefs } from '../resolvers/inventoryItemResolver'
import { resolvers as inventoryResolvers, typeDefs as inventoryTypeDefs } from '../resolvers/inventoryResolver'
import {
  resolvers as licensedLocationResolvers,
  typeDefs as licensedLocationTypeDefs,
} from '../resolvers/licensedLocationResolver'
import { resolvers as licenseResolvers, typeDefs as licenseTypeDefs } from '../resolvers/licenseResolver'
import { resolvers as lineItemResolvers, typeDefs as lineItemTypeDefs } from '../resolvers/lineItemResolver'
import { resolvers as manifestResolvers, typeDefs as manifestTypeDefs } from '../resolvers/manifestResolver'
import { resolvers as orderResolvers, typeDefs as orderTypeDefs } from '../resolvers/orderResolver'
import {
  resolvers as organizationInviteResolvers,
  typeDefs as organizationInviteTypeDefs,
} from '../resolvers/organizationInviteResolver'
import { resolvers as organizationResolvers, typeDefs as organizationTypeDefs } from '../resolvers/organizationResolver'
import {
  resolvers as organizationUserResolvers,
  typeDefs as organizationUserTypeDefs,
} from '../resolvers/organizationUserResolver'
import {
  resetPassword as resetPasswordResolvers,
  resetPasswordTypes as resetPasswordTypeDefs,
} from '../resolvers/resetPasswordResolver'
import { resolvers as roleResolvers, typeDefs as roleTypeDefs } from '../resolvers/roleResolver'
import { resolvers as sendGridResolvers, typeDefs as sendGridTypeDefs } from '../resolvers/sendSendGridEmailResolver'
import { resolvers as siteResolvers, typeDefs as siteTypeDefs } from '../resolvers/siteResolver'
import { resolvers as skuBatchResolvers, typeDefs as skuBatchTypeDefs } from '../resolvers/skuBatchResolver'
import { resolvers as skuResolvers, typeDefs as skuTypeDefs } from '../resolvers/skuResolver'
import {
  resolvers as userPreferenceResolvers,
  typeDefs as userPreferenceTypeDefs,
} from '../resolvers/userPreferenceResolver'
import { resolvers as userResolvers, typeDefs as userTypeDefs } from '../resolvers/userResolver'
import { userRoles, userRolesTypes } from '../resolvers/userRolesResolver'
import { resolvers as vehicleResolvers, typeDefs as vehicleTypeDefs } from '../resolvers/vehicleResolver'

const { Op } = Sequelize

const registerMutationType = (registry) => {
  registry.createType(
    `
    type Mutation {
      ${batchResolverTypeDefs}
      ${draftsTypes}
      ${driverTypeDefs}
      ${inventoryTypeDefs}
      ${inventoryItemTypeDefs}
      ${licensedLocationTypeDefs}
      ${licenseTypeDefs}
      ${lineItemTypeDefs}
      ${manifestTypeDefs}
      ${organizationInviteTypeDefs}
      ${organizationTypeDefs}
      ${organizationUserTypeDefs}
      ${orderTypeDefs}
      ${resetPasswordTypeDefs}
      ${roleTypeDefs}
      ${sendGridTypeDefs}
      ${skuTypeDefs}
      ${skuBatchTypeDefs}
      ${siteTypeDefs}
      ${userRolesTypes}
      ${userPreferenceTypeDefs}
      ${userTypeDefs}
      ${vehicleTypeDefs}

      uploadToOnfleet (input: UploadToOnfleetInput!) : UploadToOnfleetPayload

      createOrderChange(
        submitterId: ID!
        description: String!
        orderId: ID!
      ): Boolean

      assignInvoiceToOrder(
        invoiceId: ID!
        orderId: ID!
      ): Boolean
    }
  `,
    {
      uploadToOnfleet: resolve()((_obj, args, context) => {
        const db = context.connections.logisticsDB

        const orderIds = args.input.orders.map((o) => unpackId(o))

        const findOrders = db.Order.findAll({
          include: [
            {
              model: db.Site,
              as: 'site',
            },
            {
              association: db.Order.Organization,
            },
            {
              association: db.Order.LineItems,
            },
          ],
          where: {
            [Op.or]: [
              {
                id: {
                  [Op.in]: orderIds,
                },
              },
              {
                collectWithOrderId: {
                  [Op.in]: orderIds,
                },
              },
            ],
          },
        })

        const turnToObject = (orders) => {
          const coalesced = {}
          const done = []

          for (const o of orders) {
            if (!!orderIds.find((i) => i == o.id) && !coalesced[o.id]) {
              coalesced[o.id] = {
                site: o.site,
                organization: o.organization,
                number: o.number,
                daysTillPaymentDue: o.daysTillPaymentDue,
                gmv: o.gmv,
                date: o.date,
                estimatedArrivalTimeAfter: o.estimatedArrivalTimeAfter,
                estimatedArrivalTimeBefore: o.estimatedArrivalTimeBefore,
                orders: [],
                notes: o.notes,
              }
            } else if (!!o.collectWithOrderId && !coalesced[o.collectWithOrderId]) {
              const order = orders.find((i) => i.id == o.collectWithOrderId)

              coalesced[order.id] = {
                site: order.site,
                organization: order.organization,
                number: order.number,
                daysTillPaymentDue: order.daysTillPaymentDue,
                gmv: order.gmv,
                date: order.date,
                estimatedArrivalTimeAfter: order.estimatedArrivalTimeAfter,
                estimatedArrivalTimeBefore: order.estimatedArrivalTimeBefore,
                orders: [],
                notes: order.notes,
              }

              coalesced[order.id].orders.push(
                `${o.number}: $${(o.gmv + o.exciseTax - o.discount - o.gmvCollected - o.exciseTaxCollected).toFixed(
                  2,
                )}`,
              )
            } else if (o.collectWithOrderId) {
              coalesced[o.collectWithOrderId].orders.push(
                `${o.number}: $${(o.gmv + o.exciseTax - o.discount - o.gmvCollected - o.exciseTaxCollected).toFixed(
                  2,
                )}`,
              )
            }
          }

          for (const c in coalesced) {
            done.push(coalesced[c])
          }

          return done
        }

        const parseOrders = (_os) =>
          _os.map(
            ({
              date,
              daysTillPaymentDue,
              estimatedArrivalTimeAfter,
              estimatedArrivalTimeBefore,
              notes,
              number,
              organization,
              orders,
              site,
            }) => {
              const st = parser.parseLocation(site.address1)
              const now = new Date(date)

              const etaAfter = `${now.getUTCMonth() + 1}/${now.getUTCDate()}/${now.getFullYear()} ${
                estimatedArrivalTimeAfter
                  ? `${new Date(estimatedArrivalTimeAfter)
                      .toISOString()
                      .split('T')[1]
                      .replace('Z', '')} GMT-0000`
                  : '16:00 GMT-0000'
              }`

              const etaBefore = `${now.getUTCMonth() + 1}/${now.getUTCDate()}/${now.getFullYear()} ${
                estimatedArrivalTimeBefore
                  ? `${new Date(estimatedArrivalTimeBefore)
                      .toISOString()
                      .split('T')[1]
                      .replace('Z', '')} GMT-0000`
                  : '18:00 GMT-0000'
              }`

              const [completeAfter, completeBefore] = [new Date(etaAfter).valueOf(), new Date(etaBefore).valueOf()]

              const parseOrderOrders = (_orders) => {
                if (!_orders.length) return ''
                let string = `
          PICKUP PAYMENT(S): `
                _orders.forEach((o) => {
                  string += `
          - ${o}`
                })
                return string
              }

              let note = ''
              if (notes) {
                note = `

          NOTES: ${notes}`
              }
              return {
                destination: {
                  address: {
                    number: st.number || '',
                    street: `${st.prefix || ''}${st.prefix ? ' ' : ''}${st.street}${st.suffix ? ' ' : ''}${st.suffix ||
                      ''}`,
                    apartment: `${st.sec_unit_type || ''} ${st.sec_unit_num || ''}`,
                    city: site.city,
                    state: site.state,
                    postalCode: site.zip,
                    country: 'USA',
                  },
                },
                recipients: [
                  {
                    phone: site.pocPhoneNumber,
                    name: `${site.name}, #${number}`,
                    notes: `Contact: ${site.pocName}`,
                  },
                ],
                completeAfter,
                completeBefore,
                notes: `
          ORGANIZATION: ${organization.name}
          ORDER(S): ${number}
          TERMS: ${titleizeDaysTillPaymentDue(daysTillPaymentDue)}${parseOrderOrders(orders)}${note}`, // For driver visibility
              }
            },
          )

        const postToOnfleet = (tasks) =>
          request({
            method: 'POST',
            uri: config.onfleet.baseUrl,
            body: { tasks },
            json: true,
          })

        return findOrders
          .then(turnToObject)
          .then(parseOrders)
          .then(postToOnfleet)
          .then((res) => {
            if (res.errors.length > 0) logger.error(`Request ${context.id} error uploading tasks`, res.errors)
            return { ok: res.errors.length === 0, errors: res.errors }
          })
      }),

      createOrderChange: resolve(
        authUser,
        unmask('submitterId', 'orderId'),
      )((_obj, args, context) => {
        const db = context.connections.logisticsDB
        return db.OrderChange.create({
          submitterId: args.submitterId,
          orderId: args.orderId,
          description: args.description,
        }).then(() => true)
      }),

      assignInvoiceToOrder: resolve(
        authUser,
        unmask('invoiceId', 'orderId'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.Order.update(
          {
            collectWithOrderId: args.orderId,
          },
          {
            where: {
              id: args.invoiceId,
            },
          },
        ).then(
          () => true,
          (error) => {
            console.error('assignInvoiceToOrder error:', error)
            return false
          },
        )
      }),
      ...batchResolvers,
      ...drafts,
      ...driverResolvers,
      ...inventoryResolvers,
      ...inventoryItemResolvers,
      ...licenseResolvers,
      ...licensedLocationResolvers,
      ...lineItemResolvers,
      ...manifestResolvers,
      ...orderResolvers,
      ...organizationInviteResolvers,
      ...organizationResolvers,
      ...organizationUserResolvers,
      ...resetPasswordResolvers,
      ...roleResolvers,
      ...sendGridResolvers,
      ...siteResolvers,
      ...skuResolvers,
      ...skuBatchResolvers,
      ...userRoles,
      ...userResolvers,
      ...userPreferenceResolvers,
      ...vehicleResolvers,
    },
  )
}

export { registerMutationType }
