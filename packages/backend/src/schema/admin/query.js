import { Op } from 'sequelize'
import { resolve } from '../graphql-helpers'
import { authUser, unmask } from '../graphql-helpers/contrib/relay'
import logger from '../../logging'
import moment from 'moment'
import { unpackId } from '../graphql-helpers/contrib/relay/resolvers'
import { generateToken } from '../../utils/auth'
import config from '../../config'

import { QUERY_AFTER_LINE_ITEMS_DATE, ORDER_STATUS, ORDER_ACTIONS } from '../../constants'

export const registerQueryType = (registry) => {
  registry.createType(
    `
    type Query {
      environment: String
      viewer: Viewer
      getBatch(id: ID!): Batch
      getDraft(id: ID!): Draft
      getUnavailableDates: [String!]!
      getApiKey(clientUuid: String!): String
      getUser(id: ID!): User
      getOrganization(id: ID!): Organization
      getOrganizationUser(id: ID!): OrganizationUser
      getRole(id: ID!): Role
      getUserRole(id: ID!): UserRole
      getUserPreferences(
        organizationId: ID
        userId: ID!
      ): [UserPreference!]
      getOrder(id: ID!): Order
      getOrders(ids: [ID]): [Order!]!
      getSite(id: ID!): Site
      getLicense(id: ID!): License
      getVehicle(id: ID!): Vehicle
      getLineItem(id: ID!): LineItem
      getSku(id: ID!): Sku
      getOrganizationInvite(id: ID!): OrganizationInvite
      getInventoryItems(
        skuBatchId: ID
      ): [InventoryItem!]!
      getInventoryItem(
        id: ID
        warehouseId: ID
        skuBatchId: ID
        organizationId: ID
        storageType: InventoryStorageType
      ): InventoryItem
      getInventoryLedgerItem(id: ID!): InventoryLedgerItem
      getInventoryQuantity(
        warehouseId: ID!
        skuId: ID!
        organizationId: ID!
        storageType: InventoryStorageType!
      ): Float
      getOrderChangelog(id: ID!): [OrderChange!]!
      getSkuBatch(id: ID!): SkuBatch
      getActiveOrdersForSkuBatch(skuBatchId: ID!): [Order!]!
    }
  `,
    {
      environment: resolve()((_obj, _args, _context) => process.env.NODE_ENV),
      getApiKey: resolve()((_obj, args, _context) => {
        const { clientUuid } = args
        const clientAudience = config.auth.audience.client
        const expiration = 8640000000000000
        const token = generateToken(clientUuid, clientAudience, expiration)
        return token
      }),

      getDraft: resolve(
        authUser,
        unmask('id'),
      )(async (_obj, args, context) => {
        return context.connections.logisticsDB.Draft.findOne({
          where: { id: args.id },
        })
      }),

      /**
       * get all rows of DateRule with a maxOrders column !== null
       * map rows
       *  create date in future
       *  query for active orders with delivery date === futureDate
       * reduce rows
       *  return rows where num active orders has matched or exceeded the max
       */
      getUnavailableDates: resolve(authUser)(async (_o, _a, ctx) => {
        const { DateRule, Order } = ctx.connections.logisticsDB

        const dateRules = await DateRule.findAll({ where: { maxOrders: { [Op.ne]: null } } })

        const dateRuleToDateString = (daterule) => {
          const { dayOfYear, monthDay } = daterule
          const now = moment()
          const currentYear = now.year()
          const dateIsInNewYear = dayOfYear < now.dayOfYear()

          const yearToCompare = dateIsInNewYear ? currentYear + 1 : currentYear
          return `${monthDay.replace('/', '-')}-${yearToCompare}`
        }

        const allDateRulesWithOrders = dateRules.map(async (dateRule) => {
          const dateToCompare = moment(dateRuleToDateString(dateRule))
          const [start, end] = [dateToCompare.clone().startOf('day'), dateToCompare.clone().endOf('day')]

          return [
            dateRule,
            await Order.findAll({
              where: {
                date: {
                  [Op.lte]: end.utc().toDate(),
                  [Op.gte]: start.utc().toDate(),
                },
                isArchived: false,
                status: {
                  [Op.in]: [
                    ORDER_STATUS.SCHEDULED,
                    ORDER_STATUS.UNSCHEDULED,
                    ORDER_STATUS.TRANSFERRING,
                    ORDER_STATUS.DELAYED,
                  ],
                },
              },
            }),
          ]
        })

        const results = await Promise.all(allDateRulesWithOrders)

        return results.reduce((accum, [dateRule, activeOrders]) => {
          if (dateRule.maxOrders <= activeOrders.length) {
            return [...accum, dateRuleToDateString(dateRule)]
          }
          return accum
        }, [])
      }),

      getBatch: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, ctx) => ctx.connections.logisticsDB.Batch.findByPk(args.id)),

      getUser: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.User.findOne({
          where: { id: args.id },
        })
      }),

      getOrganization: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.Organization.findOne({
          where: { id: args.id },
        })
      }),

      getOrganizationUsers: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.OrganizationUser.findOne({
          where: { id: args.id },
        })
      }),

      getRole: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.Role.findOne({
          where: { id: args.id },
        })
      }),

      getUserRole: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.UserRole.findOne({
          where: { id: args.id },
        })
      }),

      getUserPreferences: resolve(
        authUser,
        unmask('organizationId', 'userId'),
      )((_obj, { organizationId, userId }, { connections: { logisticsDB: { UserPreference } } }) =>
        UserPreference.findAll({
          where: { organizationId, userId },
        }),
      ),

      getOrder: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.Order.findOne({
          where: { id: args.id },
        })
      }),

      getOrders: resolve(authUser)((_obj, args, context) => {
        const orderIds = args.ids.map((id) => unpackId(id))
        return context.connections.logisticsDB.Order.findAll({
          order: [['number', 'asc']],
          where: { id: { [Op.in]: orderIds } },
        })
      }),

      getSite: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.Site.findOne({
          where: { id: args.id },
        })
      }),

      getVehicle: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.Vehicle.findOne({
          where: { id: args.id },
        })
      }),

      getLineItem: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.LineItem.findOne({
          where: { id: args.id },
        })
      }),

      getLicense: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.License.findOne({
          where: { id: args.id },
        })
      }),

      getSku: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.Sku.findOne({
          where: { id: args.id },
        })
      }),

      getOrganizationInvite: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.OrganizationInvite.findOne({
          where: { id: args.id },
        })
      }),

      getInventoryItems: resolve(
        authUser,
        unmask('id', 'skuId', 'batchId', 'skuBatchId'),
      )(async (_obj, args, context) => {
        const { InventoryItem } = context.connections.logisticsDB
        const { skuBatchId } = args
        try {
          return await InventoryItem.findAll({ where: { skuBatchId } })
        } catch (error) {
          logger.error(error)
          throw error
        }
      }),

      getInventoryItem: resolve(
        authUser,
        unmask('id', 'warehouseId', 'skuId', 'organizationId', 'batchId'),
      )(async (_obj, args, context) => {
        const { InventoryItem } = context.connections.logisticsDB
        const { id, organizationId, skuBatchId, storageType, warehouseId } = args
        if (id) {
          return InventoryItem.findOne({ where: { id } })
        }
        if (warehouseId && skuBatchId && organizationId) {
          try {
            const items = await InventoryItem.findAll({
              where: {
                organizationId,
                skuBatchId,
                storageType: storageType || 'INVENTORY',
                warehouseId,
              },
            })
            return items
          } catch (error) {
            logger.error('Error searching for inventory item, that sku+batch pair does not exist', error)
            throw error
          }
        }
        logger.error(
          'Error searching for InventoryItem: must include either an id OR skuBatchId, warehouseId, organizationId and storageType',
        )
        return []
      }),

      getInventoryLedgerItem: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.InventoryLedgerItem.findOne({
          where: { id: args.id },
        })
      }),

      getInventoryQuantity: resolve(
        authUser,
        unmask('warehouseId', 'skuId', 'organizationId'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.InventoryItem.findAll({
          where: { ...args },
        }).then((res) => {
          if (res && res.length === 1) {
            return res[0].counted || 0
          }
          if (res && res.length > 1) {
            logger.error(`Multiple inventory items exist for: ${{ ...args }}`)
          }
          return 0
        })
      }),

      getOrderChangelog: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.OrderChange.findAll({
          order: [['createdAt', 'ASC']],
          where: { orderId: args.id },
        })
      }),

      getSkuBatch: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        const { id } = args
        return context.connections.logisticsDB.SkuBatch.findByPk(id, { paranoid: false })
      }),

      getActiveOrdersForSkuBatch: resolve(
        authUser,
        unmask('skuBatchId'),
      )(async (_obj, { skuBatchId }, ctx) => {
        const { Order } = ctx.connections.logisticsDB

        const outgoingWhere = {
          action: {
            [Op.in]: [
              ORDER_ACTIONS.DELIVERY_TO_BRAND,
              ORDER_ACTIONS.DELIVERY_TO_RETAILER,
              ORDER_ACTIONS.PICKUP_FROM_NABIS,
              ORDER_ACTIONS.INTERNAL_TRANSFER,
              ORDER_ACTIONS.OTHER,
            ],
          }, // orders with inventory leaving warehouse
          outtaken: false,
        }

        const externalIncomingWhere = {
          action: {
            [Op.in]: [
              ORDER_ACTIONS.PICKUP_FROM_BRAND,
              ORDER_ACTIONS.RETURN_FROM_RETAILER,
              ORDER_ACTIONS.DROPOFF_TO_NABIS,
              ORDER_ACTIONS.OTHER,
            ],
          },
          intaken: false,
        }

        const transferIncomingWhere = { action: ORDER_ACTIONS.INTERNAL_TRANSFER, intaken: false }

        const orders = await Order.findAll({
          include: [
            {
              association: Order.LineItems,
              where: {
                skuBatchId,
              },
            },
          ],
          order: [
            ['date', 'desc'],
            ['action', 'asc'],
            ['status', 'asc'],
            ['createdAt', 'desc'],
          ],
          where: {
            [Op.or]: [
              {
                ...outgoingWhere,
              },
              {
                ...externalIncomingWhere,
              },
              {
                ...transferIncomingWhere,
              },
            ],
            date: { [Op.gte]: QUERY_AFTER_LINE_ITEMS_DATE },
            isArchived: false,
            isExcluded: false,
            status: {
              [Op.in]: [
                ORDER_STATUS.SCHEDULED,
                ORDER_STATUS.UNSCHEDULED,
                ORDER_STATUS.TRANSFERRING,
                ORDER_STATUS.DELAYED,
                ORDER_STATUS.DELIVERED,
                ORDER_STATUS.DELIVERED_WITH_EDITS,
              ],
            },
          },
        })
        return orders
      }),

      viewer: resolve()((_obj, _args, _context) => ({})),
    },
  )
}
