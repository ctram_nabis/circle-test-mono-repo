import { GraphQLSchema } from 'graphql'
import { Registry } from '../graphql-helpers'
import { processGraphQL } from '../graphql-helpers/contrib/relay'
import {
  registerAttachmentTypes,
  registerDraftTypes,
  registerDriverTypes,
  registerInventoryItemTypes,
  registerInventoryLedgerItemTypes,
  registerLicenseTypes,
  registerLicensedLocationTypes,
  registerLineItemTypes,
  registerManifestTypes,
  registerOnfleetTypes,
  registerOrderTypes,
  registerOrganizationInviteTypes,
  registerOrganizationTypes,
  registerOrganizationUserTypes,
  registerPaginationTypes,
  registerRoleTypes,
  registerSalesTypes,
  registerScalarTypes,
  registerSiteTypes,
  registerSkuTypes,
  registerTitleholderTypes,
  registerUserRoleTypes,
  registerUserTypes,
  registerVehicleTypes,
  registerWarehouseTypes,
  registerDashboardDataTypes,
  registerUserPreferenceTypes,
  // Integrations
  registerSendGridTypes,
} from '../types'
import { registerQueryType } from './query'
import { registerMutationType } from './mutation'
import { registerViewerTypes } from './viewer'

const registry = new Registry(processGraphQL)
registerAttachmentTypes(registry)
registerDraftTypes(registry)
registerDriverTypes(registry)
registerInventoryItemTypes(registry)
registerInventoryLedgerItemTypes(registry)
registerLicenseTypes(registry)
registerLicensedLocationTypes(registry)
registerLineItemTypes(registry)
registerManifestTypes(registry)
registerMutationType(registry)
registerOnfleetTypes(registry)
registerOrderTypes(registry)
registerOrganizationInviteTypes(registry)
registerOrganizationTypes(registry)
registerOrganizationUserTypes(registry)
registerPaginationTypes(registry)
registerQueryType(registry)
registerRoleTypes(registry)
registerSalesTypes(registry)
registerScalarTypes(registry)
registerSiteTypes(registry)
registerSkuTypes(registry)
registerTitleholderTypes(registry)
registerUserRoleTypes(registry)
registerUserTypes(registry)
registerVehicleTypes(registry)
registerWarehouseTypes(registry)
registerViewerTypes(registry)
registerDashboardDataTypes(registry)
registerUserPreferenceTypes(registry)

// Integrations
registerSendGridTypes(registry)

const GQLSchema = new GraphQLSchema({
  query: registry.getType('Query'),
  mutation: registry.getType('Mutation'),
})

export default GQLSchema
