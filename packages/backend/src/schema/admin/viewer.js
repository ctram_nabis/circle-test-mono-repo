import Sequelize, { Op } from 'sequelize'
import idx from 'idx'
import { Registry, resolve, unmask } from '../graphql-helpers'
import { authUser, authAdminUser } from '../graphql-helpers/contrib/relay'
import logger from '../../logging'
import config from '../../config'
import getOrderAndPaginateOpts from '../utils/getOrderAndPaginateOpts'
import wrapResultsWithPageInfo from '../utils/wrapResultsWithPageInfo'
import { ordersViewerTypes, ordersViewer } from '../resolvers/ordersViewerResolver'
import { draftsViewerTypes, draftsViewer } from '../resolvers/draftsViewerResolver'
import { dashboardDataTypes, dashboardData } from '../resolvers/dashboardDataResolver'
import { adminOrdersViewerTypes, adminOrdersViewer } from '../resolvers/adminOrdersViewerResolver'
import {
  siteOutstandingInvoicesViewerTypes,
  siteOutstandingInvoicesViewer,
} from '../resolvers/siteOutstandingInvoicesViewerResolver'
import {
  outstandingInvoicesViewerTypes,
  outstandingInvoicesViewer,
} from '../resolvers/outstandingInvoicesViewerResolver'
import { skuViewerTypes, skuViewer } from '../resolvers/skuViewerResolver'
import { skuBatchViewerTypes, skuBatchViewer } from '../resolvers/skuBatchViewerResolver'
import { batchViewerTypes, batchViewer } from '../resolvers/batchViewerResolver'
import { NABIS_ORGANIZATIONS } from '../../constants'

const registerViewerTypes = (registry) => {
  registry.createType(
    `
    type Viewer {
      me: User
      allUsers: [User!]!
      allOrganizations: [Organization!]!
      allAdminOrganizations: [Organization!]!
      allOrganizationUsers: [OrganizationUser!]!
      allRoles: [Role!]!
      allUserRoles: [UserRole!]!
      ${draftsViewerTypes}
      ${ordersViewerTypes}
      ${adminOrdersViewerTypes}
      ${skuViewerTypes}
      ${skuBatchViewerTypes}
      ${batchViewerTypes}
      ${dashboardDataTypes}
      allSites(
        organizationId: ID!
      ): [Site!]!
      allVehicles: [Vehicle!]!
      allLineItems: [LineItem!]!
      allLicenses (
        organizationId: ID!
      ): AllLicensesPayload!
      allSalesTimeSeries(
        organizationId: ID!
      ): [Sales!]!
      allOrganizationInvites(
        organizationId: ID
      ): [OrganizationInvite!]!
      allWarehouses: [Warehouse!]!
      allPublicSites: [Site!]!
      allLicensedLocations (
        name: String
        showArchived: Boolean
        pageInfo: PageInfoInput
      ): AllLicensedLocationsPayload!
      allInventory(
        organizationId: ID!
      ): [InventoryRow!]
      allInventoryLedgerItems(
        inventoryItemId: ID!
        orderId: ID
        organizationID: ID
      ): [InventoryLedgerItem!]!
      allInventoryItems(
        skuBatchId: ID!
        warehouseId: ID!
        pageInfo: PageInfoInput
      ): InventoryItemInfo!
      allAdminInventoryItems(
        organizationId: ID
        siteId: ID
      ): [InventoryRow!]!
      allDrivers: [Driver!]!
      accessibleBrands(orgId: ID!): [Organization!]!
      ${outstandingInvoicesViewerTypes}
      allFutureDropoffsToDispensary(id:ID!): [Order!]!
      ${siteOutstandingInvoicesViewerTypes}
      getSite(id:ID!): Site
      getOrder(number:String): Order
      getTitleholders(organizationId: ID!): [Titleholder]
    }
  `,
    {
      me: resolve(authUser)((_obj, _args, context) => {
        const { user } = context
        return context.connections.logisticsDB.User.findByPk(user && user.id)
      }),
      allUsers: resolve(authUser)((_obj, _args, context) => {
        return context.connections.logisticsDB.User.findAll()
      }),
      allOrganizations: resolve(authUser)((_obj, _args, context) => {
        return context.connections.logisticsDB.Organization.findAll({
          include: [
            {
              model: context.connections.logisticsDB.User,
              as: 'users',
              where: {
                id: context.user.id,
              },
            },
          ],
        })
      }),
      allAdminOrganizations: resolve(authAdminUser)((_obj, _args, context) => {
        return context.connections.logisticsDB.Organization.findAll()
      }),
      allOrganizationUsers: resolve(authUser)((_obj, _args, context) => {
        return context.connections.logisticsDB.OrganizationUser.findAll({
          where: {
            userId: context.user.id,
          },
        })
      }),
      allRoles: resolve(authUser)((_obj, _args, context) => {
        return context.connections.logisticsDB.Role.findAll()
      }),
      allUserRoles: resolve(authUser)((_obj, _args, context) => {
        return context.connections.logisticsDB.UserRole.findAll()
      }),
      ...draftsViewer,
      ...ordersViewer,
      ...adminOrdersViewer,
      ...skuViewer,
      ...skuBatchViewer,
      allSites: resolve(
        authUser,
        unmask('organizationId'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.Site.findAll({
          where: {
            organizationId: args.organizationId,
          },
          order: [['name', 'ASC']],
        })
      }),
      allLineItems: resolve(authUser)((_obj, _args, context) => {
        return context.connections.logisticsDB.LineItem.findAll()
      }),
      allLicenses: resolve(
        authUser,
        unmask('organizationId', 'siteId'),
      )((_obj, args, context) => {
        return {
          organizationLicenses: context.connections.logisticsDB.License.findAll({
            order: [['createdAt', 'ASC']],
            where: {
              organizationId: args.organizationId,
            },
            returning: true,
          }),
        }
      }),
      allDrivers: resolve(authUser)((_obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.Driver.findAll({ order: [['createdAt', 'ASC']] })
      }),
      allVehicles: resolve(authUser)((_obj, _args, context) => {
        return context.connections.logisticsDB.Vehicle.findAll({
          order: [['createdAt', 'ASC']],
        })
      }),
      allSalesTimeSeries: resolve(
        authUser,
        unmask('organizationId'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.db
          .query(
            `SELECT DATE_PART('day', date) AS "day", DATE_PART('month', date) AS "month", DATE_PART('year', date) AS "year", SUM("gmv") AS "gmv", SUM("exciseTax") AS "exciseTax"
          FROM public."Order"
          WHERE "organizationId"='${args.organizationId}' AND "action"='DELIVERY_TO_RETAILER' AND "status"='COMPLETE'
          GROUP BY DATE_PART('day', date), DATE_PART('month', date), DATE_PART('year', date)
          ORDER BY DATE_PART('year', date), DATE_PART('month', date), DATE_PART('day', date) ASC`,
          )
          .spread((results, _metadata) =>
            // Results will be an empty array and metadata will contain the number of affected rows.
            results.map((res) => ({
              ...res,
              organizationId: args.organizationId,
            })),
          )
      }),
      allOrganizationInvites: resolve(
        authUser,
        unmask('organizationId'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.OrganizationInvite.findAll({
          where: {
            organizationId: args.organizationId,
          },
          order: [['createdAt', 'ASC']],
        })
      }),
      allWarehouses: resolve(authUser)((_obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.Warehouse.findAll({
          include: [{ association: db.Warehouse.Site }],
          where: { isArchived: false },
        })
      }),
      allPublicSites: resolve(authUser)((_obj, _args, context) => {
        const db = context.connections.logisticsDB
        const publicSites = {
          where: {
            organizationId: config.publicSitesOrganizationId,
          },
          include: [{ association: db.Site.Licenses }],
          // order: [['name', 'ASC']],
        }

        return db.Site.findAll(publicSites)
      }),

      allLicensedLocations: resolve(authUser)((_obj, args, context) => {
        logger.info(`Request ${context.id}`, 'In viewer.allLicensedLocations()')
        const db = context.connections.logisticsDB

        const opts = {
          where: {
            isArchived: false,
          },
        }

        if (args.showArchived) {
          delete opts.where.isArchived
        }

        if (args.name) {
          opts.where = {
            ...opts.where,
            name: {
              [Op.iLike]: `%${args.name}%`,
            },
          }
        }

        const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(args.pageInfo)
        return db.LicensedLocation.findAll({
          ...opts,
          ...orderOpts,
          ...paginateOpts,
        }).then((results) => {
          const pageInfo = wrapResultsWithPageInfo(db.LicensedLocation, { ...opts, ...orderOpts }, args)
          return {
            results,
            pageInfo,
          }
        })
      }),

      allAttachments: resolve(
        authUser,
        unmask('organizationId'),
      )((_obj, args, context) => {
        logger.info(`Request ${context.id}`, 'In viewer.allAttachments()')
        const db = context.connections.logisticsDB

        const opts = {
          where: { organizationId: args.organizationId },
        }
        const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(args.pageInfo)
        return db.Attachment.findAll({
          ...opts,
          ...orderOpts,
          ...paginateOpts,
        }).then((results) => {
          const pageInfo = wrapResultsWithPageInfo(db.Attachment, { ...opts, ...orderOpts }, args)
          return {
            pageInfo,
            results,
          }
        })
      }),
      allLinkedAttachments: resolve(
        authUser,
        unmask('orderId'),
      )((_obj, args, context) => {
        const db = context.connections.logisticsDB
        return db.Order.findByPk(args.orderId).then((order) => (order ? order.getAttachments() : []))
      }),
      allInventory: resolve(
        authUser,
        unmask('organizationId', 'siteId', 'skuId'),
      )((_obj, args, context) => {
        const db = context.connections.logisticsDB

        return db.Sku.findAll({
          attributes: {
            include: [
              [Sequelize.fn('SUM', Sequelize.col('inventoryItems.quantity')), 'totalQuantity'],
              [Sequelize.fn('MAX', Sequelize.col('inventoryItems.updatedAt')), 'lastUpdatedAt'],
            ],
          },
          group: ['Sku.id'],
          include: [
            {
              as: 'inventoryItems',
              attributes: [],
              model: db.InventoryItem,
            },
          ],
          order: [['code', 'ASC']],
          where: {
            organizationId: args.organizationId,
          },
        }).then((skus) =>
          // Return in proper shape
          skus.map((sku) => ({
            lastUpdatedAt: sku.get('lastUpdatedAt'),
            sku,
            totalQuantity: sku.get('totalQuantity'),
          })),
        )
      }),
      allInventoryItems: resolve(
        authUser,
        unmask('warehouseId', 'skuBatchId'),
      )(async (_obj, args, context) => {
        const { InventoryItem, InventoryLedgerItem } = context.connections.logisticsDB

        const items = await InventoryItem.findAll({
          returning: true,
          where: {
            skuBatchId: args.skuBatchId,
            warehouseId: args.warehouseId,
          },
        })

        const ids = items.map((i) => i.id)
        const opts = {
          include: [
            {
              association: InventoryLedgerItem.Order,
            },
          ],
          paranoid: false,
          returning: 'true',
          where: {
            inventoryItemId: { [Op.in]: ids },
            transactionType: { [Op.not]: 'ALLOCATION' },
          },
        }

        const { orderOpts, paginateOpts } = getOrderAndPaginateOpts(args.pageInfo)
        return InventoryLedgerItem.findAll({
          ...opts,
          ...orderOpts,
          ...paginateOpts,
        }).then((results) => {
          const pageInfo = wrapResultsWithPageInfo(
            InventoryLedgerItem,
            {
              ...opts,
              ...orderOpts,
            },
            args,
          )
          return { results, pageInfo }
        })
      }),
      allInventoryLedgerItems: resolve(
        authUser,
        unmask('inventoryItemId', 'orderId', 'organizationId'),
      )((_obj, args, context) => {
        const db = context.connections.logisticsDB

        const whereArg = Object.keys(args).reduce((acc, val) => {
          if (args[val]) {
            acc[val] = args[val]
          }
          return acc
        }, {})

        return db.InventoryLedgerItem.findAll({
          where: {
            inventoryItemId: args.inventoryItemId,
            ...whereArg,
          },
        })
      }),
      allAdminInventoryItems: resolve(
        authAdminUser,
        unmask('organizationId', 'siteId'),
      )((_obj, _args, context) => {
        const db = context.connections.logisticsDB
        return db.Sku.findAll({
          attributes: {
            include: [
              [Sequelize.fn('SUM', Sequelize.col('inventoryItems.quantity')), 'totalQuantity'],
              [Sequelize.fn('MAX', Sequelize.col('inventoryItems.updatedAt')), 'lastUpdatedAt'],
            ],
          },
          order: [['code', 'ASC']],
          include: [
            {
              model: db.InventoryItem,
              as: 'inventoryItems',
              attributes: [],
            },
          ],
          group: ['Sku.id'],
        }).then((skus) =>
          // Return in proper shape
          skus.map((sku) => ({
            sku,
            totalQuantity: sku.get('totalQuantity'),
            lastUpdatedAt: sku.get('lastUpdatedAt'),
          })),
        )
      }),

      accessibleBrands: resolve(
        authUser,
        unmask('orgId'),
      )((_obj, args, context) => {
        const db = context.connections.logisticsDB

        const findSubBrandIds = db.OrganizationOrganization.findAll({
          where: { superorganizationId: args.orgId },
        })

        const ensureSubBrandArray = (orgorgs) =>
          orgorgs.length >= 2 ? orgorgs.map((orgorg) => orgorg.suborganizationId) : [args.orgId]

        const findSubBrands = (subBrandIds) =>
          db.Organization.findAll({
            where: { id: { [Op.in]: subBrandIds } },
          })

        return findSubBrandIds.then(ensureSubBrandArray).then(findSubBrands)
      }),

      ...batchViewer,
      ...dashboardData,
      ...outstandingInvoicesViewer,
      allFutureDropoffsToDispensary: resolve(
        authUser,
        unmask('id'),
      )(async (_obj, args, context) => {
        // TODO: DEPRECATED TEMPORARILY - FUTURE DROPOFFS
        // const db = context.connections.logisticsDB
        // // find Order and grab licensed location id
        // // to get the dispensary's name for the page
        // const order = await db.Order.findByPk(args.id, {
        //   include: [
        //     {
        //       association: db.Order.Site,
        //       include: [
        //         {
        //           association: db.Site.LicensedLocation,
        //         },
        //       ],
        //     },
        //   ],
        // })
        // const licensedLocationId = idx(order, (_) => _.site.licensedLocationId)
        // if (!licensedLocationId) {
        //   return []
        // }
        // const params = {
        //   order: [['date', 'asc']],
        //   where: {
        //     action: 'DELIVERY_TO_RETAILER',
        //     date: {
        //       [Op.gt]: new Date(),
        //     },
        //     licensedLocationId,
        //   },
        // }
        // const futureDropOffs = await db.Order.findAll(params)
        // return futureDropOffs
      }),

      ...siteOutstandingInvoicesViewer,
      getSite: resolve(
        authUser,
        unmask('id'),
      )((_obj, args, context) => {
        return context.connections.logisticsDB.Site.findByPk(args.id)
      }),

      getOrder: resolve(authUser)((_obj, args, context) =>
        context.connections.logisticsDB.Order.findOne({
          where: {
            number: parseInt(args.number, 10) || 0,
          },
        }),
      ),

      getTitleholders: resolve(
        authUser,
        unmask('organizationId'),
      )(async (_obj, { organizationId }, context) => {
        /**
         * User is only authorized to get the titleholders that they have access to
         * "Authorized" iff:
         * 1) is Nabis user, Admin or otherwise
         *    OR
         * 2) is a member of Organization.id === organizationId
         */
        const { isAdmin, organizations } = context.user

        const memberOfOrganizationIds = organizations.map((org) => org.id)

        const isNabisOrganization = memberOfOrganizationIds.includes(NABIS_ORGANIZATIONS.DELIVERIES)

        if (!isAdmin && !isNabisOrganization) {
          const isMemberOfQueryOrganizationId = memberOfOrganizationIds.includes(organizationId)

          if (!isMemberOfQueryOrganizationId) {
            throw new Error('Not authorized to get the titleholders of this organization')
          }
        }

        const titleholders = await context.connections.logisticsDB.Titleholder.findAll({
          where: {
            organizationId,
          },
        })

        return titleholders
      }),
    },
  )
}

export { registerViewerTypes }
