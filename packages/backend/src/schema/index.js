import AdminGQLSchema from './admin/schema'
import ClientGQLSchema from './client/schema'

export { AdminGQLSchema, ClientGQLSchema }
