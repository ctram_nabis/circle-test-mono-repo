/**
 * This script force generates manifests for orders with delivery dates on
 * or after the environmental variable "DELIVERY_START_DATE", in the format
 * of 'YYYY-MM-DD'.
 *
 * To use this script, from mono-repo root,
 * run in the CLI "DELIVERY_START_DATE=<date> BATCH_SIZE=<number> BATCH_NUMBER=<number> yarn force-generate-manifests:<environment>"
 *
 * We need to run this in batches because network failures cause the script to fail silently, without errors.
 */
import fs from 'fs'
import moment from 'moment'
import Sequelize from 'sequelize'

import config from '../config'
import { GDriveAPI } from '../connections/gdrive/api'
import db from '../connections/postgres'
import { generateManifest } from '../schema/resolvers/manifestResolver'

const base = db.base
const logisticsDB = base.db.models

const googleDriveClient = new GDriveAPI({
  clientEmail: config.google.serviceAccount.clientEmail,
  privateKey: config.google.serviceAccount.privateKey,
})

const context = {
  connections: {
    logisticsDB,
    googleDriveClient,
  },
}

interface ManifestGenerationDetails {
  order?: string
  status?: string
  manifest?: any
  error?: string
}

type CreateLog = () => { addToLog: (str?: string) => void; getLog: () => string }

const createLog: CreateLog = () => {
  let buffer = ''

  const addToLog = (str = '') => {
    buffer += str + '\n'
  }

  const getLog = () => {
    return buffer
  }

  return { addToLog, getLog }
}

type GetAllOrders = (deliveryStartDateStr: string) => Promise<any[]>

const getAllOrders: GetAllOrders = (deliveryStartDateStr) => {
  const deliveryStartDate = new Date(deliveryStartDateStr)

  if (isNaN(deliveryStartDate.valueOf())) {
    throw Error('Invalid date format. deliveryStartDate should be in the format of "YYYY-MM-DD"')
  }

  const Op = Sequelize.Op

  return logisticsDB.Order.findAll({
    where: {
      date: {
        [Op.gte]: deliveryStartDate,
      },
    },
    order: [['createdAt', 'ASC']],
  })
}

type GetRelevantOrders = (
  orders: any[],
  batchNumber?: number,
  batchSize?: number,
) => { orders: any[]; startIdx: number; endIdx: number }

const getRelevantOrders: GetRelevantOrders = (orders, batchNumber, batchSize) => {
  let endIdx = orders.length - 1
  let startIdx = 0
  let res = {}

  if (batchNumber && batchSize) {
    endIdx = batchNumber * batchSize
    startIdx = (batchNumber - 1) * batchSize

    res = {
      orders: orders.slice(startIdx, endIdx),
    }
  } else {
    res = { orders }
  }

  return { ...res, startIdx, endIdx } as any
}

type GetQuerySummary = (deliveryStartDateStr: string, batchSize?: number) => Promise<string>

const getQuerySummary: GetQuerySummary = async (deliveryStartDateStr, batchSize) => {
  const allOrders = await getAllOrders(deliveryStartDateStr)

  const { numAllOrders, numBatches } = getBatchDetails(allOrders, batchSize)

  const { addToLog, getLog } = createLog()

  addToLog()
  addToLog('=============================================')
  addToLog('=============================================')
  addToLog('============  Orders Summary  ===============')
  addToLog('=============================================')
  addToLog('=============================================')
  addToLog()
  addToLog(`Environment: ${process.env.NODE_ENV}`)
  addToLog(`Database Host: ${process.env.DB_HOST}`)
  addToLog(`Num relevant orders: ${numAllOrders}`)
  addToLog(`Num batches for batch size of ${batchSize} order per batch: ${numBatches}`)

  return getLog()
}

type GetBatchDetails = (orders: any[], batchSize?: number) => { numAllOrders: number; numBatches: number }

const getBatchDetails: GetBatchDetails = (orders, batchSize) => {
  const numAllOrders = orders.length

  if (!batchSize) {
    return { numAllOrders, numBatches: 1 }
  }

  const numBatches = Math.ceil(orders.length / batchSize)
  return { numAllOrders, numBatches }
}

type GenerateOrdersProcessing = (orders: any[]) => Array<Promise<ManifestGenerationDetails>>

const generateOrdersProcessing: GenerateOrdersProcessing = (orders) => {
  let delay = 0
  let counter = 1

  return orders.map(async (order) => {
    const orderJSON = JSON.stringify(order)

    let data: ManifestGenerationDetails = {
      order: orderJSON,
    }

    return new Promise((resolve) => {
      // We do not want to send too many requests to Google at the same time
      setTimeout(async () => {
        console.log('~~~~~~~~~~')
        console.log('~~~~~~~~~~')
        console.log(`~~~~ Processing manifest number ${counter++} `)
        console.log('~~~~~~~~~~')
        console.log('~~~~~~~~~~')

        try {
          const manifest = await generateManifest(order.id, context)
          data = { ...data, status: 'success', manifest }
          resolve(data)
        } catch (e) {
          data = { ...data, status: 'failure', error: e }
          resolve(data)
        }
      }, (delay += 2000))
    })
  })
}

type GenerateManifests = (deliveryStartDateStr: string, batchNumber?: number, batchSize?: number) => Promise<void>

/**
 * @param {string} deliveryStartDate - Force generate manifest for orders with delivery dates on or after this
 *                                      date; must be in format of 'YYYY-MM-DD'
 */
const generateManifests: GenerateManifests = async (deliveryStartDateStr, batchNumber, batchSize) => {
  const allOrders = await getAllOrders(deliveryStartDateStr)

  const { orders, startIdx, endIdx } = getRelevantOrders(allOrders, batchNumber, batchSize)

  const startingTime = new Date()
  console.log('Starting processing of orders')
  const ordersProcessed = await Promise.all(generateOrdersProcessing(orders))
  console.log('Finished processing of orders')

  const finishingTime = new Date()

  const successfulGenerations = []
  const failedGenerations = []

  ordersProcessed.forEach((generationDetails: ManifestGenerationDetails) => {
    if (generationDetails.status === 'success') {
      successfulGenerations.push(generationDetails)
    } else {
      failedGenerations.push(generationDetails)
    }
  })

  const { addToLog, getLog } = createLog()

  addToLog()
  addToLog()
  addToLog('===============================================')
  addToLog('===============================================')
  addToLog('========== MANIFEST GENERATION COMPLETED ======')
  addToLog('===============================================')
  addToLog('===============================================')

  addToLog()
  addToLog()
  addToLog('===============================================')
  addToLog('=============== Summary =======================')
  addToLog('===============================================')
  addToLog()
  addToLog()

  const { numBatches } = getBatchDetails(allOrders, batchSize)

  addToLog(`Environment: ${process.env.NODE_ENV}`)
  addToLog(`Database Host: ${process.env.DB_HOST}`)
  addToLog(`Time elapsed: ${(Number(finishingTime) - Number(startingTime)) / 1000} secs`)
  addToLog('Num All Orders: ' + allOrders.length)
  addToLog('Batch Size: ' + batchSize)
  addToLog('Total Num Batches: ' + numBatches)
  addToLog('Batch Number Processed: ' + batchNumber)
  addToLog('All Orders Start Idx: ' + startIdx)
  addToLog('All Orders End Idx: ' + endIdx)
  addToLog('Num Orders Processed ' + ordersProcessed.length)
  addToLog('Num Orders Succeeded ' + successfulGenerations.length)
  addToLog('Num Orders Failed ' + failedGenerations.length)

  if (failedGenerations.length) {
    addToLog(`IDs of orders failed: ${failedGenerations.map((_) => _.order.id)}`)
  }

  addToLog()
  addToLog()
  addToLog('===============================================')
  addToLog('============ Successful Generations ===========')
  addToLog('===============================================')
  addToLog()
  addToLog()

  successfulGenerations.length
    ? successfulGenerations.forEach((generationDetails) => {
        addToLog()
        addToLog(JSON.stringify(generationDetails))
        addToLog()
      })
    : addToLog('No successful generations')

  addToLog()
  addToLog()
  addToLog('===============================================')
  addToLog('============ Failed Generations ===============')
  addToLog('===============================================')
  addToLog()
  addToLog()

  failedGenerations.length
    ? failedGenerations.forEach((generationDetails) => {
        addToLog()
        addToLog(JSON.stringify(generationDetails))
        addToLog()
      })
    : addToLog('No failed generations')

  console.log('summary report', getLog())

  let batchDetails = ''

  if (batchNumber) {
    batchDetails += `batch-number-${batchNumber}-of-${numBatches}`
  }

  const fileName = `force-generate-manifests-summary-${process.env.NODE_ENV.toUpperCase()}--${moment().format(
    'YYYY-MM-DDTHH:mm:ssZZ',
  )}--${batchDetails}.txt`
  const logsDirPath = './logs'

  return new Promise((resolve) => {
    // Check that directory exists
    if (!fs.existsSync(logsDirPath)) {
      fs.mkdirSync(logsDirPath)
    }

    console.log()
    fs.writeFileSync(logsDirPath + '/' + fileName, getLog())
    console.log(`Summary file written to "${logsDirPath}/${fileName}"`)
    resolve()
  })
}

////////////////// Start Process /////////////////////////

const _deliveryStartDateStr: string = process.env.DELIVERY_START_DATE
const _onlyShowQuerySummary: boolean = !!process.env.ONLY_SHOW_QUERY_SUMMARY
const _batchNumber: number = process.env.BATCH_NUMBER && Number(process.env.BATCH_NUMBER)
const _batchSize: number = process.env.BATCH_SIZE && Number(process.env.BATCH_SIZE)

new Promise(async (resolve, reject) => {
  if (!_deliveryStartDateStr) {
    return reject(
      'ERROR: No delivery start date set. Please set an environmental parameter named "DELIVERY_START_DATE", representing a calendar date, when invoking this script; for example "DELIVERY_START_DATE=2020-01-01 yarn force-generate-manifests"',
    )
  }

  const isAuthenticated = await base.authenticate()

  if (!isAuthenticated) {
    const {
      config: { database, host },
    } = base

    return reject(`Failed to connect to database '${database}' at host '${host}'`)
  }

  if (_onlyShowQuerySummary) {
    const querySummary = await getQuerySummary(_deliveryStartDateStr, _batchSize)
    console.log(querySummary)
    return resolve()
  }

  await generateManifests(_deliveryStartDateStr, _batchNumber, _batchSize)
  resolve()
})
  .then(() => {
    console.log('======== Finished =========')
    process.exit(0)
  })
  .catch((e) => {
    console.log(e) // console.error doesn't work here.
    process.exit(1)
  })
