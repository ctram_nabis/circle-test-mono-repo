const dollarFormat = (amount, options = {}) => {
  const { precision = 2, returnZero = false } = options

  const num = Number(amount)
  if (!(num || returnZero)) return '$0.00'

  const floatNum = num.toFixed(precision)

  const formattedNum = `$${floatNum}`
  return formattedNum
}

export default dollarFormat
