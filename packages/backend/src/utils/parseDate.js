import moment from 'moment'
import isNumber from 'lodash/isNumber'
import { GraphQLError } from 'graphql'

export const parseDate = (value) => {
  let time

  if (value instanceof Date) {
    time = value
  } else if (isNumber(value) && value < 141071564057) {
    time = moment(value, 'X')
  } else if (isNumber(value) && value >= 141071564057) {
    time = moment(value, 'x')
  } else {
    time = moment(value)
  }
  if (time.isValid()) {
    return time.toDate()
  }
  const error = `Value ${value} is not a valid datetime`
  throw new GraphQLError(error)
}

export const toUTCDate = (date) => {
  if (!date) return 'n/a'
  date = new Date(date).toISOString()
  const day = date.split('T')[0].split('-')
  return `${day[1]}/${day[2]}/${day[0]}`
}

export const toChangeLogDate = (date) => {
  if (!date) return 'n/a'
  const _d = new Date(date)
  const padNum = (n) => {
    const s = new String(n)
    if (s.length === 1) return `0${s}`
    return s
  }
  return `${padNum(_d.getUTCMonth() + 1)}/${padNum(_d.getUTCDate())}/${padNum(_d.getFullYear())}`
}
