import moment from 'moment'
import logger from '../logging'
import idx from 'idx'
import { getInventoryValues } from '../schema/types/utils'

const holidays = [
  // Thanksgiving
  moment('2019-11-28').utc(),
  // Black Friday
  moment('2019-11-29').utc(),
  // TODO: remove me after, ad hoc request from #software_support
  moment('2019-12-02').utc(),
  // Xmas
  moment('2019-12-25').utc(),
  // NYE
  moment('2019-12-31').utc(),
  // NY
  moment('2020-01-01').utc(),
]

const getIsHoliday = (deliveryDate) => {
  const deliveryDateMoment = moment(deliveryDate).utc()

  let isHoliday = false

  holidays.forEach((m) => {
    if (m.isSame(deliveryDateMoment, 'day')) {
      isHoliday = true
    }
  })

  return isHoliday
}

// TODO: this validation file needs refactor
export const validateOrder = async (args, context) => {
  const { connections, user } = context
  const { input } = args

  const { logisticsDB: db } = connections || {}
  const { date, lineItems = [] } = input || {}

  const sortedLineItems = lineItems.sort((a, b) => (a.id === b.id ? 0 : a.id > b.id ? 1 : -1))

  if (!user.isAdmin && date) {
    const isHoliday = getIsHoliday(date)

    if (isHoliday) {
      return ['The delivery date you selected is a holiday! Please choose another date.']
    }
  }

  const lineItemErrors = await Promise.all(
    sortedLineItems.map(async (lineItem) => {
      const { name, skuBatchId } = lineItem

      let error = null

      if (!skuBatchId) {
        error = `line item, ${name}, is missing an assigned SkuBatch ID`
        logger.error(error)
        return error
      } else {
        // validate the skuBatch
        const skuBatch = await db.SkuBatch.findByPk(skuBatchId, {
          include: [{ model: db.Sku, as: 'sku' }],
          paranoid: false,
        })

        if (!skuBatch) {
          error = `line item, ${name}, was assigned a SkuBatch ID that doesn't exist in the database`
          logger.error(error)
          return error
        } else {
          const { sku } = skuBatch

          if (input.action === 'DELIVERY_TO_RETAILER') {
            const inventoryVals = await getInventoryValues(skuBatch, args, context)

            if (inventoryVals) {
              let startQuantity = 0
              // if edit order
              if (input.id) {
                const lastOrderState = await db.Order.findByPk(input.id, {
                  include: [{ association: db.Order.LineItems }],
                  paranoid: false,
                })

                const wasOutgoing =
                  (lastOrderState && lastOrderState.action === 'DELIVERY_TO_RETAILER') ||
                  lastOrderState.action === 'INTERNAL_TRANSFER'

                if (wasOutgoing) {
                  const previousTotalForSkuBatch = lastOrderState.lineItems
                    .filter((_lineItem) => _lineItem.skuBatchId === skuBatch.id)
                    .reduce((sum, li) => sum + li.quantity, 0)
                  startQuantity = previousTotalForSkuBatch
                }
              }

              const totalForSkuBatch = sortedLineItems
                .filter((_lineItem) => _lineItem.skuBatchId === skuBatch.id)
                .reduce((sum, li) => sum + li.quantity, 0)
              const changeInQuantity = totalForSkuBatch - startQuantity

              const available =
                (idx(inventoryVals, (_) => _.LA.available) || 0) + (idx(inventoryVals, (_) => _.OAK.available) || 0)

              if (changeInQuantity > Math.max(available, 0)) {
                const changeInQuantityString = `${changeInQuantity < 0 ? '-' : '+'}${changeInQuantity}`
                error = `line item, ${name}, will oversell SKU ${sku &&
                  sku.code}, ${available} available while ${changeInQuantityString} requested`
                logger.error(error)
                return error
              }
            }
          }

          if (!user.isAdmin) {
            if (skuBatch.deletedAt) {
              error = `line item, ${name}, was assigned an archived SKU`
              logger.error(error)
              return error
            }

            if (sku && sku.isArchived) {
              error = `line item, ${name}, was assigned a SKU with an archived product ${sku.code}`
              logger.error(error)
              return error
            }
          }
          if (!sku) {
            error = `line item, ${name}, was assigned a SKU with a product ID that doesn't exist in the database`
            logger.error(error)
            return error
          }
        }
      }
      return null // so we can use map and promise.all
    }),
  )
  return lineItemErrors.filter((er) => er) // will break if we throw errors: null[]
}
