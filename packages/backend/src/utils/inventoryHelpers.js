import get from 'lodash/get'
import { nearestWarehouseId } from '../utils/warehouseFinder'

const updateInventory = async (db, inventoryItem, inventoryLedgerItem) => {
  try {
    await db.InventoryLedgerItem.create({
      ...inventoryLedgerItem,
    })

    await db.InventoryItem.update(
      {
        ...inventoryItem,
      },
      {
        where: { id: inventoryItem.id },
      },
    )
  } catch (error) {
    console.error('utils.updateInventory unable failed', error)
    throw error
  }
}

const getDestinationWarehouse = async (db, siteName) => {
  try {
    const warehouse = await db.Warehouse.findAll({
      include: [
        {
          association: db.Warehouse.Site,
          required: true,
        },
      ],
      where: {
        '$site.name$': siteName,
      },
    })
    return warehouse[0].id
  } catch (error) {
    console.error("couldn't find destination warehouse", error)
    throw error
  }
}

const autoAssignWarehouseByLicensedLocation = async (db, licensedLocationId) => {
  try {
    const licensedLoc = await db.LicensedLocation.findByPk(licensedLocationId)
    const newWarehouseId = get(licensedLoc, 'warehouseId')
    if (newWarehouseId) return newWarehouseId
    else {
      const _nearestWarehouseId = await nearestWarehouseId(licensedLoc.zip, db)

      // Save warehouse preference to master dispensary list
      if (get(licensedLoc, 'id') && !_nearestWarehouseId) {
        const changedLicensedLocation = await db.LicensedLocation.update(
          { warehouseId: _nearestWarehouseId },
          { where: { id: get(licensedLoc, 'id') } },
        )
      }

      return await db.Warehouse.findOne({ where: { id: _nearestWarehouseId } }).id
    }
  } catch (error) {
    console.error("couldn't auto assign warehouse", error)
    throw error
  }
}

export { updateInventory, getDestinationWarehouse, autoAssignWarehouseByLicensedLocation }
