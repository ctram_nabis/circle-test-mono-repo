import idx from 'idx'

import dollarFormat from '../dollarFormat'
import titleizeDaysTillPaymentDue from '../titleizeDaysTillPaymentDue'

export const buildFinancialFields = (order) => {
  const { lineItems = [], action, daysTillPaymentDue } = order
  const isPickup = ['PICKUP_FROM_BRAND', 'RETURN_FROM_RETAILER', 'DROPOFF_TO_NABIS'].includes(action)

  const discountTotal = idx(order, (_) => _.discount) || 0
  const exciseTaxTotal = lineItems.reduce(
    (a, li) => a + (li.isExciseTaxable ? parseFloat(li.pricePerUnit) * parseFloat(li.quantity) * 0.24 : 0),
    0,
  )
  const subtotalTotal = lineItems.reduce((a, li) => a + parseFloat(li.pricePerUnit) * parseFloat(li.quantity), 0)

  const subtotal = dollarFormat(subtotalTotal || '0')
  const exciseTax = isPickup ? 'n/a' : dollarFormat(exciseTaxTotal || '0')
  const discount = isPickup ? '$0.00' : dollarFormat(discountTotal || '0')
  const paymentTerms = isPickup ? 'TRANSFER ONLY' : titleizeDaysTillPaymentDue(daysTillPaymentDue)
  const totalBalanceDue = isPickup ? '$0.00' : dollarFormat(subtotalTotal - discountTotal + exciseTaxTotal || '0')

  return {
    subtotal,
    discount,
    excise_tax: exciseTax,
    payment_terms: paymentTerms,
    total_balance_due: totalBalanceDue,
  }
}
