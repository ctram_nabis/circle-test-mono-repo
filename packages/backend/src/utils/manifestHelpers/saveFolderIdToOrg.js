import idx from 'idx'

export const saveFolderIdToOrg = async (organization, orgFolderId, db) => {
  const orgId = idx(organization, (_) => _.id)

  if (!orgId) throw new Error('Missing valid organization ID')

  await db.Organization.update(
    {
      manifestGDriveFolderId: orgFolderId,
    },
    { where: { id: orgId } },
  )

  return orgFolderId
}
