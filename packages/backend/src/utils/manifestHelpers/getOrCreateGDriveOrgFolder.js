import cfg from '../../config'

export const getOrCreateGDriveOrgFolder = (org, googleDriveClient) => {
  const { manifestGDriveFolderId } = org
  const { gdriveFolderId } = cfg.google.drive
  const orgFolderName = `${org.name}_${org.id}`

  if (!manifestGDriveFolderId) {
    // create org folder under 'Manifests [DO NOT DELETE]'

    return googleDriveClient.createFolder(orgFolderName, [gdriveFolderId])
  }

  return googleDriveClient.getFolder(manifestGDriveFolderId).then((oFolderId) => {
    if (oFolderId) return oFolderId

    return googleDriveClient.createFolder(orgFolderName, [gdriveFolderId])
  })
}
