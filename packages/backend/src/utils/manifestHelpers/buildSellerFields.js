/* eslint-disable camelcase */
import idx from 'idx'

import formatPhoneNumber from '../formatPhoneNumber'

export const buildSellerFields = (organization = null, order) => {
  const org = organization || order.organization
  const seller_name = org.name
  const seller_address_1 = `${org.address1}${org.address2 ? `, ${org.address2}` : ''}` || ''
  const seller_address_2 = `${idx(org, (_) => _.city) || ''}, ${idx(org, (_) => _.state) || ''} ${idx(
    org,
    (_) => _.zip,
  ) || ''}`
  const seller_email = idx(org, (_) => _.owner.email) || ''
  const seller_phone = formatPhoneNumber(org.phone || '')
  return {
    seller_name,
    seller_address_1,
    seller_address_2,
    seller_email,
    seller_phone,
  }
}
