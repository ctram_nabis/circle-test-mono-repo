export const sanitizeUndefinedNull = (payload) => {
  const _payload = payload
  // takes a shallow object and replaces null with empty string
  Object.entries(_payload).forEach(([key, value]) => {
    if (value == null) {
      // check for undefined and null
      _payload[key] = ''
    }
  })
  return _payload
}
