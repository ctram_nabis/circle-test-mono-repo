export const paginateLineItems = (lineItems, itemsPerPage = 20) => {
  const paginatedItems = []
  if (!lineItems.length) {
    paginatedItems.push([])
  } else {
    for (let _idx = 0; _idx < lineItems.length; _idx += itemsPerPage) {
      paginatedItems.push(lineItems.slice(_idx, _idx + itemsPerPage))
    }
  }
  return paginatedItems
}
