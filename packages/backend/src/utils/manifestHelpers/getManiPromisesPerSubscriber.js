import idx from 'idx'
import pdfFiller from 'pdffiller'
import PDFMerge from 'pdf-merge'

import { IS_PROD } from '../../constants'
import {
  getOrCreateGDriveOrgFolder,
  saveFolderIdToOrg,
  paginateLineItems,
  buildLineItemFields,
  buildManufacturerFields,
  buildSellerFields,
  buildFinancialFields,
  buildLocationFields,
  buildCreatorFields,
  buildVehicleFields,
  buildDriverFields,
  buildDateFields,
  getPdfPaths,
  sanitizeUndefinedNull,
  catchManifestError,
  createOrUpdateGDriveFile,
} from '../manifestHelpers'

const DEFAULT_MANAGER = {
  manager_name: 'Jeremy Lapeer',
  manager_phone: '628-219-4348',
}

export const getManiPromisesPerSubscriber = ({
  subscriber,
  order,
  googleDriveClient,
  db,
  manager = DEFAULT_MANAGER,
}) => {
  const addLineItem = order.subscribers && order.subscribers.length === 0

  const brandLineItems = order.lineItems
    .filter((li) => addLineItem || (idx(li, (_) => _.organization.id) || subscriber.id) === subscriber.id)
    .sort((a, b) => (a.code > b.code ? 1 : -1))
  const getFolderIdPromise = getOrCreateGDriveOrgFolder(subscriber, googleDriveClient).then((orgFolderId) =>
    saveFolderIdToOrg(subscriber, orgFolderId, db),
  )
  const pages = paginateLineItems(brandLineItems, 20)
  const pageData = pages.map((lineItemsForPage, j) => {
    const data = {
      pg_1: j + 1 || 1,
      pg_2: pages.length || 1,
      invoice_num: `${order.number}`,
      irn: order.irn ? `${order.irn}` : '',
      ...buildLineItemFields(lineItemsForPage, subscriber),
      ...buildManufacturerFields(subscriber, order),
      ...buildSellerFields(subscriber, order),
      ...buildFinancialFields(order),
      ...buildLocationFields(order),
      ...buildCreatorFields(order),
      ...buildVehicleFields(order),
      ...buildDriverFields(order),
      ...buildDateFields(order),
      ...manager,
    }

    if (IS_PROD) {
      console.log(`api.js:generating page data for page: ${j} ${JSON.stringify(data, null, 2)}`)
    }

    Object.keys(data).forEach((key) => {
      if (typeof data[key] === 'string') {
        // replace all parentheses with a space
        data[key] = data[key].replace(/([()])+/g, ' ')
      }
    })

    if (IS_PROD) {
      console.log(`api.js:after cleaning page data for page: ${j} ${JSON.stringify(data, null, 2)}`)
    }
    return data
  })

  // included models have the through table accessible in the child (wtf!!)
  const manifestGDriveFileId =
    idx(subscriber, (_) => _.OrderSubscriber.manifestGDriveFileId) || order.manifestGDriveFileId

  const subscriberPdfPaths = getPdfPaths(subscriber, order)

  const fillPdfPromisesForSubscriber = pageData.map((payload, i) => {
    const _payload = sanitizeUndefinedNull(payload)
    const pageName = `${subscriberPdfPaths.destinationPDFPath}_ordersub_${subscriber.id}_pg_${i}.pdf`

    // Future resource for promisify-ing this section, but may be irrelevant because it is inside a Promise array anyways https://github.com/pdffillerjs/pdffiller/issues/76

    return new Promise((resolve, reject) =>
      pdfFiller.fillFormWithFlatten(subscriberPdfPaths.sourcePDFPath, pageName, _payload, false, (err) =>
        err ? reject(err) : resolve(pageName),
      ),
    )
  })

  const mergePagesToOnePdf = async (subManiPaths) => {
    try {
      await PDFMerge(subManiPaths, { output: subscriberPdfPaths.finalPDFPath })
    } catch (error) {
      throw new Error(error)
    }
  }

  const createPdfPromise = Promise.all(fillPdfPromisesForSubscriber)
    .catch((error) => catchManifestError(error, 'PDFFiller/fillPdfPromisesForSubscriber'))
    .then(mergePagesToOnePdf)
    .catch((error) => catchManifestError(error, 'PDFMerge/mergePagesToOnePdf'))

  return Promise.all([createPdfPromise, getFolderIdPromise])
    .then(([_, folderId]) =>
      createOrUpdateGDriveFile(
        subscriberPdfPaths.finalPDFPath,
        subscriberPdfPaths.destinationPDFName,
        manifestGDriveFileId,
        folderId,
        googleDriveClient,
      ),
    )
    .then((manifestGDriveFileId) => {
      googleDriveClient.createPermission(manifestGDriveFileId)

      return db.OrderSubscriber.update(
        { manifestGDriveFileId },
        {
          where: {
            orderId: order.id,
            subscriberId: subscriber.id,
          },
        },
      )
    })
    .then(() => subscriberPdfPaths.finalPDFPath)
    .catch(catchManifestError)
}
