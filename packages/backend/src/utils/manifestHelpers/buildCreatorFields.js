/* eslint-disable camelcase */
import idx from 'idx'

import formatPhoneNumber from '../formatPhoneNumber'

export const buildCreatorFields = (order) => {
  const created_by_name = `${idx(order, (_) => _.creator.firstName) || ''} ${idx(order, (_) => _.creator.lastName) ||
    ''}`
  const created_by_email = idx(order, (_) => _.creator.email) || ''
  const created_by_phone = formatPhoneNumber(idx(order, (_) => _.creator.phone) || '') || ''
  return { created_by_name, created_by_email, created_by_phone }
}
