export const buildVehicleFields = ({ vehicle }) => {
  const { make = '', model = '', year = '', licensePlate = '' } = vehicle || {}
  return { vehicle_make_model: `${make} ${model} ${year}`, vehicle_plate: licensePlate }
}
