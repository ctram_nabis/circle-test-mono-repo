import idx from 'idx'

export const buildManufacturerFields = (organization, order) => {
  // reduce: batch license numbers to since instance of each
  const _lineItems = order.lineItems

  // _licenses is an array of [legalEntityName, licenseNumber]
  const _licenses = _lineItems.reduce((accum, lineItem) => {
    const license = idx(lineItem, (_) => _.batch.license)
    const alreadyExists = accum.find((item) => item[0] === (license && license.legalEntityName))
    if (license && !alreadyExists) {
      return [...accum, [license.legalEntityName, license.licenseNumber]]
    }
    return accum
  }, [])

  const licenses = _licenses.reduce((accum, license, i) => {
    const _accum = accum
    if (i < 3) {
      ;[_accum[`manufacturer_name_${i + 1}`], _accum[`manufacturer_license_${i + 1}`]] = license
      return _accum
    }
    return _accum
  }, {})

  if (licenses.length === 0) {
    return {
      manufacturer_name_1: idx(organization, (_) => _.organization.licenses[0].legalEntityName),
      manufacturer_license_1: idx(organization, (_) => _.organization.licenses[0].licenseNumber),
    }
  }
  return licenses
}
