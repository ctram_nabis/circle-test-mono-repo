import * as Sentry from '@sentry/node'
import logger from '../../logging'

export const GDriveAPIErrorLogger = (error) => {
  const msg = `[GDrive] ${error}`
  logger.error(msg)
  Sentry.captureMessage(`api.js/GDrive: error ${error}`)
}
