import * as Sentry from '@sentry/node'
import logger from '../../logging'

export const catchManifestError = (error, category = 'error') => {
  logger.error(`api.js/${category}: error: ${error}`)
  Sentry.captureMessage(`api.js/${category}: error ${error}`)
  throw new Error(error)
}
