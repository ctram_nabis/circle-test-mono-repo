/* eslint-disable camelcase */
import moment from 'moment-timezone'

import { toUTCDate } from '../parseDate'

export const buildDateFields = (order) => {
  const deliveryDate = toUTCDate(order.date)
  const estArrivalAfter =
    order.estimatedArrivalTimeAfter &&
    moment(order.estimatedArrivalTimeAfter)
      .tz('America/Los_Angeles')
      .format('HH:mm')
  const estArrivalBefore =
    order.estimatedArrivalTimeBefore &&
    moment(order.estimatedArrivalTimeBefore)
      .tz('America/Los_Angeles')
      .format('HH:mm')
  const departure_time = deliveryDate || ''
  const estimated_arrival_time = `${deliveryDate || ''} ${
    estArrivalAfter && estArrivalBefore ? `:: ${estArrivalAfter}-${estArrivalBefore}` : ''
  }`
  return { departure_time, estimated_arrival_time }
}
