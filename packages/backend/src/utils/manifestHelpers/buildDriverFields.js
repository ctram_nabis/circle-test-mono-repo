export const buildDriverFields = ({ driver }) => {
  const { firstName = '', lastName = '', driversLicense = '' } = driver || {}
  return {
    driver_name: firstName + ' ' + lastName,
    drivers_license: driversLicense,
  }
}
