export const createOrUpdateGDriveFile = (
  path,
  destinationPDFName,
  manifestGDriveFileId,
  orgFolderId,
  googleDriveClient,
) => {
  const name = `${destinationPDFName}.pdf`
  const resource = {
    name,
    parents: [orgFolderId],
  }
  if (manifestGDriveFileId) {
    return googleDriveClient.updateFile(manifestGDriveFileId, name, 'application/pdf', path)
  }
  return googleDriveClient.createFile(resource, 'application/pdf', path)
}
