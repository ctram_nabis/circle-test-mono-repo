import dollarFormat from '../dollarFormat'

export const getSuperscripts = (lineItem) => {
  let superscript = ''
  if (lineItem.isSample) {
    superscript += '\u002a'
  }
  if (!lineItem.isExciseTaxable) {
    superscript += '\u2020'
  }
  return superscript
}

export const buildLineItemFields = (lineItemsForPage, organization) => {
  const productDescription = (name, string) => (string.indexOf(name) === -1 ? `${string}, ${name}` : string)
  return lineItemsForPage.reduce((acc, val, i) => {
    const num = i + 1
    const liGmv = parseFloat(val.pricePerUnit) * parseFloat(val.quantity)
    const newLineItem = {
      [`brand_sku_${num}`]: val.code || '',
      [`product_${num}`]: productDescription(organization.name, `${val.name}${getSuperscripts(val)}` || ''),
      [`price_${num}`]: dollarFormat(val.pricePerUnit || '0'),
      [`quantity_${num}`]: `${val.quantity}` || '',
      [`total_${num}`]: dollarFormat(liGmv || '0'),
    }
    return {
      ...acc,
      ...newLineItem,
    }
  }, {})
}
