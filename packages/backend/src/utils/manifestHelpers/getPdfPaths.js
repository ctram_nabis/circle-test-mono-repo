import moment from 'moment'

import cfg from '../../config'

export const getPdfPaths = (organization, order) => {
  const specialCharacters = /[^a-zA-Z0-9-_]+/g
  const moreThanOneWhitespace = / +/
  const orderNum = order.number
  const orgName = organization.name
  const siteName = order.site && order.site.name
  const deliveryDate = moment.utc(order.date).format('MM-DD-YYYY')
  const destinationPDFName = `${orderNum}_${orgName}_${siteName}_${deliveryDate}`
    .replace(specialCharacters, '-')
    .replace(moreThanOneWhitespace, '-')
  const sourcePDFPath = `${cfg.workingDir}/templates/manifest_template.pdf`
  const destinationPDFPath = `${cfg.workingDir}/templates/temp/${destinationPDFName}`
  const finalPDFPath = `${destinationPDFPath}.pdf`
  return {
    sourcePDFPath,
    destinationPDFPath,
    finalPDFPath,
    destinationPDFName, // yeah it's not a path but this was easier heh
  }
}
