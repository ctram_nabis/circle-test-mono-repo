import isNil from 'lodash/isNil'

const titleizeDaysTillPaymentDue = (num) => {
  if (num === 0) return 'COD'
  if (isNil(num)) return 'n/a'
  return `Net ${num}`
}

export default titleizeDaysTillPaymentDue
