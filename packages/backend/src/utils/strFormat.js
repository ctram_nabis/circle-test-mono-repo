export function strFormat(str, { trim = true, lowercase, uppercase }) {
  let _str = str
  if (trim) _str = _str.trim()
  if (lowercase) _str = _str.toLowerCase()
  if (uppercase) _str = _str.toUpperCase()

  return _str
}

/*
const email = strFormat(this.props.email, {
  trim: true, 
  lowercase: true
})
*/
