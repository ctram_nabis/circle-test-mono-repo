import zipcodes from 'zipcodes'
import { WAREHOUSES } from '../constants'

/**
 * Find the nearest Warehouse for a given zipcode.
 * @param {*} zipcode Zipcode of the location to calculate the closest Warehouse's ID
 * @param {*} db The db for Sequalize. Probably in your context.
 * @returns A Warehouse ID as a string
 */
export const nearestWarehouseId = async (zipcode, db) => {
  let wId
  let minDistance = Infinity

  const allWarehouses = Object.values(WAREHOUSES)

  for (const w of allWarehouses) {
    const warehouse = await db.Warehouse.findOne({ where: { id: w.warehouseId } })
    const warehouseLocation = await db.Site.findOne({ where: { id: warehouse.siteId } })
    const warehouseDistance = zipcodes.distance(zipcode, warehouseLocation.zip)

    if (warehouseDistance < minDistance) {
      wId = w.warehouseId
      minDistance = warehouseDistance
    }
  }

  return wId
}
