import jwt from 'jsonwebtoken'
import config from '../config'

const generateToken = (subject, audience, isAdmin = false, tokenExpirationSec = 1296000) =>
  new Promise((resolve, reject) => {
    /**
     * Subject: User id.
     * Issuer: Nabis.
     * */
    const payload = {
      exp: Math.floor(Date.now() / 1000) + tokenExpirationSec, // seconds
      isAdmin,
    }
    const options = {
      subject,
      audience,
      issuer: config.auth.issuer,
    }
    return jwt.sign(payload, config.auth.secret, options, (err, token) => {
      if (err) {
        return reject(err)
      }
      return resolve(token)
    })
  })

export { generateToken }
