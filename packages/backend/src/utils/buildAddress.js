const buildAddress = (site) => {
  const address = `${site.address1}, ${site.address2 ? `${site.address2},` : ''} ${site.city}, ${site.state} ${
    site.zip
  }`
  return address
}

export default buildAddress
