import development from './development'
import staging from './staging'
import production from './production'
import testing from './testing'
import integration from './integration'

const configs = {
  production,
  staging,
  testing,
  integration,
  development,
}

const env = process.env.NODE_ENV || 'development'
const config = configs[env]

export default config
