export default {
  auth: {
    audience: {
      admin: 'admin',
      client: 'client',
    },
    issuer: 'https://staging.getnabis.com',
    masterPassword: '431ffaee-0614-4daa-bd7a-1ac5711078ba',
    secret: Buffer.from(process.env.JWT_SECRET || 'nabis-secret', 'base64'),
  },
  google: {
    drive: {
      authProviderX509CertUrl: 'https://www.googleapis.com/oauth2/v1/certs',
      authUri: 'https://accounts.google.com/o/oauth2/auth',
      clientId: '118192562204899138029',
      clientX509CertUrl:
        'https://www.googleapis.com/robot/v1/metadata/x509/stage-352%40staging-app-198921.iam.gserviceaccount.com',
      gdriveFolderId: '1p7RDmuNjA8m01NbY7xAFIiQnVm0G1smw' /* GDrive/Deliveries/Manifests */,
      projectId: 'staging-app-198921',
      tokenUri: 'https://accounts.google.com/o/oauth2/token',
    },
    maps: {
      apiKey: 'AIzaSyB9ZkMrkLRIahjdMVQexV54CiO9mLWqUI0',
    },
    serviceAccount: {
      clientEmail: 'stage-352@staging-app-198921.iam.gserviceaccount.com',
      privateKey:
        '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDBLywaWFLMFCB/\n/9UnoMAM4SwugHklP/1uz4Aa/Wp1pLlPJ07N25TnivkoUk9VHeUd+jF5u7zcmfug\n0QtFSMLUOLO/nSMkDhDpUZXgD66MTp6gEHRpQnJ5+oVaV+HdVUQmNf23OLqMvh/r\nnafqJBBleW9jFfflA7LuiDdwMmgDcWrwaW4Htg5T/r7GZsRAWkfWz4gUievgx20/\ntwEnQijkhF77xRtfrkoDkwcSp6S03c///lxnHu/9sVuVvcUIw8F7HmUVD9YxjE5r\n3uxneNcAp3oCo69HhdBBuB8vKAnQYBTHpNlPxGuveogx1xQpmnFNqZnG/5UZdDJr\nEqOBpgNrAgMBAAECggEATQ0/Fnzu/UXz1onYX1du8lQndp64cyKMN1PdASbdfPHI\nFamUdWglAps8GJiH2uezGFtPu4GlaQmjwvdN+wLz7oh1LTKELx/LH/xx/m3SzVSY\nUThu7gLaPQuuf6O4yzp0lZcITLl8MqGsgqIdtLZQseCXDFrZRodMqXY8Qmxgbypu\nOT+5b4YO/VW1WvO9YOY6NlRTr5K+N0kEx8oNA5PAf1R8HWMJSR5B4EjUcmGWDkL9\nmZ/zwXh3oriQLGd48Mip4NuQJ8+q7Ksq3MtGnr57Yuh87HWhASr3Il4rOxRFedJM\nWNEsqKqCEpb1xJZbMl7igGsOEPSmsOI6Hw4RjyYBNQKBgQDvxbieqsRyQ/S4X37K\nm15AISBe0q4y9qgv3H7Zf9N9o580dAHgzllXS1hwW/psXT8AjtVr0y8FLqjb/xaP\nZtyzSW4nUIgpjOpQjiyiY/w9Kww+6MLAngE676cbTBPrdHZe5YqrnsCR9mLvjbRH\nDMQ9g2uPBX3zrH1935ZmwOCPlQKBgQDOQkTvv4L4mn6NaQGGAU2KjT78KyDGMwS5\nE+wTP2+vz8Y4CDWNghy1WPvoSZDUO8bUDv+CUnv7/qIRbij58Bk9vyrfLqj+rQtq\nJj8CaE+X1muj9jW2A1cbzZmdjlvlOONoGeu0AeiaMiGMRrkqIo9vWMwsAUy/oPfy\n8Sujw9OG/wKBgQCtVSE2yrqC5C94TciqDRFgiNIO2n0F6JwDJYIqF6c7L1q7H5Xv\n/xxE5/oCF+v8BJiz/Y25igx20ZlOmKSlfDXWAiJ4l1Ynu2/0ViNcLa2+Rm1u2PNZ\nLaA0C+Wg7SmFUEb2KThLAtOxcQ1qoeUOa+M9mNDbrpuz2SFLKqRemdZ81QKBgAQN\nuV0c3m4UWzhcP/NqQyPd8sbx8grzJbydb55RONiztKmVP7gwPnAx94FxMK97HUU9\npjfhdx5Uusqs55FB7r0P7L/GIvweWoMTjVl2w1mwlRz70cJp/mRFfidijNvMamu5\nVrCK+Q9OvOoM2cbCNx9xF1ImFGQPvc/AfaMSTi3xAoGBAMtwqQtJ7PkTj5VRVapS\n+Wx+Ol/e4DkhwTa7FcitWOLpB5nlw8yfnrZ3pArl2MHx0ID50sGPJfKMzhM9LrEz\n1WqbbA1yDzIqKaKvZbjiJa2Tn3cMFV55GkA+9uFXCYlR5BdSS6wAk1pnfH9xUVx4\n5X5RnxfOlhZjjy8lrXz6Hp+l\n-----END PRIVATE KEY-----\n',
      privateKeyId: '80038a35720dc0b5702fa0461f8c42adb676cdba',
    },
  },
  mailChimp: {
    apiKey: 'e16c2f15aa7d503ed9c909989ea40b24-us19',
    userMailingListId: '2e6720cf6a',
  },
  nabis: {
    baseUrl: 'https://app.staging.getnabis.com',
    demoOrganizationId: 'b60db15d-2135-4fb4-81e9-fb8a80f13383',
  },
  nabisApiKey: 'nabis-api-key-ohhhh-yeahhhhh',
  nodeEnv: process.env.NODE_ENV,
  onfleet: {
    baseUrl: process.env.ONFLEET_URL || 'https://fcba35ec5330e8897d7ddd4ee628f58f@onfleet.com/api/v2/tasks/batch',
  },
  port: process.env.PORT,
  postgres: {
    logistics: {
      database: process.env.DB_DATABASE,
      dialect: 'postgres',
      host: process.env.DB_HOST,
      logging: false,
      password: process.env.DB_PASSWORD,
      port: process.env.DB_PORT,
      username: process.env.DB_USER,
    },
  },
  publicSitesOrganizationId: '5a8c3761-e9b3-4131-8b21-165387ef2516',
  sendGrid: {
    apiKey: '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',
  },
  workingDir: `${process.cwd()}/src`,
  pdfGeneratorLink: 'https://pclskn5ur0.execute-api.us-west-1.amazonaws.com/staging/',
}
