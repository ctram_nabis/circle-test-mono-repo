export default {
  auth: {
    issuer: 'https://dev.getnabis.com',
    secret: Buffer.from(process.env.JWT_SECRET || 'nabis-secret', 'base64'),
    audience: {
      admin: 'admin',
      client: 'client',
    },
    masterPassword: 'e382ffc8-d96d-47b7-987f-16ec159b889a',
  },
  google: {
    drive: {
      clientId: '685423206744-afhin4vfgm2gil1b4iog773guh09j4a0.apps.googleusercontent.com',
      projectId: 'development-app-198921',
      authUri: 'https://accounts.google.com/o/oauth2/auth',
      tokenUri: 'https://www.googleapis.com/oauth2/v3/token',
      authProviderX509CertUrl: 'https //www.googleapis.com/oauth2/v1/certs',
      clientX509CertUrl:
        'https://www.googleapis.com/robot/v1/metadata/x509/dev-334%40development-app-198921.iam.gserviceaccount.com',
      gdriveFolderId: '1Aw8b1AfmzudgDT7mwODAgVFdCEKkqsNN' /* GDrive/Deliveries/Manifests */,
    },
    maps: {
      apiKey: 'AIzaSyBXpq530P70c0iiCSJ2L6i_cfplT8iTXZY',
    },
    serviceAccount: {
      privateKeyId: '2c13ed0c9c1115f5e7c206e209a652cc2f1a6fb7',
      clientEmail: 'dev-334@development-app-198921.iam.gserviceaccount.com',
      privateKey:
        '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC+B+3jRnzZU94e\nqa8GuBdnPelgQcnXQdyO/WthKv/hZT+/SpAF4HDMyvPN+10QpeQVAMLnOkILH5u9\nodc4nm/4K/8Ma9O1mdpVlqV/+Kjo8oHt3MHo4FL5yDHCPG+v3U64Vv46gyDYsBFK\n8vEbElP6NoCCZsDAEJQiuzMkwD5CUqh7O8DPAcpi82loOYkTltniKAFZgUyFaqYw\niV0JrCPM2BDhZg+pmMQnBUUAG1OnIw0zbH/fnVd9FDAuVHoMTwTqsnWStpR77h8L\n+WDcZqtETvAfXsjQ8BwVfHya2mToWQvTN0Aze4sIkWXQdcBb9v2HDy0CEn1aaYqa\n6K5Gp8ZjAgMBAAECggEAMDIXoexXLtFoBNPZWbVn7h3UKj8VP3xrQXLBgKfktNg2\nhkAVbgAh67bPhd7qGBZS1gbW58ktRd/y4nIBAVGhqUoTSiWw0J6gMtPdzqqUwFv9\nGwRBw9vc/4/IGvC0DODuna8mlFV8h6iEAkPCZbDMzqlzB1+jJDMwNuzOiFQfljcq\nggIZThoPZa6A0qpJBiTBIe9cGYbQgFyPNfx/QDvR7K7rcOzT7av9Fy16Uuw2Ix72\nJsSPBHOPODswI8HzPxCD80oltMEGuUWKBqxj1BwzVwOldmrzIHN3jP5ewiRak0ei\nvrck6FA86qHPO8OB6EX1/dblpHUd3XuF23MXONntPQKBgQD/WOcn6wnYAFdYhmF2\na1LNMVHpIHYoxi/b5VGM5o5cVsiWJuWNZVon2co5e6W70HHIhuMdc9Naxeb+x7+V\nWCcY6hAHSgW0ZM7Wash33IfVZAiWKRGv7SwVG6iyRWZ/YjK13cVrpJkRr/ITU4Z9\nyezKLnU3SgGfzuLwvqvmqpfvjwKBgQC+hEir5KX2n+j7bsSt3pFsdLNJQYn1Beo8\n7aJEJa/0YVrwNSo8/0zKoKcyK2vdsiCQkfkeo0m5/5jQij4yabkXOv91FdigqU+7\npYUuRNDd6ZfTtER+opeCLMzyt4tvTlCwr1xZxrDMw5V9gvvZLAs6ePRYYIinuNzO\nmKXPKkGR7QKBgE57VbjYLdNc73u++c+61t1IbmOX3ZlUqLsOdY04A8NvzQERfXaD\nEz2yW8xlK6BZRc+bXczcn7aDoybl4AWX7ZRjxH9pCe3TGhufAHSOrJUyxMzaXJxU\n6jqmoKgCWqmGVg291wLpuT5NdLrp7ilVWdkehFp0/aDPpsLV5+HixwYTAoGARRXJ\nGHQYbp88J/jZ30bHKzICVX0rnqtfbmqpndagwJiRFmvg/i+B1DR/jKTMP585SdN1\nJ9QEhysVagHHXhaSZMcjHX0nidL7s8royYdwyhfL05wZ/mXNVQoT46wFJv6aKgfd\nED0JnacKzXGzC+R5yeFUkbunslJJ16zo63+iOV0CgYEA7wih3uU5xcqQ9derzTNy\nh+7xfHQKVxjYRE473NdduwprAat2aOYTx+3KZzDSJYclrhPp3EmmsDhWF4NdSbZ8\nx9RiYXBN93gdDgQk0TPCsMvfSClvbYy7XubFOJCOI3h9K208dlaEWNawmsVJoSR+\nUxcuhylf13pCwJPS/G2b3zs=\n-----END PRIVATE KEY-----\n',
    },
  },
  mailChimp: {
    apiKey: 'e6b7053c40c072175c499e3673f27654-us19',
    userMailingListId: '6ce858ad0a',
  },
  nabis: {
    baseUrl: 'http://localhost:3000',
    demoOrganizationId: 'b60db15d-2135-4fb4-81e9-fb8a80f13383',
    testOrganizationId: 'e23a881f-645a-4331-9e11-f55dbf4bbff2',
  },
  nodeEnv: process.env.NODE_ENV || 'integration',
  port: process.env.PORT || 8080,
  postgres: {
    logistics: {
      host: process.env.DB_HOST || 'localhost',
      port: process.env.DB_PORT || 5432,
      username: process.env.DB_USER || 'Nabis',
      password: process.env.DB_PASSWORD || '',
      database: process.env.DB_DATABASE || 'Logistics',
      dialect: 'postgres',
      logging: false,
    },
  },
  sendGrid: {
    apiKey: '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',
  },
  workingDir: `${process.cwd()}/src`,
  onfleet: {
    baseUrl: process.env.ONFLEET_URL || 'https://fcba35ec5330e8897d7ddd4ee628f58f@onfleet.com/api/v2/tasks/batch',
  },
  publicSitesOrganizationId: '583b40f3-03cf-41c1-a84a-8735f9e00b51',
  nabisApiKey: 'nabis-api-key-lolol',
}
