export default {
  auth: {
    audience: { admin: 'admin', client: 'client' },
    issuer: 'https://getnabis.com',
    masterPassword: 'ebb36b13-19e8-436d-92ad-931f2c340db3',
    secret: Buffer.from(process.env.JWT_SECRET || 'nabis-secret', 'base64'),
  },
  google: {
    drive: {
      authProviderX509CertUrl: 'https://www.googleapis.com/oauth2/v1/certs',
      authUri: 'https://accounts.google.com/o/oauth2/auth',
      clientId: '108409846584732564696',
      clientX509CertUrl:
        'https://www.googleapis.com/robot/v1/metadata/x509/prod-4%40production-app-198921.iam.gserviceaccount.com',
      gdriveFolderId: '1F4HIHeR50lyzJlr7yTtNcGUMiVc8dZA2' /* GDrive/Deliveries/Manifests */,
      projectId: 'production-app-198921',
      tokenUri: 'https://accounts.google.com/o/oauth2/token',
    },
    maps: {
      apiKey: 'AIzaSyB9ZkMrkLRIahjdMVQexV54CiO9mLWqUI0',
    },
    serviceAccount: {
      clientEmail: 'prod-4@production-app-198921.iam.gserviceaccount.com',
      privateKey:
        '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC39+/PQnnXGbJX\nhbhPXFYYt+RcnL3itZym0Lor2qUBMRMNU41vs7YpZoMOw0XIC8bzblvh456Rt763\nMBA5z4DWaVWyDyx1xqch79mfYePp3b8WXC1yn/EVAaq3VBBCzsSeJOS/ybceq9h1\nvPuhQh8ClFqW3mxMsDFYfq7hrqg9kfXsNfW0ZZQ+fNNf0DbrWmBLcqEIZ4ukv5zh\nrQpX/DNiVTC4CabL773LlliK/PgCAVWHJGCeVt8n12KCdsNak4yCaHrhEGgS0Ft3\nwywWXRMp5SVXhW5BkKanaQ5hvkoCzrhQovJKspT05wx2FaAD/BwT6xhnXM7qdd4G\nexK1WQbBAgMBAAECggEAAZ5IOUID8Vz0ASoXd3c/+KKxexET6IkhciQ25wja17pX\nSRA5l8h0a0PQFlKoa/ySiPSpEUEf1XdQ0aq2EdQd9YTisJjKktSnBrjl6K2DRDzj\nn+46ph37EGy91DQEmpnYB+4aJwe+uSHY2xxI+F+tByhH7bjSKhCItpcSscCIEVaS\nATgiyBHS1cyyEOZQ/MUV1vJ6HMTyPM4+WP/6JgRHZPCq16FiZrgRtngr4IAKJ+o6\n9/elk4OH3YZLpXUMAuQvne8yH9R9AbIgH6SgsriQvQY+eMkdf1OMoteIdUgRcbjY\nBi5FRsy1YE9EOZgRPnwGwZd43COt6Kr/qPnKA4wP2QKBgQD4tpksyq5L61YYfZ+q\nduM17xhA1MYXi/o+pTOC8WVes5cnPPmRKBF8NlAKMNuzZ2K3KM14dthn4SNH+sre\nmJD70/v1OGCvSDk8nH6luX1fhFeygoFwI+px5JMh5B/l8SaKbh7h4inTXQF/0g0/\nIJoJqTc+J1tLMwX/Y1yQUk9xlQKBgQC9W70zZVb2yQkJqWb++98G0lUFSEDeHXEm\nnuldqXPKJmOLrgdpTJ+slYxQRi8vwNvzUWkYCpUqpsVYlgR/UVp8M4CzYrJ1b0cM\nTw4yuZCtY9bxJ1mSA23TyE+vSvv8skdD2tYDDizCB4Oedw2Omm/z8kLca7pxk/QN\nuAM0TiINfQKBgQCjrrCSTPV2m3P2/D4nVsTjyu4ARMf12TahIeILzEi1kaXpufRZ\nNrCDOc9lVZBRurm55w2KuVRk0UOB7Ki05F2Ojo2kbi0bToH0xN2IvdGoGhYJMmBz\nZ/+0YwT96f+gOhvuQNjsOgbixrPk5l23bQJTV10293navMK2uDLD/JeyHQKBgFqg\ne48Lo3F9igvLvfWvinBzXimnCRTFNpBrYflnznkjomUJp5ZoC8vHz13kGVVbkmmD\nvy64cef15WGeewyMXSgwZhp01ECMP47Oj1cLpQi3TfohASd26KuBpVGaK7wnfyAk\nrmTL9fPvMLk49Y4YsBgrDID+Nj+Btj6SoUX5VybpAoGBAJ/RFZyagKstdrlqCyyy\nKBY0H7SKh64ILv+7K3ALoICzFtWjF+XOEQzvKsNx/+56LzzwTTFCDEH4rliW3ow5\nEDZ4ieetQAXq16VxLZ1npWOYHUZYf9TTpV3zceiBSBXLEBg8Iqp1sUwOBv289a7E\nFvyRAUMWbkwDIgQfvyJaNcUq\n-----END PRIVATE KEY-----\n',
      privateKeyId: '0920150d14183680369b99e5aa680e48e9d21738',
    },
  },
  mailChimp: {
    apiKey: '2c00b45053c08a357b308a70658bc68e-us19',
    userMailingListId: '9f1b8f8665',
  },
  nabis: {
    baseUrl: 'https://app.getnabis.com',
    demoOrganizationId: 'b60db15d-2135-4fb4-81e9-fb8a80f13383',
  },
  nabisApiKey: 'nabis-api-key-uh-what-r-u-doingggg',
  nodeEnv: process.env.NODE_ENV,
  onfleet: {
    baseUrl: process.env.ONFLEET_URL || 'https://fcba35ec5330e8897d7ddd4ee628f58f@onfleet.com/api/v2/tasks/batch',
  },
  port: process.env.PORT,
  postgres: {
    logistics: {
      database: process.env.DB_DATABASE,
      dialect: 'postgres',
      host: process.env.DB_HOST,
      logging: false,
      password: process.env.DB_PASSWORD,
      port: process.env.DB_PORT,
      username: process.env.DB_USER,
    },
  },
  publicSitesOrganizationId: '0220bb6f-ebac-43fc-bfff-400ae4c45187',
  sendGrid: {
    apiKey: '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',
  },
  workingDir: `${process.cwd()}/src`,
  pdfGeneratorLink: 'https://47ksdcqfl6.execute-api.us-west-1.amazonaws.com/prod/',
}
