/**
 * TODO: Remove `any` where possible and do some ACTUAL typing
 */
import atob from 'atob'
import { google } from 'googleapis'
import idx from 'idx'
import Sequelize from 'sequelize'
import stream from 'stream'
import cfg from '../../config'
import { NABIS_ORGANIZATIONS } from '../../constants'
import logger from '../../logging'
import { queryOrderForManifest } from '../../schema/resolvers/manifestResolver'
import {
  buildCreatorFields,
  buildDateFields,
  buildDriverFields,
  buildLocationFields,
  buildVehicleFields,
  createOrUpdateGDriveFile,
  GDriveAPIErrorHandler,
  generatePDFData,
  getDateAndTaxInfo,
  getOrCreateGDriveOrgFolder,
  saveFolderIdToOrg,
} from './utils'

const subscriberQuery = `
  SELECT "Organization".*, "User".email as "ownerEmail"
  FROM "OrderSubscriber"
  INNER JOIN "Organization"
  ON "OrderSubscriber"."subscriberId" = "Organization".id
  INNER JOIN "User"
  ON "User".id = "Organization"."ownerId"
  WHERE "OrderSubscriber"."orderId" = :orderId AND
  "OrderSubscriber"."subscriberId" != :organizationId;
`

export class GDriveAPI {
  /*
   * Integration resolvers
   */
  resolvers = {
    generateMyMans: async (order, db, googleDriveClient, sequelize, user) => {
      const orderNumber = String(order.number)
      const brand = order.organization && order.organization.name
      const brandId = order.organization && order.organization.id

      const { deliveryDateFormatted, TAX_PERCENTAGE, TAX_RATE, createdAtFormatted } = getDateAndTaxInfo(order)

      const subscribersRaw = await sequelize.query(subscriberQuery, {
        replacements: { orderId: order.id, organizationId: brandId },
        type: Sequelize.QueryTypes.SELECT,
      })

      const subscribers = subscribersRaw && subscribersRaw.length > 0 ? subscribersRaw : [order.organization]

      const locationFields: any = buildLocationFields(order)
      const creatorFields = buildCreatorFields(order)
      const vehicleFields = buildVehicleFields(order)
      const driverFields = buildDriverFields(order)
      const dateFields = buildDateFields(order)

      const buyer = {
        name: locationFields.recipient_name,
        contact: locationFields.recipient_poc_name,
        phone: locationFields.recipient_poc_phone,
        email: locationFields.recipient_poc_email,
        license: locationFields.recipient_license,
        address: `${locationFields.recipient_address_1}${
          locationFields.recipient_address_2 ? `, ${locationFields.recipient_address_2}` : ''
        }`,
      }

      const pageData = {
        buyer,
        order,
        TAX_RATE,
        TAX_PERCENTAGE,
        orderNumber,
        db,
        dateFields,
        locationFields,
        driverFields,
        vehicleFields,
        brand,
        deliveryDateFormatted,
        createdAtFormatted,
        creatorFields,
      }

      const NABIS_PROMISE = generatePDFData({ lineItems: order.lineItems, pageData, organization: null })

      const brandEmail = pageData.order.organization.owner.email

      const SUBSCRIBER_PROMISE_ARRAY = subscribers.map((sub) => {
        const lineItems = order.lineItems.filter((li) => idx(li, (_) => _.organizationId) === sub.id)

        return generatePDFData({ lineItems, pageData, organization: sub, brandEmail })
      })

      const PROMISE_ARRAY = [NABIS_PROMISE].concat(SUBSCRIBER_PROMISE_ARRAY)

      const [nabisResponse, ...subscriberResponses]: any[] = await Promise.all(PROMISE_ARRAY)

      // CREATE PROMISE ARRAY FOR UPDATING ORDERSUBSCRIBER.invoicesS3FileLink with returned S3 links
      const SUBSCRIBER_UPDATE_PROMISE_ARRAY = []

      subscriberResponses.forEach((res) => {
        if (res.status === 200) {
          const decoded = JSON.parse(atob(res.data.body))
          const invoicesS3FileLink = decoded.link
          const SENT_DATA = JSON.parse(res.config.data)
          const subscriberId = idx(SENT_DATA, (_) => _.DATA_FOR_PDF.subscriberId)

          if (subscriberId) {
            SUBSCRIBER_UPDATE_PROMISE_ARRAY.push(
              new Promise(async (resolve, reject) => {
                try {
                  const existingSubscriber = await db.OrderSubscriber.findOne({
                    where: {
                      orderId: order.id,
                      subscriberId,
                    },
                  })

                  if (!existingSubscriber) {
                    const newSubscriber = await db.OrderSubscriber.create({
                      orderId: order.id,
                      subscriberId,
                      invoicesS3FileLink,
                    })

                    return resolve(newSubscriber)
                  }

                  const [_success, [updatedSubscriber]] = await db.OrderSubscriber.update(
                    { invoicesS3FileLink },
                    {
                      where: {
                        orderId: order.id,
                        subscriberId,
                      },
                      returning: true,
                    },
                  )

                  return resolve(updatedSubscriber)
                } catch (err) {
                  return reject(err)
                }
              }),
            )
          }
        }
      })

      await Promise.all(SUBSCRIBER_UPDATE_PROMISE_ARRAY)

      // MAIN NABIS RESPONSE FOR THE FULL INVOICE IN POV OF ADMINS
      if (nabisResponse.status === 200) {
        const decoded = JSON.parse(atob(nabisResponse.data.body))
        const apSummaryFileName = `${orderNumber}_AP_Summary`

        const NO_AP_SUMMARY_RESPONSE = 'No AP Summary'
        const UPLOAD_S3_FAILURE_RESPONSE = 'Upload failed'

        // AP Summary (GDrive File ID)
        let apSummaryGDriveFileId
        // Rest of Invoice/Manifest (S3 Link)
        const invoicesS3FileLink = decoded.link

        if (invoicesS3FileLink === UPLOAD_S3_FAILURE_RESPONSE) {
          throw new Error('Invoice generation (and uploading) has failed. Please try again!')
        }

        if (decoded.file !== NO_AP_SUMMARY_RESPONSE) {
          const folderId = await getOrCreateGDriveOrgFolder({ org: order.organization, cfg, googleDriveClient })
          saveFolderIdToOrg(order.organization, folderId, db)

          const fileObject = atob(decoded.file)
          const buf = Buffer.from(fileObject, 'binary')
          const buffer = Uint8Array.from(buf)
          const bufferStream = new stream.PassThrough()
          bufferStream.end(buffer)

          apSummaryGDriveFileId = await createOrUpdateGDriveFile({
            content: bufferStream,
            fileName: apSummaryFileName,
            fileId: order.apSummaryGDriveFileId || undefined,
            folderId,
            googleDriveClient,
          })

          googleDriveClient.createPermission(apSummaryGDriveFileId)
        }

        db.Order.update(
          { apSummaryGDriveFileId, invoicesS3FileLink },
          {
            where: {
              id: order.id,
            },
          },
        )

        const isNabis = user.organizations.find((org) => org.id === NABIS_ORGANIZATIONS.DELIVERIES)

        // if the user is not part of the Nabis org, we will not expose any of the ROOT invoice
        if (Boolean(!isNabis)) return {}

        return { apSummaryGDriveFileId, invoicesS3FileLink }
      } else {
        throw new Error(`Bad Response: ${nabisResponse.status}: ${nabisResponse.statusText}`)
      }
    },
    generateManifestHook: async (order, db, googleDriveClient, sequelize, user) => {
      // CREATING ORDER RUNS THROUGH HERE FIRST

      const _order = await queryOrderForManifest(order.id, { logisticsDB: db })

      return googleDriveClient.resolvers.generateMyMans(_order, db, googleDriveClient, sequelize, user)
    },
  }

  // (2) declarations below for bypassing Typescript errors
  config: any
  client: any

  constructor(config) {
    this.config = config
    const jwtClient = new google.auth.JWT(config.clientEmail, null, config.privateKey, [
      'https://www.googleapis.com/auth/drive',
    ])
    this.client = google.drive({
      version: 'v3',
      auth: jwtClient,
    })
  }

  getFolder = (folderId) =>
    new Promise((resolve, reject) =>
      this.client.files.get(
        {
          fileId: folderId,
        },
        (err, file) => {
          if (err) {
            return GDriveAPIErrorHandler(err)
          }
          return resolve(file.data && file.data.id)
        },
      ),
    )

  createFolder = (folderName, parents, fields = 'id') =>
    new Promise((resolve, reject) => {
      const fileMetadata = {
        name: folderName,
        mimeType: 'application/vnd.google-apps.folder',
        parents, // Parent folder ID
      }
      return this.client.files.create(
        {
          resource: fileMetadata,
          fields,
        },
        (err, file) => {
          if (err) {
            return GDriveAPIErrorHandler(err)
          }
          return resolve(file.data && file.data.id)
        },
      )
    })

  // updateFolder = () => {}

  // deleteFolder = () => {}

  createFile = (resource, mimeType, content, fields = 'id') =>
    new Promise((resolve, reject) => {
      const media = {
        mimeType,
        body: content,
      }
      return this.client.files.create(
        {
          resource,
          media,
          fields,
        },
        (err, file) => {
          if (err) {
            return GDriveAPIErrorHandler(err)
          }
          return resolve(file.data && file.data.id)
        },
      )
    })

  updateFile = (fileId, name, mimeType, content) =>
    new Promise((resolve) => {
      const media = {
        mimeType,
        body: content,
      }
      return this.client.files.update(
        {
          fileId,
          resource: { name },
          media,
        },
        (err, file) => {
          if (err) {
            return GDriveAPIErrorHandler(err)
          }
          return resolve(file.data && file.data.id)
        },
      )
    })

  deleteFile = (fileId) =>
    new Promise((resolve, reject) =>
      this.client.files.delete(
        {
          fileId,
        },
        (err) => {
          if (err) {
            return GDriveAPIErrorHandler(err)
          }
          return resolve(fileId)
        },
      ),
    )

  upsertFileAndFolder = (path, name, orgId, db) => {
    const getGDriveFolder = ({ name: _name, id, manifestGDriveFolderId }) => {
      const { gdriveFolderId } = cfg.google.drive
      const orgFolderName = `${_name}_${id}`

      const writeFolderToDb = async (googleId) => {
        await db.Organization.update(
          {
            manifestGDriveFolderId: googleId,
          },
          {
            where: {
              id: orgId,
            },
          },
        )
        return googleId
      }

      const findFolderInGDrive = () => this.getFolder(manifestGDriveFolderId)

      const createGDriveFolder = () => this.createFolder(orgFolderName, [gdriveFolderId]).then(writeFolderToDb)

      const folderInDatabase = !!manifestGDriveFolderId
      if (folderInDatabase) {
        return findFolderInGDrive().then((folderIdInGDrive) => {
          const gDriveHasFolder = !!folderIdInGDrive
          if (gDriveHasFolder) {
            return folderIdInGDrive
          }
          return createGDriveFolder()
        })
      }
      return createGDriveFolder()
    }

    const insertFileGetFileId = (folderId) =>
      this.createFile(
        {
          name,
          parents: [folderId],
        },
        'application/pdf',
        path,
      )

    return db.Organization.findOne({ where: { id: orgId } })
      .then(getGDriveFolder)
      .then(insertFileGetFileId)
  }

  createPermission = async (fileId) => {
    try {
      logger.info('*********** ~~~~~~~~ GDRIVE SET PERMISSION FOR READER ~~~~~~~~***********')

      await this.client.permissions.create({
        fields: 'id',
        fileId,
        requestBody: {
          role: 'reader',
          type: 'anyone',
        },
      })
    } catch (error) {
      logger.error(`GDrive Error for fileId ${fileId}`, error.message)
      throw new Error(`GDrive Error for fileId ${fileId}`)
    }

    /**
     * KEEP THIS CODE FOR LATER
     * Google does not support tranfering ownership
     */

    // logger.info('*********** ~~~~~~~~ GDRIVE SET PERMISSION FOR OWNER ~~~~~~~~***********')

    // await this.client.permissions.create({
    //   fields: 'id',
    //   fileId,
    //   requestBody: {
    //     emailAddress: 'deliveries@nabis.com',
    //     role: 'owner',
    //     type: 'user',
    //   },
    //   transferOwnership: true,
    // })

    logger.info('*********** ~~~~~~~~ GDRIVE - createPermission COMPLETED ~~~~~~~~***********')
  }

  updatePermission = (fileId, permissionId) =>
    new Promise((resolve) =>
      this.client.permissions.update(
        {
          fileId,
          permissionId,
        },
        (err, result) => {
          if (err) {
            return GDriveAPIErrorHandler(err)
          }
          return resolve(result)
        },
      ),
    )

  deletePermission = (fileId, permissionId) =>
    new Promise((resolve) =>
      this.client.permissions.delete(
        {
          fileId,
          permissionId,
        },
        (err, result) => {
          if (err) {
            return GDriveAPIErrorHandler(err)
          }
          return resolve(result)
        },
      ),
    )

  sanitizeUndefinedNull = (payload) => {
    const _payload = payload
    // takes a shallow object and replaces null with empty string
    Object.entries(_payload).forEach(([key, value]) => {
      if (value == null) {
        // check for undefined and null
        _payload[key] = ''
      }
    })
    return _payload
  }
}

export default GDriveAPI
