export const NABIONE_INC = 'deliveries-norcal@getnabis.com'
export const NULL_TITLEHOLDER_KEY = 'noTitleholder'
export const DISTRIBUTOR = {
  address1: '300 Pendleton Way, Ste. 348, ',
  address2: 'Oakland, CA 94621',
  contact: 'Ely Ayala',
  license: 'C11-0000150-LIC',
  name: 'Nabione, Inc. dba Nabis',
  phone: '(415) 264-0049',
  mailing: {
    address1: '1065 Folsom St, ',
    address2: 'San Francisco, CA 94103',
  },
}
export const MANAGER = {
  name: 'Jeremy Lapeer',
  phone: '628-219-4348',
}
export const TITLEHOLDER_LICENSE_TYPE_KEY = 'LICENSE'
export const TITLEHOLDER_ORG_TYPE_KEY = 'ORGANIZATION'
