import { StringIdGenerator } from './StringIdGenerator'

/**
 *
 * THIS IS FOR SUBINVOICE INCREMENTING A => B => ... => Z => AA => ... => infinity
 * https://stackoverflow.com/questions/12504042/what-is-a-method-that-can-be-used-to-increment-letters/34483399
 */

export const generateLetters = (titleholderKeys) => {
  const arr = []

  const ids = new StringIdGenerator()

  titleholderKeys.forEach(() => {
    arr.push(ids.next())
  })

  return arr
}
