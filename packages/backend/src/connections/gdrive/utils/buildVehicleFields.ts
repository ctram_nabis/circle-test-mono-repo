/* eslint-disable camelcase */
import idx from 'idx'

export const buildVehicleFields = (order) => {
  const vehicle_make_model = order.vehicle
    ? `${idx(order, (_) => _.vehicle.make) || ''} ${idx(order, (_) => _.vehicle.model) || ''} ${idx(
        order,
        (_) => _.vehicle.year,
      ) || ''}`
    : ''

  const vehicle_plate = idx(order, (_) => _.vehicle.licensePlate) || ''

  return { vehicle_make_model, vehicle_plate }
}
