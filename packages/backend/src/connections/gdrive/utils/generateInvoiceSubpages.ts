/**
 * TODO: Remove `any` where possible
 */
/* eslint-disable camelcase */
import idx from 'idx'
import uniqBy from 'lodash/uniqBy'
import dollarFormat from '../../../utils/dollarFormat'
import { TITLEHOLDER_LICENSE_TYPE_KEY } from '../constants'
import { calculateMoney } from './calculateMoney'
import { getProductDescription } from './getProductDescription'

const EMPTY_FALLBACK = ''

export const generateInvoiceSubpages = ({
  buyer,
  financialFields,
  invoiceLetters,
  lineItemsReduced,
  order,
  TAX_RATE,
  TAX_PERCENTAGE,
  titleholderIdArray,
}) => {
  const invoiceLettersClone = [...invoiceLetters]

  return titleholderIdArray.map((id) => {
    const titleholderObj =
      lineItemsReduced.licensesOrOrganizations.find(
        (licOrOrg) => idx(licOrOrg, (_) => _.dataValues.id) === id || licOrOrg.id === id,
      ) || {}

    const titleholder = titleholderObj.dataValues
    const type = titleholderObj.type

    const lineItems = lineItemsReduced.titleholders[id]
    const manufacturers = lineItemsReduced.manufacturersPerTitleholder[id] || []

    const { lineItemSubtotalBeforeDiscount, totalExciseTax } = calculateMoney({
      lineItems,
      siteUsState: order.site.state,
      deliveryDate: order.date,
    })

    const discount = lineItems.reduce((sum, li) => sum + parseFloat(li.discount), 0)

    const titleholderDetails = {
      name: '',
      contact: '',
      phone: '',
      email: '',
    }

    const isLicenseType = type === TITLEHOLDER_LICENSE_TYPE_KEY

    if (isLicenseType) {
      // Sequelize super-nested association(s) truncated `legalEntityName` => `legalEntityNa`
      titleholderDetails.name =
        titleholder.legalEntityNa ||
        titleholder.legalEntityName ||
        (titleholder.dataValues && titleholder.dataValues.legalEntityName) ||
        EMPTY_FALLBACK
      titleholderDetails.contact = titleholder.contactName || EMPTY_FALLBACK
      titleholderDetails.phone = titleholder.contactPhone || EMPTY_FALLBACK
      titleholderDetails.email = titleholder.contactEmail || EMPTY_FALLBACK
    } else {
      // org-type titleholder
      // org should always have an owner
      const titleholderOwner = titleholder.owner && titleholder.owner.dataValues

      const titleholderFirstName = titleholderOwner.fi || titleholderOwner.firstName
      const titleholderLastName = titleholderOwner.la || titleholderOwner.lastName
      const titleholderEmail = titleholderOwner.em || titleholderOwner.email
      const titleholderPhone = titleholder.phone || titleholderOwner.ph || titleholderOwner.phone

      titleholderDetails.name = titleholder.name
      titleholderDetails.contact = titleholderFirstName
        ? `${titleholderFirstName}${titleholderLastName ? ` ${titleholderLastName}` : ''}`
        : EMPTY_FALLBACK
      titleholderDetails.phone = titleholderPhone || EMPTY_FALLBACK
      titleholderDetails.email = titleholderEmail || EMPTY_FALLBACK
    }

    return {
      invoiceNumber: invoiceLettersClone.shift(),
      irn: order.irn,
      buyer,
      titleholder: titleholderDetails,
      manufacturers: (uniqBy(manufacturers, 'licenseNumber') as any)
        .map((m) => `${m.legalEntityName} (${m.licenseNumber})`)
        .join(', '),
      shipmentContents: lineItems.map((li, index) => {
        const organizationDba = idx(li, (_) => _.skuBatch.sku.organization.doingBusinessAs) || ''

        const taxableLineItemTotal = parseFloat(li.pricePerUnit) * parseFloat(li.quantity)

        const _exciseTax = li.isExciseTaxable ? taxableLineItemTotal * TAX_RATE : 0

        const lineItemTotal = taxableLineItemTotal - parseFloat(li.discount) + _exciseTax

        return {
          index: index + 1,
          batch: idx(li, (_) => _.skuBatch.batch.code) || 'n/a',
          productDescription: getProductDescription(organizationDba, `${li.name}` || ''),
          pricePerUnit: dollarFormat(li.pricePerUnit),
          isSample: li.isSample,
          quantity: li.quantity,
          discount: `(${dollarFormat(li.discount)})`,
          exciseTax: dollarFormat(_exciseTax),
          taxPercentage: TAX_PERCENTAGE,
          lineItemTotal: {
            display: dollarFormat(lineItemTotal),
            trueValue: lineItemTotal,
          },
          rejected: undefined,
        }
      }),
      pricingDetails: {
        subtotal: dollarFormat(lineItemSubtotalBeforeDiscount),
        discount: `(${dollarFormat(discount)})`,
        exciseTax: dollarFormat(totalExciseTax),
        taxPercentage: TAX_PERCENTAGE,
        totalPageBalance: undefined,
        totalOrderBalance: financialFields.total_balance_due,
        paymentTerms: financialFields.payment_terms,
        // TODO: okay to be undefined for now
        amountPaid: undefined,
        hasUnalterablePaymentTerms: order.hasUnalterablePaymentTerms,
        mustPayPreviousBalance: order.mustPayPreviousBalance,
      },
    }
  })
}
