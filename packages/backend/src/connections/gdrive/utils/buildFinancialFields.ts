/* eslint-disable camelcase */
import idx from 'idx'
import dollarFormat from '../../../utils/dollarFormat'
import titleizeDaysTillPaymentDue from '../../../utils/titleizeDaysTillPaymentDue'
import { calculateMoney } from './calculateMoney'

export const buildFinancialFields = (order, lineItems) => {
  const isPickup = ['PICKUP_FROM_BRAND', 'RETURN_FROM_RETAILER', 'DROPOFF_TO_NABIS'].includes(order.action)

  const { subtotal, totalExciseTax } = calculateMoney({
    lineItems,
    discount: order.discount,
    siteUsState: order.site.state,
    deliveryDate: order.date,
    surcharge: parseFloat(order.surcharge) || 0,
  })

  const _discount = idx(order, (_) => _.discount) || 0
  const _surcharge = idx(order, (_) => _.surcharge) || 0

  const discount = isPickup ? 0 : _discount
  const payment_terms = isPickup ? 'TRANSFER ONLY' : titleizeDaysTillPaymentDue(order.daysTillPaymentDue)
  const total_balance_due = isPickup ? 0 : subtotal - parseFloat(_discount) + parseFloat(_surcharge) + totalExciseTax

  return {
    subtotal: dollarFormat(subtotal || '0'),
    discount: `(${dollarFormat(discount)})`,
    excise_tax: dollarFormat(totalExciseTax),
    payment_terms,
    total_balance_due: dollarFormat(total_balance_due),
    netOffTotal: _surcharge - discount,
  }
}
