import moment from 'moment-timezone'

export const getDateAndTaxInfo = (order) => {
  const LA_TZ = 'America/Los_Angeles'

  const deliveryDateMoment = moment.utc(order.date).tz(LA_TZ)
  const createdAtMoment = moment.utc(order.createdAt).tz(LA_TZ)

  const is2020OrLater = deliveryDateMoment >= moment.utc('2020-01-01').tz(LA_TZ)
  const deliveryDateFormatted = deliveryDateMoment.format('MM-DD-YYYY')
  const createdAtFormatted = createdAtMoment.format('LLLL')

  const TAX_RATE = is2020OrLater ? 0.27 : 0.24
  const TAX_PERCENTAGE = TAX_RATE * 100

  return {
    deliveryDateFormatted,
    deliveryDateMoment,
    is2020OrLater,
    TAX_PERCENTAGE,
    TAX_RATE,
    createdAtMoment,
    createdAtFormatted,
  }
}
