export const saveFolderIdToOrg = (organization, orgFolderId, db) => {
  const orgId = organization && organization.id
  db.Organization.update(
    {
      manifestGDriveFolderId: orgFolderId,
    },
    { where: { id: orgId } },
  )
  return orgFolderId
}
