/* eslint-disable camelcase */
import formatPhoneNumber from '../../../utils/formatPhoneNumber'

export const buildBrandFields = (order, organization = null, brandEmail) => {
  const isSubscriber = Boolean(organization)

  const _organization = isSubscriber ? organization : order.organization

  const { address1 = '', address2 = '', city = '', name = '', phone = '', state = '', zip = '' } = _organization || {}

  const orgDetails = {
    name,
    address1,
    address2,
    city,
    state,
    zip,
    phone,
    email: isSubscriber ? brandEmail : _organization.owner && _organization.owner.email,
  }

  const brand_name = orgDetails.name || ''
  const brand_address_1 = `${orgDetails.address1}${orgDetails.address2 ? `, ${orgDetails.address2}` : ''}` || ''
  const brand_address_2 = `${orgDetails.city || ''}, ${orgDetails.state || ''} ${orgDetails.zip || ''}`
  const brand_email = orgDetails.email || ''
  const brand_phone = formatPhoneNumber(orgDetails.phone || '')

  return {
    brand_name,
    brand_address_1,
    brand_address_2,
    brand_email,
    brand_phone,
  }
}
