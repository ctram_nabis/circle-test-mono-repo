/* eslint-disable camelcase */
import idx from 'idx'
import formatPhoneNumber from '../../../utils/formatPhoneNumber'
import { NABIONE_INC } from '../constants'

export const buildLocationFields = (order) => {
  const isPickup = ['PICKUP_FROM_BRAND', 'RETURN_FROM_RETAILER', 'DROPOFF_TO_NABIS'].includes(order.action)

  const warehouse = order.warehouse

  // build source
  let source = {
    source_name: idx(warehouse, (_) => _.site.name) || 'Nabione, Inc.',
    source_license: idx(warehouse, (_) => _.license.licenseNumber) || 'C11-0000497-LIC',
    source_address_1: idx(warehouse, (_) => _.site.address1)
      ? `${warehouse.site.address1}${idx(warehouse, (_) => _.site.address2) ? `, ${warehouse.site.address2}` : ''}` // address1 exists
      : '300 Pendleton Way, Ste 348', // else
    source_address_2: `${idx(warehouse, (_) => _.site.city) || 'Oakland'}, ${idx(warehouse, (_) => _.site.state) ||
      'CA'} ${idx(warehouse, (_) => _.site.zip) || '94621'}`,
    source_poc_name: idx(warehouse, (_) => _.site.pocName) || 'Joey Zilioli',
    source_poc_phone: formatPhoneNumber(idx(warehouse, (_) => _.site.pocPhoneNumber) || '628-219-4330'),
    source_poc_email: idx(warehouse, (_) => _.site.pocEmail) || NABIONE_INC,
  } as object

  const getLegalEntityName = () => {
    /**
     * 1. Use Order.LicensedLocation
     * 2. Map through licenses associated and find the matching
     *    license number and grab the Legal Entity Name
     * 3. Use Order.Site.Licenses
     * 4. Map through licenses associated and find the matching
     *    license number and grab the Legal Entity Name
     * 5. If none are found above, then use Order.Site.Name
     */
    let legalEntityName = idx(order, (_) => _.site.name) || ''
    const { siteLicenseNum } = order
    let foundLicense = false

    legalEntityName = (idx(order, (_) => _.licensedLocation.licenses) || []).reduce((acc, val) => {
      if (foundLicense) {
        return acc
      }
      if (val.licenseNumber === siteLicenseNum) {
        foundLicense = true
        return val.legalEntityName
      }
      return acc
    }, legalEntityName)

    legalEntityName = (idx(order, (_) => _.site.siteLicensedLocation.licenses) || []).reduce((acc, val) => {
      if (foundLicense) {
        return acc
      }
      if (val.licenseNumber === siteLicenseNum) {
        foundLicense = true
        return val.legalEntityName
      }
      return acc
    }, legalEntityName)

    return legalEntityName
  }

  // build recipient
  let recipient = {
    recipient_name: `${getLegalEntityName()} | D.B.A. ${idx(order, (_) => _.site.name) || ''}`,
    recipient_license: order.siteLicenseNum || '',
    recipient_address_1: `${order.site.address1}${order.site.address2 ? `, ${order.site.address2}` : ''}` || '',
    recipient_address_2: order.site
      ? `${idx(order, (_) => _.site.city) || ''}, ${idx(order, (_) => _.site.state) || ''} ${idx(
          order,
          (_) => _.site.zip,
        ) || ''}`
      : '',
    recipient_poc_name: idx(order, (_) => _.site.pocName) || '',
    recipient_poc_phone: formatPhoneNumber(idx(order, (_) => _.site.pocPhoneNumber) || ''),
    recipient_poc_email: idx(order, (_) => _.site.pocEmail) || '',
  } as object

  if (isPickup) {
    // swap source and recipient
    const new_source = {}
    Object.entries(source).forEach(([key, _]) => {
      const rec_key = `recipient${key.slice(key.search('_'), key.length)}`
      new_source[key] = recipient[rec_key]
    })
    const new_recipient = {}
    Object.entries(recipient).forEach(([key, _]) => {
      const src_key = `source${key.slice(key.search('_'), key.length)}`
      new_recipient[key] = source[src_key]
    })
    source = new_source
    recipient = new_recipient
  }

  return { ...source, ...recipient }
}
