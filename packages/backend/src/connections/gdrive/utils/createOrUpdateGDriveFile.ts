export const createOrUpdateGDriveFile = ({
  content,
  fileName: _fileName,
  fileId,
  folderId: _folderId,
  googleDriveClient,
}: {
  content: any
  fileName: any
  fileId?: any
  folderId: any
  googleDriveClient: any
}) => {
  const name = `${_fileName}.pdf`
  const resource = {
    name,
    parents: [_folderId],
  }

  if (fileId) {
    return googleDriveClient.updateFile(fileId, name, 'application/pdf', content)
  }
  return googleDriveClient.createFile(resource, 'application/pdf', content)
}
