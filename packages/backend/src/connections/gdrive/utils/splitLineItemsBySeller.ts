import idx from 'idx'
import { TITLEHOLDER_LICENSE_TYPE_KEY, TITLEHOLDER_ORG_TYPE_KEY } from '../constants'

export const splitLineItemsBySeller = (lineItems) =>
  lineItems.reduce(
    (obj, li) => {
      const titleholderLicense = idx(
        li,
        (_) => _.dataValues.skuBatch.dataValues.titleholder.dataValues.titleholderLicense,
      )
      const titleholderOrganization = idx(
        li,
        (_) => _.dataValues.skuBatch.dataValues.titleholder.dataValues.titleholderOrganization,
      )

      // console.log({ li, tL: li.skuBatch.titleholder, tLL: titleholderLicense, tO: titleholderOrganization })

      const brand = idx(li, (_) => _.dataValues.skuBatch.dataValues.sku.dataValues.organization)
      const manufacturingLicense = idx(li, (_) => _.dataValues.skuBatch.dataValues.batch.dataValues.license)

      let key
      let type

      if (titleholderLicense || titleholderOrganization) {
        if (!titleholderLicense) {
          key = titleholderOrganization.id
          type = TITLEHOLDER_ORG_TYPE_KEY
        } else {
          key = titleholderLicense.id
          type = TITLEHOLDER_LICENSE_TYPE_KEY
        }
      } else {
        // NULL Titleholder > Organization fallback
        key = brand.id
        type = TITLEHOLDER_ORG_TYPE_KEY
      }

      // const licenseId = idx(li, (_) => _.skuBatch.batch.license.id)

      // const licenseKey = !licenseId ? 'noLicense' : licenseId

      if (!obj.titleholders[key]) {
        obj.titleholders[key] = [li]
        if (manufacturingLicense) obj.manufacturersPerTitleholder[key] = [manufacturingLicense]
      } else {
        obj.titleholders[key].push(li)
        if (manufacturingLicense && obj.manufacturersPerTitleholder[key]) {
          obj.manufacturersPerTitleholder[key].push(manufacturingLicense)
        } else if (manufacturingLicense && !obj.manufacturersPerTitleholder[key]) {
          obj.manufacturersPerTitleholder[key] = [manufacturingLicense]
        }
      }

      // collect all licenses/organizations while we're at it
      let titleholder

      if (type === TITLEHOLDER_ORG_TYPE_KEY) {
        if (titleholderOrganization) titleholder = titleholderOrganization
      } else {
        if (titleholderLicense) titleholder = titleholderLicense
      }

      // TITLEHOLDER > ORGANIZATION / BRAND FALLBACKS
      if (!titleholder) {
        titleholder = brand
      }

      const titleholderPayload = {
        type,
        ...titleholder,
      }

      obj.licensesOrOrganizations.push(titleholderPayload)

      obj.totalUnits += parseFloat(li.quantity)

      return obj
    },
    {
      licensesOrOrganizations: [],
      manufacturersPerTitleholder: {},
      titleholders: {},
      totalUnits: 0,
    },
  )
