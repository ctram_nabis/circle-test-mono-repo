import axios from 'axios'
import sanitize from 'sanitize-filename'
import cfg from '../../../config'
import { ORDER_ACTIONS } from '../../../constants'
import dollarFormat from '../../../utils/dollarFormat'
import { DISTRIBUTOR, MANAGER, NULL_TITLEHOLDER_KEY } from '../constants'
import { buildBrandFields } from './buildBrandFields'
import { buildFinancialFields } from './buildFinancialFields'
import { generateInvoiceSubpages } from './generateInvoiceSubpages'
import { generateLetters } from './generateLetters'
import { getOutstandingBalances } from './getOutstandingBalances'
import { splitLineItemsBySeller } from './splitLineItemsBySeller'

export const generatePDFData = ({ lineItems, pageData, organization, brandEmail = null }) => {
  return new Promise(async (resolve) => {
    const {
      buyer,
      order,
      TAX_RATE,
      TAX_PERCENTAGE,
      orderNumber,
      db,
      dateFields,
      locationFields,
      driverFields,
      vehicleFields,
      brand,
      deliveryDateFormatted,
      createdAtFormatted,
      creatorFields,
    } = pageData

    const brandFields = buildBrandFields(order, organization, brandEmail)
    const financialFields = buildFinancialFields(order, lineItems)

    /**
     * For Splitting an Order's Line Items by Seller
     */
    const lineItemsReduced = splitLineItemsBySeller(lineItems)

    /**
     * Creates an Array of Titleholder ID's
     * and Includes a "Bucket" for NULL Titleholder
     */
    const titleholderIdArray = Object.keys(lineItemsReduced.titleholders).sort((a, b) => {
      if (a === NULL_TITLEHOLDER_KEY) return 1
      if (b === NULL_TITLEHOLDER_KEY) return -1

      return String(a).localeCompare(String(b))
    })

    /**
     * For Incrementing Letters for Invoice(s)
     */
    const letterArray = generateLetters(titleholderIdArray)

    const invoiceLetters = letterArray.map((letter) => `${orderNumber}-${letter}`)

    /**
     * For Prepping Data for Each Split Invoice
     */
    const invoiceSubpages = generateInvoiceSubpages({
      buyer,
      financialFields,
      invoiceLetters,
      lineItemsReduced,
      order,
      TAX_RATE,
      TAX_PERCENTAGE,
      titleholderIdArray,
    })

    /**
     * For AP Summary
     */
    const outstandingBalances = await getOutstandingBalances(db, order)

    const isNabis = Boolean(!organization)

    const isDeliveryToRetailer = order.action === ORDER_ACTIONS.DELIVERY_TO_RETAILER

    const DATA_FOR_PDF = {
      subscriberId: organization ? organization.id : undefined,
      shouldRenderPages: {
        coverPage: true,
        invoiceSubpages: isDeliveryToRetailer,
        packingSlip: isNabis || (!isNabis && isDeliveryToRetailer),
        apSummary: isNabis && outstandingBalances.orders.length > 0,
      },
      coverPage: {
        orderNumber,
        invoiceNumbers: invoiceLetters,
        irn: order.irn,
        departureTime: dateFields.departure_time,
        eta: dateFields.estimated_arrival_time,
        actualArrivalTime: undefined,
        creator: {
          name: creatorFields.created_by_name,
          email: creatorFields.created_by_email,
          phone: creatorFields.created_by_phone,
          createdAt: createdAtFormatted,
        },
        // TODO: also can depend upon subscriber
        brand: {
          contact: brandFields.brand_name,
          email: brandFields.brand_email,
          name: brandFields.brand_name,
          phone: brandFields.brand_phone,
        },
        buyer,
        origin: {
          address: `${locationFields.source_address_1}${
            locationFields.source_address_2 ? `, ${locationFields.source_address_2}` : ''
          }`,
          contact: locationFields.source_poc_name,
          email: locationFields.source_poc_email,
          license: locationFields.source_license,
          name: locationFields.source_name,
          phone: locationFields.source_poc_phone,
        },
        distributor: DISTRIBUTOR,
        driver: {
          license: driverFields.drivers_license,
          name: driverFields.driver_name,
        },
        vehicle: {
          makeAndModel: vehicleFields.vehicle_make_model,
          plate: vehicleFields.vehicle_plate,
        },
        manager: MANAGER,
        /**
         * DO NOT INSERT order.notes into here. VISIBLE TO RETAILERS
         */
        notes: `Order created on ${createdAtFormatted} by ${creatorFields.created_by_name}`,
        pricingDetails: {
          // discount: financialFields.discount,
          // surcharge: financialFields.surcharge,
          exciseTax: financialFields.excise_tax,
          hasUnalterablePaymentTerms: order.hasUnalterablePaymentTerms,
          mustPayPreviousBalance: order.mustPayPreviousBalance,
          netOffTotal: financialFields.netOffTotal,
          paymentTerms: financialFields.payment_terms,
          subtotal: financialFields.subtotal,
          taxPercentage: TAX_PERCENTAGE,
          totalBalance: financialFields.total_balance_due,
        },
      },
      invoiceSubpages,
      packingSlip: {
        // TODO: diff for each subscribner
        brand,
        deliveryDate: deliveryDateFormatted,
        destination: order.site && order.site.name,
        irn: order.irn,
        orderNumber,
        totalUnits: lineItemsReduced.totalUnits,
      },
      apSummary: {
        apDetails: {
          date: deliveryDateFormatted,
          destination: order.site && order.site.name,
          orderNumber,
        },
        buyer,
        outstandingBalances: outstandingBalances.orders,
        totalAmountOutstanding: dollarFormat(outstandingBalances.totalAmountOutstanding),
        totalAmountOverdue: dollarFormat(outstandingBalances.totalAmountOverdue),
      },
    }

    /**
     * stringify `DATA_FOR_PDF` in order to be able to then
     * parse and convert all values to strings
     * `react-pdf` does not like non-string values
     */
    const json = JSON.stringify(DATA_FOR_PDF)
    const withStrings = JSON.parse(json, (key, value) => {
      if (key === 'trueValue') return value
      if (typeof value === 'number') return String(value)
      if (value === null || value === undefined) return ''
      return value
    })

    // Will eventually be used for a folder name, so important to sanitize just in case
    const sanitizedBrandName = sanitize(brand)

    return resolve(
      axios({
        method: 'POST',
        data: {
          DATA_FOR_PDF: withStrings,
          authorization: 'nabis-api-key-lolol',
          brand: sanitizedBrandName,
          isNabis,
        },
        // url: 'http://localhost:3005',
        url: cfg.pdfGeneratorLink,
        headers: { 'Content-Type': 'application/json' },
      }),
    )
  })
}
