import logger from '../../../logging'

export const GDriveAPIErrorHandler = (err) => {
  const msg = `[GDrive] ${err}`
  logger.error(msg)
  return msg
}
