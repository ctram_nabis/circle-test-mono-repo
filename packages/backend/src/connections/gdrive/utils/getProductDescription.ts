export const getProductDescription = (_name, _string) =>
  _string.indexOf(_name) === -1 ? `${_string}, ${_name}` : _string
