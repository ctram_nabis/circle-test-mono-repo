import * as Sentry from '@sentry/node'
import { IS_PROD } from '../../../constants'

export const sentryCaptureMessage = (message) => {
  if (IS_PROD) {
    Sentry.captureMessage(message)
  }
}
