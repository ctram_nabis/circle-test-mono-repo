import idx from 'idx'
import moment from 'moment-timezone'
import { Op, Sequelize } from 'sequelize'
import dollarFormat from '../../../utils/dollarFormat'

export const getOutstandingBalances = async (db, order) => {
  const outstandingOrders = await db.Order.findAll({
    attributes: [
      [Sequelize.literal('date + interval \'1 day\' * "daysTillPaymentDue"'), 'dueDate'],
      'number',
      'gmv',
      'exciseTaxCollected',
      'discount',
      'surcharge',
      'gmvCollected',
      'exciseTax',
      'date',
    ],
    where: {
      [Op.and]: [
        {
          paymentStatus: 'UNPAID',
        },
        {
          status: ['DELIVERED', 'DELIVERED_WITH_EDITS'],
        },
        {
          action: 'DELIVERY_TO_RETAILER',
        },
        {
          daysTillPaymentDue: {
            [Op.not]: null,
          },
        },
        {
          '$site.siteLicensedLocation.id$':
            idx(order, (_) => _.licensedLocation.id) || idx(order, (_) => _.site.siteLicensedLocation.id),
        },
      ],
    },
    include: [
      {
        association: db.Order.Site,
        include: [
          {
            association: db.Site.LicensedLocation,
          },
        ],
      },
      {
        association: db.Order.Organization,
      },
    ],
  })

  const numberWrapper = (num) => {
    if (!num) return 0

    return parseFloat(Number(num).toFixed(2))
  }

  const outstandingBalances = outstandingOrders.reduce(
    (obj, ord) => {
      const dueDate = moment.utc(ord.dataValues.dueDate)
      const dueDateFormatted = dueDate.format('MM-DD-YYYY')

      const exciseTaxCollectedZero = numberWrapper(ord.exciseTaxCollected) || 0
      const calculatedAmountDue =
        numberWrapper(ord.gmv) -
        numberWrapper(ord.discount) +
        numberWrapper(ord.surcharge) +
        numberWrapper(ord.exciseTax) -
        numberWrapper(ord.gmvCollected) -
        numberWrapper(exciseTaxCollectedZero)
      const due = numberWrapper(calculatedAmountDue)
      const collectedAmount = numberWrapper(ord.gmvCollected) + numberWrapper(exciseTaxCollectedZero)
      const collected = numberWrapper(collectedAmount)

      // const taxCollectedAmount = exciseTaxCollectedZero
      // const taxCollected = taxCollectedAmount

      const daysOverdue = moment.utc().diff(dueDate, 'days')

      obj.totalAmountOutstanding += due
      if (daysOverdue > 0) obj.totalAmountOverdue += due

      obj.orders.push({
        amountPaid: dollarFormat(collected),
        amountPaidDate: undefined,
        brand: ord.organization && ord.organization.name,
        daysOverdue,
        deliveryDate: moment.utc(ord.date).format('MM-DD-YYYY'),
        dueAmount: dollarFormat(due),
        dueDate: dueDateFormatted,
        orderNumber: ord.number,
      })

      return obj
    },
    {
      orders: [],
      totalAmountOutstanding: 0,
      totalAmountOverdue: 0,
    },
  )

  return outstandingBalances
}
