// import { LineItem } from 'generated/graphql'
// import logger from 'modules/Logger'
import moment from 'moment'

// NOTE: order matters! Please keep tax rates with the most recent effectiveDate
// at the beginning of the array of tax rates.
const EXCISE_TAXES: Record<string, Array<{ effectiveDate: string; rate: number }>> = {
  ca: [
    {
      effectiveDate: '2020-01-01',
      rate: 0.27,
    },
    {
      effectiveDate: '0001-01-01',
      rate: 0.24,
    },
  ],
  defaults: [
    {
      effectiveDate: '2020-01-01',
      rate: 0.27,
    },
    {
      effectiveDate: '0001-01-01', // think before ever removing this, will break getDefaultTaxRate()!
      rate: 0.24,
    },
  ],
}

const CA_TIMEZONE = 'America/Los_Angeles'

const getDefaultTaxRate = (deliveryDate: string) => {
  if (!deliveryDate) {
    return EXCISE_TAXES.defaults[0].rate
  }

  const _deliveryDate = moment(deliveryDate).tz(CA_TIMEZONE)

  return EXCISE_TAXES.defaults.find((_taxRate) => {
    const effectiveDate = moment(_taxRate.effectiveDate).tz(CA_TIMEZONE)
    return _deliveryDate >= effectiveDate
  }).rate
}

export const getTaxRate = (siteUsState: string | null | undefined, deliveryDate: string) => {
  const defaultRate = getDefaultTaxRate(deliveryDate)

  if (!siteUsState) {
    return defaultRate
  }

  const warningMsg = `No tax rates found for US State ${siteUsState}, using default rates. You should probably update the EXCISE_TAXES table on the frontend.`

  let taxDateAndRate

  try {
    const usStateTaxRates = EXCISE_TAXES[siteUsState.toLowerCase()]

    if (!usStateTaxRates) {
      throw Error(warningMsg)
    }
    const deliveryDateMoment = moment(deliveryDate).tz(CA_TIMEZONE)

    taxDateAndRate = usStateTaxRates.find((taxRate) => {
      const effectiveDate = moment(taxRate.effectiveDate).tz(CA_TIMEZONE)
      return deliveryDateMoment >= effectiveDate
    })

    if (!taxDateAndRate) {
      throw Error(warningMsg)
    }
  } catch (e) {
    // logger.warn(e)
    return defaultRate
  }

  return taxDateAndRate.rate
}

interface Props {
  lineItems: any
  discount?: number
  siteUsState: string | null | undefined // is there a reason why an existing site might not have a US state value?
  deliveryDate: string
  surcharge?: number
}

interface Return {
  subtotal: number
  totalExciseTax: number
  totalBalance: number
  lineItemSubtotalBeforeDiscount: number
}

export const calculateMoney: (data: Props) => Return = ({
  lineItems = [],
  discount = 0,
  siteUsState,
  deliveryDate,
  surcharge = 0,
}) => {
  const taxRate = getTaxRate(siteUsState, deliveryDate)

  let subtotal = 0.0
  let totalExciseTax = 0.0
  let lineItemSubtotalBeforeDiscount = 0.0

  lineItems.forEach(({ quantity = 0, pricePerUnit, isExciseTaxable, discount: lineItemDiscount = 0 }) => {
    const _pricePerUnit = pricePerUnit || 0

    const taxableLineItemTotal = quantity * _pricePerUnit
    const lineItemTotal = taxableLineItemTotal - lineItemDiscount

    subtotal += lineItemTotal
    lineItemSubtotalBeforeDiscount += taxableLineItemTotal

    if (isExciseTaxable) {
      totalExciseTax += taxableLineItemTotal * taxRate
    }
  })

  const totalBalance = subtotal - discount + totalExciseTax + surcharge
  return { subtotal, totalExciseTax, totalBalance, lineItemSubtotalBeforeDiscount }
}
