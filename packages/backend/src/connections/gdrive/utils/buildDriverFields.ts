/* eslint-disable camelcase */
import idx from 'idx'

export const buildDriverFields = (order) => {
  const driver_name = order.driver
    ? `${idx(order, (_) => _.driver.firstName) || ''} ${idx(order, (_) => _.driver.lastName) || ''}`
    : ''

  const drivers_license = idx(order, (_) => _.driver.driversLicense) || ''

  return { driver_name, drivers_license }
}
