import sanitize from 'sanitize-filename'
import { sentryCaptureMessage } from './sentryCaptureMessage'

export const getOrCreateGDriveOrgFolder = ({ org, cfg, googleDriveClient }) =>
  new Promise((resolve) => {
    const { manifestGDriveFolderId } = org
    const { gdriveFolderId } = cfg.google.drive
    const orgFolderName = sanitize(`${org.name}_${org.id}`)
    if (!manifestGDriveFolderId) {
      // create org folder under 'Manifests [DO NOT DELETE]'
      return resolve(googleDriveClient.createFolder(orgFolderName, [gdriveFolderId]))
    }
    return googleDriveClient
      .getFolder(manifestGDriveFolderId)
      .then((oFolderId) => {
        if (oFolderId) {
          return resolve(oFolderId)
        }
        return resolve(googleDriveClient.createFolder(orgFolderName, [gdriveFolderId]))
      })
      .catch((error) => {
        sentryCaptureMessage(`api/getOrCreateGDriveOrgFolder: error ${error}`)
        console.error('api/getOrCreateGDriveOrgFolder')
      })
  })
