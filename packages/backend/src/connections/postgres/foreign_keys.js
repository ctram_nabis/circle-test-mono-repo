export default (db) => {
  // User <> Organization
  db.User.Organizations = db.User.belongsToMany(db.Organization, {
    as: 'organizations',
    foreignKey: 'userId',
    through: db.OrganizationUser,
  })
  db.Organization.Users = db.Organization.belongsToMany(db.User, {
    as: 'users',
    foreignKey: 'organizationId',
    through: db.OrganizationUser,
  })

  db.User.OwnedOrganizations = db.User.hasMany(db.Organization, {
    as: 'ownedOrganizations',
    foreignKey: 'ownerId',
  })
  db.Organization.Owner = db.Organization.belongsTo(db.User, {
    as: 'owner',
    foreignKey: 'ownerId',
  })

  // User <> Role
  db.User.Roles = db.User.belongsToMany(db.Role, {
    as: 'roles',
    foreignKey: 'userId',
    through: db.UserRole,
  })
  db.Role.Users = db.Role.belongsToMany(db.User, {
    as: 'users',
    foreignKey: 'roleId',
    through: db.UserRole,
  })

  // Organization <> Role
  db.Organization.Roles = db.Organization.hasMany(db.Role, {
    as: 'roles',
    foreignKey: 'organizationId',
  })
  db.Role.Organization = db.Role.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // Organization <> UserRole
  db.UserRole.Organization = db.UserRole.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // User <> Draft
  db.User.Drafts = db.User.hasMany(db.Draft, {
    as: 'drafts',
    foreignKey: 'creatorId',
  })
  db.Draft.User = db.Draft.belongsTo(db.User, {
    as: 'creator',
    foreignKey: 'creatorId',
  })

  // User <> Draft
  db.Organization.Drafts = db.Organization.hasMany(db.Draft, {
    as: 'drafts',
    foreignKey: 'organizationId',
  })
  db.Draft.Organization = db.Draft.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // User <> UserPreference
  db.User.UserPreferences = db.User.hasMany(db.UserPreference, {
    as: 'userPreference',
    foreignKey: 'userId',
  })
  db.UserPreference.User = db.UserPreference.belongsTo(db.User, {
    as: 'user',
    foreignKey: 'userId',
  })

  // User <> Order
  db.User.Orders = db.User.hasMany(db.Order, {
    as: 'orders',
    foreignKey: 'creatorId',
  })

  // Driver <> Order
  db.Driver.Orders = db.Driver.hasMany(db.Order, {
    as: 'orders',
    foreignKey: 'driverId',
  })

  // Organization <> Order
  db.Organization.Orders = db.Organization.hasMany(db.Order, {
    as: 'orders',
    foreignKey: 'organizationId',
  })

  // Vehicle <> Order
  db.Vehicle.Orders = db.Vehicle.hasMany(db.Order, {
    as: 'orders',
    foreignKey: 'vehicleId',
  })

  // Order <> LineItem
  db.LineItem.Order = db.LineItem.belongsTo(db.Order, {
    as: 'order',
    foreignKey: 'orderId',
  })

  // Sku <> LineItem
  db.Sku.LineItems = db.Sku.hasMany(db.LineItem, {
    as: 'lineItems',
    foreignKey: 'skuId',
  })

  db.LineItem.Sku = db.LineItem.belongsTo(db.Sku, {
    as: 'sku',
    foreignKey: 'skuId',
  })

  // Batch <> LineItem
  db.Batch.LineItems = db.Batch.hasMany(db.LineItem, {
    as: 'lineItems',
    foreignKey: 'batchId',
  })

  db.LineItem.Batch = db.LineItem.belongsTo(db.Batch, {
    as: 'batch',
    foreignKey: 'batchId',
  })

  // SkuBatch <> LineItem
  db.SkuBatch.LineItems = db.SkuBatch.hasMany(db.LineItem, {
    as: 'lineItems',
    foreignKey: 'skuBatchId',
  })

  db.LineItem.SkuBatch = db.LineItem.belongsTo(db.SkuBatch, {
    as: 'skuBatch',
    foreignKey: 'skuBatchId',
  })

  // Organization <> UserPreference
  db.Organization.UserPreferences = db.Organization.hasMany(db.UserPreference, {
    as: 'userPreference',
    foreignKey: 'organizationId',
  })
  db.UserPreference.Organization = db.UserPreference.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // Organization <> Site
  db.Organization.Sites = db.Organization.hasMany(db.Site, {
    as: 'sites',
    foreignKey: 'organizationId',
  })

  // Organization <> Sku
  db.Organization.Skus = db.Organization.hasMany(db.Sku, {
    as: 'skus',
    foreignKey: 'organizationId',
  })
  db.Sku.Organization = db.Sku.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // Organization <> License
  db.Organization.Licenses = db.Organization.hasMany(db.License, {
    as: 'licenses',
    foreignKey: 'organizationId',
  })
  db.License.Organization = db.License.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // Organization <> LineItem
  db.Organization.LineItems = db.Organization.hasMany(db.LineItem, {
    as: 'lineItems',
    foreignKey: 'organizationId',
  })
  db.LineItem.Organization = db.LineItem.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // Site <> License
  db.License.Site = db.License.belongsTo(db.Site, {
    as: 'licenses',
    foreignKey: 'siteId',
  })

  // Organization <> OrganizationInvite
  db.Organization.OrganizationInvites = db.Organization.hasMany(db.OrganizationInvite, {
    as: 'organizationInvites',
    foreignKey: 'organizationId',
  })
  db.OrganizationInvite.Organization = db.OrganizationInvite.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // User <> OrganizationInvite
  db.User.OrganizationInvites = db.User.hasMany(db.OrganizationInvite, {
    as: 'organizationInvites',
    foreignKey: 'senderId',
  })
  db.OrganizationInvite.User = db.OrganizationInvite.belongsTo(db.User, {
    as: 'sender',
    foreignKey: 'senderId',
  })

  // Order <> Warehouse
  db.Warehouse.Orders = db.Warehouse.hasMany(db.Order, {
    as: 'orders',
    foreignKey: 'warehouseId',
  })

  // Warehouse <> Site
  db.Warehouse.Site = db.Warehouse.belongsTo(db.Site, {
    as: 'site',
    foreignKey: 'siteId',
  })

  // Warehouse <> License
  db.License.Warehouses = db.License.hasMany(db.Warehouse, {
    as: 'warehouses',
    foreignKey: 'licenseId',
  })
  db.Warehouse.License = db.Warehouse.belongsTo(db.License, {
    as: 'license',
    foreignKey: 'licenseId',
  })

  // Organization <> Attachment
  db.Organization.Attachments = db.Organization.hasMany(db.Attachment, {
    as: 'attachments',
    foreignKey: 'organizationId',
  })
  db.Attachment.Organization = db.Attachment.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // Order <> Attachment
  db.Attachment.Orders = db.Attachment.belongsToMany(db.Order, {
    as: 'orders',
    foreignKey: 'attachmentId',
    through: db.OrderAttachment,
  })

  // Sku <> InventoryItem
  db.Sku.InventoryItems = db.Sku.hasMany(db.InventoryItem, {
    as: 'inventoryItems',
    foreignKey: 'skuId',
  })
  db.InventoryItem.Sku = db.InventoryItem.belongsTo(db.Sku, {
    as: 'sku',
    foreignKey: 'skuId',
  })

  // Batch <> InventoryItem
  db.Batch.InventoryItems = db.Batch.hasMany(db.InventoryItem, {
    as: 'inventoryItems',
    foreignKey: 'batchId',
  })
  db.InventoryItem.Batch = db.InventoryItem.belongsTo(db.Batch, {
    as: 'batch',
    foreignKey: 'batchId',
  })

  // SkuBatch <> InventoryItem
  db.SkuBatch.InventoryItems = db.SkuBatch.hasMany(db.InventoryItem, {
    as: 'inventoryItems',
    foreignKey: 'skuBatchId',
  })
  db.InventoryItem.SkuBatch = db.InventoryItem.belongsTo(db.SkuBatch, {
    as: 'skuBatch',
    foreignKey: 'skuBatchId',
  })

  // Warehouse <> InventoryItem
  db.Warehouse.InventoryItems = db.Warehouse.hasMany(db.InventoryItem, {
    as: 'inventoryItems',
    foreignKey: 'warehouseId',
  })
  db.InventoryItem.Warehouse = db.InventoryItem.belongsTo(db.Warehouse, {
    as: 'warehouse',
    foreignKey: 'warehouseId',
  })

  // Organization <> InventoryItem
  db.Organization.InventoryItems = db.Organization.hasMany(db.InventoryItem, {
    as: 'inventoryItems',
    foreignKey: 'organizationId',
  })
  db.InventoryItem.Organization = db.InventoryItem.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // InventoryItem <> InventoryLedgerItem
  db.InventoryItem.InventoryLedgerItems = db.InventoryItem.hasMany(db.InventoryLedgerItem, {
    as: 'inventoryLedgerItems',
    foreignKey: 'inventoryItemId',
  })
  db.InventoryLedgerItem.InventoryItem = db.InventoryLedgerItem.belongsTo(db.InventoryItem, {
    as: 'inventoryItem',
    foreignKey: 'inventoryItemId',
  })

  // Order <> InventoryLedgerItem
  db.InventoryLedgerItem.Order = db.InventoryLedgerItem.belongsTo(db.Order, {
    as: 'order',
    foreignKey: 'orderId',
  })

  // InventoryLedgerItem => LineItem
  db.InventoryLedgerItem.LineItem = db.InventoryLedgerItem.belongsTo(db.LineItem, {
    as: 'lineItem',
    foreignKey: 'lineItemId',
  })

  // User <> InventoryLedgerItem
  db.User.InventoryLedgerItems = db.User.hasMany(db.InventoryLedgerItem, {
    as: 'inventoryLedgerItems',
    foreignKey: 'creatorId',
  })
  db.InventoryLedgerItem.User = db.InventoryLedgerItem.belongsTo(db.User, {
    as: 'creator',
    foreignKey: 'creatorId',
  })

  // Organization <> InventoryLedgerItem
  db.Organization.InventoryLedgerItems = db.Organization.hasMany(db.InventoryLedgerItem, {
    as: 'inventoryLedgerItems',
    foreignKey: 'organizationId',
  })
  db.InventoryLedgerItem.Organization = db.InventoryLedgerItem.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // Organization <> Organization
  db.Organization.Suborganization = db.Organization.belongsToMany(db.Organization, {
    as: 'suborganizations',
    foreignKey: 'suborganizationId',
    through: db.OrganizationOrganization,
  })
  db.Organization.Superorganization = db.Organization.belongsToMany(db.Organization, {
    as: 'superorganizations',
    foreignKey: 'superorganizationId',
    through: db.OrganizationOrganization,
  })

  db.Organization.SubscribedOrders = db.Organization.belongsToMany(db.Order, {
    as: 'subscribedOrders',
    foreignKey: 'orderId',
    through: db.OrderSubscriber,
  })

  db.OrderChange.Order = db.OrderChange.belongsTo(db.Order, {
    as: 'order',
    foreignKey: 'orderId',
  })

  db.User.OrderChanges = db.User.hasMany(db.OrderChange, {
    as: 'orderChanges',
    foreignKey: 'submitterId',
  })
  db.OrderChange.Submitter = db.OrderChange.belongsTo(db.User, {
    as: 'submitter',
    foreignKey: 'submitterId',
  })

  db.Organization.OrderChanges = db.Organization.hasMany(db.OrderChange, {
    as: 'orderChanges',
    foreignKey: 'organizationId',
  })
  db.OrderChange.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  db.OrderChange.LineItemChanges = db.OrderChange.hasMany(db.LineItemChange, {
    as: 'lineItemChanges',
    foreignKey: 'orderChangeId',
  })
  db.LineItemChange.OrderChange = db.LineItemChange.belongsTo(db.OrderChange, {
    as: 'orderChange',
    foreignKey: 'orderChangeId',
  })

  db.SkuBatch.LineItemChange = db.SkuBatch.hasMany(db.LineItemChange, {
    as: 'lineItemChanges',
    foreignKey: 'skuBatchId',
  })
  db.LineItemChange.SkuBatch = db.LineItemChange.belongsTo(db.SkuBatch, {
    as: 'skuBatch',
    foreignKey: 'skuBatchId',
  })

  db.Organization.Rus = db.Organization.hasMany(db.Ru, {
    as: 'rus',
    foreignKey: 'organizationId',
  })
  db.Ru.Organization = db.Ru.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  db.Organization.Batches = db.Organization.hasMany(db.Batch, {
    as: 'batches',
    foreignKey: 'organizationId',
  })
  db.Batch.Organization = db.Batch.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  db.License.Batches = db.License.hasMany(db.Batch, {
    as: 'batches',
    foreignKey: 'licenseId',
  })
  db.Batch.License = db.Batch.belongsTo(db.License, {
    as: 'license',
    foreignKey: 'licenseId',
  })

  db.SkuBatch.Batch = db.SkuBatch.belongsTo(db.Batch, {
    as: 'batch',
    foreignKey: 'batchId',
  })
  db.SkuBatch.Sku = db.SkuBatch.belongsTo(db.Sku, {
    as: 'sku',
    foreignKey: 'skuId',
  })

  db.LicensedLocation.Licenses = db.LicensedLocation.hasMany(db.License, {
    as: 'licenses',
    foreignKey: 'licensedLocationId',
  })
  db.License.LicensedLocation = db.License.belongsTo(db.LicensedLocation, {
    as: 'licensedLocation',
    foreignKey: 'licensedLocationId',
  })

  db.LicensedLocation.Sites = db.LicensedLocation.hasMany(db.Site, {
    as: 'sites',
    foreignKey: 'licensedLocationId',
  })
  db.Site.LicensedLocation = db.Site.belongsTo(db.LicensedLocation, {
    as: 'siteLicensedLocation',
    foreignKey: 'licensedLocationId',
  })

  // Warehouse <> LicensedLocation
  db.Warehouse.LicensedLocation = db.Warehouse.hasMany(db.LicensedLocation, {
    as: 'licensedLocations',
    foreignKey: 'warehouseId',
    constraints: false,
  })

  db.LicensedLocation.Warehouse = db.LicensedLocation.belongsTo(db.Warehouse, {
    as: 'warehouse',
    foreignKey: 'warehouseId',
    constraints: false,
  })

  // Order <> User
  // order assignment
  db.Order.User = db.Order.belongsTo(db.User, {
    as: 'assignee',
    foreignKey: 'assigneeId',
  })

  db.User.Orders = db.User.hasMany(db.Order, {
    as: 'assigned',
    foreignKey: 'assigneeId',
  })

  db.Order.Collections = db.Order.hasMany(db.Order, {
    as: 'collections',
    foreignKey: 'collectWithOrderId',
  })
  db.Order.CollectWithOrder = db.Order.belongsTo(db.Order, {
    as: 'collectWithOrder',
    foreignKey: 'collectWithOrderId',
  })

  // User <> Order
  db.Order.Creator = db.Order.belongsTo(db.User, {
    as: 'creator',
    foreignKey: 'creatorId',
  })

  // Driver <> Order
  db.Order.Driver = db.Order.belongsTo(db.Driver, {
    as: 'driver',
    foreignKey: 'driverId',
  })

  // Organization <> Order
  db.Order.Organization = db.Order.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // Vehicle <> Order
  db.Order.Vehicle = db.Order.belongsTo(db.Vehicle, {
    as: 'vehicle',
    foreignKey: 'vehicleId',
  })

  // Order <> LineItem
  db.Order.LineItems = db.Order.hasMany(db.LineItem, {
    as: 'lineItems',
    foreignKey: 'orderId',
  })

  db.Order.LicensedLocations = db.Order.belongsTo(db.LicensedLocation, {
    as: 'licensedLocation',
    foreignKey: 'licensedLocationId',
  })

  db.OrderSubscriber.Organization = db.OrderSubscriber.belongsTo(db.Organization, {
    as: 'subscriber',
    foreignKey: 'subscriberId',
  })

  // Site <> Order
  db.Order.Site = db.Order.belongsTo(db.Site, {
    as: 'site',
    foreignKey: 'siteId',
  })

  // Order <> Warehouse
  db.Order.Warehouse = db.Order.belongsTo(db.Warehouse, {
    as: 'warehouse',
    foreignKey: 'warehouseId',
  })

  // Order <> Attachment
  db.Order.Attachments = db.Order.belongsToMany(db.Attachment, {
    as: 'attachments',
    through: db.OrderAttachment,
    foreignKey: 'orderId',
  })

  // Order <> InventoryLedgerItem
  db.Order.InventoryLedgerItems = db.Order.hasMany(db.InventoryLedgerItem, {
    as: 'inventoryLedgerItems',
    foreignKey: 'orderId',
  })

  db.Order.Subscribers = db.Order.belongsToMany(db.Organization, {
    as: 'subscribers',
    through: db.OrderSubscriber,
    foreignKey: 'orderId',
  })

  db.Order.OrderChanges = db.Order.hasMany(db.OrderChange, {
    as: 'orderChanges',
    foreignKey: 'orderId',
  })

  // TitleHolder <> License
  db.Titleholder.License = db.Titleholder.belongsTo(db.License, {
    as: 'titleholderLicense',
    foreignKey: 'titleholderLicenseId',
  })

  // TitleHolder <> Organization
  db.Titleholder.Organization = db.Titleholder.belongsTo(db.Organization, {
    as: 'titleholderOrganization',
    foreignKey: 'titleholderOrganizationId',
  })
  db.Titleholder.Organization = db.Titleholder.belongsTo(db.Organization, {
    as: 'organization',
    foreignKey: 'organizationId',
  })

  // SkuBatch <> Titleholder
  db.SkuBatch.Titleholder = db.SkuBatch.belongsTo(db.Titleholder, {
    as: 'titleholder',
    foreignKey: 'titleholderId',
  })

  return db
}
