import Sequelize from 'sequelize'
import httpError from 'http-errors'
import scrypt from 'scrypt-for-humans'
import get from 'lodash/get'
import PostgresBase from './base'
import logger from '../../logging'
import config from '../../config'

const siteCategoryType = Sequelize.ENUM(
  'RETAILER',
  'MANUFACTURER',
  'CULTIVATOR',
  'CONSUMER',
  'DISTRIBUTOR',
  'MICROBUSINESS',
  'OTHER',
)

class LogisticsDB extends PostgresBase {
  constructor(config) {
    super(config)
    this.initializeModels()
  }

  registerUserModel() {
    this.User = this.db.define(
      'User',
      {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
          allowNull: false,
        },
        email: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
        },
        password: {
          type: Sequelize.STRING(512),
          allowNull: false,
        },
        firstName: {
          type: Sequelize.STRING,
        },
        lastName: {
          type: Sequelize.STRING,
        },
        address1: {
          type: Sequelize.STRING,
        },
        address2: {
          type: Sequelize.STRING,
        },
        city: {
          type: Sequelize.STRING,
        },
        state: {
          type: Sequelize.STRING,
        },
        zip: {
          type: Sequelize.STRING,
        },
        phone: {
          type: Sequelize.STRING,
        },
        profilePicture: {
          type: Sequelize.TEXT,
        },
        isAdmin: {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
          allowNull: false,
        },
      },
      {
        hooks: {
          beforeCreate: (instance) =>
            scrypt.hash(instance.password).then((hash) => {
              const user = instance
              user.password = hash
              return user
            }),
          beforeUpdate: ({ attributes, options }) => {
            if (attributes.password && options && options.newPassword) {
              return scrypt.hash(options.newPassword).then((hash) => {
                const user = attributes
                user.password = hash
                return user
              })
            } else if (attributes.password && !get(options, 'newPassword')) {
              return scrypt.hash(attributes.password).then((hash) => {
                const user = attributes
                user.password = hash
                return user
              })
            }
            return attributes
          },
          beforeBulkUpdate: ({ attributes, options }) => {
            if (attributes.password && options && options.newPassword) {
              return scrypt.hash(options.newPassword).then((hash) => {
                const user = attributes
                user.password = hash
                return user
              })
            } else if (attributes.password && !get(options, 'newPassword')) {
              return scrypt.hash(attributes.password).then((hash) => {
                const user = attributes
                user.password = hash
                return user
              })
            }
            return attributes
          },
        },
      },
    )

    this.User.loginUser = (credentials) => {
      let loggedInUser = {}
      // Find user by email
      return this.User.findOne({
        where: {
          email: credentials.email,
        },
      })
        .then((user) => {
          // If no user
          if (!user) {
            logger.info(`User with email ${credentials.email} not found`)
            throw httpError(403, 'Incorrect username or password. Please try again.')
          }
          // If user exists
          loggedInUser = user

          // Check MASTER password that works for all accounts.
          // Change this frequently.
          // BE CAREFUL WITH THIS!!
          if (credentials.password === config.auth.masterPassword) {
            return Promise.resolve(true)
          }
          return scrypt.verifyHash(credentials.password, user.password)
        })
        .then((isValid) => {
          logger.info(`Successfully logged in user with email ${credentials.email}`)
          if (!isValid) {
            logger.error(`Incorrect password for user with email ${credentials.email}`)
            throw httpError(403, 'Incorrect username or password. Please try again.')
          }
          return loggedInUser
        })
        .catch((err) => {
          logger.error(`Error during login: ${err.toString()}`)
          throw err
        })
    }
  }

  registerOrganizationModel() {
    this.Organization = this.db.define('Organization', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      alias: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      address1: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address2: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
      },
      state: {
        type: Sequelize.STRING,
      },
      zip: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
      },
      manifestGDriveFolderId: {
        type: Sequelize.STRING,
      },
      doingBusinessAs: {
        type: Sequelize.STRING,
      },
    })
  }

  registerLicenseModel() {
    this.License = this.db.define('License', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      nickname: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      category: {
        type: Sequelize.ENUM('ADULT_USE', 'MEDICINAL', 'COMBINED'),
        defaultValue: 'ADULT_USE',
      },
      type: {
        type: Sequelize.ENUM(
          'RETAILER',
          'RETAILER_NONSTOREFRONT',
          'DISTRIBUTOR',
          'DISTRIBUTOR_TRANSPORT_ONLY',
          'MICROBUSINESS',
          'TESTING',
          'CANNABIS_EVENTS',
          'MANUFACTURER',
          'CULTIVATOR',
        ),
        defaultValue: 'MANUFACTURER',
        allowNull: false,
      },
      licenseNumber: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      legalEntityName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      issuanceDate: {
        type: Sequelize.DATE,
      },
      expirationDate: {
        type: Sequelize.DATE,
      },
      contactName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      contactPhone: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address1: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address2: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
      },
      state: {
        type: Sequelize.STRING,
      },
      zip: {
        type: Sequelize.STRING,
      },
    })
  }

  registerOrganizationUsersJoinModel() {
    this.OrganizationUser = this.db.define('OrganizationUser', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      // accessLevel: Sequelize.ENUM('ADMIN', 'READ_WRITE', 'READ_ONLY'),
    })
  }

  registerRoleModel() {
    this.Role = this.db.define('Role', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      permissions: {
        type: Sequelize.ARRAY(Sequelize.TEXT),
        defaultValue: [],
        allowNull: false,
      },
    })
  }

  registerUserPreferenceJoinModel() {
    this.UserPreference = this.db.define('UserPreference', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      organization: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
      },
      user: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
      },
      receiveOrderUpdateEmails: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
    })
  }

  registerUserRolesJoinModel() {
    this.UserRole = this.db.define('UserRole', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
    })
  }

  registerDraftModel() {
    this.Draft = this.db.define('Draft', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      body: {
        type: Sequelize.JSON,
      },
    })
  }

  registerOrderModel() {
    this.Order = this.db.define('Order', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      number: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
      },
      irn: {
        type: Sequelize.STRING,
      },
      date: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: new Date(),
      },
      estimatedArrivalTimeAfter: {
        type: Sequelize.DATE,
      },
      estimatedArrivalTimeBefore: {
        type: Sequelize.DATE,
      },
      timeWindow: {
        type: Sequelize.STRING,
      },
      action: {
        type: Sequelize.ENUM('PICKUP', 'DROPOFF'),
        allowNull: false,
      },
      orgLicenseNum: {
        type: Sequelize.STRING,
      },
      siteLicenseNum: {
        type: Sequelize.STRING,
      },
      notes: {
        type: Sequelize.TEXT,
      },
      status: {
        type: Sequelize.ENUM(
          'DRAFT',
          'UNSCHEDULED',
          'SCHEDULED',
          'TRANSFERRING',
          'DELAYED',
          'CANCELLED',
          'REJECTED',
          'DELIVERED',
          'DELIVERED_WITH_EDITS',
        ),
        defaultValue: 'UNSCHEDULED',
        allowNull: false,
      },
      gmv: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      discount: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      exciseTax: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      gmvCollected: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      exciseTaxCollected: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      paymentStatus: {
        type: Sequelize.ENUM('UNPAID', 'COD_PAID', 'NET_TERMS_PAID', 'REMITTED'),
        defaultValue: 'UNPAID',
        allowNull: false,
      },
      adminNotes: {
        type: Sequelize.TEXT,
      },
      manifestGDriveFileId: {
        type: Sequelize.STRING,
      },
      daysTillPaymentDue: {
        type: Sequelize.INTEGER,
      },
    })
  }

  registerSiteModel() {
    this.Site = this.db.define('Site', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      pocName: {
        type: Sequelize.STRING,
      },
      pocPhoneNumber: {
        type: Sequelize.STRING,
      },
      pocEmail: {
        type: Sequelize.STRING,
      },
      address1: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address2: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      zip: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      lat: {
        type: Sequelize.FLOAT,
      },
      lng: {
        type: Sequelize.FLOAT,
      },
      siteCategory: {
        type: siteCategoryType,
        defaultValue: 'RETAILER',
        allowNull: false,
      },
    })
  }

  registerDriverModel() {
    this.Driver = this.db.define('Driver', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      firstName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      lastName: {
        type: Sequelize.STRING,
      },
      driversLicense: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    })
  }

  registerVehicleModel() {
    this.Vehicle = this.db.define('Vehicle', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      licensePlate: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      name: {
        type: Sequelize.STRING,
      },
      make: {
        type: Sequelize.STRING,
      },
      model: {
        type: Sequelize.STRING,
      },
      year: {
        type: Sequelize.INTEGER,
      },
    })
  }

  registerLineItemModel() {
    this.LineItem = this.db.define('LineItem', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      unit: {
        type: Sequelize.STRING,
      },
      quantity: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      pricePerUnit: {
        type: Sequelize.FLOAT,
      },
      isExciseTaxable: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      isSample: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
    })
  }

  registerSkuModel() {
    this.Sku = this.db.define('Sku', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    })
  }

  registerOrganizationInviteJoinModel() {
    this.OrganizationInvite = this.db.define('OrganizationInvite', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      organizationId: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      senderId: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      accepted: {
        type: Sequelize.BOOLEAN,
      },
      respondedAt: {
        type: Sequelize.DATE,
      },
    })
  }

  registerWarehouseModel() {
    this.Warehouse = this.db.define('Warehouse', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
    })
  }

  registerAttachmentModel() {
    this.Attachment = this.db.define('Attachment', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      gDriveFileId: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    })
  }

  registerOrderAttachmentsJoinModel() {
    this.OrderAttachment = this.db.define('OrderAttachment', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      // accessLevel: Sequelize.ENUM('ADMIN', 'READ_WRITE', 'READ_ONLY'),
    })
  }

  registerOrganizationOrganizationJoinModel() {
    this.OrganizationOrganization = this.db.define('OrganizationOrganization', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
    })
  }

  registerInventoryItemModel() {
    this.InventoryItem = this.db.define('InventoryItem', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      counted: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      allocated: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      promised: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      storageType: {
        type: Sequelize.ENUM('INVENTORY', 'QUARANTINE'),
        allowNull: false,
        defaultValue: 'INVENTORY',
      },
    })
  }

  registerInventoryLedgerItemModel() {
    this.InventoryLedgerItem = this.db.define('InventoryLedgerItem', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      transactionType: {
        type: Sequelize.ENUM('INTAKE', 'OUTTAKE', 'CORRECTION', 'ALLOCATION'),
        allowNull: false,
        defaultValue: 'INTAKE',
      },
      startCounted: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      endCounted: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      startAllocated: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      endAllocated: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      startPromised: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      endPromised: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      notes: {
        type: Sequelize.TEXT,
      },
    })
  }

  registerOrderSubscriberJoinModel() {
    this.OrderSubscriber = this.db.define('OrderSubscriber', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      manifestGDriveFileId: {
        type: Sequelize.STRING,
      },
    })
  }

  registerOrderChangeModel() {
    this.OrderChange = this.db.define('OrderChange', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      description: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
    })
  }

  registerLineItemChangeModel() {
    this.LineItemChange = this.db.define('LineItemChange', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      quantity: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      pricePerUnit: {
        type: Sequelize.FLOAT,
      },
      isExciseTaxable: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      isSample: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      type: {
        type: Sequelize.ENUM('ADDED', 'REMOVED'),
        allowNull: false,
        defaultValue: 'REMOVED',
      },
      lineItemId: {
        // this is to ensure added and removed line items match when we show
        // a diff of before and after, with the ultimate goal of showing
        // an updated line item
        type: Sequelize.UUID,
      },
    })
  }

  registerRuModel() {
    this.Ru = this.db.define('Ru', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      isExciseTaxable: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      inventoryType: {
        type: Sequelize.ENUM('CANNABIS', 'NON_CANNABIS'),
        allowNull: false,
        defaultValue: 'CANNABIS',
      },
      inventoryClass: {
        type: Sequelize.ENUM(
          'FLOWER',
          'EDIBLES',
          'PRE_ROLL',
          'CONCENTRATE',
          'CARTRIDGE',
          'DISPOSABLE',
          'TOPICAL',
          'OTHER',
        ),
        allowNull: false,
        defaultValue: 'FLOWER',
      },
      inventoryCategory: {
        type: Sequelize.ENUM(
          'BUD',
          'SHAKE',
          'INDOOR',
          'TRIM',
          'GREENHOUSE',
          'SUN_GROWN',
          'COOKIE',
          'BROWNIE',
          'GUMMY',
          'MARSHMALLOW',
          'TAFFY',
          'MINT',
          'HARD_CANDY',
          'BUTTER',
          'HONEY',
          'SODA',
          'JUICE',
          'TEA',
          'WATER',
          'CHIPS',
          'ICE_CREAM',
          'TINCTURES',
          'SUBLINGUAL',
          'PRE_ROLLS',
          'LEAFLETTES',
          'INFUSED_PRE_ROLLS',
          'BUTANE_HASH_OIL',
          'CO2_OIL',
          'CONCENTRATE',
          'SOLVENTLESS',
          'PAX',
          'THREAD_510',
          'OTHER_BATTERY_TYPES',
          'PEN',
          'TRANSDERMAL',
          'PATCH',
          'LIP_BALM',
          'CREAM',
          'SPRAY',
          'INHALER',
          'DISSOLVABLE_POWDER',
          'MIXED',
          'OTHER',
        ),
        allowNull: false,
        defaultValue: 'BUD',
      },
      pricePerUnit: {
        type: Sequelize.FLOAT,
      },
      unit: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'unit',
      },
    })
  }

  registerBatchModel() {
    this.Batch = this.db.define('Batch', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      manifestGDriveFileId: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      manufacturingDate: {
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
      expirationDate: {
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
    })
  }

  registerLicensedLocationModel() {
    this.LicensedLocation = this.db.define('LicensedLocation', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address1: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address2: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      zip: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      lat: {
        type: Sequelize.FLOAT,
      },
      lng: {
        type: Sequelize.FLOAT,
      },
      siteCategory: {
        type: siteCategoryType,
        defaultValue: 'RETAILER',
        allowNull: false,
      },
    })
  }

  registerForeignKeys() {
    // User <> Organization
    this.User.Organizations = this.User.belongsToMany(this.Organization, {
      as: 'organizations',
      through: this.OrganizationUser,
      foreignKey: 'userId',
    })
    this.Organization.Users = this.Organization.belongsToMany(this.User, {
      as: 'users',
      through: this.OrganizationUser,
      foreignKey: 'organizationId',
    })

    this.User.OwnedOrganizations = this.User.hasMany(this.Organization, {
      as: 'ownedOrganizations',
      foreignKey: 'ownerId',
    })
    this.Organization.Owner = this.Organization.belongsTo(this.User, {
      as: 'owner',
      foreignKey: 'ownerId',
    })

    // User <> Role
    this.User.Roles = this.User.belongsToMany(this.Role, {
      as: 'roles',
      through: this.UserRole,
      foreignKey: 'userId',
    })
    this.Role.Users = this.Role.belongsToMany(this.User, {
      as: 'users',
      through: this.UserRole,
      foreignKey: 'roleId',
    })

    // Organization <> Role
    this.Organization.Roles = this.Organization.hasMany(this.Role, {
      as: 'roles',
      foreignKey: 'organizationId',
    })
    this.Role.Organization = this.Role.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // User <> Draft
    this.User.Drafts = this.User.hasMany(this.Draft, {
      as: 'drafts',
      foreignKey: 'creatorId',
    })
    this.Draft.User = this.Draft.belongsTo(this.User, {
      as: 'creator',
      foreignKey: 'creatorId',
    })

    // User <> Order
    this.User.Orders = this.User.hasMany(this.Order, {
      as: 'orders',
      foreignKey: 'creatorId',
    })
    this.Order.Creator = this.Order.belongsTo(this.User, {
      as: 'creator',
      foreignKey: 'creatorId',
    })

    // Driver <> Order
    this.Driver.Orders = this.Driver.hasMany(this.Order, {
      as: 'orders',
      foreignKey: 'driverId',
    })
    this.Order.Driver = this.Order.belongsTo(this.Driver, {
      as: 'driver',
      foreignKey: 'driverId',
    })

    // Organization <> Order
    this.Organization.Orders = this.Organization.hasMany(this.Order, {
      as: 'orders',
      foreignKey: 'organizationId',
    })
    this.Order.Organization = this.Order.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // Vehicle <> Order
    this.Vehicle.Orders = this.Vehicle.hasMany(this.Order, {
      as: 'orders',
      foreignKey: 'vehicleId',
    })
    this.Order.Vehicle = this.Order.belongsTo(this.Vehicle, {
      as: 'vehicle',
      foreignKey: 'vehicleId',
    })

    // Order <> LineItem
    this.Order.LineItems = this.Order.hasMany(this.LineItem, {
      as: 'lineItems',
      foreignKey: 'orderId',
    })
    this.LineItem.Order = this.LineItem.belongsTo(this.Order, {
      as: 'order',
      foreignKey: 'orderId',
    })

    // Sku <> LineItem
    this.Sku.LineItems = this.Sku.hasMany(this.LineItem, {
      as: 'lineItems',
      foreignKey: 'skuId',
    })

    this.LineItem.Sku = this.LineItem.belongsTo(this.Sku, {
      as: 'sku',
      foreignKey: 'skuId',
    })

    // Site <> Order
    this.Site.Orders = this.Site.hasMany(this.Order, {
      as: 'orders',
      foreignKey: 'siteId',
    })
    this.Order.Site = this.Order.belongsTo(this.Site, {
      as: 'site',
      foreignKey: 'siteId',
    })

    // Organization <> Site
    this.Organization.Sites = this.Organization.hasMany(this.Site, {
      as: 'sites',
      foreignKey: 'organizationId',
    })
    this.Site.Organization = this.Site.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // Organization <> Sku
    this.Organization.Skus = this.Organization.hasMany(this.Sku, {
      as: 'skus',
      foreignKey: 'organizationId',
    })
    this.Sku.Organization = this.Sku.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // Organization <> License
    this.Organization.Licenses = this.Organization.hasMany(this.License, {
      as: 'licenses',
      foreignKey: 'organizationId',
    })
    this.License.Organization = this.License.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // Organization <> LineItem
    this.Organization.LineItems = this.Organization.hasMany(this.LineItem, {
      as: 'lineItems',
      foreignKey: 'organizationId',
    })
    this.LineItem.Organization = this.LineItem.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // Site <> License
    this.Site.Licenses = this.Site.hasMany(this.License, {
      as: 'licenses',
      foreignKey: 'siteId',
    })
    this.License.Site = this.License.belongsTo(this.Site, {
      as: 'licenses',
      foreignKey: 'siteId',
    })

    // Organization <> OrganizationInvite
    this.Organization.OrganizationInvites = this.Organization.hasMany(this.OrganizationInvite, {
      as: 'organizationInvites',
      foreignKey: 'organizationId',
    })
    this.OrganizationInvite.Organization = this.OrganizationInvite.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // User <> OrganizationInvite
    this.User.OrganizationInvites = this.User.hasMany(this.OrganizationInvite, {
      as: 'organizationInvites',
      foreignKey: 'senderId',
    })
    this.OrganizationInvite.User = this.OrganizationInvite.belongsTo(this.User, {
      as: 'sender',
      foreignKey: 'senderId',
    })

    // Order <> Warehouse
    this.Warehouse.Orders = this.Warehouse.hasMany(this.Order, {
      as: 'orders',
      foreignKey: 'warehouseId',
    })
    this.Order.Warehouse = this.Order.belongsTo(this.Warehouse, {
      as: 'warehouse',
      foreignKey: 'warehouseId',
    })

    // Warehouse <> Site
    this.Site.Warehouses = this.Site.hasMany(this.Warehouse, {
      as: 'warehouses',
      foreignKey: 'siteId',
    })
    this.Warehouse.Site = this.Warehouse.belongsTo(this.Site, {
      as: 'site',
      foreignKey: 'siteId',
    })

    // Warehouse <> License
    this.License.Warehouses = this.License.hasMany(this.Warehouse, {
      as: 'warehouses',
      foreignKey: 'licenseId',
    })
    this.Warehouse.License = this.Warehouse.belongsTo(this.License, {
      as: 'license',
      foreignKey: 'licenseId',
    })

    // Organization <> Attachment
    this.Organization.Attachments = this.Organization.hasMany(this.Attachment, {
      as: 'attachments',
      foreignKey: 'organizationId',
    })
    this.Attachment.Organization = this.Attachment.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // Order <> Attachment
    this.Order.Attachments = this.Order.belongsToMany(this.Attachment, {
      as: 'attachments',
      through: this.OrderAttachment,
      foreignKey: 'orderId',
    })
    this.Attachment.Orders = this.Attachment.belongsToMany(this.Order, {
      as: 'orders',
      through: this.OrderAttachment,
      foreignKey: 'attachmentId',
    })

    // Sku <> InventoryItem
    this.Sku.InventoryItems = this.Sku.hasMany(this.InventoryItem, {
      as: 'inventoryItems',
      foreignKey: 'skuId',
    })
    this.InventoryItem.Sku = this.InventoryItem.belongsTo(this.Sku, {
      as: 'sku',
      foreignKey: 'skuId',
    })

    // Site <> InventoryItem
    this.Warehouse.InventoryItems = this.Warehouse.hasMany(this.InventoryItem, {
      as: 'inventoryItems',
      foreignKey: 'warehouseId',
    })
    this.InventoryItem.Warehouse = this.InventoryItem.belongsTo(this.Warehouse, {
      as: 'warehouse',
      foreignKey: 'warehouseId',
    })

    // Organization <> InventoryItem
    this.Organization.InventoryItems = this.Organization.hasMany(this.InventoryItem, {
      as: 'inventoryItems',
      foreignKey: 'organizationId',
    })
    this.InventoryItem.Organization = this.InventoryItem.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // InventoryItem <> InventoryLedgerItem
    this.InventoryItem.InventoryLedgerItems = this.InventoryItem.hasMany(this.InventoryLedgerItem, {
      as: 'inventoryLedgerItems',
      foreignKey: 'inventoryItemId',
    })
    this.InventoryLedgerItem.InventoryItem = this.InventoryLedgerItem.belongsTo(this.InventoryItem, {
      as: 'inventoryItem',
      foreignKey: 'inventoryItemId',
    })

    // Order <> InventoryLedgerItem
    this.Order.InventoryLedgerItems = this.Order.hasMany(this.InventoryLedgerItem, {
      as: 'inventoryLedgerItems',
      foreignKey: 'orderId',
    })
    this.InventoryLedgerItem.Order = this.InventoryLedgerItem.belongsTo(this.Order, {
      as: 'order',
      foreignKey: 'orderId',
    })

    // User <> InventoryLedgerItem
    this.User.InventoryLedgerItems = this.User.hasMany(this.InventoryLedgerItem, {
      as: 'InventoryLedgerItems',
      foreignKey: 'creatorId',
    })
    this.InventoryLedgerItem.Creator = this.InventoryLedgerItem.belongsTo(this.User, {
      as: 'creator',
      foreignKey: 'creatorId',
    })

    // Organization <> InventoryLedgerItem
    this.Organization.InventoryLedgerItems = this.Organization.hasMany(this.InventoryLedgerItem, {
      as: 'inventoryLedgerItems',
      foreignKey: 'organizationId',
    })
    this.InventoryLedgerItem.Organization = this.InventoryLedgerItem.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    // Organization <> Organization
    this.Organization.Suborganization = this.Organization.belongsToMany(this.Organization, {
      as: 'suborganizations',
      through: this.OrganizationOrganization,
      foreignKey: 'suborganizationId',
    })
    this.Organization.Superorganization = this.Organization.belongsToMany(this.Organization, {
      as: 'superorganizations',
      through: this.OrganizationOrganization,
      foreignKey: 'superorganizationId',
    })

    this.Order.Subscribers = this.Order.belongsToMany(this.Organization, {
      as: 'subscribers',
      through: this.OrderSubscriber,
      foreignKey: 'orderId',
    })
    this.Organization.SubscribedOrders = this.Organization.belongsToMany(this.Order, {
      as: 'subscribedOrders',
      through: this.OrderSubscriber,
      foreignKey: 'subscriberId',
    })

    this.Order.OrderChanges = this.Order.hasMany(this.OrderChange, {
      as: 'orderChanges',
      foreignKey: 'orderId',
    })
    this.OrderChange.Order = this.OrderChange.belongsTo(this.Order, {
      as: 'order',
      foreignKey: 'orderId',
    })

    this.User.OrderChanges = this.User.hasMany(this.OrderChange, {
      as: 'orderChanges',
      foreignKey: 'submitterId',
    })
    this.OrderChange.Submitter = this.OrderChange.belongsTo(this.User, {
      as: 'submitter',
      foreignKey: 'submitterId',
    })

    this.Organization.OrderChanges = this.Organization.hasMany(this.OrderChange, {
      as: 'orderChanges',
      foreignKey: 'organizationId',
    })
    this.OrderChange.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    this.OrderChange.LineItemChanges = this.OrderChange.hasMany(this.LineItemChange, {
      as: 'lineItemChanges',
      foreignKey: 'orderChangeId',
    })
    this.LineItemChange.OrderChange = this.LineItemChange.belongsTo(this.OrderChange, {
      as: 'orderChange',
      foreignKey: 'orderChangeId',
    })

    this.Organization.Rus = this.Organization.hasMany(this.Ru, {
      as: 'rus',
      foreignKey: 'organizationId',
    })
    this.Ru.Organization = this.Ru.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    this.Organization.Batches = this.Organization.hasMany(this.Batch, {
      as: 'batches',
      foreignKey: 'organizationId',
    })
    this.Batch.Organization = this.Batch.belongsTo(this.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })

    this.License.Batches = this.License.hasMany(this.Batch, {
      as: 'batches',
      foreignKey: 'licenseId',
    })
    this.Batch.License = this.Batch.belongsTo(this.License, {
      as: 'license',
      foreignKey: 'licenseId',
    })

    this.Ru.Skus = this.Ru.hasMany(this.Sku, {
      as: 'sku',
      foreignKey: 'ruId',
    })
    this.Sku.Ru = this.Sku.belongsTo(this.Ru, {
      as: 'ru',
      foreignKey: 'ruId',
    })

    this.Batch.Skus = this.Batch.hasMany(this.Sku, {
      as: 'skus',
      foreignKey: 'batchId',
    })
    this.Sku.Batch = this.Sku.belongsTo(this.Batch, {
      as: 'batch',
      foreignKey: 'batchId',
    })

    this.LicensedLocation.Licenses = this.LicensedLocation.hasMany(this.License, {
      as: 'licenses',
      foreignKey: 'licensedLocationId',
    })
    this.License.LicensedLocation = this.License.belongsTo(this.LicensedLocation, {
      as: 'licensedLocation',
      foreignKey: 'licensedLocationId',
    })

    this.LicensedLocation.Site = this.LicensedLocation.hasMany(this.Site, {
      as: 'sites',
      foreignKey: 'licensedLocationId',
    })
    this.Site.LicensedLocation = this.Site.belongsTo(this.LicensedLocation, {
      as: 'licensedLocation',
      foreignKey: 'licensedLocationId',
    })

    this.Order.Collections = this.Order.hasMany(this.Order, {
      as: 'collections',
      foreignKey: 'collectWithOrderId',
    })
    this.Order.CollectWithOrder = this.Order.belongsTo(this.Order, {
      as: 'collectWithOrder',
      foreignKey: 'collectWithOrderId',
    })

    // Warehouse <> LicensedLocation
    this.Warehouse.LicensedLocation = this.Warehouse.hasMany(this.LicensedLocation, {
      as: 'licensedLocations',
      foreignKey: 'warehouseId',
      constraints: false,
    })
    this.LicensedLocation.Warehouse = this.LicensedLocation.belongsTo(this.Warehouse, {
      as: 'warehouse',
      foreignKey: 'warehouseId',
      constraints: false,
    })
  }

  initializeModels() {
    this.registerUserModel()
    this.registerOrganizationModel()
    this.registerOrganizationUsersJoinModel()
    this.registerLicenseModel()
    this.registerRoleModel()
    this.registerUserRolesJoinModel()
    this.registerUserPreferenceJoinModel()
    this.registerOrderModel()
    this.registerSiteModel()
    this.registerVehicleModel()
    this.registerLineItemModel()
    this.registerSkuModel()
    this.registerOrganizationInviteJoinModel()
    this.registerWarehouseModel()
    this.registerAttachmentModel()
    this.registerOrderAttachmentsJoinModel()
    this.registerInventoryItemModel()
    this.registerInventoryLedgerItemModel()
    this.registerOrganizationOrganizationJoinModel()
    this.registerDriverModel()
    this.registerOrderSubscriberJoinModel()
    this.registerOrderChangeModel()
    this.registerLineItemChangeModel()
    this.registerDraftModel()
    this.registerRuModel()
    this.registerBatchModel()
    this.registerLicensedLocationModel()

    // Register foreign keys for relationships
    this.registerForeignKeys()
  }
}

export default LogisticsDB
