import Sequelize from 'sequelize'
import logger from '../../logging'

class PostgresBase {
  constructor(config) {
    this.config = config
    this.db = new Sequelize(config.database, config.username, config.password, {
      host: config.host,
      port: config.port,
      benchmark: true,
      typeValidation: true,
      define: {
        timestamps: true,
        freezeTableName: true,
        charset: 'utf8',
        collate: 'utf8',
      },
      dialect: 'postgres',
      dialectOptions: {
        charset: 'utf8',
      },
      logging: false,
      // operatorsAliases: false,
    })
  }

  authenticate() {
    return this.db
      .authenticate()
      .then((err) => {
        if (err) {
          logger.error(`Failed to connect to database ${this.config.database}.`)
          return false
        }
        logger.info(`Connection to database ${this.config.database} has been established successfully.`)
        return true
      })
      .catch((err) => {
        logger.error(`Unable to connect to the database ${this.config.database}:`, err)
        return false
      })
  }

  sync(opts) {
    return this.db
      .sync(opts)
      .then((res) => {
        const dbName = this.config.database
        logger.info(`Done syncing database ${dbName}`)
        logger.info(`${dbName} models: ${res.models ? Object.keys(res.models) : res}`)
        return res
      })
      .catch((err) => {
        logger.error(`Error syncing database: ${err.message}`)
        throw err
      })
  }
}

export default PostgresBase
