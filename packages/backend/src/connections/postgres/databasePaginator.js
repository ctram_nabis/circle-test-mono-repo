class Paginator {
  constructor({ query, limit, queryInterface }) {
    this.limit = limit
    this.offset = 0
    this._hasNext = true
    this.queryFn = query
    this.queryInterface = queryInterface
    this.queryOpts = { type: this.queryInterface.sequelize.QueryTypes.SELECT }
  }
  hasNext() {
    return this._hasNext
  }
  next() {
    return new Promise((resolve, reject) => {
      if (!this.hasNext()) {
        resolve()
        return
      }

      const query = this.queryFn(this.limit, this.offset)

      this.queryInterface.sequelize
        .query(query, this.queryOpts)
        .then((result) => {
          if (result.length < this.limit) {
            this._hasNext = false
          }

          this.offset += this.limit
          resolve(result)
        })
        .catch(reject)
    })
  }
}

export default Paginator
