const fs = require('fs')
const scrypt = require('scrypt-for-humans')
const faker = require('faker')

async function generateUsers() {
  let users = []

  for (let i = 0; i < 49; i++) {
    const firstName = faker.name.firstName()
    const email = `${firstName.toLowerCase()}@${faker.internet.domainName()}`
    const password = await scrypt.hash(faker.internet.password())

    const data = {
      email,
      firstName,
      lastName: faker.name.lastName(),
      password,
      isAdmin: i === 0,
    }

    users.push(data)
  }

  fs.writeFile('../users.json', JSON.stringify(users), () => {})
}

function generateOrganizations() {
  let organizations = []

  for (let i = 0; i < 49; i++) {}
}

generateUsers()
