'use strict'
const users = require('./users.json')
const factory = require('factory-girl')
const User = require('../../user')
const scrypt = require('scrypt-for-humans')
const faker = require('faker')

async function generateUsers() {
  let users = []

  for (let i = 0; i < 49; i++) {
    const firstName = faker.name.firstName()
    const email = `${firstName.toLowerCase()}@${faker.internet.domainName()}`
    const password = await scrypt.hash(faker.internet.password())

    const data = {
      email,
      firstName,
      lastName: faker.name.lastName(),
      password,
      isAdmin: i === 0,
    }

    factory.define('user', User)

    factory.build('user', data)
  }
}

function generateOrganizations() {
  let organizations = []

  for (let i = 0; i < 49; i++) {}
}

generateUsers()
