module.exports = (model, queryInterface, Sequelize) => {
  const table = model(queryInterface.sequelize, Sequelize)

  let attributes = new Object(table.rawAttributes)
  for (let a in attributes) {
    for (let s in attributes[a]) {
      if (s.toLowerCase() == 'model' || s.toLowerCase() == '_modelattribute') {
        delete attributes[a][s]
      }
    }
  }

  return queryInterface.createTable(table.getTableName(), attributes)
}
