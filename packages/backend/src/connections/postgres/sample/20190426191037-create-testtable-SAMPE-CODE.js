const testtable = require('../models/testtable')
const define = require('../define')

module.exports = {
  up: (queryInterface, Sequelize) => define(testtable, queryInterface, Sequelize),

  down: (queryInterface, Sequelize) => {
    const table = testtable(queryInterface.sequelize, Sequelize)
    return queryInterface.dropTable(table.getTableName())
  },
}
