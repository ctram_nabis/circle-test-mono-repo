module.exports = (sequelize, Sequelize) => {
  const testtable = sequelize.define(
    'testtable',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    },
    { freezeTableName: true },
  )
  testtable.associate = function(models) {
    // associations can be defined here
  }
  return testtable
}
