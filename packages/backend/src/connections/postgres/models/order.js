import { ORDER_ACTIONS, ORDER_STATUS } from '../../../constants'

export default (sequelize, Sequelize) => {
  return sequelize.define(
    'Order',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      number: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
      },
      irn: {
        type: Sequelize.STRING,
      },
      date: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: new Date(),
      },
      estimatedArrivalTimeAfter: {
        type: Sequelize.DATE,
      },
      estimatedArrivalTimeBefore: {
        type: Sequelize.DATE,
      },
      timeWindow: {
        type: Sequelize.STRING,
      },
      action: {
        type: Sequelize.ENUM(
          ORDER_ACTIONS.DELIVERY_TO_RETAILER,
          ORDER_ACTIONS.RETURN_FROM_RETAILER,
          ORDER_ACTIONS.PICKUP_FROM_BRAND,
          ORDER_ACTIONS.DELIVERY_TO_BRAND,
          ORDER_ACTIONS.DROPOFF_TO_NABIS,
          ORDER_ACTIONS.PICKUP_FROM_NABIS,
          ORDER_ACTIONS.PAYMENT_PICKUP_FROM_RETAILER,
          ORDER_ACTIONS.INTERNAL_TRANSFER,
          ORDER_ACTIONS.OTHER,
        ),
        allowNull: false,
      },
      orgLicenseNum: {
        type: Sequelize.STRING,
      },
      siteLicenseNum: {
        type: Sequelize.STRING,
      },
      notes: {
        type: Sequelize.TEXT,
      },
      status: {
        type: Sequelize.ENUM(
          ORDER_STATUS.DRAFT,
          ORDER_STATUS.APPROVAL_NEEDED,
          ORDER_STATUS.UNSCHEDULED,
          ORDER_STATUS.SCHEDULED,
          ORDER_STATUS.TRANSFERRING,
          ORDER_STATUS.DELAYED,
          ORDER_STATUS.CANCELLED,
          ORDER_STATUS.REJECTED,
          ORDER_STATUS.DELIVERED,
          ORDER_STATUS.DELIVERED_WITH_EDITS,
        ),
        defaultValue: 'UNSCHEDULED',
        allowNull: false,
      },
      gmv: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      discount: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      exciseTax: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      gmvCollected: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      exciseTaxCollected: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0.0,
      },
      paymentStatus: {
        type: Sequelize.ENUM('UNPAID', 'COD_PAID', 'NET_TERMS_PAID', 'REMITTED', 'SELF_COLLECTED'),
        defaultValue: 'UNPAID',
        allowNull: false,
      },
      adminNotes: {
        type: Sequelize.TEXT,
      },
      internalNotes: {
        type: Sequelize.TEXT,
      },
      manifestGDriveFileId: {
        type: Sequelize.STRING,
      },
      daysTillPaymentDue: {
        type: Sequelize.INTEGER,
      },
      licensedLocationId: {
        type: Sequelize.UUID,
      },
      isArchived: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      isExcluded: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      intaken: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      outtaken: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      edited: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      surcharge: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      hasUnalterablePaymentTerms: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      mustPayPreviousBalance: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      apSummaryGDriveFileId: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null,
      },
      invoicesS3FileLink: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null,
      },
    },
    { freezeTableName: true },
  )
}
