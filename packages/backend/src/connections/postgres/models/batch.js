module.exports = (sequelize, Sequelize) =>
  sequelize.define(
    'Batch',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      expirationDate: {
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
      isArchived: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      manifestGDriveFileId: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      manufacturingDate: {
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
      status: {
        type: Sequelize.ENUM('QUARANTINE', 'SAMPLED', 'PASSED', 'FAILED'),
        defaultValue: 'QUARANTINE',
        allowNull: false,
      },
      testDate: {
        type: Sequelize.DATE,
        defaultValue: null,
      },
      deletedAt: {
        type: Sequelize.DATE,
        defaultValue: null,
      },
    },
    { freezeTableName: true, paranoid: true },
  )
