module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'LineItemChange',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      skuBatchId: {
        type: Sequelize.UUID,
      },
      quantity: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      pricePerUnit: {
        type: Sequelize.FLOAT,
      },
      isExciseTaxable: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      isSample: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      type: {
        type: Sequelize.ENUM('ADDED', 'REMOVED'),
        allowNull: false,
        defaultValue: 'REMOVED',
      },
      lineItemId: {
        // this is to ensure added and removed line items match when we show
        // a diff of before and after, with the ultimate goal of showing
        // an updated line item
        type: Sequelize.UUID,
      },
      discount: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
    },
    { freezeTableName: true },
  )
}
