module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'OrganizationInvite',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      organizationId: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      senderId: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      accepted: {
        type: Sequelize.BOOLEAN,
      },
      respondedAt: {
        type: Sequelize.DATE,
      },
    },
    { freezeTableName: true },
  )
}
