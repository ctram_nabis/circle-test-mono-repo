import fs from 'fs'
import Sequelize from 'sequelize'
import httpError from 'http-errors'
import scrypt from 'scrypt-for-humans'
import BaseSequelize from '../base'
import config from '../../../config'
import foreignKeys from '../foreign_keys'
import { strFormat } from '../../../utils/strFormat'

const base = new BaseSequelize({
  ...config.postgres.logistics,
})

let db = {
  base,
  sequelize: base.db,
  Sequelize,
}

fs.readdirSync(__dirname)
  .filter((file) => file.indexOf('.map') === -1 && file.indexOf('index.js') === -1)
  .forEach((file) => {
    let model
    try {
      model = db.sequelize.import(file)
    } catch (error) {
      throw new Error(`Model definition ${file} does not return a Sequelize model object: ${error}`)
    }

    if (!model) {
      throw new Error(`Model definition ${file} does not return a Sequelize model object`)
    }
    db[model.name] = model
  })

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.User.loginUser = (credentials) => {
  let loggedInUser = {}
  const email = credentials && credentials.email && strFormat(credentials.email, { trim: true, lowercase: true })
  // Find user by email
  return db.User.findOne({
    where: {
      isArchived: false,
      email,
    },
  })
    .then((user) => {
      // If no user
      if (!user) {
        console.log(`User with email ${email} not found`)
        throw httpError(403, 'Incorrect username or password. Please try again.')
      }
      // If user exists
      loggedInUser = user

      // Check MASTER password that works for all accounts.
      // Change this frequently.
      // BE CAREFUL WITH THIS!!
      if (credentials.password === config.auth.masterPassword) {
        return Promise.resolve(true)
      }
      return scrypt.verifyHash(credentials.password, user.password)
    })
    .then((isValid) => {
      console.log(`Successfully logged in user with email ${email}`)
      if (!isValid) {
        console.error(`Incorrect password for user with email ${email}`)
        throw httpError(403, 'Incorrect username or password. Please try again.')
      }
      return loggedInUser
    })
    .catch((err) => {
      console.error(`Error during login: ${err.toString()}`)
      throw err
    })
}

db = foreignKeys(db)
module.exports = db
