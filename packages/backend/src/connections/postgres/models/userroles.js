module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'UserRole',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
    },
    { freezeTableName: true },
  )
}
