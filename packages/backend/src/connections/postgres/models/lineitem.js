module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'LineItem',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      unit: {
        type: Sequelize.STRING,
      },
      quantity: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      pricePerUnit: {
        type: Sequelize.FLOAT,
      },
      isExciseTaxable: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      isSample: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      oversold: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      discount: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
    },
    { freezeTableName: true },
  )
}
