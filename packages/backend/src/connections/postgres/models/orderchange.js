module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'OrderChange',
    {
      description: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      id: {
        allowNull: false,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        type: Sequelize.UUID,
      },
    },
    { freezeTableName: true },
  )
}
