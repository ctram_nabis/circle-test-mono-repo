module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'OrderAttachment',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      // accessLevel: Sequelize.ENUM('ADMIN', 'READ_WRITE', 'READ_ONLY'),
    },
    { freezeTableName: true },
  )
}
