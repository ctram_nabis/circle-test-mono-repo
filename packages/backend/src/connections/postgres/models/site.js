module.exports = (sequelize, Sequelize) => {
  const Site = sequelize.define(
    'Site',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      pocName: {
        type: Sequelize.STRING,
      },
      pocPhoneNumber: {
        type: Sequelize.STRING,
      },
      pocEmail: {
        type: Sequelize.STRING,
      },
      address1: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address2: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      zip: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      lat: {
        type: Sequelize.FLOAT,
      },
      lng: {
        type: Sequelize.FLOAT,
      },
      siteCategory: {
        type: Sequelize.ENUM(
          'RETAILER',
          'MANUFACTURER',
          'CULTIVATOR',
          'CONSUMER',
          'DISTRIBUTOR',
          'MICROBUSINESS',
          'OTHER',
        ),
        defaultValue: 'RETAILER',
        allowNull: false,
      },
    },
    { freezeTableName: true },
  )

  Site.associate = (models) => {
    Site.Orders = Site.hasMany(models.Order, {
      as: 'orders',
      foreignKey: 'siteId',
    })
    Site.Organization = Site.belongsTo(models.Organization, {
      as: 'organization',
      foreignKey: 'organizationId',
    })
    Site.Licenses = Site.hasMany(models.License, {
      as: 'licenses',
      foreignKey: 'siteId',
    })
    Site.Warehouses = Site.hasMany(models.Warehouse, {
      as: 'warehouses',
      foreignKey: 'siteId',
    })
    Site.LicensedLocation = Site.belongsTo(models.LicensedLocation, {
      as: 'licensedLocation',
      foreignKey: 'licensedLocationId',
    })
    return Site
  }

  return Site
}
