module.exports = (sequelize, Sequelize) =>
  sequelize.define(
    'SkuBatch',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      titleholderId: {
        type: Sequelize.UUID,
        allowNull: true,
      },
    },

    { freezeTableName: true, paranoid: true, timestamps: true },
  )
