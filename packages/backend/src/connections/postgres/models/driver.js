module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'Driver',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      firstName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      lastName: {
        type: Sequelize.STRING,
      },
      driversLicense: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    },
    { freezeTableName: true },
  )
}
