module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'InventoryItem',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      counted: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      allocated: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      promised: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      storageType: {
        type: Sequelize.ENUM('INVENTORY', 'QUARANTINE'),
        allowNull: false,
        defaultValue: 'INVENTORY',
      },
    },
    { freezeTableName: true },
  )
}
