module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'Role',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      permissions: {
        type: Sequelize.ARRAY(Sequelize.TEXT),
        defaultValue: [],
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
        defaultValue: ' ',
        allowNull: false,
      },
    },
    { freezeTableName: true },
  )
}
