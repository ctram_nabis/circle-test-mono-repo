module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'Organization',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      alias: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      address1: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address2: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
      },
      state: {
        type: Sequelize.STRING,
      },
      zip: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
      },
      manifestGDriveFolderId: {
        type: Sequelize.STRING,
      },
      doingBusinessAs: {
        type: Sequelize.STRING,
      },
    },
    { freezeTableName: true },
  )
}
