module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'InventoryLedgerItem',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      transactionType: {
        type: Sequelize.ENUM('INTAKE', 'OUTTAKE', 'CORRECTION', 'ALLOCATION'),
        allowNull: false,
        defaultValue: 'INTAKE',
      },
      startCounted: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      endCounted: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      startAllocated: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      endAllocated: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      startPromised: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      endPromised: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
      notes: {
        type: Sequelize.TEXT,
      },
    },
    { freezeTableName: true },
  )
}
