module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'OrganizationUser',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
    },
    { freezeTableName: true },
  )
}
