module.exports = (sequelize, Sequelize) =>
  sequelize.define(
    'Titleholder',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      organizationId: {
        type: Sequelize.UUID,
        allowNull: true,
      },
      titleholderOrganizationId: {
        type: Sequelize.UUID,
        allowNull: true,
        unique: true,
      },
      titleholderLicenseId: {
        type: Sequelize.UUID,
        allowNull: true,
        unique: true,
      },
      type: {
        type: Sequelize.ENUM('LICENSE', 'ORGANIZATION'),
        allowNull: false,
      },
      createdAt: {
        defaultValue: new Date(),
        type: Sequelize.DATE,
      },
      updatedAt: {
        defaultValue: new Date(),
        type: Sequelize.DATE,
      },
    },
    { freezeTableName: true },
  )
