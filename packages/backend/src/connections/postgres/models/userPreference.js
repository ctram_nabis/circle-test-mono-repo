module.exports = (sequelize, Sequelize) =>
  sequelize.define(
    'UserPreference',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      receiveOrderUpdateEmails: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
    },
    { freezeTableName: true, paranoid: true, timestamps: true },
  )
