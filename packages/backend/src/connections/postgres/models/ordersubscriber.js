module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'OrderSubscriber',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      manifestGDriveFileId: {
        type: Sequelize.STRING,
      },
      invoicesS3FileLink: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null,
      },
    },
    { freezeTableName: true },
  )
}
