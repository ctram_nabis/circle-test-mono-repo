module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'LicensedLocation',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address1: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address2: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      zip: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      lat: {
        type: Sequelize.FLOAT,
      },
      lng: {
        type: Sequelize.FLOAT,
      },
      siteCategory: {
        type: Sequelize.ENUM(
          'RETAILER',
          'MANUFACTURER',
          'CULTIVATOR',
          'CONSUMER',
          'DISTRIBUTOR',
          'MICROBUSINESS',
          'OTHER',
        ),
        defaultValue: 'RETAILER',
        allowNull: false,
      },
      verificationStatus: {
        type: Sequelize.ENUM('UNVERIFIED', 'VERIFIED', 'REJECTED'),
        defaultValue: 'UNVERIFIED',
      },
      verificationAt: {
        type: Sequelize.DATE,
      },
      submittedById: {
        type: Sequelize.UUID,
      },
      isArchived: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      deletedAt: {
        type: Sequelize.DATE,
        defaultValue: null,
      },
    },
    { freezeTableName: true },
  )
}
