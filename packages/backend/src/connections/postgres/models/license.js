module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'License',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      nickname: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      category: {
        type: Sequelize.ENUM('ADULT_USE', 'MEDICINAL', 'COMBINED'),
        defaultValue: 'ADULT_USE',
      },
      type: {
        type: Sequelize.ENUM(
          'RETAILER',
          'RETAILER_NONSTOREFRONT',
          'DISTRIBUTOR',
          'DISTRIBUTOR_TRANSPORT_ONLY',
          'MICROBUSINESS',
          'TESTING',
          'CANNABIS_EVENTS',
          'MANUFACTURER',
          'CULTIVATOR',
        ),
        defaultValue: 'MANUFACTURER',
        allowNull: false,
      },
      licenseNumber: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      legalEntityName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      issuanceDate: {
        type: Sequelize.DATE,
      },
      expirationDate: {
        type: Sequelize.DATE,
      },
      contactName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      contactPhone: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      contactEmail: {
        type: Sequelize.STRING,
        defaultValue: null,
        allowNull: true,
      },
      address1: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      address2: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
      },
      state: {
        type: Sequelize.STRING,
      },
      zip: {
        type: Sequelize.STRING,
      },
    },
    { freezeTableName: true },
  )
}
