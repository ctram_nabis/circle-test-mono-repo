export default (sequelize, Sequelize) =>
  sequelize.define(
    'DateRule',
    {
      id: {
        allowNull: false,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      dayOfYear: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      monthDay: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      maxOrders: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.INTEGER,
      },
      createdAt: {
        defaultValue: new Date(),
        type: Sequelize.DATE,
      },
      updatedAt: {
        defaultValue: new Date(),
        type: Sequelize.DATE,
      },
      deletedAt: {
        defaultValue: null,
        type: Sequelize.DATE,
      },
    },
    { freezeTableName: true },
  )
