module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'Attachment',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      gDriveFileId: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    },
    { freezeTableName: true },
  )
}
