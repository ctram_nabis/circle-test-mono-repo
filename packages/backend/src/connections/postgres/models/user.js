const scrypt = require('scrypt-for-humans')
const get = require('lodash/get')
const { strFormat } = require('../../../utils/strFormat')

module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'User',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: Sequelize.STRING(512),
        allowNull: false,
      },
      firstName: {
        type: Sequelize.STRING,
      },
      lastName: {
        type: Sequelize.STRING,
      },
      address1: {
        type: Sequelize.STRING,
      },
      address2: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
      },
      state: {
        type: Sequelize.STRING,
      },
      zip: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
      },
      profilePicture: {
        type: Sequelize.TEXT,
      },
      isAdmin: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      isArchived: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
    },
    {
      freezeTableName: true,
      hooks: {
        beforeCreate: (instance) =>
          scrypt.hash(instance.password).then((hash) => {
            const user = instance
            user.password = hash
            return {
              ...user,
              email: strFormat(user.email, { trim: true, lowercase: true }),
            }
          }),
        beforeUpdate: ({ attributes, options }) => {
          if (attributes.password && options && options.newPassword) {
            return scrypt.hash(options.newPassword).then((hash) => {
              const user = attributes
              user.password = hash
              return user
            })
          } else if (attributes.password && !get(options, 'newPassword')) {
            return scrypt.hash(attributes.password).then((hash) => {
              const user = attributes
              user.password = hash
              return user
            })
          }
          return attributes
        },
        beforeBulkUpdate: ({ attributes, options }) => {
          if (attributes.password && options && options.newPassword) {
            return scrypt.hash(options.newPassword).then((hash) => {
              const user = attributes
              user.password = hash
              return user
            })
          } else if (attributes.password && !get(options, 'newPassword')) {
            return scrypt.hash(attributes.password).then((hash) => {
              const user = attributes
              user.password = hash
              return user
            })
          }
          return attributes
        },
      },
    },
  )
}
