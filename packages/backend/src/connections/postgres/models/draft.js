module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    'Draft',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },
      body: {
        type: Sequelize.JSON,
      },
    },
    { freezeTableName: true },
  )
}
