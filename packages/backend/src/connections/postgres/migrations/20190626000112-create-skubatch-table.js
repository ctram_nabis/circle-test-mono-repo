module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('SkuBatch', {
        id: {
          type: Sequelize.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: Sequelize.UUIDV4,
        },
        skuId: {
          defaultValue: null,
          type: Sequelize.UUID,
        },
        batchId: {
          defaultValue: null,
          type: Sequelize.UUID,
        },
        createdAt: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('NOW'),
        },
        updatedAt: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('NOW'),
        },
      })
      .then(() =>
        queryInterface.addConstraint('SkuBatch', ['batchId'], {
          type: 'foreign key',
          name: 'SkuBatch_batchId_fk',
          references: {
            table: 'Batch',
            field: 'id',
          },
          onDelete: 'set null',
          onUpdate: 'cascade',
        }),
      )
      .then(() =>
        queryInterface.addConstraint('SkuBatch', ['skuId'], {
          type: 'foreign key',
          name: 'SkuBatch_skuId_fk',
          references: {
            table: 'Sku',
            field: 'id',
          },
          onDelete: 'cascade',
          onUpdate: 'cascade',
        }),
      )
  },

  down: (queryInterface, Sequelize) => queryInterface.dropTable('SkuBatch'),
}
