module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize.query(`
      BEGIN;

      -- skuBatchId '81f861ff-3714-4028-8374-d39762a589b7'
      DELETE FROM "InventoryItem" WHERE id = '96c48c55-7ca1-4a94-ab39-0e9bde0d3dc2';

      -- skuBatchId '2ca6beb0-d0ed-43da-8bc7-7a3ce22f723e'
      DELETE FROM "InventoryItem" WHERE id = '2f2bccf2-9015-4b33-82c6-ab8ad6ba7265'; 

      -- skuBatchId 'e99fac7e-4c51-41b2-b9db-630f7a93c64e'
      DELETE FROM "InventoryItem" WHERE id = '0a8a96b4-703c-4d24-9037-b6502153820f';

      -- skuBatchId '802e785c-e6eb-4c89-b129-12c94ed744a9'
      DELETE FROM "InventoryItem" WHERE id = '2140ae72-a6b4-443b-9cad-4883301d0da6';

      -- skuBatchId '60fea0b4-c7f2-4eb1-a8a1-6aca6ce2490e'
      UPDATE "InventoryLedgerItem"
      SET ("startCounted", "endCounted", "inventoryItemId") = (50.0, 150.0, '1f72e23e-9f4a-4bd4-978a-e2540aa72ee1')
      WHERE id = 'ce9c76c2-e7b4-4366-b02f-e1efb3528c45';
      UPDATE "InventoryItem" SET counted = 150.0 WHERE id = '1f72e23e-9f4a-4bd4-978a-e2540aa72ee1';
      DELETE FROM "InventoryItem" WHERE id = '96fd7249-ee3a-4a79-9782-b57263ddea77';

      -- skuBatchId 'bf7ace67-b464-4002-b68d-166bd492b047'
      DELETE FROM "InventoryItem" WHERE id = 'dd9081f4-e6bc-4e44-9588-83ad9e9bda5a';

      -- skuBatchId '3fee1306-0022-4bf2-9e8b-23e69458c9ad'
      DELETE FROM "InventoryItem" WHERE id = '3164d9b0-ef86-453b-8469-b2d9c6fea629';

      -- skuBatchId '863be550-c5fd-46ac-8bd3-bd9b39a10474'
      DELETE FROM "InventoryItem" WHERE id = 'df43f3c2-2ea1-48ed-b5ca-ad9d98826db3';

      -- skuBatchId '2025a8f6-8d93-44c2-b929-a38ef59d336e'
      DELETE FROM "InventoryItem" WHERE id = '78ba923b-063e-41f7-b0de-fbefee036534';

      -- skuBatchId 'b0bf6ef5-ed4e-4815-a1b2-936441203260'
      DELETE FROM "InventoryItem" WHERE id = '2ea21799-4491-4931-9a38-16ea3e8e30dd';

      -- skuBatchId 'b7ea7784-6af1-4fb6-a809-75b24f97082b'
      DELETE FROM "InventoryItem" WHERE id = '2993cc59-e8ef-4c12-b9cd-672e03a5e19d';

      -- skuBatchId 'fe6e080e-8925-4032-95d0-8e22769ce3a0'
      DELETE FROM "InventoryItem" WHERE id = '6478d3c4-dd23-4dd4-9d3c-2f4f84626ada';

      -- skuBatchId '870b8387-b2cc-4141-ad1e-d7637bda3a87'
      DELETE FROM "InventoryItem" WHERE id = 'f46ee348-43e7-4ed6-b50d-260cbec29957';

      -- skuBatchId 'ae170002-9995-4cd0-85a4-90d5f9518c81'
      DELETE FROM "InventoryItem" WHERE id = 'f8e62eb2-70ea-4bad-b3fc-b419257c69be';

      -- skuBatchId 'cb829d29-4daa-4d46-bd6c-f1e38ee9b8a8'
      DELETE FROM "InventoryItem" WHERE id = 'b8811b2a-5454-44c2-8aa4-08e18e803f42';

      -- skuBatchId '1da73d64-b4a9-4076-957b-88b009046ed1'
      DELETE FROM "InventoryItem" WHERE id = 'e613d175-5e46-498e-9d1b-7ba1e29c2876';

      -- skuBatchId 'e98eb71a-4386-41d1-9a3f-63ea0e075ce3'
      DELETE FROM "InventoryItem" WHERE id = 'd3340064-2544-49c0-84c0-754ef93060b5';

      -- skuBatchId 'dd9bf114-d46b-4a0d-a069-671030e209e6'
      DELETE FROM "InventoryItem" WHERE id = '651f1f47-a4fb-4ff4-a309-43fc3f48923b';

      -- skuBatchId '1bb7a952-471c-4bed-b9d7-f671fa27fc4a'
      UPDATE "InventoryLedgerItem"
      SET ("startCounted", "endCounted", "inventoryItemId") = (190.0, 290.0, '9884bcad-1688-46d0-b625-8c89b538a3f9')
      WHERE id = 'efdcbafa-ff30-4b8a-b317-8cd3495cdac5';
      UPDATE "InventoryItem" SET counted = 290.0 WHERE id = '9884bcad-1688-46d0-b625-8c89b538a3f9';
      DELETE FROM "InventoryItem" WHERE id = 'c2d302bc-cd89-460d-abd8-83a62d801407';

      -- skuBatchId '1b12bc1d-40a3-484d-bd80-5813e85d469b'
      UPDATE "InventoryLedgerItem"
      SET ("startCounted", "endCounted", "inventoryItemId") = (224.0, 186.0, '0c73d379-0373-45f5-a812-0570994d9826')
      WHERE id = 'd5850b9a-352f-48eb-b6eb-fb9ffa196e6c';
      UPDATE "InventoryItem" SET counted = 186.0 WHERE id = '0c73d379-0373-45f5-a812-0570994d9826';
      DELETE FROM "InventoryItem" WHERE id = '94e2e939-e76c-489c-bfe8-6e89b72668ea';

      -- skuBatchId '0223ca0e-c87a-46ed-a86e-bad509dec866'
      DELETE FROM "InventoryItem" WHERE id = 'd2ff8c5b-2275-429f-9377-e425ef8e8e4b';

      -- skuBatchId '696001c4-c97c-40a5-90bd-efd1099f582f'
      DELETE FROM "InventoryItem" WHERE id = '89303f3d-c1a4-4434-b4e3-9a0d792d9476';

      -- skuBatchId 'de2a10fd-1dfd-4155-a952-34be320f4d3c'
      DELETE FROM "InventoryItem" WHERE id = 'e229e7c2-787b-46fc-af94-e33a19c91c38';

      COMMIT;
    `),

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  },
}
