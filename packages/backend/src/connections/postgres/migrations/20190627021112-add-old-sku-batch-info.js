const uuid = require('uuid/v4')
const array = require('../seeders/SkuBatch')

module.exports = {
  up: (queryInterface, Sequelize) => {
    const toJSON = array.map((a) => ({
      id: uuid(),
      skuId: a[0],
      batchId: a.length < 2 ? null : a[1],
      updatedAt: new Date(),
      createdAt: new Date(),
    }))

    return queryInterface
      .bulkInsert('SkuBatch', toJSON)
      .then((res) => {
        console.log('Total SkuBatches Created:', res)
        return queryInterface.sequelize
          .query(
            `
        select count(*) from "SkuBatch" where "skuId" is not null;
      `,
          )
          .then((count) => {
            console.log('Total Valid SkuBatches: ', count[0])
            return res
          })
      })
      .catch((error) => {
        console.log(error.parent.detail)
      })
      .finally(() =>
        queryInterface.sequelize
          .query(
            `
      select count(*) from "SkuBatch" where "skuId" is not null;
    `,
          )
          .then((count) => console.log('Total Valid SkuBatches: ', count[0])),
      )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize
      .query(
        `
      select count(*) from "SkuBatch" where "skuId" is not null;
    `,
      )
      .then((res) => {
        console.log('Total Valid SkuBatches: ', res[0])
        return res
      })
  },
}
