'use strict'

const monthDayMap = [
  {
    end: 31,
    month: 1,
    start: 1,
  },
  {
    end: 60,
    month: 2,
    start: 32,
  },
  {
    end: 91,
    month: 3,
    start: 61,
  },
  {
    end: 121,
    month: 4,
    start: 92,
  },
  {
    end: 152,
    month: 5,
    start: 122,
  },
  {
    end: 182,
    month: 6,
    start: 153,
  },
  {
    end: 213,
    month: 7,
    start: 183,
  },
  {
    end: 244,
    month: 8,
    start: 214,
  },
  {
    end: 274,
    month: 9,
    start: 245,
  },
  {
    end: 305,
    month: 10,
    start: 275,
  },
  {
    end: 335,
    month: 11,
    start: 306,
  },
  {
    end: 366,
    month: 12,
    start: 336,
  },
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('DateRule', {
      id: {
        allowNull: false,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      dayOfYear: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      monthDay: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      maxOrders: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.INTEGER,
      },
      createdAt: {
        defaultValue: Sequelize.fn('NOW'),
        type: Sequelize.DATE,
      },
      updatedAt: {
        defaultValue: Sequelize.fn('NOW'),
        type: Sequelize.DATE,
      },
      deletedAt: {
        defaultValue: null,
        type: Sequelize.DATE,
      },
    })

    const toDayMonthString = (dOY) => {
      const paddedDOY = dOY + 1

      const [monthInfo] = monthDayMap.filter(({ start, end }) => paddedDOY >= start && paddedDOY <= end)

      const dayOfMonth = paddedDOY + 1 - monthInfo.start
      const monthDayStr = `${monthInfo.month}/${dayOfMonth}`
      return monthDayStr
    }

    const daysOfYear = new Array(366).fill(0).map((_, i) => i) // [0..365]
    const rows = daysOfYear.map((day) => ({
      createdAt: new Date(),
      dayOfYear: day,
      monthDay: toDayMonthString(day),
      updatedAt: new Date(),
    }))

    const inserts = rows.map(({ createdAt, dayOfYear, monthDay, updatedAt }) => {
      const query = `
        insert into "DateRule" (
          id,
          "createdAt",
          "monthDay",
          "dayOfYear",
          "updatedAt"
        ) values (
          uuid_generate_v4(),
          '${createdAt.toUTCString()}',
          '${monthDay}',
          ${dayOfYear},
          '${updatedAt.toUTCString()}'
        );
      `
      return queryInterface.sequelize.query(query)
    })

    return Promise.all(inserts)
  },

  down: (queryInterface, _Sequelize) => queryInterface.dropTable('DateRule'),
}
