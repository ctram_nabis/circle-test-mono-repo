module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('LineItem', 'batchId', Sequelize.UUID).then(() =>
      queryInterface.addConstraint('LineItem', ['batchId'], {
        type: 'foreign key',
        name: 'LineItem_batchId_fkey',
        references: {
          table: 'Batch',
          field: 'id',
        },
        onUpdate: 'cascade',
        onDelete: 'set null',
      }),
    ),

  down: (queryInterface, Sequelize) => queryInterface.removeColumn('LineItem', 'batchId'),
}
