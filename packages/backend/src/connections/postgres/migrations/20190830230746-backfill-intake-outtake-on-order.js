'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize.query(`
      BEGIN;
      -- update for intake
      UPDATE
        "Order"
      SET
        intaken = TRUE
      FROM ( SELECT DISTINCT
          "orderId"
        FROM
          "InventoryLedgerItem"
        WHERE
          "transactionType" = 'INTAKE') ledger_items
      WHERE
        id = ledger_items. "orderId";
      -- update for outtake
      UPDATE
        "Order"
      SET
        outtaken = TRUE
      FROM ( SELECT DISTINCT
          "orderId"
        FROM
          "InventoryLedgerItem"
        WHERE
          "transactionType" = 'OUTTAKE') ledger_items
      WHERE
        id = ledger_items. "orderId";
      COMMIT;
  `),

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  },
}
