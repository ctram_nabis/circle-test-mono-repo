'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`ALTER TABLE "OrderChange" ALTER COLUMN "description" SET DATA TYPE TEXT`)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      `ALTER TABLE "OrderChange" ALTER COLUMN "description" SET DATA TYPE VARCHAR(255)`,
    )
  },
}
