'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('Order', 'edited', {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      })
      .then((res) => {
        return queryInterface.sequelize.query(`
        UPDATE public."Order"
        SET "edited" = true
        WHERE public."Order"."id" IN (
          SELECT public."OrderChange"."orderId" FROM public."OrderChange"
          INNER JOIN public."LineItemChange"
          ON public."OrderChange"."id" = public."LineItemChange"."orderChangeId"
          WHERE (
            public."OrderChange"."description" = 'Updated shipment contents' OR
            public."OrderChange"."description" = 'Removed order contents' OR
            public."OrderChange"."description" = 'Added shipment contents'
          )
        );
      `)
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Order', 'edited')
  },
}
