module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('Role', 'description', Sequelize.TEXT, {
      defaultValue: ' ',
    }),

  down: (queryInterface) => queryInterface.removeColumn('Role', 'description'),
}
