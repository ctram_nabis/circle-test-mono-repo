module.exports = {
  up: (queryInterface) =>
    queryInterface.addConstraint('SkuBatch', ['batchId', 'skuId'], {
      type: 'unique',
      name: 'SkuBatch_skuId_batchId_key',
    }),

  down: (queryInterface) => queryInterface.removeConstraint('SkuBatch', 'SkuBatch_skuId_batchId_key'),
}
