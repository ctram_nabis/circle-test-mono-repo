module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      `
ALTER TABLE "public"."Attachment" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Attachment" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Attachment" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Batch" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Batch" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Batch" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Draft" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Draft" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Draft" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Driver" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Driver" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Driver" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."InventoryItem" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."InventoryItem" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."InventoryItem" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."InventoryLedgerItem" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."InventoryLedgerItem" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."InventoryLedgerItem" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."License" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."License" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."License" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."LicensedLocation" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."LicensedLocation" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."LicensedLocation" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."LineItem" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."LineItem" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."LineItem" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."LineItemChange" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."LineItemChange" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."LineItemChange" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."OldSku" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."OldSku" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."OldSku" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Order" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Order" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Order" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."OrderAttachments" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."OrderAttachments" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."OrderAttachments" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."OrderChange" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."OrderChange" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."OrderChange" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."OrderSubscriber" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."OrderSubscriber" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."OrderSubscriber" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Organization" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Organization" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Organization" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."OrganizationInvite" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."OrganizationInvite" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."OrganizationInvite" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."OrganizationUsers" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."OrganizationUsers" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."OrganizationUsers" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Role" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Role" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Role" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Ru" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Ru" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Ru" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Site" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Site" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Site" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Sku" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Sku" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Sku" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."SkuBatch" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."SkuBatch" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."SkuBatch" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."User" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."User" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."User" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."UserRoles" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."UserRoles" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."UserRoles" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Vehicle" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Vehicle" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Vehicle" ALTER COLUMN "updatedAt" SET DEFAULT now();

ALTER TABLE "public"."Warehouse" ALTER COLUMN "id" SET DEFAULT uuid_generate_v4();
ALTER TABLE "public"."Warehouse" ALTER COLUMN "createdAt" SET DEFAULT now();
ALTER TABLE "public"."Warehouse" ALTER COLUMN "updatedAt" SET DEFAULT now();
      `,
    )
  },

  down: (queryInterface, Sequelize) => {
    return Promise.resolve()
  },
}
