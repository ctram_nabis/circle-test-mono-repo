'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('InventoryLedgerItem', 'lineItemId', Sequelize.UUID),

  down: (queryInterface) => queryInterface.removeColumn('InventoryLedgerItem', 'lineItemId'),
}
