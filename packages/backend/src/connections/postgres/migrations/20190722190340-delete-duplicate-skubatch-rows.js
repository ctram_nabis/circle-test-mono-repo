module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize.query(`
      begin;

      update "LineItem" set "skuBatchId" = '4b2716ce-b628-4659-9aac-f9d8624770d5' where "skuBatchId" = 'f27a2bfe-ea55-4325-af7a-9475a377f7ee';
      delete from "SkuBatch" where id = 'f27a2bfe-ea55-4325-af7a-9475a377f7ee';

      update "LineItem" set "skuBatchId" = 'e9b1bc0f-3138-49b0-ab18-8c8b054470da' where "skuBatchId" = '1ba59a5d-3d83-4bb8-8d2e-a38cc1123132';
      delete from "SkuBatch" where id = '1ba59a5d-3d83-4bb8-8d2e-a38cc1123132';

      update "LineItem" set "skuBatchId" = '45a14722-c328-496d-9a1f-df5b63d6c8be' where "skuBatchId" = '08ab846f-8d73-4e2c-826a-faca484a83ca';
      delete from "SkuBatch" where id = '08ab846f-8d73-4e2c-826a-faca484a83ca';

      update "LineItem" set "skuBatchId" = 'ce12bb84-e24a-4093-a2c6-1ae75034e431' where "skuBatchId" = 'd8bb2688-3d71-43ec-bd0a-5c1788048dc8';
      delete from "SkuBatch" where id = 'd8bb2688-3d71-43ec-bd0a-5c1788048dc8';

      update "LineItem" set "skuBatchId" = '56eeb9fc-d864-4d27-b74d-1cd6f7c6c2c9' where "skuBatchId" = '18fafb14-c763-4ac1-8855-98fc61a43925';
      delete from "SkuBatch" where id = '18fafb14-c763-4ac1-8855-98fc61a43925';

      update "LineItem" set "skuBatchId" = 'f151a0e0-628b-496f-ba0d-b2d42c205176' where "skuBatchId" = '2a85359f-516f-44bc-a7c7-9464d87e337f';
      delete from "SkuBatch" where id = '2a85359f-516f-44bc-a7c7-9464d87e337f';

      update "LineItem" set "skuBatchId" = 'cc24d5d8-7936-4909-9620-cbf584dc440b' where "skuBatchId" = 'ae86df7a-e4bd-4398-9f44-4b238de5f314';
      delete from "SkuBatch" where id = 'ae86df7a-e4bd-4398-9f44-4b238de5f314';

      update "LineItem" set "skuBatchId" = 'c102d477-7d7a-4ee0-91af-6a6b16e55de0' where "skuBatchId" = '10a37356-b23a-4a1d-857f-de717d8132c1';
      delete from "SkuBatch" where id = '10a37356-b23a-4a1d-857f-de717d8132c1';

      update "LineItem" set "skuBatchId" = 'a8f23733-714d-4791-8b22-cac9654065ca' where "skuBatchId" = 'a3273ada-5eb5-4602-a721-e48a90c9e53d';
      delete from "SkuBatch" where id = 'a3273ada-5eb5-4602-a721-e48a90c9e53d';
      delete from "InventoryItem" where id = '9c8fd721-cd6a-4d28-87b4-5fed8cee762c'; -- duplicate inventory

      update "LineItem" set "skuBatchId" = '5ed72161-6f98-40aa-aba0-684f93e66806' where "skuBatchId" = '336fe4ae-1c08-4619-96c8-19f47f5baadb';
      delete from "SkuBatch" where id = '336fe4ae-1c08-4619-96c8-19f47f5baadb';

      update "LineItem" set "skuBatchId" = 'f5870471-9b80-4db5-909a-e8d9036edfe2' where "skuBatchId" = 'a8c0a373-3bd9-4814-a4f6-0dc42dee352a';
      update "LineItem" set "skuBatchId" = 'f5870471-9b80-4db5-909a-e8d9036edfe2' where "skuBatchId" = '6ec070ee-3063-4870-a6ca-05156ec07e9c';
      delete from "SkuBatch" where id = 'a8c0a373-3bd9-4814-a4f6-0dc42dee352a';

      update "LineItem" set "skuBatchId" = '4c09c904-4b49-45ed-832d-55ae3e0c767a' where "skuBatchId" = 'ba901bd3-7d14-455e-8f51-ba841c32c79d';
      delete from "SkuBatch" where id = 'ba901bd3-7d14-455e-8f51-ba841c32c79d';
      commit;
    `),

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  },
}
