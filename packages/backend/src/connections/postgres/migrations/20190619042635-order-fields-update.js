module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize
      .query(
        `
      ALTER TYPE "enum_Order_paymentStatus" RENAME TO "enum_Order_paymentStatus_old";
      CREATE TYPE "enum_Order_paymentStatus" AS ENUM('UNPAID', 'COD_PAID', 'NET_TERMS_PAID', 'REMITTED', 'SELF_COLLECTED');
      ALTER TABLE public."Order" ALTER COLUMN "paymentStatus" DROP DEFAULT;
      ALTER TABLE public."Order" ALTER COLUMN "paymentStatus" TYPE "enum_Order_paymentStatus" USING "paymentStatus"::text::"enum_Order_paymentStatus";
      ALTER TABLE public."Order" ALTER COLUMN "paymentStatus" SET DEFAULT 'UNPAID';
      DROP TYPE "enum_Order_paymentStatus_old";
    `,
      )
      .then(() => {
        return queryInterface.sequelize.query(`
        ALTER TYPE "enum_Order_action" ADD VALUE 'DELIVERY_TO_RETAILER';
      `)
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        ALTER TYPE "enum_Order_action" ADD VALUE 'RETURN_FROM_RETAILER';
      `)
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        ALTER TYPE "enum_Order_action" ADD VALUE 'PICKUP_FROM_BRAND';
      `)
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        ALTER TYPE "enum_Order_action" ADD VALUE 'INTERNAL_TRANSFER';
      `)
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        UPDATE public."Order" SET action = 'DELIVERY_TO_RETAILER' WHERE action = 'DROPOFF';
        ALTER TABLE public."Order" ALTER COLUMN action SET DEFAULT 'DELIVERY_TO_RETAILER';
        UPDATE public."Order" SET action = 'RETURN_FROM_RETAILER' WHERE action = 'RETURN';
        UPDATE public."Order" SET action = 'PICKUP_FROM_BRAND' WHERE action = 'PICKUP';
        UPDATE public."Order" SET action = 'INTERNAL_TRANSFER' WHERE action = 'TRANSFER';
        ALTER TYPE "enum_Order_action" RENAME TO "enum_Order_action_old";
        CREATE TYPE "enum_Order_action" AS ENUM('DELIVERY_TO_RETAILER', 'RETURN_FROM_RETAILER', 'PICKUP_FROM_BRAND', 'DELIVERY_TO_BRAND', 'DROPOFF_TO_NABIS', 'PICKUP_FROM_NABIS', 'PAYMENT_PICKUP_FROM_RETAILER', 'INTERNAL_TRANSFER', 'OTHER');
        ALTER TABLE public."Order" ALTER COLUMN "action" DROP DEFAULT;
        ALTER TABLE public."Order" ALTER COLUMN "action" TYPE "enum_Order_action" USING "action"::text::"enum_Order_action";
        ALTER TABLE public."Order" ALTER COLUMN "action" SET DEFAULT 'DELIVERY_TO_RETAILER';
        DROP TYPE "enum_Order_action_old";
      `)
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize
      .query(
        `
      UPDATE public."Order" SET "paymentStatus" = 'UNPAID' WHERE "paymentStatus" = 'SELF_COLLECTED';
      ALTER TYPE "enum_Order_paymentStatus" RENAME TO "enum_Order_paymentStatus_old";
      CREATE TYPE "enum_Order_paymentStatus" AS ENUM('UNPAID', 'COD_PAID', 'NET_TERMS_PAID', 'REMITTED');
      ALTER TABLE public."Order" ALTER COLUMN "paymentStatus" DROP DEFAULT;
      ALTER TABLE public."Order" ALTER COLUMN "paymentStatus" TYPE "enum_Order_paymentStatus" USING "paymentStatus"::text::"enum_Order_paymentStatus";
      ALTER TABLE public."Order" ALTER COLUMN "paymentStatus" SET DEFAULT 'UNPAID';
      DROP TYPE "enum_Order_paymentStatus_old";
    `,
      )
      .then(() => {
        return queryInterface.sequelize.query(`
        ALTER TYPE "enum_Order_action" ADD VALUE 'DROPOFF';
      `)
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        ALTER TYPE "enum_Order_action" ADD VALUE 'RETURN';
      `)
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        ALTER TYPE "enum_Order_action" ADD VALUE 'PICKUP';
      `)
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        ALTER TYPE "enum_Order_action" ADD VALUE 'TRANSFER';
      `)
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        UPDATE public."Order" SET action = 'DROPOFF' WHERE action = 'DELIVERY_TO_RETAILER';
        ALTER TABLE public."Order" ALTER COLUMN action SET DEFAULT 'DROPOFF';
        UPDATE public."Order" SET action = 'RETURN' WHERE action = 'RETURN_FROM_RETAILER';
        UPDATE public."Order" SET action = 'PICKUP' WHERE action = 'PICKUP_FROM_BRAND';
        UPDATE public."Order" SET action = 'TRANSFER' WHERE action = 'INTERNAL_TRANSFER';
        ALTER TYPE "enum_Order_action" RENAME TO "enum_Order_action_old";
        CREATE TYPE "enum_Order_action" AS ENUM('DROPOFF', 'RETURN', 'PICKUP', 'TRANSFER');
        ALTER TABLE public."Order" ALTER COLUMN "action" DROP DEFAULT;
        ALTER TABLE public."Order" ALTER COLUMN "action" TYPE "enum_Order_action" USING "action"::text::"enum_Order_action";
        ALTER TABLE public."Order" ALTER COLUMN "action" SET DEFAULT 'DROPOFF';
        DROP TYPE "enum_Order_action_old";
      `)
      })
  },
}
