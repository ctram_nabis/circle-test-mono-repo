const { roles } = require('../../../../test/fixtures/roleFixture')
const role = require('../models/role')

module.exports = {
  up: (queryInterface, Sequelize) => {
    const table = role(queryInterface.sequelize, Sequelize)
    return table.bulkCreate(roles)
  },
  down: (queryInterface) =>
    queryInterface.sequelize.query('delete from "Role" where true;').then((res) => {
      console.log(res[0])
    }),
}
