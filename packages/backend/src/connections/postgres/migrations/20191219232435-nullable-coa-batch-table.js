'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`ALTER TABLE "Batch" ALTER COLUMN "manifestGDriveFileId" DROP NOT NULL;`)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`ALTER TABLE "Batch" ALTER COLUMN "manifestGDriveFileId" SET NOT NULL;`)
  },
}
