module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize.query(`
    INSERT INTO "UserRoles"
    SELECT
    uuid_generate_v4() AS "id",
    u."createdAt",
    r."updatedAt",
    u.id as "userId",
    r.id as "roleId",
    null as "organizationId"
    FROM
    "User" u JOIN "Role" r ON
    r.name ilike 'admin';
  `),
  down: (queryInterface) =>
    queryInterface.sequelize.query('delete from "UserRoles" where true;').then((res) => {
      console.log(res[0])
    }),
}
