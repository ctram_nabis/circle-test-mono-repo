module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize
      .query(
        `
    update "InventoryItem" as ii set
      "batchId" = c.batch,
      "skuBatchId" = c.skubatch
    from (
      select
        i.id as id,
        sb."batchId" as batch,
        sb.id as skubatch
      from "InventoryItem" i join "SkuBatch" sb
        on i."skuId" = sb."skuId"
    ) as c(id, batch, skubatch)
      where c.id = ii.id;
  `,
      )
      .then(() =>
        queryInterface.sequelize
          .query(
            `
    select count(*) from "InventoryItem" where "skuBatchId" is null;
  `,
          )
          .then((res) => {
            console.log('Total Invalid InventoryItems: ', res[0])
            return res
          }),
      ),

  down: (queryInterface, Sequelize) =>
    queryInterface.sequelize
      .query(
        `
    select count(*) from "InventoryItem" where "skuBatchId" is null;
  `,
      )
      .then((res) => {
        console.log('Total Invalid InventoryItems: ', res[0])
        return res
      }),
}
