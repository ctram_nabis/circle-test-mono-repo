module.exports = {
  up: (queryInterface) => queryInterface.removeColumn('Sku', 'batchId'),

  down: (queryInterface, Sequelize) =>
    queryInterface
      .addColumn('Sku', 'batchId', Sequelize.UUID, {
        defaultValue: null,
      })
      .then(() =>
        queryInterface.sequelize.query(`
    update "Sku" as s set
      "batchId" = c.batch
    from (
      select
        k.id as sku,
        b.id as batch
      from "Batch" b join "Sku" k
      on b."skuId" = k.id
    ) as c(sku, batch)
    where c.sku = s.id;
  `),
      ),
}
