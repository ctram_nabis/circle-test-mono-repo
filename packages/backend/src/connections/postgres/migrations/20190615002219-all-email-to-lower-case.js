module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.sequelize
      .query(
        `
      select
        ut.id
      from "User" uo join "User" ut on
        uo.id <> ut.id and
        uo.email ilike ut.email
      join "OrganizationUsers" ou on
        uo.id = ou."userId" and
        uo.email <> 'Armando@montereytilthfarm.com' and
        uo.email <> 'armando@montereytilthfarm.com';
    `,
      )
      .then((res) => {
        const toDelete = res[0].map((i) => i.id)
        console.log('To delete: ', toDelete)
        return queryInterface.bulkDelete('User', {
          id: toDelete,
        })
      })
      .then(() =>
        queryInterface.sequelize.query(`
      update "User" u set email=(lower(u.email)) where
        u.email <> 'Armando@montereytilthfarm.com' and
        u.email <> 'armando@montereytilthfarm.com';
    `),
      )
  },

  down: (queryInterface) =>
    queryInterface.sequelize
      .query(
        `
    select count(*) from "User";
  `,
      )
      .then((res) => {
        console.log('total users: ', res[0])
        return res
      }),
}
