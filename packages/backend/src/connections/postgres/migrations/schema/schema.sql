--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: enum_InventoryItem_serviceType; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_InventoryItem_serviceType" AS ENUM (
    'FULFILLMENT',
    'SALES',
    'STORAGE',
    'OTHER'
);


ALTER TYPE public."enum_InventoryItem_serviceType" OWNER TO "Nabis";

--
-- Name: enum_InventoryItem_storageType; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_InventoryItem_storageType" AS ENUM (
    'INVENTORY',
    'QUARANTINE'
);


ALTER TYPE public."enum_InventoryItem_storageType" OWNER TO "Nabis";

--
-- Name: enum_InventoryLedgerItem_transactionType; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_InventoryLedgerItem_transactionType" AS ENUM (
    'INTAKE',
    'OUTTAKE',
    'CORRECTION',
    'ALLOCATION'
);


ALTER TYPE public."enum_InventoryLedgerItem_transactionType" OWNER TO "Nabis";

--
-- Name: enum_License_category; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_License_category" AS ENUM (
    'ADULT_USE',
    'MEDICINAL',
    'COMBINED'
);


ALTER TYPE public."enum_License_category" OWNER TO "Nabis";

--
-- Name: enum_License_type; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_License_type" AS ENUM (
    'RETAILER',
    'RETAILER_NONSTOREFRONT',
    'DISTRIBUTOR',
    'DISTRIBUTOR_TRANSPORT_ONLY',
    'MICROBUSINESS',
    'TESTING_LABORATORY',
    'TESTING',
    'CANNABIS_EVENTS',
    'MANUFACTURER',
    'CULTIVATOR'
);


ALTER TYPE public."enum_License_type" OWNER TO "Nabis";

--
-- Name: enum_LicensedLocation_siteCategory; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_LicensedLocation_siteCategory" AS ENUM (
    'RETAILER',
    'MANUFACTURER',
    'CULTIVATOR',
    'CONSUMER',
    'DISTRIBUTOR',
    'MICROBUSINESS',
    'OTHER'
);


ALTER TYPE public."enum_LicensedLocation_siteCategory" OWNER TO "Nabis";

--
-- Name: enum_LicensedLocation_verificationStatus; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_LicensedLocation_verificationStatus" AS ENUM (
    'VERIFIED',
    'UNVERIFIED',
    'REJECTED'
);


ALTER TYPE public."enum_LicensedLocation_verificationStatus" OWNER TO "Nabis";

--
-- Name: enum_LineItemChange_type; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_LineItemChange_type" AS ENUM (
    'ADDED',
    'REMOVED'
);


ALTER TYPE public."enum_LineItemChange_type" OWNER TO "Nabis";

--
-- Name: enum_Order_action; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Order_action" AS ENUM (
    'PICKUP',
    'DROPOFF',
    'RETURN',
    'TRANSFER'
);


ALTER TYPE public."enum_Order_action" OWNER TO "Nabis";

--
-- Name: enum_Order_paymentStatus; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Order_paymentStatus" AS ENUM (
    'UNPAID',
    'COD_PAID',
    'NET_TERMS_PAID',
    'REMITTED'
);


ALTER TYPE public."enum_Order_paymentStatus" OWNER TO "Nabis";

--
-- Name: enum_Order_size; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Order_size" AS ENUM (
    'XSMALL',
    'SMALL',
    'MEDIUM',
    'LARGE',
    'XLARGE'
);


ALTER TYPE public."enum_Order_size" OWNER TO "Nabis";

--
-- Name: enum_Order_status; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Order_status" AS ENUM (
    'DRAFT',
    'APPROVAL_NEEDED',
    'UNSCHEDULED',
    'SCHEDULED',
    'TRANSFERRING',
    'DELAYED',
    'CANCELLED',
    'REJECTED',
    'DELIVERED',
    'DELIVERED_WITH_EDITS'
);


ALTER TYPE public."enum_Order_status" OWNER TO "Nabis";

--
-- Name: enum_Ru_inventoryCategory; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Ru_inventoryCategory" AS ENUM (
    'BUD',
    'SHAKE',
    'INDOOR',
    'TRIM',
    'GREENHOUSE',
    'SUN_GROWN',
    'COOKIE',
    'BROWNIE',
    'GUMMY',
    'MARSHMALLOW',
    'TAFFY',
    'MINT',
    'HARD_CANDY',
    'BUTTER',
    'HONEY',
    'SODA',
    'JUICE',
    'TEA',
    'WATER',
    'CHIPS',
    'ICE_CREAM',
    'TINCTURES',
    'SUBLINGUAL',
    'PRE_ROLLS',
    'LEAFLETTES',
    'INFUSED_PRE_ROLLS',
    'BUTANE_HASH_OIL',
    'CO2_OIL',
    'CONCENTRATE',
    'SOLVENTLESS',
    'PAX',
    'THREAD_510',
    'OTHER_BATTERY_TYPES',
    'PEN',
    'TRANSDERMAL',
    'PATCH',
    'LIP_BALM',
    'CREAM',
    'SPRAY',
    'INHALER',
    'DISSOLVABLE_POWDER',
    'MIXED',
    'OTHER'
);


ALTER TYPE public."enum_Ru_inventoryCategory" OWNER TO "Nabis";

--
-- Name: enum_Ru_inventoryClass; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Ru_inventoryClass" AS ENUM (
    'FLOWER',
    'EDIBLES',
    'PRE_ROLL',
    'CONCENTRATE',
    'CARTRIDGE',
    'DISPOSABLE',
    'TOPICAL',
    'OTHER'
);


ALTER TYPE public."enum_Ru_inventoryClass" OWNER TO "Nabis";

--
-- Name: enum_Ru_inventoryType; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Ru_inventoryType" AS ENUM (
    'CANNABIS',
    'NON_CANNABIS'
);


ALTER TYPE public."enum_Ru_inventoryType" OWNER TO "Nabis";

--
-- Name: enum_Site_siteCategory; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Site_siteCategory" AS ENUM (
    'RETAILER',
    'MANUFACTURER',
    'CULTIVATOR',
    'CONSUMER',
    'DISTRIBUTOR',
    'MICROBUSINESS',
    'OTHER'
);


ALTER TYPE public."enum_Site_siteCategory" OWNER TO "Nabis";

--
-- Name: enum_Sku_inventoryCategory; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Sku_inventoryCategory" AS ENUM (
    'BUD',
    'SHAKE',
    'INDOOR',
    'TRIM',
    'GREENHOUSE',
    'SUN_GROWN',
    'COOKIE',
    'BROWNIE',
    'GUMMY',
    'MARSHMALLOW',
    'TAFFY',
    'MINT',
    'HARD_CANDY',
    'BUTTER',
    'HONEY',
    'SODA',
    'JUICE',
    'TEA',
    'WATER',
    'CHIPS',
    'ICE_CREAM',
    'TINCTURES',
    'SUBLINGUAL',
    'PRE_ROLLS',
    'LEAFLETTES',
    'INFUSED_PRE_ROLLS',
    'BUTANE_HASH_OIL',
    'CO2_OIL',
    'CONCENTRATE',
    'SOLVENTLESS',
    'PAX',
    'THREAD_510',
    'OTHER_BATTERY_TYPES',
    'PEN',
    'TRANSDERMAL',
    'PATCH',
    'LIP_BALM',
    'CREAM',
    'SPRAY',
    'INHALER',
    'DISSOLVABLE_POWDER',
    'MIXED',
    'OTHER'
);


ALTER TYPE public."enum_Sku_inventoryCategory" OWNER TO "Nabis";

--
-- Name: enum_Sku_inventoryClass; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Sku_inventoryClass" AS ENUM (
    'FLOWER',
    'EDIBLES',
    'PRE_ROLL',
    'CONCENTRATE',
    'CARTRIDGE',
    'DISPOSABLE',
    'TOPICAL',
    'OTHER'
);


ALTER TYPE public."enum_Sku_inventoryClass" OWNER TO "Nabis";

--
-- Name: enum_Sku_inventoryType; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Sku_inventoryType" AS ENUM (
    'CANNABIS',
    'NON_CANNABIS'
);


ALTER TYPE public."enum_Sku_inventoryType" OWNER TO "Nabis";

--
-- Name: enum_Sku_unitOfMeasure; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Sku_unitOfMeasure" AS ENUM (
    'EACH',
    'MILLIGRAM',
    'GRAM',
    'KILOGRAM',
    'OUNCE',
    'POUND',
    'METRIC_TONS'
);


ALTER TYPE public."enum_Sku_unitOfMeasure" OWNER TO "Nabis";

--
-- Name: enum_Stop_action; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Stop_action" AS ENUM (
    'PICKUP',
    'DROPOFF',
    'PRINT',
    'OTHER'
);


ALTER TYPE public."enum_Stop_action" OWNER TO "Nabis";

--
-- Name: enum_Stop_status; Type: TYPE; Schema: public; Owner: Nabis
--

CREATE TYPE public."enum_Stop_status" AS ENUM (
    'INCOMPLETE',
    'UNSCHEDULED',
    'IN_PROGRESS',
    'DELAYED',
    'SKIPPED',
    'COMPLETE',
    'CANCELLED'
);


ALTER TYPE public."enum_Stop_status" OWNER TO "Nabis";

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Attachment; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Attachment" (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    "gDriveFileId" character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "organizationId" uuid
);


ALTER TABLE public."Attachment" OWNER TO "Nabis";

--
-- Name: Batch; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Batch" (
    id uuid NOT NULL,
    code character varying(255) NOT NULL,
    "manifestGDriveFileId" character varying(255) NOT NULL,
    "manufacturingDate" timestamp with time zone DEFAULT '2019-03-05 19:40:43.208-08'::timestamp with time zone,
    "expirationDate" timestamp with time zone DEFAULT '2019-03-05 19:40:43.208-08'::timestamp with time zone,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "organizationId" uuid,
    "licenseId" uuid
);


ALTER TABLE public."Batch" OWNER TO "Nabis";

--
-- Name: Draft; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Draft" (
    id uuid NOT NULL,
    body json,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "creatorId" uuid
);


ALTER TABLE public."Draft" OWNER TO "Nabis";

--
-- Name: Driver; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Driver" (
    id uuid NOT NULL,
    "firstName" character varying(255) NOT NULL,
    "lastName" character varying(255),
    "driversLicense" character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Driver" OWNER TO "Nabis";

--
-- Name: InventoryItem; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."InventoryItem" (
    id uuid NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "skuId" uuid,
    "organizationId" uuid,
    counted double precision DEFAULT 0 NOT NULL,
    allocated double precision DEFAULT 0 NOT NULL,
    promised double precision DEFAULT 0 NOT NULL,
    "storageType" public."enum_InventoryItem_storageType" DEFAULT 'INVENTORY'::public."enum_InventoryItem_storageType" NOT NULL,
    "warehouseId" uuid
);


ALTER TABLE public."InventoryItem" OWNER TO "Nabis";

--
-- Name: InventoryLedgerItem; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."InventoryLedgerItem" (
    id uuid NOT NULL,
    "startCounted" double precision DEFAULT 0 NOT NULL,
    "endCounted" double precision DEFAULT 0 NOT NULL,
    notes text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "inventoryItemId" uuid,
    "orderId" uuid,
    "organizationId" uuid,
    "transactionType" public."enum_InventoryLedgerItem_transactionType" DEFAULT 'ALLOCATION'::public."enum_InventoryLedgerItem_transactionType" NOT NULL,
    "startAllocated" double precision,
    "endAllocated" double precision,
    "startPromised" double precision,
    "endPromised" double precision,
    "creatorId" uuid
);


ALTER TABLE public."InventoryLedgerItem" OWNER TO "Nabis";

--
-- Name: License; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."License" (
    id uuid NOT NULL,
    nickname character varying(255) NOT NULL,
    category public."enum_License_category" DEFAULT 'ADULT_USE'::public."enum_License_category",
    type public."enum_License_type" DEFAULT 'MANUFACTURER'::public."enum_License_type" NOT NULL,
    "licenseNumber" character varying(255) NOT NULL,
    "legalEntityName" character varying(255) NOT NULL,
    "issuanceDate" timestamp with time zone,
    "expirationDate" timestamp with time zone,
    "contactName" character varying(255) NOT NULL,
    "contactPhone" character varying(255) NOT NULL,
    address1 character varying(255) NOT NULL,
    address2 character varying(255),
    city character varying(255),
    state character varying(255),
    zip character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "organizationId" uuid,
    "siteId" uuid,
    "licensedLocationId" uuid
);


ALTER TABLE public."License" OWNER TO "Nabis";

--
-- Name: LicensedLocation; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."LicensedLocation" (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    address1 character varying(255) NOT NULL,
    address2 character varying(255),
    city character varying(255) NOT NULL,
    state character varying(255) NOT NULL,
    zip character varying(255) NOT NULL,
    lat double precision,
    lng double precision,
    "siteCategory" public."enum_LicensedLocation_siteCategory" DEFAULT 'RETAILER'::public."enum_LicensedLocation_siteCategory" NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "warehouseId" uuid,
    "verificationStatus" public."enum_LicensedLocation_verificationStatus",
    "submittedById" uuid,
    "verificationAt" timestamp with time zone
);


ALTER TABLE public."LicensedLocation" OWNER TO "Nabis";

--
-- Name: LineItem; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."LineItem" (
    id uuid NOT NULL,
    quantity double precision NOT NULL,
    "pricePerUnit" double precision,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    code character varying(255) DEFAULT '0000'::character varying NOT NULL,
    name character varying(255) DEFAULT '----'::character varying NOT NULL,
    unit character varying(255),
    "orderId" uuid,
    "isExciseTaxable" boolean DEFAULT true NOT NULL,
    "organizationId" uuid,
    "skuId" uuid,
    "isSample" boolean DEFAULT false,
    oversold boolean DEFAULT false
);


ALTER TABLE public."LineItem" OWNER TO "Nabis";

--
-- Name: LineItemChange; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."LineItemChange" (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    quantity double precision NOT NULL,
    "pricePerUnit" double precision,
    "isExciseTaxable" boolean DEFAULT true NOT NULL,
    "isSample" boolean DEFAULT false,
    type public."enum_LineItemChange_type" DEFAULT 'REMOVED'::public."enum_LineItemChange_type" NOT NULL,
    "lineItemId" uuid,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "orderChangeId" uuid
);


ALTER TABLE public."LineItemChange" OWNER TO "Nabis";

--
-- Name: OldSku; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."OldSku" (
    id uuid NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    weight double precision,
    height double precision,
    length double precision,
    width double precision,
    unit character varying(255) DEFAULT 'Unit'::character varying NOT NULL,
    "pricePerUnit" double precision,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "organizationId" uuid,
    "isExciseTaxable" boolean DEFAULT true NOT NULL,
    "unitOfMeasure" public."enum_Sku_unitOfMeasure" DEFAULT 'EACH'::public."enum_Sku_unitOfMeasure" NOT NULL,
    description text,
    "inventoryType" public."enum_Sku_inventoryType" DEFAULT 'CANNABIS'::public."enum_Sku_inventoryType" NOT NULL,
    "inventoryClass" public."enum_Sku_inventoryClass" DEFAULT 'FLOWER'::public."enum_Sku_inventoryClass" NOT NULL,
    "inventoryCategory" public."enum_Sku_inventoryCategory" DEFAULT 'BUD'::public."enum_Sku_inventoryCategory" NOT NULL,
    strain character varying(255),
    "thcMg" double precision DEFAULT 0.00 NOT NULL,
    "cbdMg" double precision DEFAULT 0.00 NOT NULL
);


ALTER TABLE public."OldSku" OWNER TO "Nabis";

--
-- Name: Order_number_seq; Type: SEQUENCE; Schema: public; Owner: Nabis
--

CREATE SEQUENCE public."Order_number_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public."Order_number_seq" OWNER TO "Nabis";

--
-- Name: Order; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Order" (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    notes text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "driverId" uuid,
    "organizationId" uuid,
    "vehicleId" uuid,
    gmv double precision DEFAULT 0.0 NOT NULL,
    number integer DEFAULT nextval('public."Order_number_seq"'::regclass) NOT NULL,
    date timestamp with time zone DEFAULT '2018-03-26 01:54:30.244-07'::timestamp with time zone,
    "exciseTax" double precision DEFAULT 0 NOT NULL,
    "timeWindow" character varying(255),
    "paymentTerms" character varying(255),
    "siteId" uuid,
    "adminNotes" text,
    "manifestGDriveFileId" character varying(255),
    "orgLicenseNum" character varying(255),
    "siteLicenseNum" character varying(255),
    "warehouseId" uuid,
    "creatorId" uuid,
    discount double precision DEFAULT 0.00 NOT NULL,
    "gmvCollected" double precision DEFAULT 0 NOT NULL,
    "exciseTaxCollected" double precision DEFAULT 0 NOT NULL,
    "paymentStatus" public."enum_Order_paymentStatus" DEFAULT 'UNPAID'::public."enum_Order_paymentStatus" NOT NULL,
    status public."enum_Order_status" DEFAULT 'UNSCHEDULED'::public."enum_Order_status",
    irn character varying(255),
    "estimatedArrivalTimeBefore" timestamp with time zone,
    "estimatedArrivalTimeAfter" timestamp with time zone,
    "daysTillPaymentDue" integer,
    "collectWithOrderId" uuid,
    "isArchived" boolean,
    "licensedLocationId" uuid,
    action public."enum_Order_action" DEFAULT 'DROPOFF'::public."enum_Order_action" NOT NULL
);


ALTER TABLE public."Order" OWNER TO "Nabis";

--
-- Name: OrderAttachments; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."OrderAttachments" (
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "orderId" uuid NOT NULL,
    "attachmentId" uuid NOT NULL,
    id uuid NOT NULL
);


ALTER TABLE public."OrderAttachments" OWNER TO "Nabis";

--
-- Name: OrderChange; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."OrderChange" (
    id uuid NOT NULL,
    description character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "orderId" uuid,
    "submitterId" uuid,
    "organizationId" uuid
);


ALTER TABLE public."OrderChange" OWNER TO "Nabis";

--
-- Name: OrderSubscriber; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."OrderSubscriber" (
    id uuid NOT NULL,
    "manifestGDriveFileId" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "orderId" uuid,
    "subscriberId" uuid
);


ALTER TABLE public."OrderSubscriber" OWNER TO "Nabis";

--
-- Name: Organization; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Organization" (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    address1 character varying(255) NOT NULL,
    address2 character varying(255),
    city character varying(255),
    state character varying(255),
    zip character varying(255),
    phone character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "ownerId" uuid,
    alias character varying(255) NOT NULL,
    "manifestGDriveFolderId" character varying(255),
    "doingBusinessAs" character varying(255)
);


ALTER TABLE public."Organization" OWNER TO "Nabis";

--
-- Name: OrganizationInvite; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."OrganizationInvite" (
    id uuid NOT NULL,
    email character varying(255) NOT NULL,
    "organizationId" uuid NOT NULL,
    "senderId" uuid NOT NULL,
    accepted boolean,
    "respondedAt" timestamp with time zone,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."OrganizationInvite" OWNER TO "Nabis";

--
-- Name: OrganizationOrganization; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."OrganizationOrganization" (
    id uuid NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "superorganizationId" uuid,
    "suborganizationId" uuid
);


ALTER TABLE public."OrganizationOrganization" OWNER TO "Nabis";

--
-- Name: OrganizationUsers; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."OrganizationUsers" (
    id uuid NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "userId" uuid,
    "organizationId" uuid
);


ALTER TABLE public."OrganizationUsers" OWNER TO "Nabis";

--
-- Name: Role; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Role" (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    permissions text[] DEFAULT ARRAY[]::text[] NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "organizationId" uuid
);


ALTER TABLE public."Role" OWNER TO "Nabis";

--
-- Name: Ru; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Ru" (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    code character varying(255) NOT NULL,
    "isExciseTaxable" boolean DEFAULT true NOT NULL,
    "inventoryType" public."enum_Ru_inventoryType" DEFAULT 'CANNABIS'::public."enum_Ru_inventoryType" NOT NULL,
    "inventoryClass" public."enum_Ru_inventoryClass" DEFAULT 'FLOWER'::public."enum_Ru_inventoryClass" NOT NULL,
    "inventoryCategory" public."enum_Ru_inventoryCategory" DEFAULT 'BUD'::public."enum_Ru_inventoryCategory" NOT NULL,
    "pricePerUnit" double precision,
    unit character varying(255) DEFAULT 'unit'::character varying NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "organizationId" uuid
);


ALTER TABLE public."Ru" OWNER TO "Nabis";

--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO "Nabis";

--
-- Name: Site; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Site" (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    address1 character varying(255) NOT NULL,
    address2 character varying(255),
    city character varying(255) NOT NULL,
    state character varying(255) NOT NULL,
    zip character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "organizationId" uuid,
    "pocName" character varying(255),
    "pocPhoneNumber" character varying(255),
    "pocEmail" character varying(255),
    lat double precision,
    lng double precision,
    "siteCategory" public."enum_Site_siteCategory" DEFAULT 'RETAILER'::public."enum_Site_siteCategory" NOT NULL,
    "licensedLocationId" uuid,
    "adminNotes" character varying(255)
);


ALTER TABLE public."Site" OWNER TO "Nabis";

--
-- Name: Sku; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Sku" (
    id uuid NOT NULL,
    code character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "organizationId" uuid,
    "inventoryType" public."enum_Sku_inventoryType" DEFAULT 'CANNABIS'::public."enum_Sku_inventoryType" NOT NULL,
    "inventoryClass" public."enum_Sku_inventoryClass" DEFAULT 'FLOWER'::public."enum_Sku_inventoryClass" NOT NULL,
    "inventoryCategory" public."enum_Sku_inventoryCategory" DEFAULT 'BUD'::public."enum_Sku_inventoryCategory" NOT NULL,
    "batchId" uuid,
    "ruId" uuid,
    "isArchived" boolean
);


ALTER TABLE public."Sku" OWNER TO "Nabis";

--
-- Name: User; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."User" (
    id uuid NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(512) NOT NULL,
    "firstName" character varying(255),
    "lastName" character varying(255),
    address1 character varying(255),
    address2 character varying(255),
    city character varying(255),
    state character varying(255),
    zip character varying(255),
    phone character varying(255),
    "profilePicture" text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "isAdmin" boolean DEFAULT false NOT NULL
);


ALTER TABLE public."User" OWNER TO "Nabis";

--
-- Name: UserRoles; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."UserRoles" (
    id uuid NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "userId" uuid,
    "roleId" uuid
);


ALTER TABLE public."UserRoles" OWNER TO "Nabis";

--
-- Name: Vehicle; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Vehicle" (
    id uuid NOT NULL,
    "licensePlate" character varying(255) NOT NULL,
    name character varying(255),
    make character varying(255),
    model character varying(255),
    year integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Vehicle" OWNER TO "Nabis";

--
-- Name: Warehouse; Type: TABLE; Schema: public; Owner: Nabis
--

CREATE TABLE public."Warehouse" (
    id uuid NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "siteId" uuid,
    "licenseId" uuid
);


ALTER TABLE public."Warehouse" OWNER TO "Nabis";

--
-- Name: Attachment Attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Attachment"
    ADD CONSTRAINT "Attachment_pkey" PRIMARY KEY (id);


--
-- Name: Batch Batch_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Batch"
    ADD CONSTRAINT "Batch_pkey" PRIMARY KEY (id);


--
-- Name: Draft Draft_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Draft"
    ADD CONSTRAINT "Draft_pkey" PRIMARY KEY (id);


--
-- Name: Driver Driver_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Driver"
    ADD CONSTRAINT "Driver_pkey" PRIMARY KEY (id);


--
-- Name: InventoryItem InventoryItem_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."InventoryItem"
    ADD CONSTRAINT "InventoryItem_pkey" PRIMARY KEY (id);


--
-- Name: InventoryLedgerItem InventoryLedgerItem_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."InventoryLedgerItem"
    ADD CONSTRAINT "InventoryLedgerItem_pkey" PRIMARY KEY (id);


--
-- Name: License License_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."License"
    ADD CONSTRAINT "License_pkey" PRIMARY KEY (id);


--
-- Name: LicensedLocation LicensedLocation_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."LicensedLocation"
    ADD CONSTRAINT "LicensedLocation_pkey" PRIMARY KEY (id);


--
-- Name: LineItemChange LineItemChange_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."LineItemChange"
    ADD CONSTRAINT "LineItemChange_pkey" PRIMARY KEY (id);


--
-- Name: LineItem LineItem_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."LineItem"
    ADD CONSTRAINT "LineItem_pkey" PRIMARY KEY (id);


--
-- Name: OldSku OldSku_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OldSku"
    ADD CONSTRAINT "OldSku_pkey" PRIMARY KEY (id);


--
-- Name: OrderAttachments OrderAttachments_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderAttachments"
    ADD CONSTRAINT "OrderAttachments_pkey" PRIMARY KEY (id);


--
-- Name: OrderChange OrderChange_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderChange"
    ADD CONSTRAINT "OrderChange_pkey" PRIMARY KEY (id);


--
-- Name: OrderSubscriber OrderSubscriber_orderId_subscriberId_key; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderSubscriber"
    ADD CONSTRAINT "OrderSubscriber_orderId_subscriberId_key" UNIQUE ("orderId", "subscriberId");


--
-- Name: OrderSubscriber OrderSubscriber_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderSubscriber"
    ADD CONSTRAINT "OrderSubscriber_pkey" PRIMARY KEY (id);


--
-- Name: Order Order_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_pkey" PRIMARY KEY (id);


--
-- Name: OrganizationInvite OrganizationInvite_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationInvite"
    ADD CONSTRAINT "OrganizationInvite_pkey" PRIMARY KEY (id);


--
-- Name: OrganizationOrganization OrganizationOrganization_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationOrganization"
    ADD CONSTRAINT "OrganizationOrganization_pkey" PRIMARY KEY (id);


--
-- Name: OrganizationOrganization OrganizationOrganization_superorganizationId_suborganizatio_key; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationOrganization"
    ADD CONSTRAINT "OrganizationOrganization_superorganizationId_suborganizatio_key" UNIQUE ("superorganizationId", "suborganizationId");


--
-- Name: OrganizationUsers OrganizationUsers_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationUsers"
    ADD CONSTRAINT "OrganizationUsers_pkey" PRIMARY KEY (id);


--
-- Name: OrganizationUsers OrganizationUsers_userId_organizationId_key; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationUsers"
    ADD CONSTRAINT "OrganizationUsers_userId_organizationId_key" UNIQUE ("userId", "organizationId");


--
-- Name: Organization Organization_alias_key; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Organization"
    ADD CONSTRAINT "Organization_alias_key" UNIQUE (alias);


--
-- Name: Organization Organization_name_key; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Organization"
    ADD CONSTRAINT "Organization_name_key" UNIQUE (name);


--
-- Name: Organization Organization_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Organization"
    ADD CONSTRAINT "Organization_pkey" PRIMARY KEY (id);


--
-- Name: Role Role_name_key; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Role"
    ADD CONSTRAINT "Role_name_key" UNIQUE (name);


--
-- Name: Role Role_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Role"
    ADD CONSTRAINT "Role_pkey" PRIMARY KEY (id);


--
-- Name: Ru Ru_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Ru"
    ADD CONSTRAINT "Ru_pkey" PRIMARY KEY (id);


--
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: Site Site_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Site"
    ADD CONSTRAINT "Site_pkey" PRIMARY KEY (id);


--
-- Name: Sku Sku_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Sku"
    ADD CONSTRAINT "Sku_pkey" PRIMARY KEY (id);


--
-- Name: UserRoles UserRoles_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."UserRoles"
    ADD CONSTRAINT "UserRoles_pkey" PRIMARY KEY (id);


--
-- Name: UserRoles UserRoles_userId_roleId_key; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."UserRoles"
    ADD CONSTRAINT "UserRoles_userId_roleId_key" UNIQUE ("userId", "roleId");


--
-- Name: User User_email_key; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_email_key" UNIQUE (email);


--
-- Name: User User_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (id);


--
-- Name: Vehicle Vehicle_licensePlate_key; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Vehicle"
    ADD CONSTRAINT "Vehicle_licensePlate_key" UNIQUE ("licensePlate");


--
-- Name: Vehicle Vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Vehicle"
    ADD CONSTRAINT "Vehicle_pkey" PRIMARY KEY (id);


--
-- Name: Warehouse Warehouse_pkey; Type: CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Warehouse"
    ADD CONSTRAINT "Warehouse_pkey" PRIMARY KEY (id);


--
-- Name: Attachment Attachment_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Attachment"
    ADD CONSTRAINT "Attachment_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Batch Batch_licenseId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Batch"
    ADD CONSTRAINT "Batch_licenseId_fkey" FOREIGN KEY ("licenseId") REFERENCES public."License"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Batch Batch_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Batch"
    ADD CONSTRAINT "Batch_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Draft Draft_creatorId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Draft"
    ADD CONSTRAINT "Draft_creatorId_fkey" FOREIGN KEY ("creatorId") REFERENCES public."User"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: InventoryItem InventoryItem_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."InventoryItem"
    ADD CONSTRAINT "InventoryItem_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: InventoryItem InventoryItem_skuId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."InventoryItem"
    ADD CONSTRAINT "InventoryItem_skuId_fkey" FOREIGN KEY ("skuId") REFERENCES public."Sku"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: InventoryItem InventoryItem_warehouseId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."InventoryItem"
    ADD CONSTRAINT "InventoryItem_warehouseId_fkey" FOREIGN KEY ("warehouseId") REFERENCES public."Warehouse"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: InventoryLedgerItem InventoryLedgerItem_creatorId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."InventoryLedgerItem"
    ADD CONSTRAINT "InventoryLedgerItem_creatorId_fkey" FOREIGN KEY ("creatorId") REFERENCES public."User"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: InventoryLedgerItem InventoryLedgerItem_inventoryItemId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."InventoryLedgerItem"
    ADD CONSTRAINT "InventoryLedgerItem_inventoryItemId_fkey" FOREIGN KEY ("inventoryItemId") REFERENCES public."InventoryItem"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: InventoryLedgerItem InventoryLedgerItem_orderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."InventoryLedgerItem"
    ADD CONSTRAINT "InventoryLedgerItem_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES public."Order"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: InventoryLedgerItem InventoryLedgerItem_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."InventoryLedgerItem"
    ADD CONSTRAINT "InventoryLedgerItem_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: License License_licensedLocationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."License"
    ADD CONSTRAINT "License_licensedLocationId_fkey" FOREIGN KEY ("licensedLocationId") REFERENCES public."LicensedLocation"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: License License_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."License"
    ADD CONSTRAINT "License_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: License License_siteId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."License"
    ADD CONSTRAINT "License_siteId_fkey" FOREIGN KEY ("siteId") REFERENCES public."Site"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: LineItemChange LineItemChange_orderChangeId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."LineItemChange"
    ADD CONSTRAINT "LineItemChange_orderChangeId_fkey" FOREIGN KEY ("orderChangeId") REFERENCES public."OrderChange"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: LineItem LineItem_orderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."LineItem"
    ADD CONSTRAINT "LineItem_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES public."Order"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: LineItem LineItem_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."LineItem"
    ADD CONSTRAINT "LineItem_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: LineItem LineItem_skuId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."LineItem"
    ADD CONSTRAINT "LineItem_skuId_fkey" FOREIGN KEY ("skuId") REFERENCES public."Sku"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: OrderAttachments OrderAttachments_attachmentId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderAttachments"
    ADD CONSTRAINT "OrderAttachments_attachmentId_fkey" FOREIGN KEY ("attachmentId") REFERENCES public."Attachment"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: OrderAttachments OrderAttachments_orderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderAttachments"
    ADD CONSTRAINT "OrderAttachments_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES public."Order"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: OrderChange OrderChange_orderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderChange"
    ADD CONSTRAINT "OrderChange_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES public."Order"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: OrderChange OrderChange_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderChange"
    ADD CONSTRAINT "OrderChange_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: OrderChange OrderChange_submitterId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderChange"
    ADD CONSTRAINT "OrderChange_submitterId_fkey" FOREIGN KEY ("submitterId") REFERENCES public."User"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: OrderSubscriber OrderSubscriber_orderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderSubscriber"
    ADD CONSTRAINT "OrderSubscriber_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES public."Order"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: OrderSubscriber OrderSubscriber_subscriberId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrderSubscriber"
    ADD CONSTRAINT "OrderSubscriber_subscriberId_fkey" FOREIGN KEY ("subscriberId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Order Order_collectWithOrderId; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_collectWithOrderId" FOREIGN KEY ("collectWithOrderId") REFERENCES public."Order"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Order Order_creatorId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_creatorId_fkey" FOREIGN KEY ("creatorId") REFERENCES public."User"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Order Order_driverId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_driverId_fkey" FOREIGN KEY ("driverId") REFERENCES public."Driver"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Order Order_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Order Order_siteId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_siteId_fkey" FOREIGN KEY ("siteId") REFERENCES public."Site"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Order Order_vehicleId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_vehicleId_fkey" FOREIGN KEY ("vehicleId") REFERENCES public."Vehicle"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Order Order_warehouseId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_warehouseId_fkey" FOREIGN KEY ("warehouseId") REFERENCES public."Warehouse"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: OrganizationInvite OrganizationInvite_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationInvite"
    ADD CONSTRAINT "OrganizationInvite_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: OrganizationInvite OrganizationInvite_senderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationInvite"
    ADD CONSTRAINT "OrganizationInvite_senderId_fkey" FOREIGN KEY ("senderId") REFERENCES public."User"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: OrganizationOrganization OrganizationOrganization_suborganizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationOrganization"
    ADD CONSTRAINT "OrganizationOrganization_suborganizationId_fkey" FOREIGN KEY ("suborganizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: OrganizationOrganization OrganizationOrganization_superorganizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationOrganization"
    ADD CONSTRAINT "OrganizationOrganization_superorganizationId_fkey" FOREIGN KEY ("superorganizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: OrganizationUsers OrganizationUsers_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationUsers"
    ADD CONSTRAINT "OrganizationUsers_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: OrganizationUsers OrganizationUsers_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."OrganizationUsers"
    ADD CONSTRAINT "OrganizationUsers_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."User"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Organization Organization_ownerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Organization"
    ADD CONSTRAINT "Organization_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES public."User"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Role Role_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Role"
    ADD CONSTRAINT "Role_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Ru Ru_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Ru"
    ADD CONSTRAINT "Ru_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Site Site_licensedLocationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Site"
    ADD CONSTRAINT "Site_licensedLocationId_fkey" FOREIGN KEY ("licensedLocationId") REFERENCES public."LicensedLocation"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Site Site_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Site"
    ADD CONSTRAINT "Site_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Sku Sku_organizationId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Sku"
    ADD CONSTRAINT "Sku_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Organization"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Sku Sku_ruId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Sku"
    ADD CONSTRAINT "Sku_ruId_fkey" FOREIGN KEY ("ruId") REFERENCES public."Ru"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: UserRoles UserRoles_roleId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."UserRoles"
    ADD CONSTRAINT "UserRoles_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES public."Role"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: UserRoles UserRoles_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."UserRoles"
    ADD CONSTRAINT "UserRoles_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."User"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Warehouse Warehouse_licenseId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Warehouse"
    ADD CONSTRAINT "Warehouse_licenseId_fkey" FOREIGN KEY ("licenseId") REFERENCES public."License"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Warehouse Warehouse_siteId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: Nabis
--

ALTER TABLE ONLY public."Warehouse"
    ADD CONSTRAINT "Warehouse_siteId_fkey" FOREIGN KEY ("siteId") REFERENCES public."Site"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--
