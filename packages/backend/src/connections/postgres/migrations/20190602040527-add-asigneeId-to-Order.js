module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('Order', 'assigneeId', Sequelize.UUID, {
        defaultValue: null,
      })
      .then(() => {
        return queryInterface.addConstraint('Order', ['assigneeId'], {
          type: 'FOREIGN KEY',
          name: 'Order_assigneeId_fkey', // useful if using queryInterface.removeConstraint
          references: {
            table: 'User',
            field: 'id',
          },
          onDelete: 'set null',
          onUpdate: 'cascade',
        })
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('Order', 'Order_assigneeId_fkey').then(() => {
      return queryInterface.removeColumn('Order', 'assigneeId')
    })
  },
}
