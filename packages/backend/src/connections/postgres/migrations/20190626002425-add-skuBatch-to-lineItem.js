module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface
      .addColumn('LineItem', 'skuBatchId', Sequelize.UUID, {
        defaultValue: null,
      })
      .then(() =>
        queryInterface.addConstraint('LineItem', ['skuBatchId'], {
          type: 'FOREIGN KEY',
          name: 'LineItem_skuBatchId_fkey',
          references: {
            table: 'SkuBatch',
            field: 'id',
          },
          onDelete: 'set null',
          onUpdate: 'cascade',
        }),
      ),

  down: (queryInterface) =>
    queryInterface
      .removeConstraint('LineItem', 'LineItem_skuBatchId_fkey')
      .then(() => queryInterface.removeColumn('LineItem', 'skuBatchId')),
}
