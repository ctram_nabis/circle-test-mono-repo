module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize
      .transaction((t) =>
        Promise.all([
          queryInterface.addColumn('Sku', 'isExciseTaxable', Sequelize.BOOLEAN, {
            transaction: t,
            defaultValue: true,
          }),
          queryInterface.addColumn('Sku', 'unit', Sequelize.STRING, {
            transaction: t,
            defaultValue: '',
          }),
          queryInterface.addColumn('Sku', 'pricePerUnit', Sequelize.DOUBLE, {
            transaction: t,
            defaultValue: 0.0,
          }),
          queryInterface.addColumn('Sku', 'name', Sequelize.STRING, {
            transaction: t,
            allowNull: false,
          }),
        ]),
      )
      .then(() =>
        queryInterface.sequelize.query(`
    update "Sku" as s set
      name = c.name,
      unit = c.unit,
      "pricePerUnit" = c."pricePerUnit",
      "isExciseTaxable" = c."isExciseTaxable"
    from (
      select
        k.id as sku,
        r.name as name,
        r.unit as unit,
        r."pricePerUnit" as "pricePerUnit",
        r."isExciseTaxable" as "isExciseTaxable"
      from "Ru" r join "Sku" k
      on k."ruId" = r.id
    ) as c(sku, name, unit, "pricePerUnit", "isExciseTaxable")
    where c.sku = s.id;
  `),
      )
      .then(() =>
        queryInterface.sequelize.transaction((t) =>
          Promise.all([
            queryInterface.removeConstraint('Sku', 'Sku_ruId_fkey', {
              transaction: t,
            }),
            queryInterface.removeColumn('Sku', 'ruId', { transaction: t }),
          ]),
        ),
      )
      .then(() =>
        queryInterface.sequelize
          .query(
            `
    select count(*) from "Sku" where true;
  `,
          )
          .then((res) => {
            console.log('Skus updated: ', res[0])
            return res
          }),
      ),

  down: (queryInterface, Sequelize) =>
    queryInterface.sequelize
      .transaction((t) =>
        Promise.all([
          queryInterface.removeColumn('Sku', 'name', { transaction: t }),
          queryInterface.removeColumn('Sku', 'unit', { transaction: t }),
          queryInterface.removeColumn('Sku', 'pricePerUnit', {
            transaction: t,
          }),
          queryInterface.removeColumn('Sku', 'isExciseTaxable', {
            transaction: t,
          }),
        ]),
      )
      .then(() =>
        queryInterface
          .addColumn('Sku', 'ruId', Sequelize.UUID, {
            defaultValue: null,
          })
          .then(() =>
            queryInterface.addConstraint('Sku', ['ruId'], {
              type: 'FOREIGN KEY',
              name: 'Sku_ruId_fkey', // useful if using queryInterface.removeConstraint
              references: {
                table: 'Ru',
                field: 'id',
              },
              onDelete: 'set null',
              onUpdate: 'cascade',
            }),
          ),
      )
      .then(() =>
        queryInterface.sequelize
          .query(
            `
    select count(*) from "Sku" where true;
  `,
          )
          .then((res) => {
            console.log('Skus updated: ', res[0])
            return res
          }),
      ),
}
