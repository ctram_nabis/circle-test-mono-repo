module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize.query(`
      -- Perform addition
      BEGIN;
      UPDATE
        "InventoryItem"
      SET
        "counted" = ii.addii + ii.addsource
      FROM (
        SELECT
          i2.id AS targetii,
          i1.counted AS addii,
          i2.counted AS addsource
        FROM
          "InventoryItem" AS i1
          JOIN "InventoryItem" AS i2 ON i2. "skuId" = i1. "skuId"
          AND i1.id != i2.id
        WHERE
          i1. "batchId" IS NULL
          AND i2. "batchId" IS NOT NULL
          AND i1.counted > 0) ii
      WHERE
        "InventoryItem".id = ii.targetii;

      -- Zero out i1
      UPDATE
        "InventoryItem"
      SET
        "counted" = 0
      FROM (
        SELECT
          i1.id AS targetii,
          i1.counted AS addii,
          i2.counted AS addsource
        FROM
          "InventoryItem" AS i1
          JOIN "InventoryItem" AS i2 ON i2. "skuId" = i1. "skuId"
          AND i1.id != i2.id
        WHERE
          i1. "batchId" IS NULL
          AND i2. "batchId" IS NOT NULL
          AND i1.counted > 0) ii
      WHERE
        "InventoryItem".id = ii.targetii;

      UPDATE
        "LineItem"
      SET
        ("batchId", "skuBatchId") = ("InventoryItem"."batchId", "InventoryItem"."skuBatchId")
      FROM
        "InventoryItem"
      WHERE
        "InventoryItem"."skuId" = "LineItem"."skuId"
        AND "InventoryItem"."batchId" IS NOT NULL
        AND "InventoryItem"."skuBatchId" IS NOT NULL
        AND "LineItem"."batchId" IS NULL;


      DELETE FROM "InventoryItem" WHERE id in (SELECT "InventoryItem".id as ii_id
      FROM "Sku"
      JOIN "InventoryItem" on "InventoryItem"."skuId" = "Sku".id
      WHERE NOT EXISTS (
        SELECT "skuId"
        FROM "SkuBatch"
        where "SkuBatch"."skuId" = "Sku".id
        )
      );
      COMMIT;
    `),

  down: () => Promise.resolve(),
}
