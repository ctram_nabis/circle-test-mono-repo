module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      `
        --Update all emails with trailing/preceding whitespace
        UPDATE "User"
        SET email = TRIM(email)
        WHERE email != TRIM(email);

        UPDATE "OrganizationInvite"
        SET email = TRIM(email)
        WHERE email != TRIM(email);
      `,
    )
  },

  down: (queryInterface, Sequelize) => {
    return Promise.resolve()
  },
}
