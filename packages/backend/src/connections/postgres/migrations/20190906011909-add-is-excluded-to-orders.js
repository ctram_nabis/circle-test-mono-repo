module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      `
        ALTER TABLE "Order" ADD COLUMN "isExcluded" bool NOT NULL DEFAULT false;
        UPDATE "Order" SET "isExcluded" = true WHERE "Order".date < timestamp '2019-09-01 00:00:00';
      `,
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`ALTER TABLE "Order" DROP COLUMN "isExcluded";`)
  },
}
