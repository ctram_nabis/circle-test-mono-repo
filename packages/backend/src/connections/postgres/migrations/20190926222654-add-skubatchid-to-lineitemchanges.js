'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('LineItemChange', 'skuBatchId', {
      type: Sequelize.UUID,
    }),
  down: (queryInterface, _Sequelize) => queryInterface.removeColumn('LineItemChange', 'skuBatchId'),
}
