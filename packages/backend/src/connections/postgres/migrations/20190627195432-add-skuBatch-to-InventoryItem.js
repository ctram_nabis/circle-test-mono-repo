module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface
      .addColumn('InventoryItem', 'skuBatchId', Sequelize.UUID, {
        defaultValue: null,
      })
      .then(() =>
        queryInterface.addColumn('InventoryItem', 'batchId', Sequelize.UUID, {
          defaultValue: null,
        }),
      )
      .then(() =>
        queryInterface.addConstraint('InventoryItem', ['batchId'], {
          type: 'FOREIGN KEY',
          name: 'InventoryItem_batchId_fkey',
          references: {
            table: 'Batch',
            field: 'id',
          },
          onDelete: 'set null',
          onUpdate: 'cascade',
        }),
      )
      .then(() =>
        queryInterface.addConstraint('InventoryItem', ['skuBatchId'], {
          type: 'FOREIGN KEY',
          name: 'InventoryItem_skuBatchId_fkey',
          references: {
            table: 'SkuBatch',
            field: 'id',
          },
          onDelete: 'set null',
          onUpdate: 'cascade',
        }),
      ),

  down: (queryInterface) =>
    queryInterface
      .removeConstraint('InventoryItem', 'InventoryItem_skuBatchId_fkey')
      .then(() => queryInterface.removeColumn('InventoryItem', 'skuBatchId'))
      .then(() => queryInterface.removeConstraint('InventoryItem', 'InventoryItem_batchId_fkey'))
      .then(() => queryInterface.removeColumn('InventoryItem', 'batchId')),
}
