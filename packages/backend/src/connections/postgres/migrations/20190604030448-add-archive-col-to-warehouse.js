module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('Warehouse', 'isArchived', Sequelize.BOOLEAN, {
        defaultValue: false,
      })
      .then(() => {
        return queryInterface.sequelize.query(`
          update "Warehouse" set "isArchived" = false;
        `)
      })
      .then(() => {
        return queryInterface.sequelize.query(`
          update "Warehouse" set "isArchived" = true where id='6ceb0232-c127-11e8-a355-529269fb1459';
        `)
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Warehouse', 'isArchived')
  },
}
