module.exports = {
  up: (queryInterface, Sequelize) => {
    const promise = queryInterface.addColumn('SkuBatch', 'isLocked', {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    })

    return promise
  },

  down: (queryInterface, Sequelize) => {
    const promise = queryInterface.removeColumn('SkuBatch', 'isLocked')
    return promise
  },
}
