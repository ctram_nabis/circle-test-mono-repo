'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable(
      'UserPreference',
      {
        id: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
          allowNull: false,
        },
        receiveOrderUpdateEmails: {
          defaultValue: true,
          type: Sequelize.BOOLEAN,
          allowNull: false,
        },
        organizationId: {
          type: Sequelize.UUID,
          allowNull: false,
        },
        userId: {
          type: Sequelize.UUID,
          allowNull: false,
        },
        createdAt: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('NOW'),
        },
        updatedAt: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('NOW'),
        },
        deletedAt: {
          type: Sequelize.DATE,
          defaultValue: null,
        },
      },
      { freezeTableName: true, paranoid: true },
    ),

  down: (queryInterface, _Sequelize) => queryInterface.dropTable('UserPreference'),
}
