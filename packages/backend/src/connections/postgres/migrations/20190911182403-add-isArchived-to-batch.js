'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('Batch', 'isArchived', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    }),

  down: (queryInterface, _Sequelize) => queryInterface.removeColumnt('Batch', 'isArchived'),
}
