module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface
      .addColumn('Draft', 'organizationId', Sequelize.UUID, {
        defaultValue: null,
      })
      .then(() =>
        queryInterface.addConstraint('Draft', ['organizationId'], {
          type: 'FOREIGN KEY',
          name: 'Draft_organizationId_fkey', // useful if using queryInterface.removeConstraint
          references: {
            table: 'Organization',
            field: 'id',
          },
          onDelete: 'set null',
          onUpdate: 'cascade',
        }),
      ),

  down: (queryInterface) =>
    queryInterface
      .removeConstraint('Draft', 'Draft_organizationId_fkey')
      .then(() => queryInterface.removeColumn('Draft', 'organizationId')),
}
