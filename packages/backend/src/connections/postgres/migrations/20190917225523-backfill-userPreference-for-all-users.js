'use strict'

module.exports = {
  up: (queryInterface, _Sequelize) =>
    queryInterface.sequelize.query(`
      WITH ids AS (
        SELECT
          "User".id AS usr_id,
          "Organization".id AS organization_id
        FROM
          "User"
          JOIN "OrganizationUser" ON "OrganizationUser"."userId" = "User".id
          JOIN "Organization" ON "Organization".id = "OrganizationUser"."organizationId"
        )
      INSERT INTO "UserPreference" (id, "receiveOrderUpdateEmails", "organizationId", "userId")
      SELECT
        uuid_generate_v4 (),
        TRUE,
        organization_id,
        usr_id
      FROM
        ids;
    `),

  down: (queryInterface, _Sequelize) => queryInterface.sequelize.query(),
}
