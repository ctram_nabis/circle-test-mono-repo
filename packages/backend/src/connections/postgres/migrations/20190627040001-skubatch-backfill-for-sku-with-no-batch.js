module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize
      .query(
        `
    insert into "SkuBatch"
      select
        uuid_generate_v4() AS "id",
        s.id AS "skuId",
        null,
        s."createdAt",
        s."updatedAt"
      from "Sku" s where not exists (
        select * from "SkuBatch" sb
          where sb."skuId" = s.id
      );
  `,
      )
      .then(() =>
        queryInterface.sequelize
          .query(
            `
    select count(*) from "SkuBatch"
  `,
          )
          .then((res) => {
            console.log('Total SkuBatches: ', res[0])
            return res
          }),
      ),

  down: (queryInterface, Sequelize) =>
    queryInterface.sequelize
      .query(
        `
    select count(*) from "SkuBatch"
  `,
      )
      .then((res) => {
        console.log('Total SkuBatches: ', res[0])
        return res
      }),
}
