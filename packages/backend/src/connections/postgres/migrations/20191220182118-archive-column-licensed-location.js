'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('LicensedLocation', 'isArchived', Sequelize.BOOLEAN, {
        defaultValue: false,
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        update "LicensedLocation" set "isArchived" = false;
      `)
      })
      .then(() => {
        return queryInterface.addColumn('LicensedLocation', 'deletedAt', {
          type: Sequelize.DATE,
          allowNull: true,
          defaultValue: null,
        })
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('LicensedLocation', 'isArchived').then(() => {
      return queryInterface.removeColumn('LicensedLocation', 'deletedAt')
    })
  },
}
