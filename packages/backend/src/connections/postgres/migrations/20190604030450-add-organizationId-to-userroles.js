module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface
      .addColumn('UserRoles', 'organizationId', Sequelize.UUID, {
        defaultValue: null,
      })
      .then(() =>
        queryInterface.addConstraint('UserRoles', ['organizationId'], {
          type: 'FOREIGN KEY',
          name: 'UserRoles_organizationId_fkey', // useful if using queryInterface.removeConstraint
          references: {
            table: 'Organization',
            field: 'id',
          },
          onDelete: 'set null',
          onUpdate: 'cascade',
        }),
      ),

  down: (queryInterface) =>
    queryInterface
      .removeConstraint('UserRoles', 'UserRoles_organizationId_fkey')
      .then(() => queryInterface.removeColumn('UserRoles', 'organizationId')),
}
