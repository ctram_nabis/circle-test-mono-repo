'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Order', 'internalNotes', Sequelize.TEXT)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Order', 'internalNotes')
  },
}
