module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize
      .query(
        `
    insert into "SkuBatch"
      select
        uuid_generate_v4() AS "id",
        s.id AS "skuId",
        b.id AS "batchId",
        b."createdAt",
        b."updatedAt"
      from "Batch" b JOIN "Sku" s
        on b."skuId" = s.id and not exists (
          select * from "SkuBatch" sb
            where sb."skuId" = s.id
            and b.id = sb."batchId"
        );
  `,
      )
      .then(() =>
        queryInterface.sequelize
          .query(
            `
    select count(*) from "SkuBatch"
  `,
          )
          .then((res) => {
            console.log('Total SkuBatches: ', res[0])
            return res
          }),
      ),

  down: (queryInterface, Sequelize) =>
    queryInterface.sequelize
      .query(
        `
    select count(*) from "SkuBatch"
  `,
      )
      .then((res) => {
        console.log('Total SkuBatches: ', res[0])
        return res
      }),
}
