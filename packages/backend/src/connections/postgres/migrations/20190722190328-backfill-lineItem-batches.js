module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize.query(`
    UPDATE
      "LineItem" AS li
    SET
      ("batchId", "skuBatchId") = ("SkuBatch"."batchId", "SkuBatch".id)
    FROM
      "SkuBatch"
    WHERE
      "SkuBatch"."skuId" = li. "skuId"
      AND li. "batchId" IS NULL
      AND li. "skuBatchId" IS NULL
      AND EXISTS (
        SELECT
          count(*)
        FROM
          "SkuBatch"
        WHERE
          "SkuBatch"."skuId" = li. "skuId"
        GROUP BY
          "SkuBatch"."skuId"
        HAVING
          count(*) = 1);
    `),

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  },
}
