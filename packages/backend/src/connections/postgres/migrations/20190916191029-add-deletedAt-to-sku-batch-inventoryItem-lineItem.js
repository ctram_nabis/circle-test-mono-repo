module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('Sku', 'deletedAt', {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null,
      })
      .then(() => {
        return queryInterface.addColumn('InventoryItem', 'deletedAt', {
          type: Sequelize.DATE,
          allowNull: true,
          defaultValue: null,
        })
      })
      .then(() => {
        return queryInterface.addColumn('LineItem', 'deletedAt', {
          type: Sequelize.DATE,
          allowNull: true,
          defaultValue: null,
        })
      })
  },

  down: () => {
    Promise.resolve()
  },
}
