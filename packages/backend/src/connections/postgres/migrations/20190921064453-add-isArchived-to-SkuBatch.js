'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('SkuBatch', 'isArchived', {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      })
      .then(() => {
        return queryInterface.sequelize.query(`
          UPDATE
              "SkuBatch"
          SET
              "isArchived" = TRUE
          FROM ( SELECT DISTINCT
                  id
              FROM
                  "SkuBatch"
              WHERE
                  "deletedAt" IS NOT NULL) sb
          WHERE
              "SkuBatch".id = sb.id;
        `)
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('SkuBatch', 'isArchived')
  },
}
