// II = InventoryItems, ii = inventory item, SB = sku batch
const array = require('../seeders/SkuBatch')

const queryIIFindAll = (queryInterface, offset, limit) => {
  const { SELECT } = queryInterface.sequelize.QueryTypes
  const queryString = `SELECT id, "skuId" from "InventoryItem" OFFSET ${offset} LIMIT ${limit}`
  return queryInterface.sequelize.query(queryString, { type: SELECT })
}

const queryUpdateII = (queryInterface, iiId, skuBatchId, batchId) => {
  let queryString = `UPDATE "InventoryItem" SET "skuBatchId" = '${skuBatchId}'`
  if (batchId) queryString += `, "batchId" = '${batchId}'`
  queryString += ` where ("id" = '${iiId}');`

  //console.log(queryString);
  return queryInterface.sequelize.query(queryString)
}

const querySBFindOne = (queryInterface, skuId, batchId) => {
  const { SELECT } = queryInterface.sequelize.QueryTypes
  let queryString = `SELECT id from "SkuBatch" where "skuId" = '${skuId}'`

  if (batchId) queryString += ` and "batchId" = '${batchId}'`
  queryString += ' LIMIT 1;'

  //console.log(queryString);
  return queryInterface.sequelize.query(queryString, { type: SELECT }).then((result) => result[0])
}

const queryUpdateIIwithSB = (queryInterface, ii, a) => {
  return querySBFindOne(queryInterface, a[0], a.length > 1 ? a[1] : null).then((skuBatch) => {
    if (!skuBatch) return null
    //console.log(`updating II ${ii.id} to have skuBatchId - ${skuBatch.id} | sku id: ${ii.skuId}`);
    return queryUpdateII(queryInterface, ii.id, skuBatch.id, a.length > 1 ? a[1] : null)
  })
}

module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize.transaction((tx) => {
      const limit = 20
      let promises = []
      let currentIdx = 0

      return (function nextIteration() {
        console.log('---------------------------')
        console.log(`Iteration: ${currentIdx / limit}`)
        console.log()

        let II = []

        return queryIIFindAll(queryInterface, currentIdx, limit)
          .then((_II) => {
            console.log(`Found ${_II.length} Inventory Items`)
            II = _II

            promises = II.reduce((s, ii) => {
              const pairs = array.filter((a) => ii.skuId === a[0])
              const toConcat = pairs.map((a) => queryUpdateIIwithSB(queryInterface, ii, a))
              return s.concat(toConcat)
            }, [])
            return Promise.all(promises)
          })
          .then(() => {
            if (II.length === limit) {
              currentIdx += II.length
              return nextIteration()
            }
            return null
          })
      })()
    }),

  down: (queryInterface) =>
    queryInterface.sequelize.query(`
    select count(*) from "InventoryItem";
  `),
}
