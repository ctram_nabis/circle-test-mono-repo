module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('SkuBatch', 'deletedAt', Sequelize.DATE),

  down: (queryInterface) => queryInterface.removeColumn('SkuBatch', 'deletedAt'),
}
