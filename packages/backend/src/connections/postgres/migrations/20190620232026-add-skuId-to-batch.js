module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface
      .addColumn('Batch', 'skuId', Sequelize.UUID, {
        defaultValue: null,
      })
      .then(() =>
        queryInterface.addConstraint('Batch', ['skuId'], {
          type: 'FOREIGN KEY',
          name: 'Batch_skuId_fkey',
          references: {
            table: 'Sku',
            field: 'id',
          },
          onDelete: 'set null',
          onUpdate: 'cascade',
        }),
      )
      .then(() =>
        queryInterface.sequelize.query(`
    update "Batch" as b set
      "skuId" = c.sku
    from (
      select
        s.id as sku,
        a.id as batch
      from "Batch" a join "Sku" s
      on s."batchId" = a.id
    ) as c(sku, batch)
    where c.batch = b.id;
  `),
      ),

  down: (queryInterface) =>
    queryInterface
      .removeConstraint('Batch', 'Batch_skuId_fkey')
      .then(() => queryInterface.removeColumn('Batch', 'skuId')),
}
