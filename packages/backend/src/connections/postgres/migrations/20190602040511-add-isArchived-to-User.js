module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      -- Add archived to User for "user deletion"
      ALTER TABLE public."User"
        ADD COLUMN "isArchived" BOOLEAN DEFAULT FALSE;
    */
    return queryInterface
      .addColumn('User', 'isArchived', Sequelize.BOOLEAN, {
        defaultValue: false,
      })
      .then(() => {
        return queryInterface.sequelize.query(`
        update "User" set "isArchived" = false;
      `)
      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('User', 'isArchived')
  },
}
