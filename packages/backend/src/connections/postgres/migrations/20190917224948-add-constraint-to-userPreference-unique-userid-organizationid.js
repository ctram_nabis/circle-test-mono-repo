'use strict'

module.exports = {
  up: (queryInterface, _Sequelize) =>
    queryInterface.sequelize.query(`
      ALTER TABLE "UserPreference" ADD CONSTRAINT "UserPreference_organizationId_userId_key" UNIQUE ("organizationId", "userId");
    `),

  down: (queryInterface, _Sequelize) =>
    queryInterface.sequelize.query(`
      ALTER TABLE "UserPreference" DROP CONSTRAINT "UserPreference_organizationId_userId_key";
    `),
}
