module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize
      .transaction((t) =>
        Promise.all([
          queryInterface.addColumn('Batch', 'status', Sequelize.ENUM('QUARANTINE', 'SAMPLED', 'PASSED', 'FAILED'), {
            defaultValue: 'QUARANTINE',
            allowNull: false,
            transaction: t,
          }),
          queryInterface.addColumn('Batch', 'testDate', Sequelize.DATE, {
            transaction: t,
          }),
          queryInterface.addColumn('Batch', 'deletedAt', Sequelize.DATE, {
            transaction: t,
          }),
        ]),
      )
      .then(() =>
        queryInterface.sequelize
          .query(
            `
    select count(*) from "Batch" where true;
  `,
          )
          .then((res) => {
            console.log('Batches updated: ', res[0])
            return res
          }),
      ),

  down: (queryInterface) =>
    queryInterface.sequelize
      .transaction((t) =>
        Promise.all([
          queryInterface.removeColumn('Batch', 'status', { transaction: t }),
          queryInterface.removeColumn('Batch', 'testDate', { transaction: t }),
          queryInterface.removeColumn('Batch', 'deletedAt', { transaction: t }),
        ]),
      )
      .then(() =>
        queryInterface.sequelize
          .query(
            `
    select count(*) from "Batch' where true;
  `,
          )
          .then((res) => {
            console.log('Batches updated: ', res[0])
            return res
          }),
      ),
}
