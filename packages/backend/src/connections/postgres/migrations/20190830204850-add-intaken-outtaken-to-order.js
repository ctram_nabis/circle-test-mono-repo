'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize.transaction((t) =>
      Promise.all([
        queryInterface.addColumn('Order', 'intaken', {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          transaction: t,
        }),
        queryInterface.addColumn('Order', 'outtaken', {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          transaction: t,
        }),
      ]),
    ),

  down: (queryInterface, _Sequelize) =>
    queryInterface.sequelize.transaction((t) =>
      Promise.all([
        queryInterface.removeColumn('Order', 'intaken', { transaction: t }),
        queryInterface.removeColumn('Order', 'outtaken', { transaction: t }),
      ]),
    ),
}
