module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize.query(`
      ALTER TABLE "public"."UserRoles" RENAME TO "UserRole";
      ALTER TABLE "public"."OrganizationUsers" RENAME TO "OrganizationUser";
      ALTER TABLE "public"."OrderAttachments" RENAME TO "OrderAttachment";
    `),

  down: (queryInterface) =>
    queryInterface.sequelize.query(`
      ALTER TABLE "public"."UserRole" RENAME TO "UserRoles";
      ALTER TABLE "public"."OrganizationUser" RENAME TO "OrganizationUsers";
      ALTER TABLE "public"."OrderAttachment" RENAME TO "OrderAttachments";
    `),
}
