module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize.query(`
      BEGIN;
      -- for skuBatchId '6902a4eb-0bd4-42f8-810d-c3c94a1d5a19'
      UPDATE "LineItem"
      SET "batchId" = "SkuBatch"."batchId"
      FROM "SkuBatch"
      WHERE "SkuBatch".id = "LineItem"."skuBatchId"
      AND "LineItem"."batchId" IS DISTINCT FROM "SkuBatch"."batchId"
      AND "LineItem"."skuBatchId" = '6902a4eb-0bd4-42f8-810d-c3c94a1d5a19';
      DELETE FROM "InventoryItem" WHERE ID IN ('e0198053-06d4-4a68-81ac-b7955a295be8', 'f9abe25b-308a-4993-977f-a3edb87ca5b5');

      -- for skuBatchId '18cae6aa-ca6a-46de-b9a0-e5c19075569c'
      UPDATE "SkuBatch"
      SET "batchId" = '78b6e6c1-9be7-49de-a2f3-3a0bc67ac9e1'
      WHERE id = '18cae6aa-ca6a-46de-b9a0-e5c19075569c';

      UPDATE "LineItem"
      SET "batchId" = '78b6e6c1-9be7-49de-a2f3-3a0bc67ac9e1'
      WHERE "skuBatchId" = '18cae6aa-ca6a-46de-b9a0-e5c19075569c';

      -- for skuBatchId 'b65bad85-5433-4f46-bbbb-3bb9eca12ed1'
      UPDATE "LineItem"
      SET "batchId" = "SkuBatch"."batchId"
      FROM "SkuBatch"
      WHERE "SkuBatch".id = "LineItem"."skuBatchId"
      AND "LineItem"."batchId" IS DISTINCT FROM "SkuBatch"."batchId"
      AND "LineItem"."skuBatchId" = 'b65bad85-5433-4f46-bbbb-3bb9eca12ed1';
      DELETE FROM "InventoryItem" WHERE id IN ('60ab2e00-eea4-4685-8fbe-01811327588a', 'e6bb6a1b-4f49-40a0-8b9a-6b49c52170c5');

      -- for skuBatchId 'ca27676a-870d-40ea-88b6-87da7c103dff'
      UPDATE "LineItem"
      SET "batchId" = '58535954-b9a3-4d2d-a516-275b5539a212'
      WHERE "skuBatchId" = 'ca27676a-870d-40ea-88b6-87da7c103dff';

      -- for skuBatchId 'a5ea531b-4750-475f-a1bf-dc6fde5875c9' 
      UPDATE "LineItem"
      SET "batchId" = 'f1565eef-b4e0-45bb-81de-0e0d8a1dd1e2'
      WHERE "skuBatchId" = 'a5ea531b-4750-475f-a1bf-dc6fde5875c9';

      DELETE FROM "InventoryItem" WHERE id = 'fbfdbc72-590c-4bb3-91c4-9f98ed75042a';

      -- for skuBatchId 'b0b1cab7-6d34-4825-9126-f79020a488a2' 
      UPDATE "LineItem"
      SET "batchId" = '5e121c99-da1c-4a1c-a39e-bc1c68e3c695'
      WHERE "skuBatchId" = 'b0b1cab7-6d34-4825-9126-f79020a488a2';

      -- for skuBatchId 'bbfc279d-37d9-4477-8cfb-bf77b2f19c8b' 
      UPDATE "LineItem"
      SET "batchId" = '321a07ef-c47a-497a-8498-dc5bf191863b'
      WHERE "skuBatchId" = 'bbfc279d-37d9-4477-8cfb-bf77b2f19c8b';
      DELETE FROM "InventoryItem" WHERE id IN ('b181366b-7394-4981-99d3-008312772956', '7da96830-3c73-4d23-9ba6-1fa9df868c17');

      -- for skuBatchId 'eb9f7b6b-3abd-42ae-9fe8-3450a7e9150c'
      UPDATE "LineItem"
      SET "skuBatchId" = 'e2e745e9-ea5d-44a5-8371-dc700dd38a36'
      WHERE "skuId" = '77ac5a92-ad45-4243-bd06-34a282bcbf82' AND "batchId" = '6f9d87f1-f894-473c-85f1-71f5c679e2cc';
      UPDATE "InventoryItem"
      SET "batchId" = '6f9d87f1-f894-473c-85f1-71f5c679e2cc'
      WHERE "skuBatchId" = 'e2e745e9-ea5d-44a5-8371-dc700dd38a36';

      -- for skuBatchId 'e86e83b2-b86d-4ed1-a08e-f3607e29bcaf'
      UPDATE "LineItem"
      SET "batchId" = 'b450bf24-8f6c-449e-8e79-60712fcf4f95'
      WHERE "skuBatchId" = 'e86e83b2-b86d-4ed1-a08e-f3607e29bcaf';

      -- for skuBatchId 'effcdea0-bf28-4455-a91a-0402d548ea1e'
      UPDATE "LineItem"
      SET "batchId" = '53375704-07c1-42f1-b69f-1a170bbb52a2'
      WHERE "skuBatchId" = 'effcdea0-bf28-4455-a91a-0402d548ea1e';

      -- for skuBatchId '89002d24-59db-4394-b29d-3aca8ec6c1b4'
      UPDATE "LineItem"
      SET "batchId" = '1c536b30-2897-4722-b039-d9e63b583fa7'
      WHERE "skuBatchId" = '89002d24-59db-4394-b29d-3aca8ec6c1b4';

      -- for skuBatchId '06c5a0d0-6206-47d2-9fc8-8c8c452c7340'
      UPDATE "LineItem"
      SET "batchId" = 'eff99478-d1dd-404b-b330-20f00f8fff14'
      WHERE "skuBatchId" = '06c5a0d0-6206-47d2-9fc8-8c8c452c7340';
      DELETE FROM "InventoryItem" WHERE id = '03a6e590-2bcf-41a3-afbe-841aa9213733';

      -- for skuBatchId 'e549f0d0-5919-474e-9988-3977ee5a952f'
      UPDATE "LineItem"
      SET "batchId" = '5ccd1e01-face-4dae-afb9-73e53ad89ee6'
      WHERE "skuBatchId" = 'e549f0d0-5919-474e-9988-3977ee5a952f';
      UPDATE "InventoryLedgerItem"
      SET "inventoryItemId" = 'a6a9a286-3b0d-489b-a4f3-be8147092f6d'
      WHERE "inventoryItemId" = '708c0212-75d2-41b5-b7c6-29b8d53d2a26';
      UPDATE "InventoryItem"
      SET counted = 280.0
      WHERE id = 'a6a9a286-3b0d-489b-a4f3-be8147092f6d';
      DELETE FROM "InventoryItem" WHERE id = '708c0212-75d2-41b5-b7c6-29b8d53d2a26';

      -- for skuBatchId 'c64ac09c-e0e5-4310-9b67-b164b97324e3'
      UPDATE "LineItem"
      SET "batchId" = 'c6cc9917-af4c-4228-b3d8-d04395bb9c8a'
      WHERE "skuBatchId" = 'c64ac09c-e0e5-4310-9b67-b164b97324e3';

      COMMIT;
    `),
  /*
    6902a4eb-0bd4-42f8-810d-c3c94a1d5a19
    */

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  },
}
