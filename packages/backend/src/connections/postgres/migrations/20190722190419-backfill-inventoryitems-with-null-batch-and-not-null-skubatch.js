module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize.query(`
      update "InventoryItem" set "batchId" = "SkuBatch"."batchId"
      from "SkuBatch"
      where "SkuBatch".id = "InventoryItem"."skuBatchId"
      and "InventoryItem"."batchId" is null and "SkuBatch"."batchId" is not null;
    `),

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  },
}
