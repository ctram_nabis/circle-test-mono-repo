'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize
      .transaction((t) =>
        Promise.all([
          queryInterface.addColumn('Order', 'surcharge', {
            allowNull: false,
            defaultValue: 0,
            transaction: t,
            type: Sequelize.FLOAT,
          }),
          queryInterface.addColumn('Order', 'apSummaryGDriveFileId', {
            allowNull: true,
            defaultValue: null,
            transaction: t,
            type: Sequelize.STRING,
          }),
          queryInterface.addColumn('Order', 'invoicesS3FileLink', {
            allowNull: true,
            defaultValue: null,
            transaction: t,
            type: Sequelize.STRING,
          }),
          queryInterface.addColumn('LineItem', 'discount', {
            allowNull: false,
            defaultValue: 0,
            transaction: t,
            type: Sequelize.FLOAT,
          }),
          queryInterface.addColumn('LineItemChange', 'discount', {
            allowNull: false,
            defaultValue: 0,
            transaction: t,
            type: Sequelize.FLOAT,
          }),
          queryInterface.addColumn('Order', 'hasUnalterablePaymentTerms', {
            allowNull: true,
            defaultValue: false,
            transaction: t,
            type: Sequelize.BOOLEAN,
          }),
          queryInterface.addColumn('Order', 'mustPayPreviousBalance', {
            allowNull: true,
            defaultValue: false,
            transaction: t,
            type: Sequelize.BOOLEAN,
          }),
          queryInterface.addColumn('SkuBatch', 'titleholderId', {
            allowNull: true,
            defaultValue: null,
            transaction: t,
            type: Sequelize.UUID,
          }),
          queryInterface.addColumn('License', 'contactEmail', {
            allowNull: true,
            defaultValue: null,
            transaction: t,
            type: Sequelize.STRING,
          }),
          queryInterface.addColumn('OrderSubscriber', 'invoicesS3FileLink', {
            allowNull: true,
            defaultValue: null,
            transaction: t,
            type: Sequelize.STRING,
          }),
        ]),
      )
      .then(() =>
        queryInterface.sequelize.query(`
          UPDATE "Order"
          SET surcharge = 0;
        `),
      )
      .then(() =>
        queryInterface.sequelize.query(`
          UPDATE "LineItem"
          SET discount = 0;
        `),
      )
      .then(() =>
        queryInterface.sequelize.query(`
          UPDATE "LineItemChange"
          SET discount = 0;
        `),
      )
      .then(() =>
        queryInterface.createTable('Titleholder', {
          id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            defaultValue: Sequelize.UUIDV4,
          },
          organizationId: {
            defaultValue: null,
            type: Sequelize.UUID,
          },
          titleholderLicenseId: {
            defaultValue: null,
            type: Sequelize.UUID,
          },
          titleholderOrganizationId: {
            defaultValue: null,
            type: Sequelize.UUID,
          },
          type: {
            type: Sequelize.ENUM('LICENSE', 'ORGANIZATION'),
            allowNull: false,
          },
          createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.fn('NOW'),
          },
          updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.fn('NOW'),
          },
        }),
      )
      .then(() =>
        queryInterface.sequelize.transaction((t) =>
          Promise.all([
            queryInterface.addConstraint('Titleholder', ['organizationId'], {
              type: 'foreign key',
              name: 'Titleholder_organizationId_fk',
              references: {
                table: 'Organization',
                field: 'id',
              },
              onDelete: 'set null',
              onUpdate: 'cascade',
              transaction: t,
            }),
            queryInterface.addConstraint('Titleholder', ['titleholderOrganizationId'], {
              type: 'foreign key',
              name: 'Titleholder_titleholderOrganizationId_fk',
              references: {
                table: 'Organization',
                field: 'id',
              },
              onDelete: 'set null',
              onUpdate: 'cascade',
              transaction: t,
            }),
            queryInterface.addConstraint('Titleholder', ['titleholderLicenseId'], {
              type: 'foreign key',
              name: 'Titleholder_titleholderLicenseId_fk',
              references: {
                table: 'License',
                field: 'id',
              },
              onDelete: 'set null',
              onUpdate: 'cascade',
              transaction: t,
            }),
            queryInterface.addConstraint('SkuBatch', ['titleholderId'], {
              type: 'foreign key',
              name: 'SkuBatch_titleholderId_fk',
              references: {
                table: 'Titleholder',
                field: 'id',
              },
              onDelete: 'set null',
              onUpdate: 'cascade',
              transaction: t,
            }),
          ]),
        ),
      )
      .then(() =>
        queryInterface.sequelize.query(`
          -- EACH ORG GETS AN IDENTITY TITLEHOLDER THAT THEY HAVE ACCESS TO
          DO $$ 
          DECLARE
            temprow record;
          BEGIN 
            FOR temprow IN
              SELECT * FROM "Organization"
            LOOP
              INSERT INTO "Titleholder" (id, "organizationId", "titleholderOrganizationId", type) VALUES (uuid_generate_v4(), temprow.id, temprow.id, 'ORGANIZATION');
            END LOOP;
          END;
          $$;
        `),
      )
      .then(() =>
        queryInterface.sequelize.query(`
          -- EACH ORG GETS TITLEHOLDERS REPRESENTING THEIR LICENSES
          DO $$ 
          DECLARE
            temprow record;
          BEGIN 
            FOR temprow IN
              SELECT *
              FROM "License"
              INNER JOIN "Organization"
              ON "License"."organizationId" = "Organization".id
            LOOP
              INSERT INTO "Titleholder" (id, "organizationId", "titleholderLicenseId", type) VALUES (uuid_generate_v4(), temprow."organizationId", temprow.id, 'LICENSE');
            END LOOP;
          END;
          $$;
      `),
      )
      .then(() =>
        queryInterface.sequelize.query(`
          with "ids" as (
            Select "Titleholder"."id" as "tid", "SkuBatch"."id" as "sid" from "Titleholder"
            inner join "Sku" on "Titleholder"."titleholderOrganizationId" = "Sku"."organizationId"
            inner join "SkuBatch" on "SkuBatch"."skuId" = "Sku"."id"
          )
          update "SkuBatch" set "titleholderId" = "ids"."tid" from "ids" where "ids"."sid" = "SkuBatch"."id"
        `),
      ),

  down: (queryInterface, Sequelize) =>
    queryInterface.sequelize
      .transaction((t) =>
        Promise.all([
          queryInterface.removeConstraint('Titleholder', 'Titleholder_organizationId_fk', {
            transaction: t,
          }),
          queryInterface.removeConstraint('Titleholder', 'Titleholder_titleholderOrganizationId_fk', {
            transaction: t,
          }),
          queryInterface.removeConstraint('Titleholder', 'Titleholder_titleholderLicenseId_fk', {
            transaction: t,
          }),
          queryInterface.removeConstraint('SkuBatch', 'SkuBatch_titleholderId_fk', {
            transaction: t,
          }),
        ]),
      )
      .then(() =>
        queryInterface.sequelize.transaction((t) =>
          Promise.all([
            queryInterface.removeColumn('Order', 'surcharge', { transaction: t }),
            queryInterface.removeColumn('Order', 'apSummaryGDriveFileId', { transaction: t }),
            queryInterface.removeColumn('Order', 'invoicesS3FileLink', { transaction: t }),
            queryInterface.removeColumn('LineItem', 'discount', { transaction: t }),
            queryInterface.removeColumn('LineItemChange', 'discount', { transaction: t }),
            queryInterface.removeColumn('Order', 'hasUnalterablePaymentTerms', { transaction: t }),
            queryInterface.removeColumn('Order', 'mustPayPreviousBalance', { transaction: t }),
            queryInterface.removeColumn('SkuBatch', 'titleholderId', { transaction: t }),
            queryInterface.removeColumn('License', 'contactEmail', { transaction: t }),
            queryInterface.removeColumn('OrderSubscriber', 'invoicesS3FileLink', { transaction: t }),
          ]),
        ),
      )
      .then(() => queryInterface.dropTable('Titleholder')),
}
