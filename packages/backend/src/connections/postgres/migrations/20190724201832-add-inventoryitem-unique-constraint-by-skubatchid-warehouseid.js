module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addConstraint('InventoryItem', ['skuBatchId', 'warehouseId'], {
      type: 'unique',
      name: 'InventoryItem_skuBatchId_warehouseId_key',
    }),

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  },
}
