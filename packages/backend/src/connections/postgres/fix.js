const fix = (address) => {
  if (!address) return ''

  let add = address.replace(/[.,#!:;]/g, ' ')
  add = address.replace(/\s/g, ' ').toLowerCase()

  const ave = {
    av: 'avenue',
    ave: 'avenue',
    aven: 'avenue',
    avenu: 'avenue',
    hgwy: 'highway',
    bldg: 'building',
    wy: 'way',
    way: 'way',
    blvd: 'boulevard',
    dr: 'drive',
    driv: 'drive',
    drive: 'drive',
    st: 'street',
    str: 'street',
    strt: 'street',
    ln: 'lane',
    rd: 'road',
    pl: 'place',
    plce: 'place',
    plz: 'plaza',
    plaz: 'plaze',
    pkwy: 'parkway',
    parkwy: 'parkway',
    w: 'west',
    e: 'east',
    s: 'south',
    n: 'north',
    nw: 'northwest',
    ne: 'northeast',
    sw: 'southwest',
    se: 'southeast',
    ste: 'suite',
    suit: 'suite',
    uite: 'suite',
    apt: 'apartment',
    aprt: 'apartment',
    nit: 'unit',
  }

  const split = add.split(' ')
  let collect = ''
  for (let s of split) {
    if (ave[s]) {
      collect += ave[s] + ' '
    } else {
      collect += s + ' '
    }
  }

  collect = collect.toUpperCase().trim()
  return collect
}

module.exports = fix
