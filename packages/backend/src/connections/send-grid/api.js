// flow

import sgMail from '@sendgrid/mail'
import logger from '../../logging'

const SendGridAPIErrorHandler = (err) => {
  const msg = `[SendGrid] ${err}`
  logger.error(msg)
  return msg
}

export default class SendGridAPI {
  constructor(config) {
    this.config = config
    sgMail.setApiKey(config.key)
    sgMail.setSubstitutionWrappers('{{', '}}')
    this.client = sgMail
  }

  sendEmail = (emailData) => {
    /**
     * EXAMPLE
     *
     * const msg = {
     *   to: 'vince@getnabis.com',
     *   from: 'kevin@getnabis.com',
     *   subject: '[Success] Notification of Delivery',
     *   text: 'Woohoo!! This is the body of the message.',
     *   html: '<strong>HELLZ YAH</strong>',
     *   templateId: '13b8f94f-bcae-4ec6-b752-70d6cb59f932',
     *   substitutions: {
     *      name: 'Some One',
     *      city: 'Denver',
     *   },
     * };
     */

    return this.client
      .send(emailData)
      .then((res) => {
        // if error is not caught below, must mean that the email delivery was successful. return true boolean per the GraphQL resolver typedef
        if (res && res.length) return true
      })
      .catch((err) => {
        return SendGridAPIErrorHandler(err)
      })
  }
}
