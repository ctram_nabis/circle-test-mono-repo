// flow

import MailChimp from 'mailchimp-api-v3'
import cfg from '../../config'
import logger from '../../logging'

const MailChimpAPIErrorHandler = (err) => {
  const msg = `[MailChimp] ${err}`
  logger.error(msg)
  return msg
}

export default class MailChimpAPI {
  constructor(config) {
    this.config = config
    const mailchimp = new MailChimp(config.key)
    this.client = mailchimp
  }

  addNewUserToAudienceList = (user) => {
    const addNewUserToAudienceList = {
      method: 'post',
      path: `/lists/${cfg.mailChimp.userMailingListId}/members`,
      body: {
        email_address: user.email,
        status: 'subscribed',
        merge_fields: {
          FNAME: user.firstName,
          LNAME: user.lastName,
        },
      },
    }
    return this.client.request(addNewUserToAudienceList, (err, result) => {
      if (err) {
        return MailChimpAPIErrorHandler(err)
      }
      return result
    })
  }

  sendEmail = (emailData) => {
    /**
     * EXAMPLE
     *
     * const msg = {
     *   to: 'vince@getnabis.com',
     *   from: 'kevin@getnabis.com',
     *   subject: '[Success] Notification of Delivery',
     *   text: 'Woohoo!! This is the body of the message.',
     *   html: '<strong>HELLZ YAH</strong>',
     *   templateId: '13b8f94f-bcae-4ec6-b752-70d6cb59f932',
     *   substitutions: {
     *      name: 'Some One',
     *      city: 'Denver',
     *   },
     * };
     */

    return this.client
      .send(emailData)
      .then((res) => {
        if (res && res.length) {
          return res && res[0].complete
        }
      })
      .catch((err) => {
        return MailChimpAPIErrorHandler(err)
      })
  }
}
