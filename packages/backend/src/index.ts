const Sentry = require('@sentry/node')

import expressCluster from 'express-cluster'
import os from 'os'

import { IS_PROD } from './constants'
import start from './server'

if (IS_PROD) {
  Sentry.init({
    dsn: 'https://5410b6fcb2f5476a9646eed4b15269b9@sentry.io/1523476',
  })
}

if (!IS_PROD) {
  start()
} else {
  const cpuCount = os.cpus().length
  expressCluster(start, {
    count: cpuCount,
  })
}
