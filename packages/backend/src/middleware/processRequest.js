import uuidv1 from 'uuid/v1'
import logger from '../logging'

const processRequest = (context) => (req, res, next) => {
  // Set request context for availability in GraphQL resolvers
  req.context = {
    ...context,
  }
  // Generate ID for request
  req.context.id = uuidv1()
  req.context.startTime = new Date()
  logger.info(`Request ${req.context.id}`, `Start time: ${req.context.startTime}`)

  const tempEnd = res.end
  res.end = (chunk, encoding, cb) => {
    // eslint-disable-line no-unused-vars
    const endTime = new Date()
    logger.info(`Request ${req.context.id}`, `End time: ${endTime}`)
    const duration = endTime - req.context.startTime
    logger.info(`Request ${req.context.id}`, `Duration: ${duration} ms`)
    res.end = tempEnd
    res.end(chunk, encoding, cb)
  }

  next()
}

export default processRequest
