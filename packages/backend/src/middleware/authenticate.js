import httpError from 'http-errors'
import logger from '../logging'
import config from '../config'

const authenticate = (req, res, next) => {
  if (req.params.clientId) {
    // Authenticate Client URL
    if (req.auth && req.auth.aud === config.auth.audience.client) {
      const clientUuid = req.auth.sub
      if (!clientUuid) {
        logger.error('Invalid JWT payload for issuer.')
        throw httpError(403, 'Unauthorized: Invalid JWT')
      }
      return req.context.connections.logisticsDB.Organization.findByPk(clientUuid).then((client) => {
        if (!client || !req.params.clientId || client.name.toLowerCase() !== req.params.clientId.toLowerCase()) {
          return next(httpError(403, 'Unauthorized. We could not find a client for that token.'))
        }
        req.context.client = client
        return next()
      })
    }
    throw httpError(403, 'Unauthorized. We could not find a client for that token.')
  } else if (req.auth && req.auth.aud === config.auth.audience.admin) {
    // Authenticate Admin URL
    const userId = req.auth.sub
    if (!userId) {
      logger.error('Invalid JWT payload for issuer.')
      return next(httpError(403, 'Unauthorized: Invalid JWT'))
    }
    return req.context.connections.logisticsDB.User.findByPk(userId, {
      include: [{ association: 'roles' }, { association: 'organizations' }],
    }).then((user) => {
      if (!user) {
        return next(httpError(403, 'Unauthorized. We could not find a user for that token.'))
      }
      req.context.user = user
      return next()
    })
  } else if (!req.auth) {
    // No Authentication
    if (req.authToken || req.authToken === '') {
      // Check to see if the token is valid or not.
      return next(
        httpError(
          401,
          `Unauthorized. The authentication token is invalid. The${' '}
        token is either malformed or expired.`,
        ),
      )
    }
    logger.warn('Could not verify JWT')
    return next()
  }
  return next()
}

const getToken = (req) => {
  req.authToken = null
  const token = req.headers.authorization
  if (token && token.split(' ')[0] === 'Bearer') {
    req.authToken = token.split(' ')[1] || ''
  } else if (req.query && req.query.token) {
    req.authToken = req.query.token
  } else {
    req.authToken = token
  }
  return req.authToken
}

export { authenticate, getToken }
