import * as Sentry from '@sentry/node'
import { IS_PROD } from '../constants'

const LEVEL = {
  Fatal: 'fatal',
  Error: 'error',
  Warning: 'warning',
  Log: 'log',
  Info: 'info',
  Debug: 'debug',
  Critical: 'critical',
}

const logFunction = (levelType) => (...args) => {
  if (IS_PROD) {
    if (['fatal', 'error', 'critical'].includes(LEVEL[levelType])) {
      Sentry.captureMessage(args, levelType)
    }
  } else {
    switch (levelType) {
      case LEVEL.Fatal:
        console.error(`[${LEVEL.Fatal}]`, ...args)
        break
      case LEVEL.Error:
        console.error(`[${LEVEL.Error}]`, ...args)
        break
      case LEVEL.Warning:
        console.warn(`[${LEVEL.Warning}]`, ...args)
        break
      case LEVEL.Info:
        console.log(`[${LEVEL.Info}]`, ...args)
        break
      case LEVEL.Debug:
        console.log(`[${LEVEL.Debug}]`, ...args)
        break
      case LEVEL.Critical:
        console.error(`[${LEVEL.Critical}]`, ...args)
        break
      default:
        console.log(`[${LEVEL.Log}]`, ...args)
        break
    }
  }
}

const logger = {
  fatal: logFunction(LEVEL.Fatal),
  error: logFunction(LEVEL.Error),
  warn: logFunction(LEVEL.Warning),
  log: logFunction(LEVEL.Log),
  info: logFunction(LEVEL.Info),
  debug: logFunction(LEVEL.Debug),
  critical: logFunction(LEVEL.Critical),
}

const level = IS_PROD ? 'error' : 'debug'

logger.info(`Logger starting with log-level: ${level}`)

export default logger
