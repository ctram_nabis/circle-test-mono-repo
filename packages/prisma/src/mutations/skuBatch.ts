import { idArg, ObjectDefinitionBlock } from 'nexus/dist/core'

export const skuBatchMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createSkuBatch', {
    type: 'SkuBatch',
    args: {
      batchId: idArg(),
      skuId: idArg(),
      organizationId: idArg({ nullable: true }),
    },
    resolve: async (_, args, ctx) => {
      const skuBatch = await ctx.photon.skuBatchs.create({
        data: {
          ...args,
        },
      })

      return skuBatch
    },
  })

  t.field('archiveSkuBatch', {
    type: 'SkuBatch',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const skuBatch = await ctx.photon.skuBatches.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return skuBatch
    },
  })

  t.field('unarchiveSkuBatch', {
    type: 'SkuBatch',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const skuBatch = await ctx.photon.skuBatches.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return skuBatch
    },
  })
}
