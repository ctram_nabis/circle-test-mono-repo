import { hash } from 'bcryptjs'
import { ObjectDefinitionBlock, objectType, stringArg } from 'nexus/dist/core'
import { User } from '../types'
import { APP_SECRET, getUserId } from '../utils'

export const ResetPasswordPayload = objectType({
  name: 'ResetPasswordPayload',
  definition(t) {
    t.string('email', { nullable: true })
    t.boolean('success')
  },
})

export const UpdatePasswordPayload = objectType({
  name: 'UpdatePasswordPayload',
  definition(t) {
    t.boolean('success')
    t.field('user', {
      type: User,
      nullable: true,
    })
  },
})

export const userMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('resetPassword', {
    type: ResetPasswordPayload,
    args: {
      email: stringArg({ nullable: false }),
    },
    resolve: (parent, args, ctx) => {
      // TODO: Reset password via email link
    },
  })

  t.field('updatePassword', {
    type: UpdatePasswordPayload,
    args: {
      password: stringArg({ nullable: false }),
    },
    resolve: async (parent, args, ctx) => {
      const userId = getUserId(ctx)

      // the user isn't signed in
      if (!userId) {
        return {
          success: false,
        }
      }

      const user = ctx.photon.users.update({
        where: {
          id: userId,
        },
        data: {
          // TODO: Update the Salt with something other than '10'
          password: await hash(args.password, 10),
        },
      })

      return {
        user,
        success: true,
      }
    },
  })
}
