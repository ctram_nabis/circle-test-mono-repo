import { LicenseCreateInput, LicenseUpdateInput } from '@generated/photon'
import { arg, idArg, inputObjectType, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'

export const LicenseCreateInputType = inputObjectType({
  name: 'LicenseInputType',
  definition(t) {
    t.id('id')
    t.string('nickname')
    t.field('category', { type: 'EnumLicenseCategory' })
    t.field('type', { type: 'EnumLicenseType' })
    t.string('licenseNumber')
    t.string('legalEntityName')
    t.field('issuanceDate', { type: 'DateTime' })
    t.field('expirationDate', { type: 'DateTime' })
    t.string('contactName')
    t.string('contactPhone')
    t.string('address1')
    t.string('address2')
    t.string('city')
    t.string('state')
    t.string('zip')
    t.id('organization')
    t.id('site')
    t.id('licensedLocation')
  },
})

export const licenseMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createLicense', {
    type: 'License',
    args: {
      nickname: stringArg(),
      category: arg({ type: 'EnumLicenseCategory' }),
      type: arg({ type: 'EnumLicenseType' }),
      licenseNumber: stringArg(),
      legalEntityName: stringArg(),
      issuanceDate: arg({ type: 'DateTime' }),
      expirationDate: arg({ type: 'DateTime' }),
      contactName: stringArg(),
      contactPhone: stringArg(),
      address1: stringArg(),
      address2: stringArg(),
      city: stringArg(),
      state: stringArg(),
      zip: stringArg(),
      organizationId: idArg(),
      siteId: idArg(),
      licensedLocationId: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const license = await ctx.photon.licenses.create({
        data: {
          ...args,
        } as LicenseCreateInput,
      })

      return license
    },
  })

  t.field('updateLicense', {
    type: 'License',
    args: {
      id: idArg(),
      nickname: stringArg(),
      category: arg({ type: 'EnumLicenseCategory' }),
      type: arg({ type: 'EnumLicenseType' }),
      licenseNumber: stringArg(),
      legalEntityName: stringArg(),
      issuanceDate: arg({ type: 'DateTime' }),
      expirationDate: arg({ type: 'DateTime' }),
      contactName: stringArg(),
      contactPhone: stringArg(),
      address1: stringArg(),
      address2: stringArg(),
      city: stringArg(),
      state: stringArg(),
      zip: stringArg(),
      organizationId: idArg(),
      siteId: idArg(),
      licensedLocationId: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const license = await ctx.photon.licenses.update({
        where: {
          id,
        },
        data: {
          ...args,
        } as LicenseUpdateInput,
      })

      return license
    },
  })

  t.field('archiveLicense', {
    type: 'License',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const license = await ctx.photon.licenses.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return license
    },
  })

  t.field('unarchiveLicense', {
    type: 'License',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const license = await ctx.photon.licenses.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return license
    },
  })
}
