import { BatchCreateInput } from '@generated/photon'
import { arg, idArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'

export const batchMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createBatch', {
    type: 'Batch',
    args: {
      code: stringArg(),
      manifestGDriveFileId: stringArg(),
      manufacturingDate: arg({
        type: 'DateTime',
      }),
      expirationDate: arg({
        type: 'DateTime',
      }),
      organizationId: idArg(),
      licenseId: idArg(),
      skuId: idArg(),
      status: arg({ type: 'EnumBatchStatus' }),
    },
    resolve: async (_, args, ctx) => {
      const batch = await ctx.photon.batches.create({
        data: {
          ...args,
        } as BatchCreateInput,
      })

      return batch
    },
  })

  t.field('archiveBatch', {
    type: 'Batch',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const batch = await ctx.photon.batches.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return batch
    },
  })

  t.field('unarchiveBatch', {
    type: 'Batch',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const batch = await ctx.photon.batches.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return batch
    },
  })
}
