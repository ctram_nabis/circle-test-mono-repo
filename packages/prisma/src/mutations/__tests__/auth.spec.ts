import { endDB, refreshDB } from '@nabis/common/test/helper'
import { schema } from '@nabis/prisma/src'
import { graphql } from 'graphql'

import { photon } from '@nabis/common/test/helper'

beforeEach(() => refreshDB({ seed: 348 }))
afterAll(() => endDB())
beforeAll(() => {
  jest.setTimeout(20000)
})

it('should return not authorized when context is empty', async () => {
  const query = `
    query {
        me {
            id
        }
    }
  `

  const rootValue = {}

  const result = await graphql(schema, query, rootValue, {
    photon,
  })

  const { data, errors } = result

  expect(errors).not.toBeNull()
  expect(errors && errors[0].message).toEqual('Not Authorised!')
  expect(data).toBeNull()
})

it('should return no user found when email and/or password are invalid', async () => {
  const mutation = `
    mutation {
      login(
        email:"wrong@void.null",
        password:"password123"
      ) {
        token
        user {
          id
        }
      }
    }
  `

  const rootValue = {}

  const result = await graphql(schema, mutation, rootValue, {
    photon,
  })

  const { data, errors } = result

  expect(errors).not.toBeNull()
  expect(errors && errors[0].message).toContain('No user found for email')
  expect(data).toBeNull()
})

it('should return that the password is incorrect if the email is valid', async () => {
  const mutation = `
  mutation {
    login(
      email:"test@test.com",
      password:"not the right password"
    ) {
      token
      user {
        id
      }
    }
  }
`

  const rootValue = {}

  const result = await graphql(schema, mutation, rootValue, {
    photon,
  })

  const { data, errors } = result

  expect(errors).not.toBeNull()
  expect(errors && errors[0].message).toContain('Invalid password')
  expect(data).toBeNull()
})

it('should return the current user when the credentials are valid', async () => {
  const mutation = `
    mutation {
      login(
        email:"test@test.com",
        password:"password"
      ) {
        token
        user {
          id
          firstName
          lastName
          email
        }
      }
    }
  `

  const rootValue = {}
  const result = await graphql(schema, mutation, rootValue, {
    photon,
  })

  const { data } = result

  expect(data && data.login.user.firstName).toEqual('Test')
  expect(data && data.login.user.lastName).toEqual('Testington')
})

// it('should return the current user when user is logged in', async () => {
//   const query = `
//     query {
//       me {
//           id
//       }
//     }
//   `

//   const rootValue = {}
//   const context = getContext({ user })

//   const result = await graphql(schema, query, rootValue, context)
//   const { data } = result

//   expect(data.viewer.me.name).toBe(user.name)
// })
