import { DriverCreateInput } from '@generated/photon'
import { idArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'

export const driverMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createDriver', {
    type: 'Driver',
    args: {
      firstName: stringArg({ nullable: false }),
      lastName: stringArg(),
      driversLicense: stringArg(),
    },
    resolve: (_, args, ctx) => {
      const driver = ctx.photon.drivers.create({
        data: {
          ...args,
        } as DriverCreateInput,
      })

      return driver
    },
  })

  t.field('updateDriver', {
    type: 'Driver',
    args: {
      id: idArg({ nullable: false }),
      firstName: stringArg(),
      lastName: stringArg(),
      driversLicense: stringArg(),
    },
  })

  t.field('archiveDriver', {
    type: 'Driver',
    args: {
      id: idArg({ nullable: false }),
    },
    resolve: (_, args, ctx) => {
      const { id } = args
      const driver = ctx.photon.drivers.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return driver
    },
  })

  t.field('unarchiveDriver', {
    type: 'Driver',
    args: {
      id: idArg({ nullable: false }),
    },
    resolve: (_, args, ctx) => {
      const { id } = args
      const driver = ctx.photon.drivers.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return driver
    },
  })
}
