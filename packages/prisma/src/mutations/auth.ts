import { compare, hash } from 'bcryptjs'
import { sign } from 'jsonwebtoken'
import { ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'
import { APP_SECRET } from '../utils/auth'

export const authMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('signup', {
    type: 'AuthPayload',
    args: {
      firstName: stringArg({ nullable: true }),
      lastName: stringArg({ nullable: true }),
      email: stringArg(),
      password: stringArg(),
    },
    resolve: async (_parent, args, ctx) => {
      const { firstName, lastName, email, password } = args
      const hashedPassword = await hash(password, 10)

      const user = await ctx.photon.users.create({
        data: {
          firstName,
          lastName,
          email,
          password: hashedPassword,
          isAdmin: false,
        },
      })

      return {
        token: sign({ userId: user.id }, APP_SECRET),
        user,
      }
    },
  })

  t.field('signin', {
    type: 'AuthPayload',
    args: {
      email: stringArg(),
      password: stringArg(),
    },
    resolve: async (_parent, { email, password }, ctx) => {
      const users = await ctx.photon.users.findMany({
        where: {
          email,
        },
      })

      const user = users[0]

      if (!user) {
        throw new Error(`No user found for email: ${email}`)
      }

      const passwordValid = await compare(password, user.password)
      if (!passwordValid) {
        throw new Error('Invalid password')
      }

      return {
        token: sign({ userId: user.id }, APP_SECRET),
        user,
      }
    },
  })
}
