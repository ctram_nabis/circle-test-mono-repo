import { arg, idArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'

export const organizationMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createOrganization', {
    type: 'Organization',
    args: {
      name: stringArg(),
      alias: stringArg(),
      address1: stringArg(),
      address2: stringArg(),
      city: stringArg(),
      state: stringArg(),
      zip: stringArg(),
      phone: stringArg(),
      doingBusinessAs: stringArg(),
      // TODO: Add licenses arg?
      ownerId: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { ownerId } = args

      const connections = {
        owner: {
          connect: {
            id: ownerId,
          },
        },
      }

      const organization = ctx.photon.organizations.create({
        data: {
          ...connections,
          ...args,
        },
      })

      return organization
    },
  })

  t.field('updateOrganization', {
    type: 'Organization',
    args: {
      id: idArg({ nullable: false }),
      name: stringArg(),
      alias: stringArg(),
      address1: stringArg(),
      address2: stringArg(),
      city: stringArg(),
      state: stringArg(),
      zip: stringArg(),
      phone: stringArg(),
      doingBusinessAs: stringArg(),
      // TODO: Add licenses arg?
      ownerId: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id, ownerId } = args

      const connections = {
        owner: {
          id: ownerId,
        },
      }

      const organization = ctx.photon.organizations.update({
        where: {
          id,
        },
        data: {
          ...connections,
          ...args,
        },
      })

      return organization
    },
  })
}
