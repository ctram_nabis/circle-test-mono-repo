import { booleanArg, idArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'
import { getUserId } from '../utils'

export const organizationInviteMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('inviteUser', {
    type: 'OrganizationInvite',
    args: {
      email: stringArg(),
      organizationId: idArg(),
    },
    resolve: async (_parent, _args, ctx) => {
      const userId = getUserId(ctx)

      // get the user from the email
      // * the user must be signed up with their email for this to succeed
      const userToInvite = await ctx.photon.users.findOne({
        where: {
          email: _args.email,
        },
      })

      if (!userToInvite) {
        throw new Error("User doesn't have an associated account")
      }

      // create an organization invite
      const organizationInvite = await ctx.photon.organizationInvites.create({
        data: {
          accepted: false,
          email: userToInvite.email,
          sender: {
            connect: {
              id: userId,
            },
          },
          organization: {
            connect: {
              id: _args.organizationId,
            },
          },
        },
      })

      return organizationInvite
    },
  })

  t.field('acceptInvitation', {
    type: 'OrganizationInvite',
    args: {
      id: idArg(),
      accepted: booleanArg(),
    },
    resolve: async (_parent, _args, ctx) => {
      // accept the invite
      const organizationInvite = await ctx.photon.organizationInvites.update({
        where: {
          id: _args.id,
        },
        data: {
          accepted: _args.accepted,
          respondedAt: new Date(),
        },
      })

      // TODO: Save the Organization ID to an OrganizationUser row
      // const acceptedOrganizationId = organizationInvite.id

      // // create an organization user
      // await ctx.photon.organizationUsers.create({
      //   data: {
      //     organization: {
      //       connect: {
      //         id: acceptedOrganizationId,
      //       },
      //     },
      //     user: {
      //       connect: {
      //         email: organizationInvite.email,
      //       },
      //     },
      //   },
      // })

      return organizationInvite
    },
  })
}
