import { idArg, ObjectDefinitionBlock } from 'nexus/dist/core'

export const adminUserMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('adminArchiveUser', {
    type: 'User',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const user = await ctx.photon.users.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return user
    },
  })

  t.field('adminUnarchiveUser', {
    type: 'User',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const user = await ctx.photon.users.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return user
    },
  })
}
