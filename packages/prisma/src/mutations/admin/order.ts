import { EnumOrderStatus } from '@generated/photon'
import * as faker from 'faker'
import { arg, booleanArg, enumType, floatArg, idArg, intArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'
import { LineItemCreateInputType } from '../../types'

export const adminOrderMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('adminUpdateOrder', {
    args: {
      id: idArg(),
      adminNotes: stringArg(),
      assignee: idArg(),
      creator: idArg(),
      daysTillPaymentDue: intArg(),
      deletedAt: arg({ type: 'DateTime' }),
      discount: floatArg(),
      exciseTax: floatArg(),
      exciseTaxCollected: floatArg(),
      gmv: floatArg(),
      gmvCollected: floatArg(),
      irn: stringArg(),
      isArchived: booleanArg(),
      licensedLocation: idArg(),
      manifestGDriveFileId: stringArg(),
      name: stringArg(),
      notes: stringArg(),
      number: intArg(),
      orgLicenseNum: stringArg(),
      paymentTerms: stringArg(),
      siteLicenseNum: stringArg(),
      timeWindow: stringArg(),
      driver: idArg(),
      // TODO: Add  InventoryLedgerItem arg to order
      lineItems: arg({
        type: LineItemCreateInputType,
        list: true,
        description: 'Create multiple LineItems for an Order.',
        nullable: true,
      }),
      status: arg({ type: 'EnumOrderStatus' }),
      organization: idArg(),
      site: idArg(),
      vehicle: idArg(),
      warehouse: idArg(),
    },
    type: 'Order',
    resolve: async (_, args, ctx) => {
      // TODO: This should be easier than this
      // // save lineItems for later
      // const lineItems = args.lineItems
      // delete args.lineItems
      // // update order
      // const order = await ctx.photon.orders.update({
      //   where: {
      //     id: args.id,
      //   },
      //   data: {
      //     ...args,
      //   },
      // })
      // for (const li of lineItems) {
      //   // save information to connect
      //   const [skuId, batchId, organizationId] = [li.sku, li.batch, li.organization]
      //   // delete saved ids before creating the line item
      //   delete li.sku
      //   delete li.batch
      //   delete li.organization
      //   const connections = {
      //     sku: {
      //       connect: {
      //         id: skuId,
      //       },
      //     },
      //     batch: {
      //       connect: {
      //         id: batchId,
      //       },
      //     },
      //     organization: {
      //       connect: {
      //         id: organizationId,
      //       },
      //     },
      //     order: {
      //       connect: {
      //         id: order.id,
      //       },
      //     },
      //   }
      //   // remove any connections that won't exist
      //   if (!skuId) {
      //     delete connections.sku
      //   }
      //   if (!batchId) {
      //     delete connections.batch
      //   }
      //   if (!organizationId) {
      //     delete connections.organization
      //   }
      //   // update the line items if the id was provided
      //   if (li.id) {
      //     await ctx.photon.lineItems.update({
      //       where: {
      //         id: li.id,
      //       },
      //       data: {
      //         ...connections,
      //         ...li,
      //       },
      //     })
      //   } else {
      //     // otherwise try to upsert the line item
      //     try {
      //       await ctx.photon.lineItems.create({
      //         data: {
      //           ...connections.,
      //           ...li,
      //         },
      //       })
      //     } catch (e) {
      //       throw new Error(e)
      //     }
      //   }
      // }
      // // return the order
      // return order
    },
  })
}
