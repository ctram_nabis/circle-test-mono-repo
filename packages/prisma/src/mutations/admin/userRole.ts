import { idArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'

export const adminUserRoleMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('adminCreateUserRole', {
    type: 'UserRole',
    args: {
      name: stringArg(),
      permissions: stringArg({ list: true, nullable: true }),
      organizationId: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const userRole = await ctx.photon.userRoles.create({
        data: {
          ...args,
        },
      })

      return userRole
    },
  })

  t.field('adminUpdateUserRole', {
    type: 'UserRole',
    args: {
      name: stringArg({ nullable: true }),
      permissions: stringArg({ list: true, nullable: true }),
      organizationId: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const order = await ctx.photon.userRoles.update({
        where: {
          id: args.id,
        },
        data: {
          ...args,
        },
      })

      return order
    },
  })
}
