import {
  EnumLicensedLocationSiteCategory,
  EnumSiteSiteCategory,
  LicenseCreateInput,
  LicenseUpdateInput,
} from '@generated/photon'
import { arg, floatArg, idArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'
import { LicenseCreateInputType } from './license'

export const licensedLocationMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createLicensedLocation', {
    type: 'LicensedLocation',
    args: {
      name: stringArg(),
      address1: stringArg(),
      address2: stringArg({ nullable: true }),
      city: stringArg(),
      state: stringArg(),
      zip: stringArg(),
      lat: floatArg({ nullable: true }),
      lng: floatArg({ nullable: true }),
      // siteCategory: ,
      licenses: arg({
        type: LicenseCreateInputType,
        list: true,
        description: 'Create multiple Licenses for LicensedLocation.',
      }),
    },
    resolve: async (_, args, ctx) => {
      // TODO: This should be easier than this
      // // save licenses for later
      // const licenses = args.licenses
      // delete args.licenses
      // // create licensed location without licenses
      // const licensedLocation = await ctx.photon.licensedLocations.create({
      //   data: {
      //     ...args,
      //   },
      // })
      // // upsert all licenses
      // for (const license of licenses) {
      //   const [organizationId, siteId] = [license.organization, license.site]
      //   const connections = {
      //     organization: {
      //       connect: {
      //         id: organizationId,
      //       },
      //     },
      //     site: {
      //       connect: {
      //         id: siteId,
      //       },
      //     },
      //     licensedLocation: {
      //       connect: {
      //         id: licensedLocation.id,
      //       },
      //     },
      //   }
      //   if (!organizationId) {
      //     delete connections.organization
      //   }
      //   if (!siteId) {
      //     delete connections.site
      //   }
      //   await ctx.photon.licenses.upsert({
      //     where: {
      //       id: license.id || faker.random.uuid(),
      //     },
      //     create: {
      //       ...connections,
      //       ...license,
      //     },
      //     update: {
      //       ...connections,
      //       ...license,
      //     },
      //   })
      // }
      // return licensedLocation
    },
  })

  t.field('updateLicensedLocation', {
    args: {
      id: idArg(),
      name: stringArg(),
      address1: stringArg(),
      address2: stringArg({ nullable: true }),
      city: stringArg(),
      state: stringArg(),
      zip: stringArg(),
      lat: floatArg({ nullable: true }),
      lng: floatArg({ nullable: true }),
      // siteCategory: arg({ t ype}),
      licenses: arg({
        type: LicenseCreateInputType,
        list: true,
        description: 'Create multiple Licenses for LicensedLocation.',
      }),
    },
    type: 'LicensedLocation',
    resolve: async (_, args, ctx) => {
      // TODO: This should be easier than this
      // save licenses for later
      const { id, licenses } = args

      // update license
      const license = ctx.photon.licenses.update({
        where: {
          id,
        },
        data: {
          ...args,
        },
      })

      if (licenses) {
        for (const _license of licenses) {
          // save information to connect
          const { licensedLocation: licensedLocationId, organization: organizationId, site: siteId } = _license
          const connections = {
            site: {
              connect: {
                id: siteId,
              },
            },
            organization: {
              connect: {
                id: organizationId,
              },
            },
            licensedLocation: {
              connect: {
                id: licensedLocationId,
              },
            },
          }

          // remove any connections that won't exist
          if (!siteId) {
            delete connections.site
          }

          if (!organizationId) {
            delete connections.organization
          }

          if (!licensedLocationId) {
            delete connections.licensedLocation
          }

          // update the licenses if the id was provided
          if (_license.id) {
            ctx.photon.licenses.update({
              where: {
                id: _license.id,
              },
              data: {
                ...connections,
                ..._license,
              } as LicenseUpdateInput,
            })
          } else {
            // otherwise try to upsert the line item
            try {
              ctx.photon.licenses.create({
                data: {
                  ...connections,
                  ..._license,
                } as LicenseCreateInput,
              })
            } catch (e) {
              throw new Error(e)
            }
          }
        }

        // return the order
        return license
      }
    },
  })

  t.field('archiveLicensedLocation', {
    type: 'LicensedLocation',
    args: {
      id: idArg({ nullable: false }),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const licensedLocation = await ctx.photon.licensedLocations.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return licensedLocation
    },
  })

  t.field('unarchiveLicensedLocation', {
    type: 'LicensedLocation',
    args: {
      id: idArg({ nullable: false }),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const licensedLocation = await ctx.photon.licensedLocations.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return licensedLocation
    },
  })
}
