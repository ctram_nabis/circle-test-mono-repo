import { ObjectDefinitionBlock, objectType } from 'nexus/dist/core'

const GenerateManifestPayload = objectType({
  name: 'GenerateManifestPayload',
  definition(t) {
    t.string('url')
  },
})

// TODO: Add manifest mutations.
export const manifestMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {}
