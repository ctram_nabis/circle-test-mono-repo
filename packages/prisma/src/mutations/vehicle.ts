import { idArg, intArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'

export const vehicleMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createVehicle', {
    type: 'Vehicle',
    args: {
      name: stringArg({ nullable: true }),
      make: stringArg({ nullable: true }),
      model: stringArg({ nullable: true }),
      licensePlate: stringArg({ nullable: true }),
      year: intArg({ nullable: true }),
    },
    resolve: async (_, args, ctx) => {
      const vehicle = await ctx.photon.vehicles.update({
        data: {
          ...args,
        },
      })

      return vehicle
    },
  })

  t.field('archiveVehicle', {
    type: 'Vehicle',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const vehicle = await ctx.photon.vehicles.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return vehicle
    },
  })

  t.field('unarchiveVehicle', {
    type: 'Vehicle',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const vehicle = await ctx.photon.vehicles.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return vehicle
    },
  })
}
