import { idArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'
import { getUserId } from '../utils'

export const draftMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createDraft', {
    type: 'Draft',
    args: {
      organization: idArg(),
      body: stringArg(),
    },
    resolve: async (_, args, ctx) => {
      const draftCreatorId = getUserId(ctx)
      const draft = ctx.photon.drafts.create({
        data: {
          ...args,
          creator: {
            connect: {
              id: draftCreatorId,
            },
          },
        },
      })

      return draft
    },
  })

  t.field('updateDraft', {
    type: 'Draft',
    args: {
      id: idArg(),
      organization: idArg(),
      body: stringArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const draft = ctx.photon.drafts.update({
        where: {
          id,
        },
        data: {
          ...args,
        },
      })

      return draft
    },
  })

  t.field('archiveDraft', {
    type: 'Draft',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const draft = ctx.photon.drafts.update({
        where: {
          id,
        },
        data: {
          isArchived: true,
        },
      })
    },
  })
}
