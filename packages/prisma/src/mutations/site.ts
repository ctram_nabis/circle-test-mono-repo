import { LicenseCreateInput, LicenseUpdateInput, SiteUpdateInput } from '@generated/photon'
import { arg, floatArg, idArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'

export const siteMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createSite', {
    type: 'Site',
    args: {
      name: stringArg(),
      pocName: stringArg(),
      pocPhoneNumber: stringArg(),
      pocEmail: stringArg(),
      address1: stringArg(),
      address2: stringArg(),
      city: stringArg(),
      state: stringArg(),
      zip: stringArg(),
      lat: floatArg(),
      lng: floatArg(),
      // siteCategory: arg({ type: '' }),
      licenses: arg({
        type: 'LicenseInputType',
        list: true,
        description: 'Create multiple Licenses for a Site.',
      }),
      organizationId: idArg(),
      licensedLocationId: idArg(),
    },
    resolve: async (_, args, ctx) => {
      // TODO: This should be easier than this
      // // save licenses for later
      // const licenses = args.licenses
      // delete args.licenses
      // // create site without licenses
      // const site = await ctx.photon.sites.create({
      //   data: {
      //     ...args,
      //   },
      // })
      // // upsert all licenses
      // for (const license of licenses) {
      //   const [organizationId, siteId] = [license.organization, license.site]
      //   const connections = {
      //     organization: {
      //       connect: {
      //         id: organizationId,
      //       },
      //     },
      //     site: {
      //       connect: {
      //         id: siteId,
      //       },
      //     },
      //     // TODO: I think this is problematic...
      //     licensedLocation: {
      //       connect: {
      //         id: site.id,
      //       },
      //     },
      //   }
      //   if (!organizationId) {
      //     delete connections.organization
      //   }
      //   if (!siteId) {
      //     delete connections.site
      //   }
      //   await ctx.photon.licenses.upsert({
      //     where: {
      //       id: license.id || faker.random.uuid(),
      //     },
      //     create: {
      //       ...connections,
      //       ...license,
      //     },
      //     update: {
      //       ...connections,
      //       ...license,
      //     },
      //   })
      // }
      // return site
    },
  })

  t.field('updateSite', {
    type: 'Site',
    args: {
      id: idArg(),
      name: stringArg(),
      pocName: stringArg(),
      pocPhoneNumber: stringArg(),
      pocEmail: stringArg(),
      address1: stringArg(),
      address2: stringArg(),
      city: stringArg(),
      state: stringArg(),
      zip: stringArg(),
      lat: floatArg(),
      lng: floatArg(),
      // siteCategory: arg({ type: '' }),
      licenses: arg({
        type: 'LicenseInputType',
        list: true,
        description: 'Create multiple Licenses for a Site.',
        nullable: true,
      }),
      organizationId: idArg(),
      licensedLocationId: idArg(),
    },
    resolve: async (_, args, ctx) => {
      // TODO: This should be easier than this
      // save licenses for later
      const { licenses, id } = args

      // update site
      const site = ctx.photon.sites.update({
        where: {
          id,
        },
        data: {
          ...args,
        } as SiteUpdateInput,
      })

      if (licenses) {
        for (const _license of licenses) {
          // save information to connect
          const { licensedLocation: licensedLocationId, organization: organizationId } = _license
          const connections = {
            organization: {
              connect: {
                id: organizationId,
              },
            },
            licensedLocation: {
              connect: {
                id: licensedLocationId,
              },
            },
          }

          // remove any connections that won't exist
          if (!organizationId) {
            delete connections.organization
          }
          if (!licensedLocationId) {
            delete connections.licensedLocation
          }

          // update the licenses if the id was provided
          if (_license.id) {
            ctx.photon.licenses.update({
              where: {
                id: _license.id,
              },
              data: {
                ...connections,
                ..._license,
              } as LicenseUpdateInput,
            })
          } else {
            // otherwise try to upsert the line item
            try {
              ctx.photon.licenses.create({
                data: {
                  ...connections,
                  ..._license,
                } as LicenseCreateInput,
              })
            } catch (e) {
              throw new Error(e)
            }
          }
        }
      }

      // return the site
      return site
    },
  })

  t.field('archiveSite', {
    type: 'Site',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const site = await ctx.photon.sites.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return site
    },
  })

  t.field('unarchiveSite', {
    type: 'Site',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const site = await ctx.photon.sites.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return site
    },
  })
}
