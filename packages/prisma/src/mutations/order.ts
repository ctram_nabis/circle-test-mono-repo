import {
  EnumOrderAction,
  EnumOrderPaymentStatus,
  EnumOrderStatus,
  LineItemCreateInput,
  LineItemUpdateInput,
  OrderCreateInput,
} from '@generated/photon'
import * as faker from 'faker'
import { arg, booleanArg, floatArg, idArg, intArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'

export const orderMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createOrder', {
    type: 'Order',
    args: {
      action: arg({ type: 'EnumOrderAction' }),
      adminNotes: stringArg(),
      assignee: idArg(),
      creator: idArg(),
      daysTillPaymentDue: intArg(),
      discount: floatArg(),
      exciseTax: floatArg(),
      exciseTaxCollected: floatArg(),
      gmv: floatArg(),
      gmvCollected: floatArg(),
      intaken: booleanArg(),
      irn: stringArg(),
      isArchived: booleanArg(),
      isExcluded: booleanArg(),
      licensedLocation: idArg(),
      manifestGDriveFileId: stringArg(),
      name: stringArg(),
      notes: stringArg(),
      number: intArg(),
      orgLicenseNum: stringArg(),
      paymentTerms: stringArg(),
      siteLicenseNum: stringArg(),
      timeWindow: stringArg(),
      order: idArg(),
      outtaken: booleanArg(),
      paymentStatus: arg({ type: 'EnumOrderPaymentStatus' }),
      status: arg({ type: 'EnumOrderStatus' }),
      // TODO: Add Inventory Ledger Items
      lineItems: arg({
        type: 'LineItemInputType',
        list: true,
        description: 'Create multiple LineItems for an Order.',
      }),
      organization: idArg(),
      site: idArg(),
      vehicle: idArg(),
      warehouse: idArg(),
    },
    resolve: async (root, args, ctx) => {
      // TODO: This should be easier than this
      // save lineItems for later
      const { lineItems } = args

      // create order
      const order = await ctx.photon.orders.create({
        data: {
          ...args,
        } as OrderCreateInput,
      })

      if (lineItems) {
        for (const lineItem of lineItems) {
          // save information to connectem.organization]
          const { sku: skuId, organization: organizationId, batch: batchId } = lineItem

          const connections = {
            sku: {
              connect: {
                id: skuId,
              },
            },
            batch: {
              connect: {
                id: batchId,
              },
            },
            organization: {
              connect: {
                id: organizationId,
              },
            },
            order: {
              connect: {
                id: order.id,
              },
            },
          }

          // remove any connections that won't exist
          if (!skuId) {
            delete connections.sku
          }
          if (!batchId) {
            delete connections.batch
          }
          if (!organizationId) {
            delete connections.organization
          }

          // upsert the line items
          await ctx.photon.lineItems.upsert({
            where: {
              id: lineItem.id || faker.random.uuid(),
            },
            create: {
              ...connections,
              ...lineItem,
            } as LineItemCreateInput,
            update: {
              ...connections,
              ...lineItem,
            } as LineItemUpdateInput,
          })
        }
      }

      // return the order
      return order
    },
  })

  t.field('archiveOrder', {
    type: 'Order',
    args: {
      id: idArg({ nullable: false }),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const order = await ctx.photon.orders.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return order
    },
  })

  t.field('unarchiveOrder', {
    type: 'Order',
    args: {
      id: idArg({ nullable: false }),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const order = await ctx.photon.orders.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return order
    },
  })
}
