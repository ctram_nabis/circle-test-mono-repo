import { arg, booleanArg, floatArg, idArg, ObjectDefinitionBlock, stringArg } from 'nexus/dist/core'
import { SkuUpdateInput, SkuCreateInput } from '@generated/photon'

export const skuMutations = (t: ObjectDefinitionBlock<'Mutation'>) => {
  t.field('createSku', {
    type: 'Sku',
    args: {
      name: stringArg(),
      code: stringArg(),
      isExciseTaxable: booleanArg(),
      // inventoryType: arg({ type: '' }),
      // inventoryClass: arg({ type: '' }),
      // inventoryCategory: arg({ type: '' }),
      organizationId: idArg(),
      isSample: booleanArg(),
      pricePerUnit: floatArg(),
      unit: stringArg(),
      batchId: idArg(),
      isArchived: booleanArg(),
    },
    resolve: (_, args, ctx) => {
      const sku = ctx.photon.skus.create({
        data: {
          ...args,
        } as SkuCreateInput,
      })

      return sku
    },
  })

  t.field('updateSku', {
    type: 'Sku',
    args: {
      id: idArg(),
      name: stringArg(),
      code: stringArg(),
      isExciseTaxable: booleanArg(),
      // inventoryType: arg({ type: }),
      // inventoryClass: arg({ type: }),
      // inventoryCategory: arg({ type: }),
      organizationId: idArg(),
      isSample: booleanArg(),
      pricePerUnit: floatArg(),
      unit: stringArg(),
      batchId: idArg(),
      isArchived: booleanArg(),
    },
    resolve: (_, args, ctx) => {
      const { id } = args
      const sku = ctx.photon.skus.update({
        where: {
          id,
        },
        data: {
          ...args,
        } as SkuUpdateInput,
      })

      return sku
    },
  })

  t.field('archiveSku', {
    type: 'Sku',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const sku = await ctx.photon.skus.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      })

      return sku
    },
  })

  t.field('unarchiveSku', {
    type: 'Sku',
    args: {
      id: idArg(),
    },
    resolve: async (_, args, ctx) => {
      const { id } = args
      const sku = await ctx.photon.skus.update({
        where: {
          id,
        },
        data: {
          deletedAt: null,
        },
      })

      return sku
    },
  })
}
