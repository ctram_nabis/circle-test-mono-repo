import {
  EnumLicenseType,
  EnumOrderAction,
  EnumOrderPaymentStatus,
  EnumOrderStatus,
  EnumSiteSiteCategory,
  EnumSkuInventoryCategory,
  EnumSkuInventoryClass,
  EnumSkuInventoryType,
} from '@generated/photon'

export const paymentStatuses: EnumOrderPaymentStatus[] = [
  EnumOrderPaymentStatus.COD_PAID,
  EnumOrderPaymentStatus.NET_TERMS_PAID,
  EnumOrderPaymentStatus.REMITTED,
  EnumOrderPaymentStatus.SELF_COLLECTED,
  EnumOrderPaymentStatus.UNPAID,
]

export const licenseTypes: EnumLicenseType[] = [
  EnumLicenseType.CANNABIS_EVENTS,
  EnumLicenseType.CULTIVATOR,
  EnumLicenseType.DISTRIBUTOR,
  EnumLicenseType.DISTRIBUTOR_TRANSPORT_ONLY,
  EnumLicenseType.MANUFACTURER,
  EnumLicenseType.MICROBUSINESS,
  EnumLicenseType.RETAILER,
  EnumLicenseType.RETAILER_NONSTOREFRONT,
  EnumLicenseType.TESTING,
  EnumLicenseType.TESTING_LABORATORY,
]

export const inventoryCategories: EnumSkuInventoryCategory[] = [
  EnumSkuInventoryCategory.BROWNIE,
  EnumSkuInventoryCategory.BUD,
  EnumSkuInventoryCategory.BUTANE_HASH_OIL,
  EnumSkuInventoryCategory.BUTTER,
  EnumSkuInventoryCategory.CHIPS,
  EnumSkuInventoryCategory.CO2_OIL,
  EnumSkuInventoryCategory.CONCENTRATE,
  EnumSkuInventoryCategory.COOKIE,
  EnumSkuInventoryCategory.CREAM,
  EnumSkuInventoryCategory.DISSOLVABLE_POWDER,
  EnumSkuInventoryCategory.GREENHOUSE,
  EnumSkuInventoryCategory.GUMMY,
  EnumSkuInventoryCategory.HARD_CANDY,
  EnumSkuInventoryCategory.HONEY,
  EnumSkuInventoryCategory.ICE_CREAM,
  EnumSkuInventoryCategory.INDOOR,
  EnumSkuInventoryCategory.INFUSED_PRE_ROLLS,
  EnumSkuInventoryCategory.INHALER,
  EnumSkuInventoryCategory.JUICE,
  EnumSkuInventoryCategory.LEAFLETTES,
  EnumSkuInventoryCategory.LIP_BALM,
  EnumSkuInventoryCategory.MARSHMALLOW,
  EnumSkuInventoryCategory.MINT,
  EnumSkuInventoryCategory.MIXED,
  EnumSkuInventoryCategory.OTHER_BATTERY_TYPES,
  EnumSkuInventoryCategory.OTHER,
  EnumSkuInventoryCategory.PATCH,
  EnumSkuInventoryCategory.PAX,
  EnumSkuInventoryCategory.PEN,
  EnumSkuInventoryCategory.PRE_ROLLS,
  EnumSkuInventoryCategory.SHAKE,
  EnumSkuInventoryCategory.SODA,
  EnumSkuInventoryCategory.SOLVENTLESS,
  EnumSkuInventoryCategory.SPRAY,
  EnumSkuInventoryCategory.SUBLINGUAL,
  EnumSkuInventoryCategory.SUN_GROWN,
  EnumSkuInventoryCategory.TAFFY,
  EnumSkuInventoryCategory.TEA,
  EnumSkuInventoryCategory.THREAD_510,
  EnumSkuInventoryCategory.TINCTURES,
  EnumSkuInventoryCategory.TRANSDERMAL,
  EnumSkuInventoryCategory.TRIM,
  EnumSkuInventoryCategory.WATER,
]

export const inventoryClasses: EnumSkuInventoryClass[] = [
  EnumSkuInventoryClass.CARTRIDGE,
  EnumSkuInventoryClass.CONCENTRATE,
  EnumSkuInventoryClass.DISPOSABLE,
  EnumSkuInventoryClass.EDIBLES,
  EnumSkuInventoryClass.FLOWER,
  EnumSkuInventoryClass.OTHER,
  EnumSkuInventoryClass.PRE_ROLL,
  EnumSkuInventoryClass.TOPICAL,
]

export const inventoryTypes: EnumSkuInventoryType[] = [EnumSkuInventoryType.CANNABIS, EnumSkuInventoryType.NON_CANNABIS]

export const orderActions: EnumOrderAction[] = [
  EnumOrderAction.DELIVERY_TO_BRAND,
  EnumOrderAction.DELIVERY_TO_RETAILER,
  EnumOrderAction.DROPOFF_TO_NABIS,
  EnumOrderAction.INTERNAL_TRANSFER,
  EnumOrderAction.OTHER,
  EnumOrderAction.PAYMENT_PICKUP_FROM_RETAILER,
  EnumOrderAction.PICKUP_FROM_BRAND,
  EnumOrderAction.PICKUP_FROM_NABIS,
  EnumOrderAction.RETURN_FROM_RETAILER,
]

export const orderStatuses: EnumOrderStatus[] = [
  EnumOrderStatus.APPROVAL_NEEDED,
  EnumOrderStatus.CANCELLED,
  EnumOrderStatus.DELAYED,
  EnumOrderStatus.DELIVERED_WITH_EDITS,
  EnumOrderStatus.DELIVERED,
  EnumOrderStatus.DRAFT,
  EnumOrderStatus.REJECTED,
  EnumOrderStatus.SCHEDULED,
  EnumOrderStatus.TRANSFERRING,
  EnumOrderStatus.UNSCHEDULED,
]

export const siteCategories: EnumSiteSiteCategory[] = [
  EnumSiteSiteCategory.CONSUMER,
  EnumSiteSiteCategory.CULTIVATOR,
  EnumSiteSiteCategory.DISTRIBUTOR,
  EnumSiteSiteCategory.MANUFACTURER,
  EnumSiteSiteCategory.MICROBUSINESS,
  EnumSiteSiteCategory.OTHER,
  EnumSiteSiteCategory.RETAILER,
]
