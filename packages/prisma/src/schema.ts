import { makeSchema } from 'nexus'
import { nexusPrismaPlugin } from 'nexus-prisma'
import { join } from 'path'
import * as types from './types'

export const schema = makeSchema({
  types,
  plugins: [
    nexusPrismaPlugin({
      inputs: {
        photon: join(__dirname, '../node_modules/@generated/photon'),
      },
    }),
  ],
  outputs: {
    schema: join(__dirname, '/schema.graphql'),
    typegen: join(__dirname, '../generated/nexus-typegen.ts'),
  },
  typegenAutoConfig: {
    sources: [
      {
        source: '@generated/photon',
        alias: 'photon',
      },
      {
        source: require.resolve('./context'),
        alias: 'Context',
      },
    ],
    contextType: 'Context.Context',
  },
})
