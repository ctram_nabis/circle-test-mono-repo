import { photon } from '@nabis/common/test/helper'
import { schema } from '@nabis/prisma/src'
import { graphql } from 'graphql'

it('should not allow users to edit other users', async () => {
  const mutation = `
    mutation {
        updateOneUser(where:{
          email:"test@test.com"
        }, data:{
          email:"hacked@test.com"
        }) {
          id
          email
        }
      }
    `

  const rootValue = {}

  const result = await graphql(schema, mutation, rootValue, {
    photon,
  })

  const { data, errors } = result

  expect(errors[0].message).toEqual('Not Authorised!')
})
