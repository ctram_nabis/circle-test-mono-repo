import { allow, or, rule, shield } from 'graphql-shield'
import { getUserId } from '../utils/auth'

const rules = {
  isAuthenticatedUser: rule()((_parent, _args, context) => {
    const userId = getUserId(context)
    return Boolean(userId)
  }),
  isAdmin: rule()(async (_parent, { id }, context) => {
    const user = await context.photon.users.findOne({
      where: {
        id,
      },
    })

    return user.isAdmin
  }),
  isOrderOwner: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const order = await context.photon.orders.findOne({
      where: {
        id,
      },
    })

    const orderOwner = order.creator()
    return orderOwner.id === userId
  }),
  isDraftOwner: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const draft = await context.photon.drafts.findOne({
      where: {
        id,
      },
    })

    const draftOwner = draft.creator()
    return draftOwner.id === userId
  }),
  isUserOwner: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const user = await context.photon.users.findOne({
      where: {
        id,
      },
    })

    return user.id === userId
  }),
  isOrganizationOwner: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const organization = await context.photon.organizations.findOne({
      where: {
        id,
      },
    })

    const organizationOwner = organization.owner()
    return organizationOwner.id === userId
  }),
  isOrganizationBatch: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const organization = await context.photon.organizations.findOne({
      where: {
        id,
      },
    })

    const organizationUser = await context.photon.organizationUsers.findOne({
      where: {
        organization: {
          id: organization.id,
        },
        user: {
          id: userId,
        },
      },
    })

    return organizationUser
  }),
  isOrganizationLicense: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const license = await context.photon.licenses.findOne({
      where: {
        id,
      },
    })

    const licenseOrganization = license.organization()

    const organizationUser = context.photon.organizationUsers.findOne({
      where: {
        organization: {
          id: licenseOrganization.id,
        },
        user: {
          id: userId,
        },
      },
    })

    return organizationUser
  }),
  isOrganizationLicensedLocation: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const licensedLocation = await context.photon.licensedLocations.findOne({
      where: {
        id,
      },
    })

    const licensedLocationOwner = licensedLocation.submittedBy()
    return licensedLocationOwner.id === userId
  }),
  isOrganizationSite: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const site = await context.photon.sites.findOne({
      where: {
        id,
      },
    })

    const siteOrganization = site.organization()

    const organizationUser = await context.photon.organizationUsers.findOne({
      where: {
        organization: {
          id: siteOrganization.id,
        },
        user: {
          id: userId,
        },
      },
    })

    return organizationUser
  }),
  isSkuOwner: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const sku = await context.photon.skus.findOne({
      where: {
        id,
      },
    })

    const skuOrganization = sku.organization()

    const organizationUser = await context.photon.organizationUsers.findOne({
      where: {
        organization: {
          id: skuOrganization.id,
        },
        user: {
          id: userId,
        },
      },
    })

    return organizationUser
  }),
  isSkuBatchOwner: rule()(async (_parent, { id }, context) => {
    const userId = getUserId(context)
    const skuBatch = await context.photon.skuBatches.findOne({
      where: {
        id,
      },
    })

    const skuId = skuBatch.sku().id

    const sku = await context.photon.skus.findOne({
      where: {
        id: skuId,
      },
    })

    const skuOrganization = sku.organization()

    const organizationUser = await context.photon.organizationUsers.findOne({
      where: {
        organization: {
          id: skuOrganization.id,
        },
        user: {
          id: userId,
        },
      },
    })

    return organizationUser
  }),
}

export const permissions = shield({
  Query: {
    // TODO: These all need to be tested
    me: rules.isAuthenticatedUser,
  },
  Mutation: {
    // TODO: These all need to be tested
    createBatch: rules.isAuthenticatedUser, // * Raise permissions
    archiveBatch: or(rules.isAdmin, rules.isOrganizationBatch),
    unarchiveBatch: or(rules.isAdmin, rules.isOrganizationBatch),

    createDriver: rules.isAdmin, // * Do they need to be admin?
    archiveDriver: rules.isAdmin,
    unarchiveDriver: rules.isAdmin,

    createVehicle: rules.isAdmin, // * Raise permissions
    archiveVehicle: rules.isAdmin,
    unarchiveVehicle: rules.isAdmin,

    createLicense: rules.isAuthenticatedUser, // * Raise permissions
    archiveLicense: or(rules.isAdmin, rules.isOrganizationLicense),
    unarchiveLicense: or(rules.isAdmin, rules.isOrganizationLicense),

    createLicensedLocation: rules.isAuthenticatedUser, // * Raise permissions
    archiveLicensedLocation: or(rules.isAdmin, rules.isOrganizationLicensedLocation),
    unarchiveLicensedLocation: or(rules.isAdmin, rules.isOrganizationLicensedLocation),

    createOrder: rules.isAuthenticatedUser, // * Raise permissions
    archiveOrder: or(rules.isAdmin, rules.isOrderOwner),
    unarchiveOrder: or(rules.isAdmin, rules.isOrderOwner),

    createOrganization: rules.isAuthenticatedUser, // * Raise permissions

    inviteUser: rules.isAuthenticatedUser, // * Maybe raise permissions?
    acceptInvitation: allow, // * Maybe raise permissions?

    createSite: rules.isAuthenticatedUser, // * Raise permissions
    archiveSite: or(rules.isAdmin, rules.isOrganizationSite),
    unarchiveSite: or(rules.isAdmin, rules.isOrganizationSite),

    createSku: rules.isAuthenticatedUser, // * Raise permissions
    archiveSku: or(rules.isAdmin, rules.isSkuOwner),
    unarchiveSku: or(rules.isAdmin, rules.isSkuOwner),

    createSkuBatch: rules.isAuthenticatedUser, // * Raise permissions
    archiveSkuBatch: or(rules.isAdmin, rules.isSkuBatchOwner),
    unarchiveSkuBatch: or(rules.isAdmin, rules.isSkuBatchOwner),
  },
})
