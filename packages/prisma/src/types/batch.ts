import { objectType } from 'nexus'

export const Batch = objectType({
  name: 'Batch',
  definition(t) {
    t.model.id()
    t.model.code()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.expirationDate()
    t.model.inventoryItems()
    t.model.isArchived()
    t.model.license()
    t.model.lineItems()
    t.model.manifestGDriveFileId()
    t.model.manufacturingDate()
    t.model.organization()
    t.model.sku()
    t.model.skuBatches()
    t.model.status()
    t.model.testDate()
    t.model.updatedAt()
  },
})
