import { objectType } from 'nexus'

export const Sku = objectType({
  name: 'Sku',
  definition(t) {
    t.model.id()
    t.model.batches()
    t.model.code()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.inventoryCategory()
    t.model.inventoryClass()
    t.model.inventoryItems()
    t.model.inventoryType()
    t.model.isArchived()
    t.model.isExciseTaxable()
    t.model.lineItems()
    t.model.name()
    t.model.organization()
    t.model.pricePerUnit()
    t.model.skuBatches()
    t.model.unit()
    t.model.updatedAt()
  },
})
