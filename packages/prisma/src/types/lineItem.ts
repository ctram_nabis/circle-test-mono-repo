import { inputObjectType, objectType } from 'nexus'

export const LineItemCreateInputType = inputObjectType({
  name: 'LineItemInputType',
  definition(t) {
    t.id('id', { nullable: true })
    t.boolean('isExciseTaxable')
    t.boolean('isSample')
    t.boolean('oversold')
    t.float('pricePerUnit')
    t.float('quantity')
    t.id('batch')
    t.id('order')
    t.id('organization')
    t.id('skuBatch')
    t.id('sku')
    t.string('code')
    t.string('name')
    t.string('unit')
  },
})

export const LineItem = objectType({
  name: 'LineItem',
  definition(t) {
    t.model.id()
    t.model.batch()
    t.model.code()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.isExciseTaxable()
    t.model.isSample()
    t.model.name()
    t.model.order()
    t.model.organization()
    t.model.oversold()
    t.model.pricePerUnit()
    t.model.quantity()
    t.model.sku()
    t.model.skuBatch()
    t.model.unit()
    t.model.updatedAt()
  },
})
