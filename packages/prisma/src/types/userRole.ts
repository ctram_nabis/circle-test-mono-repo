import { objectType } from 'nexus'

export const UserRole = objectType({
  name: 'UserRole',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.organization()
    t.model.role()
    t.model.updatedAt()
    t.model.user()
  },
})
