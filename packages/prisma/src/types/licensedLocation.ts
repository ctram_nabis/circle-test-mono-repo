import { objectType } from 'nexus'

export const LicensedLocation = objectType({
  name: 'LicensedLocation',
  definition(t) {
    t.model.id()
    t.model.address1()
    t.model.address2()
    t.model.city()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.lat()
    t.model.licenses()
    t.model.lng()
    t.model.name()
    t.model.siteCategory()
    t.model.sites()
    t.model.state()
    t.model.submittedBy()
    t.model.updatedAt()
    t.model.verificationAt()
    t.model.verificationStatus()
    t.model.warehouse()
    t.model.zip()
  },
})
