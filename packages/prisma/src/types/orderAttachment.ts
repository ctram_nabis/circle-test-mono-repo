import { objectType } from 'nexus'

export const OrderAttachment = objectType({
  name: 'OrderAttachment',
  definition(t) {
    t.model.id()
    t.model.attachment()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.order()
    t.model.updatedAt()
  },
})
