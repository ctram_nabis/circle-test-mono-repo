import { objectType } from 'nexus'

export const Role = objectType({
  name: 'Role',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.description()
    t.model.name()
    t.model.organization()
    t.model.permissions()
    t.model.updatedAt()
    t.model.userRoles()
  },
})
