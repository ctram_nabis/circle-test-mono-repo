import { objectType } from 'nexus'

export const InventoryLedgerItem = objectType({
  name: 'InventoryLedgerItem',
  definition(t) {
    t.model.id()
    // t.model.transactionType()
    t.model.createdAt()
    t.model.creator()
    t.model.deletedAt()
    t.model.endAllocated()
    t.model.endCounted()
    t.model.endPromised()
    t.model.inventoryItem()
    t.model.notes()
    t.model.order()
    t.model.organization()
    t.model.startAllocated()
    t.model.startCounted()
    t.model.startPromised()
    t.model.updatedAt()
  },
})
