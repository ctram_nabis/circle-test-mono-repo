import { objectType } from 'nexus'

export const Organization = objectType({
  name: 'Organization',
  definition(t) {
    t.model.id()
    t.model.address1()
    t.model.address2()
    t.model.alias()
    t.model.attachments()
    t.model.batches()
    t.model.city()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.doingBusinessAs()
    t.model.drafts()
    t.model.inventoryItems()
    t.model.inventoryLedgerItems()
    t.model.licenses()
    t.model.lineItems()
    t.model.manifestGDriveFolderId()
    t.model.name()
    t.model.orders()
    t.model.orderChanges()
    t.model.orderSubscribers()
    t.model.organizationInvites()
    t.model.organizationUsers()
    t.model.owner()
    t.model.phone()
    t.model.roles()
    t.model.sites()
    t.model.skus()
    t.model.state()
    // t.model.suborganizations()
    // t.model.superorganizations()
    t.model.updatedAt()
    t.model.userRoles()
    t.model.zip()

    t.int('memberCount', {
      resolve: async (parent, args, ctx) => {
        const organization = await ctx.photon.organizations.findOne({
          where: {
            id: parent.id,
          },
        })

        const organizationUsers = await ctx.photon.organizationUsers.findMany({
          where: {
            organization: {
              id: organization.id,
            },
          },
        })

        return organizationUsers.length || 0
      },
    })
  },
})
