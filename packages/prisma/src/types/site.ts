import { objectType } from 'nexus'

export const Site = objectType({
  name: 'Site',
  definition(t) {
    t.model.id()
    t.model.address1()
    t.model.address2()
    t.model.adminNotes()
    t.model.city()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.lat()
    t.model.licenses()
    t.model.licensedLocation()
    t.model.lng()
    t.model.name()
    t.model.orders()
    t.model.organization()
    t.model.pocEmail()
    t.model.pocName()
    t.model.pocPhoneNumber()
    t.model.siteCategory()
    t.model.state()
    t.model.updatedAt()
    t.model.warehouses()
    t.model.zip()
  },
})
