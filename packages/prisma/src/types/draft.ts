import { objectType } from 'nexus'

export const Draft = objectType({
  name: 'Draft',
  definition(t) {
    t.model.id()
    t.model.body()
    t.model.createdAt()
    t.model.creator()
    t.model.deletedAt()
    t.model.organization()
    t.model.updatedAt()
  },
})
