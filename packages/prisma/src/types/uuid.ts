import { scalarType } from 'nexus'

export const uuidType = scalarType({
  name: 'UUID',
  serialize: (value: any) => value,
  description: 'A UUID type',
  parseValue: (value: any) => value,
  parseLiteral: (value: any) => value,
})
