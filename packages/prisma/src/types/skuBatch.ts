import { objectType } from 'nexus'

export const SkuBatch = objectType({
  name: 'SkuBatch',
  definition(t) {
    t.model.id()
    t.model.batch()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.inventoryItems()
    t.model.isLocked()
    t.model.lineItems()
    t.model.sku()
    t.model.updatedAt()
  },
})
