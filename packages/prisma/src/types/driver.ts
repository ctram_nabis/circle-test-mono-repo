import { objectType } from 'nexus'

export const Driver = objectType({
  name: 'Driver',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.driversLicense()
    t.model.firstName()
    t.model.lastName()
    t.model.orders()
    t.model.updatedAt()
  },
})
