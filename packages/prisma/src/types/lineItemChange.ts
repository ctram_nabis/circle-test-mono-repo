import { objectType } from 'nexus'

export const LineItemChange = objectType({
  name: 'LineItemChange',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.isExciseTaxable()
    t.model.isSample()
    t.model.lineItem()
    t.model.name()
    t.model.orderChange()
    t.model.pricePerUnit()
    t.model.quantity()
    t.model.updatedAt()
  },
})
