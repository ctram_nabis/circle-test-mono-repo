import { EnumOrderAction, EnumOrderPaymentStatus, EnumOrderStatus } from '@generated/photon'
import { idArg, objectType } from 'nexus'

export const User = objectType({
  name: 'User',
  definition(t) {
    t.model.id()
    t.model.address1()
    t.model.address2()
    t.model.assignedOrders()
    t.model.city()
    t.model.createdAt()
    t.model.createdOrders()
    t.model.deletedAt()
    t.model.drafts()
    t.model.email()
    t.model.email()
    t.model.firstName()
    t.model.inventoryLedgerItems()
    t.model.isAdmin()
    t.model.isArchived()
    t.model.lastName()
    t.model.orderChanges()
    t.model.organizations()
    t.model.organizationInvites()
    t.model.organizationUsers()
    t.model.phone()
    t.model.profilePicture()
    t.model.state()
    t.model.updatedAt()
    t.model.userRoles()
    t.model.zip()

    t.field('allOutstandingInvoices', {
      type: 'Order',
      list: true,
      args: {
        organizationId: idArg({ nullable: true }),
      },
      resolve: async (root, args, ctx) => {
        const { begin = new Date('01-01-2019') } = args
        const { end = new Date(`01-01-${new Date().getFullYear() + 10}`) } = args

        const { organizationId } = args

        const where: any = {
          isArchived: false,
          AND: [
            { paymentStatus: EnumOrderPaymentStatus.UNPAID },
            { action: EnumOrderAction.DELIVERY_TO_RETAILER },
            // TODO: Verify that the DateTime filtering works
            { date: { gt: begin, lt: end } },
          ],
          NOT: [{ daysTillPaymentDue: null }],
          OR: [{ status: EnumOrderStatus.DELIVERED }, { status: EnumOrderStatus.DELIVERED_WITH_EDITS }],
        }

        if (organizationId) {
          where.AND.push({
            organization: {
              id: organizationId,
            },
          })
        }

        // TODO: Ask philip about the remaining filtering in outstandingInvoicesViewerResolver

        if (root) {
          return ctx.photon.orders.findMany({ where })
        } else {
          throw new Error('Must be signed in to view outstanding invoices.')
        }
      },
    })
  },
})
