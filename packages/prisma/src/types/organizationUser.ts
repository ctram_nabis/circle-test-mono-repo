import { objectType } from 'nexus'

export const OrganizationUser = objectType({
  name: 'OrganizationUser',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.organization()
    t.model.updatedAt()
    t.model.user()
  },
})
