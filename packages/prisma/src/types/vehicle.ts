import { objectType } from 'nexus'

export const Vehicle = objectType({
  name: 'Vehicle',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.licensePlate()
    t.model.make()
    t.model.model()
    t.model.name()
    t.model.orders()
    t.model.updatedAt()
    t.model.year()
  },
})
