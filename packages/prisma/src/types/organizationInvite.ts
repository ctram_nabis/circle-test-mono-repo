import { objectType } from 'nexus'

export const OrganizationInvite = objectType({
  name: 'OrganizationInvite',
  definition(t) {
    t.model.id()
    t.model.accepted()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.email()
    t.model.organization()
    t.model.respondedAt()
    t.model.sender()
    t.model.updatedAt()
  },
})
