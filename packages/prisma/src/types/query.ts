import { arg, intArg, queryType, stringArg } from 'nexus'
import { getUserId } from '../utils/auth'

export const Query = queryType({
  definition(t) {
    t.field('me', {
      type: 'User',
      resolve: (_parent, _args, ctx) => {
        const userId = getUserId(ctx)

        if (!userId) {
          throw new Error('Not Authorised!')
        }

        const user = ctx.photon.users.findOne({
          where: {
            id: userId,
          },
        })

        return user
      },
    })

    t.crud.order()
    t.field('allOrders', {
      type: 'Order',
      list: true,
      args: {
        where: arg({
          type: 'OrderWhereUniqueInput',
        }),
        // orderBy: 'OrderOrderByInput',
        // select: arg({
        //   type: 'OrderSelect',
        // }),
        skip: stringArg(),
        after: stringArg(),
        before: stringArg(),
        first: intArg(),
        last: intArg(),
      },
      resolve: (parent, args, ctx) =>
        ctx.photon.orders.findMany({
          ...args,
        }),
    })

    t.field('archivedOrders', {
      type: 'Order',
      list: true,
      resolve: (parent, args, ctx) =>
        ctx.photon.orders({
          isArchived: true,
        }),
    })

    t.crud.draft()
    t.field('allDrafts', {
      type: 'Draft',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.drafts(),
    })

    t.field('archivedDrafts', {
      type: 'Draft',
      list: true,
      resolve: (parent, args, ctx) =>
        ctx.photon.drafts({
          isArchived: true,
        }),
    })

    t.crud.site()
    t.field('allSites', {
      type: 'Site',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.sites(),
    })

    t.crud.sku()
    t.field('allSkus', {
      type: 'Sku',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.skus(),
    })

    t.field('archivedSkus', {
      type: 'Sku',
      list: true,
      resolve: (parent, args, ctx) =>
        ctx.photon.skus({
          isArchived: true,
        }),
    })

    t.crud.batch()
    t.field('allBatches', {
      type: 'Batch',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.batches(),
    })

    t.field('archivedBatches', {
      type: 'Batch',
      list: true,
      resolve: (parent, args, ctx) =>
        ctx.photon.batches({
          isArchived: true,
        }),
    })

    t.crud.skuBatch()
    t.field('allSkuBatches', {
      type: 'SkuBatch',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.skuBatches(),
    })

    t.field('archivedSkuBatches', {
      type: 'SkuBatch',
      list: true,
      resolve: (parent, args, ctx) =>
        ctx.photon.skuBatches({
          isArchived: true,
        }),
    })

    t.crud.organizationInvites()

    t.crud.organizationUsers()

    t.crud.inventoryItem()
    t.field('allInventoryItems', {
      type: 'InventoryItem',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.inventoryItems(),
    })

    t.field('archivedInventoryItems', {
      type: 'InventoryItem',
      list: true,
      resolve: (parent, args, ctx) =>
        ctx.photon.inventoryItems({
          isArchived: true,
        }),
    })

    t.crud.driver()
    t.field('allDrivers', {
      type: 'Driver',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.drivers(),
    })

    t.field('archivedDrivers', {
      type: 'Driver',
      list: true,
      resolve: (parent, args, ctx) =>
        ctx.photon.drivers({
          isArchived: true,
        }),
    })

    t.crud.organization()
    t.field('allOrganizations', {
      type: 'Organization',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.organizations(),
    })

    t.field('archivedOrganizations', {
      type: 'Organization',
      list: true,
      resolve: (parent, args, ctx) =>
        ctx.photon.organizations({
          isArchived: true,
        }),
    })

    t.crud.licenses()
    t.field('allLicenses', {
      type: 'License',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.licenses(),
    })

    t.crud.licensedLocation()
    t.field('allLicensedLocations', {
      type: 'LicensedLocation',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.licensedLocations(),
    })

    t.field('archivedLicensedLocations', {
      type: 'LicensedLocation',
      list: true,
      resolve: (parent, args, ctx) =>
        ctx.photon.licensedLocations({
          isArchived: true,
        }),
    })

    t.crud.warehouse()
    t.field('allWarehouses', {
      type: 'Warehouse',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.warehouses(),
    })

    t.crud.vehicle()
    t.field('allVehicles', {
      type: 'Vehicle',
      list: true,
      resolve: (parent, args, ctx) => ctx.photon.vehicles(),
    })
  },
})
