import { objectType } from 'nexus'

export const OrderChange = objectType({
  name: 'OrderChange',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.description()
    t.model.lineItemChanges()
    t.model.order()
    t.model.organization()
    t.model.submitter()
    t.model.updatedAt()
  },
})
