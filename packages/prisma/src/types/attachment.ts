import { objectType } from 'nexus'

export const Attachment = objectType({
  name: 'Attachment',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.gDriveFileId()
    t.model.name()
    t.model.orderAttachments({ pagination: false })
    t.model.organization()
    t.model.updatedAt()
  },
})
