import { objectType } from 'nexus'

export const License = objectType({
  name: 'License',
  definition(t) {
    t.model.id()
    t.model.address1()
    t.model.address2()
    t.model.adminNotes()
    t.model.batches()
    t.model.category()
    t.model.city()
    t.model.contactName()
    t.model.contactPhone()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.expirationDate()
    t.model.issuanceDate()
    t.model.legalEntityName()
    t.model.licensedLocation()
    t.model.licenseNumber()
    t.model.nickname()
    t.model.organization()
    t.model.site()
    t.model.state()
    t.model.type()
    t.model.updatedAt()
    t.model.warehouses()
    t.model.zip()
  },
})
