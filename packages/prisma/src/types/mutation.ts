import { mutationType } from 'nexus'
import {
  attachmentMutations,
  authMutations,
  batchMutations,
  draftMutations,
  driverMutations,
  inventoryMutations,
  licensedLocationMutations,
  licenseMutations,
  lineItemMutations,
  manifestMutations,
  onfleetMutations,
  orderChangeMutations,
  orderMutations,
  organizationInviteMutations,
  organizationMutations,
  organizationUserMutations,
  roleMutations,
  sendGridMutations,
  siteMutations,
  skuBatchMutations,
  skuMutations,
  userMutations,
  userPreferenceMutations,
  userRoleMutations,
  vehicleMutations,
  warehouseMutations,
} from '../mutations'

import { adminOrderMutations, adminUserMutations, adminUserRoleMutations } from '../mutations/admin'

export const Mutation = mutationType({
  definition(t) {
    authMutations(t)

    adminOrderMutations(t)
    adminUserMutations(t)
    adminUserRoleMutations(t)

    attachmentMutations(t)
    batchMutations(t)
    draftMutations(t)
    driverMutations(t)
    inventoryMutations(t)
    licensedLocationMutations(t)
    licenseMutations(t)
    lineItemMutations(t)
    manifestMutations(t)
    onfleetMutations(t)
    orderChangeMutations(t)
    orderMutations(t)
    organizationInviteMutations(t)
    organizationMutations(t)
    organizationUserMutations(t)
    roleMutations(t)
    sendGridMutations(t)
    siteMutations(t)
    skuMutations(t)
    skuBatchMutations(t)
    userMutations(t)
    userPreferenceMutations(t)
    userRoleMutations(t)
    vehicleMutations(t)
    warehouseMutations(t)
  },
})
