import { objectType } from 'nexus'

export const Warehouse = objectType({
  name: 'Warehouse',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.inventoryItems()
    t.model.isArchived()
    t.model.license()
    t.model.orders()
    t.model.site()
    t.model.updatedAt()
  },
})
