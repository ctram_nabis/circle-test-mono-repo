import { objectType } from 'nexus'

export const UserPreference = objectType({
  name: 'UserPreference',
  definition(t) {
    t.model.id()
    t.model.deletedAt()
    t.model.organization()
    t.model.receiveOrderUpdateEmails()
    t.model.user()
  },
})
