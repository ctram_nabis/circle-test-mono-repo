import { objectType } from 'nexus'

export const OrganizationOrganization = objectType({
  name: 'OrganizationOrganization',
  definition(t) {
    t.model.id()
    // t.model.suborganizationId({})
    // t.model.superorganizationId({})
    t.model.createdAt()
    t.model.deletedAt()
    t.model.updatedAt()
  },
})
