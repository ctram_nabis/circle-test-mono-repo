import { objectType } from 'nexus'

export const InventoryItem = objectType({
  name: 'InventoryItem',
  definition(t) {
    t.model.id()
    // t.model.storageType()
    t.model.allocated()
    t.model.batch()
    t.model.counted()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.inventoryLedgerItems()
    t.model.organization()
    t.model.promised()
    t.model.sku()
    t.model.skuBatch()
    t.model.updatedAt()
    t.model.warehouse()
  },
})
