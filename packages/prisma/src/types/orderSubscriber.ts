import { objectType } from 'nexus'

export const OrderSubscriber = objectType({
  name: 'OrderSubscriber',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.deletedAt()
    t.model.manifestGDriveFileId()
    t.model.order()
    t.model.subscriber()
    t.model.updatedAt()
  },
})
