mutation SignUp {
  signup(name:"Chris", email:"chris@nabis.com", password:"mypassword") {
    user {
      id
    }
    token
  }
}

mutation CreateDraft{
  createDraft(title:"My first post", content:"content goes here...") {
    id
    published
  }
}

mutation PublishDraft{
  publish(id:"ck0xqprkj0001e260eaapacl7") {
    id
    published
    title
    content
    author {
      id
      name
      email
    }
  }
}

