/**
 * seed.ts
 * $ # Consider adding this as `yarn seed <seed>' to keep the seed consistent
 * $ ts-node seed.ts <seed>
 *
 * This will generate seed data and insert it into a running db.
 * The VS Code Outline view is helpful for this file.
 */

import {
  Driver,
  License,
  LicensedLocation,
  Order,
  Organization,
  Photon,
  Site,
  Sku,
  User,
  Vehicle,
  Warehouse,
} from '@generated/photon'
import faker from 'faker'
import {
  inventoryCategories,
  inventoryClasses,
  inventoryTypes,
  licenseTypes,
  orderActions,
  orderStatuses,
  paymentStatuses,
  siteCategories,
} from '../src/enums'

const photon = new Photon()

/// Variables for # of seeded models
const N_SEED_BATCHES = 200
const N_SEED_DRIVERS = 20
const N_SEED_LICENSEDLOCATIONS = 20
const N_SEED_LICENSES = 20
const N_SEED_ORDERS = 100
const N_SEED_ORGANIZATIONS = 15
const N_SEED_SITES = 30
const N_SEED_SKUS = 100
const N_SEED_USERS = 50
const N_SEED_VEHICLES = 20

interface SeedOptions {
  seed?: number
}

/**
 * The SeedStore generates and stores all of the seed data.
 */
export class SeedStore {
  static seededBatchs: any[] = []
  static seededDrivers: any[] = []
  static seededLicensedLocations: any[] = []
  static seededLicenses: any[] = []
  static seededOrders: any[] = []
  static seededOrganizations: any[] = []
  static seededSites: any[] = []
  static seededSkus: any[] = []
  static seededUsers: any[] = []
  static seededVehicles: any[] = []
  static seededWarehouses: any[] = []

  // This var. contains the data that will be stored in the JSON output
  static jsonOut: any = {}

  /**
   * Remove all of the seeded elements in the static fields.
   */
  static cleanSeedStore = () => {
    SeedStore.seededDrivers = []
    SeedStore.seededLicensedLocations = []
    SeedStore.seededLicenses = []
    SeedStore.seededOrders = []
    SeedStore.seededOrganizations = []
    SeedStore.seededSites = []
    SeedStore.seededSkus = []
    SeedStore.seededUsers = []
    SeedStore.seededVehicles = []
    SeedStore.seededWarehouses = []
  }

  /**
   * Get a random driver that has been seeded
   */
  static randomDriver = (): Driver => {
    return faker.random.arrayElement(SeedStore.seededDrivers)
  }

  /**
   * Get a random license that has been seeded
   */
  static randomLicense = (): License => {
    return faker.random.arrayElement(SeedStore.seededLicenses)
  }

  /**
   * Get a random license location has been seeded
   */
  static randomLicensedLocation = (): LicensedLocation => {
    return faker.random.arrayElement(SeedStore.seededLicensedLocations)
  }

  /**
   * Get a random order that has been seeded
   */
  static randomOrder = (): Order => {
    return faker.random.arrayElement(SeedStore.seededOrders)
  }

  /**
   * Get a random organization that has been seeded
   */
  static randomOrganization = (): Organization => {
    return faker.random.arrayElement(SeedStore.seededOrganizations)
  }

  /**
   * Get a random site that has been seeded
   */
  static randomSite = (): Site => {
    return faker.random.arrayElement(SeedStore.seededSites)
  }

  /**
   * Get a random sku that has been seeded
   */
  static randomSku = (): Sku => {
    return faker.random.arrayElement(SeedStore.seededSkus)
  }

  /**
   * Get a random user that has been seeded
   */
  static randomUser = (): User => {
    return faker.random.arrayElement(SeedStore.seededUsers)
  }

  /**
   * Get a random vehicle that has been seeded
   */
  static randomVehicle = (): Vehicle => {
    return faker.random.arrayElement(SeedStore.seededVehicles)
  }

  /**
   * Get a random warehouse that has been seeded
   */
  static randomWarehouse = (): Warehouse => {
    return faker.random.arrayElement(SeedStore.seededWarehouses)
  }

  /**
   * Seed Batches with random data
   */
  static seedBatchs = async () => {
    for (let i = 0; i < N_SEED_BATCHES; i++) {
      const mfgDate = faker.date.past(1)
      const expDate = faker.date.future(1, mfgDate)

      const data = {
        id: faker.random.uuid(),
        code: `SEED-${faker.random.alphaNumeric(2)}-${faker.random.alphaNumeric(4)}`,
        createdAt: faker.date.past(1),
        manufacturingDate: mfgDate,
        expirationDate: expDate,
        isArchived: false,
        manifestGDriveFileId: 'SEED',
        sku: {
          connect: {
            id: SeedStore.randomSku().id,
          },
        },
      }

      const batch = await photon.batches.create({ data })

      SeedStore.seededBatchs.push(batch)
    }
  }

  /**
   * Seed Drivers with random data
   */
  static seedDrivers = async () => {
    for (let i = 0; i < N_SEED_DRIVERS; i++) {
      const data = {
        id: faker.random.uuid(),
        createdAt: faker.date.past(1),
        driversLicense: '1234567890',
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      }

      const driver = await photon.drivers.create({ data })

      SeedStore.seededDrivers.push(driver)
    }
  }

  /**
   * Seed Licenses with random data
   */
  static seedLicenses = async () => {
    const issuanceDate = faker.date.past(2)
    const expDate = faker.date.future(1, issuanceDate)

    for (let i = 0; i < N_SEED_LICENSES; i++) {
      const stateAbbr = faker.address.stateAbbr()

      const data = {
        id: faker.random.uuid(),
        address1: faker.address.streetAddress(),
        city: faker.address.city(),
        contactName: `${faker.name.firstName()} ${faker.name.lastName()}`,
        contactPhone: faker.phone.phoneNumber(),
        expirationDate: expDate,
        issuanceDate,
        legalEntityName: faker.company.companyName(),
        licenseNumber: `SEED-${faker.random.alphaNumeric(2)}-${faker.random.alphaNumeric(6)}-LIC`,
        state: stateAbbr,
        zip: faker.address.zipCode(),
        nickname: `SEED ${faker.random.words(3)}`,
        type: faker.random.arrayElement(licenseTypes),
      }

      const license = await photon.licenses.create({ data })

      SeedStore.seededLicenses.push(data)
    }
  }

  /**
   * Seed License Locations with random data
   */
  static seedLicensedLocations = async () => {
    for (let i = 0; i < N_SEED_LICENSEDLOCATIONS; i++) {
      const data = {
        id: faker.random.uuid(),
        address1: faker.address.streetAddress(),
        city: faker.address.city(),
        state: faker.address.stateAbbr(),
        zip: faker.address.zipCode(),
        name: `SEED ${faker.random.words(3)}`,
        licenses: {
          connect: {
            id: SeedStore.randomLicense().id,
          },
        },
        siteCategory: faker.random.arrayElement(siteCategories),
        lat: 0,
        lng: 0,
      }

      const licensedLocation = await photon.licensedLocations.create({ data })

      SeedStore.seededLicensedLocations.push(licensedLocation)
    }
  }

  /**
   * Seed Orders with random data
   */
  static seedOrders = async () => {
    for (let i = 0; i < N_SEED_ORDERS; i++) {
      const numberOfParagraphsInAdminNotes = Math.round(Math.random() * 2)

      const data = {
        id: faker.random.uuid(),
        name: `SEED ${faker.lorem.sentence(4, 2)}`,
        action: faker.random.arrayElement(orderActions),
        adminNotes: faker.lorem.paragraphs(numberOfParagraphsInAdminNotes),
        createdAt: faker.date.past(1),
        date: faker.date.future(1),
        creator: {
          connect: {
            id: SeedStore.randomUser().id,
          },
        },
        status: faker.random.arrayElement(orderStatuses),
        timeWindow: 'ALL DAY',
        paymentTerms: faker.random.arrayElement([
          'COD',
          'Net 7',
          'Net 14',
          'Net 15',
          'Net 30',
          'Net 45',
          'Net 60',
          'Net 90',
          'None (pickup or transfer)',
        ]),
        exciseTax: 0,
        exciseTaxCollected: 0,
        organization: {
          connect: {
            id: SeedStore.randomOrganization().id,
          },
        },
        discount: 0,
        gmv: 0,
        gmvCollected: 0,
        intaken: false,
        outtaken: false,
        isExcluded: false,
        number: faker.random.number(99999),
        paymentStatus: faker.random.arrayElement(paymentStatuses),
        isArchived: false,
        driver: {
          connect: {
            id: SeedStore.randomDriver().id,
          },
        },
        site: {
          connect: {
            id: SeedStore.randomSite().id,
          },
        },
        vehicle: {
          connect: {
            id: SeedStore.randomVehicle().id,
          },
        },
        warehouse: {
          connect: {
            id: SeedStore.randomWarehouse().id,
          },
        },
      }

      const order = await photon.orders.create({ data })

      SeedStore.seededOrders.push(order)
    }
  }

  /**
   * Seed Organizations with random data
   */
  static seedOrganizations = async () => {
    for (let i = 0; i < N_SEED_ORGANIZATIONS; i++) {
      const companyName = faker.company.companyName()
      const stateAbbr = faker.address.stateAbbr()

      const data = {
        id: faker.random.uuid(),
        address1: faker.address.streetAddress(),
        alias: companyName,
        city: faker.address.city(),
        createdAt: faker.date.past(1),
        name: companyName,
        owner: {
          connect: {
            id: SeedStore.randomUser().id,
          },
        },
        phone: faker.phone.phoneNumber(),
        state: stateAbbr,
        zip: faker.address.zipCode(),
      }

      const organization = await photon.organizations.create({ data })

      SeedStore.seededOrganizations.push(organization)
    }
  }

  /**
   * Seed Sites with random data
   */
  static seedSites = async () => {
    for (let i = 0; i < N_SEED_SITES; i++) {
      const data = {
        id: faker.random.uuid(),
        address1: faker.address.streetAddress(),
        city: faker.address.city(),
        createdAt: faker.date.past(1),
        name: `SEED ${faker.random.words(2)}`,
        pocEmail: faker.internet.email(),
        pocName: `${faker.name.firstName()} ${faker.name.lastName()}`,
        pocPhoneNumber: faker.phone.phoneNumber(),
        siteCategory: faker.random.arrayElement(siteCategories),
        state: faker.address.stateAbbr(),
        zip: faker.address.zipCode(),
        licensedLocation: {
          connect: {
            id: SeedStore.randomLicensedLocation().id,
          },
        },
        organization: {
          connect: {
            id: SeedStore.randomOrganization().id,
          },
        },
      }

      const site = await photon.sites.create({ data })

      SeedStore.seededSites.push(site)
    }
  }

  /**
   * Seed SKUs with random data
   */
  static seedSkus = async () => {
    for (let i = 0; i < N_SEED_SKUS; i++) {
      const data = {
        id: faker.random.uuid(),
        code: `SEED-${faker.random.alphaNumeric(4)}`,
        createdAt: faker.date.past(1),
        inventoryCategory: faker.random.arrayElement(inventoryCategories),
        inventoryClass: faker.random.arrayElement(inventoryClasses),
        inventoryType: faker.random.arrayElement(inventoryTypes),
        isArchived: false,
        isExciseTaxable: false,
      }

      const sku = await photon.skus.create({ data })

      SeedStore.seededSkus.push(sku)
    }
  }

  /**
   * Seed Users with random data
   */
  static seedUsers = async () => {
    let hasMadeAnAdminYet = false

    for (let i = 0; i < N_SEED_USERS; i++) {
      const _stateAbbr = faker.address.stateAbbr()

      const data = {
        id: faker.random.uuid(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        address1: faker.address.streetAddress(),
        city: faker.address.city(),
        createdAt: faker.date.past(1),
        email: `seed.${faker.internet.email()}`,
        isAdmin: !hasMadeAnAdminYet,
        password: '$2b$10$e7BwKdcvfir4//86vNoonerbd/ErVMPMi3OZTJ1sF1U0PlQVjnjj2' /* password */,
        profilePicture: faker.image.people(),
        state: _stateAbbr,
        zip: faker.address.zipCode(),
      }

      const user = await photon.users.create({ data })

      SeedStore.seededUsers.push(user)

      hasMadeAnAdminYet = true
    }

    // add a single test account when NODE_ENV === 'test'
    if (process.env.NODE_ENV !== 'production') {
      const testUser = await photon.users.upsert({
        create: {
          id: faker.random.uuid(),
          firstName: 'Test',
          lastName: 'Testington',
          email: 'test@test.com',
          password: '$2b$10$e7BwKdcvfir4//86vNoonerbd/ErVMPMi3OZTJ1sF1U0PlQVjnjj2' /* password */,
        },
        update: {},
        where: {
          email: 'test@test.com',
        },
      })

      SeedStore.seededUsers.push(testUser)
    }
  }

  /**
   * Seed Vehicles with random data
   */
  static seedVehicles = async () => {
    for (let i = 0; i < N_SEED_VEHICLES; i++) {
      const data = {
        id: faker.random.uuid(),
        createdAt: faker.date.past(1),
        licensePlate: `${faker.random.alphaNumeric(3)}-${faker.random.alphaNumeric(4)}`,
        make: faker.random.arrayElement(['Toyota', 'Mercedes', 'Honda']),
        model: faker.random.arrayElement(['Hiace', 'Sprinter', 'Actvy']),
        name: `SEED ${faker.random.words(2)}`,
        year: faker.date.past(5).getFullYear(),
      }

      const vehicle = await photon.vehicles.create({ data })

      SeedStore.seededVehicles.push(vehicle)
    }
  }

  /**
   * Seed Warehouses with real-world data
   */
  static seedWarehouses = async () => {
    const laData = {
      id: faker.random.uuid(),
      createdAt: faker.date.past(1),
      license: {
        connect: {
          id: SeedStore.randomLicense().id,
        },
      },
      site: {
        connect: {
          id: SeedStore.randomSite().id,
        },
      },
      isArchived: false,
    }

    const oakData = {
      id: faker.random.uuid(),
      createdAt: faker.date.past(1),
      license: {
        connect: {
          id: SeedStore.randomLicense().id,
        },
      },
      site: {
        connect: {
          id: SeedStore.randomSite().id,
        },
      },
      isArchived: false,
    }

    const laWarehouse = await photon.warehouses.create({ data: laData })
    const oakWarehouse = await photon.warehouses.create({ data: oakData })

    SeedStore.seededWarehouses.push(...[laWarehouse, oakWarehouse])
  }

  static seedDB = async (opts?: SeedOptions) => {
    if (opts && opts.seed) {
      try {
        console.log(`🌱  Seed provided. Using \`${opts.seed}' to generate test data.`)
        faker.seed(Number(opts.seed))
      } catch (e) {
        console.error(`🛑  Seeding failed.`)
        console.error(e)
      }
    }

    try {
      await SeedStore.seedUsers()
      await SeedStore.seedOrganizations()

      await SeedStore.seedLicenses()
      await SeedStore.seedLicensedLocations()

      await SeedStore.seedSkus()
      await SeedStore.seedBatchs()
      await SeedStore.seedSites()
      await SeedStore.seedWarehouses()

      await SeedStore.seedVehicles()
      await SeedStore.seedDrivers()
      await SeedStore.seedOrders()
    } catch (e) {
      console.error('🛑  Seeding failed. Your DB may need to be reset.')
    }
  }

  static cleanupPhoton = async () => {
    photon.disconnect()
  }
}

async function main() {
  const userProvidedSeed = process.argv[2]
  if (userProvidedSeed) {
    await SeedStore.seedDB({ seed: Number(userProvidedSeed) })
    await SeedStore.cleanupPhoton()
  }
}

main().finally(async () => {})
