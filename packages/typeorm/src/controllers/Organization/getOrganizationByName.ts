import { Organization } from '../../entities/Organization'

export const getOrganizationByName = async (name: string) => Organization.findOneOrFail({ where: { name } })
