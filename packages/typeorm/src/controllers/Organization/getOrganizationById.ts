import { Organization } from '../../entities/Organization'

export const getOrganizationById = async (id: string) => Organization.findOneOrFail({ id })
