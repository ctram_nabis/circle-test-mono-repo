import { SkuBatch } from '../../entities/SkuBatch'

export const getSkuBatchBySkuId = async (skuId: string) => SkuBatch.find({ where: { skuId } })
