import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { SoftDeletableEntity } from '../utils/SoftDeleteEntity'
import { Batch } from './Batch'
import { InventoryItem } from './InventoryItem'
import { LineItem } from './LineItem'
import { Organization } from './Organization'
import { SkuBatch } from './SkuBatch'

@Entity('Sku', { schema: 'public' })
export class Sku extends SoftDeletableEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'code',
  })
  code: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.skus, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((Sku: Sku) => Sku.organization)
  organizationId: Promise<string[]>

  @Column('enum', {
    nullable: false,
    default: () => 'CANNABIS',
    enum: ['CANNABIS', 'NON_CANNABIS'],
    name: 'inventoryType',
  })
  inventoryType: string

  @Column('enum', {
    nullable: false,
    default: () => 'FLOWER',
    enum: ['CARTRIDGE', 'CONCENTRATE', 'DISPOSABLE', 'EDIBLES', 'FLOWER', 'OTHER', 'PRE_ROLL', 'TOPICAL'],
    name: 'inventoryClass',
  })
  inventoryClass: string

  @Column('enum', {
    nullable: false,
    default: () => 'BUD',
    enum: [
      'BROWNIE',
      'BUD',
      'BUTANE_HASH_OIL',
      'BUTTER',
      'CHIPS',
      'CO2_OIL',
      'CONCENTRATE',
      'COOKIE',
      'CREAM',
      'DISSOLVABLE_POWDER',
      'GREENHOUSE',
      'GUMMY',
      'HARD_CANDY',
      'HONEY',
      'ICE_CREAM',
      'INDOOR',
      'INFUSED_PRE_ROLLS',
      'INHALER',
      'JUICE',
      'LEAFLETTES',
      'LIP_BALM',
      'MARSHMALLOW',
      'MINT',
      'MIXED',
      'OTHER',
      'OTHER_BATTERY_TYPES',
      'PATCH',
      'PAX',
      'PEN',
      'PRE_ROLLS',
      'SHAKE',
      'SODA',
      'SOLVENTLESS',
      'SPRAY',
      'SUBLINGUAL',
      'SUN_GROWN',
      'TAFFY',
      'TEA',
      'THREAD_510',
      'TINCTURES',
      'TRANSDERMAL',
      'TRIM',
      'WATER',
    ],
    name: 'inventoryCategory',
  })
  inventoryCategory: string

  @Column('boolean', {
    nullable: true,
    name: 'isArchived',
  })
  isArchived: boolean | null

  @Column('boolean', {
    nullable: true,
    name: 'isExciseTaxable',
  })
  isExciseTaxable: boolean | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'unit',
  })
  unit: string | null

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'pricePerUnit',
  })
  pricePerUnit: number | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'name',
  })
  name: string | null

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'deletedAt',
  })
  deletedAt: Date | null

  @OneToMany(() => Batch, (Batch: Batch) => Batch.sku, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  batchs: Promise<Batch[]>

  @OneToMany(() => InventoryItem, (InventoryItem: InventoryItem) => InventoryItem.sku, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  inventoryItems: Promise<InventoryItem[]>

  @OneToMany(() => LineItem, (LineItem: LineItem) => LineItem.sku, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  lineItems: Promise<LineItem[]>

  @OneToMany(() => SkuBatch, (SkuBatch: SkuBatch) => SkuBatch.sku, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  skuBatchs: Promise<SkuBatch[]>

  constructor(init?: Partial<Sku>) {
    super()
    Object.assign(this, init)
  }
}
