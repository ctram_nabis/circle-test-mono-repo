import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { LineItemChange } from './LineItemChange'
import { Order } from './Order'
import { Organization } from './Organization'
import { User } from './User'

@Entity('OrderChange', { schema: 'public' })
export class OrderChange extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'description',
  })
  description: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Order, (Order: Order) => Order.orderChanges, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'orderId' })
  order: Promise<Order | null>

  @RelationId((OrderChange: OrderChange) => OrderChange.order)
  orderId: Promise<string[]>

  @ManyToOne(() => User, (User: User) => User.orderChanges, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'submitterId' })
  submitter: Promise<User | null>

  @RelationId((OrderChange: OrderChange) => OrderChange.submitter)
  submitterId: Promise<string[]>

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.orderChanges, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((OrderChange: OrderChange) => OrderChange.organization)
  organizationId: Promise<string[]>

  @OneToMany(() => LineItemChange, (LineItemChange: LineItemChange) => LineItemChange.orderChange, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  lineItemChanges: Promise<LineItemChange[]>

  constructor(init?: Partial<OrderChange>) {
    super()
    Object.assign(this, init)
  }
}
