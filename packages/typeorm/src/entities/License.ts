import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Batch } from './Batch'
import { LicensedLocation } from './LicensedLocation'
import { Organization } from './Organization'
import { Site } from './Site'
import { Warehouse } from './Warehouse'

@Entity('License', { schema: 'public' })
export class License extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'nickname',
  })
  nickname: string

  @Column('enum', {
    nullable: true,
    default: () => 'ADULT_USE',
    enum: ['ADULT_USE', 'COMBINED', 'MEDICINAL'],
    name: 'category',
  })
  category: string | null

  @Column('enum', {
    nullable: false,
    default: () => 'MANUFACTURER',
    enum: [
      'CANNABIS_EVENTS',
      'CULTIVATOR',
      'DISTRIBUTOR',
      'DISTRIBUTOR_TRANSPORT_ONLY',
      'MANUFACTURER',
      'MICROBUSINESS',
      'RETAILER',
      'RETAILER_NONSTOREFRONT',
      'TESTING',
      'TESTING_LABORATORY',
    ],
    name: 'type',
  })
  type: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'licenseNumber',
  })
  licenseNumber: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'legalEntityName',
  })
  legalEntityName: string

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'issuanceDate',
  })
  issuanceDate: Date | null

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'expirationDate',
  })
  expirationDate: Date | null

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'contactName',
  })
  contactName: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'contactPhone',
  })
  contactPhone: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'address1',
  })
  address1: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'address2',
  })
  address2: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'city',
  })
  city: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'state',
  })
  state: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'zip',
  })
  zip: string | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.licenses, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((License: License) => License.organization)
  organizationId: Promise<string[]>

  @ManyToOne(() => Site, (Site: Site) => Site.licenses, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'siteId' })
  site: Promise<Site | null>

  @RelationId((License: License) => License.site)
  siteId: Promise<string[]>

  @ManyToOne(() => LicensedLocation, (LicensedLocation: LicensedLocation) => LicensedLocation.licenses, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'licensedLocationId' })
  licensedLocation: Promise<LicensedLocation | null>

  @RelationId((License: License) => License.licensedLocation)
  licensedLocationId: Promise<string[]>

  @Column('text', {
    nullable: true,
    name: 'adminNotes',
  })
  adminNotes: string | null

  @OneToMany(() => Batch, (Batch: Batch) => Batch.license, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  batchs: Promise<Batch[]>

  @OneToMany(() => Warehouse, (Warehouse: Warehouse) => Warehouse.license, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  warehouses: Promise<Warehouse[]>

  constructor(init?: Partial<License>) {
    super()
    Object.assign(this, init)
  }
}
