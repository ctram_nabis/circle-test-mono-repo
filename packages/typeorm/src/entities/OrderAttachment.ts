import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Attachment } from './Attachment'
import { Order } from './Order'

@Entity('OrderAttachment', { schema: 'public' })
export class OrderAttachment extends BaseEntity {
  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Order, (Order: Order) => Order.orderAttachments, {
    nullable: false,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'orderId' })
  order: Promise<Order | null>

  @RelationId((OrderAttachment: OrderAttachment) => OrderAttachment.order)
  orderId: Promise<string[]>

  @ManyToOne(() => Attachment, (Attachment: Attachment) => Attachment.orderAttachments, {
    nullable: false,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'attachmentId' })
  attachment: Promise<Attachment | null>

  @RelationId((OrderAttachment: OrderAttachment) => OrderAttachment.attachment)
  attachmentId: Promise<string[]>

  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  constructor(init?: Partial<OrderAttachment>) {
    super()
    Object.assign(this, init)
  }
}
