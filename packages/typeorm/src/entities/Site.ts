import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { License } from './License'
import { LicensedLocation } from './LicensedLocation'
import { Order } from './Order'
import { Organization } from './Organization'
import { Warehouse } from './Warehouse'

@Entity('Site', { schema: 'public' })
export class Site extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'name',
  })
  name: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'address1',
  })
  address1: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'address2',
  })
  address2: string | null

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'city',
  })
  city: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'state',
  })
  state: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'zip',
  })
  zip: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.sites, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((Site: Site) => Site.organization)
  organizationId: Promise<string[]>

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'pocName',
  })
  pocName: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'pocPhoneNumber',
  })
  pocPhoneNumber: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'pocEmail',
  })
  pocEmail: string | null

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'lat',
  })
  lat: number | null

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'lng',
  })
  lng: number | null

  @Column('enum', {
    nullable: false,
    default: () => 'RETAILER',
    enum: ['CONSUMER', 'CULTIVATOR', 'DISTRIBUTOR', 'MANUFACTURER', 'MICROBUSINESS', 'OTHER', 'RETAILER'],
    name: 'siteCategory',
  })
  siteCategory: string

  @ManyToOne(() => LicensedLocation, (LicensedLocation: LicensedLocation) => LicensedLocation.sites, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'licensedLocationId' })
  licensedLocation: Promise<LicensedLocation | null>

  @RelationId((Site: Site) => Site.licensedLocation)
  licensedLocationId: Promise<string[]>

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'adminNotes',
  })
  adminNotes: string | null

  @OneToMany(() => License, (License: License) => License.site, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  licenses: Promise<License[]>

  @OneToMany(() => Order, (Order: Order) => Order.site, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  orders: Promise<Order[]>

  @OneToMany(() => Warehouse, (Warehouse: Warehouse) => Warehouse.site, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  warehouses: Promise<Warehouse[]>

  constructor(init?: Partial<Site>) {
    super()
    Object.assign(this, init)
  }
}
