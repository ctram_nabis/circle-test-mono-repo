import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Organization } from './Organization'
import { User } from './User'

@Entity('Draft', { schema: 'public' })
export class Draft extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('json', {
    nullable: true,
    name: 'body',
  })
  body: object | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => User, (User: User) => User.drafts, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'creatorId' })
  creator: Promise<User | null>

  @RelationId((Draft: Draft) => Draft.creator)
  creatorId: Promise<string[]>

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.drafts, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((Draft: Draft) => Draft.organization)
  organizationId: Promise<string[]>

  constructor(init?: Partial<Draft>) {
    super()
    Object.assign(this, init)
  }
}
