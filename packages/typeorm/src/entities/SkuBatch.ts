import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { SoftDeletableEntity } from '../utils/SoftDeleteEntity'
import { Batch } from './Batch'
import { InventoryItem } from './InventoryItem'
import { LineItem } from './LineItem'
import { Sku } from './Sku'

@Entity('SkuBatch', { schema: 'public' })
@Index('SkuBatch_skuId_batchId_key', ['batch', 'sku'], { unique: true })
export class SkuBatch extends SoftDeletableEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @ManyToOne(() => Sku, (Sku: Sku) => Sku.skuBatchs, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'skuId' })
  sku: Promise<Sku | null>

  @RelationId((SkuBatch: SkuBatch) => SkuBatch.sku)
  skuId: Promise<string[]>

  @ManyToOne(() => Batch, (Batch: Batch) => Batch.skuBatchs, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'batchId' })
  batch: Promise<Batch | null>

  @RelationId((SkuBatch: SkuBatch) => SkuBatch.batch)
  batchId: Promise<string[]>

  @Column('timestamp with time zone', {
    nullable: true,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date | null

  @Column('timestamp with time zone', {
    nullable: true,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date | null

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'isLocked',
  })
  isLocked: boolean

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'deletedAt',
  })
  deletedAt: Date | null

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'isArchived',
  })
  isArchived: boolean

  @OneToMany(() => InventoryItem, (InventoryItem: InventoryItem) => InventoryItem.skuBatch, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  inventoryItems: Promise<InventoryItem[]>

  @OneToMany(() => LineItem, (LineItem: LineItem) => LineItem.skuBatch, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  lineItems: Promise<LineItem[]>

  constructor(init?: Partial<SkuBatch>) {
    super()
    Object.assign(this, init)
  }
}
