import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Attachment } from './Attachment'
import { Batch } from './Batch'
import { Draft } from './Draft'
import { InventoryItem } from './InventoryItem'
import { InventoryLedgerItem } from './InventoryLedgerItem'
import { License } from './License'
import { LineItem } from './LineItem'
import { Order } from './Order'
import { OrderChange } from './OrderChange'
import { OrderSubscriber } from './OrderSubscriber'
import { OrganizationInvite } from './OrganizationInvite'
import { OrganizationOrganization } from './OrganizationOrganization'
import { OrganizationUser } from './OrganizationUser'
import { Role } from './Role'
import { Site } from './Site'
import { Sku } from './Sku'
import { User } from './User'
import { UserRole } from './UserRole'

@Entity('Organization', { schema: 'public' })
@Index('Organization_alias_key', ['alias'], { unique: true })
@Index('Organization_name_key', ['name'], { unique: true })
export class Organization extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    unique: true,
    length: 255,
    name: 'name',
  })
  name: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'address1',
  })
  address1: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'address2',
  })
  address2: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'city',
  })
  city: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'state',
  })
  state: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'zip',
  })
  zip: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'phone',
  })
  phone: string | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => User, (User: User) => User.organizations, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'ownerId' })
  owner: Promise<User | null>

  @RelationId((Organization: Organization) => Organization.owner)
  ownerId: Promise<string[]>

  @Column('character varying', {
    nullable: false,
    unique: true,
    length: 255,
    name: 'alias',
  })
  alias: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'manifestGDriveFolderId',
  })
  manifestGDriveFolderId: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'doingBusinessAs',
  })
  doingBusinessAs: string | null

  @OneToMany(() => Attachment, (Attachment: Attachment) => Attachment.organization, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  attachments: Promise<Attachment[]>

  @OneToMany(() => Batch, (Batch: Batch) => Batch.organization, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  batchs: Promise<Batch[]>

  @OneToMany(() => Draft, (Draft: Draft) => Draft.organization, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  drafts: Promise<Draft[]>

  @OneToMany(() => InventoryItem, (InventoryItem: InventoryItem) => InventoryItem.organization, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  inventoryItems: Promise<InventoryItem[]>

  @OneToMany(
    () => InventoryLedgerItem,
    (InventoryLedgerItem: InventoryLedgerItem) => InventoryLedgerItem.organization,
    { onDelete: 'SET NULL', onUpdate: 'CASCADE' },
  )
  inventoryLedgerItems: Promise<InventoryLedgerItem[]>

  @OneToMany(() => License, (License: License) => License.organization, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  licenses: Promise<License[]>

  @OneToMany(() => LineItem, (LineItem: LineItem) => LineItem.organization, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  lineItems: Promise<LineItem[]>

  @OneToMany(() => Order, (Order: Order) => Order.organization, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  orders: Promise<Order[]>

  @OneToMany(() => OrderChange, (OrderChange: OrderChange) => OrderChange.organization, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  orderChanges: Promise<OrderChange[]>

  @OneToMany(() => OrderSubscriber, (OrderSubscriber: OrderSubscriber) => OrderSubscriber.subscriber, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  orderSubscribers: Promise<OrderSubscriber[]>

  @OneToMany(() => OrganizationInvite, (OrganizationInvite: OrganizationInvite) => OrganizationInvite.organization, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  organizationInvites: Promise<OrganizationInvite[]>

  @OneToMany(
    () => OrganizationOrganization,
    (OrganizationOrganization: OrganizationOrganization) => OrganizationOrganization.suborganization,
    { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
  )
  organizationOrganizations: Promise<OrganizationOrganization[]>

  @OneToMany(
    () => OrganizationOrganization,
    (OrganizationOrganization: OrganizationOrganization) => OrganizationOrganization.superorganization,
    { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
  )
  organizationOrganizations2: Promise<OrganizationOrganization[]>

  @OneToMany(() => OrganizationUser, (OrganizationUser: OrganizationUser) => OrganizationUser.organization, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  organizationUsers: Promise<OrganizationUser[]>

  @OneToMany(() => Role, (Role: Role) => Role.organization, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  roles: Promise<Role[]>

  @OneToMany(() => Site, (Site: Site) => Site.organization, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  sites: Promise<Site[]>

  @OneToMany(() => Sku, (Sku: Sku) => Sku.organization, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  skus: Promise<Sku[]>

  @OneToMany(() => UserRole, (UserRole: UserRole) => UserRole.organization, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  userRoles: Promise<UserRole[]>

  constructor(init?: Partial<Organization>) {
    super()
    Object.assign(this, init)
  }
}
