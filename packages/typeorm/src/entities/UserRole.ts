import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Organization } from './Organization'
import { Role } from './Role'
import { User } from './User'

@Entity('UserRole', { schema: 'public' })
@Index('UserRoles_userId_roleId_key', ['role', 'user'], { unique: true })
export class UserRole extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => User, (User: User) => User.userRoles, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'userId' })
  user: Promise<User | null>

  @RelationId((UserRole: UserRole) => UserRole.user)
  userId: Promise<string[]>

  @ManyToOne(() => Role, (Role: Role) => Role.userRoles, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'roleId' })
  role: Promise<Role | null>

  @RelationId((UserRole: UserRole) => UserRole.role)
  roleId: Promise<string[]>

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.userRoles, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((UserRole: UserRole) => UserRole.organization)
  organizationId: Promise<string[]>

  constructor(init?: Partial<UserRole>) {
    super()
    Object.assign(this, init)
  }
}
