import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { SoftDeletableEntity } from '../utils/SoftDeleteEntity'
import { InventoryItem } from './InventoryItem'
import { License } from './License'
import { LineItem } from './LineItem'
import { Organization } from './Organization'
import { Sku } from './Sku'
import { SkuBatch } from './SkuBatch'

@Entity('Batch', { schema: 'public' })
export class Batch extends SoftDeletableEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'code',
  })
  code: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'manifestGDriveFileId',
  })
  manifestGDriveFileId: string

  @Column('timestamp with time zone', {
    nullable: true,
    default: () => "'2019-03-06 03:40:43.208+00'",
    name: 'manufacturingDate',
  })
  manufacturingDate: Date | null

  @Column('timestamp with time zone', {
    nullable: true,
    default: () => "'2019-03-06 03:40:43.208+00'",
    name: 'expirationDate',
  })
  expirationDate: Date | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(
    () => Organization,
    (Organization: Organization) => Organization.batchs,
    {
      onDelete: 'SET NULL',
      onUpdate: 'CASCADE',
    },
  )
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((Batch: Batch) => Batch.organization)
  organizationId: Promise<string[]>

  @ManyToOne(
    () => License,
    (License: License) => License.batchs,
    { onDelete: 'SET NULL', onUpdate: 'CASCADE' },
  )
  @JoinColumn({ name: 'licenseId' })
  license: Promise<License | null>

  @RelationId((Batch: Batch) => Batch.license)
  licenseId: Promise<string[]>

  @Column('enum', {
    nullable: true,
    enum: ['FAILED', 'PASSED', 'QUARANTINE', 'SAMPLED'],
    name: 'status',
  })
  status: string | null

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'testDate',
  })
  testDate: Date | null

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'deletedAt',
  })
  deletedAt: Date | null

  @ManyToOne(
    () => Sku,
    (Sku: Sku) => Sku.batchs,
    { onDelete: 'SET NULL', onUpdate: 'CASCADE' },
  )
  @JoinColumn({ name: 'skuId' })
  sku: Promise<Sku | null>

  @RelationId((Batch: Batch) => Batch.sku)
  skuId: Promise<string[]>

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'isArchived',
  })
  isArchived: boolean

  @OneToMany(
    () => InventoryItem,
    (InventoryItem: InventoryItem) => InventoryItem.batch,
    {
      onDelete: 'SET NULL',
      onUpdate: 'CASCADE',
    },
  )
  inventoryItems: Promise<InventoryItem[]>

  @OneToMany(
    () => LineItem,
    (LineItem: LineItem) => LineItem.batch,
    { onDelete: 'SET NULL', onUpdate: 'CASCADE' },
  )
  lineItems: Promise<LineItem[]>

  @OneToMany(
    () => SkuBatch,
    (SkuBatch: SkuBatch) => SkuBatch.batch,
    { onDelete: 'SET NULL', onUpdate: 'CASCADE' },
  )
  skuBatchs: Promise<SkuBatch[]>

  constructor(init?: Partial<Batch>) {
    super()
    Object.assign(this, init)
  }
}
