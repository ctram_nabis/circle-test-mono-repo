import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Driver } from './Driver'
import { InventoryLedgerItem } from './InventoryLedgerItem'
import { LineItem } from './LineItem'
import { OrderAttachment } from './OrderAttachment'
import { OrderChange } from './OrderChange'
import { OrderSubscriber } from './OrderSubscriber'
import { Organization } from './Organization'
import { Site } from './Site'
import { User } from './User'
import { Vehicle } from './Vehicle'
import { Warehouse } from './Warehouse'

@Entity('Order', { schema: 'public' })
export class Order extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'name',
  })
  name: string

  @Column('text', {
    nullable: true,
    name: 'notes',
  })
  notes: string | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Driver, (Driver: Driver) => Driver.orders, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'driverId' })
  driver: Promise<Driver | null>

  @RelationId((Order: Order) => Order.driver)
  driverId: Promise<string[]>

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.orders, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((Order: Order) => Order.organization)
  organizationId: Promise<string[]>

  @ManyToOne(() => Vehicle, (Vehicle: Vehicle) => Vehicle.orders, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'vehicleId' })
  vehicle: Promise<Vehicle | null>

  @RelationId((Order: Order) => Order.vehicle)
  vehicleId: Promise<string[]>

  @Column('double precision', {
    nullable: false,
    default: () => '0.0',
    precision: 53,
    name: 'gmv',
  })
  gmv: number

  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'number',
  })
  number: number

  @Column('timestamp with time zone', {
    nullable: true,
    default: () => "'2018-03-26 08:54:30.244+00'",
    name: 'date',
  })
  date: Date | null

  @Column('double precision', {
    nullable: false,
    default: () => '0',
    precision: 53,
    name: 'exciseTax',
  })
  exciseTax: number

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'timeWindow',
  })
  timeWindow: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'paymentTerms',
  })
  paymentTerms: string | null

  @ManyToOne(() => Site, (Site: Site) => Site.orders, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'siteId' })
  site: Promise<Site | null>

  @RelationId((Order: Order) => Order.site)
  siteId: Promise<string[]>

  @Column('text', {
    nullable: true,
    name: 'adminNotes',
  })
  adminNotes: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'manifestGDriveFileId',
  })
  manifestGDriveFileId: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'orgLicenseNum',
  })
  orgLicenseNum: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'siteLicenseNum',
  })
  siteLicenseNum: string | null

  @ManyToOne(() => Warehouse, (Warehouse: Warehouse) => Warehouse.orders, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'warehouseId' })
  warehouse: Promise<Warehouse | null>

  @RelationId((Order: Order) => Order.warehouse)
  warehouseId: Promise<string[]>

  @ManyToOne(() => User, (User: User) => User.orders2, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'creatorId' })
  creator: Promise<User | null>

  @RelationId((Order: Order) => Order.creator)
  creatorId: Promise<string[]>

  @Column('double precision', {
    nullable: false,
    default: () => '0.00',
    precision: 53,
    name: 'discount',
  })
  discount: number

  @Column('double precision', {
    nullable: false,
    default: () => '0',
    precision: 53,
    name: 'gmvCollected',
  })
  gmvCollected: number

  @Column('double precision', {
    nullable: false,
    default: () => '0',
    precision: 53,
    name: 'exciseTaxCollected',
  })
  exciseTaxCollected: number

  @Column('enum', {
    nullable: false,
    default: () => 'UNPAID',
    enum: ['COD_PAID', 'NET_TERMS_PAID', 'REMITTED', 'SELF_COLLECTED', 'UNPAID'],
    name: 'paymentStatus',
  })
  paymentStatus: string

  @Column('enum', {
    nullable: true,
    default: () => 'UNSCHEDULED',
    enum: [
      'APPROVAL_NEEDED',
      'CANCELLED',
      'DELAYED',
      'DELIVERED',
      'DELIVERED_WITH_EDITS',
      'DRAFT',
      'REJECTED',
      'SCHEDULED',
      'TRANSFERRING',
      'UNSCHEDULED',
    ],
    name: 'status',
  })
  status: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'irn',
  })
  irn: string | null

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'estimatedArrivalTimeBefore',
  })
  estimatedArrivalTimeBefore: Date | null

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'estimatedArrivalTimeAfter',
  })
  estimatedArrivalTimeAfter: Date | null

  @Column('integer', {
    nullable: true,
    name: 'daysTillPaymentDue',
  })
  daysTillPaymentDue: number | null

  @ManyToOne(() => Order, (Order: Order) => Order.orders, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'collectWithOrderId' })
  collectWithOrder: Promise<Order | null>

  @RelationId((Order: Order) => Order.collectWithOrder)
  collectWithOrderId: Promise<string[]>

  @Column('boolean', {
    nullable: true,
    name: 'isArchived',
  })
  isArchived: boolean | null

  @Column('uuid', {
    nullable: true,
    name: 'licensedLocationId',
  })
  licensedLocationId: string | null

  @Column('enum', {
    nullable: false,
    default: () => 'DELIVERY_TO_RETAILER',
    enum: [
      'DELIVERY_TO_BRAND',
      'DELIVERY_TO_RETAILER',
      'DROPOFF_TO_NABIS',
      'INTERNAL_TRANSFER',
      'OTHER',
      'PAYMENT_PICKUP_FROM_RETAILER',
      'PICKUP_FROM_BRAND',
      'PICKUP_FROM_NABIS',
      'RETURN_FROM_RETAILER',
    ],
    name: 'action',
  })
  action: string

  @ManyToOne(() => User, (User: User) => User.orders, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'assigneeId' })
  assignee: Promise<User | null>

  @RelationId((Order: Order) => Order.assignee)
  assigneeId: Promise<string[]>

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'intaken',
  })
  intaken: boolean

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'outtaken',
  })
  outtaken: boolean

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'isExcluded',
  })
  isExcluded: boolean

  @OneToMany(() => InventoryLedgerItem, (InventoryLedgerItem: InventoryLedgerItem) => InventoryLedgerItem.order, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  inventoryLedgerItems: Promise<InventoryLedgerItem[]>

  @OneToMany(() => LineItem, (LineItem: LineItem) => LineItem.order, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  lineItems: Promise<LineItem[]>

  @OneToMany(() => Order, (Order: Order) => Order.collectWithOrder, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  orders: Promise<Order[]>

  @OneToMany(() => OrderAttachment, (OrderAttachment: OrderAttachment) => OrderAttachment.order, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  orderAttachments: Promise<OrderAttachment[]>

  @OneToMany(() => OrderChange, (OrderChange: OrderChange) => OrderChange.order, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  orderChanges: Promise<OrderChange[]>

  @OneToMany(() => OrderSubscriber, (OrderSubscriber: OrderSubscriber) => OrderSubscriber.order, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  orderSubscribers: Promise<OrderSubscriber[]>

  constructor(init?: Partial<Order>) {
    super()
    Object.assign(this, init)
  }
}
