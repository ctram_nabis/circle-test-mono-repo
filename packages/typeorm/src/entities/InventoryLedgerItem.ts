import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { InventoryItem } from './InventoryItem'
import { Order } from './Order'
import { Organization } from './Organization'
import { User } from './User'

@Entity('InventoryLedgerItem', { schema: 'public' })
export class InventoryLedgerItem extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('double precision', {
    nullable: false,
    default: () => '0',
    precision: 53,
    name: 'startCounted',
  })
  startCounted: number

  @Column('double precision', {
    nullable: false,
    default: () => '0',
    precision: 53,
    name: 'endCounted',
  })
  endCounted: number

  @Column('text', {
    nullable: true,
    name: 'notes',
  })
  notes: string | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => InventoryItem, (InventoryItem: InventoryItem) => InventoryItem.inventoryLedgerItems, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'inventoryItemId' })
  inventoryItem: Promise<InventoryItem | null>

  @RelationId((InventoryLedgerItem: InventoryLedgerItem) => InventoryLedgerItem.inventoryItem)
  inventoryItemId: Promise<string[]>

  @ManyToOne(() => Order, (Order: Order) => Order.inventoryLedgerItems, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'orderId' })
  order: Promise<Order | null>

  @RelationId((InventoryLedgerItem: InventoryLedgerItem) => InventoryLedgerItem.order)
  orderId: Promise<string[]>

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.inventoryLedgerItems, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((InventoryLedgerItem: InventoryLedgerItem) => InventoryLedgerItem.organization)
  organizationId: Promise<string[]>

  @Column('enum', {
    nullable: false,
    default: () => 'ALLOCATION',
    enum: ['ALLOCATION', 'CORRECTION', 'INTAKE', 'OUTTAKE'],
    name: 'transactionType',
  })
  transactionType: string

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'startAllocated',
  })
  startAllocated: number | null

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'endAllocated',
  })
  endAllocated: number | null

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'startPromised',
  })
  startPromised: number | null

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'endPromised',
  })
  endPromised: number | null

  @ManyToOne(() => User, (User: User) => User.inventoryLedgerItems, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'creatorId' })
  creator: Promise<User | null>

  @RelationId((InventoryLedgerItem: InventoryLedgerItem) => InventoryLedgerItem.creator)
  creatorId: Promise<string[]>

  constructor(init?: Partial<InventoryLedgerItem>) {
    super()
    Object.assign(this, init)
  }
}
