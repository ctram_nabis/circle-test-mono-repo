import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { OrderAttachment } from './OrderAttachment'
import { Organization } from './Organization'

@Entity('Attachment', { schema: 'public' })
export class Attachment extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'name',
  })
  name: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'gDriveFileId',
  })
  gDriveFileId: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.attachments, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((Attachment: Attachment) => Attachment.organization)
  organizationId: Promise<string[]>

  @OneToMany(() => OrderAttachment, (OrderAttachment: OrderAttachment) => OrderAttachment.attachment, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  orderAttachments: Promise<OrderAttachment[]>

  constructor(init?: Partial<Attachment>) {
    super()
    Object.assign(this, init)
  }
}
