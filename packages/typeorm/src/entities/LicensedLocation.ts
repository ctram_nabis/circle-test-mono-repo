import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { License } from './License'
import { Site } from './Site'

@Entity('LicensedLocation', { schema: 'public' })
export class LicensedLocation extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'name',
  })
  name: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'address1',
  })
  address1: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'address2',
  })
  address2: string | null

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'city',
  })
  city: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'state',
  })
  state: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'zip',
  })
  zip: string

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'lat',
  })
  lat: number | null

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'lng',
  })
  lng: number | null

  @Column('enum', {
    nullable: false,
    default: () => 'RETAILER',
    enum: ['CONSUMER', 'CULTIVATOR', 'DISTRIBUTOR', 'MANUFACTURER', 'MICROBUSINESS', 'OTHER', 'RETAILER'],
    name: 'siteCategory',
  })
  siteCategory: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @Column('uuid', {
    nullable: true,
    name: 'warehouseId',
  })
  warehouseId: string | null

  @Column('enum', {
    nullable: true,
    enum: ['REJECTED', 'UNVERIFIED', 'VERIFIED'],
    name: 'verificationStatus',
  })
  verificationStatus: string | null

  @Column('uuid', {
    nullable: true,
    name: 'submittedById',
  })
  submittedById: string | null

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'verificationAt',
  })
  verificationAt: Date | null

  @OneToMany(() => License, (License: License) => License.licensedLocation, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  licenses: Promise<License[]>

  @OneToMany(() => Site, (Site: Site) => Site.licensedLocation, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  sites: Promise<Site[]>

  constructor(init?: Partial<LicensedLocation>) {
    super()
    Object.assign(this, init)
  }
}
