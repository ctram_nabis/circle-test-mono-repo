import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Organization } from './Organization'

@Entity('OrganizationOrganization', { schema: 'public' })
@Index('OrganizationOrganization_superorganizationId_suborganizatio_key', ['suborganization', 'superorganization'], {
  unique: true,
})
export class OrganizationOrganization extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.organizationOrganizations2, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'superorganizationId' })
  superorganization: Promise<Organization | null>

  @RelationId((OrganizationOrganization: OrganizationOrganization) => OrganizationOrganization.superorganization)
  superorganizationId: Promise<string[]>

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.organizationOrganizations, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'suborganizationId' })
  suborganization: Promise<Organization | null>

  @RelationId((OrganizationOrganization: OrganizationOrganization) => OrganizationOrganization.suborganization)
  suborganizationId: Promise<string[]>

  constructor(init?: Partial<OrganizationOrganization>) {
    super()
    Object.assign(this, init)
  }
}
