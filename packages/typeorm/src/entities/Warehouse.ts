import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { InventoryItem } from './InventoryItem'
import { License } from './License'
import { Order } from './Order'
import { Site } from './Site'

@Entity('Warehouse', { schema: 'public' })
export class Warehouse extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Site, (Site: Site) => Site.warehouses, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'siteId' })
  site: Promise<Site | null>

  @RelationId((Warehouse: Warehouse) => Warehouse.site)
  siteId: Promise<string[]>

  @ManyToOne(() => License, (License: License) => License.warehouses, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'licenseId' })
  license: Promise<License | null>

  @RelationId((Warehouse: Warehouse) => Warehouse.license)
  licenseId: Promise<string[]>

  @Column('boolean', {
    nullable: true,
    name: 'isArchived',
  })
  isArchived: boolean | null

  @OneToMany(() => InventoryItem, (InventoryItem: InventoryItem) => InventoryItem.warehouse, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  inventoryItems: Promise<InventoryItem[]>

  @OneToMany(() => Order, (Order: Order) => Order.warehouse, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  orders: Promise<Order[]>

  constructor(init?: Partial<Warehouse>) {
    super()
    Object.assign(this, init)
  }
}
