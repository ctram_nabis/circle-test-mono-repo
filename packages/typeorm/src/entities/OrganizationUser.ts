import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Organization } from './Organization'
import { User } from './User'

@Entity('OrganizationUser', { schema: 'public' })
@Index('OrganizationUsers_userId_organizationId_key', ['organization', 'user'], { unique: true })
export class OrganizationUser extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => User, (User: User) => User.organizationUsers, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'userId' })
  user: Promise<User | null>

  @RelationId((OrganizationUser: OrganizationUser) => OrganizationUser.user)
  userId: Promise<string[]>

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.organizationUsers, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((OrganizationUser: OrganizationUser) => OrganizationUser.organization)
  organizationId: Promise<string[]>

  constructor(init?: Partial<OrganizationUser>) {
    super()
    Object.assign(this, init)
  }
}
