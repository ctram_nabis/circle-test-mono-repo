import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { SoftDeletableEntity } from '../utils/SoftDeleteEntity'
import { Batch } from './Batch'
import { Order } from './Order'
import { Organization } from './Organization'
import { Sku } from './Sku'
import { SkuBatch } from './SkuBatch'

@Entity('LineItem', { schema: 'public' })
export class LineItem extends SoftDeletableEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('double precision', {
    nullable: false,
    // precision: 53,
    name: 'quantity',
  })
  quantity: number

  @Column('double precision', {
    nullable: true,
    // precision: 53,
    name: 'pricePerUnit',
  })
  pricePerUnit: number | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @Column('character varying', {
    nullable: false,
    length: 255,
    default: () => "'0000'",
    name: 'code',
  })
  code: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    default: () => "'----'",
    name: 'name',
  })
  name: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'unit',
  })
  unit: string | null

  @ManyToOne(() => Order, (Order: Order) => Order.lineItems, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'orderId' })
  order: Promise<Order | null>

  @RelationId((LineItem: LineItem) => LineItem.order)
  orderId: Promise<string[]>

  @Column('boolean', {
    nullable: false,
    default: () => 'true',
    name: 'isExciseTaxable',
  })
  isExciseTaxable: boolean

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.lineItems, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((LineItem: LineItem) => LineItem.organization)
  organizationId: Promise<string[]>

  @ManyToOne(() => Sku, (Sku: Sku) => Sku.lineItems, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'skuId' })
  sku: Promise<Sku | null>

  @RelationId((LineItem: LineItem) => LineItem.sku)
  skuId: Promise<string[]>

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'isSample',
  })
  isSample: boolean | null

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'oversold',
  })
  oversold: boolean | null

  @ManyToOne(() => SkuBatch, (SkuBatch: SkuBatch) => SkuBatch.lineItems, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'skuBatchId' })
  skuBatch: Promise<SkuBatch | null>

  @RelationId((LineItem: LineItem) => LineItem.skuBatch)
  skuBatchId: Promise<string[]>

  @ManyToOne(() => Batch, (Batch: Batch) => Batch.lineItems, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'batchId' })
  batch: Promise<Batch | null>

  @RelationId((LineItem: LineItem) => LineItem.batch)
  batchId: Promise<string[]>

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'deletedAt',
  })
  deletedAt: Date | null

  constructor(init?: Partial<LineItem>) {
    super()
    Object.assign(this, init)
  }
}
