import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Order } from './Order'
import { Organization } from './Organization'

@Entity('OrderSubscriber', { schema: 'public' })
@Index('OrderSubscriber_orderId_subscriberId_key', ['order', 'subscriber'], { unique: true })
export class OrderSubscriber extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'manifestGDriveFileId',
  })
  manifestGDriveFileId: string | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Order, (Order: Order) => Order.orderSubscribers, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'orderId' })
  order: Promise<Order | null>

  @RelationId((OrderSubscriber: OrderSubscriber) => OrderSubscriber.order)
  orderId: Promise<string[]>

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.orderSubscribers, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'subscriberId' })
  subscriber: Promise<Organization | null>

  @RelationId((OrderSubscriber: OrderSubscriber) => OrderSubscriber.subscriber)
  subscriberId: Promise<string[]>

  constructor(init?: Partial<OrderSubscriber>) {
    super()
    Object.assign(this, init)
  }
}
