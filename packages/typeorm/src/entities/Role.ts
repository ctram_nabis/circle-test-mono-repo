import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Organization } from './Organization'
import { UserRole } from './UserRole'

@Entity('Role', { schema: 'public' })
@Index('Role_name_key', ['name'], { unique: true })
export class Role extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    unique: true,
    length: 255,
    name: 'name',
  })
  name: string

  @Column('text', {
    nullable: false,
    default: () => 'ARRAY[]::text[]',
    array: true,
    name: 'permissions',
  })
  permissions: string[]

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.roles, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((Role: Role) => Role.organization)
  organizationId: Promise<string[]>

  @Column('text', {
    nullable: true,
    name: 'description',
  })
  description: string | null

  @OneToMany(() => UserRole, (UserRole: UserRole) => UserRole.role, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  userRoles: Promise<UserRole[]>

  constructor(init?: Partial<Role>) {
    super()
    Object.assign(this, init)
  }
}
