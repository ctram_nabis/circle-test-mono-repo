import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Order } from './Order'

@Entity('Vehicle', { schema: 'public' })
@Index('Vehicle_licensePlate_key', ['licensePlate'], { unique: true })
export class Vehicle extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    unique: true,
    length: 255,
    name: 'licensePlate',
  })
  licensePlate: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'name',
  })
  name: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'make',
  })
  make: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'model',
  })
  model: string | null

  @Column('integer', {
    nullable: true,
    name: 'year',
  })
  year: number | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @OneToMany(() => Order, (Order: Order) => Order.vehicle, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  orders: Promise<Order[]>

  constructor(init?: Partial<Vehicle>) {
    super()
    Object.assign(this, init)
  }
}
