import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'

@Entity('SequelizeMeta', { schema: 'public' })
export class SequelizeMeta extends BaseEntity {
  @Column('character varying', {
    nullable: false,
    primary: true,
    length: 255,
    name: 'name',
  })
  name: string

  constructor(init?: Partial<SequelizeMeta>) {
    super()
    Object.assign(this, init)
  }
}
