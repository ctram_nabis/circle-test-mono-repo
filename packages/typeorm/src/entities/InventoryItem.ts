import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { SoftDeletableEntity } from '../utils/SoftDeleteEntity'
import { Batch } from './Batch'
import { InventoryLedgerItem } from './InventoryLedgerItem'
import { Organization } from './Organization'
import { Sku } from './Sku'
import { SkuBatch } from './SkuBatch'
import { Warehouse } from './Warehouse'

@Entity('InventoryItem', { schema: 'public' })
@Index('InventoryItem_skuBatchId_warehouseId_key', ['skuBatch', 'warehouse'], { unique: true })
export class InventoryItem extends SoftDeletableEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => Sku, (Sku: Sku) => Sku.inventoryItems, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'skuId' })
  sku: Promise<Sku | null>

  @RelationId((InventoryItem: InventoryItem) => InventoryItem.sku)
  skuId: Promise<string[]>

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.inventoryItems, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((InventoryItem: InventoryItem) => InventoryItem.organization)
  organizationId: Promise<string[]>

  @Column('double precision', {
    nullable: false,
    default: () => '0',
    // precision: 53,
    name: 'counted',
  })
  counted: number

  @Column('double precision', {
    nullable: false,
    default: () => '0',
    // precision: 53,
    name: 'allocated',
  })
  allocated: number

  @Column('double precision', {
    nullable: false,
    default: () => '0',
    // precision: 53,
    name: 'promised',
  })
  promised: number

  @Column('enum', {
    nullable: false,
    default: () => 'INVENTORY',
    enum: ['INVENTORY', 'QUARANTINE'],
    name: 'storageType',
  })
  storageType: string

  @ManyToOne(() => Warehouse, (Warehouse: Warehouse) => Warehouse.inventoryItems, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'warehouseId' })
  warehouse: Promise<Warehouse | null>

  @RelationId((InventoryItem: InventoryItem) => InventoryItem.warehouse)
  warehouseId: Promise<string[]>

  @ManyToOne(() => SkuBatch, (SkuBatch: SkuBatch) => SkuBatch.inventoryItems, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'skuBatchId' })
  skuBatch: Promise<SkuBatch | null>

  @RelationId((InventoryItem: InventoryItem) => InventoryItem.skuBatch)
  skuBatchId: Promise<string[]>

  @ManyToOne(() => Batch, (Batch: Batch) => Batch.inventoryItems, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  @JoinColumn({ name: 'batchId' })
  batch: Promise<Batch | null>

  @RelationId((InventoryItem: InventoryItem) => InventoryItem.batch)
  batchId: Promise<string[]>

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'deletedAt',
  })
  deletedAt: Date | null

  @OneToMany(
    () => InventoryLedgerItem,
    (InventoryLedgerItem: InventoryLedgerItem) => InventoryLedgerItem.inventoryItem,
    { onDelete: 'SET NULL', onUpdate: 'CASCADE' },
  )
  inventoryLedgerItems: Promise<InventoryLedgerItem[]>

  constructor(init?: Partial<InventoryItem>) {
    super()
    Object.assign(this, init)
  }
}
