import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { OrderChange } from './OrderChange'

@Entity('LineItemChange', { schema: 'public' })
export class LineItemChange extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'name',
  })
  name: string

  @Column('double precision', {
    nullable: false,
    precision: 53,
    name: 'quantity',
  })
  quantity: number

  @Column('double precision', {
    nullable: true,
    precision: 53,
    name: 'pricePerUnit',
  })
  pricePerUnit: number | null

  @Column('boolean', {
    nullable: false,
    default: () => 'true',
    name: 'isExciseTaxable',
  })
  isExciseTaxable: boolean

  @Column('boolean', {
    nullable: true,
    default: () => 'false',
    name: 'isSample',
  })
  isSample: boolean | null

  @Column('enum', {
    nullable: false,
    default: () => 'REMOVED',
    enum: ['ADDED', 'REMOVED'],
    name: 'type',
  })
  type: string

  @Column('uuid', {
    nullable: true,
    name: 'lineItemId',
  })
  lineItemId: string | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @ManyToOne(() => OrderChange, (OrderChange: OrderChange) => OrderChange.lineItemChanges, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'orderChangeId' })
  orderChange: Promise<OrderChange | null>

  @RelationId((LineItemChange: LineItemChange) => LineItemChange.orderChange)
  orderChangeId: Promise<string[]>

  constructor(init?: Partial<LineItemChange>) {
    super()
    Object.assign(this, init)
  }
}
