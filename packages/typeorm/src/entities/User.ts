import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Draft } from './Draft'
import { InventoryLedgerItem } from './InventoryLedgerItem'
import { Order } from './Order'
import { OrderChange } from './OrderChange'
import { Organization } from './Organization'
import { OrganizationInvite } from './OrganizationInvite'
import { OrganizationUser } from './OrganizationUser'
import { UserRole } from './UserRole'

@Entity('User', { schema: 'public' })
@Index('User_email_key', ['email'], { unique: true })
export class User extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    unique: true,
    length: 255,
    name: 'email',
  })
  email: string

  @Column('character varying', {
    nullable: false,
    length: 512,
    name: 'password',
  })
  password: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'firstName',
  })
  firstName: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'lastName',
  })
  lastName: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'address1',
  })
  address1: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'address2',
  })
  address2: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'city',
  })
  city: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'state',
  })
  state: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'zip',
  })
  zip: string | null

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'phone',
  })
  phone: string | null

  @Column('text', {
    nullable: true,
    name: 'profilePicture',
  })
  profilePicture: string | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @Column('boolean', {
    nullable: false,
    default: () => 'false',
    name: 'isAdmin',
  })
  isAdmin: boolean

  @Column('boolean', {
    nullable: true,
    name: 'isArchived',
  })
  isArchived: boolean | null

  @OneToMany(() => Draft, (Draft: Draft) => Draft.creator, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  drafts: Promise<Draft[]>

  @OneToMany(() => InventoryLedgerItem, (InventoryLedgerItem: InventoryLedgerItem) => InventoryLedgerItem.creator, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  inventoryLedgerItems: Promise<InventoryLedgerItem[]>

  @OneToMany(() => Order, (Order: Order) => Order.assignee, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  orders: Promise<Order[]>

  @OneToMany(() => Order, (Order: Order) => Order.creator, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  orders2: Promise<Order[]>

  @OneToMany(() => OrderChange, (OrderChange: OrderChange) => OrderChange.submitter, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  orderChanges: Promise<OrderChange[]>

  @OneToMany(() => Organization, (Organization: Organization) => Organization.owner, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  organizations: Promise<Organization[]>

  @OneToMany(() => OrganizationInvite, (OrganizationInvite: OrganizationInvite) => OrganizationInvite.sender, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  organizationInvites: Promise<OrganizationInvite[]>

  @OneToMany(() => OrganizationUser, (OrganizationUser: OrganizationUser) => OrganizationUser.user, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  organizationUsers: Promise<OrganizationUser[]>

  @OneToMany(() => UserRole, (UserRole: UserRole) => UserRole.user, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  userRoles: Promise<UserRole[]>

  constructor(init?: Partial<User>) {
    super()
    Object.assign(this, init)
  }
}
