import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Organization } from './Organization'
import { User } from './User'

@Entity('OrganizationInvite', { schema: 'public' })
export class OrganizationInvite extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'email',
  })
  email: string

  @ManyToOne(() => Organization, (Organization: Organization) => Organization.organizationInvites, {
    nullable: false,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'organizationId' })
  organization: Promise<Organization | null>

  @RelationId((OrganizationInvite: OrganizationInvite) => OrganizationInvite.organization)
  organizationId: Promise<string[]>

  @ManyToOne(() => User, (User: User) => User.organizationInvites, {
    nullable: false,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'senderId' })
  sender: Promise<User | null>

  @RelationId((OrganizationInvite: OrganizationInvite) => OrganizationInvite.sender)
  senderId: Promise<string[]>

  @Column('boolean', {
    nullable: true,
    name: 'accepted',
  })
  accepted: boolean | null

  @Column('timestamp with time zone', {
    nullable: true,
    name: 'respondedAt',
  })
  respondedAt: Date | null

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  constructor(init?: Partial<OrganizationInvite>) {
    super()
    Object.assign(this, init)
  }
}
