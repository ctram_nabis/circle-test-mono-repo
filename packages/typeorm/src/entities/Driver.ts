import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm'
import { Order } from './Order'

@Entity('Driver', { schema: 'public' })
export class Driver extends BaseEntity {
  @Column('uuid', {
    nullable: false,
    primary: true,
    default: () => 'uuid_generate_v4()',
    name: 'id',
  })
  id: string

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'firstName',
  })
  firstName: string

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'lastName',
  })
  lastName: string | null

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'driversLicense',
  })
  driversLicense: string

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'createdAt',
  })
  createdAt: Date

  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'now()',
    name: 'updatedAt',
  })
  updatedAt: Date

  @OneToMany(() => Order, (Order: Order) => Order.driver, { onDelete: 'SET NULL', onUpdate: 'CASCADE' })
  orders: Promise<Order[]>

  constructor(init?: Partial<Driver>) {
    super()
    Object.assign(this, init)
  }
}
