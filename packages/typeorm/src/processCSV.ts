import * as csv from 'csvtojson'
import 'reflect-metadata'
import { createConnection } from 'typeorm'
import { argv } from 'yargs'
import { Batch } from './entities/Batch'
import { InventoryItem } from './entities/InventoryItem'
import { InventoryLedgerItem } from './entities/InventoryLedgerItem'
import { Organization } from './entities/Organization'
import { Sku } from './entities/Sku'
import { User } from './entities/User'
import { TransactionType } from './enums'

interface RowData {
  batchCode: string
  skuCode: string
  skuName: string
  oak: string
  la: string
  total: string
}

const NABIS_DELIVERY_ORG = '960f4b56-c5db-41d6-824d-8563e98d850e'

const WAREHOUSES = {
  LAS: '85ae34f3-b9c7-415b-b75e-a85e612c5bf5',
  OAK: '6ceafe22-c127-11e8-a355-529269fb1459',
}

const processRow = async (row: RowData) => {
  console.log({ row })
  try {
    // Promises
    const [sku, batch] = await Promise.all([
      Sku.findOne({ code: row.skuCode, isArchived: false }),
      Batch.findOne({ code: row.batchCode || null }),
    ])

    const inventoryItems = await InventoryItem.find({
      where: {
        sku,
        batch,
      },
    })

    interface LocationCount {
      startCounted: number | null
      endCounted: number | null
    }

    // tslint:disable-next-line: variable-name
    let LAInventory: LocationCount = { startCounted: null, endCounted: null }
    // tslint:disable-next-line: variable-name
    let OAKInventory: LocationCount = { startCounted: null, endCounted: null }

    inventoryItems.forEach(async (inventoryItem) => {
      const warehouse = await inventoryItem.warehouse

      const isOAK = warehouse.id === WAREHOUSES.OAK
      const isLA = warehouse.id === WAREHOUSES.LAS

      const startCounted = inventoryItem.counted
      const endCounted = parseFloat(isOAK ? row.oak : row.la) || 0

      if (isOAK) {
        OAKInventory = { startCounted, endCounted }
      } else if (isLA) {
        LAInventory = { startCounted, endCounted }
      } else {
        if (inventoryItem.counted > 0) {
          console.error('Inventory is not LA or OAK', inventoryItem)
        }
        return
      }

      if (startCounted !== endCounted) {
        const organization = Organization.findOne({ id: NABIS_DELIVERY_ORG })
        const creator = User.findOne({ id: 'a20bfa42-151d-4965-8038-de49a5860a65' })

        const inventoryLedgerItem = new InventoryLedgerItem({
          startCounted,
          endCounted,
          notes: 'Update inventory from csv',
          startAllocated: 0,
          endAllocated: 0,
          startPromised: 0,
          endPromised: 0,
          inventoryItem: Promise.resolve(inventoryItem),
          transactionType: TransactionType.CORRECTION,
          organization,
          creator,
        })

        inventoryItem.counted = endCounted

        await Promise.all([InventoryItem.save(inventoryItem), InventoryLedgerItem.save(inventoryLedgerItem)])

        if (isOAK) {
          console.log(`OAK -> SAVED -> InventoryItem: ${inventoryItem.id}`)
        } else if (isLA) {
          console.log(`LA -> SAVED -> InventoryItem: ${inventoryItem.id}`)
        }
      } else {
        console.log(`--- Counts Match ${sku.code} BATCH ${batch && batch.code} ---`)
      }
    })

    console.log({
      skuCode: row.skuCode || '',
      skuIsMatch: sku && sku.code === row.skuCode,
      batchCode: row.batchCode || '',
      batchIsMatch: batch && row.batchCode === batch.code,
      LAInventory,
      OAKInventory,
    })
  } catch (error) {
    console.error(error.message)
  }
}

console.log('Connecting...')
createConnection()
  .then(async (connection) => {
    console.log('Connected.', connection)
    console.log('Start processing CSV...')
    const { _: args } = argv
    const csvFilePath = args[0]

    console.log('Getting CSV:', csvFilePath)
    // tslint:disable-next-line: await-promise
    const rows = await csv().fromFile(csvFilePath)
    console.log(`Done. There are ${rows.length} rows.`)

    rows.forEach((row) => {
      console.log(row)
    })

    // const promises = rows.map(processRow)
    // await Promise.all(promises)
    console.log('DONE!')
  })
  .catch((error: ErrorConstructor) => {
    console.error(error)
  })
