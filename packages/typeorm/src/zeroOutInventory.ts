import 'reflect-metadata'
import { createConnection } from 'typeorm'
import { argv } from 'yargs'
import { WAREHOUSES } from './constants'
import { getOrganizationByName } from './controllers/Organization/getOrganizationByName'
import { InventoryItem } from './entities/InventoryItem'
import { InventoryLedgerItem } from './entities/InventoryLedgerItem'
import { Warehouse } from './entities/Warehouse'
import { TransactionType } from './enums'

const start = async () => {
  const { _: args } = argv
  const DO_IT = args[0] || false

  const organization = await getOrganizationByName('BZL Manufacturing Corp.')
  console.log(`Organization is: ${organization.name}`)

  const warehouse = await Warehouse.findOneOrFail({ where: { id: WAREHOUSES.OAK } })

  const inventoryItems = await InventoryItem.find({ where: { organization, warehouse } })
  console.log(`Number of InventoryItems: ${inventoryItems.length}`)

  for (const inventoryItem of inventoryItems) {
    const inventoryLedgerItem = new InventoryLedgerItem({
      startCounted: inventoryItem.counted,
      endCounted: 0,
      transactionType: TransactionType.CORRECTION,
      notes: 'Zero Out Correction Script',
      organization: Promise.resolve(organization),
      inventoryItem: Promise.resolve(inventoryItem),
    })

    inventoryItem.counted = 0
    inventoryItem.updatedAt = new Date()

    if (DO_IT) {
      await Promise.all([InventoryItem.save(inventoryItem), InventoryLedgerItem.save(inventoryLedgerItem)])
      await inventoryItem.reload()
      console.log(`Saved InventoryItem ${inventoryItem.id} was set to ${inventoryItem.counted}.`)
    }
  }
}

createConnection().then(start)
