import 'reflect-metadata'
import { createConnection } from 'typeorm'
import { argv } from 'yargs'
import { getOrganizationByName } from './controllers/Organization/getOrganizationByName'
import { Batch } from './entities/Batch'
import { LineItem } from './entities/LineItem'
import { Sku } from './entities/Sku'
import { SkuBatch } from './entities/SkuBatch'
import { appendArrToObject } from './utils/appendArrToObject'
import { getDuplicates } from './utils/getDuplicates'
import { getLastUpdatedBatch, getLastUpdatedSku } from './utils/getLastUpdated'
import { getSkuBatchCode } from './utils/getSkuBatchCode'

const { _: args } = argv
const DO_IT = args[0] || false

const start = async () => {
  const organization = await getOrganizationByName('Friendly Farms')
  console.log(`Organization is: ${organization.name}`)

  const skus = await Sku.find({
    where: { organization },
    // where: { organization, deletedAt: null },
    order: { updatedAt: 'DESC' },
  })

  const batchs = await Batch.find({
    where: { organization },
    // where: { organization, deletedAt: null },
    order: { updatedAt: 'DESC' },
  })

  // Group All Duplicates by Code
  const skusBySkuCode: Record<string, Sku[]> = {}
  const batchsByBatchCode: Record<string, Batch[]> = {}

  skus.forEach((sku) => {
    appendArrToObject(skusBySkuCode, sku.code, sku)
  })

  batchs.forEach((batch) => {
    appendArrToObject(batchsByBatchCode, batch.code, batch)
  })

  const duplicateSkusBySkuCode = getDuplicates(skusBySkuCode)
  console.log({ duplicateSkusBySkuCode })

  const duplicateBatchsByBatchCode = getDuplicates(batchsByBatchCode)
  console.log({ duplicateBatchsByBatchCode })

  /**
   * HANDLE DUPLICATE SKUS
   */

  for (const skuCode in duplicateSkusBySkuCode) {
    if (duplicateSkusBySkuCode.hasOwnProperty(skuCode)) {
      console.log(`\n\n\n`)
      console.log(`*************************************************************`)
      console.log(`processing skuCode ${skuCode}`)
      console.log(`*************************************************************`)

      const duplicates = duplicateSkusBySkuCode[skuCode]
      console.log(`There are ${duplicates.length} duplicates \n`)

      const keepSku = await getLastUpdatedSku(duplicates)

      if (!keepSku) {
        throw new Error(`keepSku is undefined`)
      } else {
        console.log('keepSkuId', keepSku.id)
      }

      const skusToDelete: Sku[] = []
      const skuBatchesFromRemovedSkus: SkuBatch[] = []

      for (const sku of duplicates) {
        console.log(`- dup sku: ${sku.id}`)

        if (sku.id !== keepSku.id) {
          // get all skuBatchs to iterate over
          const skuBatchs = await sku.skuBatchs

          for (const skuBatch of skuBatchs) {
            const skuBatchCode = await getSkuBatchCode(skuBatch)
            console.log(`-- skuBatch`, `skuBatch.id ${skuBatch.id}`, `skuBatch code ${skuBatchCode}\n`)
            skuBatchesFromRemovedSkus.push(skuBatch)
          }

          skusToDelete.push(sku)
        }
      }

      const numSkusToDelete = skusToDelete.length
      console.log('number of skus to delete', numSkusToDelete)

      const skuBatchsBySkuBatchCode: Record<string, SkuBatch[]> = {}

      for (const skuBatch of skuBatchesFromRemovedSkus) {
        const batch = await skuBatch.batch
        const skuBatchCode = `${skuCode}~${batch ? batch.code : 'null'}`
        appendArrToObject(skuBatchsBySkuBatchCode, skuBatchCode, skuBatch)
      }

      for (const skuBatchCode in skuBatchsBySkuBatchCode) {
        if (skuBatchsBySkuBatchCode.hasOwnProperty(skuBatchCode)) {
          console.log(`\n`)
          console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`)
          console.log(` processing skuBatchCode ${skuBatchCode}`)
          console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`)

          const skuBatchs = skuBatchsBySkuBatchCode[skuBatchCode]
          console.log(`number of SkuBatchs for SkuCode ${skuBatchCode}: ${skuBatchs.length} \n`)

          const keepSkuBatch: SkuBatch =
            skuBatchs.find((skuBatch) => {
              return !!skuBatch.isArchived
            }) || skuBatchs[0]

          /**
           * Figure out which SkuBatch to use
           */
          // let allInventoryItems: InventoryItem[] = []

          // for (const skuBatch of skuBatchs) {
          //   const inventoryItems = await skuBatch.inventoryItems
          //   allInventoryItems = allInventoryItems.concat(inventoryItems)
          // }

          // if (allInventoryItems.length > 0) {
          //   const keepInventoryItem: InventoryItem = await getLastUpdatedInventoryItem(allInventoryItems)

          //   if (keepInventoryItem) {
          //     // TODO - Override the SkuBatch, and set to new SkuBatch we want to finalize with
          //     console.log('keep inventoryItem id', keepInventoryItem.id)
          //     keepSkuBatch = await keepInventoryItem.skuBatch

          //     // TODO - Remove all the other InventoryItems except the one we wan to keep
          //     const deleteInventoryItems = allInventoryItems.filter((ii) => ii.id !== keepInventoryItem.id)
          //     const numDeleteInventoryItems = deleteInventoryItems.length
          //     console.log('num of inventoryItems to delete', numDeleteInventoryItems)

          //     for (const inventoryItem of deleteInventoryItems) {
          //       console.log(`(-) will softRemove inventoryItem: ${inventoryItem.id}`)

          //       if (DO_IT) {
          //         await inventoryItem.softRemove()
          //         console.log(`----- ✓ removed inventoryItem: ${inventoryItem.id}`)
          //       }
          //     }
          //   } else {
          //     console.error(`| (!) No LATEST InventoryItem for skuBatchCode: ${skuBatchCode}`)
          //   }
          // } else {
          //   console.log(`| (!) No inventory items found for skuBatchCode: ${skuBatchCode}`)
          // }

          // console.log('keep skuBatchId', keepSkuBatch.id)
          // console.log('------')

          /**
           * -- end get keepInventoryItem
           */

          const deleteSkuBatchs: SkuBatch[] = skuBatchs.filter((sb) => sb.id !== keepSkuBatch.id)
          console.log('num of SkuBatchs to delete', deleteSkuBatchs.length)

          for (const deleteSkuBatch of deleteSkuBatchs) {
            // TODO - REASSIGN LINE ITEMS FOR THE SKU BATCH TO NEW SKU BATCH
            console.log('\n -- Reassign lineItems with skuBatch to new skuBatch')
            const lineItems = await deleteSkuBatch.lineItems

            for (const lineItem of lineItems) {
              lineItem.skuBatch = Promise.resolve(keepSkuBatch)
              console.log(`(+) will updating LineItem ${lineItem.id}`)

              if (DO_IT) {
                await LineItem.save(lineItem)
                console.log(`✓ updated LineItem ${lineItem.id}`)
              }
            }

            // TODO - DELETE INVENTORY ITEMS THAT POINT TO OLD SKU BATCH
            const inventoryItems = await deleteSkuBatch.inventoryItems

            for (const inventoryItem of inventoryItems) {
              console.log(`(-) will remove InventoryItem ${inventoryItem.id}`)
              if (DO_IT) {
                await inventoryItem.softRemove()
                console.log(`✓ removed LineItem ${inventoryItem.id}`)
              }
            }

            // TODO - DELETE THE SKU BATCH
            console.log(`(-) will softRemove skuBatch ${deleteSkuBatch.id}`)

            if (DO_IT) {
              await deleteSkuBatch.softRemove()
              console.log(`✓ removed skuBatch: ${deleteSkuBatch.id}`)
            }
          }
        }
      }

      // TODO - LAST STEP
      for (const sku of skusToDelete) {
        console.log(`(-) will softRemove sku: ${sku.id}`)

        const skuBatchs = await sku.skuBatchs

        for (const skuBatch of skuBatchs) {
          if (skuBatch.deletedAt === null) {
            console.log(`** will removing skuBatch to now deleted sku, that wasn't already deleted ${skuBatch.id}`)

            if (DO_IT) {
              skuBatch.softRemove()
              console.log(`** -- soft removed skuBatch ${skuBatch.id}`)
            }
          }
        }

        if (DO_IT) {
          await sku.softRemove()
          console.log(`✓ removed sku: ${sku.id}`)
        }
      }
    }
  }

  /**
   * HANDLE DUPLICATE BATCHS
   */

  for (const batchCode in duplicateBatchsByBatchCode) {
    if (duplicateBatchsByBatchCode.hasOwnProperty(batchCode)) {
      console.log(`\n\n\n`)
      console.log(`*************************************************************`)
      console.log(`processing batchCode ${batchCode}`)
      console.log(`*************************************************************`)

      const duplicates = duplicateBatchsByBatchCode[batchCode]
      console.log(`There are ${duplicates.length} duplicates \n`)

      const keepBatch = await getLastUpdatedBatch(duplicates)

      if (!keepBatch) {
        throw new Error(`keepBatch is undefined`)
      } else {
        console.log('keepBatchId', keepBatch.id)
      }

      const batchsToDelete: Batch[] = []
      const skuBatchesFromRemovedBatchs: SkuBatch[] = []

      for (const batch of duplicates) {
        console.log(`- dup batch: ${batch.id}`)

        if (batch.id !== keepBatch.id) {
          // get all skuBatchs to iterate over
          const skuBatchs = await batch.skuBatchs

          for (const skuBatch of skuBatchs) {
            const skuBatchCode = await getSkuBatchCode(skuBatch)
            console.log(`-- skuBatch`, `skuBatch.id ${skuBatch.id}`, `skuBatch code ${skuBatchCode}\n`)
            skuBatchesFromRemovedBatchs.push(skuBatch)
          }

          batchsToDelete.push(batch)
        }
      }

      const numBatchsToDelete = batchsToDelete.length
      console.log('number of batchs to delete', numBatchsToDelete)

      const skuBatchsBySkuBatchCode: Record<string, SkuBatch[]> = {}

      for (const skuBatch of skuBatchesFromRemovedBatchs) {
        const batch = await skuBatch.batch
        const skuBatchCode = `${batchCode}~${batch ? batch.code : 'null'}`
        appendArrToObject(skuBatchsBySkuBatchCode, skuBatchCode, skuBatch)
      }

      for (const skuBatchCode in skuBatchsBySkuBatchCode) {
        if (skuBatchsBySkuBatchCode.hasOwnProperty(skuBatchCode)) {
          console.log(`\n`)
          console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`)
          console.log(` processing skuBatchCode ${skuBatchCode}`)
          console.log(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`)

          const skuBatchs = skuBatchsBySkuBatchCode[skuBatchCode]
          console.log(`number of SkuBatchs for SkuCode ${skuBatchCode}: ${skuBatchs.length} \n`)

          const keepSkuBatch: SkuBatch =
            skuBatchs.find((skuBatch) => {
              return !!skuBatch.isArchived
            }) || skuBatchs[0]

          /**
           * Figure out which SkuBatch to use
           */
          // let allInventoryItems: InventoryItem[] = []

          // for (const skuBatch of skuBatchs) {
          //   const inventoryItems = await skuBatch.inventoryItems
          //   allInventoryItems = allInventoryItems.concat(inventoryItems)
          // }

          // if (allInventoryItems.length > 0) {
          //   const keepInventoryItem: InventoryItem = await getLastUpdatedInventoryItem(allInventoryItems)

          //   if (keepInventoryItem) {
          //     // TODO - Override the SkuBatch, and set to new SkuBatch we want to finalize with
          //     console.log('keep inventoryItem id', keepInventoryItem.id)
          //     keepSkuBatch = await keepInventoryItem.skuBatch

          //     // TODO - Remove all the other InventoryItems except the one we wan to keep
          //     const deleteInventoryItems = allInventoryItems.filter((ii) => ii.id !== keepInventoryItem.id)
          //     const numDeleteInventoryItems = deleteInventoryItems.length
          //     console.log('num of inventoryItems to delete', numDeleteInventoryItems)

          //     for (const inventoryItem of deleteInventoryItems) {
          //       console.log(`(-) will softRemove inventoryItem: ${inventoryItem.id}`)

          //       if (DO_IT) {
          //         await inventoryItem.softRemove()
          //         console.log(`----- ✓ removed inventoryItem: ${inventoryItem.id}`)
          //       }
          //     }
          //   } else {
          //     console.error(`| (!) No LATEST InventoryItem for skuBatchCode: ${skuBatchCode}`)
          //   }
          // } else {
          //   console.log(`| (!) No inventory items found for skuBatchCode: ${skuBatchCode}`)
          // }

          // console.log('keep skuBatchId', keepSkuBatch.id)
          // console.log('------')

          /**
           * -- end get keepInventoryItem
           */

          const deleteSkuBatchs: SkuBatch[] = skuBatchs.filter((sb) => sb.id !== keepSkuBatch.id)
          console.log('num of SkuBatchs to delete', deleteSkuBatchs.length)

          for (const deleteSkuBatch of deleteSkuBatchs) {
            // TODO - REASSIGN LINE ITEMS FOR THE SKU BATCH TO NEW SKU BATCH
            console.log('\n -- Reassign lineItems with skuBatch to new skuBatch')
            const lineItems = await deleteSkuBatch.lineItems

            for (const lineItem of lineItems) {
              lineItem.skuBatch = Promise.resolve(keepSkuBatch)
              console.log(`(+) will update LineItem ${lineItem.id}`)

              if (DO_IT) {
                await LineItem.save(lineItem)
                console.log(`✓ updated LineItem ${lineItem.id}`)
              }
            }

            // TODO - DELETE INVENTORY ITEMS THAT POINT TO OLD SKU BATCH
            const inventoryItems = await deleteSkuBatch.inventoryItems

            for (const inventoryItem of inventoryItems) {
              console.log(`(-) will remove InventoryItem ${inventoryItem.id}`)
              if (DO_IT) {
                await inventoryItem.softRemove()
                console.log(`✓ removed LineItem ${inventoryItem.id}`)
              }
            }

            // TODO - DELETE THE SKU BATCH
            console.log(`(-) will softRemove skuBatch ${deleteSkuBatch.id}`)

            if (DO_IT) {
              await deleteSkuBatch.softRemove()
              console.log(`✓ removed skuBatch: ${deleteSkuBatch.id}`)
            }
          }
        }
      }

      // TODO - LAST STEP
      for (const batch of batchsToDelete) {
        console.log(`(-) will softRemove batch: ${batch.id}`)

        if (DO_IT) {
          await batch.softRemove()
          console.log(`✓ removed batch: ${batch.id}`)
        }
      }
    }
  }
}

createConnection().then(start)
