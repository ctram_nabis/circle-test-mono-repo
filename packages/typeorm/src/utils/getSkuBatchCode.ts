import { SkuBatch } from '../entities/SkuBatch'

export async function getSkuBatchCode(skuBatch: SkuBatch) {
  const sku = await skuBatch.sku
  const batch = await skuBatch.batch
  const skuCode = sku.code
  const batchCode = batch ? batch.code : 'null'

  return `${skuCode}~${batchCode}`
}
