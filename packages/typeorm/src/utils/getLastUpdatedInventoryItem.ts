import { InventoryItem } from '../entities/InventoryItem'

export async function getLastUpdatedInventoryItem(items: InventoryItem[], checkBatch = false) {
  let lastUpdateDate = 0
  let lastUpdated: InventoryItem | undefined

  for (const item of items) {
    const { updatedAt } = item
    const skuBatch = await item.skuBatch
    const sku = await skuBatch.sku
    const batch = await skuBatch.batch

    let isRemoved: boolean

    if (checkBatch) {
      isRemoved = !!batch.deletedAt
    } else {
      isRemoved = sku.isArchived || !!sku.deletedAt
    }

    if (!isRemoved && updatedAt && updatedAt.getTime() > lastUpdateDate) {
      lastUpdateDate = updatedAt.getTime()
      lastUpdated = item
    }
  }

  if (!lastUpdated && items.length > 0) {
    return items[0]
  }

  return lastUpdated
}
