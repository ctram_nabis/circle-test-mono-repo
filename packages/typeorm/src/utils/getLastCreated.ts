export function getLastCreated<T extends Partial<{ createdAt?: Date }>>(items: T[]) {
  let lastCreatedDate = 0
  let lastCreated: T

  items.forEach((item) => {
    if (item && item.createdAt && item.createdAt.getTime() > lastCreatedDate) {
      lastCreatedDate = item.createdAt.getTime()
      lastCreated = item
    }
  })

  return lastCreated
}
