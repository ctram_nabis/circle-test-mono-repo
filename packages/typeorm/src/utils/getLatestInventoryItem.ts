import { InventoryItem } from '../entities/InventoryItem'
import { TransactionType } from '../enums'

export async function getLatestInventoryItem(inventoryItems: InventoryItem[]) {
  // Find Last Corrected Inventory Item
  let lastCorrectedDate = 0
  let lastUpdated: InventoryItem

  for (const inventoryItem of inventoryItems) {
    const inventoryLedgerItems = await inventoryItem.inventoryLedgerItems

    let lastCreatedInventoryLedgerItem = 0

    for (const inventoryLedgerItem of inventoryLedgerItems) {
      if (inventoryLedgerItem.transactionType !== TransactionType.CORRECTION) return
      if (inventoryLedgerItem.createdAt.getTime() > lastCreatedInventoryLedgerItem) {
        lastCreatedInventoryLedgerItem = inventoryLedgerItem.createdAt.getTime()
      }
    }

    if (lastCreatedInventoryLedgerItem > lastCorrectedDate) {
      lastCorrectedDate = lastCreatedInventoryLedgerItem
      lastUpdated = inventoryItem
    }
  }

  return lastUpdated
}
