export const getDuplicates = <T>(records: Record<string, T[]>) => {
  const dups: Record<string, T[]> = {}

  for (const key in records) {
    if (records.hasOwnProperty(key)) {
      const item = records[key]
      if (item.length > 1) dups[key] = item
    }
  }

  return dups
}
