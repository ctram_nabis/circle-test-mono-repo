import { Field } from 'type-graphql'
import { BaseEntity, BeforeRemove, Column, EntityManager, FindManyOptions, FindOneOptions } from 'typeorm'

export abstract class SoftDeletableEntity extends BaseEntity {
  @Field()
  @Column({})
  deletedAt: Date

  @BeforeRemove()
  beforeRemove() {
    throw new Error(`Attempted to hard-delete a soft-deletable ${this.constructor.name}`)
  }

  async softRemove(): Promise<void> {
    await this.softRemoveInternal()
  }

  async softRemoveUsing(manager: EntityManager): Promise<void> {
    await this.softRemoveInternal(manager)
  }

  private async softRemoveInternal(manager?: EntityManager): Promise<void> {
    const thisClass = this.constructor as typeof SoftDeletableEntity

    if (this.deletedAt) {
      // throw new Error(`Attempted to soft-delete a(n) ${thisClass.name} that has already been soft-deleted`)
    }

    manager = manager || thisClass.getRepository().manager
    await manager.update(thisClass, thisClass.getId(this), { deletedAt: new Date() })
  }

  static async count<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    options?: FindManyOptions<T>,
  ): Promise<number> {
    return super.count<T>({
      where: { deletedAt: null },
      ...options,
    })
  }

  static async find<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    options?: FindManyOptions<T>,
  ): Promise<T[]> {
    return super.find<T>({
      where: { deletedAt: null },
      ...options,
    })
  }

  static async findAndCount<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    options?: FindManyOptions<T>,
  ): Promise<[T[], number]> {
    return super.findAndCount<T>({
      where: { deletedAt: null },
      ...options,
    })
  }

  static async findByIds<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    ids: string[],
    options?: FindManyOptions<T>,
  ): Promise<T[]> {
    return super.findByIds<T>(ids, {
      where: { deletedAt: null },
      ...options,
    })
  }

  static async findOne<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    id?: string,
    options?: FindOneOptions<T>,
  ): Promise<T | undefined>
  static async findOne<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    options?: FindOneOptions<T>,
  ): Promise<T | undefined>
  static async findOne<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    idOrOptions?: string | FindOneOptions<T>,
    maybeOptions?: FindOneOptions<T>,
  ): Promise<T | undefined> {
    const id = idOrOptions instanceof Object ? undefined : idOrOptions
    const options: FindOneOptions<T> = {
      where: { deletedAt: null },
      ...(idOrOptions instanceof Object ? idOrOptions : maybeOptions),
    }

    return super.findOne<T>(id, options)
  }

  static async findOneOrFail<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    id?: string,
    options?: FindOneOptions<T>,
  ): Promise<T | undefined>
  static async findOneOrFail<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    options?: FindOneOptions<T>,
  ): Promise<T | undefined>
  static async findOneOrFail<TStatic extends typeof BaseEntity, T extends InstanceType<TStatic>>(
    this: TStatic,
    idOrOptions?: string | FindOneOptions<T>,
    maybeOptions?: FindOneOptions<T>,
  ): Promise<T | undefined> {
    const id = idOrOptions instanceof Object ? undefined : idOrOptions
    const options: FindOneOptions<T> = {
      where: { deletedAt: null },
      ...(idOrOptions instanceof Object ? idOrOptions : maybeOptions),
    }

    return super.findOneOrFail<T>(id, options)
  }
}
