import { Batch } from '../entities/Batch'
import { Sku } from '../entities/Sku'
import { SkuBatch } from '../entities/SkuBatch'

export async function getLastUpdatedBatch(batchs: Batch[]) {
  let lastUpdateDate = 0
  let lastUpdated: Batch

  const batchsWithActiveSkuBatchs: Batch[] = []

  for (const batch of batchs) {
    const skuBatchs = await batch.skuBatchs
    const activeSkuBatch = skuBatchs.find((skuBatch) => !skuBatch.isArchived)

    if (activeSkuBatch) {
      batchsWithActiveSkuBatchs.push(batch)
    }
  }

  batchsWithActiveSkuBatchs.forEach((batch) => {
    const { updatedAt, deletedAt, isArchived } = batch

    if (!isArchived && !deletedAt && updatedAt && updatedAt.getTime() > lastUpdateDate) {
      lastUpdateDate = batch.updatedAt.getTime()
      lastUpdated = batch
    }
  })

  if (!lastUpdated) {
    batchs.forEach((batch) => {
      const { updatedAt, deletedAt, isArchived } = batch

      if (!isArchived && !deletedAt && updatedAt && updatedAt.getTime() > lastUpdateDate) {
        lastUpdateDate = batch.updatedAt.getTime()
        lastUpdated = batch
      }
    })
  }

  if (!lastUpdated) {
    return batchs[0]
  }

  return lastUpdated
}

export async function getLastUpdatedSku(skus: Sku[]) {
  let lastUpdateDate = 0
  let lastUpdated: Sku

  const skusWithActiveSkuBatchs: Sku[] = []

  for (const sku of skus) {
    const skuBatchs = await sku.skuBatchs
    const activeSkuBatch = skuBatchs.find((skuBatch) => !skuBatch.isArchived)

    if (activeSkuBatch) {
      skusWithActiveSkuBatchs.push(sku)
    }
  }

  skusWithActiveSkuBatchs.forEach((sku) => {
    const { updatedAt, deletedAt, isArchived } = sku

    if (!isArchived && !deletedAt && updatedAt && updatedAt.getTime() > lastUpdateDate) {
      lastUpdateDate = sku.updatedAt.getTime()
      lastUpdated = sku
    }
  })

  if (!lastUpdated) {
    skus.forEach((sku) => {
      const { updatedAt, deletedAt, isArchived } = sku

      if (!isArchived && !deletedAt && updatedAt && updatedAt.getTime() > lastUpdateDate) {
        lastUpdateDate = sku.updatedAt.getTime()
        lastUpdated = sku
      }
    })
  }

  if (!lastUpdated) {
    return skus[0]
  }

  return lastUpdated
}
