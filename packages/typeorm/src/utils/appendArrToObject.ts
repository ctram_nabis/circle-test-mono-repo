export function appendArrToObject<T>(object: Record<string, T[]>, key: string, item: T) {
  if (!object[key]) object[key] = []
  object[key].push(item)
  return object
}
