import { InventoryItem } from '../entities/InventoryItem'

export function getMostCounted(inventoryItems: InventoryItem[]) {
  let highestItem: InventoryItem
  let highestCounted = 0

  inventoryItems.forEach((inventoryItem) => {
    const counted = inventoryItem.counted
    if (counted > highestCounted) {
      highestCounted = counted
      highestItem = inventoryItem
    }
  })

  return highestItem
}
