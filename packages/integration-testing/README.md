# End to End Testing With Cypress

## Getting Cypress Running

1. From mono-repo root, run `docker-compose up postgres-testing`; this builds/starts the testing database. This must always be running for the testing app to work, just like the development app.

2. From mono-repo root, run `yarn migrate:testing` to run migration for the testing database.

3. From mono-repo root, run `yarn start:testing` to start both the testing frontend and backend. These apps run on port `3001` and `8081` respectively.

4. From mono-repo root, run `yarn cyress:open` to open the Cypress GUI. Cypress GUI checks for test files found under `packages/integration-testing/cypress/integration/*` and lists them within the GUI. You may run individual tests and watch Cypress interact with the app.

5. Changes made to the test files automatically re-run that cypress test in the GUI.

## Running Cypress Tests Headless (No Browser GUI)

`yarn cypress:run`

## Adding Attributes to React Components For Wasy Querying In Tests

If you need to make selecting a DOM element easier, set a `data-test` attribute on the React component.

For example

```js
// React components

function AdminOrdersRow() {
  return <div data-test="admin-orders-row">{/* some stuff here */}</div>
}

function AdminOrdersPage(orders) {
  // orders.length === 4

  return (
    <div>
      {orders.map((order) => {
        return <AdminOrdersRow order={order} />
      })}
    </div>
  )
}
```

Now in our Cypress tests, we can do this:

```js
// Cypress test file
cy.get('[data-test=admin-orders-row]').should('have.length', 4)
```

## You Can Use `debugger` In Test Files.

```js
describe('Create Organization', function() {
  it('Creates a new Organization', function() {
    const contactName = 'Bruce Wayne'
    const contactPhoneNumber = '555-444-3333'

    debugger

    const orgName = randomOrganization()
    cy.log('Waiting for organizations to fully load.')

    // ...
  }
})
```

Running the above test with the developer tools open within Cypress' browser will pause in the test as expected.

## Alias

https://docs.cypress.io/guides/core-concepts/variables-and-aliases.html#Aliases

Alias' provide a way for us to store a reference a value returned from an async call, without using closures.

Instead of doing this

```js
let user

describe('Create SkuBatch', function() {
  before(() => {
    cy.createUser().then((_user) => {
      user = _user
    })
  })

  it('creates SkuBatch', function() {
    // Now we can access the user object created in the before hook above.
    console.log(user) // { id: 123, firstName: 'Frank', ....} /
  })
})
```

We can do this

```js
describe('Create SkuBatch', function() {
  before(() => {
    cy.createUser().as('user') // Alias the returned value of createUser() to the name 'user'
  })

  it('creates SkuBatch', function() {
    // Now we can access the user object created in the before hook above.
    console.log(this.user) // { id: 123, firstName: 'Frank', ....} /
  })
})
```

NOTE: in order to access alias', the callback passed into your it() function **must** be a regular function, not an arrow function.

## Use The Electron Browser to Avoid Confusion With Your Chrome Browser

By default, the Cypress GUI opens a new Chrome browser instance to run the tests. If you have your own Chrome browser open, it might get confusing when switch between applications.

Within the Cypress GUI, in the top right corner, you can choose to use the Electron browser, to avoid app switching confusion.

Just remember, when you are done writing tests, it is a good idea to run all tests again using Chrome, just in case.

## Cypress Selector Playground

Cypress GUI feature that helps you figure out what selector to use to query a DOM element(s)

https://docs.cypress.io/guides/core-concepts/test-runner.html#Selector-Playground

## References

### API

https://docs.cypress.io/api/api/table-of-contents.html

### For best practices:

https://docs.cypress.io/guides/references/best-practices.html

https://docs.cypress.io/guides/references/best-practices.html#Selecting-Elements
