/* eslint-disable no-unused-expressions */
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })

// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import faker from 'faker'
import moment from 'moment-timezone'

import {
  TEST_ORGANIZATION_MASKED_ID,
  TEST_ORGANIZATION_USER_MASKED_ID,
  WAREHOUSES,
  NABIS_DELIVERIES_PASSWORD,
  DEFAULT_PASSWORD,
  ORDER_ACTIONS,
  NABIS_DELIVERIES_EMAIL,
  SITES,
  CA_TIMEZONE,
  ROLE_IDS,
  TITLE_HOLDER_IDS,
} from '../fixtures/constants'

const SIGN_IN_URL = '/sign-in'

const FORMAT = 'YYYY-MM-DD'

const baseHeaders = {
  'content-type': 'application/json',
}

function headersWithAuthToken() {
  const authToken = window.localStorage.getItem('AUTH_TOKEN')

  if (!authToken) {
    throw Error(
      'Attempted to build AUTHORIZATION header, but there is no authToken in localstorage to use. You probably forgot to login or create a user.',
    )
  }

  return { ...baseHeaders, AUTHORIZATION: `Bearer ${authToken}` }
}

/**
 *
 *
 * @param {string} firstName
 * @param {boolean=false} isAdmin Whether to include the string 'admin' in the email address.
 *                                New created user with the string 'admin' in email address will be
 *                                given Nabis admin rights, only in test environment.
 * @returns {string}
 */
function generateRandomEmail({ firstName, isAdmin = false }) {
  firstName = firstName ? firstName : faker.name.firstName()

  // New users with 'admin' in their email will be created as a Nabis admin.
  // This only occurs in testing environment.
  return `${firstName}-${faker.random.number()}-${faker.random.number()}${
    isAdmin ? '-admin' : ''
  }@${faker.internet.domainName()}`
}

/**
 * Makes a Cypress request using boilerplate request properties.
 *
 * @param {object} data
 * @param {string=} requestName
 * @returns {Cypress Promise}
 */
function cyRequest(data, requestName) {
  const defaultData = {
    url: 'http://localhost:8081/graphql/admin',
    method: 'POST',
  }

  let { body, headers } = data

  headers = headers ? headers : headersWithAuthToken()

  body = typeof body === 'string' ? body : JSON.stringify(body)

  const request = { ...defaultData, ...data, body, headers }

  if (requestName) {
    cy.log(`Start request: ${requestName}`)
  }

  return cy.request(request).then((res) => {
    cy.log(`End request: ${requestName}`)
    return cy.wrap(res)
  })
}

function ensureLoggedInAsNabisAdmin() {
  const isAdmin = window.localStorage.getItem('isAdmin')

  if (isAdmin && isAdmin === 'true') {
    return cy
  }

  return cy.login({}, { asNabisAdmin: true })
}

function setSiteForInternalTransfer(data) {
  const { warehouseId: sourceWarehouseId } = data
  const { oak, la } = WAREHOUSES
  const { nabione, zasp } = SITES

  let site

  // Destination site is site of the other warehouse
  switch (sourceWarehouseId) {
    case oak.id:
      site = zasp
      break
    case la.id:
      site = nabione
      break
    default:
      throw Error('sourceWarehouseId not recongized ${sourceWarehouseId}')
  }

  const { id: siteId, licensedLocationId, siteLicenseNum } = site

  data = { ...data, siteId, licensedLocationId, siteLicenseNum }
  return data
}

function momentCaTimezone() {
  return moment().tz(CA_TIMEZONE)
}

function buildLineItem(skuBatch, lineItem = {}) {
  const { sku } = skuBatch
  delete lineItem.skuBatch

  const defaultLineItem = {
    code: sku.code,
    isExciseTaxable: sku.isExciseTaxable,
    isSample: false,
    name: sku.name,
    organizationId: sku.organizationId,
    oversold: false,
    pricePerUnit: sku.pricePerUnit,
    quantity: 10,
    skuBatchId: skuBatch.id,
    unit: sku.unit,
  }

  return { ...defaultLineItem, ...lineItem }
}

Cypress.Commands.add('loginAsNabis', () => {
  return cy.fixture('users').then((users) => {
    const nabisDeliveriesEmail = users.nabis.deliveries.email

    cy.visit(SIGN_IN_URL)

    cy.get('input[name="email"]')
      .type(nabisDeliveriesEmail)
      .should('have.value', nabisDeliveriesEmail)

    cy.get('input[name="password"]')
      .type(NABIS_DELIVERIES_PASSWORD)
      .should('have.value', NABIS_DELIVERIES_PASSWORD)

    cy.get('form').submit()
  })
})

const loginCommand = {
  getProps: (data = {}, options = {}) => {
    let { email } = data
    const { asNabisAdmin } = options
    let isAdmin = !!asNabisAdmin

    const customRequestFn = () => {
      if (asNabisAdmin) {
        email = NABIS_DELIVERIES_EMAIL
      }

      const body = {
        operationName: 'SignIn',
        variables: {
          input: {
            email,
            password: NABIS_DELIVERIES_PASSWORD,
          },
        },
        query:
          'mutation SignIn($input: LoginUserInput!) {\n  loginUser(input: $input) {\n    token\n    user {\n      ...userFragment\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment userFragment on User {\n  id\n  email\n  firstName\n  lastName\n  address1\n  address2\n  city\n  state\n  zip\n  phone\n  profilePicture\n  isAdmin\n  __typename\n}\n',
      }

      return cy.clearCookies().then(() => {
        return cyRequest({ body, headers: baseHeaders }, 'loginUser')
      })
    }

    const responseHandler = (response) => {
      const body = response.body

      return cy
        .setCookie('nabis_user_profile_development', JSON.stringify(body.data.loginUser.user))
        .then(() => cy.setCookie('nabis_user_token_development', body.data.loginUser.token))
        .then(() => {
          window.localStorage.setItem('AUTH_TOKEN', body.data.loginUser.token)
          window.localStorage.setItem('isAdmin', isAdmin)

          const userInfo = {
            firstName: data.firstName,
            lastName: data.lastName,
            email,
            isAdmin,
          }

          cy.log(`User logged in: ${JSON.stringify(userInfo)}`)

          const date = new Date(Date.now() + 10000000000)
          const dateString = date.toUTCString()
          return cy.setCookie('expires', dateString)
        })
        .then(() => {
          const { user, token } = response.body.data.loginUser
          return { ...user, token }
        })
    }

    return { customRequestFn, responseHandler }
  },
}

const createOrderCommand = {
  getProps: (data) => {
    const name = `${faker.name.firstName()}'s order-${faker.random.number()}`

    const defaultData = {
      action: ORDER_ACTIONS.PICKUP_FROM_BRAND,
      adminNotes: `admin notes: ${faker.lorem.paragraph()}`,
      internalNotes: `Internal notes: ${faker.lorem.paragraph()}`,
      creatorId: TEST_ORGANIZATION_USER_MASKED_ID,
      date: momentCaTimezone()
        .add(1, 'week')
        .format(FORMAT),
      daysTillPaymentDue: null,
      discount: 0,
      exciseTax: 0,
      exciseTaxCollected: 0,
      gmv: 0,
      gmvCollected: 0,
      irn: name,
      licensedLocationId: '',
      lineItems: [
        // {
        //   code: 'SR-FOCUS',
        //   isExciseTaxable: true,
        //   isSample: false,
        //   name: 'FOCUS Vape Pen, Silk Road',
        //   organizationId: 'T3JnYW5pemF0aW9uOmFiNzE5YTZkLWEwOGMtNDAxOS05NjQyLTFmZWVlNDcyOTdjYg==',
        //   oversold: false,
        //   pricePerUnit: 22.5,
        //   quantity: 1,
        //   skuBatchId: 'U2t1QmF0Y2g6YmY1YTBkNTAtNjVlZS00Mzc3LTgyZDUtM2ZmZTcyMGY1M2Q2',
        //   unit: '',
        // },
      ],
      name: name,
      notes: `Notes: ${faker.lorem.paragraph()}`,
      orgLicenseNum: null,
      organizationId: TEST_ORGANIZATION_MASKED_ID,
      siteId: '',
      siteLicenseNum: '',
      paymentStatus: 'UNPAID',
      status: 'UNSCHEDULED',
      timeWindow: 'ALL DAY',
      warehouseId: WAREHOUSES.oak.id,
    }

    data = { ...defaultData, ...data }

    if (data.action === ORDER_ACTIONS.INTERNAL_TRANSFER) {
      data = setSiteForInternalTransfer(data)
    }

    let { siteId, siteLicenseNum, licensedLocationId, lineItems } = data

    let site
    let skuBatches = {}

    const customRequestFn = () => {
      return cy
        .then(() => {
          if (lineItems.length === 0) {
            return cy.createSkuBatch({}, { initializeInventory: true }).then((skuBatch) => {
              skuBatches[skuBatch.id] = skuBatch
              lineItems.push(buildLineItem(skuBatch))
            })
          }

          lineItems = lineItems.map((lineItem) => {
            const { skuBatch } = lineItem
            return buildLineItem(skuBatch, lineItem)
          })
        })
        .then(() => {
          if (siteId) {
            return
          }

          return cy.createSite().then((_site) => {
            site = _site
            siteId = site.id
            siteLicenseNum = site.licensedLocation.licenses[0].licenseNumber
            licensedLocationId = site.licensedLocation.id
          })
        })
        .then(() => {
          data = { ...data, lineItems, siteId, licensedLocationId, siteLicenseNum }

          const body = {
            operationName: 'CreateOrder',
            variables: {
              input: data,
            },
            query:
              'mutation CreateOrder($input: CreateOrderInput!) {\n  createOrder(input: $input) {\n    changedOrder {\n      ...orderFragment\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment orderFragment on Order {\n  action\n  adminNotes\n  createdAt\n  date\n  daysTillPaymentDue\n  discount\n  estimatedArrivalTimeAfter\n  estimatedArrivalTimeBefore\n  exciseTax\n  exciseTaxCollected\n  gmv\n  gmvCollected\n  id\n  intaken\n  internalNotes\n  irn\n  isArchived\n  manifestGDriveFileId\n  name\n  notes\n  number\n  orgLicenseNum\n  outtaken\n  paymentStatus\n  siteLicenseNum\n  status\n  timeWindow\n  warehouseId\n  __typename\n}\n',
          }

          return cyRequest({ body }, 'createOrder')
        })
    }

    const responseHandler = (response) => {
      lineItems.forEach((item) => {
        item.skuBatch = skuBatches[item.skuBatchId]
      })

      return { ...response.body.data.createOrder.changedOrder, lineItems, site }
    }

    return { customRequestFn, responseHandler }
  },
}

const createLicenseCommand = {
  getProps: (data) => {
    const defaultData = {
      address1: '123 License Way',
      address2: 'Suite ABC',
      city: 'San Diego',
      state: 'CA',
      zip: '90001',
      contactName: 'Peter Parker',
      contactPhone: '555-555-5551',
      expirationDate: momentCaTimezone()
        .add(1, 'year')
        .toDate(),
      issuanceDate: momentCaTimezone()
        .subtract(1, 'year')
        .toDate(),
      legalEntityName: "Peter's Legally Licensed Company",
      licenseNumber: faker.random.uuid(),
      nickname: `Peter Parker's Shop #${faker.random.number()}`,
      organizationId: TEST_ORGANIZATION_MASKED_ID,
      type: 'RETAILER',
      category: 'ADULT_USE',
    }

    const input = { ...defaultData, ...data }

    const body = {
      operationName: 'CreateLicense',
      variables: {
        input,
      },
      query:
        'mutation CreateLicense($input: CreateLicenseInput!) {\n  createLicense(input: $input) {\n    changedLicense {\n      ...licenseFragment\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment licenseFragment on License {\n  id\n  nickname\n  category\n  type\n  licenseNumber\n  legalEntityName\n  issuanceDate\n  expirationDate\n  contactName\n  contactPhone\n  address1\n  address2\n  city\n  state\n  zip\n  __typename\n}\n',
    }

    const responseHandler = (response) => {
      return response.body.data.createLicense.changedLicense
    }

    return { body, responseHandler }
  },
}

const createSkuBatchCommand = {
  getProps: (data = {}, options = {}) => {
    let { skuId, batchId } = data
    const { initializeInventory } = options

    delete data.skuId
    delete data.batchId

    let sku
    let batch
    let skuBatch
    let res

    const customRequestFn = () => {
      return cy
        .then(() => {
          return ensureLoggedInAsNabisAdmin()
        })
        .then(() => {
          if (skuId) {
            return
          }

          debugger

          return cy.createSku(data).then((_sku) => {
            debugger
            sku = _sku
            skuId = sku.id
          })
        })
        .then(() => {
          if (batchId) {
            return
          }
          return cy
            .createLicense()
            .then((license) => {
              debugger
              return cy.createBatch({ licenseId: license.id })
            })
            .then((_batch) => {
              debugger
              batch = _batch
              batchId = batch.id
            })
        })
        .then(() => {
          const body = {
            operationName: 'CreateSkuBatch',
            variables: {
              skuId,
              batchId,
              titleholderId: TITLE_HOLDER_IDS.nabioneInc,
            },
            query:
              'mutation CreateSkuBatch($batchId: ID, $skuId: ID!, $titleholderId: ID!) {\n  createSkuBatch(batchId: $batchId, skuId: $skuId, titleholderId: $titleholderId) {\n    id\n    __typename\n  }\n}\n',
          }
          debugger

          return cyRequest({ body }, 'createSkuBatch')
        })
        .then((_res) => {
          res = _res

          debugger
          skuBatch = { id: res.body.data.createSkuBatch.id, sku, batch }

          if (initializeInventory) {
            return cy.createInventoryItem({ skuBatchId: skuBatch.id }).then((inventoryItem) => {
              return cy.correctInventoryItem({ id: inventoryItem.id })
            })
          }
        })
        .then(() => {
          return res
        })
    }

    const responseHandler = () => {
      return { ...skuBatch }
    }

    return { customRequestFn, responseHandler }
  },
}

const createBatchCommand = {
  getProps: (data) => {
    const defaultData = {
      code: `${faker.random.number()}-${faker.random.number()}`,
      expirationDate: momentCaTimezone()
        .add(1, 'year')
        .toDate(),
      manufacturingDate: momentCaTimezone()
        .subtract(1, 'year')
        .toDate(),
      organizationId: TEST_ORGANIZATION_MASKED_ID,
      manifestGDriveFileId: '1MVP7l9KKeY2YnTCGGcWPV0ITfAW0FeXi',
    }

    let input = { ...defaultData, ...data }

    const body = {
      operationName: 'CreateBatch',
      variables: {
        input,
      },
      query:
        'mutation CreateBatch($input: CreateBatchInput!) {\n  createBatch(input: $input) {\n    ...batchFragment\n    __typename\n  }\n}\n\nfragment batchFragment on Batch {\n  id\n  code\n  testDate\n  deletedAt\n  status\n  expirationDate\n  manufacturingDate\n  manifestGDriveFileId\n  createdAt\n  __typename\n}\n',
    }

    const responseHandler = (response) => {
      return response.body.data.createBatch
    }

    return { responseHandler, body }
  },
}

const createSkuCommand = {
  getProps: (data = {}) => {
    const defaultData = {
      name: `${faker.name.firstName()}'s Product #${faker.random.number()}`,
      code: `${faker.random.number()}-${faker.random.number()}`,
      isExciseTaxable: true,
      inventoryType: 'CANNABIS',
      inventoryClass: 'FLOWER',
      inventoryCategory: 'BUD',
      pricePerUnit: 12,
      unit: 'Ounce',
      organizationId: TEST_ORGANIZATION_MASKED_ID,
    }

    const input = { ...defaultData, ...data }

    const body = {
      operationName: 'CreateSku',
      variables: {
        input,
      },
      query:
        'mutation CreateSku($input: CreateSkuInput!) {\n  createSku(input: $input) {\n    changedSku {\n      id\n      __typename\n    }\n    __typename\n  }\n}\n',
    }

    const responseHandler = (response) => {
      const _body = response.body
      return { ...input, id: _body.data.createSku.changedSku.id }
    }

    return { body, responseHandler }
  },
}

const createInventoryItemCommand = {
  getProps: (data) => {
    const defaultData = {
      warehouseId: WAREHOUSES.oak.id,
      organizationId: TEST_ORGANIZATION_MASKED_ID,
      skuBatchId: '',
    }

    data = { ...defaultData, ...data }

    const body = {
      operationName: 'CreateInventoryItem',
      variables: data,
      query:
        'mutation CreateInventoryItem($organizationId: ID!, $skuBatchId: ID!, $warehouseId: ID!) {\n  createInventoryItem(organizationId: $organizationId, skuBatchId: $skuBatchId, warehouseId: $warehouseId) {\n    id\n    __typename\n  }\n}\n',
    }

    const customRequestFn = () => {
      return ensureLoggedInAsNabisAdmin().then(() => {
        return cyRequest({ body }, 'inventoryItem')
      })
    }

    const responseHandler = (response) => {
      return response.body.data.createInventoryItem
    }

    return { customRequestFn, responseHandler }
  },
}

/**
 * Creates a user and sets the new user's AUTH_TOKEN in localstorage i.e. logs the
 * user into the app.
 */
const createUserCommand = {
  getProps: (data) => {
    const defaultData = {
      password: DEFAULT_PASSWORD,
      firstName: 'Bruce',
      lastName: 'Wayne',
      phone: '2014442223',
      invitationId: '',
    }

    data = { ...defaultData, ...data }
    let isAdmin = !!data.isAdmin

    const email = data.email || generateRandomEmail({ firstName: data.firstName, isAdmin: data.isAdmin })

    delete data.isAdmin

    const input = { ...data, email }

    const body = {
      operationName: 'CreateUser',
      variables: {
        input,
      },
      query:
        'mutation CreateUser($input: CreateUserInput!) {\n  createUser(input: $input) {\n    token\n    changedUser {\n      id\n      email\n      __typename\n    }\n    __typename\n  }\n}\n',
    }

    const responseHandler = (response) => {
      const body = response.body

      const user = body.data.createUser.changedUser
      const authToken = body.data.createUser.token

      window.localStorage.setItem('AUTH_TOKEN', authToken)
      window.localStorage.setItem('isAdmin', isAdmin)

      cy.log('AUTH_TOKEN ', window.localStorage.getItem('AUTH_TOKEN'))

      const userInfo = {
        firstName: data.firstName,
        lastName: data.lastName,
        email,
        isAdmin,
      }

      cy.log(`User created and logged in: ${JSON.stringify(userInfo)}`)

      return cy
        .setCookie('nabis_user_profile_development', JSON.stringify(body.data.createUser.changedUser))
        .then(() => cy.setCookie('nabis_user_token_development', body.data.createUser.token))
        .then(() => {
          const date = new Date(Date.now() + 10000000000)
          const dateString = date.toUTCString()
          return cy.setCookie('expires', dateString)
        })
        .then(() => {
          return { ...user, ...userInfo, authToken }
        })
    }

    return { body, responseHandler, headers: baseHeaders }
  },
}

const createUserRoleCommand = {
  getProps: (data) => {
    const { role, userId } = data

    const body = {
      operationName: 'CreateUserRole',
      variables: {
        input: {
          roleId: ROLE_IDS[role],
          userId,
        },
      },
      query:
        'mutation CreateUserRole($input: CreateUserRoleInput!) {\n  createUserRole(input: $input) {\n    id\n    email\n    phone\n    firstName\n    lastName\n    roles {\n      id\n      __typename\n    }\n    __typename\n  }\n}\n',
    }

    const responseHandler = (response) => {
      // Returns a user, not a userRole
      return response.body.data.createUserRole
    }

    return { body, responseHandler }
  },
}

const correctInventoryItemCommand = {
  getProps: (data) => {
    const defaultData = {
      endCounted: 100,
      notes: 'Should be 100 items',
      id: '',
      transactionType: 'CORRECTION',
    }

    const body = {
      operationName: 'CorrectInventoryItem',
      variables: {
        input: { ...defaultData, ...data },
      },
      query:
        'mutation CorrectInventoryItem($input: CorrectInventoryItemInput!) {\n  correctInventoryItem(input: $input) {\n    changedInventoryItem {\n      id\n      __typename\n    }\n    __typename\n  }\n}\n',
    }

    const customRequestFn = () => {
      return cy.login({}, { asNabisAdmin: true }).then((user) => {
        return cyRequest({ body }, 'correctInventoryItem')
      })
    }

    const responseHandler = (response) => {
      return response.body.data.correctInventoryItem.changedInventoryItem
    }

    return { customRequestFn, responseHandler }
  },
}

const createSiteCommand = {
  getProps: (data) => {
    const firstName = faker.name.firstName()
    const lastName = faker.name.lastName()

    const defaultData = {
      name: faker.company.companyName(),
      pocName: firstName + ' ' + lastName,
      pocEmail: `${firstName}-${faker.random.number()}@${faker.internet.domainName()}`,
      pocPhoneNumber: faker.phone.phoneNumberFormat(),
      address1: faker.address.streetAddress(),
      address2: faker.address.secondaryAddress(),
      city: faker.address.city(),
      state: 'CA',
      zip: '94612',
      siteCategory: 'RETAILER',
      licensedLocationId: '',
      organizationId: TEST_ORGANIZATION_MASKED_ID,
    }

    data = { ...defaultData, ...data }
    let { licensedLocationId } = data
    let licensedLocation

    let body = {
      operationName: 'CreateSite',
      variables: {
        input: data,
      },
      query:
        'mutation CreateSite($input: CreateSiteInput!) {\n  createSite(input: $input) {\n    changedSite {\n      id\n      __typename\n    }\n    __typename\n  }\n}\n',
    }

    const customRequestFn = () => {
      return cy
        .then(() => {
          if (licensedLocationId) {
            return cy
          }
          return cy.createLicensedLocation().then((_licensedLocation) => {
            licensedLocation = _licensedLocation
            data = { ...data, licensedLocationId: licensedLocation.id }
            body = { ...body, variables: { input: data } }
          })
        })
        .then(() => {
          return cyRequest({ body }, 'createSite')
        })
    }

    const responseHandler = (response) => {
      return { id: response.body.data.createSite.changedSite.id, ...data, licensedLocation }
    }

    return { customRequestFn, responseHandler }
  },
}

const createLicensedLocationCommand = {
  getProps: (data) => {
    const companyName = faker.company.companyName()

    const defaultData = {
      name: companyName,
      address1: faker.address.streetAddress(),
      address2: faker.address.secondaryAddress(),
      city: faker.address.city(),
      state: 'CA',
      zip: '90001',
      siteCategory: 'RETAILER',
      licenses: [
        {
          address1: faker.address.streetAddress(),
          address1: faker.address.secondaryAddress(),
          city: faker.address.city(),
          state: 'CA',
          zip: '90001',
          contactName: faker.name.firstName() + ' ' + faker.name.lastName(),
          contactPhone: faker.phone.phoneNumberFormat(),
          nickname: `${companyName}'s license`,
          category: 'ADULT_USE',
          type: 'RETAILER',
          licenseNumber: faker.random.number() + '-' + faker.random.number(),
          legalEntityName: companyName + ' ' + 'entity',
          issuanceDate: momentCaTimezone()
            .subtract(1, 'year')
            .toISOString(),
          expirationDate: momentCaTimezone()
            .add(1, 'year')
            .toISOString(),
        },
      ],
    }

    data = { ...defaultData, ...data }

    const body = {
      operationName: 'CreateLicensedLocation',
      variables: {
        input: data,
      },
      query:
        'mutation CreateLicensedLocation($input: CreateLicensedLocationInput!) {\n  createLicensedLocation(input: $input)\n}\n',
    }

    const responseHandler = (response) => {
      return { id: response.body.data.createLicensedLocation, ...data }
    }

    return { body, responseHandler }
  },
}

/**
 * Shape of a CRUD command:
 *
 * const someCrudCommand = {
 *    headers: object, // Object representing request headers properties
 *    getProps: (data: object) => {
 *      body?: object, // request body
 *      customRequestFn?: (data) => Cypress Promise<HTTP Response Object>,
 *      responseHandler: (response) => any
 *    }
 * }
 */
const crudCommands = {
  correctInventoryItem: correctInventoryItemCommand,
  createBatch: createBatchCommand,
  createInventoryItem: createInventoryItemCommand,
  createLicense: createLicenseCommand,
  createSku: createSkuCommand,
  createSkuBatch: createSkuBatchCommand,
  createUser: createUserCommand,
  login: loginCommand,
  createOrder: createOrderCommand,
  createLicensedLocation: createLicensedLocationCommand,
  createSite: createSiteCommand,
  createUserRole: createUserRoleCommand,
}

// Add CRUD commands
for (let commandName in crudCommands) {
  const command = crudCommands[commandName]

  Cypress.Commands.add(commandName, (data, options) => {
    const { getProps } = command

    const { body, responseHandler, customRequestFn, headers } = getProps(data, options)

    if (!body && !customRequestFn) {
      throw Error(
        `Neither a body or customRequestFn property was found for "${commandName}" command. You probably forgot to return those properties from within getProps() of the command, or misspelled a property.`,
      )
    }

    if (!responseHandler) {
      throw Error(`reponseHandler function not found for command "${commandName}".`)
    }

    const makeRequest = () => {
      if (customRequestFn) {
        return customRequestFn()
      }

      return cyRequest({ body, headers }, commandName)
    }

    return makeRequest().then((response) => {
      const _body = response.body

      expect(response.status).to.eq(200)

      if (_body.errors) {
        throw Error(`Error requesting ${commandName}: ${JSON.stringify(_body.errors)}`)
      }

      expect(_body.errors).to.be.undefined

      return responseHandler(response)
    })
  })
}
