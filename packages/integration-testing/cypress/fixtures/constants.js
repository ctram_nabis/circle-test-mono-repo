export const CA_TIMEZONE = 'America/Los_Angeles'

export const BASE_URL = process.env.BASE_URL || 'localhost:3000'

export const num = () => Math.floor(Math.random() * 10000)

export const counted = 50
export const allocated = -25

export const name = `dank OG kush greeeen machine`
export const code = `DOGKGM`

export const SKUS = {
  gummy: {
    code: 'Gummy 01 TEST',
  },
}

export const draftOrder = `Draft order to test for suite 4`

export const NABIS_DELIVERIES_PASSWORD = 'e382ffc8-d96d-47b7-987f-16ec159b889a'
export const NABIS_DELIVERIES_EMAIL = 'deliveries@nabis.com'

// Masked IDs
export const ROLE_IDS = {
  admin: 'Um9sZTpkMmEzMTFlNS1iMzRhLTQxOTItOGRhYi03YzNhNzhiNDcyNmI=',
  manager: 'Um9sZTpiZDkyMjk1NC1mZWYxLTRhMmMtOTQyZi05Yzg0MWE5NzgzZjc=',
  user: 'Um9sZToyNDA4NzA0MC05YjQ3LTRhMGMtOTVkYS1hZjUzNDAwM2MxM2E=',
  viewer: 'Um9sZTo0NDllYjJlOC1kMzUxLTQxMTYtOTlkMC1jNDNlZTUxYjU5Yzc=',
}

// Test Org ID hardcoded in 'packages/backend/src/config/testing.js'; users created in testing app
// by default are part of this organization, see 'packages/backend/src/schema/resolvers/userResolver.js'
export const TEST_ORGANIZATION_MASKED_ID = 'T3JnYW5pemF0aW9uOmUyM2E4ODFmLTY0NWEtNDMzMS05ZTExLWY1NWRiZjRiYmZmMg=='
export const TEST_ORGANIZATION_USER_MASKED_ID = 'VXNlcjpjMTRlNGUxZS1hNDk1LTQ5YjMtOTdlOC04N2IxMzhiZmIzNzk' // philip@getnabis.com

export const DEFAULT_PASSWORD = 'test1231'

export const WAREHOUSES = {
  oak: {
    id: 'V2FyZWhvdXNlOjZjZWFmZTIyLWMxMjctMTFlOC1hMzU1LTUyOTI2OWZiMTQ1OQ==',
  },
  la: {
    id: 'V2FyZWhvdXNlOjg1YWUzNGYzLWI5YzctNDE1Yi1iNzVlLWE4NWU2MTJjNWJmNQ==',
  },
}

export const SITES = {
  nabione: {
    id: 'U2l0ZTpjNTVjZGZjOS1kMmU1LTQ3ZDQtYWM5My1mMjA5MGEzY2I3MzE=',
    licensedLocationId: 'TGljZW5zZWRMb2NhdGlvbjo4ZGVmYmFmZS05MDZmLTQxZWEtYjQwZi0xYjkyMGRmZGY0MGY=',
    siteLicenseNum: 'C11-0000150-LIC',
  },
  zasp: {
    id: 'U2l0ZToyZGVhZjcwYy1hYTUzLTQ5NTctYjE0YS1mYWFkYzM5MzA3NDM=',
    licensedLocationId: 'TGljZW5zZWRMb2NhdGlvbjpiMTkxYTljYS1kNmIyLTRkNDctYjdiNC05NjYyMjVlMTAyMTA=',
    siteLicenseNum: 'C11-0000497-LIC',
  },
}

export const TITLE_HOLDER_IDS = {
  nabioneInc: 'VGl0bGVob2xkZXI6ODgwM2U4MGMtYWMxZS00MDQ1LTgyODItYTg5OTJkMDdkOWM1',
}

export const ORDER_ACTIONS = {
  DELIVERY_TO_RETAILER: 'DELIVERY_TO_RETAILER',
  RETURN_FROM_RETAILER: 'RETURN_FROM_RETAILER',
  PICKUP_FROM_BRAND: 'PICKUP_FROM_BRAND',
  DELIVERY_TO_BRAND: 'DELIVERY_TO_BRAND',
  DROPOFF_TO_NABIS: 'DROPOFF_TO_NABIS',
  PICKUP_FROM_NABIS: 'PICKUP_FROM_NABIS',
  PAYMENT_PICKUP_FROM_RETAILER: 'PAYMENT_PICKUP_FROM_RETAILER',
  INTERNAL_TRANSFER: 'INTERNAL_TRANSFER',
  OTHER: 'OTHER',
}
