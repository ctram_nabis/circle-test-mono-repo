const getAvailable = (content) => {
  let la = parseInt(
    content
      .split('(')[1]
      .split(',')[0]
      .split(': ')[1],
  )
  let oak = parseInt(content.split('Oak: ')[1].split(')')[0])
  return { la, oak }
}
export default (cy, objects) => {
  cy.contains('New Order').click()
  cy.get('[name=lineItem]').click()
  cy.get('div[role=listbox] > div[role=option] > span', {
    mutiple: true,
    force: true,
  }).each((result) => {
    console.log(result)
    let text = result['0'].textContent
    let index = objects.findIndex((o) => text.includes(o.name))
    let found = objects[index]

    if (found) {
      let { la, oak } = getAvailable(text)
      expect(oak).to.eql(parseInt(found.OAK.counted + found.OAK.allocated))
      expect(la).to.eql(parseInt(found.LA.counted + found.LA.allocated))
    }
  })
}
