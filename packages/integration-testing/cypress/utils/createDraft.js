import { num, draftOrder } from '../fixtures/constants'

const name = `${draftOrder} - ${num()}`

export default function createDraft({ draftName, quantityChosen, sku }) {
  // cy.writeFile('cypress/fixtures/email.txt', email) // what's this for?
  cy.createUser()
  cy.visit(`/my-organizations`)
  cy.log('Waiting for organizations to fully load.')
  cy.contains('Testing One Two Three Co.').click()

  cy.then(function() {
    cy.contains('New Order').click()
    cy.log('Waiting for Orders to load.')

    cy.get('input[name=name]').type(draftName)
    cy.get('input[name=irn]').type(draftName)

    cy.get('[name=date]').click()
    cy.get('div.react-datepicker__week:last-child > div:nth-child(6)').click()

    cy.get('[name=daysTillPaymentDue]').click()
    cy.contains('Net 7').click({ force: true })

    cy.get('[name=site]').click()
    cy.get('.visible.menu > [role=option]:first-child').click()

    cy.get('[name=lineItem]')
      .click()
      .type(`${sku.code}{enter}`)
      .then((span) => {
        span = span[0]

        span.click()
        cy.get('[name=quantity]')
          .clear()
          .type(quantityChosen)
      })

    cy.contains('Save Draft').click()
    cy.contains('Saved!') // Save button changes text
    cy.contains('Save Draft') //Save button reverts back to previous text
    cy.url().should('include', 'orders/new') // Still on same page
  })
}
