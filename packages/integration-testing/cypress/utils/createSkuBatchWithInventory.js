// Don't think we need this anymore, there is a Cypress command that should handle this.
export default function createSkuBatchWithInventory() {
  // Only an admin user can create a SkuBatch
  return cy.createUser({ isAdmin: true }).then(() => {
    let inventoryItem
    let skuBatch

    return cy
      .createSkuBatch()
      .then((_skuBatch) => {
        skuBatch = _skuBatch
        return cy.createInventoryItem({ skuBatchId: skuBatch.id })
      })
      .then((_inventoryItem) => {
        inventoryItem = _inventoryItem
        return cy.correctInventoryItem({ id: inventoryItem.id })
      })
      .then(() => {
        return { ...skuBatch, inventoryItem }
      })
  })
}
