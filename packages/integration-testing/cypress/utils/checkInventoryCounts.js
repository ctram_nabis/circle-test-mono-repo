const { _ } = Cypress // Lodash

function areNumbers() {
  return Array.prototype.every.call(arguments, (arg) => {
    return _.isNumber(arg)
  })
}

const VALID_INVENTORY_PROPS = ['countOutgoing', 'countIncoming', 'counted']
const VALID_WAREHOUSES = ['OAK', 'LA']

function validateSkuBatchInventoryItem(inventoryItem) {
  for (let propName in inventoryItem) {
    if (propName === 'name') {
      continue
    }

    if (VALID_WAREHOUSES.includes(propName)) {
      const warehouseInventory = inventoryItem[propName]

      for (let propName in warehouseInventory) {
        if (!VALID_INVENTORY_PROPS.includes(propName)) {
          throw Error(`${propName} is not a recognized property. Perhaps you need to update VALID_INVENTORY_PROPS?`)
        }
      }

      continue
    }

    throw Error(`${propName} is not a valid property. Valid properties are: ${VALID_WAREHOUSES} and name`)
  }
}

export default (cy, skuBatchInventoryItems) => {
  // skuBatchInventoryItems is an array of skuBatchInventoryItems
  // { name: <skubatch>, OAK: { countOutgoing: 0, countIncoming: 0 }, LA: { countOutgoing: 0, countIncoming: 0 } }

  cy.contains('Inventory').click()
  cy.log('Waiting for Inventory to load')

  cy.get('thead')
    .contains('OAK')
    .click()
  cy.get('thead')
    .contains('LA')
    .click()

  skuBatchInventoryItems.forEach((inventoryItem) => {
    inventoryItem = {
      ...inventoryItem,
      OAK: inventoryItem.OAK || {},
      LA: inventoryItem.LA || {},
    }
    validateSkuBatchInventoryItem(inventoryItem)

    cy.get('.prompt')
      .clear()
      .type(inventoryItem.name)

    cy.get('[data-test=sku-batch-row]', { mutiple: true }).then(($rows) => {
      let el

      for (let i = 0; i < $rows.length; i++) {
        const row = $rows[i]

        if (row.textContent.includes(inventoryItem.name)) {
          el = row
          break
        }
      }

      if (!el) return

      const children = el.children

      // DOM elements
      const elCountedTotal = children[6]
      const elCountedOak = children[7]
      const elCountedLa = children[8]

      const elIncomingTotal = children[9]
      const elIncomingOak = children[10]
      const elIncomingLa = children[11]

      const elOutgoingTotal = children[12]
      const elOutgoingOak = children[13]
      const elOutgoingLa = children[14]

      const elAvailableTotal = children[15]
      const elAvailableOak = children[16]
      const elAvaiableLa = children[17]

      // Get actual counts displayed on the UI
      const countedTotal = parseInt(elCountedTotal.textContent)
      const countedOak = parseInt(elCountedOak.textContent)
      const countedLa = parseInt(elCountedLa.textContent)

      const countIncomingTotal = parseInt(elIncomingTotal.textContent)
      const countIncomingOak = parseInt(elIncomingOak.textContent)
      const countIncomingLa = parseInt(elIncomingLa.textContent)

      const countOutgoingTotal = parseInt(elOutgoingTotal.textContent)
      const countOutgoingOak = parseInt(elOutgoingOak.textContent)
      const countOutgoingLa = parseInt(elOutgoingLa.textContent)

      const countAvailableTotal = parseInt(elAvailableTotal.textContent)
      const countAvailableOak = parseInt(elAvailableOak.textContent)
      const countAvailableLa = parseInt(elAvaiableLa.textContent)

      // Check counted
      if (areNumbers(inventoryItem.OAK.counted, inventoryItem.LA.counted)) {
        expect(countedTotal).to.equal(inventoryItem.OAK.counted + inventoryItem.LA.counted)
      }
      if (areNumbers(inventoryItem.OAK.counted)) {
        expect(countedOak).to.equal(inventoryItem.OAK.counted)
      }
      if (areNumbers(inventoryItem.LA.counted)) {
        expect(countedLa).to.equal(inventoryItem.LA.counted)
      }

      // Check incoming counts on the UI against what they should be
      if (areNumbers(inventoryItem.OAK.countIncoming, inventoryItem.LA.countIncoming)) {
        expect(countIncomingTotal).to.equal(inventoryItem.OAK.countIncoming + inventoryItem.LA.countIncoming)
      }
      if (areNumbers(inventoryItem.OAK.countIncoming)) {
        expect(countIncomingOak).to.equal(inventoryItem.OAK.countIncoming)
      }
      if (areNumbers(inventoryItem.LA.countIncoming)) {
        expect(countIncomingLa).to.equal(inventoryItem.LA.countIncoming)
      }

      // Check outgoing and available counts on the UI against what they should be
      if (areNumbers(inventoryItem.OAK.countOutgoing, inventoryItem.LA.countOutgoing)) {
        expect(countOutgoingTotal).to.equal(0 - inventoryItem.OAK.countOutgoing - inventoryItem.LA.countOutgoing)
        expect(countAvailableTotal).to.equal(
          countedTotal - inventoryItem.OAK.countOutgoing - inventoryItem.LA.countOutgoing,
        )
      }
      if (areNumbers(inventoryItem.OAK.countOutgoing)) {
        expect(countOutgoingOak).to.equal(0 - inventoryItem.OAK.countOutgoing)
        expect(countAvailableOak).to.equal(countedOak - inventoryItem.OAK.countOutgoing)
      }
      if (areNumbers(inventoryItem.LA.countOutgoing)) {
        expect(countOutgoingLa).to.equal(0 - inventoryItem.LA.countOutgoing)
        expect(countAvailableLa).to.equal(countedLa - inventoryItem.LA.countOutgoing)
      }
    })
  })
}
