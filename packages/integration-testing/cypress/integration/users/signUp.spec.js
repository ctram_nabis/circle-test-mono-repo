const SIGNUP_URL = '/signup'
const randEmail = `jane-${Math.floor(Math.random() * 10000)}@doe.com`
const password = 'superduperpassword'

describe('Users', function() {
  describe('SignUp', function() {
    beforeEach(() => {
      window.localStorage.clear()
    })

    it('create a user', function() {
      const selectorEmail = 'input[name="email"]'
      const selectorPassword = 'input[name="password"]'

      cy.visit(SIGNUP_URL)

      cy.get('input[name="firstName"]')
        .type('Jane')
        .should('have.value', 'Jane')

      cy.get('input[name="lastName"]')
        .type('Doe')
        .should('have.value', 'Doe')

      cy.get(selectorEmail)
        .type(randEmail)
        .should('have.value', randEmail)

      cy.get('input[name="phone"]')
        .type('123-456-7890')
        .should('have.value', '123-456-7890')

      cy.get(selectorPassword)
        .type(password)
        .should('have.value', password)

      cy.get('input[name="confirmPassword"]')
        .type(password)
        .should('have.value', password)
        .type('{enter}')

      // Assert dashboard
      cy.url().should('include', '/my-organizations')

      // Leave
      cy.contains('Sign Out').click()

      cy.url().should('include', '/')

      cy.get(selectorEmail)
        .type(randEmail)
        .should('have.value', randEmail)

      cy.get(selectorPassword)
        .type(password)
        .should('have.value', password)
        .type('{enter}')

      // Assert dashboard
      cy.url().should('include', '/my-organizations')
      cy.contains('Testing One Two Three Co.')
      cy.get('[data-test=organization-row]').should('have.length', 1) // Ensure there is only one organization listed
    })
  })
})
