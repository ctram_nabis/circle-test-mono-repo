describe('Users', function() {
  describe('Sign In', function() {
    const password = 'someReallyGoodPassword123#423%5'

    before(() => {
      window.localStorage.clear()

      cy.createUser({ password }).then((user) => {
        cy.wrap(user.email).as('email')
        window.localStorage.clear() // Clear AUTH_TOKEN so we don't log in automatically
      })
    })

    beforeEach(() => {
      cy.visit('/')
    })

    it('sign in and sign out', function() {
      const { email } = this

      cy.get('[name=email]')
        .type(email)
        .should('have.value', email)
      cy.get('[name=password]')
        .type(password)
        .should('have.value', password)
        .type('{enter}')

      cy.url().should('include', '/my-organizations')
      cy.contains('Testing One Two Three Co.')
      cy.get('[data-test=organization-row]').should('have.length', 1) // Ensure there is only one organization listed

      cy.contains('Sign Out').click()

      cy.url().should('include', '/sign-in')

      cy.contains('Sign into your account')
    })

    it('fails to signs in with incorrect password', function() {
      const { email } = this
      cy.get('[name=email]').type(email)

      cy.get('[name=password]').type('somewrongpassword{enter}')

      cy.contains('The password did not match')
      cy.url().should('include', '/sign-in')
    })

    it('fails to signs in with incorrect email', function() {
      cy.get('[name=email]').type('somerandomemail@random.com')

      cy.get('[name=password]').type('somewrongpassword{enter}')

      cy.contains('Incorrect username or password. Please try again')
      cy.url().should('include', '/sign-in')
    })
  })
})
