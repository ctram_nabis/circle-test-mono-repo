module.exports = {
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT || 5432,
  username: process.env.DB_USER || 'Nabis',
  password: process.env.DB_PASSWORD || '',
  database: process.env.DB_DATABASE || 'Logistics',
  dialect: 'postgres',
}
