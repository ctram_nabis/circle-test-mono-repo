### Commands
1. `yarn build` Builds all containers (postgres, backend, and frontend)
2. `yarn up` Runs all containers in development mode
3. `yarn down` Stops all running containers
4. `yarn test` Initializes testing environment and runs the tests

### TODO
1. When running `yarn up`, the default Logistics database currently fails because of the way we handle schema / migrations. It gives a `relation "User" not found` error. You'll need to switch to run 1) `yarn test` and 2) switch to use the `testing` database in `docker-compose.yml` to run with data preloaded.

### Docker
#### Frontend
Notes on hot reloading:
- Hot-reloading works out of the box if you run `yarn run up-development`. However, if you have your browser open to `localhost:3000` and ran `yarn run up-development`, you'll need to refresh the page once the frontend server is ready. After that, all reloads will be automatic.

*Why is this?*
Hot-reloading works by maintaing a WebSocket connection that captures when a change has occured in the filesystem. If you kill your frontend server, the WebSocket connection is terminated. Reloading your browser re-enables the connection and hot reloading works again.

You can see that this is the case because your terminal says that it's compiling, but no changes are sent to the front-end. Once you reload, the connection is recreated.

#### Postgres
`./docker_storage/postgres` Docker Compose mounts a volume here so all postgres data is stored in this folder.
`./docker_storage/postgres_initdb` The Postgres image runs any scripts here to initialize the database. [Read more here](https://hub.docker.com/_/postgres)

#### Backend
