# !/bin/bash
echo 'DROP USER IF EXISTS "'$DB_USER'"; DROP DATABASE IF EXISTS "'$DB_DATABASE'";' | psql --username=postgres --host=$DB_HOST --port=$DB_PORT;
echo 'CREATE USER "'$DB_USER'" WITH SUPERUSER; CREATE DATABASE "'$DB_DATABASE'";' | psql --username=postgres --host=$DB_HOST --port=$DB_PORT;
echo 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";' | psql --username=$DB_USER --host=$DB_HOST --port=$DB_PORT --dbname=$DB_DATABASE;

# pg_dump -h nabis-prod-2.cnwlqcoumxfz.us-west-2.rds.amazonaws.com -U Nabis -d Logistics -f db-backup.sql --no-owner --no-privileges --clean --verbose --if-exists;
psql -h $DB_HOST -d $DB_DATABASE -U $DB_USER -v ON_ERROR_STOP=1 -f ./db-backup.sql;
