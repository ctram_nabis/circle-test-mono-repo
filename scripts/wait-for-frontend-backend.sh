#!/bin/sh
# wait-for-postgres.sh

set -e

host="$1"
shift
cmd="$@"

until curl localhost:3001 && curl localhost:8081; do
  >&2 echo "Waiting for frontend and backend to finish booting..."
  sleep 1
done

>&2 echo "Frontend and Backend are up!"
exec $cmd
