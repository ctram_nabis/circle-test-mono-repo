#!/bin/sh

set -e

docker-compose up postgres-testing & ./scripts/wait-for.sh 'psql -h localhost -p 54320 -U NabisTest LogisticsTest -c \q' "postgres-testing still booting..." "postgres-testing ready!"
yarn migrate:testing
yarn start:testing & ./scripts/wait-for.sh 'curl localhost:3001 && curl localhost:8081' "Testing app still booting..." "Testing app ready!"
mkdir -p /tmp/artifacts
touch /tmp/artifacts/cypress-test-results
cd packages/integration-testing
yarn cypress:run --reporter-options --no-color  2>&1 | tee /tmp/artifacts/cypress-test-results