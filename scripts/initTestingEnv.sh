# !/bin/bash
echo 'DROP DATABASE IF EXISTS "'$DB_DATABASE'";' | psql --username=$DB_USER -d $DB_DATABASE --host=$DB_HOST --port=$DB_PORT;
echo 'DROP USER IF EXISTS "'$DB_USER'";' | psql --username=postgres -d $DB_DATABASE --host=$DB_HOST --port=$DB_PORT;
echo 'CREATE USER "'$DB_USER'" WITH SUPERUSER; CREATE DATABASE "'$DB_DATABASE'";' | psql --username=postgres --host=$DB_HOST --port=$DB_PORT;
echo 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";' | psql --username=$DB_USER --host=$DB_HOST --port=$DB_PORT --dbname=$DB_DATABASE;
psql testing < './db/schema/schema.sql'
npx sequelize db:migrate;
