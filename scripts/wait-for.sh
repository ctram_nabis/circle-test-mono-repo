#!/bin/sh

# Example use
# ./wait-for.sh "curl localhost:3001" "Retrying..." "Curl finished"

set -e

dependent_command="$1"
retry_message="$2"
finish_message="$3"

until $dependent_command; do
  >&2 echo 
  >&2 echo "####################"
  >&2 echo "$retry_message"
  >&2 echo "####################"
  >&2 echo 
  sleep 1
done

>&2 echo 
>&2 echo "####################"
>&2 echo "$finish_message"
>&2 echo "####################"
>&2 echo 