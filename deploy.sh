#!/bin/bash

# Set ECR environment variables
$(~/.local/bin/aws ecr get-login --no-include-email --region us-west-2)

# Deploy backend
cd packages/backend

# Build docker backend and tag it
docker build -t nabis-backend .

if [ "${CIRCLE_BRANCH}" == "master" ]; then

    # Push production docker image to AWS
    docker tag nabis-backend 585798724347.dkr.ecr.us-west-2.amazonaws.com/prod-nabis-backend:latest
    docker push 585798724347.dkr.ecr.us-west-2.amazonaws.com/prod-nabis-backend:latest

    # Trigger new deployment on AWS
    ~/.local/bin/aws ecs update-service --service NabisProductionBackend --cluster NabisProductionBackend --task-definition NabisBackend:13 --force-new-deployment

elif [ "${CIRCLE_BRANCH}" == "staging" ]; then

    # Push staging docker image to AWS
    docker tag nabis-backend 585798724347.dkr.ecr.us-west-2.amazonaws.com/staging-nabis-backend:latest
    docker push 585798724347.dkr.ecr.us-west-2.amazonaws.com/staging-nabis-backend:latest

    # Trigger new deployment on AWS
    ~/.local/bin/aws ecs update-service --service NabisStagingBackend --cluster NabisStagingBackend --task-definition NabisBackendStaging:7 --force-new-deployment
fi

# Deploy frontend
cd ../../packages/frontend

# Build frontend "react-scripts build"

if [ "${CIRCLE_BRANCH}" == "master" ]; then
    npm run build-production
    # Deploy to production on Zeit
    now build --token $ZEIT_TOKEN --team $ZEIT_TEAM -A ../now.prod.json
    now alias --token $ZEIT_TOKEN --team $ZEIT_TEAM -A now.prod.json
    now alias --token $ZEIT_TOKEN --team $ZEIT_TEAM nabis-app.now.sh beta.getnabis.com
    now alias --token $ZEIT_TOKEN --team $ZEIT_TEAM nabis-app.now.sh app.getnabis.com
elif [ "${CIRCLE_BRANCH}" == "staging" ]; then
    npm run build-staging
    # Deploy to staging on Zeit
    now build --token $ZEIT_TOKEN --team $ZEIT_TEAM -A ../now.staging.json
    now alias --token $ZEIT_TOKEN --team $ZEIT_TEAM -A now.staging.json
    now alias --token $ZEIT_TOKEN --team $ZEIT_TEAM nabis-app-staging.now.sh beta.staging.getnabis.com
    now alias --token $ZEIT_TOKEN --team $ZEIT_TEAM nabis-app-staging.now.sh app.staging.getnabis.com
fi
