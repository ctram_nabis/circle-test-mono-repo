SELECT
"Order"."date"::date as "Order__date",
"Order"."number" as "Order__number",
"Organization"."doingBusinessAs" as "Organization__doingBusinessAs",
"WarehouseSite".name as "Warehouse__name",
"Organization"."name" as "Organization__name",
"Order"."name" as "Order__name",
"Order"."gmv" as "Order__gmv",
"Order"."status" as "Order__status",
"Order"."irn" as "Order__irn",
"Order"."action" as "Order__action",
"Order"."intaken" as "Order__intaken",
"Order"."outtaken" as "Order__outtaken",
"LineItem"."quantity" as "LineItem__quantity",
"LineItem"."pricePerUnit" as "LineItem__pricePerUnit",
"Sku"."code" as "Sku__code",
"Sku"."inventoryType" as "Sku__inventoryType",
"Sku"."inventoryClass" as "Sku__inventoryClass",
"Sku"."inventoryCategory" as "Sku__inventoryCategory",
"Batch"."code" as "Batch__code",
"Batch"."manufacturingDate"::date as "Batch__manufacturingDate",
"Batch"."expirationDate"::date as "Batch__expirationDate",
"Batch"."status" as "Batch__status",
"InventoryItem"."counted" as "InventoryItem__counted",
"InventoryItem"."storageType" as "InventoryItem__storageType",
"Driver"."firstName" as "Driver__firstName",
"Driver"."lastName" as "Driver__lastName",
"LicensedLocation".name as "LicensedLocation__name",
"Organization"."address1" as "Organization__address1",
"Organization"."address2" as "Organization__address2",
"Organization"."city" as "Organization__city",
"Organization"."state" as "Organization__state",
"Organization"."zip" as "Organization__zip",
"Organization"."phone" as "Organization__phone"

FROM "Order"
JOIN "LineItem" ON "LineItem"."orderId" = "Order".id
JOIN "SkuBatch" ON "SkuBatch".id = "LineItem"."skuBatchId"
JOIN "Sku" ON "Sku".id = "SkuBatch"."skuId"
JOIN "Batch" ON "Batch".id = "SkuBatch"."batchId"
JOIN "Organization" ON "Organization".id = "Order"."organizationId"
JOIN "InventoryItem" ON "InventoryItem"."skuBatchId" = "SkuBatch".id
JOIN "Driver" ON "Driver".id = "Order"."driverId"
JOIN "Site" ON "Site".id = "Order"."siteId"
JOIN "User" ON "User".id = "Order"."creatorId"
JOIN "LicensedLocation" ON "LicensedLocation".id = "Site"."licensedLocationId"
JOIN "Warehouse" ON "Warehouse".id = "Order"."warehouseId"
JOIN "Site" "WarehouseSite" ON "WarehouseSite".id = "Warehouse"."siteId"


WHERE "Order"."status" IN ('DELIVERED', 'DELIVERED_WITH_EDITS')
AND "Order"."action" IN ('DROPOFF_TO_NABIS', 'PICKUP_FROM_BRAND')
AND "Order"."date" <= TIMESTAMP '2019-11-01 00:00:00'
AND "Order"."date" >= TIMESTAMP '2019-10-01 00:00:00';
